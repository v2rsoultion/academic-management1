<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
      'passPhrase' => '1234', //Optional
      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];