<?php

    return [

        'school_medium_type'           => array(
            ''  => 'Select Medium',
            '0' => 'Hindi',
            '1' => 'English',
            '2' => 'Both',
        ),
        'selection_status' => array(
            '0' => 'All',
            '1' => 'Selected',
        ),
        'group_type'           => array(
            ''  => 'Select Type',
            '0' => 'Student',
            '1' => 'Parent',
        ),
        'stream_status'           => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'student_type'           => array(
            '0' => 'Paid',
            '1' => 'RTE',
            '2' => 'Free',
        ),
        'fees_status'           => array(
            ''  => 'Fees status',
            '0' => 'Pending',
            '1' => 'Paid',
            '2' => 'NA',
        ),
        'mode'           => array(
            ''  => 'Mode',
            '0' => 'Offline',
            '1' => 'Online',
        ),
        'month'                    => array(
            '1'  => 'January',
            '2'  => 'February',
            '3'  => 'March',
            '4'  => 'April',
            '5'  => 'May',
            '6'  => 'June',
            '7'  => 'July',
            '8'  => 'August',
            '9'  => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ),
        'school_logo'           => array(
            'upload_path' => '/uploads/school-logo/',
            'display_path' => 'public/uploads/school-logo/',
        ),
        'school_img'           => array(
            'upload_path' => '/uploads/school-images/',
            'display_path' => 'public/uploads/school-images/',
        ),
        'student_gender'           => array(
            '0' => 'Boy',
            '1' => 'Girl',
        ),
        'annual_salary'   => array(
            'Below 1 Lac' => 'Below 1 Lac',
            'Below 2.5 Lac' => 'Below 2.5 Lac',
            'Below 5 Lac' => 'Below 5 Lac',
            'Below 7.5 Lac' => 'Below 7.5 Lac',
            'Below 10 Lac' => 'Below 10 Lac',
            'Above 10 Lac' => 'Above 10 Lac'
        ),
        'student_category'   => array(
            'General' => 'General',
            'OBC' => 'OBC',
            'SC' => 'SC',
            'ST' => 'ST'
        ),
        'student_image'           => array(
            'upload_path' => '/uploads/student-profile/',
            'display_path' => 'public/uploads/student-profile/',
        ),
        'admission_image'           => array(
            'upload_path' => '/uploads/admission-images/',
            'display_path' => 'public/uploads/admission-images/',
        ),
        'student_status'           => array(
            '0' => 'Leave',
            '1' => 'Studying',
        ),
        'staff_profile'           => array(
            'upload_path' => '/uploads/staff-profile/',
            'display_path' => 'public/uploads/staff-profile/',
        ),
        'staff_gender'           => array(
            ''  => 'Select gender',
            '0' => 'Male',
            '1' => 'Female',
        ),
        'staff_marital'           => array(
            ''  => 'Marital Status',
            '0' => 'Unmarried',
            '1' => 'Married',
        ),
        'teaching_status'           => array(
            '0' => 'Non Teaching',
            '1' => 'Teaching',
        ),
        'student_document_file'           => array(
            'upload_path' => '/uploads/student-document/',
            'display_path' => 'public/uploads/student-document/',
        ),
        'staff_document_file'           => array(
            'upload_path' => '/uploads/staff-document/',
            'display_path' => 'public/uploads/staff-document/',
        ),
        'student_leave_document_file'     => array(
            'upload_path' => '/uploads/student-leave-document/',
            'display_path' => 'public/uploads/student-leave-document/',
        ),
        'staff_leave_document_file'     => array(
            'upload_path' => '/uploads/staff-leave-document/',
            'display_path' => 'public/uploads/staff-leave-document/',
        ),
        'question_paper_document'           => array(
            'upload_path' => '/uploads/question-paper-document/',
            'display_path' => 'public/uploads/question-paper-document/',
        ),
        'week_days'      => array(
            // ''   => 'Nothing selected',
            '1'  => 'Monday',
            '2'  => 'Tuesday',
            '3'  => 'Wednesday',
            '4'  => 'Thursday',
            '5'  => 'Friday',
            '6'  => 'Saturday',
        ),
        'brochure_file'           => array(
            'upload_path' => '/uploads/brochure-file/',
            'display_path' => 'public/uploads/brochure-file/',
        ),
        'form_type'           => array(
            '0' => 'Admission',
            '1' => 'Enquiry',
        ),
        'email_receipt'           => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'status'           => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'message_via'           => array(
            '0' => 'Email',
            '1' => 'Phone',
            '2' => 'Both'
        ),
        'payment_mode'           => array(
            'Cash' => 'Cash',
            'Cheque' => 'Cheque',
            'Online' => 'Online',
            'Swipe Machine' => 'Swipe Machine'
        ),
        'student_fields'           => array(
            'student_enroll_number' => 'Enroll No',
            'student_name' => 'Name ',
            'student_type' => 'Student Type',
            'student_image' => 'Photo of student',
            'student_reg_date' => 'Date of Reg',
            'student_dob' => 'DOB ',
            'student_email' => 'Email Address',
            'student_gender' => 'Gender',
            'admission_class_id' => 'Admission Class',
            'student_privious_school' => 'School Name',
            'student_privious_class' => 'Class',
            'student_privious_tc_no' => 'TC No.',
            'student_privious_tc_date' => 'TC Date',
            'student_privious_result' => 'Result',
            'student_category' => 'Category',
            'title_id' => 'Title',
            'caste_id' => 'Caste',
            'religion_id' => 'Religion',
            'nationality_id' => 'Nationality ',
            'stream_id' => 'Stream',
            // 'student_sibling_name' => 'Sibling Name',
            // 'student_sibling_class_id' => 'Sibling Class',
            'student_adhar_card_number' => 'Adhaar Card Number',
            'group_id' => 'House/Group ',
            'student_father_name' => 'Father Name',
            'student_father_mobile_number' => 'Father’s Mobile number',
            'student_father_email' => 'Father’s Email Address',
            'student_father_occupation' => 'Father’s Occupation',
            'student_father_annual_salary' => 'Father’s Annual Salary',
            'student_mother_name' => 'Mother Name',
            'student_mother_mobile_number' => 'Mother’s Mobile number',
            'student_mother_email' => 'Mother’s Email Address',
            'student_mother_occupation' => 'Mother’s Occupation',
            'student_mother_annual_salary' => 'Mother’s Annual Salary',
            'student_mother_tongue' => 'Mother Tongue',
            'student_guardian_name' => 'Guardian Name',
            'student_guardian_email' => 'Email Address',
            'student_guardian_mobile_number' => 'Mobile Number',
            'student_guardian_relation' => 'Relation with guardian',
            'student_temporary_address' => 'Temporary Address',
            'student_temporary_city' => 'Temporary City',
            'student_temporary_state' => 'Temporary State',
            'student_temporary_country' => 'Temporary Country',
            'student_temporary_pincode' => 'Temporary Pincode',
            'student_permanent_address' => 'Permanent Address',
            'student_permanent_city' => 'Permanent Country',
            'student_permanent_state' => 'Permanent State',
            'student_permanent_county' => 'Permanent City',
            'student_permanent_pincode' => 'Permanent Pincode',
            'student_height' => 'Height',
            'student_weight' => 'Weight',
            'student_blood_group' => 'Blood Group',
            'student_vision_left' => 'Vision Right',
            'student_vision_right' => 'Vision Left',
            'medical_issues' => 'Medical Issue',
            'document_category_id' => 'Document Category',
            'student_document_details' => 'Document Details',
            'student_document_file' => 'Document File'
        ),
        'online_notes'           => array(
            'upload_path' => '/uploads/online-notes/',
            'display_path' => 'public/uploads/online-notes/',
        ),
        'refundable_status'           => array(
            '0' => 'No',
            '1' => 'Yes',
        ), 
        'rte_status'           => array(
            '0' => 'NA',
            '1' => 'Free by RTE',
        ),
        'grade_type'    => array(
            '0' => 'Percent',
            '1' => 'Marks',
        ),
        'student_for'    => array(
            '0' => 'New',
            '1' => 'Old',
            '2' => 'Both',
        ),
        'deduct_imprest_status'    => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'receipt_status'    => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'head_type'    => array(
            ''  => 'Select Head Type',
            '0' => 'One Time',
            '1' => 'Recurring',
            '2' => 'Facility',
        ),
        'hostel_type'           => array(
            '0' => 'Boys',
            '1' => 'Girls',
            '2' => 'Both',
        ),
        'template_for'      => array(
            '0'  => 'Participation',
            '1'  => 'Winner',
        ), 
        'competition_template'      => array(
            'participation_template1'  => 'Participation Template 1',
            'winner_template1'  => 'Winner Template 1',
        ), 
        'character_template'      => array(
            'character_template1'  => 'CC Template 1',
            'character_template2'  => 'CC Template 2',
        ), 
        'transfer_template'      => array(
            'transfer_template1'  => 'TC Template 1',
            'transfer_template2'  => 'TC Template 2',
            'transfer_template3'  => 'TC Template 3',
        ), 
        'holiday_for'           => array(
            '0' => 'Both',
            '1' => 'Students',
            '2' => 'Staff',
        ),

        'task_priority'         => array(
            '0' => 'Low',
            '1' => 'Medium',
            '2' => 'High',
        ),
        'task_status'           => array(
            '0' => 'Open',
            '1' => 'On Hold',
            '2' => 'Resolved',
        ),
        'admin_type'    => array(
            '1' => 'School',
            '2' => 'Staff',
            '3' => 'Parent',
            '4' => 'Student',
        ),

        'vehicle_ownership'           => array(
            ''  => 'Select Ownership',
            '0' => 'Owner',
            '1' => 'Contractor',
        ),
        'invoice_type' => array(
            '1'  => 'Purchase',
            '2'  => 'Purchase Return',
            '3'  => 'Sales',
            '4'  => 'Sales Return',
        ),
        'inv_payment_mode' => array(
            '0' => 'Cash',
            '1' => 'Credit',
            '2' => 'Cheque',
        ),
        'vehicle_document_file'           => array(
            'upload_path' => '/uploads/vehicle-document/',
            'display_path' => 'public/uploads/vehicle-document/',
        ),
        'route_type'           => array(
            // ''  => 'Select Route Type',
            '1' => 'Pickup',
            '2' => 'Drop',
        ),
        'pf_year'    => array( 
            '1' => '2019',
            '2' => '2020',
            '3' => '2021',
            '4' => '2022',
            '5' => '2023',
        ), 
        'job_durations'   => array(
            ''  => "Durations",   
            '1' => "1 Month",
            '2' => "2 Months",
            '3' => "3 Months",
        ),
        'staff_fields'      => array(
            'staff_name'                        => 'Name',
            'staff_profile_img'                 => 'Profile Image',
            'staff_aadhar'                      => 'Aadhar Number',
            'staff_UAN'                         => 'Universal Account Number',
            'staff_ESIN'                        => 'Employee State Ins. Number',
            'staff_PAN'                         => 'Permanent Account Number',
            'designation_id'                    => 'Designation',
            'staff_email'                       => 'Email',
            'staff_mobile_number'               => 'Mobile Number',
            'staff_dob'                         => 'DOB',
            'staff_gender'                      => 'Gender',
            'staff_father_name_husband_name'    => 'Father Name/Husband Name',
            'staff_father_husband_mobile_no'    => 'Contact Number',
            'staff_mother_name'                 => 'Mother Name',
            'staff_blood_group'                 => 'Blood Group',
            'staff_marital_status'              => 'Marital Status',
            'title_id'                          => 'Title',
            'caste_id'                          => 'Caste',
            'religion_id'                       => 'Religion',
            'nationality_id'                    => 'Nationality ',
            'staff_qualification'               => 'Qualification',
            'staff_specialization'              => 'Specialization',
            'staff_reference'                   => 'Reference',
            'staff_experience'                  => 'Experience',
            'staff_temporary_address'           => 'Temporary Address',
            'staff_temporary_county'            => 'Temporary Country',
            'staff_temporary_state'             => 'Temporary State',
            'staff_temporary_city'              => 'Temporary City',
            'staff_temporary_pincode'           => 'Temporary Pincode',
            'staff_permanent_address'           => 'Permanent Address',
            'staff_permanent_county'            => 'Permanent Country',
            'staff_permanent_state'             => 'Permanent State',
            'staff_permanent_city'              => 'Permanent City',
            'staff_permanent_pincode'           => 'Permanent Pincode',
        ),
        'recruitment_form'           => array(
            'upload_path' => '/uploads/recruitment-form/',
            'display_path' => 'public/uploads/recruitment-form/',
        ),

        'task_type'           => array(
            ''  => 'Select Type',
            '1' => 'Student',
            '2' => 'Staff',
        ),

        'task_file'    => array(
            'upload_path' => '/uploads/task-manager/',
            'display_path' => 'public/uploads/task-manager/',
        ),
        'exam_type'    => array(
            ''  => 'Select Type',
            '1' => 'Exam-Wise',
            '2' => 'Term-Wise',
        ),

        'tabsheet_type'    => array(
            ''  => 'Select Type',
            '1' => 'Exam-Wise',
            '2' => 'Consolidated',
        ),
        'percentage_type'    => array(
            ''  => 'Select Type',
            '1' => 'Exam-Wise',
            '2' => 'Annual',
        ),
        'book_member_type' => array(
            ''  => 'Select Member Type',
            '1' => 'Student',
            '2' => 'Staff',
        ),
        'st_cheques_status' => array(
            '0'  => 'Pending',
            '1'  => 'Cancel',
            '2'  => 'Cleared',
            '3'  => 'Bounce',
        ),

        'leave_period' => array(
            '1' => 'Monthly',
            '2' => 'Yearly',
        ),

        'staff_leave_status' => array(
            '0' => 'Pending',
            '1' => 'Approved',
            '2' => 'Disapproved',
        ),
        'conversation_attechment'           => array(
            'upload_path' => '/uploads/conversation_attechment/',
            'display_path' => 'public/uploads/conversation_attechment/',
        ),
        'pay_extra'      => array(
            '1'  => 'NA',
            '2'  => 'Per Day',
            '3'  => 'Per Lecture',
        ),
        'staff_apoint'      => array(
            '0'  => 'No',
            '1'  => 'Yes',
        ),
        'reverse_week_days'      => array(
            // ''   => 'Nothing selected',
            'Monday'    => 1,
            'Tuesday'   => 2,
            'Wednesday' => 3,
            'Thursday'  => 4,
            'Friday'    => 5,
            'Saturday'  => 6,
            'Sunday'    => 7,
        ),
    ];


    
