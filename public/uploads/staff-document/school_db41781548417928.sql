-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 23, 2019 at 10:33 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ac_entry_map`
--

CREATE TABLE `ac_entry_map` (
  `ac_entry_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_map_amt` int(11) DEFAULT NULL,
  `ac_entry_map_amt_paid` int(11) DEFAULT NULL,
  `ac_entry_map_amt_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Due,1=Paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin',
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=false,1=true',
  `android_current_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_current_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_type`, `admin_name`, `email`, `password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`, `notification_status`, `android_current_app_version`, `ios_current_app_version`) VALUES
(1, 0, 'Super Admin', 'admin@gmail.com', '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG', NULL, 'zTpMVCx0ksG97TXVKGDgILZJRGWMYrB3RHp92HYUTfqhUcKufkKYvhjN8V6Q', '2018-12-29 00:03:50', '2018-12-29 00:03:50', 1, NULL, NULL),
(2, 1, 'Academic Eye', 'school@gmail.com', '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG', NULL, 'j4IZVvkjQsPNKh2HTVHy6qYYhb86qdnKSnBpAkhKqHAig4f2Qu6jYGLzkrPq', '2018-12-29 00:03:50', '2018-12-29 00:52:19', 1, NULL, NULL),
(3, 2, 'Dipesh', '9999999999', '$2y$10$y2PL/yIeBQmkCPIvfyd5PeugzY73G9YE4XCurlNsk88os8ICV5OIm', NULL, '5X8hUFVgRyQ7awbSBGKi6SkOZHIvI5eUUtgRjOdJ1dyHqXMv9ttpYnWMzG6F', '2018-12-29 02:48:50', '2018-12-29 02:48:50', 1, NULL, NULL),
(4, 2, 'Dinesh', '8888888888', '$2y$10$YTJkgTWgC4z2vQvg3V3jkucaEh7nCq8jKlelmWKHLvAgZcwCLEQSO', NULL, 'XaTRrjc1a9X06Flp9GcN6uO9T7iH7o5N4IeY7MwNuyhkqwaIuT6gGyW5VECD', '2018-12-29 04:26:29', '2018-12-29 04:26:29', 1, NULL, NULL),
(5, 3, 'Ravi', '5555555555', '$2y$10$Et3z2MwzFq56aiCDi5qXA.jdyoEJeVRJAVeFp9erM/InosmeQSnHm', NULL, NULL, '2019-01-03 02:15:18', '2019-01-07 04:42:57', 1, NULL, NULL),
(6, 4, 'Teena', 'N0001', '$2y$10$w2JoLbFdJbzsKc224HlpvOaCQq/kSfI4jEutFP8oyh99zz3Wg4ODq', NULL, NULL, '2019-01-03 02:15:18', '2019-01-03 02:15:18', 1, NULL, NULL),
(7, 4, 'Reena', 'N0002', '$2y$10$H26RIWhHjSohsgOdbsLD1.r4cIXSt.GLG21im6SI7lhOTuL90ya1q', NULL, NULL, '2019-01-03 02:22:01', '2019-01-03 02:22:01', 1, NULL, NULL),
(8, 2, 'Vivek', '6666666666', '$2y$10$mql6J2DlphI9N3mDlYNqiORS4R/bISUOskXL/hAKmSFJUxn0IPGH2', NULL, NULL, '2019-01-04 22:55:21', '2019-01-04 22:55:21', 1, NULL, NULL),
(9, 4, 'Neha', 'N0005', '$2y$10$0MfkHlOTPhvil7Kxf.OuqurVDSPDcaC1.Ftm5CdOFmN83zh2LjIx6', NULL, '7NPGHtqCr1kwuH6Dz5CSErPw0rUP40cmKLbJsiUiIP9ozIM8U7h2V72TUHuX', '2019-01-04 23:53:31', '2019-01-04 23:53:31', 1, NULL, NULL),
(11, 3, 'Nath', '4444444444', '$2y$10$X2Uprs4vfdTHZtpP8FwX3.COkhrB2NOkW95HMis0YSQKuGgQ7VGHa', NULL, NULL, '2019-01-07 04:49:06', '2019-01-07 04:49:06', 1, NULL, NULL),
(12, 4, 'Ravina', 'N0007', '$2y$10$0bMNtoPnad.J.32NXIREuelAjCCR9SUTKgwLMbAnipgqj55wrXm/S', NULL, 'VoW1LKU4FY7ivmz3H3UA12UciUwBpDEwgXFkf2Io0mLNEZ7kZL6yijVdTvM2', '2019-01-07 04:49:06', '2019-01-07 04:49:06', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admission_data`
--

CREATE TABLE `admission_data` (
  `admission_data_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `admission_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Admission,1=Enquiry',
  `form_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` int(11) UNSIGNED NOT NULL,
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=PaidBoy,1=RTE,2=FREE',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_date` date DEFAULT NULL,
  `student_privious_result` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_session_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_class_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_section_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `current_class_id` int(10) UNSIGNED DEFAULT NULL,
  `current_section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `student_login_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_data_mode` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Offline,1=Online',
  `admission_data_pay_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_data_fees` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Paid',
  `admission_data_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Pending,1=Contacted, 2=Admitted ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admission_fields`
--

CREATE TABLE `admission_fields` (
  `admission_field_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `admission_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_keys` text COLLATE utf8mb4_unicode_ci,
  `form_pay_mode` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admission_forms`
--

CREATE TABLE `admission_forms` (
  `admission_form_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `class_ids` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_intake` int(11) DEFAULT NULL,
  `form_url` text COLLATE utf8mb4_unicode_ci,
  `form_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_instruction` text COLLATE utf8mb4_unicode_ci,
  `form_information` text COLLATE utf8mb4_unicode_ci,
  `form_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Admission,1=Enquiry',
  `brochure_id` int(10) UNSIGNED DEFAULT NULL,
  `form_last_date` date DEFAULT NULL,
  `form_fee_amount` int(10) UNSIGNED DEFAULT NULL,
  `form_pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_email_receipt` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `form_success_message` text COLLATE utf8mb4_unicode_ci,
  `form_failed_message` text COLLATE utf8mb4_unicode_ci,
  `form_message_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Email,1=Phone,2=Both',
  `form_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `api_tokens`
--

CREATE TABLE `api_tokens` (
  `token_id` int(10) UNSIGNED NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `csrf_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `app_setting_id` int(10) UNSIGNED NOT NULL,
  `android_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_mandatory` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `comapny_page_url` text COLLATE utf8mb4_unicode_ci,
  `privacy_policy_url` text COLLATE utf8mb4_unicode_ci,
  `terms_condition_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_id` int(10) UNSIGNED DEFAULT NULL,
  `book_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=General ,1=Barcoded',
  `book_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_isbn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_id` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_no` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_id` int(10) UNSIGNED DEFAULT NULL,
  `book_price` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `book_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `admin_id`, `update_by`, `book_category_id`, `book_name`, `book_type`, `book_subtitle`, `book_isbn_no`, `author_name`, `publisher_name`, `edition`, `book_vendor_id`, `book_copies_no`, `book_cupboard_id`, `book_cupboardshelf_id`, `book_price`, `book_remark`, `book_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Book 1', 0, 'subtitle', '145263', 'Author name', 'Publisher', 'Edition', 1, 0, 1, 1, 100.00, 'Remark here', 1, '2019-01-05 04:38:29', '2019-01-05 04:38:29');

-- --------------------------------------------------------

--
-- Table structure for table `book_allowance`
--

CREATE TABLE `book_allowance` (
  `book_allowance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `allow_for_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allow_for_student` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_allowance`
--

INSERT INTO `book_allowance` (`book_allowance_id`, `admin_id`, `update_by`, `allow_for_staff`, `allow_for_student`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 10, 10, '2018-12-29 00:08:42', '2019-01-05 04:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `book_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`book_category_id`, `admin_id`, `update_by`, `book_category_name`, `book_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Book Cate1', 1, '2019-01-05 04:32:37', '2019-01-05 04:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `book_copies_info`
--

CREATE TABLE `book_copies_info` (
  `book_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `book_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclusive_for_staff` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=No,1=Yes',
  `book_copy_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Available,1=Issued',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_copies_info`
--

INSERT INTO `book_copies_info` (`book_info_id`, `admin_id`, `update_by`, `book_id`, `book_unique_id`, `exclusive_for_staff`, `book_copy_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Book#1', 0, 0, '2019-01-05 04:38:29', '2019-01-05 05:30:58'),
(2, 2, 2, 1, 'Book#2', 0, 0, '2019-01-05 04:38:29', '2019-01-05 04:38:29'),
(3, 2, 2, 1, 'Book#3', 0, 0, '2019-01-05 04:38:29', '2019-01-05 04:38:29'),
(4, 2, 2, 1, 'Book#4', 0, 0, '2019-01-05 04:38:29', '2019-01-05 04:38:29'),
(5, 2, 2, 1, 'Book#5', 0, 0, '2019-01-05 04:38:29', '2019-01-05 04:38:29');

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboards`
--

CREATE TABLE `book_cupboards` (
  `book_cupboard_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboards`
--

INSERT INTO `book_cupboards` (`book_cupboard_id`, `admin_id`, `update_by`, `book_cupboard_name`, `book_cupboard_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Cup Board', 1, '2019-01-05 04:34:19', '2019-01-05 04:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboardshelfs`
--

CREATE TABLE `book_cupboardshelfs` (
  `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_capacity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_shelf_detail` text COLLATE utf8mb4_unicode_ci,
  `book_cupboardshelf_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboardshelfs`
--

INSERT INTO `book_cupboardshelfs` (`book_cupboardshelf_id`, `admin_id`, `update_by`, `book_cupboard_id`, `book_cupboardshelf_name`, `book_cupboard_capacity`, `book_cupboard_shelf_detail`, `book_cupboardshelf_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Shelf 1', 10, 'Details', 1, '2019-01-05 04:35:08', '2019-01-05 04:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `book_vendors`
--

CREATE TABLE `book_vendors` (
  `book_vendor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_vendor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_vendors`
--

INSERT INTO `book_vendors` (`book_vendor_id`, `admin_id`, `update_by`, `book_vendor_name`, `book_vendor_number`, `book_vendor_email`, `book_vendor_address`, `book_vendor_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Book Vendor', '8888888888', 'bookvendor@gmail.com', 'Address', 1, '2019-01-05 04:33:40', '2019-01-05 04:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `brochures`
--

CREATE TABLE `brochures` (
  `brochure_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `brochure_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brochure_upload_date` date DEFAULT NULL,
  `brochure_file` text COLLATE utf8mb4_unicode_ci,
  `brochure_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brochures`
--

INSERT INTO `brochures` (`brochure_id`, `admin_id`, `update_by`, `session_id`, `brochure_name`, `brochure_upload_date`, `brochure_file`, `brochure_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'test', '2018-12-31', 'dinesh625331546243408.jpg', 1, '2018-12-31 02:33:28', '2018-12-31 02:33:28');

-- --------------------------------------------------------

--
-- Table structure for table `caste`
--

CREATE TABLE `caste` (
  `caste_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `caste_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `caste`
--

INSERT INTO `caste` (`caste_id`, `admin_id`, `update_by`, `caste_name`, `caste_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Jain', 1, '2018-12-29 02:45:33', '2018-12-29 02:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `certificate_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `template_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=TC,1=CC,2=COMP',
  `template_for` tinyint(4) DEFAULT NULL COMMENT '0=Participation,1=Winner',
  `template_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_no_start` int(10) UNSIGNED DEFAULT NULL,
  `book_no_end` int(10) UNSIGNED DEFAULT NULL,
  `serial_no_start` int(10) UNSIGNED DEFAULT NULL,
  `serial_no_end` int(10) UNSIGNED DEFAULT NULL,
  `certificate_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`certificate_id`, `admin_id`, `update_by`, `template_name`, `template_type`, `template_for`, `template_link`, `book_no_start`, `book_no_end`, `serial_no_start`, `serial_no_end`, `certificate_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Participation Certificate', 2, 0, 'participation_template1', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(2, 1, 1, 'Winner Certificate', 2, 1, 'winner_template1', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(3, 1, 1, 'Character Certificate 1', 1, 0, 'character_template1', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(4, 1, 1, 'Character Certificate 2', 1, 0, 'character_template2', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(5, 1, 1, 'Transfer Certificate 1', 0, 0, 'transfer_template1', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(6, 1, 1, 'Transfer Certificate 2', 0, 0, 'transfer_template2', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22'),
(7, 1, 1, 'Transfer Certificate 3', 0, 0, 'transfer_template3', 0, 0, 0, 0, 0, '2018-12-29 00:13:22', '2018-12-29 00:13:22');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `state_id`, `city_name`, `city_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Jodhpur', 1, '2018-12-29 00:50:12', '2018-12-29 00:50:12'),
(2, 1, 'Jaipur', 1, '2018-12-29 00:50:39', '2018-12-29 00:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `board_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `class_stream` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `admin_id`, `update_by`, `class_name`, `class_order`, `board_id`, `medium_type`, `class_stream`, `class_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Class I', 0, 1, 1, 0, 1, '2018-12-29 02:38:44', '2018-12-29 02:38:44'),
(2, 1, 2, 'Class II', 0, 1, 1, 1, 1, '2018-12-29 02:39:05', '2019-01-01 04:36:52'),
(3, 1, 1, 'Second Class', 0, 1, 1, 0, 1, '2019-01-10 07:52:40', '2019-01-10 07:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject_staff_map`
--

CREATE TABLE `class_subject_staff_map` (
  `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_subject_staff_map`
--

INSERT INTO `class_subject_staff_map` (`class_subject_staff_map_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `subject_id`, `staff_ids`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 2, 2, 1, '2018-12-31 00:58:33', '2018-12-31 00:58:33'),
(2, 1, 1, 1, 1, 3, 1, 1, '2018-12-31 00:58:41', '2018-12-31 01:33:05'),
(3, 1, 1, 1, 1, 1, NULL, 1, '2018-12-31 01:32:50', '2018-12-31 01:32:58'),
(4, 1, 1, 1, 2, 3, 1, 1, '2018-12-31 00:58:41', '2018-12-31 01:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `competition_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `competition_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `competition_description` text COLLATE utf8mb4_unicode_ci,
  `competition_date` date DEFAULT NULL,
  `competition_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=School,1=Class',
  `competition_class_ids` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `competition_issue_participation_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `competition_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `competition_map`
--

CREATE TABLE `competition_map` (
  `student_competition_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `competition_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `position_rank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `issue_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `concession_map`
--

CREATE TABLE `concession_map` (
  `concession_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `reason_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `concession_amt` int(10) UNSIGNED DEFAULT NULL,
  `concession_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=Paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `concession_map`
--

INSERT INTO `concession_map` (`concession_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `head_id`, `reason_id`, `head_type`, `concession_amt`, `concession_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 2, 1, 1, 'ONETIME', 50, 0, '2019-01-04 05:17:39', '2019-01-04 05:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'India', 1, '2018-12-29 00:16:20', '2018-12-29 00:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `co_scholastic_types`
--

CREATE TABLE `co_scholastic_types` (
  `co_scholastic_type_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `co_scholastic_type_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `co_scholastic_type_description` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `co_scholastic_types`
--

INSERT INTO `co_scholastic_types` (`co_scholastic_type_id`, `admin_id`, `update_by`, `co_scholastic_type_name`, `co_scholastic_type_description`, `co_scholastic_type_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Discipline', 'Description here', 1, '2018-12-29 07:02:19', '2018-12-29 07:02:19'),
(2, 1, 1, 'Sports', 'Description here', 1, '2018-12-29 07:02:49', '2018-12-29 07:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_description` text COLLATE utf8mb4_unicode_ci,
  `teaching_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Non Teaching,1=Teaching',
  `designation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `admin_id`, `update_by`, `designation_name`, `designation_description`, `teaching_status`, `designation_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Class Teacher', NULL, 1, 1, '2018-12-29 02:43:08', '2018-12-29 02:43:08'),
(2, 1, 1, 'Subject Teacher', NULL, 0, 1, '2018-12-29 02:43:18', '2018-12-29 02:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `document_category`
--

CREATE TABLE `document_category` (
  `document_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `document_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=staff,2=both',
  `document_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_category`
--

INSERT INTO `document_category` (`document_category_id`, `admin_id`, `update_by`, `document_category_name`, `document_category_for`, `document_category_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Common Document', 2, 1, '2018-12-29 02:53:15', '2018-12-29 02:53:15'),
(2, 1, 1, 'Staff Document', 1, 1, '2018-12-29 02:53:29', '2018-12-29 02:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`exam_id`, `admin_id`, `update_by`, `term_exam_id`, `exam_name`, `medium_type`, `exam_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'test', 1, 1, '2018-12-31 02:08:02', '2018-12-31 02:08:02'),
(2, 1, 1, 2, 'Mid Term Exam', 1, 1, '2019-01-10 05:25:05', '2019-01-10 07:02:09'),
(3, 1, 1, 2, 'Half Yearly Exam', 1, 1, '2019-01-10 05:25:17', '2019-01-10 07:01:45'),
(4, 1, 1, 3, 'Quartly Exam', 1, 1, '2019-01-10 05:25:05', '2019-01-10 07:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `exam_map`
--

CREATE TABLE `exam_map` (
  `exam_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `marks_criteria_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_map`
--

INSERT INTO `exam_map` (`exam_map_id`, `admin_id`, `update_by`, `exam_id`, `session_id`, `class_id`, `section_id`, `subject_id`, `marks_criteria_id`, `grade_scheme_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 1, 1, 1, 2, 1, 3, '2018-12-31 02:16:45', '2019-01-05 01:16:53'),
(2, 1, 2, 1, 1, 1, 1, 3, 1, 1, '2018-12-31 02:16:45', '2019-01-05 01:16:53'),
(3, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2018-12-31 02:16:45', '2018-12-31 02:16:45'),
(4, 1, 1, 3, 1, 1, 1, 2, 1, 3, '2019-01-10 05:27:18', '2019-01-10 05:29:53'),
(5, 1, 1, 3, 1, 1, 1, 3, 1, 3, '2019-01-10 05:27:18', '2019-01-10 05:29:53'),
(6, 1, 1, 3, 1, 1, 1, 1, 1, 3, '2019-01-10 05:27:18', '2019-01-10 05:29:53'),
(7, 1, 1, 2, 1, 1, 1, 2, 1, 3, '2019-01-10 06:27:57', '2019-01-10 06:27:57'),
(8, 1, 1, 2, 1, 1, 1, 3, 1, 3, '2019-01-10 06:27:57', '2019-01-10 06:27:57'),
(9, 1, 1, 2, 1, 1, 1, 1, 1, 3, '2019-01-10 06:27:57', '2019-01-10 06:27:57'),
(10, 1, 1, 4, 1, 1, 1, 2, NULL, NULL, '2019-01-10 07:30:39', '2019-01-10 07:30:39'),
(11, 1, 1, 4, 1, 1, 1, 3, NULL, NULL, '2019-01-10 07:30:39', '2019-01-10 07:30:39'),
(12, 1, 1, 4, 1, 1, 1, 1, NULL, NULL, '2019-01-10 07:30:39', '2019-01-10 07:30:39'),
(13, 1, 1, 4, 1, 3, 5, 1, 1, 3, '2019-01-10 07:57:27', '2019-01-10 07:58:49'),
(14, 1, 1, 4, 1, 3, 5, 2, 1, 3, '2019-01-10 07:57:27', '2019-01-10 07:58:49'),
(15, 1, 1, 4, 1, 3, 5, 3, 1, 3, '2019-01-10 07:57:27', '2019-01-10 07:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `exam_marks`
--

CREATE TABLE `exam_marks` (
  `exam_mark_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_marks`
--

INSERT INTO `exam_marks` (`exam_mark_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `student_id`, `subject_id`, `marks`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 3, 1, 1, 4, 1, 50, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(2, 1, 1, 1, NULL, 3, 1, 1, 4, 2, 20, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(3, 1, 1, 1, NULL, 3, 1, 1, 4, 3, 65, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(4, 1, 1, 1, NULL, 3, 1, 1, 2, 1, 60, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(5, 1, 1, 1, NULL, 3, 1, 1, 2, 2, 45, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(6, 1, 1, 1, NULL, 3, 1, 1, 2, 3, 55, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(7, 1, 1, 1, NULL, 3, 1, 1, 1, 1, 45, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(8, 1, 1, 1, NULL, 3, 1, 1, 1, 2, 50, '2019-01-14 07:43:42', '2019-01-14 07:43:42'),
(9, 1, 1, 1, NULL, 3, 1, 1, 1, 3, 50, '2019-01-14 07:43:42', '2019-01-14 07:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `exam_schedules`
--

CREATE TABLE `exam_schedules` (
  `exam_schedule_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `schedule_name` text COLLATE utf8mb4_unicode_ci,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_schedules`
--

INSERT INTO `exam_schedules` (`exam_schedule_id`, `admin_id`, `update_by`, `schedule_name`, `session_id`, `exam_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'schedule 1', 1, 1, '2018-12-31 06:01:14', '2018-12-31 06:01:14'),
(2, 1, 1, 'test', 1, 3, '2019-01-10 05:34:27', '2019-01-10 05:34:27'),
(3, 1, 1, 'Quartly', 1, 4, '2019-01-10 07:59:28', '2019-01-10 07:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_fees_applied` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not apply,1=apply',
  `facility_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fees_st_map`
--

CREATE TABLE `fees_st_map` (
  `fees_st_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_ids` text COLLATE utf8mb4_unicode_ci COMMENT 'student ids are stored',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt`
--

CREATE TABLE `fee_receipt` (
  `receipt_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total receipt amount',
  `wallet_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Wallet amount',
  `total_concession_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Concession amount',
  `total_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Fine amount',
  `total_calculate_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Calculate Fine amount',
  `net_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total net amount',
  `pay_later_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Pay later amount',
  `imprest_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Imprest amount',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_date` date DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'If cheque mode',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If online mode',
  `transaction_date` date DEFAULT NULL COMMENT 'If online mode',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Cancel,1=Approve',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt_details`
--

CREATE TABLE `fee_receipt_details` (
  `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `receipt_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head_inst_id` int(10) UNSIGNED DEFAULT NULL,
  `inst_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total installment amount',
  `inst_paid_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total paid installment amount',
  `inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Less,1=complete',
  `r_concession_map_id` int(10) UNSIGNED DEFAULT NULL,
  `r_concession_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Concession amount',
  `r_fine_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_days` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Fine amount',
  `cancel_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fine`
--

CREATE TABLE `fine` (
  `fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=One Time Fees,1=Recurring Fees',
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fine_details`
--

CREATE TABLE `fine_details` (
  `fine_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_detail_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_detail_amt` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `grade_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_min` int(10) UNSIGNED DEFAULT NULL,
  `grade_max` int(10) UNSIGNED DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_point` double DEFAULT NULL,
  `grade_remark` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `admin_id`, `update_by`, `grade_scheme_id`, `grade_min`, `grade_max`, `grade_name`, `grade_point`, `grade_remark`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 1, 45, 'B', 100, 'AA', '2018-12-31 02:13:50', '2018-12-31 02:13:50'),
(2, 2, 2, 3, 45, 60, 'A', 10, 'remark', '2019-01-05 00:51:59', '2019-01-05 00:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `grade_schemes`
--

CREATE TABLE `grade_schemes` (
  `grade_scheme_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `scheme_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Percent,1=Marks',
  `scheme_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_schemes`
--

INSERT INTO `grade_schemes` (`grade_scheme_id`, `admin_id`, `update_by`, `scheme_name`, `grade_type`, `scheme_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'test', 1, 1, '2018-12-31 02:13:50', '2018-12-31 02:13:50'),
(3, 2, 2, 'Scheme 1', 1, 1, '2019-01-05 00:51:59', '2019-01-05 00:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holiday_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `holiday_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_for` tinyint(4) DEFAULT NULL COMMENT '0=Both,1=Students,2=Staff',
  `holiday_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`holiday_id`, `admin_id`, `update_by`, `session_id`, `holiday_name`, `holiday_start_date`, `holiday_end_date`, `holiday_for`, `holiday_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Test', '2019-01-04', '2019-01-04', 0, 1, '2019-01-03 00:51:14', '2019-01-03 00:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `homework_conversations`
--

CREATE TABLE `homework_conversations` (
  `h_conversation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `homework_group_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `h_conversation_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homework_conversations`
--

INSERT INTO `homework_conversations` (`h_conversation_id`, `admin_id`, `update_by`, `homework_group_id`, `staff_id`, `subject_id`, `h_conversation_text`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 1, 1, 3, 'test', '2019-01-01 04:21:36', '2019-01-01 04:21:36'),
(2, 4, 4, 1, 2, 2, 'test data', '2019-01-05 07:43:35', '2019-01-05 07:43:35'),
(3, 4, 4, 2, 2, 2, 'test data', '2019-01-05 07:43:35', '2019-01-05 07:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `homework_groups`
--

CREATE TABLE `homework_groups` (
  `homework_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `homework_group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homework_groups`
--

INSERT INTO `homework_groups` (`homework_group_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `homework_group_name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 'Class I - A Group', '2018-12-29 06:49:02', '2018-12-29 06:49:02'),
(2, 1, 1, 1, 2, 'Class I - B Group', '2018-12-29 06:49:18', '2018-12-29 06:49:18'),
(3, 1, 1, 2, 3, 'Class II - A Group', '2018-12-29 06:49:31', '2018-12-29 06:49:31'),
(4, 1, 1, 2, 4, 'Class II - B Group', '2018-12-29 06:49:51', '2018-12-29 06:49:51'),
(5, 1, 1, 3, 5, 'Second Class - A Group', '2019-01-10 07:53:41', '2019-01-10 07:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `hostels`
--

CREATE TABLE `hostels` (
  `hostel_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostel_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boys,1=Girls',
  `hostel_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostels`
--

INSERT INTO `hostels` (`hostel_id`, `admin_id`, `update_by`, `hostel_name`, `hostel_type`, `hostel_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'H2', 0, 1, '2018-12-31 01:56:07', '2018-12-31 01:56:20');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_blocks`
--

CREATE TABLE `hostel_blocks` (
  `h_block_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_block_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_blocks`
--

INSERT INTO `hostel_blocks` (`h_block_id`, `admin_id`, `update_by`, `hostel_id`, `h_block_name`, `h_block_status`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, 'B2', 1, '2018-12-31 01:59:23', '2018-12-31 01:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_floors`
--

CREATE TABLE `hostel_floors` (
  `h_floor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_floor_rooms` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_floors`
--

INSERT INTO `hostel_floors` (`h_floor_id`, `admin_id`, `update_by`, `h_block_id`, `h_floor_no`, `h_floor_rooms`, `h_floor_description`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 2, '1', 1, 'test', '2018-12-31 01:59:23', '2018-12-31 01:59:23'),
(3, 1, 1, 2, '2', 2, 'test', '2018-12-31 01:59:23', '2018-12-31 01:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_rooms`
--

CREATE TABLE `hostel_rooms` (
  `h_room_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_room_alias` text COLLATE utf8mb4_unicode_ci,
  `h_room_capacity` int(10) UNSIGNED DEFAULT NULL,
  `h_room_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hostel_room_cate`
--

CREATE TABLE `hostel_room_cate` (
  `h_room_cate_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_room_cate_fees` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_des` text COLLATE utf8mb4_unicode_ci,
  `h_room_cate_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hostel_student_map`
--

CREATE TABLE `hostel_student_map` (
  `h_student_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_id` int(10) UNSIGNED DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `fees_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=paid',
  `leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Stay,1=leave',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac`
--

CREATE TABLE `imprest_ac` (
  `imprest_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imprest_ac_amt` double DEFAULT NULL,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac`
--

INSERT INTO `imprest_ac` (`imprest_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `imprest_ac_name`, `imprest_ac_amt`, `imprest_ac_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 'Teena', 0, 1, '2019-01-03 02:15:18', '2019-01-03 02:15:18'),
(2, 2, 2, 1, 1, 2, 'Reena', 0, 1, '2019-01-03 02:22:01', '2019-01-03 02:22:01'),
(3, 2, 2, 1, 1, 3, 'Neha', 0, 1, '2019-01-04 23:53:32', '2019-01-04 23:53:32'),
(4, 2, 2, 1, 1, 4, 'Ravina', 0, 1, '2019-01-07 04:49:06', '2019-01-07 04:49:06');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac_confi`
--

CREATE TABLE `imprest_ac_confi` (
  `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_confi_amt` double UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac_confi`
--

INSERT INTO `imprest_ac_confi` (`imprest_ac_confi_id`, `admin_id`, `update_by`, `imprest_ac_confi_amt`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, '2018-12-29 00:04:05', '2018-12-29 00:04:05');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac_entries`
--

CREATE TABLE `imprest_ac_entries` (
  `ac_entry_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ac_entry_date` date DEFAULT NULL,
  `ac_entry_amt` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `issued_books`
--

CREATE TABLE `issued_books` (
  `issued_book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_id` int(10) UNSIGNED DEFAULT NULL,
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `issued_book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=issued,2=returned',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issued_books`
--

INSERT INTO `issued_books` (`issued_book_id`, `admin_id`, `update_by`, `book_id`, `member_id`, `member_type`, `book_copies_id`, `issued_from_date`, `issued_to_date`, `issued_book_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, '2018-01-26', '2018-01-27', 2, '2019-01-05 05:24:13', '2019-01-05 05:30:58');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books_history`
--

CREATE TABLE `issued_books_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_id` int(10) UNSIGNED DEFAULT NULL,
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issued_books_history`
--

INSERT INTO `issued_books_history` (`history_id`, `admin_id`, `update_by`, `book_id`, `member_id`, `member_type`, `book_copies_id`, `issued_from_date`, `issued_to_date`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, '2018-01-26', '2018-01-28', '2019-01-05 05:30:44', '2019-01-05 05:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `library_fine`
--

CREATE TABLE `library_fine` (
  `library_fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `fine_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `fine_date` date DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=due,2=paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `library_members`
--

CREATE TABLE `library_members` (
  `library_member_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `library_member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `member_member_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_members`
--

INSERT INTO `library_members` (`library_member_id`, `admin_id`, `update_by`, `member_id`, `library_member_type`, `member_member_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, 1, 1, '2019-01-05 05:10:57', '2019-01-05 05:10:57'),
(2, 2, 2, 1, 2, 1, '2019-01-05 05:16:15', '2019-01-05 05:16:15'),
(3, 2, 2, 2, 2, 1, '2019-01-05 05:16:15', '2019-01-05 05:16:15'),
(4, 2, 2, 3, 2, 1, '2019-01-05 05:16:15', '2019-01-05 05:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `marks_criteria`
--

CREATE TABLE `marks_criteria` (
  `marks_criteria_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `criteria_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_marks` double UNSIGNED DEFAULT NULL,
  `passing_marks` double UNSIGNED DEFAULT NULL,
  `criteria_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marks_criteria`
--

INSERT INTO `marks_criteria` (`marks_criteria_id`, `admin_id`, `update_by`, `criteria_name`, `max_marks`, `passing_marks`, `criteria_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'test', 100, 24, 1, '2019-01-05 00:49:40', '2019-01-05 00:49:40');

-- --------------------------------------------------------

--
-- Table structure for table `medium_type`
--

CREATE TABLE `medium_type` (
  `medium_type_id` int(10) UNSIGNED NOT NULL,
  `medium_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medium_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medium_type`
--

INSERT INTO `medium_type` (`medium_type_id`, `medium_type`, `medium_type_status`, `created_at`, `updated_at`) VALUES
(1, 'English', 1, '2018-12-29 00:15:50', '2018-12-29 00:15:50'),
(2, 'Hindi', 1, '2018-12-29 00:16:07', '2018-12-29 00:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(122, '2018_09_04_081951_create_admins_table', 1),
(123, '2018_09_04_082724_create_country_table', 1),
(124, '2018_09_04_082725_create_medium_type_table', 1),
(125, '2018_09_04_085450_create_state_table', 1),
(126, '2018_09_04_085724_create_city_table', 1),
(127, '2018_09_04_090000_create_school_boards_table', 1),
(128, '2018_09_04_090037_create_school_table', 1),
(129, '2018_09_04_090038_create_imprest_ac_confi_table', 1),
(130, '2018_09_04_091709_create_sessions_table', 1),
(131, '2018_09_04_092243_create_holidays_table', 1),
(132, '2018_09_04_092628_create_shifts_table', 1),
(133, '2018_09_04_092824_create_facilities_table', 1),
(134, '2018_09_04_093153_create_document_category_table', 1),
(135, '2018_09_04_093346_create_designation_table', 1),
(136, '2018_09_04_093413_create_titles_table', 1),
(137, '2018_09_04_093453_create_caste_table', 1),
(138, '2018_09_04_093537_create_religions_table', 1),
(139, '2018_09_04_093957_create_nationality_table', 1),
(140, '2018_09_04_094116_create_groups_table', 1),
(141, '2018_09_04_094117_create_streams_table', 1),
(142, '2018_09_04_094221_create_classes_table', 1),
(143, '2018_09_04_094401_create_sections_table', 1),
(144, '2018_09_04_094530_create_co_scholastic_types_table', 1),
(145, '2018_09_04_100509_create_subjects_table', 1),
(146, '2018_09_04_100807_create_virtual_classes_table', 1),
(147, '2018_09_04_100943_create_competitions_table', 1),
(148, '2018_09_04_101149_create_room_no_table', 1),
(149, '2018_09_04_101335_create_student_parents_table', 1),
(150, '2018_09_06_065837_create_term_exams_table', 1),
(151, '2018_09_06_065937_create_exams_table', 1),
(152, '2018_09_06_083000_create_student_table', 1),
(153, '2018_09_06_100023_create_student_academic_info_table', 1),
(154, '2018_09_06_100515_create_student_academic_history_info_table', 1),
(155, '2018_09_06_101111_create_student_health_info_table', 1),
(156, '2018_09_06_101112_create_imprest_ac_table', 1),
(157, '2018_09_06_101113_create_wallet_ac_table', 1),
(158, '2018_09_06_101240_create_virtual_class_map_table', 1),
(159, '2018_09_06_101310_create_competition_map_table', 1),
(160, '2018_09_06_104053_create_online_notes_table', 1),
(161, '2018_09_06_104234_create_staff_roles_table', 1),
(162, '2018_09_06_104354_create_staff_table', 1),
(163, '2018_09_06_132553_create_time_tables_table', 1),
(164, '2018_09_07_043215_create_question_papers_table', 1),
(165, '2018_09_07_043425_create_staff_documents_table', 1),
(166, '2018_09_07_044319_create_student_documents_table', 1),
(167, '2018_09_08_082046_create_staff_class_allocations_table', 1),
(168, '2018_09_08_082829_create_subject_class_map_table', 1),
(169, '2018_09_08_083054_create_class_subject_staff_map_table', 1),
(170, '2018_09_08_083343_create_staff_leaves_table', 1),
(171, '2018_09_08_091546_create_student_leaves_table', 1),
(172, '2018_09_10_061556_create_brochures_table', 1),
(173, '2018_09_30_094828_create_class_subject_staff_maping_table', 1),
(174, '2018_10_01_043537_create_one_time_heads_table', 1),
(175, '2018_10_01_044251_create_recurring_heads_table', 1),
(176, '2018_10_01_044530_create_recurring_inst_table', 1),
(177, '2018_10_02_005202_create_book_category_table', 1),
(178, '2018_10_02_005800_create_book_allowance_table', 1),
(179, '2018_10_02_005938_create_book_vendor_table', 1),
(180, '2018_10_02_010120_create_book_cupboard_table', 1),
(181, '2018_10_02_010258_create_book_cupboardshelf_table', 1),
(182, '2018_10_02_044518_create_banks_table', 1),
(183, '2018_10_02_044836_create_student_cheques_table', 1),
(184, '2018_10_02_045309_create_rte_cheques_table', 1),
(185, '2018_10_02_165720_create_books_table', 1),
(186, '2018_10_02_170222_create_book_copies_info_table', 1),
(187, '2018_10_02_170530_create_library_members_table', 1),
(188, '2018_10_02_170949_create_issued_books_table', 1),
(189, '2018_10_03_045935_create_rte_heads_table', 1),
(190, '2018_10_03_140228_create_visitors_table', 1),
(191, '2018_10_06_075741_create_grade_schemes_table', 1),
(192, '2018_10_06_080011_create_grades_table', 1),
(193, '2018_10_08_072229_create_fine_table', 1),
(194, '2018_10_08_072737_create_fine_details_table', 1),
(195, '2018_10_09_055058_create_rte_head_map_table', 1),
(196, '2018_10_09_125120_create_marks_criteria_table', 1),
(197, '2018_10_09_141430_create_subject_section_map_table', 1),
(198, '2018_10_11_123244_create_api_tokens_table', 1),
(199, '2018_10_12_091353_create_reasons_table', 1),
(200, '2018_10_12_091354_create_concession_map_table', 1),
(201, '2018_10_13_054337_create_fees_st_map_table', 1),
(202, '2018_10_18_073910_create_fee_receipt_table', 1),
(203, '2018_10_18_074452_create_fee_receipt_details_table', 1),
(204, '2018_10_21_160700_create_homework_groups_table', 1),
(205, '2018_10_22_063731_create_exam_map_table', 1),
(206, '2018_10_23_111947_create_staff_attendance_table', 1),
(207, '2018_10_23_112802_create_student_attendance_table', 1),
(208, '2018_10_27_095406_create_student_subjects_table', 1),
(209, '2018_10_29_120146_create_admission_forms_table', 1),
(210, '2018_10_29_120354_create_admission_fields_table', 1),
(211, '2018_10_29_120645_create_admission_data_table', 1),
(212, '2018_10_29_170551_create_issued_books_history_table', 1),
(213, '2018_10_29_171144_create_library_fine_table', 1),
(214, '2018_10_30_131719_create_st_parent_groups_table', 1),
(215, '2018_10_30_131914_create_st_parent_group_map_table', 1),
(216, '2018_10_31_093217_create_remarks_table', 1),
(217, '2018_11_12_093152_create_hostels_table', 1),
(218, '2018_11_13_092000_create_hostel_blocks_table', 1),
(219, '2018_11_13_092231_create_hostel_floors_table', 1),
(220, '2018_11_13_092838_create_hostel_room_cate_table', 1),
(221, '2018_11_13_093306_create_hostel_rooms_table', 1),
(222, '2018_11_16_055339_create_homework_conversations_table', 1),
(223, '2018_11_20_074124_create_hostel_student_map_table', 1),
(224, '2018_11_22_075313_create_imprest_ac_entries_table', 1),
(225, '2018_11_22_104714_create_ac_entry_map_table', 1),
(226, '2018_11_22_113151_create_notice_table', 1),
(227, '2018_11_27_070619_create_time_table_map_table', 1),
(228, '2018_11_28_071442_add_notification_status_to_admins', 1),
(229, '2018_12_05_072332_create_certificates_table', 1),
(230, '2018_12_06_064816_create_exam_schedules_table', 1),
(231, '2018_12_06_065948_create_schedule_map_table', 1),
(232, '2018_12_06_070715_create_schedule_room_map_table', 1),
(233, '2018_12_06_071222_create_exam_marks_table', 1),
(234, '2018_12_13_052715_add_fields_to_admins_and_staff', 1),
(235, '2018_12_13_062324_create_app_settings_table', 1),
(236, '2018_12_24_103214_create_api_tokens_table', 1),
(237, '2019_01_05_112116_create_staff_attend_details_table', 2),
(238, '2019_01_05_112804_create_student_attend_details_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `nationality_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`nationality_id`, `admin_id`, `update_by`, `nationality_name`, `nationality_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Indian', 1, '2018-12-29 02:45:10', '2018-12-29 02:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `notice_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_text` text COLLATE utf8mb4_unicode_ci,
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `staff_ids` text COLLATE utf8mb4_unicode_ci,
  `for_class` tinyint(4) DEFAULT NULL COMMENT '0=All,1=Selected',
  `for_staff` tinyint(4) DEFAULT NULL COMMENT '0=All,1=Selected',
  `notice_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `admin_id`, `update_by`, `session_id`, `notice_name`, `notice_text`, `class_ids`, `staff_ids`, `for_class`, `for_staff`, `notice_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'What is Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, NULL, 0, NULL, 1, '2019-01-18 00:23:33', '2019-01-18 00:23:33'),
(2, 1, 1, 1, 'lLorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', NULL, 1, NULL, 1, '2019-01-18 00:37:15', '2019-01-18 00:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `one_time_heads`
--

CREATE TABLE `one_time_heads` (
  `ot_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `ot_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ot_alias` text COLLATE utf8mb4_unicode_ci,
  `ot_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `ot_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `ot_date` date DEFAULT NULL,
  `ot_refundable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `for_students` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=New,1=Old,2=Both',
  `deduct_imprest_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `receipt_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `ot_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `one_time_heads`
--

INSERT INTO `one_time_heads` (`ot_head_id`, `admin_id`, `update_by`, `ot_particular_name`, `ot_alias`, `ot_classes`, `ot_amount`, `ot_date`, `ot_refundable`, `for_students`, `deduct_imprest_status`, `receipt_status`, `ot_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Admission', 'Admission', '1,2', 1000.00, '2019-01-31', 1, 0, 0, 0, 1, '2019-01-04 05:16:49', '2019-01-04 05:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `online_notes`
--

CREATE TABLE `online_notes` (
  `online_note_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `online_note_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_note_topic` text COLLATE utf8mb4_unicode_ci,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_url` text COLLATE utf8mb4_unicode_ci,
  `online_note_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_notes`
--

INSERT INTO `online_notes` (`online_note_id`, `admin_id`, `update_by`, `online_note_name`, `class_id`, `section_id`, `subject_id`, `online_note_unit`, `online_note_topic`, `staff_id`, `online_note_url`, `online_note_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'What is Lorem Ipsum', 1, 1, 2, 'test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, NULL, 1, '2019-01-18 00:52:14', '2019-01-18 00:52:14'),
(2, 1, 1, 'Why do we use it', 1, 2, 1, '5', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1, 'SSN Rectangular.607691547792584xlsx', 1, '2019-01-18 00:53:04', '2019-01-18 01:28:32'),
(3, 1, 1, 'Where does it come from', 1, 2, 1, '5', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 1, NULL, 1, '2019-01-18 00:53:45', '2019-01-18 00:53:45'),
(4, 5, 5, 'sdfs', 3, 2, 2, '435', 'sdagg', 5, NULL, 1, '2019-01-22 02:50:56', '2019-01-22 02:50:56');

-- --------------------------------------------------------

--
-- Table structure for table `question_papers`
--

CREATE TABLE `question_papers` (
  `question_paper_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_file` text COLLATE utf8mb4_unicode_ci,
  `question_paper_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `reason_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reason_text` text COLLATE utf8mb4_unicode_ci,
  `reason_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`reason_id`, `admin_id`, `update_by`, `reason_text`, `reason_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'By management', 1, '2019-01-04 05:17:18', '2019-01-04 05:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `recurring_heads`
--

CREATE TABLE `recurring_heads` (
  `rc_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_alias` text COLLATE utf8mb4_unicode_ci,
  `rc_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `rc_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recurring_inst`
--

CREATE TABLE `recurring_inst` (
  `rc_inst_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_head_id` int(10) UNSIGNED DEFAULT NULL,
  `rc_inst_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_inst_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_inst_date` date DEFAULT NULL,
  `rc_inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `religion_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `religion_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`religion_id`, `admin_id`, `update_by`, `religion_name`, `religion_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Hindu', 1, '2018-12-29 02:45:22', '2018-12-29 02:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `remarks`
--

CREATE TABLE `remarks` (
  `remark_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `remark_text` text COLLATE utf8mb4_unicode_ci,
  `remark_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `remarks`
--

INSERT INTO `remarks` (`remark_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `staff_id`, `subject_id`, `remark_text`, `remark_date`, `created_at`, `updated_at`) VALUES
(1, 4, 4, 1, 1, 2, 3, 2, 1, 'Test', '2019-01-05', '2019-01-05 07:39:23', '2019-01-05 07:39:23'),
(2, 4, 4, 1, 1, 2, 3, 2, 2, 'Testing Testing Testing Testing Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing TestingTesting Testing Testing', '2019-01-05', '2019-01-05 07:40:07', '2019-01-05 07:40:07'),
(3, 4, 4, 1, 1, 1, 4, 2, 2, 'testing 1', '2019-01-05', '2019-01-05 07:41:26', '2019-01-05 07:41:26'),
(4, 4, 4, 2, 1, 1, 4, 2, 2, 'testing 1', '2019-01-05', '2019-01-05 07:41:26', '2019-01-05 07:41:26'),
(5, 4, 4, 1, 1, 1, 2, 2, 2, 'testing 1', '2019-01-05', '2019-01-05 07:41:26', '2019-01-05 07:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `room_no`
--

CREATE TABLE `room_no` (
  `room_no_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_no_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_no`
--

INSERT INTO `room_no` (`room_no_id`, `admin_id`, `update_by`, `room_no`, `room_no_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', 1, '2019-01-10 05:51:47', '2019-01-10 05:51:47');

-- --------------------------------------------------------

--
-- Table structure for table `rte_cheques`
--

CREATE TABLE `rte_cheques` (
  `rte_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `rte_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_date` date DEFAULT NULL,
  `rte_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rte_heads`
--

CREATE TABLE `rte_heads` (
  `rte_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rte_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rte_head_map`
--

CREATE TABLE `rte_head_map` (
  `rte_head_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_map`
--

CREATE TABLE `schedule_map` (
  `schedule_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` text COLLATE utf8mb4_unicode_ci,
  `exam_date` date DEFAULT NULL,
  `exam_time_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exam_time_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_map`
--

INSERT INTO `schedule_map` (`schedule_map_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `subject_id`, `staff_ids`, `exam_date`, `exam_time_from`, `exam_time_to`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 3, 1, 1, 2, '2', '2019-01-18', '09:00', '12:00', '2019-01-10 05:40:38', '2019-01-10 05:40:38'),
(2, 1, 1, 1, 2, 3, 1, 1, 3, '2,1', '2019-01-19', '06:00', '12:00', '2019-01-10 05:40:38', '2019-01-10 05:40:38'),
(3, 1, 1, 1, 2, 3, 1, 1, 1, '2,1,3', '2019-01-20', '09:00', '12:00', '2019-01-10 05:40:38', '2019-01-10 05:40:38'),
(10, 1, 1, 1, 1, 1, 1, 1, 2, ',2', '2019-01-12', '09:00', '12:00', '2019-01-10 06:31:10', '2019-01-10 06:31:10'),
(11, 1, 1, 1, 1, 1, 1, 1, 3, '1', '2019-01-11', '09:00', '12:00', '2019-01-10 06:31:10', '2019-01-10 06:31:10'),
(12, 1, 1, 1, 1, 1, 1, 1, 1, '1', '2019-01-24', '09:00', '12:00', '2019-01-10 06:31:10', '2019-01-10 06:31:10'),
(13, 1, 1, 1, 3, 4, 3, 5, 1, '2', '2019-01-25', '10:00', '13:00', '2019-01-10 08:00:51', '2019-01-10 08:00:51'),
(14, 1, 1, 1, 3, 4, 3, 5, 2, '3', '2019-01-26', '10:00', '13:00', '2019-01-10 08:00:51', '2019-01-10 08:00:51'),
(15, 1, 1, 1, 3, 4, 3, 5, 3, '2', '2019-01-24', '10:00', '13:00', '2019-01-10 08:00:51', '2019-01-10 08:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_room_map`
--

CREATE TABLE `schedule_room_map` (
  `schedule_room_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `room_no_id` int(10) UNSIGNED DEFAULT NULL,
  `roll_no_from` text COLLATE utf8mb4_unicode_ci,
  `roll_no_to` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_room_map`
--

INSERT INTO `schedule_room_map` (`schedule_room_map_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `room_no_id`, `roll_no_from`, `roll_no_to`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 3, 1, 1, 1, '1', '100', '2019-01-10 05:59:07', '2019-01-10 05:59:07'),
(2, 1, 1, 1, 1, 1, 1, 1, 1, '1', '100', '2019-01-10 06:32:14', '2019-01-10 06:32:14'),
(3, 1, 1, 1, 3, 4, 3, 5, 1, '1', '20', '2019-01-10 08:01:27', '2019-01-10 08:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_sno_numbers` text COLLATE utf8mb4_unicode_ci COMMENT 'SNO numbers are stored with comma',
  `school_description` text COLLATE utf8mb4_unicode_ci,
  `school_address` text COLLATE utf8mb4_unicode_ci,
  `school_district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `state_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `school_pincode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_medium` text COLLATE utf8mb4_unicode_ci COMMENT 'Medium names are stored with comma',
  `school_board_of_exams` text COLLATE utf8mb4_unicode_ci COMMENT 'Board names are stored with comma',
  `school_class_from` text COLLATE utf8mb4_unicode_ci,
  `school_class_to` text COLLATE utf8mb4_unicode_ci,
  `school_total_students` int(10) UNSIGNED DEFAULT NULL,
  `school_total_staff` int(10) UNSIGNED DEFAULT NULL,
  `school_fee_range_from` double(18,2) UNSIGNED DEFAULT NULL,
  `school_fee_range_to` double(18,2) UNSIGNED DEFAULT NULL,
  `school_facilities` text COLLATE utf8mb4_unicode_ci COMMENT 'Facilities are stored with comma',
  `school_url` text COLLATE utf8mb4_unicode_ci,
  `school_logo` text COLLATE utf8mb4_unicode_ci,
  `school_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_img1` text COLLATE utf8mb4_unicode_ci,
  `school_img2` text COLLATE utf8mb4_unicode_ci,
  `school_img3` text COLLATE utf8mb4_unicode_ci,
  `school_img4` text COLLATE utf8mb4_unicode_ci,
  `school_img5` text COLLATE utf8mb4_unicode_ci,
  `school_img6` text COLLATE utf8mb4_unicode_ci,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `school_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `admin_id`, `update_by`, `school_name`, `school_registration_no`, `school_sno_numbers`, `school_description`, `school_address`, `school_district`, `city_id`, `state_id`, `country_id`, `school_pincode`, `school_medium`, `school_board_of_exams`, `school_class_from`, `school_class_to`, `school_total_students`, `school_total_staff`, `school_fee_range_from`, `school_fee_range_to`, `school_facilities`, `school_url`, `school_logo`, `school_email`, `school_mobile_number`, `school_telephone`, `school_fax_number`, `school_img1`, `school_img2`, `school_img3`, `school_img4`, `school_img5`, `school_img6`, `imprest_ac_status`, `school_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Academic Eye', '123', NULL, NULL, 'Address', NULL, 1, 1, 1, '342001', '1,2', '1,2', 'Nursery', 'Class 12', 0, 0, 1.00, 1000000.00, NULL, NULL, 'logo385561546064539.png', 'school@gmail.com', '8888888888', NULL, NULL, 'logo723611546065070.png', 'logo883681546065070.png', NULL, NULL, NULL, NULL, 0, 1, '2018-12-29 00:04:02', '2018-12-29 01:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `school_boards`
--

CREATE TABLE `school_boards` (
  `board_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `board_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `board_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school_boards`
--

INSERT INTO `school_boards` (`board_id`, `admin_id`, `update_by`, `board_name`, `board_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'CBSE Board', 1, '2018-12-29 00:03:56', '2018-12-29 00:03:56'),
(2, 1, 1, 'RBSE Board', 1, '2018-12-29 00:03:56', '2018-12-29 00:03:56'),
(3, 1, 1, 'IB Board', 1, '2018-12-29 00:03:56', '2018-12-29 00:03:56'),
(4, 1, 1, 'IGCSE Board', 1, '2018-12-29 00:03:56', '2018-12-29 00:03:56'),
(5, 1, 1, 'State Board', 1, '2018-12-29 00:03:56', '2018-12-29 00:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `shift_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_intake` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'capacity/seats',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `section_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `admin_id`, `update_by`, `class_id`, `shift_id`, `stream_id`, `section_name`, `section_intake`, `medium_type`, `section_order`, `section_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, 'A', 10, 1, 0, 1, '2018-12-29 06:49:01', '2018-12-29 06:49:01'),
(2, 1, 1, 1, 1, NULL, 'B', 10, 1, 0, 1, '2018-12-29 06:49:18', '2018-12-29 06:49:18'),
(3, 1, 1, 2, 1, NULL, 'A', 20, 1, 0, 1, '2018-12-29 06:49:31', '2018-12-29 06:49:31'),
(4, 1, 1, 2, 1, NULL, 'B', 20, 1, 0, 1, '2018-12-29 06:49:51', '2018-12-29 06:49:51'),
(5, 1, 1, 3, 1, NULL, 'A', 30, 1, 0, 1, '2019-01-10 07:53:41', '2019-01-10 07:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_start_date` date DEFAULT NULL,
  `session_end_date` date DEFAULT NULL,
  `session_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `admin_id`, `update_by`, `session_name`, `session_start_date`, `session_end_date`, `session_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-2019', '2018-04-01', '2019-03-30', 1, '2018-12-29 01:02:10', '2018-12-29 01:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `shift_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `shift_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift_start_time` time DEFAULT NULL,
  `shift_end_time` time DEFAULT NULL,
  `shift_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`shift_id`, `admin_id`, `update_by`, `shift_name`, `shift_start_time`, `shift_end_time`, `shift_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1st Shift', '07:00:00', '13:00:00', 1, '2018-12-29 06:48:50', '2018-12-29 06:48:50');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_profile_img` text COLLATE utf8mb4_unicode_ci,
  `staff_aadhar` text COLLATE utf8mb4_unicode_ci,
  `staff_UAN` text COLLATE utf8mb4_unicode_ci,
  `staff_ESIN` text COLLATE utf8mb4_unicode_ci,
  `staff_PAN` text COLLATE utf8mb4_unicode_ci,
  `designation_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_id` text COLLATE utf8mb4_unicode_ci COMMENT 'Multiple staff role ids are store',
  `staff_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_dob` date DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `staff_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Male,1=Female',
  `staff_father_name_husband_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_father_husband_mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_marital_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unmarried,1=Married',
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_qualification` text COLLATE utf8mb4_unicode_ci COMMENT 'Qualifications are store with comma',
  `staff_specialization` text COLLATE utf8mb4_unicode_ci COMMENT 'Specializations are store with comma',
  `staff_reference` text COLLATE utf8mb4_unicode_ci,
  `staff_experience` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `staff_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shift_ids` text COLLATE utf8mb4_unicode_ci,
  `teaching_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `admin_id`, `update_by`, `reference_admin_id`, `staff_name`, `staff_profile_img`, `staff_aadhar`, `staff_UAN`, `staff_ESIN`, `staff_PAN`, `designation_id`, `staff_role_id`, `staff_email`, `staff_mobile_number`, `staff_dob`, `medium_type`, `staff_gender`, `staff_father_name_husband_name`, `staff_father_husband_mobile_no`, `staff_mother_name`, `staff_blood_group`, `staff_marital_status`, `title_id`, `caste_id`, `nationality_id`, `religion_id`, `staff_attendance_unique_id`, `staff_qualification`, `staff_specialization`, `staff_reference`, `staff_experience`, `staff_temporary_address`, `staff_temporary_city`, `staff_temporary_state`, `staff_temporary_county`, `staff_temporary_pincode`, `staff_permanent_address`, `staff_permanent_city`, `staff_permanent_state`, `staff_permanent_county`, `staff_permanent_pincode`, `staff_status`, `created_at`, `updated_at`, `shift_ids`, `teaching_status`) VALUES
(1, 1, 1, 3, 'Dipesh', NULL, '123456', NULL, NULL, NULL, 1, '2,3', NULL, '9999999999', '2017-12-28', 1, 0, 'Dipesh F', '8888888888', 'Dipesh M', NULL, 1, 1, 1, 1, 1, 'Staff_1', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '[\"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\",\"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\",\"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\"]', NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342006', 1, '2018-12-29 02:48:50', '2018-12-29 06:59:55', NULL, 1),
(2, 1, 1, 4, 'Dinesh', 'dinesh529801546077389.jpg', '123', NULL, NULL, NULL, 1, '2,3', NULL, '8888888888', '2000-01-25', 1, 0, 'F', '8888888888', 'M', NULL, 0, 1, 1, 1, 1, 'Staff_2', 'test', 'test', 'test', '[\"1\",\"2\",\"3\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-12-29 04:26:29', '2018-12-29 04:26:29', NULL, 1),
(3, 2, 2, 8, 'Vivek', 'ravi344331546662321.jpg', '123456', '123456', '123456', '123456', 1, '2', 'vivek@gmail.com', '6666666666', '2018-01-25', 1, 0, 'F name', '6666666666', 'M name', 'A', 0, 1, 1, 1, 1, 'Staff_3', 'Qualification', 'Specialization', 'Reference', '[\"Experience 1\",\"Experience 2\"]', 'Jodhpur', 1, 1, 1, '342006', 'Jodhpur', 1, 1, 1, '342206', 1, '2019-01-04 22:55:21', '2019-01-04 22:55:21', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff_attendance`
--

CREATE TABLE `staff_attendance` (
  `staff_attendance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `total_present_staff` int(10) UNSIGNED DEFAULT NULL,
  `total_absent_staff` int(10) UNSIGNED DEFAULT NULL,
  `total_halfday_staff` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_date` date DEFAULT NULL,
  `staff_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_attendance`
--

INSERT INTO `staff_attendance` (`staff_attendance_id`, `admin_id`, `update_by`, `session_id`, `total_present_staff`, `total_absent_staff`, `total_halfday_staff`, `staff_attendance_date`, `staff_attendance_status`, `created_at`, `updated_at`) VALUES
(8, 2, 2, 1, 2, 0, 0, '2019-01-01', 1, '2019-01-02 03:50:19', '2019-01-02 23:43:49'),
(10, 2, 2, 1, 1, 1, 0, '2019-01-02', 1, '2019-01-03 00:20:52', '2019-01-04 23:45:47'),
(12, 2, 2, 1, 2, 0, 0, '2019-01-03', 1, '2019-01-04 07:12:41', '2019-01-04 07:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `staff_attend_details`
--

CREATE TABLE `staff_attend_details` (
  `staff_attend_d_id` int(10) UNSIGNED NOT NULL,
  `staff_attendance_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `staff_attendance_type` tinyint(4) DEFAULT NULL COMMENT '0=Absent,1=Present,2=Half Day',
  `staff_overtime_type` tinyint(2) NOT NULL COMMENT '0=Hours,1=Lectures',
  `staff_overtime_value` double(18,2) DEFAULT NULL,
  `staff_attendance_date` date DEFAULT NULL,
  `staff_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_attend_details`
--

INSERT INTO `staff_attend_details` (`staff_attend_d_id`, `staff_attendance_id`, `admin_id`, `update_by`, `session_id`, `staff_id`, `in_time`, `out_time`, `staff_attendance_type`, `staff_overtime_type`, `staff_overtime_value`, `staff_attendance_date`, `staff_attendance_status`, `created_at`, `updated_at`) VALUES
(1, 8, 2, 2, 1, 2, '10:00:00', '11:00:00', 1, 2, NULL, '2019-01-01', 1, '2019-01-02 03:57:32', '2019-01-02 23:43:49'),
(2, 8, 2, 2, 1, 1, '12:00:00', '17:00:00', 1, 2, NULL, '2019-01-01', 1, '2019-01-02 03:57:32', '2019-01-02 23:43:49'),
(5, 10, 2, 2, 1, 1, NULL, NULL, 0, 2, NULL, '2019-01-02', 1, '2019-01-02 23:56:59', '2019-01-03 00:20:52'),
(6, 10, 2, 2, 1, 2, '10:00:00', '14:00:00', 1, 2, NULL, '2019-01-02', 1, '2019-01-02 23:57:00', '2019-01-04 23:45:47'),
(9, 12, 2, 2, 1, 1, '10:00:00', '23:00:00', 1, 1, 1.00, '2019-01-03', 1, '2019-01-04 07:12:41', '2019-01-04 07:12:41'),
(10, 12, 2, 2, 1, 2, '10:00:00', '12:00:00', 1, 0, 1.00, '2019-01-03', 1, '2019-01-04 07:12:41', '2019-01-04 07:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `staff_class_allocations`
--

CREATE TABLE `staff_class_allocations` (
  `staff_class_allocation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_class_allocations`
--

INSERT INTO `staff_class_allocations` (`staff_class_allocation_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `staff_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 2, 1, '2018-12-29 06:50:20', '2018-12-29 06:50:20'),
(2, 1, 1, 1, 2, 1, 1, '2018-12-29 07:00:13', '2018-12-29 07:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `staff_documents`
--

CREATE TABLE `staff_documents` (
  `staff_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_document_details` text COLLATE utf8mb4_unicode_ci,
  `staff_document_file` text COLLATE utf8mb4_unicode_ci,
  `staff_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_documents`
--

INSERT INTO `staff_documents` (`staff_document_id`, `admin_id`, `update_by`, `staff_id`, `document_category_id`, `staff_document_details`, `staff_document_file`, `staff_document_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 'test', 'logo956771546072177.png', 0, '2018-12-29 02:59:37', '2018-12-29 02:59:37'),
(2, 1, 1, 1, 1, 'test', 'logo659101546075904.png', 0, '2018-12-29 04:01:44', '2018-12-29 04:01:44'),
(3, 1, 1, 2, 1, 'C', 'dinesh579131546078113.jpg', 0, '2018-12-29 04:38:33', '2018-12-29 04:38:33'),
(4, 1, 1, 2, 2, 'S', 'dinesh737721546078113.jpg', 0, '2018-12-29 04:38:33', '2018-12-29 04:38:33'),
(5, 1, 1, 2, 1, 'CCC', NULL, 0, '2018-12-29 04:38:55', '2018-12-29 04:38:55');

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE `staff_leaves` (
  `staff_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_leave_from_date` date DEFAULT NULL,
  `staff_leave_to_date` date DEFAULT NULL,
  `staff_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `staff_role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_name` text COLLATE utf8mb4_unicode_ci,
  `staff_role_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_roles`
--

INSERT INTO `staff_roles` (`staff_role_id`, `admin_id`, `update_by`, `staff_role_name`, `staff_role_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'School Principal', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(2, 1, 1, 'Class Teacher', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(3, 1, 1, 'Subject Teacher', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(4, 1, 1, 'Marks Manager', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(5, 1, 1, 'Exam Manager', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(6, 1, 1, 'Accountant', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(7, 1, 1, 'Hostel Warden', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(8, 1, 1, 'HR Manager', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(9, 1, 1, 'Finance Head', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(10, 1, 1, 'Admission Officer / Public Relation Officer', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(11, 1, 1, 'Guard for Visitor Management', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(12, 1, 1, 'Librarian', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(13, 1, 1, 'Website Manager', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(14, 1, 1, 'Driver', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02'),
(15, 1, 1, 'Conductor', 1, '2018-12-29 00:07:02', '2018-12-29 00:07:02');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`, `state_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rajasthan', 1, '2018-12-29 00:16:38', '2018-12-29 00:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `streams`
--

CREATE TABLE `streams` (
  `stream_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `stream_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stream_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_parent_id` int(10) UNSIGNED DEFAULT NULL,
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=PaidBoy,1=RTE,2=FREE',
  `medium_type` int(11) UNSIGNED DEFAULT NULL,
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_date` date DEFAULT NULL,
  `student_privious_result` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_apply_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `student_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_parent_id`, `student_enroll_number`, `student_roll_no`, `student_name`, `student_image`, `student_reg_date`, `student_dob`, `student_email`, `student_gender`, `student_type`, `medium_type`, `student_category`, `caste_id`, `title_id`, `religion_id`, `nationality_id`, `student_sibling_name`, `student_sibling_class_id`, `student_adhar_card_number`, `student_privious_school`, `student_privious_class`, `student_privious_tc_no`, `student_privious_tc_date`, `student_privious_result`, `student_temporary_address`, `student_temporary_city`, `student_temporary_state`, `student_temporary_county`, `student_temporary_pincode`, `student_permanent_address`, `student_permanent_city`, `student_permanent_state`, `student_permanent_county`, `student_permanent_pincode`, `rte_apply_status`, `student_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 6, 1, 'N0001', NULL, 'Teena', NULL, '2018-12-25', '2015-12-25', 'teena@gmail.com', 1, 0, 1, 'OBC', 1, 2, 1, 1, 'Leena', NULL, '123456', 'Apex', 'Nursery', 'Tc0001', '2018-12-25', 'Pass', 'Jodhpur', 1, 1, 1, '342006', 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-03 02:15:18', '2019-01-03 02:15:18'),
(2, 2, 1, 7, 1, 'N0002', NULL, 'Reena', NULL, '2018-01-25', '2018-12-25', 'reena@gmail.com', 0, 0, 1, 'OBC', 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-25', NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-03 02:22:01', '2019-01-22 01:59:04'),
(3, 2, 2, 9, 1, 'N0005', NULL, 'Neha', 'download334151546665811.jpg', '2018-01-25', '2018-01-25', 'neha@gmail.com', 1, 0, 1, 'General', 1, 2, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 2, 1, 1, '342006', 0, 1, '2019-01-04 23:53:31', '2019-01-04 23:53:31'),
(4, 2, 2, 12, 2, 'N0007', NULL, 'Ravina', 'download819521546856346.jpg', '2018-01-25', '2018-01-25', 'ravina@gmail.com', 0, 0, 1, 'OBC', 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-07 04:49:06', '2019-01-07 04:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_history_info`
--

CREATE TABLE `student_academic_history_info` (
  `student_academic_history_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_academic_info_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `percentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `activity_winner` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_info`
--

CREATE TABLE `student_academic_info` (
  `student_academic_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_session_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_class_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_section_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `current_class_id` int(10) UNSIGNED DEFAULT NULL,
  `current_section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_academic_info`
--

INSERT INTO `student_academic_info` (`student_academic_info_id`, `admin_id`, `update_by`, `student_id`, `admission_session_id`, `admission_class_id`, `admission_section_id`, `current_session_id`, `current_class_id`, `current_section_id`, `student_unique_id`, `group_id`, `stream_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 'N0001_1', NULL, NULL, '2019-01-03 02:15:18', '2019-01-03 02:15:18'),
(2, 2, 1, 2, 1, 2, 3, 1, 1, 1, 'N0002_2', NULL, NULL, '2019-01-03 02:22:01', '2019-01-22 01:59:04'),
(3, 2, 2, 3, 1, 1, 2, 1, 1, 2, 'N0005_3', NULL, NULL, '2019-01-04 23:53:31', '2019-01-04 23:53:31'),
(4, 2, 2, 4, 1, 1, 1, 1, 1, 1, 'N0007_4', NULL, NULL, '2019-01-07 04:49:06', '2019-01-07 04:49:06');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE `student_attendance` (
  `student_attendance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `total_present_student` int(10) UNSIGNED DEFAULT NULL,
  `total_absent_student` int(10) UNSIGNED DEFAULT NULL,
  `student_ids` text COLLATE utf8mb4_unicode_ci COMMENT 'Student ids of absent students',
  `student_attendance_date` date DEFAULT NULL,
  `student_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`student_attendance_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `total_present_student`, `total_absent_student`, `student_ids`, `student_attendance_date`, `student_attendance_status`, `created_at`, `updated_at`) VALUES
(6, 4, 4, 1, 1, 1, 1, 2, NULL, '2019-01-02', 1, '2019-01-05 07:08:56', '2019-01-05 07:12:00'),
(7, 4, 4, 1, 2, 2, 1, 2, NULL, '2019-01-02', 1, '2019-01-05 07:08:56', '2019-01-05 07:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_attend_details`
--

CREATE TABLE `student_attend_details` (
  `student_attend_d_id` int(10) UNSIGNED NOT NULL,
  `student_attendance_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_attendance_type` tinyint(4) DEFAULT NULL COMMENT '0=Absent,1=Present',
  `student_attendance_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_attend_details`
--

INSERT INTO `student_attend_details` (`student_attend_d_id`, `student_attendance_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `student_attendance_type`, `student_attendance_date`, `created_at`, `updated_at`) VALUES
(10, 6, 4, 4, 1, 1, 1, 2, 1, '2019-01-02', '2019-01-05 07:08:56', '2019-01-05 07:08:56'),
(11, 6, 4, 4, 1, 1, 1, 2, 0, '2019-02-07', '2019-01-05 07:08:56', '2019-01-05 07:08:56'),
(12, 6, 4, 4, 1, 1, 1, 3, 0, '2019-01-02', '2019-01-05 07:08:56', '2019-01-05 07:12:00'),
(13, 6, 4, 4, 1, 1, 1, 4, 0, '2019-02-06', '2019-01-05 07:08:56', '2019-01-05 07:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_cheques`
--

CREATE TABLE `student_cheques` (
  `student_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `student_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_date` date DEFAULT NULL,
  `student_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `student_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `student_document_details` text COLLATE utf8mb4_unicode_ci,
  `student_document_file` text COLLATE utf8mb4_unicode_ci,
  `student_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_health_info`
--

CREATE TABLE `student_health_info` (
  `student_health_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_health_info`
--

INSERT INTO `student_health_info` (`student_health_info_id`, `admin_id`, `update_by`, `student_id`, `student_height`, `student_weight`, `student_blood_group`, `student_vision_left`, `student_vision_right`, `medical_issues`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '2.2', '55', 'A', 'Right', 'Left', 'No Issues', '2019-01-03 02:15:18', '2019-01-03 02:15:18'),
(2, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, 'No issues', '2019-01-03 02:22:01', '2019-01-22 01:59:04'),
(3, 2, 2, 3, NULL, NULL, NULL, NULL, NULL, 'No issues', '2019-01-04 23:53:31', '2019-01-04 23:53:31'),
(4, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, 'No issues', '2019-01-07 04:49:06', '2019-01-07 04:49:06');

-- --------------------------------------------------------

--
-- Table structure for table `student_leaves`
--

CREATE TABLE `student_leaves` (
  `student_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `student_leave_from_date` date DEFAULT NULL,
  `student_leave_to_date` date DEFAULT NULL,
  `student_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `student_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_leaves`
--

INSERT INTO `student_leaves` (`student_leave_id`, `admin_id`, `update_by`, `student_id`, `student_leave_reason`, `student_leave_from_date`, `student_leave_to_date`, `student_leave_attachment`, `student_leave_status`, `created_at`, `updated_at`) VALUES
(1, 12, 12, 4, 'Sick', '2019-01-19', '2019-01-19', NULL, 0, '2019-01-18 01:34:35', '2019-01-18 01:34:35'),
(2, 12, 12, 4, 'out of station', '2019-01-23', '2019-01-26', NULL, 1, '2019-01-18 01:35:12', '2019-01-18 02:02:19'),
(3, 12, 12, 4, 'Sick', '2019-01-19', '2019-01-19', NULL, 2, '2019-01-18 01:34:35', '2019-01-18 02:02:52'),
(4, 12, 12, 4, 'Sick', '2019-01-19', '2019-01-19', NULL, 0, '2019-01-18 01:34:35', '2019-01-18 01:34:35'),
(5, 5, 5, 3, 'testing', '2019-01-31', '2019-02-16', NULL, 0, '2019-01-22 05:49:06', '2019-01-22 06:59:24'),
(7, 5, 5, 1, 'sadasgfhgjghj', '2019-02-28', '2019-02-28', NULL, 1, '2019-01-22 05:59:52', '2019-01-22 05:59:52'),
(8, 5, 5, 2, 'vbmghvbjmh', '2019-02-27', '2019-02-27', NULL, 0, '2019-01-22 06:16:48', '2019-01-22 06:16:48'),
(9, 5, 5, 2, 'vbmghvbjmh', '2019-02-28', '2019-02-28', NULL, 0, '2019-01-22 06:23:15', '2019-01-22 06:23:15'),
(10, 5, 5, 2, 'htrt', '2019-01-25', '2019-01-26', NULL, 0, '2019-01-22 06:25:20', '2019-01-22 06:25:20'),
(11, 5, 5, 2, 'hjkhjk', '2019-01-24', '2019-01-24', NULL, 0, '2019-01-22 06:32:29', '2019-01-22 06:32:29'),
(12, 5, 5, 1, 'jj', '2019-01-24', '2019-01-24', NULL, 0, '2019-01-22 06:33:07', '2019-01-22 06:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `student_parents`
--

CREATE TABLE `student_parents` (
  `student_parent_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_login_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_parent_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_parents`
--

INSERT INTO `student_parents` (`student_parent_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_login_name`, `student_login_email`, `student_login_contact_no`, `student_father_name`, `student_father_mobile_number`, `student_father_email`, `student_father_occupation`, `student_father_annual_salary`, `student_mother_name`, `student_mother_mobile_number`, `student_mother_email`, `student_mother_occupation`, `student_mother_annual_salary`, `student_mother_tongue`, `student_guardian_name`, `student_guardian_email`, `student_guardian_mobile_number`, `student_guardian_relation`, `student_parent_status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 5, 'Ravi', 'ravi@gmail.com', '5555555555', 'Ravi', '7777777777', 'ravi@gmail.com', 'Teacher', 'Below 5 Lac', 'Mamta', '9999999999', 'mamta@gmail.com', 'Teacher', 'Below 7.5 Lac', 'Tongue', 'Ramesh', 'ramesh@gmail.com', '8888888888', 'Uncle', 1, '2019-01-03 02:15:18', '2019-01-22 01:59:04'),
(2, 2, 2, 11, 'Nath', 'nath@gmail.com', '4444444444', 'Nath', '2222222222', 'nath@gmail.com', 'Job', 'Below 7.5 Lac', 'Manita', '8888888888', 'manita@gmail.com', NULL, 'Below 10 Lac', NULL, NULL, NULL, NULL, NULL, 1, '2019-01-07 04:49:06', '2019-01-07 04:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `st_exclude_sub`
--

CREATE TABLE `st_exclude_sub` (
  `st_exclude_sub_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_parent_groups`
--

CREATE TABLE `st_parent_groups` (
  `st_parent_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=parent',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_parent_group_map`
--

CREATE TABLE `st_parent_group_map` (
  `st_parent_group_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `st_parent_group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_member_id` int(11) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=parent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_is_coscholastic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_id` int(10) UNSIGNED DEFAULT NULL,
  `co_scholastic_subject_des` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `subject_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `admin_id`, `update_by`, `subject_name`, `subject_code`, `subject_is_coscholastic`, `class_ids`, `co_scholastic_type_id`, `co_scholastic_subject_des`, `medium_type`, `subject_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Hindi', 'H', 0, '', NULL, NULL, 1, 1, '2018-12-31 00:28:31', '2018-12-31 00:28:31'),
(2, 1, 1, 'English', 'E', 0, '', NULL, NULL, 1, 1, '2018-12-31 00:28:41', '2018-12-31 00:28:41'),
(3, 1, 1, 'Sports', 'S', 1, '1,2', 2, 'Sports', 1, 1, '2018-12-31 00:29:11', '2018-12-31 00:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `subject_class_map`
--

CREATE TABLE `subject_class_map` (
  `subject_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_class_map`
--

INSERT INTO `subject_class_map` (`subject_class_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `subject_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, 2, 1, '2018-12-31 00:32:26', '2018-12-31 00:32:26'),
(2, 1, 1, 1, 1, NULL, 3, 1, '2018-12-31 00:32:26', '2018-12-31 00:32:26'),
(3, 1, 1, 1, 1, NULL, 1, 1, '2018-12-31 00:53:47', '2018-12-31 00:53:47'),
(4, 1, 1, 1, 3, NULL, 1, 1, '2019-01-10 07:56:29', '2019-01-10 07:56:29'),
(5, 1, 1, 1, 3, NULL, 2, 1, '2019-01-10 07:56:29', '2019-01-10 07:56:29'),
(6, 1, 1, 1, 3, NULL, 3, 1, '2019-01-10 07:56:29', '2019-01-10 07:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `subject_section_map`
--

CREATE TABLE `subject_section_map` (
  `subject_section_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_section_map`
--

INSERT INTO `subject_section_map` (`subject_section_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 2, '2018-12-31 00:32:49', '2018-12-31 00:32:49'),
(2, 1, 1, 1, 1, 1, 3, '2018-12-31 00:32:49', '2018-12-31 00:32:49'),
(3, 1, 1, 1, 1, 1, 1, '2018-12-31 00:54:06', '2018-12-31 00:54:06'),
(4, 1, 1, 1, 3, 5, 1, '2019-01-10 07:56:56', '2019-01-10 07:56:56'),
(5, 1, 1, 1, 3, 5, 2, '2019-01-10 07:56:56', '2019-01-10 07:56:56'),
(6, 1, 1, 1, 3, 5, 3, '2019-01-10 07:56:56', '2019-01-10 07:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `term_exams`
--

CREATE TABLE `term_exams` (
  `term_exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_exam_caption` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `term_exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_exams`
--

INSERT INTO `term_exams` (`term_exam_id`, `admin_id`, `update_by`, `term_exam_name`, `term_exam_caption`, `medium_type`, `term_exam_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Term 1', 'C', 1, 1, '2018-12-31 02:07:47', '2018-12-31 02:07:47'),
(2, 1, 1, 'First Term', NULL, 1, 1, '2019-01-10 05:24:11', '2019-01-10 07:00:56'),
(3, 1, 1, 'Mid Term', NULL, 1, 1, '2019-01-10 05:24:24', '2019-01-10 07:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `time_tables`
--

CREATE TABLE `time_tables` (
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `time_table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `lecture_no` int(10) UNSIGNED DEFAULT NULL,
  `time_table_week_days` text COLLATE utf8mb4_unicode_ci,
  `time_table_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_tables`
--

INSERT INTO `time_tables` (`time_table_id`, `admin_id`, `update_by`, `session_id`, `time_table_name`, `class_id`, `section_id`, `lecture_no`, `time_table_week_days`, `time_table_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Class I', 1, 1, 5, '1,2,3,4,5,6', 1, '2018-12-31 23:34:55', '2019-01-01 04:57:14');

-- --------------------------------------------------------

--
-- Table structure for table `time_table_map`
--

CREATE TABLE `time_table_map` (
  `time_table_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `time_table_id` int(10) UNSIGNED DEFAULT NULL,
  `lecture_no` int(10) UNSIGNED DEFAULT NULL,
  `lunch_break` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `day` int(10) UNSIGNED DEFAULT NULL,
  `start_time` text COLLATE utf8mb4_unicode_ci,
  `end_time` text COLLATE utf8mb4_unicode_ci,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `expire_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Expire,1=Running',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_table_map`
--

INSERT INTO `time_table_map` (`time_table_map_id`, `admin_id`, `update_by`, `session_id`, `time_table_id`, `lecture_no`, `lunch_break`, `day`, `start_time`, `end_time`, `subject_id`, `staff_id`, `expire_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 0, 1, '09:00', '10:00', 2, 2, 1, '2019-01-01 00:26:27', '2019-01-01 00:42:05'),
(2, 2, 2, 1, 1, 3, 1, 4, '12:00', '13:00', NULL, NULL, 1, '2019-01-01 00:29:36', '2019-01-01 00:29:36');

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `title_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `admin_id`, `update_by`, `title_name`, `title_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Mr', 1, '2018-12-29 02:44:15', '2018-12-29 02:44:15'),
(2, 1, 1, 'Ms', 1, '2018-12-29 02:44:40', '2018-12-29 02:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_classes`
--

CREATE TABLE `virtual_classes` (
  `virtual_class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `virtual_class_description` text COLLATE utf8mb4_unicode_ci,
  `virtual_class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `virtual_classes`
--

INSERT INTO `virtual_classes` (`virtual_class_id`, `admin_id`, `update_by`, `virtual_class_name`, `virtual_class_description`, `virtual_class_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'virtual class', 'description', 1, '2018-12-31 23:18:17', '2018-12-31 23:19:20');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_class_map`
--

CREATE TABLE `virtual_class_map` (
  `virtual_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `visitor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purpose` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `check_in_time` time DEFAULT NULL,
  `check_out_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`visitor_id`, `admin_id`, `update_by`, `name`, `purpose`, `contact`, `email`, `visit_date`, `check_in_time`, `check_out_time`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'test', 'test', '9999999999', 'test@gmail.com', '2019-01-02', '10:00:00', '10:10:00', '2019-01-02 03:48:08', '2019-01-02 03:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_ac`
--

CREATE TABLE `wallet_ac` (
  `wallet_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `wallet_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet_ac_amt` double DEFAULT NULL,
  `wallet_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_ac`
--

INSERT INTO `wallet_ac` (`wallet_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `wallet_ac_name`, `wallet_ac_amt`, `wallet_ac_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 'Teena', 0, 1, '2019-01-03 02:15:18', '2019-01-03 02:15:18'),
(2, 2, 2, 1, 1, 2, 'Reena', 0, 1, '2019-01-03 02:22:01', '2019-01-03 02:22:01'),
(3, 2, 2, 1, 1, 3, 'Neha', 0, 1, '2019-01-04 23:53:32', '2019-01-04 23:53:32'),
(4, 2, 2, 1, 1, 4, 'Ravina', 0, 1, '2019-01-07 04:49:06', '2019-01-07 04:49:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  ADD PRIMARY KEY (`ac_entry_map_id`),
  ADD KEY `ac_entry_map_admin_id_foreign` (`admin_id`),
  ADD KEY `ac_entry_map_update_by_foreign` (`update_by`),
  ADD KEY `ac_entry_map_session_id_foreign` (`session_id`),
  ADD KEY `ac_entry_map_class_id_foreign` (`class_id`),
  ADD KEY `ac_entry_map_section_id_foreign` (`section_id`),
  ADD KEY `ac_entry_map_student_id_foreign` (`student_id`),
  ADD KEY `ac_entry_map_ac_entry_id_foreign` (`ac_entry_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admission_data`
--
ALTER TABLE `admission_data`
  ADD PRIMARY KEY (`admission_data_id`),
  ADD KEY `admission_data_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_data_update_by_foreign` (`update_by`),
  ADD KEY `admission_data_admission_form_id_foreign` (`admission_form_id`),
  ADD KEY `admission_data_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `admission_data_title_id_foreign` (`title_id`),
  ADD KEY `admission_data_caste_id_foreign` (`caste_id`),
  ADD KEY `admission_data_religion_id_foreign` (`religion_id`),
  ADD KEY `admission_data_nationality_id_foreign` (`nationality_id`),
  ADD KEY `admission_data_student_permanent_county_foreign` (`student_permanent_county`),
  ADD KEY `admission_data_student_permanent_state_foreign` (`student_permanent_state`),
  ADD KEY `admission_data_student_permanent_city_foreign` (`student_permanent_city`),
  ADD KEY `admission_data_student_temporary_county_foreign` (`student_temporary_county`),
  ADD KEY `admission_data_student_temporary_state_foreign` (`student_temporary_state`),
  ADD KEY `admission_data_student_temporary_city_foreign` (`student_temporary_city`),
  ADD KEY `admission_data_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `admission_data_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `admission_data_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `admission_data_current_session_id_foreign` (`current_session_id`),
  ADD KEY `admission_data_current_class_id_foreign` (`current_class_id`),
  ADD KEY `admission_data_current_section_id_foreign` (`current_section_id`),
  ADD KEY `admission_data_group_id_foreign` (`group_id`),
  ADD KEY `admission_data_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `admission_fields`
--
ALTER TABLE `admission_fields`
  ADD PRIMARY KEY (`admission_field_id`),
  ADD KEY `admission_fields_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_fields_update_by_foreign` (`update_by`),
  ADD KEY `admission_fields_admission_form_id_foreign` (`admission_form_id`);

--
-- Indexes for table `admission_forms`
--
ALTER TABLE `admission_forms`
  ADD PRIMARY KEY (`admission_form_id`),
  ADD KEY `admission_forms_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_forms_update_by_foreign` (`update_by`),
  ADD KEY `admission_forms_session_id_foreign` (`session_id`),
  ADD KEY `admission_forms_brochure_id_foreign` (`brochure_id`);

--
-- Indexes for table `api_tokens`
--
ALTER TABLE `api_tokens`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`app_setting_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `books_admin_id_foreign` (`admin_id`),
  ADD KEY `books_update_by_foreign` (`update_by`),
  ADD KEY `books_book_category_id_foreign` (`book_category_id`),
  ADD KEY `books_book_vendor_id_foreign` (`book_vendor_id`),
  ADD KEY `books_book_cupboard_id_foreign` (`book_cupboard_id`),
  ADD KEY `books_book_cupboardshelf_id_foreign` (`book_cupboardshelf_id`);

--
-- Indexes for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD PRIMARY KEY (`book_allowance_id`),
  ADD KEY `book_allowance_admin_id_foreign` (`admin_id`),
  ADD KEY `book_allowance_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`book_category_id`),
  ADD KEY `book_categories_admin_id_foreign` (`admin_id`),
  ADD KEY `book_categories_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD PRIMARY KEY (`book_info_id`),
  ADD KEY `book_copies_info_admin_id_foreign` (`admin_id`),
  ADD KEY `book_copies_info_update_by_foreign` (`update_by`),
  ADD KEY `book_copies_info_book_id_foreign` (`book_id`);

--
-- Indexes for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD PRIMARY KEY (`book_cupboard_id`),
  ADD KEY `book_cupboards_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboards_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD PRIMARY KEY (`book_cupboardshelf_id`),
  ADD KEY `book_cupboardshelfs_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboardshelfs_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD PRIMARY KEY (`book_vendor_id`),
  ADD KEY `book_vendors_admin_id_foreign` (`admin_id`),
  ADD KEY `book_vendors_update_by_foreign` (`update_by`);

--
-- Indexes for table `brochures`
--
ALTER TABLE `brochures`
  ADD PRIMARY KEY (`brochure_id`),
  ADD KEY `brochures_admin_id_foreign` (`admin_id`),
  ADD KEY `brochures_update_by_foreign` (`update_by`),
  ADD KEY `brochures_session_id_foreign` (`session_id`);

--
-- Indexes for table `caste`
--
ALTER TABLE `caste`
  ADD PRIMARY KEY (`caste_id`),
  ADD KEY `caste_admin_id_foreign` (`admin_id`),
  ADD KEY `caste_update_by_foreign` (`update_by`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`certificate_id`),
  ADD KEY `certificates_admin_id_foreign` (`admin_id`),
  ADD KEY `certificates_update_by_foreign` (`update_by`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `city_state_id_foreign` (`state_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `classes_admin_id_foreign` (`admin_id`),
  ADD KEY `classes_update_by_foreign` (`update_by`),
  ADD KEY `classes_board_id_foreign` (`board_id`);

--
-- Indexes for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD PRIMARY KEY (`class_subject_staff_map_id`),
  ADD KEY `class_subject_staff_map_admin_id_foreign` (`admin_id`),
  ADD KEY `class_subject_staff_map_update_by_foreign` (`update_by`),
  ADD KEY `class_subject_staff_map_class_id_foreign` (`class_id`),
  ADD KEY `class_subject_staff_map_section_id_foreign` (`section_id`),
  ADD KEY `class_subject_staff_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`competition_id`),
  ADD KEY `competitions_admin_id_foreign` (`admin_id`),
  ADD KEY `competitions_update_by_foreign` (`update_by`);

--
-- Indexes for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD PRIMARY KEY (`student_competition_map_id`),
  ADD KEY `competition_map_admin_id_foreign` (`admin_id`),
  ADD KEY `competition_map_update_by_foreign` (`update_by`),
  ADD KEY `competition_map_competition_id_foreign` (`competition_id`),
  ADD KEY `competition_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD PRIMARY KEY (`concession_map_id`),
  ADD KEY `concession_map_admin_id_foreign` (`admin_id`),
  ADD KEY `concession_map_update_by_foreign` (`update_by`),
  ADD KEY `concession_map_class_id_foreign` (`class_id`),
  ADD KEY `concession_map_session_id_foreign` (`session_id`),
  ADD KEY `concession_map_student_id_foreign` (`student_id`),
  ADD KEY `concession_map_reason_id_foreign` (`reason_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD PRIMARY KEY (`co_scholastic_type_id`),
  ADD KEY `co_scholastic_types_admin_id_foreign` (`admin_id`),
  ADD KEY `co_scholastic_types_update_by_foreign` (`update_by`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`),
  ADD KEY `designations_admin_id_foreign` (`admin_id`),
  ADD KEY `designations_update_by_foreign` (`update_by`);

--
-- Indexes for table `document_category`
--
ALTER TABLE `document_category`
  ADD PRIMARY KEY (`document_category_id`),
  ADD KEY `document_category_admin_id_foreign` (`admin_id`),
  ADD KEY `document_category_update_by_foreign` (`update_by`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`exam_id`),
  ADD KEY `exams_admin_id_foreign` (`admin_id`),
  ADD KEY `exams_update_by_foreign` (`update_by`),
  ADD KEY `exams_term_exam_id_foreign` (`term_exam_id`);

--
-- Indexes for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD PRIMARY KEY (`exam_map_id`),
  ADD KEY `exam_map_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_map_update_by_foreign` (`update_by`),
  ADD KEY `exam_map_session_id_foreign` (`session_id`),
  ADD KEY `exam_map_class_id_foreign` (`class_id`),
  ADD KEY `exam_map_section_id_foreign` (`section_id`),
  ADD KEY `exam_map_subject_id_foreign` (`subject_id`),
  ADD KEY `exam_map_marks_criteria_id_foreign` (`marks_criteria_id`),
  ADD KEY `exam_map_grade_scheme_id_foreign` (`grade_scheme_id`);

--
-- Indexes for table `exam_marks`
--
ALTER TABLE `exam_marks`
  ADD PRIMARY KEY (`exam_mark_id`),
  ADD KEY `exam_marks_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_marks_update_by_foreign` (`update_by`),
  ADD KEY `exam_marks_session_id_foreign` (`session_id`),
  ADD KEY `exam_marks_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `exam_marks_exam_id_foreign` (`exam_id`),
  ADD KEY `exam_marks_class_id_foreign` (`class_id`),
  ADD KEY `exam_marks_section_id_foreign` (`section_id`),
  ADD KEY `exam_marks_student_id_foreign` (`student_id`),
  ADD KEY `exam_marks_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  ADD PRIMARY KEY (`exam_schedule_id`),
  ADD KEY `exam_schedules_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_schedules_update_by_foreign` (`update_by`),
  ADD KEY `exam_schedules_session_id_foreign` (`session_id`),
  ADD KEY `exam_schedules_exam_id_foreign` (`exam_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `facilities_admin_id_foreign` (`admin_id`),
  ADD KEY `facilities_update_by_foreign` (`update_by`);

--
-- Indexes for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD PRIMARY KEY (`fees_st_map_id`),
  ADD KEY `fees_st_map_admin_id_foreign` (`admin_id`),
  ADD KEY `fees_st_map_update_by_foreign` (`update_by`),
  ADD KEY `fees_st_map_class_id_foreign` (`class_id`),
  ADD KEY `fees_st_map_session_id_foreign` (`session_id`),
  ADD KEY `fees_st_map_section_id_foreign` (`section_id`);

--
-- Indexes for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `fee_receipt_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_session_id_foreign` (`session_id`),
  ADD KEY `fee_receipt_class_id_foreign` (`class_id`),
  ADD KEY `fee_receipt_student_id_foreign` (`student_id`),
  ADD KEY `fee_receipt_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD PRIMARY KEY (`fee_receipt_detail_id`),
  ADD KEY `fee_receipt_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_details_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_details_receipt_id_foreign` (`receipt_id`),
  ADD KEY `fee_receipt_details_head_inst_id_foreign` (`head_inst_id`),
  ADD KEY `fee_receipt_details_r_concession_map_id_foreign` (`r_concession_map_id`),
  ADD KEY `fee_receipt_details_r_fine_id_foreign` (`r_fine_id`),
  ADD KEY `fee_receipt_details_r_fine_detail_id_foreign` (`r_fine_detail_id`);

--
-- Indexes for table `fine`
--
ALTER TABLE `fine`
  ADD PRIMARY KEY (`fine_id`),
  ADD KEY `fine_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_update_by_foreign` (`update_by`);

--
-- Indexes for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD PRIMARY KEY (`fine_detail_id`),
  ADD KEY `fine_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_details_update_by_foreign` (`update_by`),
  ADD KEY `fine_details_fine_id_foreign` (`fine_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`),
  ADD KEY `grades_admin_id_foreign` (`admin_id`),
  ADD KEY `grades_update_by_foreign` (`update_by`),
  ADD KEY `grades_grade_scheme_id_foreign` (`grade_scheme_id`);

--
-- Indexes for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD PRIMARY KEY (`grade_scheme_id`),
  ADD KEY `grade_schemes_admin_id_foreign` (`admin_id`),
  ADD KEY `grade_schemes_update_by_foreign` (`update_by`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `groups_admin_id_foreign` (`admin_id`),
  ADD KEY `groups_update_by_foreign` (`update_by`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holiday_id`),
  ADD KEY `holidays_admin_id_foreign` (`admin_id`),
  ADD KEY `holidays_update_by_foreign` (`update_by`),
  ADD KEY `holidays_session_id_foreign` (`session_id`);

--
-- Indexes for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  ADD PRIMARY KEY (`h_conversation_id`),
  ADD KEY `homework_conversations_admin_id_foreign` (`admin_id`),
  ADD KEY `homework_conversations_update_by_foreign` (`update_by`),
  ADD KEY `homework_conversations_homework_group_id_foreign` (`homework_group_id`),
  ADD KEY `homework_conversations_staff_id_foreign` (`staff_id`),
  ADD KEY `homework_conversations_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD PRIMARY KEY (`homework_group_id`),
  ADD KEY `homework_groups_admin_id_foreign` (`admin_id`),
  ADD KEY `homework_groups_update_by_foreign` (`update_by`),
  ADD KEY `homework_groups_class_id_foreign` (`class_id`),
  ADD KEY `homework_groups_section_id_foreign` (`section_id`);

--
-- Indexes for table `hostels`
--
ALTER TABLE `hostels`
  ADD PRIMARY KEY (`hostel_id`),
  ADD KEY `hostels_admin_id_foreign` (`admin_id`),
  ADD KEY `hostels_update_by_foreign` (`update_by`);

--
-- Indexes for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  ADD PRIMARY KEY (`h_block_id`),
  ADD KEY `hostel_blocks_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_blocks_update_by_foreign` (`update_by`),
  ADD KEY `hostel_blocks_hostel_id_foreign` (`hostel_id`);

--
-- Indexes for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  ADD PRIMARY KEY (`h_floor_id`),
  ADD KEY `hostel_floors_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_floors_update_by_foreign` (`update_by`),
  ADD KEY `hostel_floors_h_block_id_foreign` (`h_block_id`);

--
-- Indexes for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  ADD PRIMARY KEY (`h_room_id`),
  ADD KEY `hostel_rooms_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_rooms_update_by_foreign` (`update_by`),
  ADD KEY `hostel_rooms_hostel_id_foreign` (`hostel_id`),
  ADD KEY `hostel_rooms_h_block_id_foreign` (`h_block_id`),
  ADD KEY `hostel_rooms_h_floor_id_foreign` (`h_floor_id`),
  ADD KEY `hostel_rooms_h_room_cate_id_foreign` (`h_room_cate_id`);

--
-- Indexes for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  ADD PRIMARY KEY (`h_room_cate_id`),
  ADD KEY `hostel_room_cate_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_room_cate_update_by_foreign` (`update_by`);

--
-- Indexes for table `hostel_student_map`
--
ALTER TABLE `hostel_student_map`
  ADD PRIMARY KEY (`h_student_map_id`),
  ADD KEY `hostel_student_map_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_student_map_update_by_foreign` (`update_by`),
  ADD KEY `hostel_student_map_student_id_foreign` (`student_id`),
  ADD KEY `hostel_student_map_hostel_id_foreign` (`hostel_id`),
  ADD KEY `hostel_student_map_h_block_id_foreign` (`h_block_id`),
  ADD KEY `hostel_student_map_h_floor_id_foreign` (`h_floor_id`),
  ADD KEY `hostel_student_map_h_room_id_foreign` (`h_room_id`);

--
-- Indexes for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  ADD PRIMARY KEY (`imprest_ac_id`),
  ADD KEY `imprest_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_update_by_foreign` (`update_by`),
  ADD KEY `imprest_ac_session_id_foreign` (`session_id`),
  ADD KEY `imprest_ac_class_id_foreign` (`class_id`),
  ADD KEY `imprest_ac_student_id_foreign` (`student_id`);

--
-- Indexes for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  ADD PRIMARY KEY (`imprest_ac_confi_id`),
  ADD KEY `imprest_ac_confi_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_confi_update_by_foreign` (`update_by`);

--
-- Indexes for table `imprest_ac_entries`
--
ALTER TABLE `imprest_ac_entries`
  ADD PRIMARY KEY (`ac_entry_id`),
  ADD KEY `imprest_ac_entries_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_entries_update_by_foreign` (`update_by`),
  ADD KEY `imprest_ac_entries_session_id_foreign` (`session_id`),
  ADD KEY `imprest_ac_entries_class_id_foreign` (`class_id`),
  ADD KEY `imprest_ac_entries_section_id_foreign` (`section_id`);

--
-- Indexes for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD PRIMARY KEY (`issued_book_id`),
  ADD KEY `issued_books_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_book_id_foreign` (`book_id`),
  ADD KEY `issued_books_book_copies_id_foreign` (`book_copies_id`);

--
-- Indexes for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `issued_books_history_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_history_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_history_book_id_foreign` (`book_id`),
  ADD KEY `issued_books_history_book_copies_id_foreign` (`book_copies_id`);

--
-- Indexes for table `library_fine`
--
ALTER TABLE `library_fine`
  ADD PRIMARY KEY (`library_fine_id`),
  ADD KEY `library_fine_admin_id_foreign` (`admin_id`),
  ADD KEY `library_fine_update_by_foreign` (`update_by`),
  ADD KEY `library_fine_member_id_foreign` (`member_id`);

--
-- Indexes for table `library_members`
--
ALTER TABLE `library_members`
  ADD PRIMARY KEY (`library_member_id`),
  ADD KEY `library_members_admin_id_foreign` (`admin_id`),
  ADD KEY `library_members_update_by_foreign` (`update_by`);

--
-- Indexes for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  ADD PRIMARY KEY (`marks_criteria_id`),
  ADD KEY `marks_criteria_admin_id_foreign` (`admin_id`),
  ADD KEY `marks_criteria_update_by_foreign` (`update_by`);

--
-- Indexes for table `medium_type`
--
ALTER TABLE `medium_type`
  ADD PRIMARY KEY (`medium_type_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`nationality_id`),
  ADD KEY `nationality_admin_id_foreign` (`admin_id`),
  ADD KEY `nationality_update_by_foreign` (`update_by`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`),
  ADD KEY `notice_admin_id_foreign` (`admin_id`),
  ADD KEY `notice_update_by_foreign` (`update_by`),
  ADD KEY `notice_session_id_foreign` (`session_id`);

--
-- Indexes for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  ADD PRIMARY KEY (`ot_head_id`),
  ADD KEY `one_time_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `one_time_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD PRIMARY KEY (`online_note_id`),
  ADD KEY `online_notes_admin_id_foreign` (`admin_id`),
  ADD KEY `online_notes_update_by_foreign` (`update_by`),
  ADD KEY `online_notes_class_id_foreign` (`class_id`),
  ADD KEY `online_notes_section_id_foreign` (`section_id`),
  ADD KEY `online_notes_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD PRIMARY KEY (`question_paper_id`),
  ADD KEY `question_papers_admin_id_foreign` (`admin_id`),
  ADD KEY `question_papers_update_by_foreign` (`update_by`),
  ADD KEY `question_papers_class_id_foreign` (`class_id`),
  ADD KEY `question_papers_section_id_foreign` (`section_id`),
  ADD KEY `question_papers_subject_id_foreign` (`subject_id`),
  ADD KEY `question_papers_exam_id_foreign` (`exam_id`),
  ADD KEY `question_papers_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`reason_id`),
  ADD KEY `reasons_admin_id_foreign` (`admin_id`),
  ADD KEY `reasons_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  ADD PRIMARY KEY (`rc_head_id`),
  ADD KEY `recurring_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  ADD PRIMARY KEY (`rc_inst_id`),
  ADD KEY `recurring_inst_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_inst_update_by_foreign` (`update_by`),
  ADD KEY `recurring_inst_rc_head_id_foreign` (`rc_head_id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`religion_id`),
  ADD KEY `religions_admin_id_foreign` (`admin_id`),
  ADD KEY `religions_update_by_foreign` (`update_by`);

--
-- Indexes for table `remarks`
--
ALTER TABLE `remarks`
  ADD PRIMARY KEY (`remark_id`),
  ADD KEY `remarks_admin_id_foreign` (`admin_id`),
  ADD KEY `remarks_update_by_foreign` (`update_by`),
  ADD KEY `remarks_session_id_foreign` (`session_id`),
  ADD KEY `remarks_class_id_foreign` (`class_id`),
  ADD KEY `remarks_section_id_foreign` (`section_id`),
  ADD KEY `remarks_student_id_foreign` (`student_id`),
  ADD KEY `remarks_staff_id_foreign` (`staff_id`),
  ADD KEY `remarks_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `room_no`
--
ALTER TABLE `room_no`
  ADD PRIMARY KEY (`room_no_id`),
  ADD KEY `room_no_admin_id_foreign` (`admin_id`),
  ADD KEY `room_no_update_by_foreign` (`update_by`);

--
-- Indexes for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  ADD PRIMARY KEY (`rte_cheque_id`),
  ADD KEY `rte_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_cheques_update_by_foreign` (`update_by`),
  ADD KEY `rte_cheques_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `rte_heads`
--
ALTER TABLE `rte_heads`
  ADD PRIMARY KEY (`rte_head_id`),
  ADD KEY `rte_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  ADD PRIMARY KEY (`rte_head_map_id`),
  ADD KEY `rte_head_map_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_head_map_update_by_foreign` (`update_by`),
  ADD KEY `rte_head_map_rte_head_id_foreign` (`rte_head_id`),
  ADD KEY `rte_head_map_session_id_foreign` (`session_id`),
  ADD KEY `rte_head_map_class_id_foreign` (`class_id`),
  ADD KEY `rte_head_map_section_id_foreign` (`section_id`),
  ADD KEY `rte_head_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `schedule_map`
--
ALTER TABLE `schedule_map`
  ADD PRIMARY KEY (`schedule_map_id`),
  ADD KEY `schedule_map_admin_id_foreign` (`admin_id`),
  ADD KEY `schedule_map_update_by_foreign` (`update_by`),
  ADD KEY `schedule_map_session_id_foreign` (`session_id`),
  ADD KEY `schedule_map_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `schedule_map_exam_id_foreign` (`exam_id`),
  ADD KEY `schedule_map_class_id_foreign` (`class_id`),
  ADD KEY `schedule_map_section_id_foreign` (`section_id`),
  ADD KEY `schedule_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `schedule_room_map`
--
ALTER TABLE `schedule_room_map`
  ADD PRIMARY KEY (`schedule_room_map_id`),
  ADD KEY `schedule_room_map_admin_id_foreign` (`admin_id`),
  ADD KEY `schedule_room_map_update_by_foreign` (`update_by`),
  ADD KEY `schedule_room_map_session_id_foreign` (`session_id`),
  ADD KEY `schedule_room_map_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `schedule_room_map_exam_id_foreign` (`exam_id`),
  ADD KEY `schedule_room_map_class_id_foreign` (`class_id`),
  ADD KEY `schedule_room_map_section_id_foreign` (`section_id`),
  ADD KEY `schedule_room_map_room_no_id_foreign` (`room_no_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `school_admin_id_foreign` (`admin_id`),
  ADD KEY `school_update_by_foreign` (`update_by`),
  ADD KEY `school_country_id_foreign` (`country_id`),
  ADD KEY `school_state_id_foreign` (`state_id`),
  ADD KEY `school_city_id_foreign` (`city_id`);

--
-- Indexes for table `school_boards`
--
ALTER TABLE `school_boards`
  ADD PRIMARY KEY (`board_id`),
  ADD KEY `school_boards_admin_id_foreign` (`admin_id`),
  ADD KEY `school_boards_update_by_foreign` (`update_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `sections_admin_id_foreign` (`admin_id`),
  ADD KEY `sections_update_by_foreign` (`update_by`),
  ADD KEY `sections_class_id_foreign` (`class_id`),
  ADD KEY `sections_shift_id_foreign` (`shift_id`),
  ADD KEY `sections_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `sessions_admin_id_foreign` (`admin_id`),
  ADD KEY `sessions_update_by_foreign` (`update_by`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `shifts_admin_id_foreign` (`admin_id`),
  ADD KEY `shifts_update_by_foreign` (`update_by`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `staff_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_update_by_foreign` (`update_by`),
  ADD KEY `staff_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `staff_designation_id_foreign` (`designation_id`),
  ADD KEY `staff_title_id_foreign` (`title_id`),
  ADD KEY `staff_caste_id_foreign` (`caste_id`),
  ADD KEY `staff_religion_id_foreign` (`religion_id`),
  ADD KEY `staff_nationality_id_foreign` (`nationality_id`),
  ADD KEY `staff_staff_temporary_county_foreign` (`staff_temporary_county`),
  ADD KEY `staff_staff_temporary_state_foreign` (`staff_temporary_state`),
  ADD KEY `staff_staff_temporary_city_foreign` (`staff_temporary_city`),
  ADD KEY `staff_staff_permanent_county_foreign` (`staff_permanent_county`),
  ADD KEY `staff_staff_permanent_state_foreign` (`staff_permanent_state`),
  ADD KEY `staff_staff_permanent_city_foreign` (`staff_permanent_city`);

--
-- Indexes for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  ADD PRIMARY KEY (`staff_attendance_id`),
  ADD KEY `student_attendance_admin_id_foreign` (`admin_id`),
  ADD KEY `student_attendance_update_by_foreign` (`update_by`),
  ADD KEY `student_attendance_session_id_foreign` (`session_id`);

--
-- Indexes for table `staff_attend_details`
--
ALTER TABLE `staff_attend_details`
  ADD PRIMARY KEY (`staff_attend_d_id`),
  ADD KEY `staff_attendance_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_attendance_update_by_foreign` (`update_by`),
  ADD KEY `staff_attendance_session_id_foreign` (`session_id`),
  ADD KEY `staff_id` (`staff_id`),
  ADD KEY `staff_attendance_id` (`staff_attendance_id`);

--
-- Indexes for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD PRIMARY KEY (`staff_class_allocation_id`),
  ADD KEY `staff_class_allocations_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_class_allocations_update_by_foreign` (`update_by`),
  ADD KEY `staff_class_allocations_class_id_foreign` (`class_id`),
  ADD KEY `staff_class_allocations_section_id_foreign` (`section_id`),
  ADD KEY `staff_class_allocations_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD PRIMARY KEY (`staff_document_id`),
  ADD KEY `staff_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_documents_update_by_foreign` (`update_by`),
  ADD KEY `staff_documents_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD PRIMARY KEY (`staff_leave_id`),
  ADD KEY `staff_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_leaves_update_by_foreign` (`update_by`),
  ADD KEY `staff_leaves_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`staff_role_id`),
  ADD KEY `staff_roles_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_roles_update_by_foreign` (`update_by`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `state_country_id_foreign` (`country_id`);

--
-- Indexes for table `streams`
--
ALTER TABLE `streams`
  ADD PRIMARY KEY (`stream_id`),
  ADD KEY `streams_admin_id_foreign` (`admin_id`),
  ADD KEY `streams_update_by_foreign` (`update_by`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_admin_id_foreign` (`admin_id`),
  ADD KEY `students_update_by_foreign` (`update_by`),
  ADD KEY `students_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `students_student_parent_id_foreign` (`student_parent_id`),
  ADD KEY `students_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `students_caste_id_foreign` (`caste_id`),
  ADD KEY `students_title_id_foreign` (`title_id`),
  ADD KEY `students_religion_id_foreign` (`religion_id`),
  ADD KEY `students_nationality_id_foreign` (`nationality_id`),
  ADD KEY `students_student_permanent_county_foreign` (`student_permanent_county`),
  ADD KEY `students_student_permanent_state_foreign` (`student_permanent_state`),
  ADD KEY `students_student_permanent_city_foreign` (`student_permanent_city`),
  ADD KEY `students_student_temporary_county_foreign` (`student_temporary_county`),
  ADD KEY `students_student_temporary_state_foreign` (`student_temporary_state`),
  ADD KEY `students_student_temporary_city_foreign` (`student_temporary_city`),
  ADD KEY `medium_type` (`medium_type`);

--
-- Indexes for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD PRIMARY KEY (`student_academic_history_info_id`),
  ADD KEY `student_academic_history_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_history_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_history_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_history_info_student_academic_info_id_foreign` (`student_academic_info_id`),
  ADD KEY `student_academic_history_info_session_id_foreign` (`session_id`),
  ADD KEY `student_academic_history_info_class_id_foreign` (`class_id`),
  ADD KEY `student_academic_history_info_stream_id_foreign` (`stream_id`),
  ADD KEY `student_academic_history_info_section_id_foreign` (`section_id`);

--
-- Indexes for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`),
  ADD KEY `student_academic_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_info_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `student_academic_info_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `student_academic_info_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `student_academic_info_current_session_id_foreign` (`current_session_id`),
  ADD KEY `student_academic_info_current_class_id_foreign` (`current_class_id`),
  ADD KEY `student_academic_info_current_section_id_foreign` (`current_section_id`),
  ADD KEY `student_academic_info_group_id_foreign` (`group_id`),
  ADD KEY `student_academic_info_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD PRIMARY KEY (`student_attendance_id`),
  ADD KEY `student_attendance_admin_id_foreign` (`admin_id`),
  ADD KEY `student_attendance_update_by_foreign` (`update_by`),
  ADD KEY `student_attendance_session_id_foreign` (`session_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `student_attend_details`
--
ALTER TABLE `student_attend_details`
  ADD PRIMARY KEY (`student_attend_d_id`),
  ADD KEY `student_attend_details_admin_id_foreign` (`admin_id`),
  ADD KEY `student_attend_details_update_by_foreign` (`update_by`),
  ADD KEY `student_attend_details_session_id_foreign` (`session_id`),
  ADD KEY `student_attend_details_class_id_foreign` (`class_id`),
  ADD KEY `student_attend_details_section_id_foreign` (`section_id`),
  ADD KEY `student_attend_details_student_attendance_id_foreign` (`student_attendance_id`),
  ADD KEY `student_attend_details_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_cheques`
--
ALTER TABLE `student_cheques`
  ADD PRIMARY KEY (`student_cheque_id`),
  ADD KEY `student_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `student_cheques_update_by_foreign` (`update_by`),
  ADD KEY `student_cheques_student_id_foreign` (`student_id`),
  ADD KEY `student_cheques_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`student_document_id`),
  ADD KEY `student_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_documents_update_by_foreign` (`update_by`),
  ADD KEY `student_documents_student_id_foreign` (`student_id`),
  ADD KEY `student_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD PRIMARY KEY (`student_health_info_id`),
  ADD KEY `student_health_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_health_info_update_by_foreign` (`update_by`),
  ADD KEY `student_health_info_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD PRIMARY KEY (`student_leave_id`),
  ADD KEY `student_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `student_leaves_update_by_foreign` (`update_by`),
  ADD KEY `student_leaves_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD PRIMARY KEY (`student_parent_id`),
  ADD KEY `student_parents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_parents_update_by_foreign` (`update_by`),
  ADD KEY `student_parents_reference_admin_id_foreign` (`reference_admin_id`);

--
-- Indexes for table `st_exclude_sub`
--
ALTER TABLE `st_exclude_sub`
  ADD PRIMARY KEY (`st_exclude_sub_id`),
  ADD KEY `st_exclude_sub_admin_id_foreign` (`admin_id`),
  ADD KEY `st_exclude_sub_update_by_foreign` (`update_by`),
  ADD KEY `st_exclude_sub_session_id_foreign` (`session_id`),
  ADD KEY `st_exclude_sub_class_id_foreign` (`class_id`),
  ADD KEY `st_exclude_sub_section_id_foreign` (`section_id`),
  ADD KEY `st_exclude_sub_subject_id_foreign` (`subject_id`),
  ADD KEY `st_exclude_sub_student_id_foreign` (`student_id`);

--
-- Indexes for table `st_parent_groups`
--
ALTER TABLE `st_parent_groups`
  ADD PRIMARY KEY (`st_parent_group_id`),
  ADD KEY `st_parent_groups_admin_id_foreign` (`admin_id`),
  ADD KEY `st_parent_groups_update_by_foreign` (`update_by`);

--
-- Indexes for table `st_parent_group_map`
--
ALTER TABLE `st_parent_group_map`
  ADD PRIMARY KEY (`st_parent_group_map_id`),
  ADD KEY `st_parent_group_map_admin_id_foreign` (`admin_id`),
  ADD KEY `st_parent_group_map_update_by_foreign` (`update_by`),
  ADD KEY `st_parent_group_map_st_parent_group_id_foreign` (`st_parent_group_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD KEY `subjects_admin_id_foreign` (`admin_id`),
  ADD KEY `subjects_update_by_foreign` (`update_by`),
  ADD KEY `subjects_co_scholastic_type_id_foreign` (`co_scholastic_type_id`);

--
-- Indexes for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  ADD PRIMARY KEY (`subject_class_map_id`),
  ADD KEY `subject_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_class_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_class_map_session_id_foreign` (`session_id`),
  ADD KEY `subject_class_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_class_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_class_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  ADD PRIMARY KEY (`subject_section_map_id`),
  ADD KEY `subject_section_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_section_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_section_map_session_id_foreign` (`session_id`),
  ADD KEY `subject_section_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_section_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_section_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD PRIMARY KEY (`term_exam_id`),
  ADD KEY `term_exams_admin_id_foreign` (`admin_id`),
  ADD KEY `term_exams_update_by_foreign` (`update_by`);

--
-- Indexes for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD PRIMARY KEY (`time_table_id`),
  ADD KEY `time_tables_admin_id_foreign` (`admin_id`),
  ADD KEY `time_tables_update_by_foreign` (`update_by`),
  ADD KEY `time_tables_session_id_foreign` (`session_id`),
  ADD KEY `time_tables_class_id_foreign` (`class_id`),
  ADD KEY `time_tables_section_id_foreign` (`section_id`);

--
-- Indexes for table `time_table_map`
--
ALTER TABLE `time_table_map`
  ADD PRIMARY KEY (`time_table_map_id`),
  ADD KEY `time_table_map_admin_id_foreign` (`admin_id`),
  ADD KEY `time_table_map_update_by_foreign` (`update_by`),
  ADD KEY `time_table_map_session_id_foreign` (`session_id`),
  ADD KEY `time_table_map_time_table_id_foreign` (`time_table_id`),
  ADD KEY `time_table_map_subject_id_foreign` (`subject_id`),
  ADD KEY `time_table_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`),
  ADD KEY `titles_admin_id_foreign` (`admin_id`),
  ADD KEY `titles_update_by_foreign` (`update_by`);

--
-- Indexes for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD PRIMARY KEY (`virtual_class_id`),
  ADD KEY `virtual_classes_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_classes_update_by_foreign` (`update_by`);

--
-- Indexes for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  ADD PRIMARY KEY (`virtual_class_map_id`),
  ADD KEY `virtual_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_class_map_update_by_foreign` (`update_by`),
  ADD KEY `virtual_class_map_virtual_class_id_foreign` (`virtual_class_id`),
  ADD KEY `virtual_class_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`visitor_id`),
  ADD KEY `visitors_admin_id_foreign` (`admin_id`),
  ADD KEY `visitors_update_by_foreign` (`update_by`);

--
-- Indexes for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  ADD PRIMARY KEY (`wallet_ac_id`),
  ADD KEY `wallet_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `wallet_ac_update_by_foreign` (`update_by`),
  ADD KEY `wallet_ac_session_id_foreign` (`session_id`),
  ADD KEY `wallet_ac_class_id_foreign` (`class_id`),
  ADD KEY `wallet_ac_student_id_foreign` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  MODIFY `ac_entry_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `admission_data`
--
ALTER TABLE `admission_data`
  MODIFY `admission_data_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admission_fields`
--
ALTER TABLE `admission_fields`
  MODIFY `admission_field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admission_forms`
--
ALTER TABLE `admission_forms`
  MODIFY `admission_form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api_tokens`
--
ALTER TABLE `api_tokens`
  MODIFY `token_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `app_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_allowance`
--
ALTER TABLE `book_allowance`
  MODIFY `book_allowance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `book_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  MODIFY `book_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  MODIFY `book_cupboard_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  MODIFY `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_vendors`
--
ALTER TABLE `book_vendors`
  MODIFY `book_vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brochures`
--
ALTER TABLE `brochures`
  MODIFY `brochure_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `caste`
--
ALTER TABLE `caste`
  MODIFY `caste_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `certificate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  MODIFY `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `competition_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `competition_map`
--
ALTER TABLE `competition_map`
  MODIFY `student_competition_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `concession_map`
--
ALTER TABLE `concession_map`
  MODIFY `concession_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  MODIFY `co_scholastic_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document_category`
--
ALTER TABLE `document_category`
  MODIFY `document_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exam_map`
--
ALTER TABLE `exam_map`
  MODIFY `exam_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `exam_marks`
--
ALTER TABLE `exam_marks`
  MODIFY `exam_mark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  MODIFY `exam_schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  MODIFY `fees_st_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  MODIFY `receipt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  MODIFY `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fine`
--
ALTER TABLE `fine`
  MODIFY `fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fine_details`
--
ALTER TABLE `fine_details`
  MODIFY `fine_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  MODIFY `grade_scheme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holiday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  MODIFY `h_conversation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `homework_groups`
--
ALTER TABLE `homework_groups`
  MODIFY `homework_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hostels`
--
ALTER TABLE `hostels`
  MODIFY `hostel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  MODIFY `h_block_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  MODIFY `h_floor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  MODIFY `h_room_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  MODIFY `h_room_cate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostel_student_map`
--
ALTER TABLE `hostel_student_map`
  MODIFY `h_student_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  MODIFY `imprest_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  MODIFY `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `imprest_ac_entries`
--
ALTER TABLE `imprest_ac_entries`
  MODIFY `ac_entry_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `issued_books`
--
ALTER TABLE `issued_books`
  MODIFY `issued_book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `library_fine`
--
ALTER TABLE `library_fine`
  MODIFY `library_fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `library_members`
--
ALTER TABLE `library_members`
  MODIFY `library_member_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  MODIFY `marks_criteria_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `medium_type`
--
ALTER TABLE `medium_type`
  MODIFY `medium_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `nationality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  MODIFY `ot_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `online_notes`
--
ALTER TABLE `online_notes`
  MODIFY `online_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `question_papers`
--
ALTER TABLE `question_papers`
  MODIFY `question_paper_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `reason_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  MODIFY `rc_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  MODIFY `rc_inst_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `religion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `remarks`
--
ALTER TABLE `remarks`
  MODIFY `remark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_no`
--
ALTER TABLE `room_no`
  MODIFY `room_no_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  MODIFY `rte_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rte_heads`
--
ALTER TABLE `rte_heads`
  MODIFY `rte_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  MODIFY `rte_head_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedule_map`
--
ALTER TABLE `schedule_map`
  MODIFY `schedule_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `schedule_room_map`
--
ALTER TABLE `schedule_room_map`
  MODIFY `schedule_room_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `school_boards`
--
ALTER TABLE `school_boards`
  MODIFY `board_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `shift_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  MODIFY `staff_attendance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `staff_attend_details`
--
ALTER TABLE `staff_attend_details`
  MODIFY `staff_attend_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  MODIFY `staff_class_allocation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff_documents`
--
ALTER TABLE `staff_documents`
  MODIFY `staff_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  MODIFY `staff_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `staff_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `streams`
--
ALTER TABLE `streams`
  MODIFY `stream_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  MODIFY `student_academic_history_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  MODIFY `student_academic_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_attendance`
--
ALTER TABLE `student_attendance`
  MODIFY `student_attendance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_attend_details`
--
ALTER TABLE `student_attend_details`
  MODIFY `student_attend_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `student_cheques`
--
ALTER TABLE `student_cheques`
  MODIFY `student_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `student_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_health_info`
--
ALTER TABLE `student_health_info`
  MODIFY `student_health_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_leaves`
--
ALTER TABLE `student_leaves`
  MODIFY `student_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student_parents`
--
ALTER TABLE `student_parents`
  MODIFY `student_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `st_exclude_sub`
--
ALTER TABLE `st_exclude_sub`
  MODIFY `st_exclude_sub_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_parent_groups`
--
ALTER TABLE `st_parent_groups`
  MODIFY `st_parent_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_parent_group_map`
--
ALTER TABLE `st_parent_group_map`
  MODIFY `st_parent_group_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  MODIFY `subject_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  MODIFY `subject_section_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `term_exams`
--
ALTER TABLE `term_exams`
  MODIFY `term_exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `time_tables`
--
ALTER TABLE `time_tables`
  MODIFY `time_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `time_table_map`
--
ALTER TABLE `time_table_map`
  MODIFY `time_table_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  MODIFY `virtual_class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  MODIFY `virtual_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `visitor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  MODIFY `wallet_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  ADD CONSTRAINT `ac_entry_map_ac_entry_id_foreign` FOREIGN KEY (`ac_entry_id`) REFERENCES `imprest_ac_entries` (`ac_entry_id`),
  ADD CONSTRAINT `ac_entry_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `ac_entry_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `ac_entry_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `ac_entry_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `ac_entry_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `ac_entry_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_data`
--
ALTER TABLE `admission_data`
  ADD CONSTRAINT `admission_data_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_data_admission_class_id_foreign` FOREIGN KEY (`admission_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_admission_form_id_foreign` FOREIGN KEY (`admission_form_id`) REFERENCES `admission_forms` (`admission_form_id`),
  ADD CONSTRAINT `admission_data_admission_section_id_foreign` FOREIGN KEY (`admission_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `admission_data_admission_session_id_foreign` FOREIGN KEY (`admission_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_data_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `admission_data_current_class_id_foreign` FOREIGN KEY (`current_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_current_section_id_foreign` FOREIGN KEY (`current_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `admission_data_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_data_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `admission_data_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `admission_data_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `admission_data_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`),
  ADD CONSTRAINT `admission_data_student_permanent_city_foreign` FOREIGN KEY (`student_permanent_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `admission_data_student_permanent_county_foreign` FOREIGN KEY (`student_permanent_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `admission_data_student_permanent_state_foreign` FOREIGN KEY (`student_permanent_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `admission_data_student_sibling_class_id_foreign` FOREIGN KEY (`student_sibling_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_student_temporary_city_foreign` FOREIGN KEY (`student_temporary_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `admission_data_student_temporary_county_foreign` FOREIGN KEY (`student_temporary_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `admission_data_student_temporary_state_foreign` FOREIGN KEY (`student_temporary_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `admission_data_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`),
  ADD CONSTRAINT `admission_data_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_fields`
--
ALTER TABLE `admission_fields`
  ADD CONSTRAINT `admission_fields_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_fields_admission_form_id_foreign` FOREIGN KEY (`admission_form_id`) REFERENCES `admission_forms` (`admission_form_id`),
  ADD CONSTRAINT `admission_fields_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_forms`
--
ALTER TABLE `admission_forms`
  ADD CONSTRAINT `admission_forms_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_forms_brochure_id_foreign` FOREIGN KEY (`brochure_id`) REFERENCES `brochures` (`brochure_id`),
  ADD CONSTRAINT `admission_forms_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_forms_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `books_book_category_id_foreign` FOREIGN KEY (`book_category_id`) REFERENCES `book_categories` (`book_category_id`),
  ADD CONSTRAINT `books_book_cupboard_id_foreign` FOREIGN KEY (`book_cupboard_id`) REFERENCES `book_cupboards` (`book_cupboard_id`),
  ADD CONSTRAINT `books_book_cupboardshelf_id_foreign` FOREIGN KEY (`book_cupboardshelf_id`) REFERENCES `book_cupboardshelfs` (`book_cupboardshelf_id`),
  ADD CONSTRAINT `books_book_vendor_id_foreign` FOREIGN KEY (`book_vendor_id`) REFERENCES `book_vendors` (`book_vendor_id`),
  ADD CONSTRAINT `books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD CONSTRAINT `book_allowance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_allowance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD CONSTRAINT `book_categories_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_categories_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD CONSTRAINT `book_copies_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_copies_info_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `book_copies_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD CONSTRAINT `book_cupboards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_cupboards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD CONSTRAINT `book_cupboardshelfs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_cupboardshelfs_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD CONSTRAINT `book_vendors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_vendors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `brochures`
--
ALTER TABLE `brochures`
  ADD CONSTRAINT `brochures_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `brochures_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `brochures_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `caste`
--
ALTER TABLE `caste`
  ADD CONSTRAINT `caste_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `caste_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `certificates`
--
ALTER TABLE `certificates`
  ADD CONSTRAINT `certificates_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `certificates_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`);

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `classes_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `school_boards` (`board_id`),
  ADD CONSTRAINT `classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD CONSTRAINT `class_subject_staff_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `class_subject_staff_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `class_subject_staff_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `class_subject_staff_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `class_subject_staff_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `competitions`
--
ALTER TABLE `competitions`
  ADD CONSTRAINT `competitions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `competitions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD CONSTRAINT `competition_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `competition_map_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competitions` (`competition_id`),
  ADD CONSTRAINT `competition_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `competition_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD CONSTRAINT `concession_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `concession_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `concession_map_reason_id_foreign` FOREIGN KEY (`reason_id`) REFERENCES `reasons` (`reason_id`),
  ADD CONSTRAINT `concession_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `concession_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `concession_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD CONSTRAINT `co_scholastic_types_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `co_scholastic_types_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
  ADD CONSTRAINT `designations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `designations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `document_category`
--
ALTER TABLE `document_category`
  ADD CONSTRAINT `document_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `document_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exams_term_exam_id_foreign` FOREIGN KEY (`term_exam_id`) REFERENCES `term_exams` (`term_exam_id`),
  ADD CONSTRAINT `exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD CONSTRAINT `exam_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `exam_map_grade_scheme_id_foreign` FOREIGN KEY (`grade_scheme_id`) REFERENCES `grade_schemes` (`grade_scheme_id`),
  ADD CONSTRAINT `exam_map_marks_criteria_id_foreign` FOREIGN KEY (`marks_criteria_id`) REFERENCES `marks_criteria` (`marks_criteria_id`),
  ADD CONSTRAINT `exam_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `exam_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `exam_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_marks`
--
ALTER TABLE `exam_marks`
  ADD CONSTRAINT `exam_marks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_marks_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `exam_marks_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `exam_marks_exam_schedule_id_foreign` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`),
  ADD CONSTRAINT `exam_marks_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `exam_marks_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_marks_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `exam_marks_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `exam_marks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  ADD CONSTRAINT `exam_schedules_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_schedules_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `exam_schedules_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_schedules_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `facilities_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD CONSTRAINT `fees_st_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fees_st_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `fees_st_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `fees_st_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `fees_st_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD CONSTRAINT `fee_receipt_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fee_receipt_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `fee_receipt_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `fee_receipt_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `fee_receipt_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `fee_receipt_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD CONSTRAINT `fee_receipt_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fee_receipt_details_head_inst_id_foreign` FOREIGN KEY (`head_inst_id`) REFERENCES `recurring_inst` (`rc_inst_id`),
  ADD CONSTRAINT `fee_receipt_details_r_concession_map_id_foreign` FOREIGN KEY (`r_concession_map_id`) REFERENCES `concession_map` (`concession_map_id`),
  ADD CONSTRAINT `fee_receipt_details_r_fine_detail_id_foreign` FOREIGN KEY (`r_fine_detail_id`) REFERENCES `fine_details` (`fine_detail_id`),
  ADD CONSTRAINT `fee_receipt_details_r_fine_id_foreign` FOREIGN KEY (`r_fine_id`) REFERENCES `fine` (`fine_id`),
  ADD CONSTRAINT `fee_receipt_details_receipt_id_foreign` FOREIGN KEY (`receipt_id`) REFERENCES `fee_receipt` (`receipt_id`),
  ADD CONSTRAINT `fee_receipt_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fine`
--
ALTER TABLE `fine`
  ADD CONSTRAINT `fine_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fine_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD CONSTRAINT `fine_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fine_details_fine_id_foreign` FOREIGN KEY (`fine_id`) REFERENCES `fine` (`fine_id`),
  ADD CONSTRAINT `fine_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `grades_grade_scheme_id_foreign` FOREIGN KEY (`grade_scheme_id`) REFERENCES `grade_schemes` (`grade_scheme_id`),
  ADD CONSTRAINT `grades_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD CONSTRAINT `grade_schemes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `grade_schemes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `holidays_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `holidays_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `holidays_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  ADD CONSTRAINT `homework_conversations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `homework_conversations_homework_group_id_foreign` FOREIGN KEY (`homework_group_id`) REFERENCES `homework_groups` (`homework_group_id`),
  ADD CONSTRAINT `homework_conversations_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `homework_conversations_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `homework_conversations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD CONSTRAINT `homework_groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `homework_groups_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `homework_groups_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `homework_groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostels`
--
ALTER TABLE `hostels`
  ADD CONSTRAINT `hostels_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostels_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  ADD CONSTRAINT `hostel_blocks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_blocks_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `hostels` (`hostel_id`),
  ADD CONSTRAINT `hostel_blocks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  ADD CONSTRAINT `hostel_floors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_floors_h_block_id_foreign` FOREIGN KEY (`h_block_id`) REFERENCES `hostel_blocks` (`h_block_id`),
  ADD CONSTRAINT `hostel_floors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  ADD CONSTRAINT `hostel_rooms_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_rooms_h_block_id_foreign` FOREIGN KEY (`h_block_id`) REFERENCES `hostel_blocks` (`h_block_id`),
  ADD CONSTRAINT `hostel_rooms_h_floor_id_foreign` FOREIGN KEY (`h_floor_id`) REFERENCES `hostel_floors` (`h_floor_id`),
  ADD CONSTRAINT `hostel_rooms_h_room_cate_id_foreign` FOREIGN KEY (`h_room_cate_id`) REFERENCES `hostel_room_cate` (`h_room_cate_id`),
  ADD CONSTRAINT `hostel_rooms_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `hostels` (`hostel_id`),
  ADD CONSTRAINT `hostel_rooms_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  ADD CONSTRAINT `hostel_room_cate_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_room_cate_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
