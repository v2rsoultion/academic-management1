$(function () {
    //Datetimepicker plugin
    $('.datetimepicker1').bootstrapMaterialDatePicker({
        format: 'DD MMMM YYYY',
        clearButton: false,
        weekStart: 1,
        time: false
    });

    $('.datetimepicker2').bootstrapMaterialDatePicker({
        format: 'DD MMMM YYYY',
        clearButton: false,
        weekStart: 1,
        time: false
    });

    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm A',
        clearButton: true,
        ampm: true, // FOR AM/PM FORMAT
        date: false
    });

    $('.timepicker1').bootstrapMaterialDatePicker({
        format: 'HH:mm A',
        ampm: true, // FOR AM/PM FORMAT
        clearButton: true,
        date: false
    });

});