<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'invoice_config';
    protected $primaryKey = 'invoice_config_id';

    public function up()
    {
        if(!Schema::hasTable('invoice_config')) {
            Schema::create('invoice_config', function (Blueprint $table) {
                $table->increments('invoice_config_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('invoice_prefix',20)->nullable();
                $table->tinyInteger('year')->default(0)->nullable();
                $table->tinyInteger('month')->default(0)->nullable();
                $table->tinyInteger('day')->default(0)->nullable();
                $table->tinyInteger('invoice_type')->nullable();
                $table->timestamps();
            });

            Schema::table('invoice_config', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('invoice_config', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('invoice_config')->insert(
                array(
                    array(
                        'invoice_config_id' => 1,
                        'admin_id'          => 2,
                        'update_by'         => 2,
                        'invoice_prefix'    => 'PI',
                        'year'              => 1,
                        'month'             => 1,
                        'day'               => 1,
                        'invoice_type'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'invoice_config_id' => 2,
                        'admin_id'          => 2,
                        'update_by'         => 2,
                        'invoice_prefix'    => 'PR',
                        'year'              => 1,
                        'month'             => 1,
                        'day'               => 1,
                        'invoice_type'      => 2,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'invoice_config_id' => 3,
                        'admin_id'          => 2,
                        'update_by'         => 2,
                        'invoice_prefix'    => 'SI',
                        'year'              => 1,
                        'month'             => 1,
                        'day'               => 1,
                        'invoice_type'      => 3,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'invoice_config_id' => 4,
                        'admin_id'          => 2,
                        'update_by'         => 2,
                        'invoice_prefix'    => 'SR',
                        'year'              => 1,
                        'month'             => 1,
                        'day'               => 1,
                        'invoice_type'      => 4,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_config');
    }
}
