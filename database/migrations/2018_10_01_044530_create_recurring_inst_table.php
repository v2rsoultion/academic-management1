<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringInstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'recurring_inst';
    protected $primaryKey = 'rc_inst_id';
    public function up()
    {
        if (!Schema::hasTable('recurring_inst')) {
            Schema::create('recurring_inst', function (Blueprint $table) {
                $table->increments('rc_inst_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('rc_head_id')->unsigned()->nullable();
                $table->string('rc_inst_particular_name',255)->nullable();
                $table->double('rc_inst_amount',18, 2)->unsigned()->nullable();
                $table->date('rc_inst_date')->nullable();
                $table->tinyInteger('rc_inst_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('recurring_inst', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('recurring_inst', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('recurring_inst', function($table) {
                $table->foreign('rc_head_id')->references('rc_head_id')->on('recurring_heads');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurring_inst');
    }
}
