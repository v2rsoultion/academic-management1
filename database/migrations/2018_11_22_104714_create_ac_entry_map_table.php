<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcEntryMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'ac_entry_map';
    protected $primaryKey = 'ac_entry_map_id';
    public function up()
    {
        if (!Schema::hasTable('ac_entry_map')) {
            Schema::create('ac_entry_map', function (Blueprint $table) {
                $table->increments('ac_entry_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('ac_entry_id')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('ac_entry_map_amt')->nullable();
                $table->integer('ac_entry_map_amt_paid')->nullable();
                $table->tinyInteger('ac_entry_map_amt_status')->default(1)->comment = '0=Due,1=Paid';
                $table->timestamps();
            });

            Schema::table('ac_entry_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('ac_entry_map', function($table) {
                $table->foreign('ac_entry_id')->references('ac_entry_id')->on('imprest_ac_entries');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ac_entry_map');
    }
}
