<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesStMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'fees_st_map';
    protected $primaryKey = 'fees_st_map_id';
    public function up()
    {
        if (!Schema::hasTable('fees_st_map')) {
            Schema::create('fees_st_map', function (Blueprint $table) {
                $table->increments('fees_st_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('head_id')->unsigned()->nullable();
                $table->text('head_type',255)->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->text('student_ids')->nullable()->comment = 'student ids are stored';
                $table->timestamps();
            });

            Schema::table('fees_st_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('fees_st_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('fees_st_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('fees_st_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('fees_st_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees_st_map');
    }
}
