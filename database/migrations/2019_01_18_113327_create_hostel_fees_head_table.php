<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelFeesHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'hostel_fees_head';
    protected $primaryKey = 'h_fees_head_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_fees_head')) {
            Schema::create('hostel_fees_head', function (Blueprint $table) {
                $table->increments('h_fees_head_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('h_fees_head_name', 255)->nullable();
                $table->double('h_fees_head_price',18, 2)->unsigned()->nullable();
                $table->tinyInteger('h_fees_head_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('hostel_fees_head', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_fees_head', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_fees_head');
    }
}
