<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMarksheets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_marksheet';
    protected $primaryKey = 'student_marksheet_id';
    public function up()
    {
        if (!Schema::hasTable('student_marksheet')) {
            Schema::create('student_marksheet', function (Blueprint $table) {
                $table->increments('student_marksheet_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->double('total_marks',18,2)->nullable();
                $table->double('obtain_marks',18,2)->nullable();
                $table->double('percentage',18,2)->nullable();
                $table->integer('class_rank')->unsigned()->nullable();
                $table->integer('section_rank')->unsigned()->nullable();
                $table->tinyInteger('pass_fail')->default(1)->comment = '0=Fail,1=Pass';
                $table->timestamps();
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_marksheet', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_marksheet');
    }
}
