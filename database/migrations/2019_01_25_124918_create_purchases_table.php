<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "purchases";
    protected $primaryKey = "purchase_id";

    public function up()
    {
        if(!Schema::hasTable('purchases')) {
            Schema::create('purchases', function (Blueprint $table) {
                $table->increments('purchase_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('system_invoice_no',255)->nullable();
                $table->string('ref_invoice_no',255)->nullable();
                $table->date('date')->nullable();
                $table->integer('vendor_id')->unsigned()->nullable();
                $table->integer('category_id')->unsigned()->nullable();
                $table->integer('subcategory_id')->unsigned()->nullable();
                $table->tinyInteger('payment_mode')->nullable()->comment = '0=Cash,1=Credit,2=Cheque';
                $table->integer('bank_id')->unsigned()->nullable();
                $table->integer('cheque_no')->nullable();
                $table->date('cheque_date')->nullable();
                $table->bigInteger('cheque_amount')->nullable();
                $table->double('total_amount',18,2)->nullable();
                $table->double('tax',18,2)->nullable();
                $table->double('discount',18,2)->nullable();
                $table->double('gross_amount',18,2)->nullable();
                $table->double('net_amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('purchases', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('purchases', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('purchases', function($table) {
                $table->foreign('vendor_id')->references('vendor_id')->on('inv_vendor');
            });

            Schema::table('purchases', function($table) {
                $table->foreign('category_id')->references('category_id')->on('inv_category');
            });

            Schema::table('purchases', function($table) {
                $table->foreign('subcategory_id')->references('category_id')->on('inv_category');
            });

            Schema::table('purchases', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
