<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPlanSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_schedule', function (Blueprint $table) {
            $table->integer('current_session_id')->unsigned()->nullable()->after('staff_id');
        });

        Schema::table('plan_schedule', function($table) {
            $table->foreign('current_session_id')->references('session_id')->on('sessions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
