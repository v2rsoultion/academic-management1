<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddStatusStchequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_cheques', function (Blueprint $table) {
            $table->tinyInteger('cheques_for')->default(0)->comment = '0=Academic Fee,1=Hostel Fee';
        });
        Schema::table('hostel_fees', function($table1) {
            $table1->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_cheques', function (Blueprint $table) {
            $table->dropColumn('cheques_for');
        });

        Schema::table('hostel_fees', function (Blueprint $table1) {
            $table1->dropColumn('description');
        });
    }
}
