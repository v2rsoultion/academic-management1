<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccOpenBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'acc_open_balance';
    protected $primaryKey = 'acc_open_balance_id';

    public function up()
    {
        if(!Schema::hasTable('acc_open_balance')) {
            Schema::create('acc_open_balance', function (Blueprint $table) {
                $table->increments('acc_open_balance_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('acc_sub_head_id')->unsigned()->nullable();
                $table->integer('acc_group_head_id')->unsigned()->nullable(); 
                $table->double('balance_amount',18,2)->nullable();
                $table->date('balance_date')->nullable();  
                $table->timestamps();
            });

            Schema::table('acc_open_balance', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
 
            Schema::table('acc_open_balance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('acc_open_balance', function($table) {
                $table->foreign('acc_sub_head_id')->references('acc_sub_head_id')->on('acc_sub_heads');
            });

            Schema::table('acc_open_balance', function($table) {
                $table->foreign('acc_group_head_id')->references('acc_group_head_id')->on('acc_group_heads');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_open_balance');
    }
}
