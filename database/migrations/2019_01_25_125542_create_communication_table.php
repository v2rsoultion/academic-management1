<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'communication_message';
    protected $primaryKey = 'comm_message_id';
    public function up()
    {
        if (!Schema::hasTable('communication_message')) {
            Schema::create('communication_message', function (Blueprint $table) {
                $table->increments('comm_message_id');
                $table->integer('student_parent_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->integer('sender_id')->nullable();
                $table->tinyInteger('sender_type')->comment = '1=Staff,2=Parent';
                $table->text('message')->nullable();
                $table->timestamps();
            });

            Schema::table('communication_message', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });

            Schema::table('communication_message', function($table) {
                $table->foreign('student_parent_id')->references('student_parent_id')->on('student_parents');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication');
    }
}
