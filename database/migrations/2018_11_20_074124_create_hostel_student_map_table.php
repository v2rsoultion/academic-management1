<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelStudentMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'hostel_student_map';
    protected $primaryKey = 'h_student_map_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_student_map')) {
            Schema::create('hostel_student_map', function (Blueprint $table) {
                $table->increments('h_student_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('hostel_id')->unsigned()->nullable();
                $table->integer('h_block_id')->unsigned()->nullable();
                $table->integer('h_floor_id')->unsigned()->nullable();
                $table->integer('h_room_id')->unsigned()->nullable();
                $table->date('join_date')->nullable();
                $table->date('leave_date')->nullable();
                $table->tinyInteger('fees_status')->default(0)->comment = '0=Unpaid,1=paid';
                $table->tinyInteger('leave_status')->default(0)->comment = '0=Stay,1=leave';
                $table->timestamps();
            });

            Schema::table('hostel_student_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('hostel_id')->references('hostel_id')->on('hostels');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('h_block_id')->references('h_block_id')->on('hostel_blocks');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('h_floor_id')->references('h_floor_id')->on('hostel_floors');
            });
            Schema::table('hostel_student_map', function($table) {
                $table->foreign('h_room_id')->references('h_room_id')->on('hostel_rooms');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_student_map');
    }
}
