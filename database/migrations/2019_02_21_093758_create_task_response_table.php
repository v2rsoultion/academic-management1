<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'task_response';
    protected $primaryKey = 'task_response_id';
    public function up()
    {
        if(!Schema::hasTable('task_response')) {
            Schema::create('task_response', function (Blueprint $table) {
                $table->increments('task_response_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('task_id')->unsigned()->nullable();
                $table->string('user_name', 255)->nullable();
                $table->string('response_file', 255)->nullable();
                $table->text('response_text')->nullable();
                $table->timestamps();
            });

            Schema::table('task_response', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('task_response', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('task_response', function($table) {
                $table->foreign('task_id')->references('task_id')->on('tasks')->onDelete('cascade');
            });
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_response');
    }
}
