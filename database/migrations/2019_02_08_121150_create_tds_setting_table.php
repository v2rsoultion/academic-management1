<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'tds_setting';
    protected $primaryKey = 'tds_setting_id';

    public function up()
    {
        if(!Schema::hasTable('tds_setting')) {
            Schema::create('tds_setting', function (Blueprint $table) {
                $table->increments('tds_setting_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->double('salary_min',18,2)->nullable();
                $table->double('salary_max',18,2)->nullable();
                $table->float('salary_deduction',8,2)->nullable();
                $table->tinyInteger('tds_setting_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('tds_setting', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('tds_setting', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('tds_setting', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tds_setting');
    }
}
