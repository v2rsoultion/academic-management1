<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'grades';
    protected $primaryKey = 'grade_id';

    public function up()
    {
        if (!Schema::hasTable('grades')) {
            Schema::create('grades', function (Blueprint $table) {
                $table->increments('grade_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('grade_scheme_id')->unsigned()->nullable();
                $table->integer('grade_min')->unsigned()->nullable();
                $table->integer('grade_max')->unsigned()->nullable();
                $table->string('grade_name',255)->nullable();
                $table->double('grade_point')->nullable();
                $table->text('grade_remark')->nullable();
                $table->timestamps();
            });

            Schema::table('grades', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('grades', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('grades', function($table) {
                $table->foreign('grade_scheme_id')->references('grade_scheme_id')->on('grade_schemes');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
