<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalStructMapStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'sal_struct_map_staff';
    protected $primaryKey = 'sal_struct_map_staff_id';

    public function up()
    {
        if(!Schema::hasTable('sal_struct_map_staff')) {
            Schema::create('sal_struct_map_staff', function (Blueprint $table) {
                $table->increments('sal_struct_map_staff_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->double('basic_pay_amt',18,2)->nullable();
                $table->integer('salary_structure_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('sal_struct_map_staff', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('sal_struct_map_staff', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('sal_struct_map_staff', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('sal_struct_map_staff', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });

            Schema::table('sal_struct_map_staff', function($table) {
                $table->foreign('salary_structure_id')->references('salary_structure_id')->on('salary_structure');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sal_struct_map_staff');
    }
}
