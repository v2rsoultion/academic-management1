<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceConfigTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER update_purchase_counter AFTER INSERT ON `purchases` FOR EACH ROW
            BEGIN
            UPDATE `invoice_config` 
            SET `total_invoice` = total_invoice+1 
            WHERE `invoice_type` = 1;
            END
        ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `update_purchase_counter`');
    }
}
