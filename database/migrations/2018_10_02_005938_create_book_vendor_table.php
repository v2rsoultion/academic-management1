<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_vendors';
    protected $primaryKey = 'book_vendor_id';
    public function up()
    {
        if (!Schema::hasTable('book_vendors')){
            Schema::create('book_vendors', function (Blueprint $table) {
                $table->increments('book_vendor_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('book_vendor_name', 255)->nullable();
                $table->string('book_vendor_number', 15)->nullable();
                $table->string('book_vendor_email', 100)->nullable();
                $table->string('book_vendor_address', 255)->nullable();
                $table->tinyInteger('book_vendor_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('book_vendors', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('book_vendors', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_vendors');
    }
}
