<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprestAcConfiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    protected $table      = 'imprest_ac_confi';
    protected $primaryKey = 'imprest_ac_confi_id';
    public function up()
    {
        if (!Schema::hasTable('imprest_ac_confi')){
            Schema::create('imprest_ac_confi', function (Blueprint $table) {
                $table->increments('imprest_ac_confi_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->double('imprest_ac_confi_amt')->unsigned()->default(0) ;
                $table->timestamps();
            });
            Schema::table('imprest_ac_confi', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins');
                });
            Schema::table('imprest_ac_confi', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('imprest_ac_confi')->insert(
                array(
                    'admin_id'              => 1,
                    'update_by'             => 1,
                    'imprest_ac_confi_amt'  => 0,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imprest_ac_confi');
    }
}
