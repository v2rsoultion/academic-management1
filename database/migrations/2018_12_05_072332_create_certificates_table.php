<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'certificates';
    protected $primaryKey = 'certificate_id';

    public function up()
    {
        if (!Schema::hasTable('certificates')) {
            Schema::create('certificates', function (Blueprint $table) {
                $table->increments('certificate_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('template_name',255)->nullable();
                $table->tinyInteger('template_type')->default(0)->comment = '0=TC,1=CC,2=COMP';
                $table->tinyInteger('template_for')->nullable()->comment = '0=Participation,1=Winner';
                $table->string('template_link',255)->nullable();
                $table->integer('book_no_start')->unsigned()->nullable();
                $table->integer('book_no_end')->unsigned()->nullable();
                $table->integer('serial_no_start')->unsigned()->nullable();
                $table->integer('serial_no_end')->unsigned()->nullable();
                $table->tinyInteger('certificate_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('certificates', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('certificates', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('certificates')->insert(
                array(
                    array(
                        'certificate_id'        => 1,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Participation Certificate',
                        'template_type'         => 2,
                        'template_for'          => 0,
                        'template_link'         => 'participation_template1',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 2,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Winner Certificate',
                        'template_type'         => 2,
                        'template_for'          => 1,
                        'template_link'         => 'winner_template1',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 3,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Character Certificate 1',
                        'template_type'         => 1,
                        'template_for'          => '',
                        'template_link'         => 'character_template1',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 4,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Character Certificate 2',
                        'template_type'         => 1,
                        'template_for'          => '',
                        'template_link'         => 'character_template2',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 5,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Transfer Certificate 1',
                        'template_type'         => 0,
                        'template_for'          => '',
                        'template_link'         => 'transfer_template1',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 6,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Transfer Certificate 2',
                        'template_type'         => 0,
                        'template_for'          => '',
                        'template_link'         => 'transfer_template2',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'certificate_id'        => 7,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Transfer Certificate 3',
                        'template_type'         => 0,
                        'template_for'          => '',
                        'template_link'         => 'transfer_template3',
                        'book_no_start'         => '',
                        'book_no_end'           => '',
                        'serial_no_start'       => '',
                        'serial_no_end'         => '',
                        'certificate_status'    => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
