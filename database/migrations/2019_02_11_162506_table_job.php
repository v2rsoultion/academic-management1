<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'job';
    protected $primaryKey = 'job_id';

    public function up()
    {
        
        Schema::create('job', function (Blueprint $table) {
            $table->increments('job_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->string('job_name',255)->nullable();
            $table->integer('job_medium_type')->unsigned()->nullable();
            $table->integer('job_type')->unsigned()->comment = '0=Temporary,1=Permament';
            $table->integer('job_durations')->unsigned()->nullable();
            $table->integer('job_no_of_vacancy')->unsigned()->nullable();
            $table->text('job_recruitment')->nullable();
            $table->text('job_description')->nullable();
            $table->tinyInteger('job_status')->default(1)->comment = '0=Deactive,1=Active';
            $table->timestamps();
        });
        
        Schema::table('job', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('job', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });

        Schema::table('job', function($table) {
            $table->foreign('job_medium_type')->references('medium_type_id')->on('medium_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job');
    }
}
