<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'hostel_blocks';
    protected $primaryKey = 'h_block_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_blocks')) {
            Schema::create('hostel_blocks', function (Blueprint $table) {
                $table->increments('h_block_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('hostel_id')->unsigned()->nullable();
                $table->string('h_block_name', 255)->nullable();
                $table->tinyInteger('h_block_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('hostel_blocks', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_blocks', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_blocks', function($table) {
                $table->foreign('hostel_id')->references('hostel_id')->on('hostels');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_blocks');
    }
}
