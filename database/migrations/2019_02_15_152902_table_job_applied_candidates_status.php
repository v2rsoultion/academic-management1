<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableJobAppliedCandidatesStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'job_applied_cadidate_status';
    protected $primaryKey = 'applied_cadidate_status_id';

    public function up()
    {
        if (!Schema::hasTable('job_applied_cadidate_status')) {
            Schema::create('job_applied_cadidate_status', function (Blueprint $table) {
                $table->increments('applied_cadidate_status_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('applied_cadidate_id')->unsigned()->nullable();
                $table->integer('job_id')->unsigned()->nullable();
                $table->integer('job_form_id')->unsigned()->nullable();
                $table->dateTime('datetime');
                $table->text('message')->nullable();
                $table->tinyInteger('form_message_via')->default(0)->comment = '0=Email,1=Phone,2=Both';
                $table->tinyInteger('status_type')->default(0)->comment = '1=Schedule,2=Approved,3=Disapproved';
                $table->timestamps();
            });
            
            Schema::table('job_applied_cadidate_status', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('job_applied_cadidate_status', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('job_applied_cadidate_status', function($table) {
                $table->foreign('applied_cadidate_id')->references('applied_cadidate_id')->on('job_applied_cadidates');
            });
            Schema::table('job_applied_cadidate_status', function($table) {
                $table->foreign('job_id')->references('job_id')->on('job');
            });
            Schema::table('job_applied_cadidate_status', function($table) {
                $table->foreign('job_form_id')->references('job_form_id')->on('job_forms');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applied_cadidates');
    }
}
