<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'exam_map';
    protected $primaryKey = 'exam_map_id';

    public function up()
    {
        if (!Schema::hasTable('exam_map')) {
            Schema::create('exam_map', function (Blueprint $table) {
                $table->increments('exam_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('marks_criteria_id')->unsigned()->nullable();
                $table->integer('grade_scheme_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('marks_criteria_id')->references('marks_criteria_id')->on('marks_criteria');
            });
            Schema::table('exam_map', function($table) {
                $table->foreign('grade_scheme_id')->references('grade_scheme_id')->on('grade_schemes');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_map');
    }
}
