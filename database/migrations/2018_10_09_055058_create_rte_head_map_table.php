<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRteHeadMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'rte_head_map';
    protected $primaryKey = 'rte_head_map_id';
    public function up()
    {
        
        if (!Schema::hasTable('rte_head_map')) {
            Schema::create('rte_head_map', function (Blueprint $table) {
                $table->increments('rte_head_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('rte_head_id')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('rte_head_id')->references('rte_head_id')->on('rte_heads');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('rte_head_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rte_head_map');
    }
}
