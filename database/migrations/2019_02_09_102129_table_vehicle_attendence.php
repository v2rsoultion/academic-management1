<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableVehicleAttendence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'vehicle_attendence';
    protected $primaryKey = 'vehicle_attendence_id';

    public function up()
    {
        if (!Schema::hasTable('vehicle_attendence')) {
            Schema::create('vehicle_attendence', function (Blueprint $table) {
                $table->increments('vehicle_attendence_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('vehicle_id')->unsigned()->nullable();
                $table->integer('total_present_staff')->unsigned()->nullable();
                $table->integer('total_present_student')->unsigned()->nullable();
                $table->integer('total_strength')->unsigned()->nullable();
                $table->date('vehicle_attendence_date')->nullable();
                $table->tinyInteger('vehicle_attendence_status')->default(1)->comment = '0=Unmark,1=Mark';
                $table->timestamps();
            });
            Schema::table('vehicle_attendence', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('vehicle_attendence', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('vehicle_attendence', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('vehicle_attendence', function($table) {
                $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_attendence');
    }
}
