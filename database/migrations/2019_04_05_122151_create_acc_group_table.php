<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'acc_group';
    protected $primaryKey = 'acc_group_id';

    public function up()
    {
        if(!Schema::hasTable('acc_group')) {
            Schema::create('acc_group', function (Blueprint $table) {
                $table->increments('acc_group_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('acc_sub_head_id')->unsigned()->nullable();
                $table->string('acc_group')->nullable();  
                $table->tinyInteger('acc_group_status')->default(1)->comment = '0=Deactive,1=Active'; 
                $table->timestamps();
            });

            Schema::table('acc_group', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
 
            Schema::table('acc_group', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('acc_group', function($table) {
                $table->foreign('acc_sub_head_id')->references('acc_sub_head_id')->on('acc_sub_heads');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_group');
    }
}
