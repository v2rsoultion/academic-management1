<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'document_category';
    protected $primaryKey = 'document_category_id';
    public function up()
    {
        if (!Schema::hasTable('document_category')) { 
            Schema::create('document_category', function (Blueprint $table) {
                $table->increments('document_category_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('document_category_name', 255)->nullable();
                $table->tinyInteger('document_category_for')->default(0)->comment = '0=student,1=staff,2=both';
                $table->tinyInteger('document_category_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('document_category', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('document_category', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_category');
    }
}
