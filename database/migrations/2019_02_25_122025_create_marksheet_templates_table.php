<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksheetTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'marksheet_templates';
    protected $primaryKey = 'marksheet_template_id';
    public function up()
    {
        if (!Schema::hasTable('marksheet_templates')) {
            Schema::create('marksheet_templates', function (Blueprint $table) {
                $table->increments('marksheet_template_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('template_name',255)->nullable();
                $table->tinyInteger('template_type')->default(0)->comment = '0=Exam Wise,1=Term Wise,2=Consolidated/Annual Marksheet';
                $table->string('template_link',255)->nullable();
                $table->tinyInteger('template_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('marksheet_templates', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('marksheet_templates', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('marksheet_templates')->insert(
                array(
                    array(
                        'marksheet_template_id' => 1,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Exam Wise T1',
                        'template_type'         => 0,
                        'template_link'         => 'examwise-template1',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'marksheet_template_id' => 2,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Term Wise T1',
                        'template_type'         => 1,
                        'template_link'         => 'termwise-template1',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'marksheet_template_id' => 3,
                        'admin_id'              => 1,
                        'update_by'             => 1,
                        'template_name'         => 'Annual Marksheet T1',
                        'template_type'         => 2,
                        'template_link'         => 'annual-template1',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marksheet_templates');
    }
}
