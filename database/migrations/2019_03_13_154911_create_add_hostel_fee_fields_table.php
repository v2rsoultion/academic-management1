<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddHostelFeeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hostel_fees', function (Blueprint $table) {
            $table->decimal('concession_amount', 10, 2)->default(0);
            $table->decimal('fine_amount', 10, 2)->default(0);
            $table->tinyInteger('cancel_status')->default(0)->comment = '0=No,1=Yes';
        });

        Schema::table('hostel_fee_details', function($table1) {
            $table1->date('month_date')->nullable();
            $table1->tinyInteger('cancel_status')->default(0)->comment = '0=No,1=Yes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hostel_fees', function (Blueprint $table) {
            $table->dropColumn('concession_amount');
            $table->dropColumn('fine_amount');
            $table->dropColumn('cancel_status');
        });
        Schema::table('hostel_fee_details', function (Blueprint $table1) {
            $table1->dropColumn('month_date');
            $table1->dropColumn('cancel_status');
        });
    }
}
