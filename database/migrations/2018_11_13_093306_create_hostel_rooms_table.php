<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'hostel_rooms';
    protected $primaryKey = 'h_room_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_rooms')) {
            Schema::create('hostel_rooms', function (Blueprint $table) {
                $table->increments('h_room_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('hostel_id')->unsigned()->nullable();
                $table->integer('h_block_id')->unsigned()->nullable();
                $table->integer('h_floor_id')->unsigned()->nullable();
                $table->integer('h_room_cate_id')->unsigned()->nullable();
                $table->string('h_room_no', 255)->nullable();
                $table->text('h_room_alias')->nullable();
                $table->integer('h_room_capacity')->unsigned()->nullable();
                $table->tinyInteger('h_room_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('hostel_rooms', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_rooms', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_rooms', function($table) {
                $table->foreign('hostel_id')->references('hostel_id')->on('hostels');
            });
            Schema::table('hostel_rooms', function($table) {
                $table->foreign('h_block_id')->references('h_block_id')->on('hostel_blocks');
            });
            Schema::table('hostel_rooms', function($table) {
                $table->foreign('h_floor_id')->references('h_floor_id')->on('hostel_floors');
            });
            Schema::table('hostel_rooms', function($table) {
                $table->foreign('h_room_cate_id')->references('h_room_cate_id')->on('hostel_room_cate');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_rooms');
    }
}
