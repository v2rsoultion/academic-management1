<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPurchaseItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->nullable()->after('unit_id');
            $table->integer('sub_category_id')->unsigned()->nullable()->after('category_id');
            $table->integer('purchase_type')->unsigned()->nullable()->after('sub_category_id')->comment = '1=Purchase,2=purchase Return';
        });

        Schema::table('purchase_items', function($table) {
            $table->foreign('category_id')->references('category_id')->on('inv_category');
        });


        Schema::table('purchase_items', function($table) {
            $table->foreign('sub_category_id')->references('category_id')->on('inv_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->dropColumn('sub_category_id');
            $table->dropColumn('purchase_type');
        });
    }
}
