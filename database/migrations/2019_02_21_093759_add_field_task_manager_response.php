<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTaskManagerResponse1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'task_response';
    protected $primaryKey = 'task_response_id';

    public function up()
    {
        Schema::dropIfExists('task_response');

        if(!Schema::hasTable('task_response')) {
            Schema::create('task_response', function (Blueprint $table) {
                $table->increments('task_response_id')->unsigned();
                $table->integer('task_id')->unsigned()->nullable();
                $table->integer('sender_type')->unsigned()->nullable()->comment = '0=Admin,1=Student,2=Staff';
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->text('response_text')->nullable();
                $table->text('response_file', 255)->nullable();                
                $table->timestamps();
            });

            Schema::table('task_response', function($table) {
                $table->foreign('task_id')->references('task_id')->on('tasks')->onDelete('cascade');
            });

            Schema::table('task_response', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('task_response', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });

            Schema::table('task_response', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }    

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_response');
    }
}
