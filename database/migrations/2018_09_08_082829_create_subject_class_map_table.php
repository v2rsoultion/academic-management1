<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectClassMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'subject_class_map';
    protected $primaryKey = 'subject_class_map_id';
    public function up()
    {
        if (!Schema::hasTable('subject_class_map')) {
            Schema::create('subject_class_map', function (Blueprint $table) {
                $table->increments('subject_class_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->timestamps();
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('subject_class_map', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_class_map');
    }
}
