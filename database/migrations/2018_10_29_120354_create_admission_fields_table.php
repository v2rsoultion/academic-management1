<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'admission_fields';
    protected $primaryKey = 'admission_field_id';
    public function up()
    {
        if (!Schema::hasTable('admission_fields')) {
            Schema::create('admission_fields', function (Blueprint $table) {
                $table->increments('admission_field_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('admission_form_id')->unsigned()->nullable();
                $table->text('form_keys')->nullable();
                $table->text('form_pay_mode')->nullable();
                $table->timestamps();
            });
            
            Schema::table('admission_fields', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('admission_fields', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('admission_fields', function($table) {
                $table->foreign('admission_form_id')->references('admission_form_id')->on('admission_forms');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_fields');
    }
}
