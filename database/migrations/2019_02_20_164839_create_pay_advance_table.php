<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayAdvanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_advance';
    protected $primaryKey = 'pay_advance_id';

    public function up()
    {
        if(!Schema::hasTable('pay_advance')) {
            Schema::create('pay_advance', function (Blueprint $table) {
                $table->increments('pay_advance_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->double('advance_amount',18,2)->nullable();
                $table->double('suggested_amount',18,2)->nullable();
                $table->double('paid_amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('pay_advance', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_advance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pay_advance', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('pay_advance', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_advance');
    }
}
