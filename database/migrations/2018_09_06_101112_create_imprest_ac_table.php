<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprestAcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'imprest_ac';
    protected $primaryKey = 'imprest_ac_id';
    public function up()
    {
        if (!Schema::hasTable('imprest_ac')) {
            Schema::create('imprest_ac', function (Blueprint $table) {
                $table->increments('imprest_ac_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->string('imprest_ac_name', 255)->nullable();
                $table->double('imprest_ac_amt')->nullable();
                $table->tinyInteger('imprest_ac_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('imprest_ac', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('imprest_ac', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('imprest_ac', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('imprest_ac', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('imprest_ac', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imprest_ac');
    }
}
