<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'staff_roles';
    protected $primaryKey = 'staff_role_id';
    public function up()
    {
        if (!Schema::hasTable('staff_roles')) {
            Schema::create('staff_roles', function (Blueprint $table) {
                $table->increments('staff_role_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->text('staff_role_name',255)->nullable();
                $table->tinyInteger('staff_role_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('staff_roles', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_roles', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('staff_roles')->insert(
                array(
                    array(
                        'staff_role_id'     => 1,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'School Principal',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 2,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Class Teacher',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 3,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Subject Teacher',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 4,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Marks Manager',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 5,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Exam Manager',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 6,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Accountant',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 7,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Hostel Warden',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 8,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'HR Manager',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 9,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Finance Head',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 10,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Admission Officer / Public Relation Officer',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 11,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Guard for Visitor Management',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 12,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Librarian',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 13,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Website Manager',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 14,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Driver',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'staff_role_id'     => 15,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'staff_role_name'   => 'Conductor',
                        'staff_role_status' => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_roles');
    }
}
