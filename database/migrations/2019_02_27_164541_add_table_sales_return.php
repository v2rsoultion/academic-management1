<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSalesReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = "sale_items";
    protected $primaryKey = "sale_items_id";

    public function up()
    {
        if(!Schema::hasTable('sale_items')) {
            Schema::create('sale_items', function (Blueprint $table) {
                $table->increments('sale_items_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('sale_id')->unsigned()->nullable();
                $table->integer('item_id')->unsigned()->nullable();
                $table->integer('unit_id')->unsigned()->nullable();

                $table->integer('category_id')->unsigned()->nullable();
                $table->integer('sub_category_id')->unsigned()->nullable();
                $table->integer('sales_type')->unsigned()->nullable()->comment = '1=Sale,2=sales Return';

                $table->float('rate',8,2)->nullable();
                $table->integer('quantity')->nullable();
                $table->double('amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('sale_items', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('sale_items', function($table) {
                $table->foreign('category_id')->references('category_id')->on('inv_category');
            });


            Schema::table('sale_items', function($table) {
                $table->foreign('sub_category_id')->references('category_id')->on('inv_category');
            });
            
            Schema::table('sale_items', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('sale_items', function($table) {
                $table->foreign('sale_id')->references('sale_id')->on('sales');
            });

            Schema::table('sale_items', function($table) {
                $table->foreign('item_id')->references('item_id')->on('inv_items');
            });

            Schema::table('sale_items', function($table) {
                $table->foreign('unit_id')->references('unit_id')->on('inv_unit');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
}
