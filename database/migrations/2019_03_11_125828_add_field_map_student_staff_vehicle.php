<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldMapStudentStaffVehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('map_student_staff_vehicle', function (Blueprint $table) {
            $table->tinyInteger('student_staff_status')->default(1)->after('student_staff_type')->comment = '0=Deactive,1=Active';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_student_staff_vehicle', function (Blueprint $table) {
            $table->dropColumn('student_staff_status');
        });
    }
}
