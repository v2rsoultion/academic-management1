<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_config';
    protected $primarykey = 'pay_config_id';
    
    public function up()
    {
        if(!Schema::hasTable('pay_config')) {
            Schema::create('pay_config', function (Blueprint $table) {
                $table->increments('pay_config_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('pay_config_name',255)->nullable();
                $table->tinyInteger('pay_config_status')->default(0);
                $table->timestamps();
            });

            Schema::table('pay_config', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_config', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('pay_config')->insert(
                array(
                    array(
                        'pay_config_id'      => 1,
                        'admin_id'           => 1,
                        'update_by'          => 1,
                        'pay_config_name'    => 'Provident Fund',
                        'pay_config_status'  => 0,
                        'created_at'         => date('Y-m-d H:i:s'),
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'pay_config_id'      => 2,
                        'admin_id'           => 1,
                        'update_by'          => 1,
                        'pay_config_name'    => 'Employee State Insurance',
                        'pay_config_status'  => 0,
                        'created_at'         => date('Y-m-d H:i:s'),
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'pay_config_id'      => 3,
                        'admin_id'           => 1,
                        'update_by'          => 1,
                        'pay_config_name'    => 'Tax Deducted at Source',
                        'pay_config_status'  => 0,
                        'created_at'         => date('Y-m-d H:i:s'),
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'pay_config_id'      => 4,
                        'admin_id'           => 1,
                        'update_by'          => 1,
                        'pay_config_name'    => 'Professional Tax',
                        'pay_config_status'  => 0,
                        'created_at'         => date('Y-m-d H:i:s'),
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_config');
    }
}
