<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTaskManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'tasks';
    protected $primaryKey = 'task_id';

    public function up()
    {

        if(Schema::hasTable('tasks')) {
            Schema::table('tasks', function (Blueprint $table) {
                $table->dropColumn('staff_student_id');
                $table->integer('student_id')->unsigned()->nullable()->after('section_id');
                $table->integer('staff_id')->unsigned()->nullable()->after('student_id');
            });

            Schema::table('tasks', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });

            Schema::table('tasks', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
