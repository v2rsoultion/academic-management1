<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeworkConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'homework_conversations';
    protected $primaryKey = 'h_conversation_id';

    public function up()
    {
        if (!Schema::hasTable('homework_conversations')) {

            Schema::create('homework_conversations', function (Blueprint $table) {
                $table->increments('h_conversation_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('homework_group_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->text('h_conversation_text')->nullable();
                $table->timestamps();
            });

            Schema::table('homework_conversations', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('homework_conversations', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('homework_conversations', function($table) {
                $table->foreign('homework_group_id')->references('homework_group_id')->on('homework_groups');
            });
            Schema::table('homework_conversations', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
            Schema::table('homework_conversations', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homework_conversations');
    }
}
