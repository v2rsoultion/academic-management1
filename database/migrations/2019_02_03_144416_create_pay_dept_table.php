<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayDeptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "pay_dept";
    protected $primaryKey = "pay_dept_id";

    public function up()
    {
        if(!Schema::hasTable('pay_dept')) {
            Schema::create('pay_dept', function (Blueprint $table) {
                $table->increments('pay_dept_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('dept_name',255)->nullable();
                $table->text('dept_description')->nullable();
                $table->integer('no_of_staff_mapped')->default(0);
                $table->tinyInteger('dept_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('pay_dept', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_dept', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_dept');
    }
}
