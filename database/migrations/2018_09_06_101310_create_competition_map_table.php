<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'competition_map';
    protected $primaryKey = 'competition_map_id';
    public function up()
    {
        if (!Schema::hasTable('competition_map')) { 
            Schema::create('competition_map', function (Blueprint $table) {
                $table->increments('student_competition_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('competition_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->string('position_rank',255)->nullable(0);
                $table->tinyInteger('issue_certificate')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('competition_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('competition_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('competition_map', function($table) {
                $table->foreign('competition_id')->references('competition_id')->on('competitions');
            });
            Schema::table('competition_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_map');
    }
}
