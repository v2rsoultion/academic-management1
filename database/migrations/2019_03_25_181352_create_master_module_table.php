<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'master_modules';
    protected $primaryKey = 'master_module_id';

    public function up()
    {
        if(!Schema::hasTable('master_modules')) {
            Schema::create('master_modules', function (Blueprint $table) {
                $table->increments('master_module_id');
                $table->string('module_name', 255)->nullable();
                $table->timestamps();
            });

            DB::table('master_modules')->insert(
                array(
                    array(
                        'master_module_id'  => 1,
                        'module_name'       => 'Academics',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 2,
                        'module_name'       => 'Admission',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 3,
                        'module_name'       => 'Student Info System',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 4,
                        'module_name'       => 'Staff Info System',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 5,
                        'module_name'       => 'Examination and Report Card',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 6,
                        'module_name'       => 'Fees Collection',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 7,
                        'module_name'       => 'Communication',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 8,
                        'module_name'       => 'Visitor Management',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 9,
                        'module_name'       => 'Online Content',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 10,
                        'module_name'       => 'Notice Board',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 11,
                        'module_name'       => 'System Utilities',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 12,
                        'module_name'       => 'Task Manager',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 13,
                        'module_name'       => 'Transport',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 14,
                        'module_name'       => 'Hostel',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 15,
                        'module_name'       => 'Library',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 16,
                        'module_name'       => 'Payroll',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 17,
                        'module_name'       => 'Inventory',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 18,
                        'module_name'       => 'Recruitment',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 19,
                        'module_name'       => 'Substitute Management',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'master_module_id'  => 20,
                        'module_name'       => 'Accounts',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_modules');
    }
}
