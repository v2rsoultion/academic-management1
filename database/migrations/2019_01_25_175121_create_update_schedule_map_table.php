<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateScheduleMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule_map', function (Blueprint $table) {
            $table->tinyInteger('publish_status')->default(0)->comment = '0=No,1=Yes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_map', function (Blueprint $table) {
            $table->dropColumn('publish_status');
        });
    }
}
