<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'virtual_classes';
    protected $primaryKey = 'virtual_class_id';
    public function up()
    {
        if (!Schema::hasTable('virtual_classes')) { 
            Schema::create('virtual_classes', function (Blueprint $table) {
                $table->increments('virtual_class_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('virtual_class_name', 255)->nullable();
                $table->text('virtual_class_description')->nullable();
                $table->tinyInteger('virtual_class_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('virtual_classes', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('virtual_classes', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('virtual_classes', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_classes');
    }
}
