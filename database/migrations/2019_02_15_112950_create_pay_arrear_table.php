<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayArrearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_arrear';
    protected $primaryKey = 'pay_arrear_id';

    public function up()
    {
        if(!Schema::hasTable('pay_arrear')) {
            Schema::create('pay_arrear', function (Blueprint $table) {
                $table->increments('pay_arrear_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('arrear_name', 255)->nullable();
                $table->date('arrear_from_date')->nullable();
                $table->date('arrear_to_date')->nullable();
                $table->double('arrear_amount',18,2)->nullable();
                $table->double('arrear_total_amt',18,2)->nullable();
                $table->tinyInteger('round_off')->nullable()->comment = '0=Nearest Rupee,1=Highest Rupee';
                $table->timestamps();
            });

            Schema::table('pay_arrear', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_arrear', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_arrear');
    }
}
