<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTableMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'time_table_map';
    protected $primaryKey = 'time_table_map_id';

    public function up()
    {
        if (!Schema::hasTable('time_table_map')) {
            Schema::create('time_table_map', function (Blueprint $table) {
                $table->increments('time_table_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('time_table_id')->unsigned()->nullable();
                $table->integer('lecture_no')->unsigned()->nullable();
                $table->integer('day')->unsigned()->nullable();
                $table->tinyInteger('lunch_break')->default(0)->comment = '0=No,1=Yes';
                $table->text('start_time')->nullable();
                $table->text('end_time')->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->tinyInteger('expire_status')->default(1)->comment = '0=Expire,1=Running';
                $table->timestamps();
            });

            Schema::table('time_table_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('time_table_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('time_table_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('time_table_map', function($table) {
                $table->foreign('time_table_id')->references('time_table_id')->on('time_tables');
            });
            Schema::table('time_table_map', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
            Schema::table('time_table_map', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_table_map');
    }
}
