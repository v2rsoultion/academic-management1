<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayArrearMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_arrear_map';
    protected $primaryKey = 'pay_arrear_map_id';

    public function up()
    {
        if(!Schema::hasTable('pay_arrear_map')) {
            Schema::create('pay_arrear_map', function (Blueprint $table) {
                $table->increments('pay_arrear_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('pay_arrear_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('pay_arrear_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_arrear_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pay_arrear_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('pay_arrear_map', function($table) {
                $table->foreign('pay_arrear_id')->references('pay_arrear_id')->on('pay_arrear');
            });

            Schema::table('pay_arrear_map', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_arrear_map');
    }
}
