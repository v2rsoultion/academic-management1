<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayLeaveSchemeMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_leave_scheme_map';
    protected $primaryKey = 'pay_leave_scheme_map_id';
    public function up()
    {
        if(!Schema::hasTable('pay_leave_scheme_map')) {
            Schema::create('pay_leave_scheme_map', function (Blueprint $table) {
                $table->increments('pay_leave_scheme_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('pay_leave_scheme_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('pay_leave_scheme_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_leave_scheme_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pay_leave_scheme_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('pay_leave_scheme_map', function($table) {
                $table->foreign('pay_leave_scheme_id')->references('pay_leave_scheme_id')->on('pay_leave_scheme');
            });

            Schema::table('pay_leave_scheme_map', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_leave_scheme_map');
    }
}
