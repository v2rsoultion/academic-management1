<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCCertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'student_cc';
    protected $primaryKey = 'student_cc_id';
    public function up()
    {
        if (!Schema::hasTable('student_cc')) {
            Schema::create('student_cc', function (Blueprint $table) {
                $table->increments('student_cc_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->date('cc_date')->nullable();
                $table->text('cc_reason')->nullable();
                $table->tinyInteger('cc_status')->default(1)->comment = '0=Cancel,1=Approved';
                $table->timestamps();
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_cc', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_cc');
    }
}
