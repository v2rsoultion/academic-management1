<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'salary_structure';
    protected $primaryKey = 'salary_structure_id';

    public function up()
    {
        if(!Schema::hasTable('salary_structure')) {
            Schema::create('salary_structure', function (Blueprint $table) {
                $table->increments('salary_structure_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('sal_structure_name',255)->nullable();
                $table->tinyInteger('round_off')->default(1)->comment = '0=Nearest Rupee,1=Highest Rupee';
                $table->timestamps();
            });

            Schema::table('salary_structure', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('salary_structure', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_structure');
    }
}
