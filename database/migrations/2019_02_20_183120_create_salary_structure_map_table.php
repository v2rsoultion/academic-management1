<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryStructureMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'salary_structure_map';
    protected $primaryKey = 'salary_structure_map_id';

    public function up()
    {
        if(!Schema::hasTable('salary_structure_map')) {
            Schema::create('salary_structure_map', function (Blueprint $table) {
                $table->increments('salary_structure_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('salary_structure_id')->unsigned()->nullable();
                $table->integer('pay_salary_head_id')->unsigned()->nullable();
                $table->float('salary_head_pf',8,2)->nullable();
                $table->float('salary_head_esi',8,2)->nullable();
                $table->timestamps();
            });

            Schema::table('salary_structure_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('salary_structure_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('salary_structure_map', function($table) {
                $table->foreign('salary_structure_id')->references('salary_structure_id')->on('salary_structure');
            });

            Schema::table('salary_structure_map', function($table) {
                $table->foreign('pay_salary_head_id')->references('pay_salary_head_id')->on('pay_salary_head');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_structure_map');
    }
}
