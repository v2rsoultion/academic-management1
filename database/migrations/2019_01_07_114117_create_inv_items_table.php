<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'inv_items';
    protected $primaryKey = 'item_id';
    public function up()
    {
        if(!Schema::hasTable('inv_items')) {
            Schema::create('inv_items', function (Blueprint $table) {
                $table->increments('item_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('category_id')->unsigned()->nullable();
                $table->integer('sub_category_id')->unsigned()->nullable();
                $table->integer('unit_id')->unsigned()->nullable(); 
                $table->string('item_name', 255)->nullable();
                $table->tinyInteger('item_status')->default(1)->comment = '0=Deactive,1=Active';
                
                $table->timestamps();
            });

            Schema::table('inv_items', function($table) {
                $table->foreign('category_id')->references('category_id')->on('inv_category')->onDelete('cascade');
            });

            Schema::table('inv_items', function($table) {
                $table->foreign('sub_category_id')->references('category_id')->on('inv_category')->onDelete('cascade');
            });

            Schema::table('inv_items', function($table) {
                $table->foreign('unit_id')->references('unit_id')->on('inv_unit')->onDelete('cascade');
            });

            Schema::table('inv_items', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('inv_items', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        } 
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_items');
    }
}
