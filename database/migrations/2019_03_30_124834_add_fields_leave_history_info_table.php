<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsLeaveHistoryInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_leave_history_info', function (Blueprint $table) {
            $table->integer('session_id')->unsigned()->nullable()->after('update_by');
            $table->integer('lscheme_period_type')->after('pay_leave_scheme_id')->nullable()->comment = '1=Monthly,2=Yearly';
        });

        Schema::table('staff_leave_history_info', function($table) {
            $table->foreign('session_id')->references('session_id')->on('sessions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_leave_history_info', function (Blueprint $table) {
            $table->dropForeign('staff_leave_history_info_session_id_foreign');
            $table->dropColumn('session_id');
            $table->dropColumn('lscheme_period_type');
        });
    }
}
