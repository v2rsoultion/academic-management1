<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAppliedAttendence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_applied_cadidates', function (Blueprint $table) {
            $table->dateTime('datetime')->after('staff_permanent_pincode');
            $table->text('message')->nullable()->after('datetime');
            $table->tinyInteger('form_message_via')->after('message')->default(0)->comment = '0=Email,1=Phone,2=Both';
            $table->tinyInteger('status_type')->after('form_message_via')->default(0)->comment = '1=Schedule,2=Approved,3=Disapproved';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
