<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccGroupHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'acc_group_heads';
    protected $primaryKey = 'acc_group_head_id';

    public function up()
    {
        if(!Schema::hasTable('acc_group_heads')) {
            Schema::create('acc_group_heads', function (Blueprint $table) {
                $table->increments('acc_group_head_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('acc_sub_head_id')->unsigned()->nullable();
                $table->integer('acc_group_id')->unsigned()->nullable();
                $table->string('acc_group_head')->nullable();  
                $table->tinyInteger('acc_group_head_status')->default(1)->comment = '0=Deactive,1=Active'; 
                $table->timestamps();
            });

            Schema::table('acc_group_heads', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
 
            Schema::table('acc_group_heads', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('acc_group_heads', function($table) {
                $table->foreign('acc_sub_head_id')->references('acc_sub_head_id')->on('acc_sub_heads');
            });

            Schema::table('acc_group_heads', function($table) {
                $table->foreign('acc_group_id')->references('acc_group_id')->on('acc_group');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_group_heads');
    }
}
