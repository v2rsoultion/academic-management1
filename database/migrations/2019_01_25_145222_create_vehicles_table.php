<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'vehicles';
    protected $primaryKey = 'vehicle_id';
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('vehicle_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->string('vehicle_name', 255)->nullable();
            $table->string('vehicle_registration_no', 255)->nullable();
            $table->tinyInteger('vehicle_ownership')->default(1)->comment = '0=Owner,1=Contract';
            $table->integer('vehicle_capacity')->nullable();
            $table->string('contact_person', 255)->nullable();
            $table->string('contact_number', 255)->nullable();
            $table->tinyInteger('vehicle_status')->default(1)->comment = '0=Deactive,1=Active';
            $table->timestamps();
        });

        Schema::table('vehicles', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('vehicles', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
