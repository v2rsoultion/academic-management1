<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePtSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pt_setting';
    protected $primaryKey = 'pt_setting_id';

    public function up()
    {
        if(!Schema::hasTable('pt_setting')) {
            Schema::create('pt_setting', function (Blueprint $table) {
                $table->increments('pt_setting_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->double('salary_min',18,2)->nullable();
                $table->double('salary_max',18,2)->nullable();
                $table->text('monthly_amount')->nullable();
                $table->tinyInteger('pt_setting_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('pt_setting', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pt_setting', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pt_setting', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pt_setting');
    }
}
