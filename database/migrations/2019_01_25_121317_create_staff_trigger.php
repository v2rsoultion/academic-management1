<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER update_staff_counter AFTER INSERT ON `staff` FOR EACH ROW
            BEGIN
            UPDATE `school` 
            SET `school_total_staff` =  (SELECT COUNT(*) FROM `staff` );
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `update_staff_counter`');
    }
}
