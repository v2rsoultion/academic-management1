<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table        = 'inv_unit';
    protected $primaryKey   = 'unit_id';
    public function up()
    {
        if(!Schema::hasTable('inv_unit')) {
            Schema::create('inv_unit', function (Blueprint $table) {
                $table->increments('unit_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('unit_name', 255)->nullable();
                $table->tinyInteger('unit_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('inv_unit', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('inv_unit', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_unit');
    }
}
