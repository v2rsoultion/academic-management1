<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePlanSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'plan_schedule';
    protected $primaryKey = 'plan_schedule_id';

    public function up()
    {
        if (!Schema::hasTable('plan_schedule')) {
            Schema::create('plan_schedule', function (Blueprint $table) {
                $table->increments('plan_schedule_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->text('plan_description')->nullable();
                $table->date('plan_date')->nullable();
                $table->time('plan_time')->nullable();
                $table->tinyInteger('plan_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('plan_schedule', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('plan_schedule', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('plan_schedule', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_schedule');
    }
}
