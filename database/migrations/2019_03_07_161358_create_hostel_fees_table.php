<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'hostel_fees';
    protected $primaryKey = 'hostel_fee_id';
    public function up()
    {
        if (!Schema::hasTable('hostel_fees'))
        {
            Schema::create('hostel_fees', function (Blueprint $table) {
                $table->increments('hostel_fee_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('h_fee_session_id')->unsigned()->nullable();
                $table->integer('h_fee_class_id')->unsigned()->nullable();
                $table->integer('h_fee_student_id')->unsigned()->nullable();
                $table->integer('h_fee_student_map_id')->unsigned()->nullable();
                $table->string('receipt_number');
                $table->date('receipt_date');
                $table->decimal('net_amount', 10, 2)->default(0);
                $table->decimal('total_paid_amount', 10, 2)->default(0);
                $table->decimal('pay_later_amount', 10, 2)->default(0);
                $table->string('pay_mode',255)->nullable();
                $table->integer('bank_id')->unsigned()->nullable();
                $table->date('cheque_date')->nullable()->comment = 'If cheque mode';
                $table->string('cheque_number',255)->nullable()->comment = 'If cheque mode';
                $table->decimal('cheque_amt',10,2)->unsigned()->nullable()->comment = 'If cheque mode';
                $table->string('transaction_id',255)->nullable()->comment = 'If online mode';
                $table->date('transaction_date')->nullable()->comment = 'If online mode';
                $table->tinyInteger('fee_status')->default(1)->comment = '1:Paid, 2:Reverted,3:Refunded';
                $table->timestamps();
            });
            Schema::table('hostel_fees', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_fees', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_fees', function($table) {
                $table->foreign('h_fee_session_id')->references('session_id')->on('sessions');
            });
            Schema::table('hostel_fees', function($table) {
                $table->foreign('h_fee_class_id')->references('class_id')->on('classes');
            });

            Schema::table('hostel_fees', function($table) {
                $table->foreign('h_fee_student_id')->references('student_id')->on('students');
            });
            Schema::table('hostel_fees', function($table) {
                $table->foreign('h_fee_student_map_id')->references('h_student_map_id')->on('hostel_student_map');
            });
            
            Schema::table('hostel_fees', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_fees');
    }
}
