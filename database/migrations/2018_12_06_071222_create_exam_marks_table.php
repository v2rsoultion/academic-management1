<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'exam_marks';
    protected $primaryKey = 'exam_mark_id';

    public function up()
    {
        if (!Schema::hasTable('exam_marks')) {
            Schema::create('exam_marks', function (Blueprint $table) {
                $table->increments('exam_mark_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('exam_schedule_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('marks')->nullable();
                $table->timestamps();
            });

            Schema::table('exam_marks', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('exam_schedule_id')->references('exam_schedule_id')->on('exam_schedules');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('exam_marks', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_marks');
    }
}
