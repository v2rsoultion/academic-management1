<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelRoomCateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'hostel_room_cate';
    protected $primaryKey = 'h_room_cate_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_room_cate')) {
            Schema::create('hostel_room_cate', function (Blueprint $table) {
                $table->increments('h_room_cate_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('h_room_cate_name', 255)->nullable();
                $table->integer('h_room_cate_fees')->unsigned()->nullable();
                $table->text('h_room_cate_des')->nullable();
                $table->tinyInteger('h_room_cate_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('hostel_room_cate', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_room_cate', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_room_cate');
    }
}
