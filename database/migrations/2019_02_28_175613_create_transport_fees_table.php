<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'transport_fees';
    protected $primaryKey = 'transport_fee_id';
    public function up()
    {
        if (!Schema::hasTable('transport_fees'))
        {
            Schema::create('transport_fees', function (Blueprint $table) {
                $table->increments('transport_fee_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('map_student_staff_id')->unsigned()->nullable();
                $table->string('receipt_number');
                $table->date('receipt_date');
                $table->decimal('total_paid_amount', 10, 2)->default(0);
                $table->tinyInteger('fee_status')->default(1)->comment = '1:Paid, 2:Reverted,3:Refunded';
                $table->timestamps();
            });
            Schema::table('transport_fees', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('transport_fees', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('transport_fees', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('transport_fees', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });

            Schema::table('transport_fees', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('transport_fees', function($table) {
                $table->foreign('map_student_staff_id')->references('map_student_staff_id')->on('map_student_staff_vehicle');
            });
           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_fees');
    }
}
