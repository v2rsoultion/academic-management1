<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'app_settings';
    protected $primaryKey = 'app_setting_id';

    public function up()
    {
        if (!Schema::hasTable('app_settings')) {
            Schema::create('app_settings', function (Blueprint $table) {
                $table->increments('app_setting_id');
                $table->string('android_app_version',255)->nullable();
                $table->string('ios_app_version',255)->nullable();
                $table->tinyInteger('update_mandatory')->default(0)->comment = '0=No,1=Yes';
                $table->text('comapny_page_url')->nullable();
                $table->text('privacy_policy_url')->nullable();
                $table->text('terms_condition_url')->nullable();
                $table->timestamps();
            });

            DB::table('app_settings')->insert(
                array(
                    array(
                        'comapny_page_url'       => 'http://www.academiceye.com/company.php',
                        'privacy_policy_url'     => 'http://www.academiceye.com/privacy-policy.php',
                        'terms_condition_url'    => 'http://www.academiceye.com/terms-condition.php',
                        'created_at'       => date('Y-m-d H:i:s'),
                        'updated_at'       => date('Y-m-d H:i:s'),
                    ),
                    
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_settings');
    }
}
