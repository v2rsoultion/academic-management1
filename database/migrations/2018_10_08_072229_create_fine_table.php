<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'fine';
    protected $primaryKey = 'fine_id';
    public function up()
    {
        if (!Schema::hasTable('fine')) {
            Schema::create('fine', function (Blueprint $table) {
                $table->increments('fine_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('fine_name',255)->nullable();
                $table->tinyInteger('fine_for')->default(0)->comment = '0=One Time Fees,1=Recurring Fees';
                $table->integer('head_id')->unsigned()->nullable();
                $table->tinyInteger('fine_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('fine', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('fine', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fine');
    }
}
