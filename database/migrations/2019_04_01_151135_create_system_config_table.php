<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'system_config';
    protected $primaryKey = 'system_config_id';

    public function up()
    {
        if(!Schema::hasTable('system_config')) {
            Schema::create('system_config', function (Blueprint $table) {
                $table->increments('system_config_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('email')->nullable();
                $table->string('password');
                $table->text('sms_gateway_url')->nullable();
                $table->string('sms_username')->nullable();
                $table->string('sms_password');
                $table->string('sms_sender')->nullable();
                $table->string('sms_priority')->nullable();
                $table->string('sms_stype')->nullable();
                $table->timestamps();
            });

            Schema::table('system_config', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('system_config', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('system_config')->insert(
                array(
                    'system_config_id'  => 1,
                    'admin_id'          => 1,
                    'update_by'         => 1,
                    'email'             => 'v2rteam@gmail.com',
                    'password'          => 'v2r@987@7q12t!',
                    'sms_gateway_url'   => 'http://bulksms.wscubetech.com/api/sendmsg.php',
                    'sms_username'      => 'DEMACD',
                    'sms_password'      => 'demacd@123!',
                    'sms_sender'        => 'DEMACD',
                    'sms_priority'      => 'ndnd',
                    'sms_stype'         => 'normal',
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                )
            );
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_config');
    }
}
