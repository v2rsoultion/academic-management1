<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStParentGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'st_parent_groups';
    protected $primaryKey = 'st_parent_group_id';

    public function up()
    {
        if (!Schema::hasTable('st_parent_groups')) {
            Schema::create('st_parent_groups', function (Blueprint $table) {
                $table->increments('st_parent_group_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('group_name', 255)->nullable();
                $table->text('description')->nullable();
                $table->tinyInteger('flag')->default(0)->comment = '0=student,1=parent';
                $table->tinyInteger('status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('st_parent_groups', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('st_parent_groups', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_parent_groups');
    }
}
