<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffLeaveHistoryInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'staff_leave_history_info';
    protected $primaryKey = 'staff_leave_history_info_id';

    public function up()
    {
        if(!Schema::hasTable('staff_leave_history_info')) {
            Schema::create('staff_leave_history_info', function (Blueprint $table) {
                $table->increments('staff_leave_history_info_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->integer('pay_leave_scheme_id')->unsigned()->nullable();
                $table->integer('total_no_of_leaves')->nullable();
                $table->integer('remaining_leaves')->nullable();
                $table->timestamps();
            });

            Schema::table('staff_leave_history_info', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_leave_history_info', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('staff_leave_history_info', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
            Schema::table('staff_leave_history_info', function($table) {
                $table->foreign('pay_leave_scheme_id')->references('pay_leave_scheme_id')->on('pay_leave_scheme');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_leave_history_info');
    }
}
