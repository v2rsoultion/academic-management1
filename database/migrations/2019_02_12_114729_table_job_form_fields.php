<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableJobFormFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'job_form_fields';
    protected $primaryKey = 'job_form_field_id';

    public function up()
    {
        if (!Schema::hasTable('job_form_fields')) {
            Schema::create('job_form_fields', function (Blueprint $table) {
                $table->increments('job_form_field_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('job_form_id')->unsigned()->nullable();
                $table->text('form_keys')->nullable();
                $table->timestamps();
            });
            
            Schema::table('job_form_fields', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('job_form_fields', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('job_form_fields', function($table) {
                $table->foreign('job_form_id')->references('job_form_id')->on('job_forms');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_form_fields');
    }

}
