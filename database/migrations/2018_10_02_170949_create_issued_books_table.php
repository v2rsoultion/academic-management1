<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'issued_books';
    protected $primaryKey = 'issued_book_id';
    public function up()
    {
        if (!Schema::hasTable('issued_books')){ 
            Schema::create('issued_books', function (Blueprint $table) {
                $table->increments('issued_book_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('book_id')->unsigned()->nullable();
                $table->integer('member_id')->unsigned()->nullable();
                $table->integer('member_type')->unsigned()->nullable();
                $table->integer('book_copies_id')->unsigned()->nullable();
                $table->date('issued_from_date')->nullable();
                $table->date('issued_to_date')->nullable();
                $table->tinyInteger('issued_book_status')->default(1)->comment = '1=issued,2=returned';
                $table->timestamps();
            });

            Schema::table('issued_books', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('issued_books', function($table) {
                    $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('issued_books', function($table) {
                    $table->foreign('book_id')->references('book_id')->on('books');
            });
            Schema::table('issued_books', function($table) {
                $table->foreign('book_copies_id')->references('book_info_id')->on('book_copies_info');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_books');
    }
}
