<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediumTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'medium_type';
    protected $primaryKey = 'medium_type_id';
    public function up()
    {
        if (!Schema::hasTable('medium_type')) {
            Schema::create('medium_type', function (Blueprint $table) {
                $table->increments('medium_type_id');
                $table->string('medium_type', 255);
                $table->tinyInteger('medium_type_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medium_type');
    }
}
