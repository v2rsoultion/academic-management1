<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvConfigSalesTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER update_sales_counter AFTER INSERT ON `sales` FOR EACH ROW
            BEGIN
            UPDATE `invoice_config` 
            SET `total_invoice` = total_invoice+1 
            WHERE `invoice_type` = 3;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `update_sales_counter`');
    }
}
