<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAttendDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('staff_attend_details')) {
            Schema::create('staff_attend_details', function (Blueprint $table) {
                $table->increments('staff_attend_d_id');
                $table->integer('staff_attendance_id')->unsigned()->nullable();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->time('in_time')->nullable();
                $table->time('out_time')->nullable();
                $table->tinyInteger('staff_attendance_type')->nullable()->comment = '0=Absent,1=Present,2=Half Day';
                $table->tinyInteger('staff_overtime_type')->nullable()->comment = '0=Hours,1=Lectures';
                $table->double('staff_overtime_value',18, 2)->nullable();
                $table->date('staff_attendance_date')->nullable();
                $table->tinyInteger('staff_attendance_status')->default(1)->comment = '0=Unmark,1=Mark';
                $table->timestamps();
            });
            Schema::table('staff_attend_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_attend_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('staff_attend_details', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('staff_attend_details', function($table) {
                $table->foreign('staff_attendance_id')->references('staff_attendance_id')->on('staff_attendance');
            });
            Schema::table('staff_attend_details', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_attend_details');
    }
}
