<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_loan';
    protected $primaryKey = 'pay_loan_id';

    public function up()
    {
        if(!Schema::hasTable('pay_loan')) {
            Schema::create('pay_loan', function (Blueprint $table) {
                $table->increments('pay_loan_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->double('loan_principal',18,2)->nullable();
                $table->date('loan_from_date')->nullable();
                $table->date('loan_to_date')->nullable();
                $table->float('loan_inrterest_rate',8,2)->nullable();
                $table->double('loan_deduct_amount',18,2)->nullable();
                $table->double('loan_total_amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('pay_loan', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_loan', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pay_loan', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('pay_loan', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_loan');
    }
}
