<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleRoomMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'schedule_room_map';
    protected $primaryKey = 'schedule_room_map_id';

    public function up()
    {
        if (!Schema::hasTable('schedule_room_map')) {
            Schema::create('schedule_room_map', function (Blueprint $table) {
                $table->increments('schedule_room_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('exam_schedule_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('room_no_id')->unsigned()->nullable();
                $table->text('roll_no_from')->nullable();
                $table->text('roll_no_to')->nullable();
                $table->timestamps();
            });

            Schema::table('schedule_room_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('exam_schedule_id')->references('exam_schedule_id')->on('exam_schedules');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('schedule_room_map', function($table) {
                $table->foreign('room_no_id')->references('room_no_id')->on('room_no');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_room_map');
    }
}
