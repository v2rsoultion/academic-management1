<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'routes';
    protected $primaryKey = 'route_id';
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('route_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->string('route_name', 255)->nullable();
            $table->tinyInteger('route_location')->default(1)->comment = '1=Pickup,2=Drop';
            $table->text('route_description')->nullable();
            $table->tinyInteger('route_status')->default(1)->comment = '0=Deactive,1=Active';
            $table->timestamps();
        });

        Schema::table('routes', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('routes', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
