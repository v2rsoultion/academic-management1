<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsStaffLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
            Schema::table('staff_leaves', function (Blueprint $table) {
                $table->integer('pay_leave_scheme_id')->unsigned()->nullable();
                $table->tinyInteger('pay_leaves_status')->default(1)->comment= '1=paid leave,2=loss of pay leave';
            });

            Schema::table('staff_leaves', function($table) {
                $table->foreign('pay_leave_scheme_id')->references('pay_leave_scheme_id')->on('pay_leave_scheme');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_leaves', function (Blueprint $table) {
            $table->dropColumn('pay_leave_scheme_id');
            $table->dropColumn('pay_leaves_status');
        });
    }
}
