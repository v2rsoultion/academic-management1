<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookAllowanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_allowance';
    protected $primaryKey = 'book_allowance_id';
    public function up()
    {
        if (!Schema::hasTable('book_allowance')){
            Schema::create('book_allowance', function (Blueprint $table) {
                $table->increments('book_allowance_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('allow_for_staff')->unsigned()->default(0) ;
                $table->integer('allow_for_student')->unsigned()->default(0) ;
                $table->timestamps();
            });
            Schema::table('book_allowance', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins');
                });
            Schema::table('book_allowance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('book_allowance')->insert(
                array(
                    'admin_id'          => 1,
                    'update_by'         => 1,
                    'allow_for_staff'   => 0,
                    'allow_for_student' => 0,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_allowance');
    }
}
