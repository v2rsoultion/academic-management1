<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'fee_receipt';
    protected $primaryKey = 'fee_receipt_id';
    public function up()
    {
        if (!Schema::hasTable('fee_receipt')) {
            Schema::create('fee_receipt', function (Blueprint $table) {
                $table->increments('receipt_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->string('receipt_number',255)->nullable();
                $table->date('receipt_date')->nullable();
                $table->decimal('total_amount',10,2)->unsigned()->nullable()->comment = 'Total receipt amount';
                $table->decimal('wallet_amount',10,2)->unsigned()->nullable()->comment = 'Wallet amount';
                $table->decimal('total_concession_amount',10,2)->unsigned()->nullable()->comment = 'Total Concession amount';
                $table->decimal('total_fine_amount',10,2)->unsigned()->nullable()->comment = 'Total Fine amount';
                $table->decimal('total_calculate_fine_amount',10,2)->unsigned()->nullable()->comment = 'Total Calculate Fine amount';
                $table->decimal('net_amount',10,2)->unsigned()->nullable()->comment = 'Total net amount';
                $table->decimal('pay_later_amount',10,2)->unsigned()->nullable()->comment = 'Pay later amount';
                $table->decimal('imprest_amount',10,2)->unsigned()->nullable()->comment = 'Imprest amount';
                $table->integer('bank_id')->unsigned()->nullable();
                $table->string('pay_mode',255)->nullable();
                $table->date('cheque_date')->nullable()->comment = 'If cheque mode';
                $table->string('cheque_number',255)->nullable()->comment = 'If cheque mode';
                $table->decimal('cheque_amt',10,2)->unsigned()->nullable()->comment = 'If cheque mode';
                $table->string('transaction_id',255)->nullable()->comment = 'If online mode';
                $table->date('transaction_date')->nullable()->comment = 'If online mode';
                $table->string('description')->nullable();
                $table->tinyInteger('receipt_status')->default(1)->comment = '0=Cancel,1=Approve';
                $table->timestamps();
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('fee_receipt', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_receipt');
    }
}
