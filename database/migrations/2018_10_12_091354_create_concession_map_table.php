<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcessionMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'concession_map';
    protected $primaryKey = 'concession_map_id';
    public function up()
    {
        if (!Schema::hasTable('concession_map')) {
            Schema::create('concession_map', function (Blueprint $table) {
                $table->increments('concession_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('head_id')->unsigned()->nullable();
                $table->integer('reason_id')->unsigned()->nullable();
                $table->text('head_type',255)->nullable();
                $table->integer('concession_amt')->unsigned()->nullable();
                $table->tinyInteger('concession_status')->default(0)->comment = '0=Unpaid,1=Paid';
                $table->timestamps();
            });

            Schema::table('concession_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('concession_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('concession_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('concession_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('concession_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('concession_map', function($table) {
                $table->foreign('reason_id')->references('reason_id')->on('reasons');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concession_map');
    }
}
