<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = "purchase_items";
    protected $primaryKey = "purchase_items_id";
    public function up()
    {
        if(!Schema::hasTable('purchase_items')) {
            Schema::create('purchase_items', function (Blueprint $table) {
                $table->increments('purchase_items_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('purchase_id')->unsigned()->nullable();
                $table->integer('item_id')->unsigned()->nullable();
                $table->integer('unit_id')->unsigned()->nullable();
                $table->float('rate',8,2)->nullable();
                $table->integer('quantity')->nullable();
                $table->double('amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('purchase_items', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('purchase_items', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('purchase_items', function($table) {
                $table->foreign('purchase_id')->references('purchase_id')->on('purchases');
            });

            Schema::table('purchase_items', function($table) {
                $table->foreign('item_id')->references('item_id')->on('inv_items');
            });

            Schema::table('purchase_items', function($table) {
                $table->foreign('unit_id')->references('unit_id')->on('inv_unit');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
