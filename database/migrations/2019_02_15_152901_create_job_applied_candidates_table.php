<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAppliedCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'job_applied_cadidates';
    protected $primaryKey = 'applied_cadidate_id';

    public function up()
    {
        if (!Schema::hasTable('job_applied_cadidates')) {
            Schema::create('job_applied_cadidates', function (Blueprint $table) {
                $table->increments('applied_cadidate_id');
                $table->integer('job_id')->unsigned()->nullable();
                $table->integer('job_form_id')->unsigned()->nullable();
                $table->string('staff_name', 255)->nullable();
                $table->text('staff_profile_img')->nullable();
                $table->text('staff_aadhar')->nullable();
                $table->text('staff_UAN')->nullable();
                $table->text('staff_ESIN')->nullable();
                $table->text('staff_PAN')->nullable();
                $table->integer('designation_id')->unsigned()->nullable();
                $table->string('staff_email', 255)->nullable();
                $table->string('staff_mobile_number', 255)->nullable();
                $table->date('staff_dob')->nullable();
                $table->tinyInteger('staff_gender')->default(0)->comment = '0=Male,1=Female';
                $table->string('staff_father_name_husband_name')->nullable();
                $table->string('staff_father_husband_mobile_no',20)->nullable();
                $table->string('staff_mother_name', 255)->nullable();
                $table->string('staff_blood_group',255)->nullable();
                $table->tinyInteger('staff_marital_status')->default(0)->comment = '0=Unmarried,1=Married';
                $table->integer('title_id')->unsigned()->nullable();
                $table->integer('caste_id')->unsigned()->nullable();
                $table->integer('nationality_id')->unsigned()->nullable();
                $table->integer('religion_id')->unsigned()->nullable();
                $table->text('staff_qualification')->nullable()->comment = 'Qualifications are store with comma';
                $table->text('staff_specialization')->nullable()->comment = 'Specializations are store with comma';
                $table->text('staff_reference')->nullable();
                $table->text('staff_experience')->nullable();
                $table->text('staff_temporary_address')->nullable();
                $table->integer('staff_temporary_city')->unsigned()->nullable();
                $table->integer('staff_temporary_state')->unsigned()->nullable();
                $table->integer('staff_temporary_county')->unsigned()->nullable();
                $table->string('staff_temporary_pincode', 20)->nullable();
                $table->text('staff_permanent_address')->nullable();
                $table->integer('staff_permanent_city')->unsigned()->nullable();
                $table->integer('staff_permanent_state')->unsigned()->nullable();
                $table->integer('staff_permanent_county')->unsigned()->nullable();
                $table->string('staff_permanent_pincode', 20)->nullable();
                $table->tinyInteger('staff_status')->default(1)->comment = '0=Leaved,1=Studying';
                $table->timestamps();
            });

            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('job_id')->references('job_id')->on('job');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('job_form_id')->references('job_form_id')->on('job_forms');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('designation_id')->references('designation_id')->on('designations');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('title_id')->references('title_id')->on('titles');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('caste_id')->references('caste_id')->on('caste');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('religion_id')->references('religion_id')->on('religions');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('nationality_id')->references('nationality_id')->on('nationality');
            });

            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_temporary_county')->references('country_id')->on('country');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_temporary_state')->references('state_id')->on('state');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_temporary_city')->references('city_id')->on('city');
            });

            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_permanent_county')->references('country_id')->on('country');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_permanent_state')->references('state_id')->on('state');
            });
            Schema::table('job_applied_cadidates', function($table) {
                $table->foreign('staff_permanent_city')->references('city_id')->on('city');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applied_cadidates');
    }
}
