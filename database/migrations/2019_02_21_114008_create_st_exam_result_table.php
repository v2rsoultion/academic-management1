<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStExamResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'st_exam_result';
    protected $primaryKey = 'st_exam_result_id';
    public function up()
    {
        if (!Schema::hasTable('st_exam_result')) {
            Schema::create('st_exam_result', function (Blueprint $table) {
                $table->increments('st_exam_result_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->date('result_date')->nullable();
                $table->time('result_time')->nullable();
                $table->timestamps();
            });
            Schema::table('st_exam_result', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('st_exam_result', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('st_exam_result', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('st_exam_result', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams');
            });
            Schema::table('st_exam_result', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
          
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_exam_result');
    }
}
