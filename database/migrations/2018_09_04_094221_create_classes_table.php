<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'classes';
    protected $primaryKey = 'class_id';
    public function up()
    {
        if (!Schema::hasTable('classes')) { 
            Schema::create('classes', function (Blueprint $table) {
                $table->increments('class_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('class_name', 255)->nullable();
                $table->integer('class_order')->unsigned()->default(0);
                $table->integer('board_id')->unsigned()->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->tinyInteger('class_stream')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('class_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('classes', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('classes', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('classes', function($table) {
                $table->foreign('board_id')->references('board_id')->on('school_boards');
            });
            Schema::table('classes', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
