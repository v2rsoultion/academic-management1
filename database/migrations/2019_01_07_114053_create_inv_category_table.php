<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'inv_category';
    protected $primaryKey = 'category_id';
    public function up()
    {
        if(!Schema::hasTable('inv_category')) {
            Schema::create('inv_category', function (Blueprint $table) {
                $table->increments('category_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('category_level')->default(0)->nullable();
                $table->string('category_name', 255)->nullable();
                $table->tinyInteger('category_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('inv_category', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
 
            Schema::table('inv_category', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_category');
    }
}
