<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEsiSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'esi_setting';
    protected $primaryKey = 'esi_setting_id';
    
    public function up()
    {
        if(!Schema::hasTable('esi_setting')) {
            Schema::create('esi_setting', function (Blueprint $table) {
                $table->increments('esi_setting_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->float('employee',8,2)->nullable();
                $table->float('employer',8,2)->nullable();
                $table->double('esi_cut_off',18,2)->nullable();
                $table->tinyInteger('round_off')->nullable()->comment = '0=Nearest Rupee,1=Highest Rupee';
                $table->tinyInteger('esi_setting_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('esi_setting', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('esi_setting', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('esi_setting', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esi_setting');
    }
}
