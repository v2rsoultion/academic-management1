<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccSubHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'acc_sub_heads';
    protected $primaryKey = 'acc_sub_head_id';
    public function up()
    {
        if(!Schema::hasTable('acc_sub_heads')) {
            Schema::create('acc_sub_heads', function (Blueprint $table) {
                $table->increments('acc_sub_head_id');
                $table->integer('acc_main_head_id')->unsigned()->nullable();
                $table->string('acc_sub_head')->nullable();
                $table->tinyInteger('acc_sub_head_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('acc_sub_heads', function($table) {
                $table->foreign('acc_main_head_id')->references('acc_main_head_id')->on('acc_main_heads');
            });

            DB::table('acc_sub_heads')->insert(
                array(
                    array(
                        'acc_sub_head_id'       => 1,
                        'acc_main_head_id'      => 1,
                        'acc_sub_head'          => 'Fixed Assets',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 2,
                        'acc_main_head_id'      => 1,
                        'acc_sub_head'          => 'Current Assets',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 3,
                        'acc_main_head_id'      => 2,
                        'acc_sub_head'          => 'Long-term Liability',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 4,
                        'acc_main_head_id'      => 2,
                        'acc_sub_head'          => 'Current Liability',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 5,
                        'acc_main_head_id'      => 3,
                        'acc_sub_head'          => 'Direct Income',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 6,
                        'acc_main_head_id'      => 3,
                        'acc_sub_head'          => 'Indirect Income',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 7,
                        'acc_main_head_id'      => 4,
                        'acc_sub_head'          => 'Direct Expenses',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_sub_head_id'       => 8,
                        'acc_main_head_id'      => 4,
                        'acc_sub_head'          => 'Indirect Expenses',
                        'acc_sub_head_status'   => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_sub_heads');
    }
}
