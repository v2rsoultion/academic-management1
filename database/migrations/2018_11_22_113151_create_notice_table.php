<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'notice';
    protected $primaryKey = 'notice_id';
    public function up()
    {
        if (!Schema::hasTable('notice')) {
            Schema::create('notice', function (Blueprint $table) {
                $table->increments('notice_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('notice_name',255)->nullable();
                $table->text('notice_text')->nullable();
                $table->text('class_ids')->nullable();
                $table->text('staff_ids')->nullable();
                $table->tinyInteger('for_class')->nullable()->comment = '0=All,1=Selected';
                $table->tinyInteger('for_staff')->nullable()->comment = '0=All,1=Selected';
                $table->tinyInteger('notice_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('notice', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('notice', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('notice', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice');
    }
}
