<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'notifications';
    protected $primaryKey = 'notification_id';

    public function up()
    {
        if (!Schema::hasTable('notifications')) {
            Schema::create('notifications', function (Blueprint $table) {
                $table->increments('notification_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->tinyInteger('notification_via')->nullable()->comment = '0=Normal,1=Topic,2=All Students,3=All Staff';
                $table->tinyInteger('topic_type')->nullable()->comment = '1=Student,1=Parent';
                $table->text('notification_type',255)->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('module_id')->unsigned()->nullable();
                $table->integer('student_admin_id')->unsigned()->nullable();
                $table->integer('parent_admin_id')->unsigned()->nullable();
                $table->integer('staff_admin_id')->unsigned()->nullable();
                $table->integer('school_admin_id')->unsigned()->nullable();
                $table->text('notification_text')->nullable();
                $table->timestamps();
            });

            Schema::table('notifications', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('student_admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('parent_admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('staff_admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('notifications', function($table) {
                $table->foreign('school_admin_id')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
