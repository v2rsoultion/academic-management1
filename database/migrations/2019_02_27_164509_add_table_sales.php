<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "sales";
    protected $primaryKey = "sale_id";

    public function up()
    {
        if(!Schema::hasTable('sales')) {
            Schema::create('sales', function (Blueprint $table) {
                $table->increments('sale_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('system_invoice_no',255)->nullable();
                $table->string('ref_invoice_no',255)->nullable();
                $table->date('date')->nullable();
                $table->tinyInteger('sale_type')->nullable()->comment = '1=Student,2=Staff';
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('role_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->tinyInteger('payment_mode')->nullable()->comment = '0=Cash,1=Credit,2=Cheque';
                $table->integer('bank_id')->unsigned()->nullable();
                $table->integer('cheque_no')->nullable();
                $table->date('cheque_date')->nullable();
                $table->bigInteger('cheque_amount')->nullable();
                $table->double('total_amount',18,2)->nullable();
                $table->double('tax',18,2)->nullable();
                $table->double('discount',18,2)->nullable();
                $table->double('gross_amount',18,2)->nullable();
                $table->double('net_amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('sales', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('sales', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('sales', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });

            Schema::table('sales', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });

            Schema::table('sales', function($table) {
                $table->foreign('role_id')->references('staff_role_id')->on('staff_roles');
            });

            Schema::table('sales', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });

            Schema::table('sales', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
