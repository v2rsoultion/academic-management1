<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'transport_fees_head';
    protected $primaryKey = 'fees_head_id';
    public function up()
    {
        Schema::create('transport_fees_head', function (Blueprint $table) {
            $table->increments('fees_head_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->string('fees_head_name', 255)->nullable();
            $table->string('fees_head_km_from', 255)->nullable();
            $table->string('fees_head_km_to', 255)->nullable();
            $table->string('fees_head_amount', 255)->nullable()->comment = 'per month';
            $table->tinyInteger('fees_head_status')->default(1)->comment = '0=Deactive,1=Active';
            $table->timestamps();
        });

        Schema::table('transport_fees_head', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
        });

        Schema::table('transport_fees_head', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_fees_head');
    }
}
