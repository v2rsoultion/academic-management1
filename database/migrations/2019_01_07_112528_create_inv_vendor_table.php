<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'inv_vendor';
    protected $primaryKey = 'vendor_id';
    public function up()
    {
        if(!Schema::hasTable('inv_vendor')) {
            Schema::create('inv_vendor', function (Blueprint $table) {
                $table->increments('vendor_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('vendor_name', 255)->nullable();
                $table->string('vendor_gstin', 255)->nullable();
                $table->string('vendor_contact_no', 255)->nullable();
                $table->string('vendor_email', 255)->nullable();
                $table->string('vendor_address', 255)->nullable();
                $table->tinyInteger('vendor_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('inv_vendor', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('inv_vendor', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_vendor');
    }
}

