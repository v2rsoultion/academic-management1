<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffClassAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'staff_class_allocations';
    protected $primaryKey = 'staff_class_allocation_id';
    public function up()
    {
        if (!Schema::hasTable('staff_class_allocations')) {
            Schema::create('staff_class_allocations', function (Blueprint $table) {
                $table->increments('staff_class_allocation_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->timestamps();
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_class_allocations');
    }
}
