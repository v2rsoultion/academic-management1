<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFieldPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_modules', function (Blueprint $table) {
            $table->tinyInteger('mandatory')->default(0)->comment= '0=No, 1=yes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_modules', function (Blueprint $table) {
            $table->dropColumn('mandatory');
        });
    }
}
