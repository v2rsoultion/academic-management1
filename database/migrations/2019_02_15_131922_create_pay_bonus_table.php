<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_bonus';
    protected $primaryKey = 'pay_bonus_id';

    public function up()
    {
        if(!Schema::hasTable('pay_bonus')) {
            Schema::create('pay_bonus', function (Blueprint $table) {
                $table->increments('pay_bonus_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('bonus_name', 255)->nullable();
                $table->double('bonus_amount',18,2)->nullable();
                $table->timestamps();
            });

            Schema::table('pay_bonus', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_bonus', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_bonus');
    }
}
