<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'tasks';
    protected $primaryKey = 'task_id';
    public function up()
    {
        if(!Schema::hasTable('tasks')) {
            Schema::create('tasks', function (Blueprint $table) {
                $table->increments('task_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('task_name', 255)->nullable();
                $table->tinyInteger('task_priority')->nullable()->comment = '0=Low,1=Medium,2=High';
                $table->date('task_date')->nullable();
                $table->text('task_description')->nullable();
                $table->tinyInteger('task_status')->default(1)->comment = '0=Open,1=On Hold,2=Resolved';
                $table->timestamps();
            });

            Schema::table('tasks', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('tasks', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
