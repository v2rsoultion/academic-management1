<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTaskManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('tasks', function (Blueprint $table) {
            $table->integer('task_type')->unsigned()->nullable()->after('task_priority')->comment = '1=Student,2=Staff';
            $table->integer('class_id')->unsigned()->nullable()->after('task_type');
            $table->integer('section_id')->unsigned()->nullable()->after('class_id');
            $table->integer('staff_student_id')->unsigned()->nullable()->after('section_id');
            $table->text('task_file')->nullable()->after('task_description');
        });

        Schema::table('tasks', function($table) {
            $table->foreign('class_id')->references('class_id')->on('classes');
        });

        Schema::table('tasks', function($table) {
            $table->foreign('section_id')->references('section_id')->on('sections');
        });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
