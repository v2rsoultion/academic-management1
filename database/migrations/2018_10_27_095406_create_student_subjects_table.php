<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'st_exclude_sub';
    protected $primaryKey = 'st_exclude_sub_id';

    public function up()
    {
        if (!Schema::hasTable('st_exclude_sub')) {
            Schema::create('st_exclude_sub', function (Blueprint $table) {
                $table->increments('st_exclude_sub_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
            Schema::table('st_exclude_sub', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_exclude_sub');
    }
}
