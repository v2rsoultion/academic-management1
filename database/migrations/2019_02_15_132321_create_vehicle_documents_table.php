<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'vehicle_documents';
    protected $primaryKey = 'vehicle_document_id';
    public function up()
    {
        if(!Schema::hasTable('vehicle_documents')) {
            Schema::create('vehicle_documents', function (Blueprint $table) {
                $table->increments('vehicle_document_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('vehicle_id')->unsigned()->nullable();
                $table->integer('document_category_id')->unsigned()->nullable();
                $table->text('vehicle_document_details')->nullable();
                $table->string('vehicle_document_file')->nullable();
                $table->timestamps();
            });

            Schema::table('vehicle_documents', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('vehicle_documents', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('vehicle_documents', function($table) {
                $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
            });

            Schema::table('vehicle_documents', function($table) {
                $table->foreign('document_category_id')->references('document_category_id')->on('document_category');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_documents');
    }
}
