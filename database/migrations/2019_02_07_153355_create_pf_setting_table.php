<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePfSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pf_setting';
    protected $primaryKey = 'pf_setting_id';

    public function up()
    {
        if(!Schema::hasTable('pf_setting')) {
            Schema::create('pf_setting', function (Blueprint $table) {
                $table->increments('pf_setting_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->tinyInteger('pf_month')->nullable();
                $table->integer('pf_year')->nullable();
                $table->float('epf_a',8,2)->nullable();
                $table->float('pension_fund_b',8,2)->nullable();
                $table->float('epf_a_b',8,2)->nullable();
                $table->double('pf_cuff_off',18,2)->nullable();
                $table->float('acc_no_02',8,2)->nullable();
                $table->float('acc_no_21',8,2)->nullable();
                $table->float('acc_no_22',8,2)->nullable();
                $table->tinyInteger('round_off')->nullable()->comment = '0=Nearest Rupee,1=Highest Rupee';
                $table->timestamps();
            });

            Schema::table('pf_setting', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pf_setting', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pf_setting', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pf_setting');
    }
}
