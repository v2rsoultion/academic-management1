<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaySalaryHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_salary_head';
    protected $primaryKey = 'pay_salary_head_id';

    public function up()
    {
        if(!Schema::hasTable('pay_salary_head')) {
            Schema::create('pay_salary_head', function (Blueprint $table) {
                $table->increments('pay_salary_head_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('sal_head_name', 255)->nullable();
                $table->tinyInteger('head_type')->default(1)->comment = '0=Deduction,1=Allowance';
                $table->tinyInteger('pay_salary_head_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('pay_salary_head', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_salary_head', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('pay_salary_head')->insert(
                array(
                        'pay_salary_head_id'      => 1,
                        'admin_id'                => 1,
                        'update_by'               => 1,
                        'sal_head_name'           => 'Basic Pay',
                        'head_type'               => 1,
                        'pay_salary_head_status'  => 1,
                        'created_at'              => date('Y-m-d H:i:s'),
                        'updated_at'              => date('Y-m-d H:i:s'),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_salary_head');
    }
}
