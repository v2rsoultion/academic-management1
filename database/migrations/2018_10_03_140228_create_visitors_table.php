<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'visitors';
    protected $primaryKey = 'visitor_id';
    public function up()
    {
        if (!Schema::hasTable('visitors')) {
            Schema::create('visitors', function (Blueprint $table) {
                $table->increments('visitor_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('name',255)->nullable();
                $table->text('purpose')->nullable();
                $table->string('contact',255)->nullable();
                $table->string('email',255)->nullable();
                $table->date('visit_date')->nullable();
                $table->time('check_in_time')->nullable();
                $table->time('check_out_time')->nullable();
                $table->timestamps();
            });

            Schema::table('visitors', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('visitors', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
