<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('admins'))
        {
            Schema::create('admins', function (Blueprint $table) {
                $table->increments('admin_id')->unsigned();
                $table->integer('admin_type')->unsigned()->default('0')->comment = '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin';
                $table->string('admin_name')->nullable();
                $table->string('email')->nullable();
                $table->string('password');
                $table->text('admin_profile_img')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
            DB::table('admins')->insert(
                array(
                    array(
                        'admin_id'          => 1,
                        'admin_type'        => 0,
                        'admin_name'        => 'Super Admin',
                        'email'             => 'admin@gmail.com',
                        'password'          => '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG',
                        'remember_token'    => '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'admin_id'          => 2,
                        'admin_type'        => 1,
                        'admin_name'        => 'School Admin',
                        'email'             => 'school@gmail.com',
                        'password'          => '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG',
                        'remember_token'    => '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
