<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddHomeworkFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('homework_conversations', function (Blueprint $table) {
            $table->text('h_conversation_attechment')->nullable();
            $table->integer('h_session_id')->unsigned()->nullable();
        });
        Schema::table('homework_conversations', function($table) {
            $table->foreign('h_session_id')->references('session_id')->on('sessions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homework_conversations', function (Blueprint $table) {
            $table->dropColumn('h_conversation_attechment');
            $table->dropForeign('homework_conversations_session_id_foreign');
            $table->dropColumn('session_id');
        });
    }
}
