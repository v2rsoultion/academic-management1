<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableVehicleAttendDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'vehicle_attend_details';
    protected $primaryKey = 'vehicle_attend_d_id';

    public function up()
    {
        if (!Schema::hasTable('vehicle_attend_details')) {
            Schema::create('vehicle_attend_details', function (Blueprint $table) {
                $table->increments('vehicle_attend_d_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('vehicle_id')->unsigned()->nullable();
                $table->integer('vehicle_attendence_id')->unsigned()->nullable();
                $table->tinyInteger('vehicle_attendence_type')->nullable()->comment = '0=Absent,1=Present';
                $table->integer('student_staff_id')->unsigned()->nullable();
                $table->tinyInteger('student_staff_type')->nullable()->comment = '1=Student,2=Staff';
                $table->integer('attendence_pick_up')->unsigned()->nullable();
                $table->integer('attendence_drop')->unsigned()->nullable();
                $table->date('vehicle_attendence_date')->nullable();
                $table->tinyInteger('vehicle_attendence_status')->default(1)->comment = '0=Unmark,1=Mark';
                $table->timestamps();
            });

            Schema::table('vehicle_attend_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('vehicle_attend_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('vehicle_attend_details', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('vehicle_attend_details', function($table) {
                $table->foreign('vehicle_attendence_id')->references('vehicle_attendence_id')->on('vehicle_attendence');
            });
            Schema::table('vehicle_attend_details', function($table) {
                $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_attend_details');
    }
}
