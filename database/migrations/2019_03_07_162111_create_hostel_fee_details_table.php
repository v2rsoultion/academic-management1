<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelFeeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'hostel_fee_details';
    protected $primaryKey = 'hostel_fee_detail_id';
    public function up()
    {
        if (!Schema::hasTable('hostel_fee_details'))
        {
            Schema::create('hostel_fee_details', function (Blueprint $table) {
                $table->increments('hostel_fee_detail_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('hostel_fee_id')->unsigned()->nullable();
                $table->string('month_year',255)->nullable();
                $table->decimal('fee_amount', 10, 2)->default(0);
                $table->decimal('paid_fee_amount', 10, 2)->default(0);
                $table->tinyInteger('amount_status')->comment     = '1:full amount, 2:Less amount';
                $table->timestamps();
            });
            Schema::table('hostel_fee_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_fee_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_fee_details', function($table) {
                $table->foreign('hostel_fee_id')->references('hostel_fee_id')->on('hostel_fees');
            });
           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_fee_details');
    }
}
