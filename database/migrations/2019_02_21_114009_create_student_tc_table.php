<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_tc';
    protected $primaryKey = 'student_tc_id';
    public function up()
    {
        if (!Schema::hasTable('student_tc')) {
            Schema::create('student_tc', function (Blueprint $table) {
                $table->increments('student_tc_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->date('tc_date')->nullable();
                $table->text('tc_remark')->nullable();
                $table->text('tc_reason')->nullable();
                $table->tinyInteger('tc_status')->default(1)->comment = '0=Cancel,1=Approved';
                $table->timestamps();
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_tc', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_tc');
    }
}
