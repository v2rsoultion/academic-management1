<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\School\School; // School Model
use App\Model\Staff\Staff; // Staff Model
use App\Model\Student\Student; // Student Model
use App\Model\Student\StudentParent; // StudentParent Model
use App\Admin; // Admin Model

use DB;
use Hash;
use DateTime;
use Validator;

class EditProfileController extends Controller
{
    /*  Function name : editProfile
     *  To edit all user profile
     *  @Sumit on 26 Jan 2019    
     *  @parameters : device_id,csrf_token,name,profile_image,user_type,user_id
     *  @V2R by Sumit
    */
    public function editProfile(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $name           = $request->input('name');
        $profile_image  = $request->input('profile_image');
        $user_type      = $request->input('user_type');
        $user_id        = $request->input('user_id');
        $mobile         = $request->input('mobile');
        $email          = $request->input('email');
        
        if (!empty($request->input()) && !empty($csrf_token)  && !empty($device_id) && !empty($name) && !empty($user_type) && !empty($user_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if ( $user_type == 1 ){
                    $school = School::where('admin_id',$user_id)->first();
                    $admin  = Admin::where('admin_id',$user_id)->first();

                    if( !empty($school) && !empty($admin) ){

                        $validatior = Validator::make($request->all(), [
                            'email'  => 'unique:admins,email,'.$user_id.',admin_id',
                            'mobile'  => 'unique:admins,mobile_no,'.$user_id.',admin_id',
                        ]);
                
                        if ($validatior->fails())
                        {
                            $status = false;
                            $errors = $validatior->errors();
                            if(isset($errors['mobile']) && isset($errors['email']) ){
                                $errors = "The mobile and email has already been taken.";
                            }

                            if(isset($errors['mobile'])){
                                $errors = "The mobile has already been taken.";
                            }

                            if(isset($errors['email'])){
                                $errors = "The email has already been taken.";
                            }
                            $message = $errors;
                            
                        } else {
                            DB::beginTransaction();
                            try
                            {
                                $admin->admin_name      = $name;
                                $admin->mobile_no       = $mobile;
                                $admin->email           = $email;

                                $school->school_name            = $name;
                                $school->school_email           = $email;
                                $school->school_mobile_number   = $mobile;

                                if ($request->hasFile('profile_image'))
                                {
                                    if (!empty($school->school_id)){
                                        $attachment = check_file_exist($school->school_logo, 'school_logo');
                                        if (!empty($attachment))
                                        {
                                            unlink($attachment);
                                        }
                                    }
                                    $file                          = $request->File('profile_image');
                                    $config_document_upload_path   = \Config::get('custom.school_logo');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $school->school_logo = $filename;
                                }
                                $school->save();
                                $admin->save();
                            }
                            catch (\Exception $e)
                            {
                                //failed logic here
                                DB::rollback();
                                $error_message = $e->getMessage();
                                $status = false;
                                $message = $error_message;
                            }
                            DB::commit();

                            $status = true;
                            $message = 'success';
                        }

                        

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if($user_type == 2){
                    $staff = Staff::where('reference_admin_id',$user_id)->first();
                    $admin  = Admin::where('admin_id',$user_id)->first();

                    if( !empty($staff) && !empty($admin) ){

                        $validatior = Validator::make($request->all(), [
                            'email'  => 'unique:admins,email,'.$user_id.',admin_id',
                            'mobile'  => 'unique:admins,mobile_no,'.$user_id.',admin_id',
                        ]);
                
                        if ($validatior->fails())
                        {
                            $status = false;
                            $errors = $validatior->errors();
                            if(isset($errors['mobile']) && isset($errors['email']) ){
                                $errors = "The mobile and email has already been taken.";
                            }

                            if(isset($errors['mobile'])){
                                $errors = "The mobile has already been taken.";
                            }

                            if(isset($errors['email'])){
                                $errors = "The email has already been taken.";
                            }
                            $message = $errors;
                            
                        } else {
                            DB::beginTransaction();
                            try
                            {
                                $admin->admin_name      = $name;
                                $admin->mobile_no       = $mobile;
                                $admin->email           = $email;

                                $staff->staff_name            = $name;
                                $staff->staff_email           = $email;
                                $staff->staff_mobile_number   = $mobile;

                                if ($request->hasFile('profile_image'))
                                {
                                    if (!empty($staff->staff)){
                                        $attachment = check_file_exist($staff->staff_profile_img, 'staff_profile');
                                        if (!empty($attachment))
                                        {
                                            unlink($attachment);
                                        }
                                    }
                                    $file                          = $request->File('profile_image');
                                    $config_document_upload_path   = \Config::get('custom.staff_profile');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $staff->staff_profile_img = $filename;
                                }
                                $staff->save();
                                $admin->save();
                            }
                            catch (\Exception $e)
                            {
                                //failed logic here
                                DB::rollback();
                                $error_message = $e->getMessage();
                                $status = false;
                                $message = $error_message;
                            }
                            DB::commit();
                            $status = true;
                            $message = 'success';
                        }

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if($user_type == 3){
                    $admin  = Admin::where('admin_id',$user_id)->first();
                    $student_parent  = StudentParent::where('reference_admin_id',$user_id)->first();

                    if( !empty($admin) && !empty($student_parent) ){

                        $validatior = Validator::make($request->all(), [
                            'email'  => 'unique:admins,email,'.$user_id.',admin_id',
                            'mobile'  => 'unique:admins,mobile_no,'.$user_id.',admin_id',
                        ]);
                
                        if ($validatior->fails())
                        {
                            $status = false;
                            $errors = $validatior->errors()->messages();

                            if(isset($errors['mobile']) && isset($errors['email']) ){
                                $errors = "The mobile and email has already been taken.";
                            }

                            if(isset($errors['mobile'])){
                                $errors = "The mobile has already been taken.";
                            }

                            if(isset($errors['email'])){
                                $errors = "The email has already been taken.";
                            }

                            $message = $errors;
                            
                        } else {

                            DB::beginTransaction();
                            try
                            {
                                $admin->admin_name      = $name;
                                $admin->mobile_no       = $mobile;
                                $admin->email           = $email;

                                $student_parent->student_login_name         = $name;
                                $student_parent->student_login_email        = $email;
                                $student_parent->student_login_contact_no   = $mobile;

                                $admin->save();
                                $student_parent->save();
                            }
                            catch (\Exception $e)
                            {
                                //failed logic here
                                DB::rollback();
                                $error_message = $e->getMessage();
                                $status = false;
                                $message = $error_message;
                            }
                            DB::commit();

                            $status = true;
                            $message = 'success';

                        }
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if($user_type == 4){
                    $student = Student::where('reference_admin_id',$user_id)->first();
                    $admin  = Admin::where('admin_id',$user_id)->first();

                    if( !empty($admin) && !empty($student) ){
                        $validatior = Validator::make($request->all(), [
                            'email'  => 'unique:admins,email,'.$user_id.',admin_id',
                        ]);

                        if ($validatior->fails())
                        {
                            $status = false;
                            $errors = $validatior->errors();
                            if(isset($errors['mobile']) && isset($errors['email']) ){
                                $errors = "The mobile and email has already been taken.";
                            }

                            if(isset($errors['mobile'])){
                                $errors = "The mobile has already been taken.";
                            }

                            if(isset($errors['email'])){
                                $errors = "The email has already been taken.";
                            }
                            $message = $errors;
                            
                        } else {

                            DB::beginTransaction();
                            try
                            {
                                $admin->admin_name      = $name;
                                $admin->email           = $email;

                                $student->student_name  = $name;
                                $student->student_email  = $email;

                                if ($request->hasFile('profile_image'))
                                {
                                    if (!empty($school->school_id)){
                                        $attachment = check_file_exist($student->student_image, 'student_image');
                                        if (!empty($attachment))
                                        {
                                            unlink($attachment);
                                        }
                                    }
                                    $file                          = $request->File('profile_image');
                                    $config_document_upload_path   = \Config::get('custom.student_image');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $student->student_image = $filename;
                                }
                                $student->save();
                                $admin->save();
                            }
                            catch (\Exception $e)
                            {
                                //failed logic here
                                DB::rollback();
                                $error_message = $e->getMessage();
                                $status = false;
                                $message = $error_message;
                            }
                            DB::commit();

                            $status = true;
                            $message = 'success';
                        }
                    }
                }
            
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }
}
