<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Staff\Staff; // Staff Model
use App\Model\TimeTable\TimeTable; // TimeTable Model
use App\Model\TimeTable\TimeTableMap; // TimeTableMap Model

use DB;
use Hash;
use DateTime;

class TimeTableController extends Controller
{

    /* Function name : getTimeTable
     *  To get staff schedule
     *  @Sumit on 03 JAN 2019    
     *  @parameters : device_id,csrf_token,class_id,section_id
     *  @V2R by Sumit
    */
    public function getTimeTable(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $total_lecture = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $arr_week_days  = \Config::get('custom.week_days');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $time_table_map = TimeTableMap::

                join('time_tables', function($join) use ($class_id,$section_id){
                    $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
                    $join->where('time_tables.class_id', '=',$class_id);
                    $join->where('time_tables.section_id', '=',$section_id);
                })
                ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
                ->orderBy('map_lecture_no', 'ASC')
                ->with('getSubject')->with('getStaff')
                ->with('getClass')
                ->with('getSection')
                ->get()
                ->groupBy('map_lecture_no')->toArray();

                $final_arr = [];

                if( !empty($time_table_map) ){
                    foreach($time_table_map as $lecture_key => $time_table){

                        $arr_days = [];

                        if(!empty($time_table)){

                            foreach ($time_table as $key => $time_table_data) {

                                $lecture = $time_table_data['lecture_no'];
                                $total_lecture = $time_table_data['total_lecture'];
                                $start_time = date("g:i a", strtotime($time_table_data['start_time']));
                                $end_time = date("g:i a", strtotime($time_table_data['end_time']));
                                $class = $time_table_data['get_class']['class_name'];
                                $section = $time_table_data['get_section']['section_name'];

                                $subject_name = '';

                                $staff_name = '';

                                $lunch_break = '';

                                if($time_table_data['lunch_break'] == '1'){
                                    $lunch_break = "Lunch Break";
                                } else {
                                    $subject_name   = $time_table_data['get_subject']['subject_name'];
                                    $staff_name   = $time_table_data['get_staff']['staff_name'];
                                }

                                $arr_days[] = array(
                                    'day'           => $arr_week_days[$time_table_data['day']],
                                    'day_key'       => $time_table_data['day'],
                                    'lecture_no'    => $lecture,
                                    'start_time'    => $start_time,
                                    'end_time'      => $end_time,
                                    'class'         => $class,
                                    'section'       => $section,
                                    'subject_name'  => $subject_name,
                                    'staff_name'    => $staff_name,
                                    'lunch_break'   => $lunch_break
                                );
                            }
                        }

                        usort($arr_days, function($a, $b) {
                            return $a['day_key'] <=> $b['day_key'];
                        });

                        $final_arr[] = array(
                            'day_keys'      => $time_table[0]['time_table_week_days'],
                            'for_lecture'   => $lecture_key,
                            'day_info'      => $arr_days
                        );
                    }

                    $result_data['staff_schedule'] = $final_arr;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['total_lecture'] = $total_lecture;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);
    }

}

