<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\City\City; // Model

use DB;
use Hash;
use DateTime;
use Validator;

class CityController extends Controller
{
    /*  Function name : viewCity
     *  To get all city
     *  @Sandeep on 27 march 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function view_city(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $city_id        = $request->input('city_id');
        $state_id       = $request->input('state_id');
        
        if ( !empty($csrf_token) && !empty($device_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $city  = City::where(function($query) use ($city_id,$state_id,$request) 
                {
                    if (!empty($city_id))
                    {
                        $query->where('city_id', $city_id);
                    }
                    if (!empty($state_id))
                    {
                        $query->where('state_id', $state_id);
                    }
                    if (!empty($request) && !empty($request->get('city_name')))
                    {
                        $query->where('city_name', "like", "%{$request->get('city_name')}%");
                    }
                })->orderBy('city_name', 'ASC')->paginate(20);


                if( count($city) != 0 ){

                    $city_info  = [];
                    foreach($city as $city_res){

                        $city_info[] = array(
                            'city_id'     =>  check_string_empty($city_res->city_id),
                            'city_name'   =>  check_string_empty($city_res->city_name),
                            'state_id'    =>  check_string_empty($city_res->state_id),
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['city_info'] = $city_info;
                    $lastPage = $city->lastPage($city_res);

                } else {
                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


}