<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Hostel\Hostel; // Hostel Model
use App\Model\Hostel\HostelBlock; // HostelBlock Model
use App\Model\Hostel\HostelFloor;  // HostelFloor Model
use App\Model\Hostel\HostelRoomCategory; // HostelRoomCategory Model
use App\Model\Hostel\HostelRooms; // HostelRooms Model
use App\Model\Hostel\HostelStudentMap; // HostelStudentMap Model
use App\Model\Student\Student;// Student Model

use DateTime;
use DB;
use URL;

class HostelController extends Controller
{
    /*  Function name : getStudentForHostel
     *  To get student for hostel
     *  @Sumit on 04 Jan 2019
     *  @parameters : device_id,csrf_token,type
     *  @V2R by Sumit
     */
    public function getStudentForHostel(Request $request){
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $type = $request->input('type');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && $type != '') {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {
                
                $arr_student  = Student::where(function($query) use ($request,$type)
                {
                    if ( $request->get('student_name') !='' ){
                        $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                    }
                })->join('student_academic_info', function($join) use ($request){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                        $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                    }
                    if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                        $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                    }
                })
                ->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
                ->join('student_parents', function($join) use ($request){
                    $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                })
                ->leftJoin('hostel_student_map', function($join) use ($request){
                    $join->on('hostel_student_map.student_id', '=', 'students.student_id');
                    if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
                        $join->where('hostel_student_map.hostel_id', '=',$request->get('hostel_id'));
                    }
                    $join->where('hostel_student_map.leave_status', '=',0);
                })
                ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
                ->orderBy('students.student_id', 'DESC')
                ->with(['getStudentHostelMap' => function($query) use($request) {
                    $query->with('getHostel');
                    $query->with('getBlock');
                    $query->with('getFloor');
                    $query->with('getRoom');
                }])
                ->groupBy('students.student_id')
                ->paginate(20);

                //p($arr_student);
                $students = [];
                if (!empty($arr_student[0]->student_id))
                {
                    foreach ($arr_student as $student)
                    {
                        $student_image = '';
                        if (!empty($student->student_image))
                        {
                            $profile = check_file_exist($student->student_image, 'student_image');
                            if (!empty($profile))
                            {
                                $student_image = URL::to($profile);
                            }
                        } else {
                            $student_image = "";
                        }
                        $leave_status = 0;
                        if(isset($student->leave_status) && $student->leave_status == 1){
                            $leave_status = 1;
                        }
                        $join_status = 0;
                        if(isset($student->leave_status) && $student->leave_status == 0){
                            $join_status = 1;
                        }
                        // For class Name
                        $class_name = '';
                        if(isset($student['getStudentAcademic']['getCurrentClass']->class_name) && $student['getStudentAcademic']['getCurrentClass']->class_name != ''){
                            $class_name = $student['getStudentAcademic']['getCurrentClass']->class_name;
                        }
                        // For Section Name
                        $section_name = '';
                        if(isset($student['getStudentAcademic']['getCurrentSection']->section_name) && $student['getStudentAcademic']['getCurrentSection']->section_name != ''){
                            $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                        }
                        // For hostel Name
                        $hostel_name = '';
                        if(isset($student['getStudentHostelMap']['getHostel']->hostel_name) && $student['getStudentHostelMap']['getHostel']->hostel_name != '' && $student->hostel_id != ''){
                            $hostel_name = $student['getStudentHostelMap']['getHostel']->hostel_name;
                        }
                        // For block Name
                        $block_name = '';
                        if(isset($student['getStudentHostelMap']['getBlock']->h_block_name) && $student['getStudentHostelMap']['getBlock']->h_block_name != '' && $student->h_block_id != ''){
                            $block_name = $student['getStudentHostelMap']['getBlock']->h_block_name;
                        }
                        // For Floor No
                        $floor_no = '';
                        if(isset($student['getStudentHostelMap']['getFloor']->h_floor_no) && $student['getStudentHostelMap']['getFloor']->h_floor_no != '' && $student->h_floor_id != ''){
                            $floor_no = $student['getStudentHostelMap']['getFloor']->h_floor_no;
                        }
                        // For Room No
                        $room_no = '';
                        if(isset($student['getStudentHostelMap']['getRoom']->h_room_no) && $student['getStudentHostelMap']['getRoom']->h_room_no != ''  && $student->h_room_id != '' ) {
                            $room_no = $student['getStudentHostelMap']['getRoom']->h_room_no;
                        }
                        // For join date
                        $join_date = '';
                        if($student->join_date != ''){
                            $join_date = date('d F Y', strtotime($student->join_date));
                        }
                        // For leave Date
                        $leave_date = '';
                        if($student->leave_date != ''){
                            $leave_date = date('d F Y', strtotime($student->leave_date));
                        }
                        
                        if( $type == 0){
                            if ($student['h_student_map_id'] == '' || $student['leave_status'] == 1  ) {
                                $students[] = array(
                                    'h_student_map_id'  => check_string_empty($student['h_student_map_id']),
                                    'student_id'        => check_string_empty($student['student_id']),
                                    'student_name'      => check_string_empty($student['student_name']),
                                    'profile_image'     => check_string_empty($student_image),
                                    'enroll_no'         => check_string_empty($student['student_enroll_number']),
                                    'class_section'     => check_string_empty($class_name.'-'.$section_name),
                                    'father_name'       => check_string_empty($student['student_father_name']),
                                    'h_student_map_id'  => check_string_empty($student['h_student_map_id']),
                                    'h_room_id'         => check_string_empty($student['h_room_id']),
                                    'leave_status'      => $leave_status,
                                    'register_status'   => 0
                                );
                            } 
                        } else if($type == 1) {

                            if ($student['h_student_map_id'] != '' && $student['leave_status'] != 1 && $student['h_room_id'] == ''  ) {
                                $students[] = array(
                                    'h_student_map_id'  => check_string_empty($student['h_student_map_id']),
                                    'student_id'        => check_string_empty($student['student_id']),
                                    'student_name'      => check_string_empty($student['student_name']),
                                    'profile_image'     => check_string_empty($student_image),
                                    'enroll_no'         => check_string_empty($student['student_enroll_number']),
                                    'class_section'     => check_string_empty($class_name.'-'.$section_name),
                                    'father_name'       => check_string_empty($student['student_father_name']),
                                    'h_student_map_id'  => check_string_empty($student['h_student_map_id']),
                                    'h_room_id'         => check_string_empty($student['h_room_id']),
                                    'register_status'   => 1,
                                    'leave_status'      => $leave_status,
                                );
                            }
                        }
                    }
                }

                if( $type == 0 ){
                    if (!empty($students)) {
                        $result_data['student_info'] = $students;
                        $lastPage = $arr_student->lastPage($student);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if( $type == 1 ){
                    if (!empty($students)) {
                        $result_data['student_info'] = $students;
                        $lastPage = $arr_student->lastPage($student);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }

                
            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page'] = $lastPage;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : RegisterHostelStudent
     *  To register student for hostel
     *  @Sumit on 04 Jan 2019
     *  @parameters : device_id,csrf_token,student_id
     *  @V2R by Sumit
     */
    public function RegisterHostelStudent(Request $request){
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $student_id = $request->input('student_id');
        $admin_id = $request->input('admin_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($student_id) && !empty($admin_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $register = HostelStudentMap::where([ ['student_id', $student_id], ['leave_status', 0] ])->first();

                if (!empty($register)) {

                    $error_message = "Sorry this student is already registered.";
                    $status = false;
                    $message = $error_message;

                } else {

                    $register = new HostelStudentMap();
                    $success_msg = '';

                    DB::beginTransaction();
                    try
                    {
                        $register->admin_id = $admin_id;
                        $register->update_by = $admin_id;
                        $register->student_id = $student_id;
                        $register->save();
                        $success_msg = "Student register successfully!";

                    //    $send_notification  = $this->send_push_notification($register->h_student_map_id,$student_id,'hostel_registration',$map['student_id']);

                    } catch (\Exception $e) {
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                }

                if ($success_msg != "") {
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = $error_message;
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);

    }

    /*  Function name : getHostelAllocatedStudentList
     *  To get student hostel allocated list
     *  @Sumit on 04 Jan 2019
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
     */
    public function getHostelAllocatedStudentList(Request $request){
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $type       = 2;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $arr_student = Student::where(function($query) use ($request,$type) 
                {
                    if ( $request->get('student_name') !='' ){
                        $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                    }
                })
                ->join('student_academic_info', function($join) use ($request){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                        $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                    }
                    if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                        $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                    }
                })
                ->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
                ->join('student_parents', function($join) use ($request){
                    $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                })
                
                ->join('hostel_student_map', function($join) use ($request){
                    $join->on('hostel_student_map.student_id', '=', 'students.student_id');
                    $join->Where('h_student_map_id', '!=', NULL);

                    if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
                        $join->where('hostel_id', '=',$request->get('hostel_id'));
                    }

                    if (!empty($request) && !empty($request->get('status')) && $request->get('status') != null){
                        $join->Where('hostel_id', '!=', NULL);
                    }
                    if (!empty($request) && !empty($request->get('leave_status')) && $request->get('leave_status') != null){
                        $join->where('leave_status', '=',$request->get('leave_status'));
                    } else {
                        $join->where('leave_status', '=',0);
                    }
                    if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null){
                        $join->where('h_room_id', '=',$request->get('h_room_id'));
                    } 

                })
                ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
                ->orderBy('hostel_student_map.h_student_map_id', 'DESC')
                ->with(['getStudentHostelMap' => function($query) use($request) {
                    $query->where('hostel_student_map.leave_status',0);
                    $query->with(['getHostel'=>function($q1) use ($request) {

                        if (!empty($request) && !empty($request->get('hostel_name')))
                        {
                            $q1->where('hostel_name', "like", "%{$request->get('hostel_name')}%");
                        }
    
                    }]);
                    $query->with('getBlock');
                    $query->with('getFloor');
                    $query->with('getRoom');
                }])
                ->paginate(20);

                $students = [];
                if (!empty($arr_student[0]->student_id))
                {
                    foreach ($arr_student as $student)
                    {
                        $student_image = '';
                        if (!empty($student->student_image))
                        {
                            $profile = check_file_exist($student->student_image, 'student_image');
                            if (!empty($profile))
                            {
                                $student_image = URL::to($profile);
                            }
                        } else {
                            $student_image = "";
                        }
                        $leave_status = 0;
                        if(isset($student->leave_status) && $student->leave_status == 1){
                            $leave_status = 1;
                        }
                        $join_status = 0;
                        if(isset($student->leave_status) && $student->leave_status == 0){
                            $join_status = 1;
                        }
                        // For class Name
                        $class_name = '';
                        if(isset($student['getStudentAcademic']['getCurrentClass']->class_name) && $student['getStudentAcademic']['getCurrentClass']->class_name != ''){
                            $class_name = $student['getStudentAcademic']['getCurrentClass']->class_name;
                        }
                        // For Section Name
                        $section_name = '';
                        if(isset($student['getStudentAcademic']['getCurrentSection']->section_name) && $student['getStudentAcademic']['getCurrentSection']->section_name != ''){
                            $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                        }
                        // For hostel Name
                        $hostel_name = '';
                        if(isset($student['getStudentHostelMap']['getHostel']->hostel_name) && $student['getStudentHostelMap']['getHostel']->hostel_name != '' && $student->hostel_id != ''){
                            $hostel_name = $student['getStudentHostelMap']['getHostel']->hostel_name;
                        }
                        // For block Name
                        $block_name = '';
                        if(isset($student['getStudentHostelMap']['getBlock']->h_block_name) && $student['getStudentHostelMap']['getBlock']->h_block_name != '' && $student->h_block_id != ''){
                            $block_name = $student['getStudentHostelMap']['getBlock']->h_block_name;
                        }
                        // For Floor No
                        $floor_no = '';
                        if(isset($student['getStudentHostelMap']['getFloor']->h_floor_no) && $student['getStudentHostelMap']['getFloor']->h_floor_no != '' && $student->h_floor_id != ''){
                            $floor_no = $student['getStudentHostelMap']['getFloor']->h_floor_no;
                        }
                        // For Room No
                        $room_no = '';
                        if(isset($student['getStudentHostelMap']['getRoom']->h_room_no) && $student['getStudentHostelMap']['getRoom']->h_room_no != ''  && $student->h_room_id != '' ) {
                            $room_no = $student['getStudentHostelMap']['getRoom']->h_room_no;
                        }
                        // For join date
                        $join_date = '';
                        if($student->join_date != ''){
                            $join_date = date('d F Y', strtotime($student->join_date));
                        }
                        // For leave Date
                        $leave_date = '';
                        if($student->leave_date != ''){
                            $leave_date = date('d F Y', strtotime($student->leave_date));
                        }

                        if ($student['hostel_id'] != '' && $hostel_name !='' ) {

                            $students[] = array(
                                'student_id'        => check_string_empty($student['student_id']),
                                'student_name'      => check_string_empty($student['student_name']),
                                'profile_image'     => check_string_empty($student_image),
                                'enroll_no'         => check_string_empty($student['student_enroll_number']),
                                'class_section'     => check_string_empty($class_name.'-'.$section_name),
                                'father_name'       => check_string_empty($student['student_father_name']),
                                'h_student_map_id'  => check_string_empty($student['h_student_map_id']),
                                'hostel_block'      => check_string_empty($hostel_name.'-'.$block_name),
                                'hostel_name'       => check_string_empty($hostel_name),
                                'block_name'        => check_string_empty($block_name),
                                'floor_room_no'     => check_string_empty($floor_no.'-'.$room_no),
                                'floor_no'          => check_string_empty($floor_no),
                                'room_no'           => check_string_empty($room_no),
                                'hostel_id'         => check_string_empty($student['hostel_id']),
                                'h_block_id'        => check_string_empty($student['h_block_id']),
                                'h_floor_id'        => check_string_empty($student['h_floor_id']),
                                'h_room_id'         => check_string_empty($student['h_room_id']),
                                'join_date'         => check_string_empty($student['join_date']),
                                'leave_date'        => check_string_empty($student['leave_date']),
                            );
                        }
                    }
                }

                if (!empty($students)) {
                    $result_data['student_info'] = $students;
                    $lastPage = $arr_student->lastPage($student);  
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                    
            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : allocateHostelStudent
     *  To allocate hostel to student
     *  @Sumit on 09 Jan 2019
     *  @parameters : device_id,csrf_token,hostel_id,block_id,floor_id,room_no,join_date,leave_date
     *  @V2R by Sumit
     */
    public function allocateHostelStudent(Request $request){

        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $hostel_id = $request->input('hostel_id');
        $block_id = $request->input('block_id');
        $floor_id = $request->input('floor_id');
        $room_id = $request->input('room_id');
        $join_date = $request->input('join_date');
        $leave_date = $request->input('leave_date');
        $student_id = $request->input('student_id');
        $h_student_map_id = $request->input('h_student_map_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($hostel_id) && !empty($block_id) && !empty($floor_id) && !empty($room_id) && !empty($join_date) && !empty($leave_date) && !empty($h_student_map_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $allocate = HostelStudentMap::find($request->get('h_student_map_id'));

                DB::beginTransaction();
                try
                {
                    if ($request->get('hostel_id') != '') {
                        $allocate->hostel_id = $request->get('hostel_id');
                    }

                    if ($request->get('block_id') != '') {
                        $allocate->h_block_id = $request->get('block_id');
                    }

                    if ($request->get('floor_id') != '') {
                        $allocate->h_floor_id = $request->get('floor_id');
                    }

                    if ($request->get('room_id') != '') {
                        $allocate->h_room_id = $request->get('room_id');
                    }

                    if ($request->get('student_id') != '') {
                        $allocate->student_id = $request->get('student_id');
                    }

                    if ($request->get('join_date') != '') {
                        $allocate->join_date = $request->get('join_date');
                    }

                    if ($request->get('leave_date') != '') {
                        $allocate->leave_date = $request->get('leave_date');
                    }

                    if ($request->get('leave_status') != '') {
                        $allocate->leave_status = $request->get('leave_status');
                    }

                    $allocate->save();

                    $status = true;
                    $message = 'success';

                } catch (\Exception $e) {

                    DB::rollback();
                    $error_message = $e->getMessage();
                    $status = false;
                    $message = $error_message;
                }

                DB::commit();
                

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';

        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : leaveHostelRoom
     *  To leave hostel room
     *  @Sumit on 09 Jan 2019
     *  @parameters : device_id,csrf_token,allocated_student_id
     *  @V2R by Sumit
     */
    public function leaveHostelRoom(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $student_id = $request->input('allocated_student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($student_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $allocate = HostelStudentMap::where([ ['student_id', $student_id], ['leave_status', 0] ])->first();

                $success_msg = '';

                if (!empty($allocate)) {

                    DB::beginTransaction();
                    try
                    {
                        $allocate->leave_status = 1;
                        $allocate->save();
                        $success_msg = "Student successfully leave room.";
                    } catch (\Exception $e) {
                        DB::rollback();
                        $error_message = $e->getMessage();
                    }
                    DB::commit();

                    if ($success_msg != "") {
                        $status = true;
                        $message = $success_msg;
                    } else {
                        $status = false;
                        $message = $error_message;
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : getRoomStudent
     *  To leave hostel room
     *  @Sumit on 09 Jan 2019
     *  @parameters : device_id,csrf_token,hostel_id,block_id,floor_id,room_id
     *  @V2R by Sumit
     */

    public function getRoomStudent(Request $request){
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $hostel_id = $request->input('hostel_id');
        $block_id = $request->input('block_id');
        $floor_id = $request->input('floor_id');
        $room_id = $request->input('room_id');
        $type = 2;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($hostel_id) && !empty($block_id) && !empty($floor_id) && !empty($room_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $students = Student::where(function($query) use ($request,$type) 
                {
                    if ( $request->get('student_name') !='' ){
                        $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                    }
                })
                ->join('student_academic_info', function($join) use ($request){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                        $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                    }
                    if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                        $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                    }
                })
                ->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
                ->join('student_parents', function($join) use ($request){
                    $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                })
                
                ->join('hostel_student_map', function($join) use ($request){
                    $join->on('hostel_student_map.student_id', '=', 'students.student_id');
                    $join->Where('h_student_map_id', '!=', NULL);

                    if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
                        $join->where('hostel_id', '=',$request->get('hostel_id'));
                    }

                    if (!empty($request) && !empty($request->get('block_id')) && $request->get('block_id') != null){
                        $join->where('h_block_id', '=',$request->get('block_id'));
                    }

                    if (!empty($request) && !empty($request->get('floor_id')) && $request->get('floor_id') != null){
                        $join->where('h_floor_id', '=',$request->get('floor_id'));
                    }

                    if (!empty($request) && !empty($request->get('room_id')) && $request->get('room_id') != null){
                        $join->where('h_room_id', '=',$request->get('room_id'));
                    }

                    if (!empty($request) && !empty($request->get('status')) && $request->get('status') != null){
                        $join->Where('hostel_id', '!=', NULL);
                    }
                    if (!empty($request) && !empty($request->get('leave_status')) && $request->get('leave_status') != null){
                        $join->where('leave_status', '=',1);
                    } else {
                        $join->where('leave_status', '=',0);
                    }
                    if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null){
                        $join->where('h_room_id', '=',$request->get('h_room_id'));
                    } 

                })
                ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
                ->orderBy('hostel_student_map.h_student_map_id', 'DESC')
                ->with(['getStudentHostelMap' => function($query) use($request) {
                    $query->where('hostel_student_map.leave_status',0);
                    $query->with('getHostel');
                    $query->with('getBlock');
                    $query->with('getFloor');
                    $query->with('getRoom');
                }])
                ->paginate(20);

                $room_capacity = 0;

                $roomInfo = get_room_info($room_id);

                //p($roomInfo);

                $room_capacity = $roomInfo['h_room_capacity'];

                $roomOccupantsCount = get_room_occupant_count($room_id);

                $room_available = $room_capacity - $roomOccupantsCount;

                $room_result = array(
                    'room_capacity'         => $room_capacity,
                    'room_available'        => $room_available,
                    'room_occupied'         => $roomOccupantsCount,
                    'hostel_block_floor'    => $roomInfo['hostel_block_floor'],
                    'room_category'         => $roomInfo['room_category'],
                );

                $student_info = [];
                foreach ($students as $student) {

                    $student_image = '';
                    if (!empty($student->student_image))
                    {
                        $profile = check_file_exist($student->student_image, 'student_image');
                        if (!empty($profile))
                        {
                            $student_image = URL::to($profile);
                        }
                    } else {
                        $student_image = "";
                    }
                    $leave_status = 0;
                    if(isset($student->leave_status) && $student->leave_status == 1){
                        $leave_status = 1;
                    }
                    $join_status = 0;
                    if(isset($student->leave_status) && $student->leave_status == 0){
                        $join_status = 1;
                    }
                    // For class Name
                    $class_name = '';
                    if(isset($student['getStudentAcademic']['getCurrentClass']->class_name) && $student['getStudentAcademic']['getCurrentClass']->class_name != ''){
                        $class_name = $student['getStudentAcademic']['getCurrentClass']->class_name;
                    }
                    // For Section Name
                    $section_name = '';
                    if(isset($student['getStudentAcademic']['getCurrentSection']->section_name) && $student['getStudentAcademic']['getCurrentSection']->section_name != ''){
                        $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                    }
                    // For hostel Name
                    $hostel_name = '';
                    if(isset($student['getStudentHostelMap']['getHostel']->hostel_name) && $student['getStudentHostelMap']['getHostel']->hostel_name != '' && $student->hostel_id != ''){
                        $hostel_name = $student['getStudentHostelMap']['getHostel']->hostel_name;
                    }
                    // For block Name
                    $block_name = '';
                    if(isset($student['getStudentHostelMap']['getBlock']->h_block_name) && $student['getStudentHostelMap']['getBlock']->h_block_name != '' && $student->h_block_id != ''){
                        $block_name = $student['getStudentHostelMap']['getBlock']->h_block_name;
                    }
                    // For Floor No
                    $floor_no = '';
                    if(isset($student['getStudentHostelMap']['getFloor']->h_floor_no) && $student['getStudentHostelMap']['getFloor']->h_floor_no != '' && $student->h_floor_id != ''){
                        $floor_no = $student['getStudentHostelMap']['getFloor']->h_floor_no;
                    }
                    // For Room No
                    $room_no = '';
                    if(isset($student['getStudentHostelMap']['getRoom']->h_room_no) && $student['getStudentHostelMap']['getRoom']->h_room_no != ''  && $student->h_room_id != '' ) {
                        $room_no = $student['getStudentHostelMap']['getRoom']->h_room_no;
                    }
                    // For join date
                    $join_date = '';
                    if($student->join_date != ''){
                        $join_date = date('d F Y', strtotime($student->join_date));
                    }
                    // For leave Date
                    $leave_date = '';
                    if($student->leave_date != ''){
                        $leave_date = date('d F Y', strtotime($student->leave_date));
                    }

                    $student_info[] = array(
                        'student_id'    => check_string_empty($student['student_id']),
                        'student_name'  => check_string_empty($student['student_name']),
                        'profile_image' => check_string_empty($student_image),
                        'enroll_no'     => check_string_empty($student['student_enroll_number']),
                        'class_section' => check_string_empty($class_name.'-'.$section_name),
                    );
                }

                    $result_data = $room_result;
                    $result_data['student_info'] = $student_info;
                    $lastPage = $students->lastPage($student);
                    $status = true;
                    $message = 'success';
                
            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        return response()->json($result_response, 200);

    }

    /*  Function name : getHostelData
     *  To all hostel data
     *  @Sumit on 09 Jan 2019
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
     */

    public function getHostelData(Request $request){
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                $get_hostels = Hostel::where('hostel_status', 1)->get()->toArray();

                if (!empty($get_hostels)) {

                    $hostel_data_arr = [];

                    foreach ($get_hostels as $get_hostel_res) {

                        $get_hostel_blocks = HostelBlock::where([['hostel_id', $get_hostel_res['hostel_id']], ['h_block_status', 1]])->with(array('getFloors' => function ($query) {

                            $query->select('h_block_id', 'h_floor_id', 'h_floor_no', 'h_floor_rooms', 'h_floor_description');

                        }))->with(array('getFloors.getRooms' => function ($query) {

                            $query->where('h_room_status', 1);
                            $query->select('h_room_id', 'h_floor_id', 'h_room_no', 'h_room_alias', 'h_room_capacity', 'h_room_cate_id');

                            $query->with(array('getRoomCate' => function ($room_cat_q) {

                                $room_cat_q->select('h_room_cate_id', 'h_room_cate_name', 'h_room_cate_fees', 'h_room_cate_des');

                            }));

                        }))->select('h_block_id', 'h_block_name')->get()->toArray();

                        $hostel_data_arr[] = array(
                            'hostel_id'     => $get_hostel_res['hostel_id'],
                            'hostel_name'   => $get_hostel_res['hostel_name'],
                            'hostel_type'   => $get_hostel_res['hostel_type'],
                            'hostel_blocks' => $get_hostel_blocks,
                        );
                    }

                    $result_data['hostel_data'] = $hostel_data_arr;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

}
