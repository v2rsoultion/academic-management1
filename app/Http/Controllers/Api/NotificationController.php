<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin; // Admin Model
use App\Model\Notification\Notification; // Notification Model

use DB;
use Hash;
use DateTime;
use Validator;

class NotificationController extends Controller
{
    /*  Function name : changeNotificationStatus
     *  To change notification status
     *  @Pratyush on 01 FEB 2019    
     *  @parameters : device_id,csrf_token,user_id,user_type,notification_status
     *  @V2R by Sumit
    */
    public function changeNotificationStatus(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_id        = $request->input('user_id');
        $user_type      = $request->input('user_type');
        $notification_status     = $request->input('notification_status');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_id)  && $notification_status !='' ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                Admin::where('admin_id', $user_id)->update(['notification_status' => $notification_status]);

                $status         = true;
                $message        = 'notification status update successfully';

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getNotification
     *  To change notification status
     *  @Pratyush on 01 FEB 2019    
     *  @parameters : device_id,csrf_token,user_id,user_type,notification_status
     *  @V2R by Sumit
    */
    public function getNotification(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_id        = $request->input('user_admin_id');
        $user_type      = $request->input('user_type');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_id) && !empty($user_type) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $session = get_current_session();
                
                if( $user_type == 1 ){
                    $notification = Notification::where(function($query) use($user_id,$session,$class_id,$section_id){
                        if ( !empty($user_id) )
                        {
                            $query->where([ ['school_admin_id', $user_id], ['session_id', $session['session_id']] ]);
                        }
                    })->orderby('notification_id', 'desc')->paginate(20);

                    if( count($notification) !=0 ){

                        $notification_info = [];

                        foreach ($notification as $notification_res) {
                            $notification_info[] = array(
                                'notification_id'   =>  check_string_empty($notification_res['notification_id']),
                                'student_id'        =>  check_string_empty($notification_res['student_admin_id']),
                                'parent_id'         =>  check_string_empty($notification_res['parent_admin_id']),
                                'class_id'          =>  check_string_empty($notification_res['class_id']),
                                'section_id'        =>  check_string_empty($notification_res['section_id']),
                                'staff_id'          =>  check_string_empty($notification_res['staff_admin_id']),
                                'notification_type' =>  check_string_empty($notification_res['notification_type']),
                                'module_id'         =>  check_string_empty($notification_res['module_id']),
                                'notification_text' =>  check_string_empty($notification_res['notification_text']),
                                'date'              =>  date('d F Y',strtotime($notification_res['created_at'])),
                            );
                        }

                        $result_data = $notification_info;
                        $lastPage = $notification->lastPage($notification_res);
                        $status  = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if( $user_type == 2 ){
                    $notification = Notification::where(function($query) use($user_id,$session){
                        if ( !empty($user_id) )
                        {
                            $query->where([ ['staff_admin_id', $user_id], ['session_id', $session['session_id']] ]);
                            $query->orWhere([ ['notification_via', 3] ]);
                        }
                    })->orderby('notification_id', 'desc')->paginate(20);

                    if( count($notification) !=0 ){

                        $notification_info = [];

                        foreach ($notification as $notification_res) {
                            $notification_info[] = array(
                                'notification_id'   =>  check_string_empty($notification_res['notification_id']),
                                'student_id'        =>  check_string_empty($notification_res['student_admin_id']),
                                'parent_id'         =>  check_string_empty($notification_res['parent_admin_id']),
                                'class_id'          =>  check_string_empty($notification_res['class_id']),
                                'section_id'        =>  check_string_empty($notification_res['section_id']),
                                'staff_id'          =>  check_string_empty($notification_res['staff_admin_id']),
                                'notification_type' =>  check_string_empty($notification_res['notification_type']),
                                'module_id'         =>  check_string_empty($notification_res['module_id']),
                                'notification_text' =>  check_string_empty($notification_res['notification_text']),
                                'date'              =>  date('d F Y',strtotime($notification_res['created_at'])),
                            );
                        }

                        $result_data = $notification_info;
                        $lastPage = $notification->lastPage($notification_res);
                        $status  = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else if( $user_type == 3 ){

                    $notification = Notification::where(function($query) use($user_id,$session,$class_id,$section_id){
                        if ( !empty($user_id) )
                        {
                            $query->where([ ['parent_admin_id', $user_id], ['session_id', $session['session_id']] ]);
                            $query->orWhere([ ['notification_via', 2], ['topic_type', 2] ]);
                            $query->orWhere([ ['notification_via', 1], ['topic_type', 2], ['class_id', $class_id] , ['section_id', $section_id] ]);
                        }
                    })->orderby('notification_id', 'desc')->paginate(20);

                    if( count($notification) !=0 ){

                        $notification_info = [];

                        foreach ($notification as $notification_res) {
                            $notification_info[] = array(
                                'notification_id'   =>  check_string_empty($notification_res['notification_id']),
                                'student_id'        =>  check_string_empty($notification_res['student_admin_id']),
                                'parent_id'         =>  check_string_empty($notification_res['parent_admin_id']),
                                'class_id'          =>  check_string_empty($notification_res['class_id']),
                                'section_id'        =>  check_string_empty($notification_res['section_id']),
                                'staff_id'          =>  check_string_empty($notification_res['staff_admin_id']),
                                'notification_type' =>  check_string_empty($notification_res['notification_type']),
                                'module_id'         =>  check_string_empty($notification_res['module_id']),
                                'notification_text' =>  check_string_empty($notification_res['notification_text']),
                                'date'              =>  date('d F Y',strtotime($notification_res['created_at'])),
                            );
                        }

                        $result_data = $notification_info;
                        $lastPage = $notification->lastPage($notification_res);
                        $status  = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else if( $user_type == 4 ){

                    $notification = Notification::where(function($query) use($user_id,$session,$class_id,$section_id){
                        if ( !empty($user_id) )
                        {
                            $query->where([ ['student_admin_id', $user_id], ['session_id', $session['session_id']] ]);
                            $query->orWhere([ ['notification_via', 2], ['topic_type', 1] ]);
                            $query->orWhere([ ['notification_via', 1], ['topic_type', 1], ['class_id', $class_id] , ['section_id', $section_id] ]);
                        }
                    })->orderby('notification_id', 'desc')->paginate(20);

                    if( count($notification) !=0 ){

                        $notification_info = [];

                        foreach ($notification as $notification_res) {
                            $notification_info[] = array(
                                'notification_id'   =>  check_string_empty($notification_res['notification_id']),
                                'student_id'        =>  check_string_empty($notification_res['student_admin_id']),
                                'parent_id'         =>  check_string_empty($notification_res['parent_admin_id']),
                                'class_id'          =>  check_string_empty($notification_res['class_id']),
                                'section_id'        =>  check_string_empty($notification_res['section_id']),
                                'staff_id'          =>  check_string_empty($notification_res['staff_admin_id']),
                                'notification_type' =>  check_string_empty($notification_res['notification_type']),
                                'module_id'         =>  check_string_empty($notification_res['module_id']),
                                'notification_text' =>  check_string_empty($notification_res['notification_text']),
                                'date'              =>  date('d F Y',strtotime($notification_res['created_at'])),
                            );
                        }

                        $result_data = $notification_info;
                        $lastPage = $notification->lastPage($notification_res);
                        $status  = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['last_page']     = $lastPage;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }
}
