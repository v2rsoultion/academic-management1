<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Staff\StaffDocument; // StaffDocument Model
use App\Model\Student\StudentDocument; // StudentDocument Model

use DB;
use Hash;
use DateTime;

class DocumentsController extends Controller
{

    /*  Function name : getDocuments
     *  To get all documents and single documents details according to user_type and user_id
     *  @Sumit on 28 NOV 2018    
     *  @parameters : device_id,csrf_token,user_type,user_id
     *  @V2R by Sumit
    */
    public function getDocuments(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_type      = $request->input('user_type');
        $user_id        = $request->input('user_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Staff Documents Data
                if( $user_type == 1 ){

                    $staff_documents = StaffDocument::where(function($query) use($user_id){
                        if ( !empty($user_id) )
                        {
                            $query->where('staff_id', $user_id);
                        }
                    })->with('getDocumentCategory')->orderby('staff_document_id', 'DESC')->paginate(20);

                    if( count($staff_documents) !=0 ){

                        foreach($staff_documents as $staff_documents_res){

                            $staff_document_file = check_file_exist($staff_documents_res['staff_document_file'], 'staff_document_file');

                            $documents_info[] = array(
                                'document_id' => check_string_empty($staff_documents_res['staff_document_id']),
                                'document_details' => check_string_empty($staff_documents_res['staff_document_details']),
                                'document_category' => check_string_empty($staff_documents_res['getDocumentCategory']['document_category_name']),
                                'document_file' => $staff_document_file,
                                'submit_date' => date('d F Y',strtotime($staff_documents_res['created_at'])),
                            );
                        }

                        $result_data = $documents_info;
                        $lastPage = $staff_documents->lastPage($staff_documents_res);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                // Student Documents Data
                } else if($user_type == 2){

                    $student_documents = StudentDocument::where(function($query) use($user_id){

                        if ( !empty($user_id) )
                        {
                            $query->where('student_id', $user_id);
                        }
                    })->with('getDocumentCategory')->orderby('student_document_id', 'DESC')->paginate(20);

                    if( count($student_documents) !=0 ){

                        foreach($student_documents as $student_documents_res){

                            $student_document_file = check_file_exist($student_documents_res['student_document_file'], 'student_document_file');

                            $documents_info[] = array(
                                'document_id' => check_string_empty($student_documents_res['student_document_id']),
                                'document_details' => check_string_empty($student_documents_res['student_document_details']),
                                'document_category' => check_string_empty($student_documents_res['get_document_category']['document_category_name']),
                                'document_file' => $student_document_file,
                                'submit_date' => date('d F Y',strtotime($student_documents_res['created_at'])),
                            );
                        }

                        $result_data = $documents_info;
                        $lastPage = $student_documents->lastPage($student_documents_res);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        return response()->json($result_response, 200);

    }

}

