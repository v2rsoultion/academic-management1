<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PlanSchedule\PlanSchedule; // PlanSchedule Model
use App\Model\Staff\Staff; //Staff Model
use DB;
use Hash;
use DateTime;
use Validator;

class PlanScheduleController extends Controller
{


    /*  Function name : savePlanSchedule
     *  To add/edit online plan schedule
     *  @Sandeep on 19 FEB 2019    
     *  @parameters : device_id,csrf_token,plan_schedule_id,admin_id,staff_id,plan_description,plan_date,plan_time
     *  @V2R by Sandeep
    */

    public function savePlanSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $admin_id           = $request->input('admin_id');
        $staff_id           = $request->input('staff_id');
        $plan_schedule_id   = $request->input('plan_schedule_id');
        $plan_description   = $request->input('plan_description');
        $plan_date          = $request->input('plan_date');
        $plan_time          = $request->input('plan_time');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_id) && !empty($staff_id) && !empty($plan_description) && !empty($plan_date) && !empty($plan_time) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session   = get_current_session();

                if (!empty($plan_schedule_id)){

                    $plan_schedule = PlanSchedule::find($plan_schedule_id);
                    $admin_id = $plan_schedule['admin_id'];
                    $title = "Plan Updated";

                    if (!$plan_schedule){

                        $status = false;
                        $message = 'Plan Schedule not found!';

                        $result_response['status']      = $status;
                        $result_response['message']     = $message;
                        $result_response['data']        = $result_data;

                        return response()->json($result_response, 200);
                    }
                    $success_msg = 'Plan Schedule updated successfully!';
                } else {
                    $plan_schedule           = New PlanSchedule;
                    $success_msg     = 'Plan Schedule saved successfully!';
                    $title         = "New Plan Received";
                }

                $validatior = Validator::make($request->all(), [
                    'staff_id'          => 'required',
                    'plan_description'  => 'required',
                    'plan_date'         => 'required',
                    'plan_time'         => 'required',
                ]);

                if ($validatior->fails()) {

                    $status = false;
                    $message = 'Required field Missings !!';

                    $result_response['status']      = $status;
                    $result_response['message']     = $message;
                    $result_response['data']        = $result_data;

                    return response()->json($result_response, 200);
                } else {

                    DB::beginTransaction();
                    try
                    {
                        $plan_schedule->admin_id           = $admin_id;
                        $plan_schedule->update_by          = $admin_id;
                        $plan_schedule->current_session_id = $session['session_id'];
                        $plan_schedule->staff_id           = $staff_id;
                        $plan_schedule->plan_description   = $plan_description;
                        $plan_schedule->plan_date          = $plan_date;
                        $plan_schedule->plan_time          = $plan_time;
                        $plan_schedule->save();

                        
                        $notification = $this->send_push_notification_staff($plan_schedule->plan_schedule_id,$plan_schedule->staff_id,'plan_scheduled',$admin_id,$plan_description,$title);
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;

                        $result_response['status']      = $status;
                        $result_response['message']     = $message;
                        $result_response['data']        = $result_data;

                        return response()->json($result_response, 200);
                    }
                    DB::commit();
                }

                $status = true;
                $message = $success_msg;

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : getPlanSchedule
     *  To get all plan schedule
     *  @Sandeep on 19 FEB 2019    
     *  @parameters : device_id,csrf_token,staff_id,plan_date
     *  @V2R by Sandeep
    */

    public function getPlanSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $staff_id       = $request->input('staff_id');
        $plan_date      = $request->input('plan_date');
        $session        = get_current_session();

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $plan_schedule = PlanSchedule::where(function($query) use ($staff_id,$plan_date,$session) 
                {   
                    $query->where('current_session_id', $session['session_id']); 

                    if ( $staff_id !='' ){
                        $query->where('staff_id', $staff_id);
                        $query->where('plan_status', 1); 
                    }
                    
                    if ( $plan_date !='' ){
                        $query->where('plan_date', $plan_date); 
                    }
                })
                ->with('getStaff')
                ->orderBy('plan_schedule_id','DESC')
                ->paginate(20);

                if( count($plan_schedule) != 0 ){

                    $plan_schedule_info = [];
                    foreach ($plan_schedule as $plan_schedule_res) {

                        $plan_schedule_info[] = array(
                            'plan_schedule_id'    => check_string_empty($plan_schedule_res['plan_schedule_id']),
                            'admin_id'            => check_string_empty($plan_schedule_res['admin_id']),
                            'staff_id'            => check_string_empty($plan_schedule_res['staff_id']),
                            'staff_name'          => check_string_empty($plan_schedule_res['getStaff']['staff_name']),
                            'plan_description'    => check_string_empty($plan_schedule_res['plan_description']),
                            'plan_date'           => date('d M Y',strtotime($plan_schedule_res['plan_date'])),
                            'plan_time'           => date("g:i a", strtotime($plan_schedule_res['plan_time'])),
                            'plan_status'         => check_string_empty($plan_schedule_res['plan_status']),
                            'plan_created'        => date('d M Y',strtotime($plan_schedule_res['created_at'])),
                        );
                    }

                    $result_data['plan_schedule_info'] = $plan_schedule_info;
                    $lastPage = $plan_schedule->lastPage($plan_schedule_res);

                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : deletePlanSchedule
     *  To delete Plan Schedule
     *  @Sandeep on 19 FEB 2019
     *  @parameters : device_id,csrf_token,plan_schedule_id
     *  @V2R by Sandeep
    */
    public function deletePlanSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id           = $request->input('device_id');
        $csrf_token          = $request->input('csrf_token');
        $plan_schedule_id    = $request->input('plan_schedule_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($plan_schedule_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $plan_schedule = PlanSchedule::find($plan_schedule_id);
                if (!empty($plan_schedule))
                {
                    DB::beginTransaction();
                    try
                    {
                        $title = "Plan Deleted";
                        $notification = $this->send_push_notification_staff($plan_schedule->plan_schedule_id,$plan_schedule->staff_id,'plan_scheduled',$plan_schedule->admin_id,$plan_schedule->plan_description,$title);

                        $plan_schedule->delete();
                        $success_msg = "Plan Schedule deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }


    /*  Function name : changeStatusPlanSchedule
     *  To change status plan schedule
     *  @Sandeep on 19 FEB 2019    
     *  @parameters : device_id,csrf_token,id,status
     *  @V2R by Sandeep
    */
    public function changeStatusPlanSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $status             = $request->input('status');
        $plan_schedule_id   = $request->input('plan_schedule_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($plan_schedule_id) && $status !='' ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                $plan_schedule 		= PlanSchedule::find($plan_schedule_id);

                if ($plan_schedule)
                {
                    $plan_schedule->plan_status  = $status;
                    $plan_schedule->save();
                    $success_msg = "Plan Schedule status update successfully!";

                    $status = true;
                    $message = $success_msg;
                } else {

                    $error_message = "Plan Schedule not found!";
                    $status = false;
                    $message = $error_message;
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $essage = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }


    // Notification function
    public function send_push_notification_staff($module_id,$staff_ids,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($staff_ids) 
        {
            $query->whereIn('staff_id',explode(",",$staff_ids));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $staff['get_staff_admin_info']['admin_id'],
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }

    // Notification function
    // public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type,$admin_id){
    //     $device_ids = [];
    //     $section_info        = Section::where(function($query) use ($class_id,$section_id) 
    //     {
    //         $query->where('class_id',$class_id);
    //         $query->where('section_id',$section_id);
    //     })
    //     ->with('sectionClass')
    //     ->get()->toArray();
    //     $message = "New online content is published, click here to check.";
    //     foreach($section_info as $section){
    //         $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
    //         $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            
    //         $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
    //         $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

    //         $students = api_pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['class_id'],$section['section_id'],$admin_id);
    //         $parents = api_pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['class_id'],$section['section_id'],$admin_id);
           
    //     }
        
    // }


}

