<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Notice\Notice; // Notice Model
use App\Model\Staff\Staff; //Staff Model

use DB;
use Hash;
use DateTime;
use Validator;

class NoticeController extends Controller
{
    /*  Function name : getNotices
     *  To get all notices
     *  @Sumit on 10 JAN 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getNotices(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_id        = $request->input('user_id');
        $user_type      = $request->input('user_type');
        $notice_id      = $request->input('notice_id');
        $class_id       = $request->input('class_id');
        $staff_id       = $request->input('staff_id');
        $notice_name    = $request->input('notice_name');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $notice = Notice::where(function($query) use ($notice_id,$user_type,$user_id,$notice_name,$class_id,$staff_id) 
                {
                    
                    if( $user_type == 2 && !empty($user_id) ){
                        $query->orWhereRaw('FIND_IN_SET('.$user_id.',staff_ids)');
                        $query->orWhere('for_staff', "=", 0);
                    }

                    if( $user_type == 5 && !empty($user_id) ){
                        $query->orWhereRaw('FIND_IN_SET('.$user_id.',class_ids)');
                        $query->orWhere('for_class', "=", 0);
                    }

                    if ( $class_id !='' ) {
                        $query->WhereRaw('FIND_IN_SET('.$class_id.',class_ids)');
                        $query->orWhere('for_class', "=", 0);
                    }

                    if ( $staff_id !='' ) {
                        $query->WhereRaw('FIND_IN_SET('.$staff_id.',staff_ids)');
                        $query->orWhere('for_staff', "=", 0);
                    }

                    if ( $notice_id !='' ) {
                        $query->where('notice_id', $notice_id); 
                    }

                    if ( $notice_name !='' )
                    {

                        $query->where('notice_name', "like", "%{$notice_name}%");
                    }

                })->orderBy('notice_id', 'DESC')->paginate(20);

                if( count($notice) != 0 ){

                    $notice_info  = [];
                    foreach($notice as $notice_res){

                        if($notice_res->class_ids != ''){
                            $classes        = get_selected_classes($notice_res->class_ids);
                            $classes_json   = get_selected_classes_json($notice_res->class_ids);
                        } else {
                            $classes = '';
                            $classes_json = [];
                        }

                        if($notice_res->staff_ids != ''){
                            $staff      = get_selected_staff($notice_res->staff_ids);
                            $staff_json = get_selected_staff_json($notice_res->staff_ids);
                        } else {
                            $staff = '';
                            $staff_json = [];
                        }

                        $for_class = '';
                        if( is_null($notice_res->for_class) ){
                            $for_class = '';
                        } else if($notice_res->for_class == 1){
                            $for_class = 1;
                        } else {
                            $for_class = 0;
                        }

                        $for_staff = '';
                        if( is_null($notice_res->for_staff)){
                            $for_staff = '';
                        } else if($notice_res->for_staff == 1){
                            $for_staff = 1;
                        } else {
                            $for_staff = 0;
                        }

                        $notice_info[] = array(
                            'notice_id'     =>  check_string_empty($notice_res->notice_id),
                            'notice_name'   =>  check_string_empty($notice_res->notice_name),
                            'notice_text'   =>  check_string_empty($notice_res->notice_text),
                            'for_staff'     =>  $for_staff,
                            'for_class'     =>  $for_class,
                            'classes'       =>  check_string_empty($classes),
                            'classes_json'  =>  $classes_json,
                            'staff'         =>  check_string_empty($staff),
                            'staff_json'    =>  $staff_json,
                            'publish_date'  =>  date('d F Y',strtotime($notice_res->created_at)),
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['notice_info'] = $notice_info;
                    $lastPage = $notice->lastPage($notice_res);

                } else {

                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : saveNotices
     *  To add/edit notice
     *  @Sumit on 10 JAN 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function saveNotices(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $notice_name        = $request->input('notice_name');
        $notice_text        = $request->input('notice_text');
        $for_class_flag     = $request->input('for_class');
        $class_ids          = $request->input('class_ids');
        $for_staff_flag     = $request->input('for_staff');
        $staff_ids          = $request->input('staff_ids');
        $notice_id          = $request->input('notice_id');
        $admin_id           = $request->input('admin_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($notice_name) && !empty($notice_text) && !empty($admin_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                if (!empty($notice_id))
                {
                    $notice     = Notice::find($notice_id);
                    $admin_id   = $notice['admin_id'];
                    // print_r($notice);
                    if ( empty($notice) )
                    { 
                        $status = false;
                        $message = 'Notice not found!';
                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                        
                        return response()->json($result_response, 200);
                    }
                    $success_msg = 'Notice updated successfully!';
                }
                else
                {
                    $notice         = New Notice;
                    $success_msg    = 'Notice saved successfully!';
                }
                
                $validatior = Validator::make($request->all(), [
                        'notice_name' => 'required|unique:notice,notice_name,' . $notice_id . ',notice_id',
                        'notice_text' => 'required',
                ]);
                if ($validatior->fails())
                {
                    $status = false;
                    $message = 'Notice name already exists';

                    $result_response['status'] 	    = $status;
                    $result_response['message']     = $message;
                    $result_response['data'] 	    = $result_data;
                    return response()->json($result_response, 200);
                }
                else
                {
                    $class_ids = NULL;
                    if($request->get('class_ids') != ''){
                        $class_ids =  $request->get('class_ids');
                    }
                    $staff_ids = NULL;
                    if($request->get('staff_ids') != ''){
                        $staff_ids =  $request->get('staff_ids');
                    }
                    DB::beginTransaction();
                    try
                    {
                        $notice->admin_id       = $admin_id;
                        $notice->update_by      = $admin_id;
                        $notice->session_id     = $session['session_id'];
                        $notice->notice_name    = $notice_name;
                        $notice->notice_text    = $notice_text;
                        $notice->class_ids      = $class_ids;
                        $notice->staff_ids      = $staff_ids;
                        $notice->for_class      = $for_class_flag;
                        $notice->for_staff      = $for_staff_flag;
                        $notice->save();

                        if (empty($notice_id)){

                            $title = $notice_name;

                            if($for_staff_flag === '0' ){
                                $notification = api_pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_staff',$notice_text,'','',$admin_id,$title);
                            } else {
                                $notification = $this->send_push_notification_staff($notice->notice_id,$staff_ids,'notice_issue',$admin_id,$notice_text,$title);
                            }
                            if($for_class_flag === '0'){
                                $notification = api_pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_student',$notice_text,'','',$admin_id,$title);
                                $notification = api_pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_parent',$notice_text,'','',$admin_id,$title);
                            } else {
                                $notification = $this->send_push_notification_class($notice->notice_id,$class_ids,'notice_issue',$admin_id,$notice_text,$title);
                            }
                        }
                        
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                        return response()->json($result_response, 200);
                    }
                    DB::commit();
                }

                $status = true;
                $message = $success_msg;
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : deleteNotices
     *  To delete notices
     *  @Sumit on 11 JAN 2019    
     *  @parameters : device_id,csrf_token,notice_id
     *  @V2R by Sumit
    */
    public function deleteNotices(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $notice_id          = $request->input('notice_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($notice_id) ){
            
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $notice = Notice::find($notice_id);
                if (!empty($notice))
                {
                    DB::beginTransaction();
                    try
                    {
                        $notice->delete();
                        $success_msg = "Notice deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    // Notification function
    public function send_push_notification_staff($module_id,$staff_ids,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($staff_ids) 
        {
            $query->whereIn('staff_id',explode(",",$staff_ids));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $staff['get_staff_admin_info']['admin_id'],
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        //$title = "New Notice Issued";
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }

    public function send_push_notification_class($module_id,$class_ids,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_ids) 
        {
            $query->whereIn('class_id',explode(",",$class_ids));
        })
        ->with('sectionClass')
        ->get()->toArray();
        //$message = "New Notice Issued";
        //$title = "New Notice Issued";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);

            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = api_pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],$admin_id,"",$title);
            $parents = api_pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],$admin_id,"",$title);
        }
        
    }
}