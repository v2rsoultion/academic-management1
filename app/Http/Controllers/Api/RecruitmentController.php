<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use DateTime;
use Validator;
use Mail;

use App\Model\Recruitment\Job; // Job Model
use App\Model\Recruitment\JobFields; // JobFields Model
use App\Model\Recruitment\JobForm; // JobForm Model
use App\Model\Recruitment\RecruitmentForm; // RecruitmentForm Model
// use App\Model\Recruitment\RecruitmentFormStatus; // RecruitmentFormStatus Model

class RecruitmentController extends Controller
{
    /* Function name : getJobList
     *  To get job list
     *  @Sumit on 14 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getJobList(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $medium         = $request->input('medium');
        $job_name       = $request->input('job_name');
        $job_id         = $request->input('job_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $jobs = Job::where(function($query) use ($medium,$job_name,$job_id) 
                { 
                    $query->where('job_status', 1); 
                    
                    if ( $job_name !='' ) 
                    {
                        $query->where('job_name', "like", "%{$job_name}%");
                    }

                    if ( $medium !='' ) 
                    {
                        $query->where('job_medium_type', $medium); 
                    }

                    if ( $job_id !='' ) 
                    {
                        $query->where('job_id', $job_id); 
                    }
                })
                ->with('getForm')
                ->with('get_medium')
                ->withCount('get_remaining_jobs')
                ->withCount('approved_jobs')
                ->orderBy('job_id','DESC')
                ->paginate(20);


                if( count($jobs) != 0 ){

                    $jobs_info = [];

                    foreach ($jobs as $jobs_res) {

                        if($jobs_res['job_type'] == 0) {
                            $job_type = 'Temporary';
                            $job_durations = $jobs_res['job_durations'].' Month';
                        } else {
                            $job_type = 'Permanent';
                            $job_durations = '';
                        }

                        $remaining_vacancy = $jobs_res['job_no_of_vacancy']-$jobs_res['approved_jobs_count'];

                        $jobs_info[] = array(
                            'job_id'            => check_string_empty($jobs_res['job_id']),
                            'job_name'          => check_string_empty($jobs_res['job_name']),
                            'medium_type_id'    => check_string_empty($jobs_res['get_medium']['medium_type_id']),
                            'job_medium_type'   => check_string_empty($jobs_res['get_medium']['medium_type']),
                            'job_form_id'       => check_string_empty($jobs_res['getForm']['job_form_id']),
                            'form_name'         => check_string_empty($jobs_res['getForm']['form_name']),
                            'job_type'          => check_string_empty($job_type),
                            'job_durations'     => check_string_empty($job_durations),
                            'job_no_of_vacancy' => check_string_empty($jobs_res['job_no_of_vacancy']),
                            'approved_jobs_count' => check_string_empty($jobs_res['approved_jobs_count']),
                            'remaining_vacancy'   => $remaining_vacancy,
                            'job_recruitment'   => check_string_empty($jobs_res['job_recruitment']),
                            'job_description'   => check_string_empty($jobs_res['job_description']),
                        );
                    }

                    $result_data['jobs_info'] = $jobs_info;
                    $lastPage = $jobs->lastPage($jobs_res);  
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : getCandidateofJob
     *  To get candidate of job
     *  @Sumit on 14 FEB 2019    
     *  @parameters : device_id,csrf_token,job_id
     *  @V2R by Sumit
    */
    public function getCandidateofJob(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $job_id         = $request->input('job_id');
        $name           = $request->input('name');
        $status         = $request->input('status');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($job_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $candidates = RecruitmentForm::where(function($query) use ($name,$job_id,$status) 
                { 
                    $query->where('job_id', $job_id); 

                    if ( $status !='' ) 
                    {
                        $query->where('status_type', $status); 
                    }

                    if ( $name !='' ) 
                    {
                        $query->where('staff_name', "like", "%{$name}%");
                    }

                })
                ->with('GetJob')
                ->orderBy('applied_cadidate_id','DESC')
                ->paginate(20);

                if( count($candidates) != 0 ){

                    $candidates_info = [];

                    foreach ($candidates as $candidates_res) {

                        $candidates_info[] = array(
                            'cadidate_id'   => check_string_empty($candidates_res['applied_cadidate_id']),
                            'form_number'   => check_string_empty($candidates_res['form_number']),
                            'name'          => check_string_empty($candidates_res['staff_name']),
                            'email'         => check_string_empty($candidates_res['staff_email']),
                            'mobile_number' => check_string_empty($candidates_res['staff_mobile_number']),
                            'status_type'   => $candidates_res['status_type'],
                        ); 
                    }

                    if( !empty($candidates_info) ){
                        $result_data['candidates_info'] = $candidates_info;
                        $lastPage = $candidates->lastPage($candidates_res);  
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : getInterviewSchedule
     *  To get interview schedule
     *  @Sumit on 14 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getInterviewSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $job_id       = $request->input('job_id');
        $candidate_name = $request->input('candidate_name');
        $candidate_id   = $request->input('candidate_id');
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $candidates = RecruitmentForm::where(function($query) use ($candidate_name,$job_id,$candidate_id) 
                { 
                    $query->where('status_type','!=',2)->where('status_type','!=',3);

                    if ($candidate_id !='' ) 
                    {
                        $query->where('applied_cadidate_id', $candidate_id);
                    }

                    if ($job_id !='' ) 
                    {
                        $query->where('job_id', $job_id);
                    }

                    if ($candidate_name !='' ) 
                    {
                        $query->where('staff_name', "like", "%{$candidate_name}%");
                    }
                })
                // ->with(['GetAppliedCandidateDetails' => function($candidate_query) use ($candidate_name,$job_name,$candidate_id) {
                //     if ( $candidate_name !='' ) 
                //     {
                //         $candidate_query->where('staff_name', "like", "%{$candidate_name}%");
                //     }
                // }])
                ->with(['GetJob' => function($job_query) use ($candidate_name,$job_name) {
                    if ( $job_name !='' ) 
                    {
                        $job_query->where('job_name', "like", "%{$job_name}%");
                    }
                }])
                //->with('GetForm')
                ->orderBy('applied_cadidate_id','DESC')
                ->paginate(20);

               // p($candidates);
                
                if( count($candidates) != 0 ){

                    $candidates_info = [];

                    foreach ($candidates as $candidates_res) {

                        if($candidates_res['datetime'] == "0000-00-00 00:00:00"){ 
                            $candidates_res['datetime'] = ""; 
                        } else { 
                            $candidates_res['datetime'] = date('d F Y : g:i a',strtotime($candidates_res['datetime'])); 
                        }

                        if($candidates_res['message'] == ""){ 
                            $candidates_res['message'] = "Dear #CandidateName, you applied for #jobname in #schoolname, your interview is scheduled on #datetime, we request you to be on time with all the Education and work experience related documents."; 
                        } else { 
                            $candidates_res['message'] = $candidates_res['message']; 
                        }

                        //if( $candidates_res['GetJob']['job_name'] !='' ){
                            $candidates_info[] = array(
                                'cadidate_id'   => check_string_empty($candidates_res['applied_cadidate_id']),
                                'job_id'        => check_string_empty($candidates_res['job_id']),
                                'job_name'      => check_string_empty($candidates_res['GetJob']['job_name']),
                                'form_number'   => check_string_empty($candidates_res['form_number']),
                                'name'          => check_string_empty($candidates_res['staff_name']),
                                'email'         => check_string_empty($candidates_res['staff_email']),
                                'mobile_number' => check_string_empty($candidates_res['staff_mobile_number']),
                                'resume'        => check_string_empty($candidates_res['staff_resume']),
                                'schedule_date_time' => $candidates_res['datetime'],
                                'message'       => $candidates_res['message'],
                                'form_message_via'       => $candidates_res['form_message_via'],
                                'status_type'       => $candidates_res['status_type'],
                            );
                        //}
                    }

                    if( !empty($candidates_info) ){
                        $result_data['candidates_info'] = $candidates_info;
                        $lastPage = $candidates->lastPage($candidates_res);  
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : getSelectedCandidate
     *  To get selected candidate
     *  @Sumit on 15 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getSelectedCandidate(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $job_id         = $request->input('job_id');
        $candidate_name = $request->input('candidate_name');
        $candidate_id   = $request->input('candidate_id');
        $status_type    = $request->input('status_type');
        $date           = $request->input('date');
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $candidates = RecruitmentForm::where(function($query) use ($candidate_name,$job_id,$candidate_id,$status_type,$date) 
                { 
                    $query->where('status_type','!=',0)->where('datetime','<',date('Y-m-d'));

                    if ( $candidate_id !='' ) 
                    {
                        $query->where('applied_cadidate_id', $candidate_id);
                    }

                    if ( $job_id !='' ) 
                    {
                        $query->where('job_id', $job_id);
                    }
                    if ( $status_type !='' ) 
                    {
                        $query->where('status_type', $status_type);
                    }

                    if ( $date !='' ) 
                    {
                        $query->where('datetime', "like", "%{$date}%");
                    }

                    if ( $candidate_name !='' ) 
                    {
                        $query->where('staff_name', "like", "%{$candidate_name}%");
                    }
                })
                // ->with(['GetAppliedCandidateDetails' => function($candidate_query) use ($candidate_name,$job_name,$candidate_id) {
                    
                // }])
                ->with(['GetJob' => function($job_query) use ($candidate_name,$job_name) {
                    if ( $job_name !='' ) 
                    {
                        $job_query->where('job_name', "like", "%{$job_name}%");
                    }
                }])
                //->with('GetForm')
                ->orderBy('applied_cadidate_id','DESC')
                ->paginate(20);
                
                if( count($candidates) != 0 ){

                    $candidates_info = [];

                    foreach ($candidates as $candidates_res) {

                        if($candidates_res['datetime'] == "0000-00-00 00:00:00"){ 
                            $candidates_res['datetime'] = ""; 
                        } else { 
                            $candidates_res['datetime'] = date('d F Y : g:i a',strtotime($candidates_res['datetime'])); 
                        }

                        // if($candidates_res['message'] == ""){ 
                        //     $candidates_res['message'] = "Dear #CandidateName, you applied for #jobname in #schoolname, your interview is scheduled on #datetime, we request you to be on time with all the Education and work experience related documents."; 
                        // } else { 
                        //     $candidates_res['message'] = $candidates_res['message']; 
                        // }

                        if( $candidates_res['GetJob']['job_name'] !='' ){
                            $candidates_info[] = array(
                                'cadidate_id'   => check_string_empty($candidates_res['applied_cadidate_id']),
                                'job_id'        => check_string_empty($candidates_res['job_id']),
                                'job_name'      => check_string_empty($candidates_res['GetJob']['job_name']),
                                'form_number'   => check_string_empty($candidates_res['form_number']),
                                'name'          => check_string_empty($candidates_res['staff_name']),
                                'email'         => check_string_empty($candidates_res['staff_email']),
                                'mobile_number' => check_string_empty($candidates_res['staff_mobile_number']),
                                'schedule_date_time' => $candidates_res['datetime'],
                                'form_message_via'       => $candidates_res['form_message_via'],
                                'status_type'       => $candidates_res['status_type'],
                            );
                        }
                    }

                    if( !empty($candidates_info) ){
                        $result_data['candidates_info'] = $candidates_info;
                        $lastPage = $candidates->lastPage($candidates_res);  
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : scheduleInterview
     *  To schedule interview
     *  @Sumit on 15 FEB 2019    
     *  @parameters : device_id,csrf_token,candidate_id,date_time,message,send_flag
     *  @V2R by Sumit
    */
    public function scheduleInterview(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $candidate_id   = $request->input('candidate_id');
        $date           = $request->input('date');
        $time           = $request->input('time');
        $message        = $request->input('message');
        $send_flag      = $request->input('send_flag');
        $status_type    = $request->input('status_type');   
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($candidate_id) && !empty($status_type)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $recruitment                       = RecruitmentForm::Find($candidate_id); 
                $recruitment->message              = $message;
                $recruitment->datetime             = $date.' '.$time;
                $recruitment->form_message_via     = $send_flag;
                $recruitment->status_type          = $status_type;
                $recruitment->save();
                

                $_REQUEST['name']  = $recruitment->staff_name;
                $_REQUEST['email'] = $recruitment->staff_email;
                $mobile_no         = $recruitment->staff_mobile_number;
                

                if($status_type == 1){
                    $data=['name'=> $recruitment->staff_name ,'rec_message'=> $message, 'date'=> date('d M Y',strtotime($date)),'time'=> date("g:i a", strtotime($time)) ];
                    $message           = $request['message'].''.'   Date : '.date('d M Y',strtotime($request['date'])).''.'   Time : '.date("g:i a", strtotime($request['time']));
                } else if($status_type == 2){
                    $data=['name'=> $recruitment->staff_name ,'rec_message'=> $request->message ];
                    $message           = $request['message'];
                } else if($status_type == 3){
                    $data=['name'=> $recruitment->staff_name ,'rec_message'=> $request->message ];
                    $message           = $request['message'];
                }


                if($send_flag == 0){

                    if($_REQUEST['email'] != ""){
                        Mail::send(['html' => 'mails/recuriment_mail'], $data , function ($message) {
                          $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Recruitment');
                        });
                    }
                }

                if($send_flag == 1){
                    $bulkSms           = send_sms_with_parameters($mobile_no, $message);
                }

                if($send_flag == 2){
                    
                    if($_REQUEST['email'] != ""){
                        Mail::send(['html' => 'mails/recuriment_mail'], $data , function ($message) {
                          $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Recruitment');
                        });
                    }

                    $bulkSms           = send_sms_with_parameters($mobile_no, $message);

                }

                $status = true;
                $message = 'success';
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }



    /* Function name : candidateDetail
     *  To Candidate Detail
     *  @Sandeep on 19 FEB 2019    
     *  @parameters : device_id,csrf_token,candidate_id
     *  @V2R by Sandeep
    */
    public function candidateDetail(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $candidate_id   = $request->input('candidate_id');
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($candidate_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $config_upload_path             = \Config::get('custom.recruitment_form');
                $applied_candidate              = get_candidate_signle_data($candidate_id);
                $applied_candidate              = isset($applied_candidate[0]) ? $applied_candidate[0] : [];

                $config_upload_path    = isset($config_upload_path['display_path']) ? $config_upload_path['display_path'] : [];;
                $status = true;
                $message = 'success';
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['config_upload_path']     = $config_upload_path;
        $result_response['data']        = $applied_candidate;

        return response()->json($result_response, 200);
    }




}
