<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Hash;
use DateTime;

use App\Model\Book\Book; // Book Model
use App\Model\LibraryMember\LibraryMember; // Model 
use App\Model\Student\Student; // Model
use App\Model\Staff\Staff; // Model 
use App\Model\IssueBook\IssueBook; // Model


class LibraryController extends Controller
{

    /*  Function name : getBooks
     *  To get books
     *  @Sumit on 04 Jan 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getBooks(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $book_name      = $request->input('book_name');
        $book_subtitle  = $request->input('book_subtitle');
        $publisher_name = $request->input('publisher_name');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth

            if(validate_token($device_id,$csrf_token) == true){

                $books = Book::where(function($query) use ($request,$type) 
                {
                    if ( $request->get('book_name') !='' ){
                        $query->where('book_name', "like", "%{$request->get('book_name')}%");
                    }

                    if ( $request->get('book_subtitle') !='' ){
                        $query->where('book_subtitle', "like", "%{$request->get('book_subtitle')}%");
                    }

                    if ( $request->get('publisher_name') !='' ){
                        $query->where('publisher_name', "like", "%{$request->get('publisher_name')}%");
                    }

                })->with('getCupBoard')->with('getCupBoardShelf')->with('getCategory')->with('getBookCopies')->withCount([
                    'getBookCopies as total_copies',
                    'getBookCopies AS total_issued_copies' => function ($query) {
                        $query->where('book_copy_status', 1);
                    }
                ])->orderBy('book_id','DESC')->paginate(20);

                if( count($books) !=0 ){

                    $books_info = [];

                    foreach ($books as $books_res) {

                        $books_info[] = array(
                            'book_id'               => check_string_empty($books_res['book_id']),
                            'book_name'             => check_string_empty($books_res['book_name']),
                            'book_subtitle'         => check_string_empty($books_res['book_subtitle']),
                            'book_category_name'    => check_string_empty($books_res['getCategory']['book_category_name']),
                            'book_isbn_no'          => check_string_empty($books_res['book_isbn_no']),
                            'author_name'           => check_string_empty($books_res['author_name']),
                            'edition'               => check_string_empty($books_res['edition']),
                            'publisher_name'        => check_string_empty($books_res['publisher_name']),
                            'book_price'            => check_string_empty($books_res['book_price']),
                            'book_cupboard_name'    => check_string_empty($books_res['getCupBoard']['book_cupboard_name']),
                            'book_cupboardshelf_name'    => check_string_empty($books_res['getCupBoardShelf']['book_cupboardshelf_name']),
                            'total_copies'          => check_string_empty($books_res['total_copies']),
                            'total_issued_copies'   => check_string_empty($books_res['total_issued_copies']),
                        );
                    }

                    $result_data['books_info'] = $books_info;
                    $lastPage = $books->lastPage($books_res);
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
    
        return response()->json($result_response, 200);
    }

    /*  Function name : getBookDetails
     *  To get book details
     *  @Sumit on 04 Jan 2019    
     *  @parameters : device_id,csrf_token,book_id
     *  @V2R by Sumit
    */
    public function getBookDetails(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $book_id        = $request->input('book_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($book_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $books = Book::where('book_id',$book_id)->with('getCupBoard')->with('getCupBoardShelf')->with('getCategory')

                ->with(['getBookCopies' => function($query) use($request) {
                    
                    if (!empty($request) && !empty($request->get('book_unique_id')))
                    {
                        $query->where('book_unique_id', "like", "%{$request->get('book_unique_id')}%");
                    }

                }])
                ->with('getBookVendor')->first();                

                if( !empty($books) ){

                    $book_copies = [];

                    foreach ($books['getBookCopies'] as $book_copies_res) {

                        $book_copies[] = array(
                            'book_unique_id'        => $book_copies_res['book_unique_id'],
                            'exclusive_for_staff'   => $book_copies_res['exclusive_for_staff'],
                            'book_copy_status'      => $book_copies_res['book_copy_status'],
                        );
                    }

                    $books_info = array(
                        'book_id'               => check_string_empty($books['book_id']),
                        'book_name'             => check_string_empty($books['book_name']),
                        'book_subtitle'         => check_string_empty($books['book_subtitle']),
                        'book_category_name'    => check_string_empty($books['getCategory']['book_category_name']),
                        'book_isbn_no'          => check_string_empty($books['book_isbn_no']),
                        'author_name'           => check_string_empty($books['author_name']),
                        'edition'               => check_string_empty($books['edition']),
                        'book_type'             => $books['book_type'],
                        'book_price'            => check_string_empty($books['book_price']),
                        'book_remark'           => check_string_empty($books['book_remark']),
                        'publisher_name'        => check_string_empty($books['publisher_name']),
                        'book_vendor_name'      => check_string_empty($books['getBookVendor']['book_vendor_name']),
                        'book_cupboard_name'    => check_string_empty($books['getCupBoard']['book_cupboard_name']),
                        'book_cupboardshelf_name'    => check_string_empty($books['getCupBoardShelf']['book_cupboardshelf_name']),
                        'book_copies'           => $book_copies
                    );

                    $result_data['books_info'] = $books_info;
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getLibraryMember
     *  To get library member
     *  @Sumit on 04 Jan 2019    
     *  @parameters : device_id,csrf_token,member_type
     *  @V2R by Sumit
    */
    public function getLibraryMenber(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $member_type    = $request->input('member_type');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($member_type) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $library_members = LibraryMember::where('library_member_type', $member_type)->select('library_member_type','member_id')->orderBy('library_member_id','DESC')->get()->toArray();

                if( !empty($library_members) ){

                    $student_ids_array = [];

                    $staff_ids_array = [];

                    foreach ($library_members as $library_members_res) {

                        if( $library_members_res['library_member_type'] == 1 ){
                            $student_ids_array[] = $library_members_res['member_id'];
                        } else if ($library_members_res['library_member_type'] == 2) {
                            $staff_ids_array[] = $library_members_res['member_id'];
                        }

                    }

                    // Get Student Data

                    if( $member_type == 1 ){

                        $student_data  = Student::where(function($query) use ($student_ids_array,$request) 
                        {
                            $query->whereIn('student_id',$student_ids_array);

                            if ( $request->get('enroll_no') !='' )
                            {
                                $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                            }
                            
                            if ( $request->get('member_name') !='' )
                            {
                                $query->where('student_name', "like", "%{$request->get('member_name')}%");
                            }

                        })->select('student_id','student_name','student_enroll_number','student_roll_no','student_image','student_image')
                        ->with('getStudentAcademic.getCurrentClass')
                        ->with('getStudentAcademic.getCurrentSection')
                        ->orderBy('student_name','ASC')
                        ->paginate(20);

                        if( count($student_data) !=0 ){

                            $student_info = [];

                            foreach ($student_data as $student) {

                                $total_issued_books = IssueBook::where([ ['member_id', $student['student_id']], ['member_type', $member_type], ['issued_book_status', 1] ])->get()->count();

                                $profile_image = check_file_exist($student['student_image'], 'student_image');

                                $student_info[] = array(
                                    'student_id'        => check_string_empty($student['student_id']),
                                    'student_name'      => check_string_empty($student['student_name']),
                                    'enroll_no'         => check_string_empty($student['student_enroll_number']),
                                    'student_roll_no'   => check_string_empty($student['student_roll_no']),
                                    'current_class'     => check_string_empty($student['getStudentAcademic']['getCurrentClass'                      ]['class_name']),
                                    'current_section'   => check_string_empty($student['getStudentAcademic']['getCurrentSection']['section_name']),
                                    'profile_image'         => $profile_image,
                                    'total_issued_books'    => $total_issued_books,
                                );
                            }

                            $result_data['student_info'] = $student_info;
                            $lastPage = $student_data->lastPage($student);
                            $status = true;
                            $message = 'success';

                        } else {
                            $status = false;
                            $message = 'No record found';
                        }

                    } else if($member_type == 2){

                        $staff_data  = Staff::where(function($query) use ($staff_ids_array,$request) 
                        {
                            $query->whereIn('staff_id',$staff_ids_array);

                            if ( $request->get('member_name') !='' )
                            {
                                $query->where('staff_name', "like", "%{$request->get('member_name')}%");
                            }

                        })->select('staff_id','staff_name','staff_profile_img','designation_id')
                        ->with('getDesignation')
                        ->orderBy('staff_name','ASC')
                        ->paginate(20);

                        if( count($staff_data) !=0 ){

                            $staff_info = [];
                            foreach ($staff_data as $staff) {
                                $total_issued_books = IssueBook::where([ ['member_id', $staff['staff_id']], ['member_type', $member_type], ['issued_book_status', 1] ])->get()->count();

                                $profile_image = check_file_exist($staff['staff_profile_img'], 'staff_profile');

                                $staff_info[] = array(
                                    'staff_id'          => check_string_empty($staff['staff_id']),
                                    'staff_name'        => check_string_empty($staff['staff_name']),
                                    'profile_image'     => $profile_image,
                                    'designation_name'  => check_string_empty($staff['getDesignation']['designation_name']),
                                    'total_issued_books'    => $total_issued_books,
                                );
                            }

                            $result_data['staff_info'] = $staff_info;
                            $lastPage = $staff_data->lastPage($staff);
                            $status = true;
                            $message = 'success';
                        } else {
                            $status = false;
                            $message = 'No record found';
                        }
                    }
                    
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
    
        return response()->json($result_response, 200);
    }

    /*  Function name : getIssuedBook
     *  To get library member
     *  @Sumit on 04 Jan 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getIssuedBook(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $issued_books = IssueBook::where(function($query) use ($request,$type) 
                {
                    if ( $request->get('issued_from_date') !='' ){
                        $query->where('issued_from_date', "=", "{$request->get('issued_from_date')}");
                    }

                    if ( $request->get('issued_to_date') !='' ){
                        $query->where('issued_to_date', "=", "{$request->get('issued_to_date')}");
                    }

                })->where('issued_book_status',1)

                ->join('books', function($join) use ($request){
                    $join->on('books.book_id', '=', 'issued_books.book_id');
                    
                    if ( $request->get('book_name') !='' ){
                        $join->where('book_name', "LIKE", "%{$request->get('book_name')}%");
                    }

                })


                ->with('getBook')->orderBy('issued_book_id','DESC')->paginate(20);

                if( count($issued_books) !=0 ){

                    $issued_books_info = [];

                    foreach ($issued_books as $issued_books_res) {

                        $issued_books_info[] = array(
                            'book_id'           => check_string_empty($issued_books_res['getBook']['book_id']),
                            'book_name'         => check_string_empty($issued_books_res['getBook']['book_name']),
                            'book_isbn_no'      => check_string_empty($issued_books_res['getBook']['book_isbn_no']),
                            'issued_from_date'  => check_string_empty($issued_books_res['issued_from_date']),
                            'issued_to_date'    => check_string_empty($issued_books_res['issued_to_date']),
                        );
                    }

                    $result_data['issued_books_info'] = $issued_books_info;
                    $lastPage = $issued_books->lastPage($issued_books_res);
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);

    }

}

