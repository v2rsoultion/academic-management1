<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Admin; // Model
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{   

    use SendsPasswordResetEmails;

    private function findUsername($admin_type,$username){
        if($admin_type == 1 || $admin_type == 2 || $admin_type == 3) 
        {
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
        if($admin_type == 4)
        {
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'enroll_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
    }

    public function broker()
    {
        return Password::broker('admins');
    }

    /*  Function name : forgotPassword
     *  To forgot password
     *  @Sumit on 25 JAN 2019    
     *  @parameters : device_id,csrf_token,username,admin_type
     *  @V2R by Sumit
    */
    public function forgotPassword(Request $request){

        /*  
         * Variable initia  lization
         */
        $result_data = [];
        $arr_response = [];
        /*
         * input variables
         */

        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $username  	= $request->input('username');
        $admin_flag = $request->input('admin_flag');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($username) && $admin_flag != ''){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $user_login_from = $this->findUsername($admin_flag,$username);

                if($admin_flag == 1 || $admin_flag == 2 || $admin_flag == 3){
                    if( $user_login_from == 'mobile_no' ){
                        $admin = DB::table('admins')->where('mobile_no', $username)->where('admin_type',$admin_flag)->first();

                        if( !empty($admin) ){
                            $token = str_random(64);
                            DB::table('admin_password_resets')->insert(
                                ['mobile_no' => $username, 'token' => Hash::make($token), 'created_at' => date('Y-m-d H:i:s')]
                            );
                            // Short URL
                            $base_url  = env('APP_URL');
                            $reset_url = $base_url."/academic-management/admin-panel/password/reset/".$token."/".$username;
                            // when website is real hosting name then create short url  
                            $reset_short_url = shortenUrl($reset_url);
                            $message   = "Reset Your Password Click here >>".$reset_short_url['short_url'];
                            $bulkSms   = send_sms_with_parameters($username, $message);

                            $status = true;
                            $message = 'SMS has been Sent successfully!';
                        } else {
                            $status = false;
                            $message = 'Entered mobile number is not registered with us';
                        }
                    } else {
                        $admin = DB::table('admins')->where('email', $username)->where('admin_type',$admin_flag)->first();

                        if( !empty($admin) ){
                            $response = $this->broker()->sendResetLink(['email' => $username]);
                            if($response === Password::RESET_LINK_SENT) {
                                $status = true;
                                $message = 'Mail has been sent successfully! Please check your email'; 
                            }
                        }  else {
                            $status = false;
                            $message = 'Entered email address is not registered with us';
                        }
                    }
                }

                if($admin_flag == 4){
                    if( $user_login_from == 'enroll_no' ){
                        
                        $admin = Admin::where('enroll_no', $username)->where('admin_type',$admin_flag)->with('adminStudent.getParent')->first();

                        
                        if( !empty($admin) ){
                            $token = str_random(64);
                            DB::table('admin_password_resets')->insert(
                                ['enroll_no' => $username, 'token' => Hash::make($token), 'created_at' => date('Y-m-d H:i:s')]
                            );
                            
                            $mobile_no = $admin['adminStudent']['getParent']['student_login_contact_no'];

                            // Short URL
                            $base_url  = env('APP_URL');
                            $reset_url = $base_url."/academic-management/admin-panel/password/reset/".$token."/".$username;
                            // when website is real hosting name then create short url  
                            $reset_short_url = shortenUrl($reset_url);
                            $message   = "Reset Your Password Click here >>".$reset_short_url['short_url'];
                            $bulkSms   = send_sms_with_parameters($mobile_no, $message);

                            $status = true;
                            $message = 'SMS has been Sent successfully!';
                        } else {
                            $status = false;
                            $message = 'Invalid Username';
                        }

                    } else {
                        $admin = DB::table('admins')->where('email', $username)->where('admin_type',$admin_flag)->first();

                        if( !empty($admin) ){
                            $response = $this->broker()->sendResetLink(['email' => $username]);
                            if($response === Password::RESET_LINK_SENT) {
                                $status = true;
                                $message = 'Mail has been sent successfully! Please check your email'; 
                            }
                        }  else {
                            $status = false;
                            $message = 'Entered email address is not registered with us';
                        }
                    }
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 		= $status;
        $result_response['message'] 	= $message;
        $result_response['data'] 		= $result_data;
        
        return response()->json($result_response, 200);
    }
}
