<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TaskManager\Task; // Model
use App\Model\TaskManager\TaskResponse; // Model
use App\Model\Admin\Admin; // Model
use App\Model\Classes\Classes;
use App\Model\Section\Section;
use App\Model\Staff\Staff;
use App\Model\Student\Student;
use DB;
use Hash;
use DateTime;
use Validator;

class TaskManagerController extends Controller
{


    /*  Function name : saveTaskManager
     *  To add/edit online Task manager 
     *  @Sandeep on 22 FEB 2019    
     *  @parameters : device_id,csrf_token,task_id,admin_id,task_name,task_priority,task_type,class_id,section_id,student_id,staff_id,task_date,task_description,task_file,task_status
     *  @V2R by Sandeep
    */

    public function saveTaskManager(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $task_id            = $request->input('task_id');
        $admin_id           = $request->input('admin_id');
        $task_name          = $request->input('task_name');
        $task_priority      = $request->input('task_priority');
        $task_type          = $request->input('task_type');
        $class_id           = $request->input('class_id');
        $section_id         = $request->input('section_id');
        $student_id         = $request->input('student_id');
        $staff_id           = $request->input('staff_id');
        $task_date          = $request->input('task_date');
        $task_description   = $request->input('task_description');
        $task_status        = $request->input('task_status');

//          p($request->all());

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_id) && !empty($task_name) && !empty($task_date) && !empty($task_description) && !empty($task_type)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if (!empty($task_id)){

                    $task = Task::find($task_id);
                    $admin_id = $task['admin_id'];
                    $title = $task_name;

                    if (!$task){

                        $status = false;
                        $message = 'Task not found!';

                        $result_response['status']      = $status;
                        $result_response['message']     = $message;
                        $result_response['data']        = $result_data;

                        return response()->json($result_response, 200);
                    }
                    $success_msg = 'Task updated successfully!';
                } else {
                    $task     = New Task;
                    $success_msg     = 'Task saved successfully!';
                    $title = $task_name;
                }

                $validatior = Validator::make($request->all(), [
                    'task_name'         => 'required',
                    'task_priority'     => 'required',
                    'task_date'         => 'required',
                    'task_description'  => 'required',
                    'task_type'         => 'required',
                ]);

                if ($validatior->fails()) {

                    $status = false;
                    $message = 'Required field Missings !!';

                    $result_response['status']      = $status;
                    $result_response['message']     = $message;
                    $result_response['data']        = $result_data;

                    return response()->json($result_response, 200);
                } else {

                    DB::beginTransaction();
                    try
                    {   

                        $task->admin_id           = $admin_id;
                        $task->update_by          = $admin_id;
                        $task->task_name          = $task_name;
                        $task->task_priority      = $task_priority;
                        $task->task_date          = $task_date;
                        $task->task_description   = $task_description;
                        $task->task_type          = $task_type;
                        $task->class_id           = $class_id;
                        $task->section_id         = $section_id;
                        $task->staff_id           = $staff_id;
                        $task->student_id         = $student_id;
                        $task->task_status        = $task_status;
                        if ($request->hasFile('task_file'))
                        {
                            if (!empty($id)){
                                $task_file = check_file_exist($task->task_file, 'task_file');
                                if (!empty($task_file))
                                {
                                    unlink($task_file);
                                } 
                            }

                            $file                          = $request->file('task_file');
                            $config_upload_path            = \Config::get('custom.task_file');
                            $destinationPath               = public_path() . $config_upload_path['upload_path'];
                            $ext                           = $file->getClientOriginalExtension();
                            $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
                            $filename                      = $name.mt_rand(0,100000).time().'.'.$ext;
                            $file->move($destinationPath, $filename);
                            $task->task_file = $filename;
                        }
                        $task->save();
                        
                        if($task_type == 1){
                            $notification = $this->send_push_notification_student($task->task_id,$task->student_id,'task_add_student',$admin_id,$task_description,$title);
                        } else {
                            $notification = $this->send_push_notification_staff($task->task_id,$task->staff_id,'task_add_staff',$admin_id,$task_description,$title);
                        }
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;

                        $result_response['status']      = $status;
                        $result_response['message']     = $message;
                        $result_response['data']        = $result_data;

                        return response()->json($result_response, 200);
                    }
                    DB::commit();
                }

                $status = true;
                $message = $success_msg;

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : getTaskManager
     *  To get all Task Manager
     *  @Sandeep on 22 FEB 2019    
     *  @parameters : device_id,csrf_token,task_name,task_type,task_date,user_id
     *  @V2R by Sandeep
    */

    public function getTaskManager(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $task_name      = $request->input('task_name');
        $task_id        = $request->input('task_id');
        $task_type      = $request->input('task_type');
        $task_date      = $request->input('task_date');
        $user_id        = $request->input('user_id');


        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $task = Task::where(function($query) use ($task_name,$task_type,$task_date,$user_id,$task_id) 
                {   
                    if ( $task_name !='' ){
                        $query->where('task_name', 'Like', '%'.$task_name.'%');
                    }

                    if ( $task_id !='' ){
                        $query->where('task_id', $task_id); 
                    }
                    if ( $task_type !='' ){
                        $query->where('task_type', $task_type); 
                    }

                    if ( $task_type == 1 &&  $user_id!= "" ){
                        $query->where('tasks.student_id', $user_id); 
                    }

                    if ( $task_type == 2 &&  $user_id!= ""  ){
                        $query->where('staff_id', $user_id); 
                    }
                    
                    if ( $task_date !='' ){
                        $query->where('task_date', $task_date); 
                    }
                })->with('getStudent')->with('getSiblingClass')->with('getSections')->with('getStaff')->orderBy('task_id','DESC')->paginate(20);

                if( count($task) != 0 ){

                    
                    $task_manager_info = [];
                    foreach ($task as $task_res) {

                        $config_upload_path            = \Config::get('custom.task_file');
                        if($task_res['task_file'] != ""){
                            $task_res['task_file']   = $config_upload_path['display_path'].$task_res['task_file'];
                        } else {
                            $destinationPath   = "";
                        }

                        $task_manager_info[] = array(
                            'task_id'           => check_string_empty($task_res['task_id']),
                            'admin_id'          => check_string_empty($task_res['admin_id']),
                            'task_name'         => check_string_empty($task_res['task_name']),
                            'task_priority'     => $task_res['task_priority'],
                            'task_type'         => check_string_empty($task_res['task_type']),
                            'class_id'          => check_string_empty($task_res['class_id']),
                            'class_name'        => check_string_empty($task_res['getSiblingClass']['class_name']),
                            'section_id'        => check_string_empty($task_res['section_id']),
                            'section_name'      => check_string_empty($task_res['getSections']['section_name']),
                            'student_id'        => check_string_empty($task_res['student_id']),
                            'student_name'      => check_string_empty($task_res['getStudent']['student_name']),
                            'staff_id'          => check_string_empty($task_res['staff_id']),
                            'staff_name'        => check_string_empty($task_res['getStaff']['staff_name']),
                            'task_date'         => date('d M Y',strtotime($task_res['task_date'])),
                            'task_description'  => check_string_empty($task_res['task_description']),
                            'task_file'         => check_string_empty($task_res['task_file']),
                            'task_status'       => $task_res['task_status'],
                            'created_at'        => date('d M Y',strtotime($task_res['created_at'])),
                        );
                    }

                    $result_data['task_manager_info'] = $task_manager_info;
                    $lastPage = $task->lastPage($task_res);

                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : deleteTaskManager
     *  To delete task manager
     *  @Sandeep on 22 FEB 2019
     *  @parameters : device_id,csrf_token,task_id
     *  @V2R by Sandeep
    */
    public function deleteTaskManager(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id       = $request->input('device_id');
        $csrf_token      = $request->input('csrf_token');
        $task_id         = $request->input('task_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($task_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $task = Task::find($task_id);
                if (!empty($task))
                {
                    DB::beginTransaction();
                    try
                    {
                        $title = "Task Deleted";
                        //$notification = $this->send_push_notification_staff($plan_schedule->task_id,$plan_schedule->staff_id,'plan_scheduled',$plan_schedule->admin_id,$plan_schedule->plan_description,$title);

                        $task->delete();
                        $success_msg = "Task deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }


    /*  Function name : changeStatusTaskManager
     *  To change status task manager
     *  @Sandeep on 22 FEB 2019    
     *  @parameters : device_id,csrf_token,task_id,status
     *  @V2R by Sandeep
    */
    public function changeStatusTaskManager(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $status             = $request->input('status');
        $task_id   = $request->input('task_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($task_id) && $status !='' ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                $task 		= Task::find($task_id);

                if ($task)
                {
                    $task->task_status  = $status;
                    $task->save();
                    $success_msg = "Task status update successfully!";

                    $status = true;
                    $message = $success_msg;
                } else {

                    $error_message = "Task not found!";
                    $status = false;
                    $message = $error_message;
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $essage = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }



    /*  Function name : add_task_response
     *  To add task response 
     *  @Sandeep on 22 FEB 2019    
     *  @parameters : device_id,csrf_token,task_id,message,sender_type,file
     *  @V2R by Sandeep
    */

    public function add_task_response(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $task_id            = $request->input('task_id');
        $message            = $request->input('message');
        $sender_type        = $request->input('sender_type');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($task_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $task_record     = Task::where('task_id',$task_id)->with('getStudent')->with('getStaff')->first();              
                $message_date    = '';

                //p($task_record['getStudent']->student_name);

                $taskresponse = New TaskResponse();
                DB::beginTransaction();
                try
                {
                    $taskresponse->task_id          = $task_id;
                    $taskresponse->sender_type      = $sender_type;
                    $taskresponse->admin_id         = $task_record->admin_id;
                    $taskresponse->student_id       = $task_record->student_id;
                    $taskresponse->staff_id         = $task_record->staff_id;
                    $taskresponse->response_text    = $message;

                    if ($request->hasFile('file'))
                    {
                        $file                          = $request->file('file');
                        $config_upload_path            = \Config::get('custom.task_file');
                        $destinationPath               = public_path() . $config_upload_path['upload_path'];
                        $ext                           = $file->getClientOriginalExtension();
                        $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
                        $filename                      = $name.'.'.$ext;
                        $file->move($destinationPath, $filename);

                        $response_file = $config_upload_path['display_path'].$filename;  
                        $taskresponse->response_file   = $filename;
                    }
                    $taskresponse->save();

                    if($sender_type == 0){
                        $admin_info = Admin::where('admin_id',$task_record->admin_id)->first();
                        $message = $admin_info->admin_name.' : '.$message;
                    } else if($sender_type == 1){
                        $message = $task_record['getStudent']->student_name.' : '.$message;
                    } else if($sender_type == 2){
                        $message = $task_record['getStaff']->staff_name.' : '.$message;
                    }


                    $title = $task_record->task_name;
                    if($sender_type == 1 || $sender_type == 2){
                        $notification = $this->send_push_notification_admin($task_id,$task_record->admin_id,'task_response_add',$task_record->admin_id,$message,$title);
                    }

                    if($sender_type == 0 && $task_record->student_id != ""){
                        $notification = $this->send_push_notification_student($task_id,$task_record->student_id,'task_response_add',$task_record->admin_id,$message,$title);
                    }
                    if($sender_type == 0 && $task_record->staff_id != ""){
                        $notification = $this->send_push_notification_staff($task_id,$task_record->staff_id,'task_response_add',$task_record->admin_id,$message,$title);
                    }

                    $message_date = $taskresponse->created_at;
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $status = false;
                    $message = $error_message;

                    $result_response['status']      = $status;
                    $result_response['message']     = $message;
                    $result_response['data']        = $result_data;

                    return response()->json($result_response, 200);
                }
                DB::commit();
                
                $success_msg     = 'Task response saved successfully!';
                $title = "Task Response Added";

                $status = true;
                $message = $success_msg;

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }



    /*  Function name : view_task_response
     *  To View task response 
     *  @Sandeep on 22 FEB 2019    
     *  @parameters : device_id,csrf_token,task_name,task_type,task_date,user_id
     *  @V2R by Sandeep
    */

    public function view_task_response(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $task_id        = $request->input('task_id');
        

        if (!empty($request->input()) && !empty($csrf_token) && !empty($task_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $task_record    = TaskResponse::where('task_id',$task_id)->orderBy('task_response_id', 'DESC')->paginate(20);

                if(count($task_record) != 0 ){
                    
                    $TaskMnagerData = [];
                    foreach($task_record as $tm){     

                        $task_rec  = Task::where('task_id',$tm['task_id'])->first();

                        $response_file = check_file_exist($tm['response_file'], 'task_file');

                        $TaskMnagerData[] = array(
                            'task_response_id' => $tm['task_response_id'],
                            'task_id'          => $tm['task_id'],
                            'task_name'        => $task_rec['task_name'],
                            'sender_type'      => $tm['sender_type'],
                            'response_text'    => $tm['response_text'],
                            'response_file'    => $response_file,
                            'time'             => time_elapsed_string($tm['created_at']),
                            'date'             => date('d M Y',strtotime($tm['created_at'])), 
                        );
                        
                    }

                    $result_data['TaskMnagerData'] = $TaskMnagerData;
                    $lastPage = $task_record->lastPage($tm);

                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }



    //Notification function
    public function send_push_notification_staff($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($user_id) 
        {
            $query->whereIn('staff_id',explode(",",$user_id));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $user_id,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }

    //Notification function
    public function send_push_notification_student($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $student_info        = Student::where(function($query) use ($user_id) 
        {
            $query->whereIn('student_id',explode(",",$user_id));
        })
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();

        foreach($student_info as $student){
            if($student['get_admin_info']['fcm_device_id'] != "" && $student['get_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_admin_info']['fcm_device_id'];
            }
            if($student['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student['get_parent']['get_parent_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_parent']['get_parent_admin_info']['fcm_device_id'];
                $parent_admin_id = $student['get_parent']['get_parent_admin_info']['admin_id'];
            }

            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => $user_id,
                'parent_admin_id' => $parent_admin_id,
                'staff_admin_id' => Null,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    //Notification function
    public function send_push_notification_admin($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $admin_info    = Admin::where(function($query) use ($user_id) 
        {
            $query->whereIn('admin_id',explode(",",$user_id));
        })
        ->get()->toArray();

        foreach($admin_info as $admin){
            if($admin['fcm_device_id'] != "" && $admin['notification_status'] == 1){
                $device_ids[] = $admin['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => Null,
                'school_admin_id' => $admin_id,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }
   


}

