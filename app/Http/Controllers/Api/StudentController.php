<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Hash;
use DateTime;

use App\Model\Student\Student; // School Model
use App\Model\Student\StudentAcademic; // StudentAcademic Model
use App\Model\Student\StudentAcademicHistory; // StudentAcademicHistory Model
use App\Model\StudentParentGroup\StudentParentGroup; //Model
use App\Model\StudentParentGroup\MapStudentParentGroup; //Model
use App\Model\Student\StudentDocument; // StudentDocument Model
use App\Model\Classes\Classes; // Classes Model
use App\Model\Section\Section; // Section Model
use App\Model\StudentSubjectManage\StudentSubjectManage; // StudentSubjectManage Model
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // SubjectSectionMapping Model


class StudentController extends Controller
{
    /* Function name : getStudentsDetails
     *  To get Students basic details
     *  @Sumit on 12 Oct 2018	
     *  @parameters : device_id,csrf_token,name
    */
    public function getStudentsDetails(Request $request) {
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;
        
        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $class_id   = $request->input('class_id');
        $section_id = $request->input('section_id');
        $flag       = $request->input('flag');
        $lastPage 	= ''; 

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $student = Student::leftJoin('student_academic_info', 'student_academic_info.student_id', '=', 'students.student_id')->leftJoin('student_parents', 'student_parents.student_parent_id', '=', 'students.student_parent_id')->where(function($query) use($request){

                    if (!empty($request) && $request->get('name') !=''){
                        $query->where('student_name', "like", "%{$request->get('name')}%");
                    }

                    if (!empty($request) && $request->get('father_name') !='' ){
                        $query->where('student_parents.student_father_name', "like", "%{$request->get('father_name')}%");
                    }

                    if (!empty($request) && $request->get('roll_no') !='') {
                        $query->where('student_roll_no', $request->get('roll_no'));
                    }

                    if (!empty($request) && $request->get('enroll_number') !=''){
                        $query->where('student_enroll_number', $request->get('enroll_number'));
                    }

                    if (!empty($request) && $request->get('class_id') !='' ){
                        $query->where('student_academic_info.current_class_id', $request->get('class_id'));
                    }

                    if (!empty($request) && $request->get('section_id') !=''){
                        $query->where('student_academic_info.current_section_id', $request->get('section_id'));
                    }

                    if (!empty($request) && $request->get('medium_id') !=''){
                        $query->where('medium_type', $request->get('medium_id'));
                    }

                })->with('getParent')->with('getTitle')->with('getStudentMedium')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->select('students.student_id', 'student_name','student_roll_no','student_enroll_number','student_academic_info.current_class_id','student_academic_info.current_section_id', 'medium_type', 'students.student_parent_id', 'student_parents.student_father_name','student_image','students.title_id')->orderBy('student_name', 'asc');

                if($flag == 1){
                    $perpage    = $student->count();
                    $student    = $student->paginate($perpage);
                } else {
                    $student = $student->paginate(20);
                }

            	if(count($student) != 0){

            		foreach($student as $value){
                        $profile_image = check_file_exist($value->student_image, 'student_image');

                        $st_exclude_sub = StudentSubjectManage::where([ ['class_id', '=', $class_id], ['section_id', '=', $section_id], ['student_id', '=', $value['student_id']] ])->select('subject_id')->get()->toArray();

                        $st_exclude_sub_arr = []; 

                        if( count($st_exclude_sub) !=0 ){
                            foreach($st_exclude_sub as $st_exclude_sub_res){
                                $st_exclude_sub_arr[] = $st_exclude_sub_res['subject_id'];
                            }
                        }

                        $get_student_subjects = SubjectSectionMapping::where(function($query) use ($request,$class_id,$section_id,$st_exclude_sub_arr) 
                        {
                            if ( !empty($class_id) ){
                                $query->where('subject_section_map.class_id', "=", $class_id);
                            }    

                            if ( !empty($section_id) ){
                                $query->where('subject_section_map.section_id', "=", $section_id);
                            }

                            $query->whereNotIn('subject_section_map.subject_id', $st_exclude_sub_arr);

                        })->with('getSubjects')->get();

                        $subject_arr= [];
                    
                        foreach ($get_student_subjects as $get_student_subjects_res) {

                            $subject_arr[] = array(
                                'subject_id'   => $get_student_subjects_res['getSubjects']['subject_id'],
                                'subject_name' => $get_student_subjects_res['getSubjects']['subject_name']
                            );
                        }

            			$result_data[] = array(
            				'student_id'			=> check_string_empty($value->student_id),
                            'student_title'         => check_string_empty($value['getTitle']['title_name']),
            				'student_name'			=> check_string_empty($value->student_name),
            				'student_roll_no'		=> check_string_empty($value->student_roll_no),
                            'student_enroll_number'	=> check_string_empty($value->student_enroll_number),
                            'student_class_name'	=> check_string_empty($value['getStudentAcademic']['getCurrentClass']['class_name']),
                            'class_id'		        => check_string_empty($value['getStudentAcademic']['getCurrentClass']['class_id']),
                            'student_section_name'	=> check_string_empty($value['getStudentAcademic']['getCurrentSection']['section_name']),
                            'section_id'		        => check_string_empty($value['getStudentAcademic']['getCurrentSection']['section_id']),
            				'medium_type'	        => check_string_empty($value['getStudentMedium']['medium_type']),
            				'student_father_name'	=> check_string_empty($value['getParent']['student_father_name']),
            				'student_image'	        => $profile_image,
            				'subject_info'	        => $subject_arr,
            			);
            		}

            		$lastPage = $student->lastPage($value);            	
            		$status = true;
                	$message = 'success';
            	}else{
            		$status = false;
                	$message = 'No record found';	
            	}

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        }else{
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 		= $status;
        $result_response['message'] 	= $message;
        $result_response['last_page'] 	= $lastPage;
        $result_response['data'] 		= $result_data;
        return response()->json($result_response, 200);

    }

    /*  Function name : getSingleStudentDetail
     *  To get single student all details
     *  @Sumit on 15 Oct 2018    
     *  @parameters : device_id,csrf_token,student_id
    */
    public function getSingleStudentDetail(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $student_id   = $request->input('student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($student_id)){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Getting student details
                $student = Student::where('student_id', $student_id)->with('getTitle')->with('getStudentAcademic.getAdmitSession')->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')->with('getStudentAcademic.getStudentGroup')->with('getStudentAcademic.getStudentStream')->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->with('getStudentCaste')->with('getStudentReligion')->with('getStudentNationality')->with('getSiblingClass')->with('getParent')->with('getStudentMedium')->with('getStudentHealth')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')->with('getPermanentCountry')->with('getStudentAcademicHistory.getSession')->with('getStudentAcademicHistory.getClass')->with('getStudentAcademic.getCurrentClass.getBoard')->first();

                if(!empty($student)){

                // -- Basic Information start here

                    //  Getting student type
                    $arr_type     = \Config::get('custom.student_type');
                    $student_type = '';
                    if( $student->student_type !='' ){
                        $student_type = $arr_type[$student->student_type];
                    }

                    // Getting gender
                    if( $student->student_gender == 0 ){
                        $student_gender = 'Boy';
                    } else {
                        $student_gender = 'Girl';
                    }

                    if( $student->student_status == 0 ){
                        $student_status = 'Leaved';
                    } else {
                        $student_status = 'Studying';
                    }

                    $profile_image = check_file_exist($student->student_image, 'student_image');

                    $basic_info = array(
                        'enroll_no'     => check_string_empty($student->student_enroll_number),
                        'student_title'       => check_string_empty($student['getTitle']['title_name']),
                        'student_name'  => check_string_empty($student->student_name),
                        'student_roll_no'  => check_string_empty($student->student_roll_no),
                        'current_class'    => check_string_empty($student['getStudentAcademic']['getCurrentClass'                      ]['class_name']),
                        'current_section'       => check_string_empty($student['getStudentAcademic']['getCurrentSection']['section_name']),
                        'medium_type'           => check_string_empty($student['getStudentMedium']['medium_type']),
                        'board_name'    => check_string_empty($student['getStudentAcademic']['getCurrentClass'                      ]['getBoard']['board_name']),
                        'student_type'  => $student_type,
                        'student_status'  => $student_status,
                        'profile_image' => $profile_image,
                        'date_of_reg'   => date('d F Y',strtotime($student->student_reg_date)),
                        'dob'           => date('d F Y',strtotime($student->student_dob)),
                        'email'         => check_string_empty($student->student_email),
                        'gender'        => $student_gender,
                        'mother_tongue' => check_string_empty($student['getParent']['student_mother_tongue']),
                    );
                // -- Basic Information end here

                // -- Academic Information start here

                    // Getting gender
                    $arr_gender   = \Config::get('custom.medium_type');
                    $medium_type = '';
                    if($student->medium_type === 0 || !empty($student->medium_type)){
                        $medium_type = $arr_gender[$student->medium_type];
                    }

                    $academic_history_info = [];

                    foreach($student['getStudentAcademicHistory'] as  $student_academic_hist){

                            $academic_history_info[] = array(
                                'session'   => check_string_empty($student_academic_hist['getSession']['session_name']),
                                'class'   => check_string_empty($student_academic_hist['getClass']['class_name']),
                                'percentage'   => check_string_empty($student_academic_hist['percentage']),
                                'rank'   => check_string_empty($student_academic_hist['rank']),
                                'activity_winner'   => check_string_empty($student_academic_hist['activity_winner'])
                            );
                    }

                    $academic_info = array(
                        'medium_type'           => check_string_empty($student['getStudentMedium']['medium_type']),
                        'admit_session'         => check_string_empty($student['getStudentAcademic']['getAdmitSession'                      ]->session_name),
                        'admin_class'           => check_string_empty($student['getStudentAcademic']['getAdmitClass'                      ]->class_name),
                        'admin_session'         => check_string_empty($student['getStudentAcademic']['getAdmitSection'                      ]->section_name),
                        'current_session'       => check_string_empty($student['getStudentAcademic']['getCurrentSession']->session_name),
                        'current_class'         => check_string_empty($student['getStudentAcademic']['getCurrentClass'                      ]->class_name),
                        'current_section'       => check_string_empty($student['getStudentAcademic']['getCurrentSection']->section_name),
                        'privious_school'       => check_string_empty($student['student_privious_school']),
                        'privious_class'        => check_string_empty($student['student_privious_class']),
                        'privious_tc_no'        => check_string_empty($student['student_privious_tc_no']),
                        'privious_tc_date'      => $student->student_privious_tc_date != ''?date('d F Y',strtotime(                         $student->student_privious_tc_date)):'',
                        'privious_result'       => check_string_empty($student['student_privious_result']),

                        'attendence_unique_id'  => check_string_empty($student['getStudentAcademic']->student_unique_id),
                        'group_name'            => check_string_empty($student['getStudentAcademic']['getStudentGroup'                      ]->group_name),
                        'stream_name'           => check_string_empty($student['getStudentAcademic']['getStudentStream'                      ]->stream_name),


                        'academic_history_info' => $academic_history_info
                    );
                // -- Academic Information end here

                // -- Other Information start here                    
                    $other_info = array(
                        'category'          => check_string_empty($student['student_category']),
                        'caste'             => check_string_empty($student['getStudentCaste']['caste_name']),
                        'religion'          => check_string_empty($student['getStudentReligion']['religion_name']),
                        'nationality'       => check_string_empty($student['getStudentNationality']['nationality_name']),
                        'sibling_name'      => check_string_empty($student['student_sibling_name']),
                        'sibling_class'     => $student->student_sibling_class_id != ''?$student['getSiblingClass']['class_name']:'',
                        'aadhaar_card_no'   => check_string_empty($student['student_adhar_card_number']),
                         // 'group'             => check_string_empty($student->student_privious_result),
                    );
                // -- Other Information end here                    

                // -- Parent Information start here
                    $parent_info = array(
                        'father_name'       => check_string_empty($student['getParent']['student_father_name']),
                        'father_mobile_no'  => check_string_empty($student['getParent']['student_father_mobile_number']),
                        'father_email'      => check_string_empty($student['getParent']['student_father_email']),
                        'father_salary'     => check_string_empty($student['getParent']['student_father_annual_salary']),
                        'father_occupation' => check_string_empty($student['getParent']['student_father_occupation']),
                        'mother_name'       => check_string_empty($student['getParent']['student_mother_name']),
                        'mother_mobile_no'  => check_string_empty($student['getParent']['student_mother_mobile_number']),
                        'mother_email'      => check_string_empty($student['getParent']['student_mother_email']),
                        'mother_salary'     => check_string_empty($student['getParent']['student_mother_annual_salary']),
                        'mother_occupation' => check_string_empty($student['getParent']['student_mother_occupation']),
                        'guardian_name'             => check_string_empty($student['getParent']['student_guardian_name']),
                        'guardian_mobile_number'    => check_string_empty($student['getParent']['student_guardian_mobile_number']),
                        'guardian_email'            => check_string_empty($student['getParent']['student_guardian_email']),
                        'student_guardian_relation' => check_string_empty($student['getParent']['student_guardian_relation']),
                    );
                // -- Parent Information end here

                // -- Health Information start here
                    $health_issue = array(
                        'height'            => check_string_empty($student['getStudentHealth']['student_height']),
                        'weight'            => check_string_empty($student['getStudentHealth']['student_weight']),
                        'blood_group'       => check_string_empty($student['getStudentHealth']['student_blood_group']),
                        'vision_left'       => check_string_empty($student['getStudentHealth']['student_vision_left']),
                        'vision_right'      => check_string_empty($student['getStudentHealth']['student_vision_right']),
                        'medical_issues'    => check_string_empty($student['getStudentHealth']['medical_issues']),
                    );
                // -- Health Information end here

                // -- Address Information Start here                    
                    $address_info = array(
                        'temporary_address'     => check_string_empty($student['student_temporary_address']),
                        'temporary_city'        => check_string_empty($student['getTemporaryCity']['city_name']),
                        'temporary_state'       => check_string_empty($student['getTemporaryState']['state_name']),
                        'temporary_county'      => check_string_empty($student['getTemporaryCountry']['country_name']),
                        'temporary_pincode'     => check_string_empty($student['student_temporary_pincode']),
                        'permanent_address'     => check_string_empty($student['student_permanent_address']),
                        'permanent_city'        => check_string_empty($student['getPermanentCity']['city_name']),
                        'permanent_state'       => check_string_empty($student['getPermanentState']['state_name']),
                        'permanent_county'      => check_string_empty($student['getPermanentCountry']['country_name']),
                        'permanent_pincode'     => check_string_empty($student['student_permanent_pincode']),
                    );
                // -- Address Information end here

                    $result_data['basic_info'] = $basic_info;
                    $result_data['academic_info'] = $academic_info;
                    $result_data['other_info'] = $other_info;
                    $result_data['parent_info'] = $parent_info;
                    $result_data['health_issue'] = $health_issue;
                    $result_data['address_info'] = $address_info;

                    $status = true;
                    $message = 'success';
                }else{
                    $status = false;
                    $message = 'Invalid student ID';    
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        }else{
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : getStudentClass
     *  To get student class
     *  @Sumit on 04 DEC 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getStudentClass(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $student_class = Classes::with(['getSections' => function($query) use ($request){

                    if (!empty($request) && !empty($request->get('section_id'))) {
                        $query->where('section_id', "=", $request->get('section_id'));
                    }

                    if (!empty($request) && !empty($request->get('class_id'))) {
                        $query->where('class_id', "=", $request->get('class_id'));
                    }

                }])->with(['getMedium' => function($query) use ($request){

                    if (!empty($request) && !empty($request->get('medium_id'))) {
                        $query->where('medium_type_id', "=", $request->get('medium_id'));
                    }

                }])->with('getBoard')->orderby('class_order')->paginate(20);

                if(count($student_class) != 0){

                    $class_info = [];
                    foreach($student_class as $student_class_res){
                        $section_arra = [];

                        foreach($student_class_res->getSections as $section_res){

                            $get_student_count = StudentAcademic::where([ ['current_class_id', $student_class_res->class_id], ['current_section_id', $section_res->section_id ] ])->get()->count();

                            $section_arra[] = array(
                                'section_id'    => $section_res->section_id,
                                'section_name'  => $section_res->section_name,
                                'student_count' => $get_student_count,
                            );
                        }

                        if( !empty($section_arra) ){
                            $class_info[] = array(
                                'class_id'      => $student_class_res['class_id'],
                                'class_name'    => $student_class_res['class_name'],
                                'medium_id'     => $student_class_res['getMedium']['medium_type_id'],
                                'medium_type'   => $student_class_res['getMedium']['medium_type'],
                                'board_id'      => $student_class_res['getBoard']['board_id'],
                                'board_name'    => $student_class_res['getBoard']['board_name'],
                                'sections'      => $section_arra,
                            );
                        }
                    }

                    $result_data = $class_info;
                    $lastPage = $student_class->lastPage($student_class_res);  
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }
}

