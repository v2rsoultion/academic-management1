<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Competition\Competition; // Competition Model
use App\Model\Competition\CompetitionMapping; // CompetitionMapping Model
use App\Model\Student\StudentCC; // Model
use App\Model\Certificate\Certificate;
use App\Model\Student\StudentTC; // Model
use DB;
use Hash;
use DateTime;
use Validator;

class CertificateController extends Controller
{
    /*  Function name : getStudentCertificates
     *  To get student certificate
     *  @Sumit on 28 DEC 2018    
     *  @parameters : device_id,csrf_token,student_id,flag
     *  @V2R by Sumit
    */
    public function getStudentCertificates(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $student_id     = $request->input('student_id');
        $flag           = $request->input('flag');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  && !empty($student_id) && !empty($flag) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if( $flag == 2 ){

                    $competition = CompetitionMapping::where('student_id', $student_id)
                    ->with('getCompetition')
                    ->with('getStudent')
                    ->with('getStudent.getParent')
                    ->with('getStudentAcademic')
                    ->with('getStudentAcademic.getCurrentClass')
                    ->orderby('student_competition_map_id', 'DESC')
                    ->paginate(20);

                    if( count($competition) != 0 ){

                        $certificate_info = [];

                        foreach ($competition as $competition_res) {

                            $student_competition_map_id = get_encrypted_value($competition_res['student_competition_map_id'], true);
                
                            $certificate_link     = "competition/certificate/".$student_competition_map_id;

                            $certificate_info[] = array(
                                'student_id'                => $competition_res['getStudent']['student_id'],
                                'student_name'              => $competition_res['getStudent']['student_name'],
                                'student_father_name'       => $competition_res['getStudent']['getParent']['student_father_name'],
                                'class'                     => $competition_res['getStudentAcademic']['getCurrentClass']['class_name'],
                                'position_rank'             => $competition_res['position_rank'],
                                'competition_name'          => $competition_res['getCompetition']['competition_name'],
                                'competition_description'   => $competition_res['getCompetition']['competition_description'],
                                'issue_date'                => date('d F Y',strtotime($competition_res['updated_at'])),
                                'certificate_link'     => $certificate_link,
                            );
                        }

                        $result_data['certificate_info'] = $certificate_info;
                        $lastPage = $competition->lastPage($competition_res);
                        $status = true;
                        $message = 'success';

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);

    }


    /*  Function name : StudentAcademicCertificates
     *  To get student academic certificate
     *  @Sandeep on 29 Feb 2019    
     *  @parameters : device_id,csrf_token,student_id
     *  @V2R by Sandeep
    */
    public function StudentAcademicCertificates(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $student_id     = $request->input('student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  && !empty($student_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $encrypted_student_id = get_encrypted_value($student_id, true);
                $arr_student_cc = $arr_student_tc = [];

                $arr_student_cc   = StudentCC::where(function($query) use ($request) 
                {
                    $query->where('student_id',$request->get('student_id'));
                })
                ->orderBy('student_cc_id', 'DESC')
                ->get();

                if(!empty($arr_student_cc)){
                    foreach ($arr_student_cc as $key => $arr_student_ccs) {
                        $certificate_link     = "student/character-certificate/".$encrypted_student_id;
                        $student_list[] = array(
                            'certificate_id'       => $arr_student_ccs->student_cc_id,
                            'certificate_name'     => "CC",
                            'date_of_issued'       => $arr_student_ccs->cc_date,
                            'certificate_link'     => $certificate_link,
                        );
                    }
                }

                $arr_student_tc   = StudentTC::where(function($query) use ($request) 
                {
                    $query->where('student_id',$request->get('student_id'));
                })
                ->orderBy('student_tc_id', 'DESC')
                ->get();

                if(!empty($arr_student_tc)){
                    foreach ($arr_student_tc as $key => $arr_student_tcs) {
                        $certificate_link     = "student/transfer-certificate/".$encrypted_student_id;
                        $student_list[] = array(
                            'certificate_id'       => $arr_student_tcs->student_tc_id,
                            'certificate_name'     => "TC",
                            'date_of_issued'       => $arr_student_tcs->tc_date,
                            'certificate_link'     => $certificate_link,
                        );
                    }
                }

                if( count($student_list) != 0 ){

                    $result_data['certificate_info'] = $student_list;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        return response()->json($result_response, 200);

    }



}

