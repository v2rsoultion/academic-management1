<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Hash;
use DateTime;
use Validator;

use App\Admin; // Admin Model
use App\Model\Student\Student; // Student Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation;
use App\Model\Student\StudentParent; // StudentParent Model
use App\Model\Remarks\Remarks; // Remarks Model
use App\Model\StudentLeaveApplication\StudentLeaveApplication; // Model
use App\Model\Exam\ExamSchedules; // ExamSchedules Model
use App\Model\Exam\ExamScheduleMap; // ExamScheduleMap Model
use App\Model\Examination\MarksCriteria; // MarksCriteria Model
use App\Model\Examination\ExamMarks; // ExamMarks Model
use App\Model\Examination\ExamMap; // ExamMap Model
use App\Model\StudentSubjectManage\StudentSubjectManage; // StudentSubjectManage Model
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // SubjectSectionMapping Model


class ParentController extends Controller
{
    /*  Function name : getParentDetails
     *  To get parent details with parent_id
     *  @Sumit on 21 Dec 2018    
     *  @parameters : device_id,csrf_token,parent_id
     *  @V2R by Sumit
    */
    public function getParentDetails(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $parent_id      = $request->input('parent_id');


        if (!empty($request->input()) && !empty($csrf_token) && !empty($parent_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $parent = Admin::where('admin_id', '=', $parent_id)
                ->with('adminStudentParent')
                ->with('adminStudentParent.getStudents')
                ->with('adminStudentParent.getStudents.getStudentAcademic.getCurrentClass')
                ->with('adminStudentParent.getStudents.getStudentAcademic.getCurrentSection')
                ->with('adminStudentParent.getStudents.getStudentMedium')
                ->with('adminStudentParent.getStudents.getTemporaryCity')
                ->with('adminStudentParent.getStudents.getTemporaryState')
                ->with('adminStudentParent.getStudents.getTemporaryCountry')
                ->with('adminStudentParent.getStudents.getPermanentCity')
                ->with('adminStudentParent.getStudents.getPermanentState')
                ->with('adminStudentParent.getStudents.getPermanentCountry')
                ->first();

                if( !empty($parent) ){

                    $father_info = array(
                        'name'           => check_string_empty($parent['adminStudentParent']['student_father_name']),
                        'email'          => check_string_empty($parent['adminStudentParent']['student_father_email']),
                        'mobile_number'  => check_string_empty($parent['adminStudentParent']['student_father_mobile_number']),
                        'occupation'     => check_string_empty($parent['adminStudentParent']['student_father_occupation']),
                        'annual_salary'  => check_string_empty($parent['adminStudentParent']['student_father_annual_salary']),
                    );

                    $mother_info = array(
                        'name'           => check_string_empty($parent['adminStudentParent']['student_mother_name']),
                        'email'          => check_string_empty($parent['adminStudentParent']['student_mother_email']),
                        'mobile_number'  => check_string_empty($parent['adminStudentParent']['student_mother_mobile_number']),
                        'occupation'     => check_string_empty($parent['adminStudentParent']['student_mother_occupation']),
                        'annual_salary'  => check_string_empty($parent['adminStudentParent']['student_mother_annual_salary']),
                    );

                    $guardian_info = array(
                        'name'           => check_string_empty($parent['adminStudentParent']['student_guardian_name']),
                        'email'          => check_string_empty($parent['adminStudentParent']['student_guardian_email']),
                        'mobile_number'  => check_string_empty($parent['adminStudentParent']['student_guardian_mobile_number']),
                        'relation'  => check_string_empty($parent['adminStudentParent']['student_guardian_relation']),
                    );

                    $student_info = [];

                    if( !empty($parent['adminStudentParent']['getStudents']) ){

                        foreach ($parent['adminStudentParent']['getStudents'] as $student_res) {

                            $profile_image = check_file_exist($student_res['student_image'], 'student_image');

                            $address_info = array(
                                'temporary_address'     => check_string_empty($student_res['student_temporary_address']),
                                'temporary_city'        => check_string_empty($student_res['getTemporaryCity']['city_name']),
                                'temporary_state'       => check_string_empty($student_res['getTemporaryState']['state_name']),
                                'temporary_county'      => check_string_empty($student_res['getTemporaryCountry']['country_name']),
                                'temporary_pincode'     => check_string_empty($student_res['student_temporary_pincode']),
                                'permanent_address'     => check_string_empty($student_res['student_permanent_address']),
                                'permanent_city'        => check_string_empty($student_res['getPermanentCity']['city_name']),
                                'permanent_state'       => check_string_empty($student_res['getPermanentState']['state_name']),
                                'permanent_county'      => check_string_empty($student_res['getPermanentCountry']['country_name']),
                                'permanent_pincode'     => check_string_empty($student_res['student_permanent_pincode']),
                            );

                            $student_info[] = array(
                                'student_name'          => check_string_empty($student_res['student_name']),
                                'student_roll_no'       => check_string_empty($student_res['student_roll_no']),
                                'student_enroll_number' => check_string_empty($student_res['student_enroll_number']),
                                'student_image'         => check_string_empty($profile_image),
                                'current_class'         => check_string_empty($student_res['getStudentAcademic']['getCurrentClass']['class_name']),
                                'current_section'       => check_string_empty($student_res['getStudentAcademic']['getCurrentSection']['section_name']),
                                'medium_type'           => check_string_empty($student_res['getStudentMedium']['medium_type']),
                                'address_info'          => $address_info,
                            ); 
                        }
                    }

                    $parent_info = array(
                        'name'          => check_string_empty($parent['adminStudentParent']['student_father_name']),
                        'email'         => check_string_empty($parent['adminStudentParent']['student_login_email']),
                        'mobile'        => check_string_empty($parent['adminStudentParent']['student_login_contact_no']),
                        'occupation'    => check_string_empty($parent['adminStudentParent']['student_father_occupation']),
                        'father_info'   => $father_info,
                        'mother_info'   => $mother_info,
                        'guardian_info' => $guardian_info,
                        'student_info'  => $student_info,
                    );

                    $result_data['parent_info'] =  $parent_info;
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);

    }

    /*  Function name : getRemarks
     *  To get remarks details with student_id
     *  @Sumit on 21 Dec 2018    
     *  @parameters : device_id,csrf_token,student_id
     *  @V2R by Sumit
    */
    public function getRemarks(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $student_id     = $request->input('student_id');
        $subject_id     = $request->input('subject_id');
        $teacher_id     = $request->input('teacher_id');
        $date           = $request->input('date');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($student_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $remarks = Remarks::where(function($query) use($request, $student_id, $subject_id, $teacher_id, $date){

                    if ( !empty($student_id) )
                    {
                        $query->where('student_id', $student_id);
                    }

                    if ( !empty($subject_id) )
                    {
                        $query->where('subject_id', $subject_id);
                    }

                    if ( !empty($teacher_id) )
                    {
                        $query->where('staff_id', $teacher_id);
                    }

                    if ( !empty($date) )
                    {
                        $query->whereDate('remark_date', '=', $date);
                    }

                })->with('getClass')
                ->with('getSection')
                ->with('getStudent')
                ->with('getStaff')
                ->with('getSubject')
                ->orderBy('remark_id','DESC')
                ->paginate(20);

                if( count($remarks) !=0 ){

                    $remarks_info = [];

                    foreach ($remarks as $remarks_res) {

                        $profile_image = check_file_exist($remarks_res['getStaff']['staff_profile_img'], 'staff_profile');

                        $remarks_info[] =  array(
                            "remark_id"     => $remarks_res['remark_id'],
                            "subject_id"    => $remarks_res['getSubject']['subject_id'],
                            "subject_name"  => $remarks_res['getSubject']['subject_name'],
                            "staff_id"      => $remarks_res['getStaff']['staff_id'],
                            "staff_name"    => $remarks_res['getStaff']['staff_name'],
                            'staff_image'   => $profile_image,
                            "remark"        => $remarks_res['remark_text'],
                            "remark_date"   => date('d F Y',strtotime($remarks_res['remark_date'])),
                        );
                    }

                    $result_data['remarks_info']  = $remarks_info;
                    $lastPage = $remarks->lastPage($remarks_res);  
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : saveStudentLeave
     *  To add & edit student leave
     *  @Sumit on 22 Dec 2018    
     *  @parameters : device_id,csrf_token,student_id,reason,date_from,date_to,attachment
     *  @V2R by Sumit
    */
    public function saveStudentLeave(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $leave_id       = $request->input('leave_id');
        $student_id     = $request->input('student_id');
        $reason         = $request->input('reason');
        $date_from      = $request->input('date_from');
        $date_to        = $request->input('date_to');
        $attachment     = $request->input('attachment');
        $admin_id       = $request->input('admin_id');

        if (!empty($request->input()) && !empty($csrf_token)  && !empty($admin_id) && !empty($student_id) && !empty($reason) && !empty($student_id) && !empty($date_from) && !empty($date_to) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if (!empty($leave_id))
                {
                    $leave_application = StudentLeaveApplication::find($leave_id);

                    $admin_id = $leave_application['admin_id'];

                    if (!$leave_application)
                    {
                        $status = false;
                        $message = 'Leave Application not found!';
                    }
                    $success_msg = 'Leave Application updated successfully!';
                }
                else
                {
                    $leave_application  = New StudentLeaveApplication;
                    $success_msg 		= 'Leave Application saved successfully!';
                }

                $validatior = Validator::make($request->all(), [
                        'reason'   	    => 'required',
                        'date_from'     => 'required|unique:student_leaves,student_leave_from_date,' . $leave_id . ',student_leave_id,student_id,' . $student_id,
                        'date_to'   	=> 'required',
                ]);

                if ($validatior->fails())
                {
                    $status = false;
                    $message = $validatior;
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $leave_application->admin_id      			   = $admin_id;
                        $leave_application->update_by      			   = $admin_id;
                        $leave_application->student_id     			   = $student_id;
                        $leave_application->student_leave_reason 	   = $reason;
                        $leave_application->student_leave_from_date    = $date_from;
                        $leave_application->student_leave_to_date 	   = $date_to;

                        if ($request->hasFile('attachment'))
                        {
                            if (!empty($leave_id)){
                                $attachment = check_file_exist($leave_application->student_leave_attachment, 'student_leave_document_file');

                                if (!empty($attachment))
                                {
                                    unlink($attachment);
                                }
                            }

                            $file                          = $request->File('attachment');
                            $config_document_upload_path   = \Config::get('custom.student_leave_document_file');
                            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                            $ext                           = substr($file->getClientOriginalName(),-4);
                            $name                          = substr($file->getClientOriginalName(),0,-4);
                            $filename                      = $name.mt_rand(0,100000).time().$ext;

                            $file->move($destinationPath, $filename);
                            $leave_application->student_leave_attachment = $filename;
                        }
                        $leave_application->save();

                        $student_name = get_student_name($student_id);
                        $send_notification  = $this->send_push_notification($leave_application->student_leave_id,"student_leave_by_parent",$student_name,$student_id,'student_leave_by_parent');


                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                }

                $status = true;
                $message = $success_msg;

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getStudentMarks
     *  To get student marks
     *  @Sumit on 22 Dec 2018    
     *  @parameters : device_id,csrf_token,student_id,exam_id
     *  @V2R by Sumit
    */
    public function getStudentMarks(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $exam_id        = $request->input('exam_id');
        $student_id     = $request->input('student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($exam_id) && !empty($student_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam_marks = DB::table('exam_marks')
                            ->where([ ['exam_marks.exam_id', $exam_id],['exam_marks.student_id', $student_id] ])
                            ->join('exam_map', function ($join) {
                                $join->on('exam_marks.exam_id', '=', 'exam_map.exam_id');
                                $join->on('exam_marks.class_id', '=', 'exam_map.class_id');
                                $join->on('exam_marks.section_id', '=', 'exam_map.section_id');
                                $join->on('exam_marks.subject_id', '=', 'exam_map.subject_id');
                            })
                            ->join('marks_criteria', 'exam_map.marks_criteria_id', '=', 'marks_criteria.marks_criteria_id')
                            ->join('classes', 'exam_marks.class_id', '=', 'classes.class_id')
                            ->join('sections', 'exam_marks.section_id', '=', 'sections.section_id')
                            ->join('subjects', 'exam_marks.subject_id', '=', 'subjects.subject_id')
                            ->get();

                if( count($exam_marks) !=0 ){

                    $subject_info = [];

                    foreach ($exam_marks as $exam_marks_res) {

                        $subject_info[] = array(
                            'subject_id'     => $exam_marks_res->subject_id,
                            'subject_name'   => $exam_marks_res->subject_name,
                            'subject_obtained_marks'  => $exam_marks_res->marks,
                            'subject_passing_marks'  => $exam_marks_res->passing_marks,
                            'subject_total_marks'  => $exam_marks_res->max_marks,
                        );
                    }

                    $result_data['subject_info'] = $subject_info;
	            	$status = true;
                	$message = 'success';

                } else {
            		$status = false;
                	$message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getAllParentList
     *  To get all parent list
     *  @Sumit on 26 Dec 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getAllParentList(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $parents = StudentParent::where(function($query_parent) use ($request) 
                {
                    if (!empty($request) && !empty($request->get('father_name')))
                    {
                        $query_parent->where('student_father_name', "like", "%{$request->get('father_name')}%");
                    }

                })->whereHas('getStudents', function($query_student) use ($request) {

                    if (!empty($request) && !empty($request->get('roll_no')))
                    {
                        $query_student->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                    }

                    if (!empty($request) && !empty($request->get('enroll_number')))
                    {
                        $query_student->where('student_enroll_number', "like", "%{$request->get('enroll_number')}%");
                    }

                })

                ->with(['getStudents'=>function($q) use ($request) {

                    if (!empty($request) && !empty($request->get('roll_no')))
                    {
                        $q->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                    }

                    if (!empty($request) && !empty($request->get('enroll_number')))
                    {
                        $q->where('student_enroll_number', "like", "%{$request->get('enroll_number')}%");
                    }
    
                    $q->with(['getStudentAcademic.getCurrentClass'=>function($query_class) use ($request) {

                        if (!empty($request) && !empty($request->get('class_id')))
                        {
                            $query_class->where('class_id', $request->get('class_id'));
                        }

                    }])->with(['getStudentAcademic.getCurrentSection'=>function($query_section) use ($request) {                    
                        if (!empty($request) && !empty($request->get('section_id')))
                        {
                            $query_section->where('section_id', $request->get('section_id'));
                        }
                    }]);

                }])


                // ->with(['getStudents.getStudentAcademic.getCurrentClass'=>function($query_class) use ($request) {

                //     if (!empty($request) && !empty($request->get('class_id')))
                //     {
                //         $query_class->where('class_id', $request->get('class_id'));
                //     }

                // }])->with(['getStudents.getStudentAcademic.getCurrentSection'=>function($query_section) use ($request) {                    
                //     if (!empty($request) && !empty($request->get('section_id')))
                //     {
                //         $query_section->where('section_id', $request->get('section_id'));
                //     }
                // }])
                ->paginate(20);

                //p($parents);

                if( count($parents) !=0 ){

                    $parent_info = [];
                    foreach ($parents as $parents_res) {

                        $student_info = [];

                        foreach ($parents_res['getStudents'] as $student_res) {

                            if($student_res['getStudentAcademic']['getCurrentClass']['class_name'] != ""){
                                $profile_image = check_file_exist($student_res['student_image'], 'student_image');

                                $student_info[] = array(
                                    'student_name'          => check_string_empty($student_res['student_name']),
                                    'student_roll_no'       => check_string_empty($student_res['student_roll_no']),
                                    'student_enroll_number' => check_string_empty($student_res['student_enroll_number']),
                                    'student_image'         => check_string_empty($profile_image),
                                    'current_class'         => check_string_empty($student_res['getStudentAcademic']['getCurrentClass']['class_name']),
                                    'current_section'       => check_string_empty($student_res['getStudentAcademic']['getCurrentSection']['section_name']),
                                    'medium_type'           => check_string_empty($student_res['getStudentMedium']['medium_type']),
                                ); 
                            }
                        }

                        $parent_info[] = array(
                            'parent_id'             => $parents_res['student_parent_id'],
                            'reference_admin_id'    => $parents_res['reference_admin_id'],
                            'father_name'   => $parents_res['student_father_name'],
                            'father_mobile_number' => $parents_res['student_father_mobile_number'],
                            'student_info'  => $student_info,
                        );
                    }

                    $result_data['parent_info'] =  $parent_info;
                    $lastPage = $parents->lastPage($parents_res);  
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }


    public function send_push_notification($module_id,$module_name,$student_name,$student_id,$notification_type){
        $device_ids = [];
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            if (!empty($student_id) && !empty($student_id) && $student_id != null){
                $query->where('students.student_id', $student_id);
            }
            $query->where('students.student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($class_id,$section_id){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('students.current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('students.current_class_id', '=',$class_id);
            }
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('students.student_id','students.student_name','students.student_parent_id','students.reference_admin_id','student_academic_info.current_section_id','student_academic_info.current_class_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];

        $staff_info = ClassTeacherAllocation::where('class_id', $student_info['current_class_id'])->where('section_id',$student_info['current_section_id'])->with('getStaff')->with('getStaff.getStaffAdminInfo')->get();
        $staff_info = isset($staff_info[0]) ? $staff_info[0] : [];
        if($staff_info['getStaff']['getStaffAdminInfo']->fcm_device_id != "" && $staff_info['getStaff']['getStaffAdminInfo']->notification_status == 1){
            $device_ids[] = $staff_info['getStaff']['getStaffAdminInfo']->fcm_device_id;
            $staff_admin_id = $staff_info['getStaff']['getStaffAdminInfo']->admin_id;
        }

        $message = $student_name." applied for leave";
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,$staff_id,'','',$device_ids,$message);
        //p($send_notification);
    }
    

}

