<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Admission\AdmissionData; // AdmissionData Model
use App\Model\Admission\AdmissionFields; // AdmissionFields Model
use App\Model\Admission\AdmissionForm; // AdmissionForm Model

use DB;
use Hash;
use DateTime;
use Validator;

class AdmissionController extends Controller
{

    /*  Function name : getForm
     *  To get forms
     *  @Sumit on 10 Jan 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getForm(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $admission_forms = AdmissionForm::where(function($query) use ($request) 
                {
                    if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
                    {
                        $query->where('session_id', "=", $request->get('session_id'));
                    }

                    if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
                    {
                        $query->where('medium_type', "=", $request->get('medium_type'));
                    }

                    if (!empty($request) && !empty($request->get('name')))
                    {
                        $query->where('form_name', "like", "%{$request->get('name')}%");
                    }

                    if (!empty($request)  && $request->get('form_type') != null)
                    {
                        $query->where('form_type', "=", $request->get('form_type'));
                    }

                })->orderBy('admission_form_id', 'DESC')->with('getSessionInfo')->paginate(20);


                if( count($admission_forms) !=0 ){

                    $admission_forms_info = [];

                    foreach ($admission_forms as $admission_forms_res) {

                        $arr_form_type  = \Config::get('custom.form_type');
                        $form_type      =  $arr_form_type[$admission_forms_res['form_type']];

                        $class_names = [];
                        $class_ids = explode(",",$admission_forms_res['class_ids']);

                        if(!empty($class_ids)) {
                            foreach($class_ids as $class_id){
                                $class_names[] = get_class_name($class_id);
                            }
                        }

                        $arr_medium   = get_all_mediums();

                        $admission_forms_info[] = array(
                            'form_name'     => $admission_forms_res['form_name'],
                            'form_type'     => $form_type,
                            'session_name'  => $admission_forms_res['getSessionInfo']['session_name'],
                            'class_name'    => implode(",", $class_names),
                            'medium_type'   => $arr_medium[$admission_forms_res['medium_type']],
                            'no_of_intake'  => $admission_forms_res['no_of_intake'],
                            'form_url'      => check_string_empty($admission_forms_res['form_url']),
                        );
                    }


                    if( !empty($admission_forms_info) ){
                        $result_data['admission_forms_info'] = $admission_forms_info;
                        $lastPage = $admission_forms->lastPage($admission_forms_res);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getApplicant
     *  To get applicant
     *  @Sumit on 10 Jan 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getApplicant(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth

            if(validate_token($device_id,$csrf_token) == true){

                $admission_data = AdmissionData::where(function($query) use ($request) 
                {
                    $query->where('form_type', "=", '0');
                    $query->where('admission_data_status', "=", '1');

                    if (!empty($request) && !empty($request->get('form_number')) && $request->get('form_number') != null)
                    {
                        $query->where('form_number', "=", $request->get('form_number'));
                    }

                    if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null && $request->get('class_id') != '')
                    {
                        $query->where('current_class_id', "=", $request->get('class_id'));
                    }

                    if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
                    {
                        $query->where('medium_type', "=", $request->get('medium_type'));
                    }

                    if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
                    {
                        $query->where('current_session_id', "=", $request->get('session_id'));
                    }

                    if (!empty($request) && !empty($request->get('student_name')))
                    {
                        $query->where('student_name', "like", "%{$request->get('student_name')}%");
                    }

                    if (!empty($request) && !empty($request->get('date')  && $request->get('date') !='' ))
                    {
                        $query->whereDate('created_at', "=", $request->get('date'));
                    }

                })->orderBy('admission_data_id', 'DESC')->with('getFormInfo')->with('getClassInfo')->with('getSessionInfo')->paginate(20);

                if( count($admission_data) !=0 ){

                    $admission_data_info = [];

                    foreach ($admission_data as $admission_data_res) {

                        $admission_data_info[] = array(
                            'applicant_id'      => check_string_empty($admission_data_res['admission_data_id']),
                            'student_name'      => check_string_empty($admission_data_res['student_name']),
                            'class_session'     => check_string_empty($admission_data_res['getClassInfo']['class_name'].' - '.$admission_data_res['getSessionInfo']['session_name']),
                            'form_number'       => check_string_empty($admission_data_res['form_number']),
                            'date'              => date('d F Y',strtotime($admission_data_res['created_at'])),
                            'status'            => $admission_data_res['admission_data_status'],
                        );
                    }

                    if( !empty($admission_data_info) ){
                        $result_data['admission_forms_info'] = $admission_data_info;
                        $lastPage = $admission_data->lastPage($admission_data_res);
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);
    }


    /*  Function name : getApplicantDetails
     *  To get applicant
     *  @Sumit on 10 Jan 2019    
     *  @parameters : device_id,csrf_token,applicant_id
     *  @V2R by Sumit
    */
    public function getApplicantDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $applicant_id   = $request->input('applicant_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($applicant_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $applicant_data = AdmissionData::where(function($query) use ($request,$applicant_id) 
                {
                    $query->where('admission_data_id', "=", $applicant_id);

                })->orderBy('admission_data_id', 'DESC')->with('getFormInfo')->with('getClassInfo')->with('getSessionInfo')->first();
            
                if( !empty($applicant_data) ){

                    $profile_image = check_file_exist($applicant_data['student_image'], 'student_image');

                    //  Getting student type
                    if( $applicant_data['student_type'] == 0 ){
                        $student_type = 'Paid';
                    } elseif($applicant_data['student_type'] == 1) {
                        $student_type = 'RTE';
                    } else {
                        $student_type = 'Free';
                    }

                    // Getting gender
                    if( $applicant_data['student_gender'] == 0 ){
                        $student_gender = 'Boy';
                    } else {
                        $student_gender = 'Girl';
                    }

                    $arr_medium      = get_all_mediums();

                    $form_info = array(
                        'form_number' => check_string_empty($applicant_data['form_number']),
                        'admission_data_pay_mode' => check_string_empty($applicant_data['admission_data_pay_mode']),
                        'admission_data_fees' => check_string_empty($applicant_data['admission_data_fees']),
                        'admission_data_status' => check_string_empty($applicant_data['admission_data_status']),
                    );

                    $basic_info = array(
                        'enroll_no'         => check_string_empty($applicant_data['student_enroll_number']),
                        'student_name'      => check_string_empty($applicant_data['student_name']),
                        'student_roll_no'   => check_string_empty($applicant_data['student_roll_no']),
                        'current_class'     => check_string_empty($applicant_data['getClassInfo'                      ]['class_name']),
                        'current_session'   => check_string_empty($applicant_data['getSessionInfo']['session_name']),
                        'medium_type'       => $arr_medium[$applicant_data['medium_type']],
                        'student_type'      => $student_type,
                        'profile_image'     => $profile_image,
                        'date_of_reg'       => date('d F Y',strtotime($applicant_data['student_reg_date'])),
                        'dob'               => date('d F Y',strtotime($applicant_data['student_dob'])),
                        'email'             => check_string_empty($applicant_data['student_email']),
                        'gender'            => $student_gender,
                        'mother_tongue'     => check_string_empty($applicant_data['student_mother_tongue']),
                    );

                    $privious_school_info = array(
                        'privious_school'   => check_string_empty($applicant_data['student_privious_school']),
                        'privious_class'    => check_string_empty($applicant_data['student_privious_class']),
                        'privious_tc_no'    => check_string_empty($applicant_data['student_privious_tc_no']),
                        'privious_tc_date'  => $applicant_data->student_privious_tc_date != ''?date('d F Y',strtotime(                         $applicant_data->student_privious_tc_date)):'',
                        'privious_result'   => check_string_empty($applicant_data['student_privious_result']),
                    );

                    $other_info = array(
                        'category'          => check_string_empty($applicant_data['student_category']),
                        'caste'             => check_string_empty($applicant_data['getStudentCaste']['caste_name']),
                        'religion'          => check_string_empty($applicant_data['getStudentReligion']['religion_name']),
                        'nationality'       => check_string_empty($applicant_data['getStudentNationality']['nationality_name']),
                        'sibling_name'      => check_string_empty($applicant_data['student_sibling_name']),
                        'sibling_class'     => $applicant_data->student_sibling_class_id != ''?$applicant_data['getSiblingClass']['class_name']:'',
                        'aadhaar_card_no'   => check_string_empty($applicant_data['student_adhar_card_number']),
                    );

                    $parent_info = array(
                        'father_name'       => check_string_empty($applicant_data['student_father_name']),
                        'father_mobile_no'  => check_string_empty($applicant_data['student_father_mobile_number']),
                        'father_email'      => check_string_empty($applicant_data['student_father_email']),
                        'father_salary'     => check_string_empty($applicant_data['student_father_annual_salary']),
                        'father_occupation' => check_string_empty($applicant_data['student_father_occupation']),
                        'mother_name'       => check_string_empty($applicant_data['student_mother_name']),
                        'mother_mobile_no'  => check_string_empty($applicant_data['student_mother_mobile_number']),
                        'mother_email'      => check_string_empty($applicant_data['student_mother_email']),
                        'mother_salary'     => check_string_empty($applicant_data['student_mother_annual_salary']),
                        'mother_occupation' => check_string_empty($applicant_data['student_mother_occupation']),
                        'guardian_name'             => check_string_empty($applicant_data['student_guardian_name']),
                        'guardian_mobile_number'    => check_string_empty($applicant_data['student_guardian_mobile_number']),
                        'guardian_email'            => check_string_empty($applicant_data['student_guardian_email']),
                        'student_guardian_relation' => check_string_empty($applicant_data['student_guardian_relation']),
                    );

                    $health_issue = array(
                        'height'            => check_string_empty($applicant_data['student_height']),
                        'weight'            => check_string_empty($applicant_data['student_weight']),
                        'blood_group'       => check_string_empty($applicant_data['student_blood_group']),
                        'vision_left'       => check_string_empty($applicant_data['student_vision_left']),
                        'vision_right'      => check_string_empty($applicant_data['student_vision_right']),
                        'medical_issues'    => check_string_empty($applicant_data['medical_issues']),
                    );

                    $address_info = array(
                        'temporary_address'     => check_string_empty($applicant_data['student_temporary_address']),
                        'temporary_city'        => check_string_empty($applicant_data['getTemporaryCity']['city_name']),
                        'temporary_state'       => check_string_empty($applicant_data['getTemporaryState']['state_name']),
                        'temporary_county'      => check_string_empty($applicant_data['getTemporaryCountry']['country_name']),
                        'temporary_pincode'     => check_string_empty($applicant_data['student_temporary_pincode']),
                        'permanent_address'     => check_string_empty($applicant_data['student_permanent_address']),
                        'permanent_city'        => check_string_empty($applicant_data['getPermanentCity']['city_name']),
                        'permanent_state'       => check_string_empty($applicant_data['getPermanentState']['state_name']),
                        'permanent_county'      => check_string_empty($applicant_data['getPermanentCountry']['country_name']),
                        'permanent_pincode'     => check_string_empty($applicant_data['student_permanent_pincode']),
                    );

                    $result_data['form_info'] = $form_info;
                    $result_data['basic_info'] = $basic_info;
                    $result_data['privious_school_info'] = $privious_school_info;
                    $result_data['other_info'] = $other_info;
                    $result_data['parent_info'] = $parent_info;
                    $result_data['health_issue'] = $health_issue;
                    $result_data['address_info'] = $address_info;

                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        return response()->json($result_response, 200);
    }

}

