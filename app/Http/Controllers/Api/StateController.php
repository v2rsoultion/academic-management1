<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\State\State; // Model

use DB;
use Hash;
use DateTime;
use Validator;

class StateController extends Controller
{
    /*  Function name : viewState
     *  To get all state
     *  @Sandeep on 27 march 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function view_state(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $country_id     = $request->input('country_id');
        $state_id       = $request->input('state_id');
        
        if ( !empty($csrf_token) && !empty($device_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
            
                $state   = State::where(function($query) use ($state_id,$country_id,$request) 
                {
                    if (!empty($state_id))
                    {
                        $query->where('state_id', $state_id);
                    }
                    if (!empty($country_id))
                    {
                        $query->where('country_id', $country_id);
                    }
                    if (!empty($request) && !empty($request->get('state_name')))
                    {
                        $query->where('state_name', "like", "%{$request->get('state_name')}%");
                    }
                })->orderBy('state_name', 'ASC')->paginate(20);

                if( count($state) != 0 ){

                    $state_info  = [];
                    foreach($state as $state_res){

                        $state_info[] = array(
                            'state_id'     =>  check_string_empty($state_res->state_id),
                            'state_name'   =>  check_string_empty($state_res->state_name),
                            'country_id'   =>  check_string_empty($state_res->country_id),
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['state_info'] = $state_info;
                    $lastPage = $state->lastPage($state_res);

                } else {
                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


}