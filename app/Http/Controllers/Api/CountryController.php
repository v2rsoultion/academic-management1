<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Country\Country; // Model

use DB;
use Hash;
use DateTime;
use Validator;

class CountryController extends Controller
{
    /*  Function name : viewCountry
     *  To get all country
     *  @Sandeep on 27 march 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function view_country(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $country_id     = $request->input('country_id');
        
        if ( !empty($csrf_token) && !empty($device_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $country     = Country::where(function($query) use ($country_id,$request) 
                {
                    if (!empty($country_id))
                    {
                        $query->where('country_id', $country_id);
                    }
                    if (!empty($request) && !empty($request->get('country_name')))
                    {
                        $query->where('country_name', "like", "%{$request->get('country_name')}%");
                    }
                })->orderBy('country_name', 'ASC')->paginate(20);

                if( count($country) != 0 ){

                    $country_info  = [];
                    foreach($country as $country_res){

                        $country_info[] = array(
                            'country_id'     =>  check_string_empty($country_res->country_id),
                            'country_name'   =>  check_string_empty($country_res->country_name),
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['country_info'] = $country_info;
                    $lastPage = $country->lastPage($country_res);

                } else {
                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


}