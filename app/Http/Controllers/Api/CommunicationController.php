<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Model\Communication\CommunicationMessage; // CommunicationMessage Model
use App\Model\Staff\Staff; //Staff Model
use App\Model\Student\Student; // School Model

use DB;
use Hash;
use DateTime;
use Validator;

class CommunicationController extends Controller
{
    /*  Function name : sendCommunicationMessage
     *  To send communication message
     *  @Sumit on 26 JAN 2019    
     *  @parameters : device_id,csrf_token,sender_id,message
     *  @V2R by Sumit
    */
    public function sendCommunicationMessage(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $parent_id      = $request->input('parent_id');
        $staff_id       = $request->input('staff_id');
        $sender_id      = $request->input('sender_id');
        $student_id     = $request->input('student_id');
        $admin_id       = $request->input('login_id');
        $sender_type    = $request->input('sender_type');
        $message        = $request->input('message');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($parent_id) && !empty($staff_id) && !empty($sender_id) && !empty($sender_type) && !empty($message)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $comm_message = New CommunicationMessage();
                DB::beginTransaction();
                try
                {
                    $comm_message->student_parent_id   = $parent_id;
                    $comm_message->staff_id            = $staff_id;
                    $comm_message->sender_id           = $sender_id;
                    $comm_message->sender_type         = $sender_type;
                    $comm_message->message             = $message;
                    $comm_message->save();


                    $title = "New Message Received";
                    if($sender_type == 1){
                        $notification = $this->send_push_notification_parent($comm_message->comm_message_id,$student_id,'communication',$admin_id,$message,$title);
                    }

                    if($sender_type == 2){
                        $notification = $this->send_push_notification_staff($comm_message->comm_message_id,$staff_id,'communication',$admin_id,$message,$title);
                    }

                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return response()->json(['status'=>false, 'error_message'=> $error_message]);
                }
                DB::commit();

                $status = true;
                $message = 'success';
            } else {
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getCommunicationMessage
     *  To get communication message
     *  @Sumit on 26 JAN 2019    
     *  @parameters : device_id,csrf_token,parent_id,staff_id
     *  @V2R by Sumit
    */
    public function getCommunicationMessage(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $parent_id      = $request->input('parent_id');
        $staff_id       = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($parent_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $comm_message = CommunicationMessage::where(function($query) use ($parent_id,$staff_id) 
                { 
                    if ( $parent_id !='' ) 
                    {
                        $query->where('student_parent_id', $parent_id); 
                    }
                    if ( $staff_id !='' ) 
                    {
                        $query->where('staff_id', $staff_id); 
                    }
                })
                ->orderBy('comm_message_id','DESC')
                ->paginate(20);

                if( count($comm_message) != 0 ){
                    $message_info = [];
                    foreach ($comm_message as $comm_message_res) {
                        
                        $message_info[] = array(
                            'parent_id'     => check_string_empty($comm_message_res['student_parent_id']),
                            'staff_id'      => check_string_empty($comm_message_res['staff_id']),
                            'sender_id'     => check_string_empty($comm_message_res['sender_id']),
                            'sender_type'   => check_string_empty($comm_message_res['sender_type']),
                            'message'       => check_string_empty($comm_message_res['message']),
                            'date_time'     => date("Y-m-d h:i A",strtotime($comm_message_res['created_at'])),
                        );
                    }

                    $result_data['message_info'] = $message_info;
                    $lastPage = $comm_message->lastPage($comm_message_res);
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    //Notification function
    public function send_push_notification_staff($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($user_id) 
        {
            $query->whereIn('staff_id',explode(",",$user_id));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $user_id,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    //Notification function
    public function send_push_notification_parent($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $student_info        = Student::where(function($query) use ($user_id) 
        {
            $query->whereIn('student_id',explode(",",$user_id));
        })
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();

        foreach($student_info as $student){
            // if($student['get_admin_info']['fcm_device_id'] != "" && $student['get_admin_info']['notification_status'] == 1){
            //     $device_ids[] = $student['get_admin_info']['fcm_device_id'];
            // }
            if($student['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student['get_parent']['get_parent_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_parent']['get_parent_admin_info']['fcm_device_id'];
                $parent_admin_id = $student['get_parent']['get_parent_admin_info']['admin_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => $parent_admin_id,
                'staff_admin_id' => Null,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


}
