<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use DateTime;

use App\Model\FeesCollection\OneTime; //OneTime Model
use App\Model\FeesCollection\ConcessionMap; //ConcessionMap Model
use App\Model\FeesCollection\ImprestAc; //ImprestAc Model
use App\Model\Student\WalletAc; //WalletAc Model
use App\Model\FeesCollection\RecurringHead; //RecurringHead Model
use App\Model\FeesCollection\Receipt; //Receipt Model
use App\Model\FeesCollection\Reason; //Reason Model
use App\Model\FeesCollection\FeesStMap; // Model
use App\Model\FeesCollection\ReceiptDetail;  // ReceiptDetail
use App\Model\Student\Student;  // Student

class FeesController extends Controller
{
    /*  Function name : getStudentFees
     *  To get exam schedule
     *  @Sumit on 12 Dec 2018    
     *  @parameters : device_id,csrf_token,student_id,class_id,section_id
     *  @V2R by Sumit
    */
    public function getStudentFees(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $student_id     = $request->input('student_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($student_id) && !empty($class_id) && !empty($section_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $one_time_amount    = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')->sum('ot_amount');
                $recurring_amount   = RecurringHead::whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->sum('rc_amount');

                $concession_amount  = ConcessionMap::where([ ['class_id', '=', $class_id], ['student_id', '=', $student_id] ])->sum('concession_amt');
                
                $imprest_amount     = ImprestAc::where([ ['class_id', '=', $class_id], ['student_id', '=', $student_id] ])->sum('imprest_ac_amt');
                $wallet_amount      = WalletAc::where([ ['class_id', '=', $class_id], ['student_id', '=', $student_id] ])->sum('wallet_ac_amt');

                $fees_total_amount = Receipt::where([ ['class_id', '=', $class_id],  ['student_id', '=', $student_id] ])
                ->sum('total_amount');
                $fees_pay_later_amount = Receipt::where([ ['class_id', '=', $class_id],  ['student_id', '=', $student_id] ])
                ->sum('pay_later_amount');

                $fine_amount = Receipt::where([ ['class_id', '=', $class_id],  ['student_id', '=', $student_id] ])
                ->leftJoin('fee_receipt_details', function($join) use ($class_id,$student_id){
                    $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                })->sum('r_fine_amt');

                $fees_total_paid_amount = $fees_total_amount-$fees_pay_later_amount;
                $total_fees = $recurring_amount+$one_time_amount;

                $fees_total_amount = Receipt::where([ ['class_id', '=', $class_id],  ['student_id', '=', $student_id] ])
                ->sum('total_amount');

                $fee_summary = array(
                    'total_fees'        => $total_fees,
                    'total_paid_amount' => $fees_total_paid_amount,
                    'concession_amount' => $concession_amount,
                    'wallet_amount'     => $wallet_amount,
                    'imprest_amount'    => $imprest_amount,
                    'fine_amount'       => $fine_amount,
                );


            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getFees
     *  To get fees
     *  @Sumit on 11 JAN 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getFees(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){


            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getConcessionData
     *  To get Concession Data
     *  @Sumit on 28 JAN 2019    
     *  @parameters : device_id,csrf_token,class_id,section_id
     *  @V2R by Sumit
    */
    public function getConcessionData(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $student_id     = $request->input('student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $student  = Student::where(function($query) use ($class_id,$section_id,$student_id) 
                {
                    if (!empty($student_id) && !empty($student_id) && $student_id != null){
                        $query->where('students.student_id', '=',$student_id);
                    }
                    
                })->join('student_academic_info', function($join) use ($class_id,$section_id){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    if (!empty($section_id) && !empty($section_id) && $section_id != null){
                        $join->where('current_section_id', '=',$section_id);
                    }
                    if (!empty($class_id) && !empty($class_id) && $class_id != null){
                        $join->where('current_class_id', '=',$class_id);
                    }
                })->join('student_parents', function($join) use ($class_id,$section_id){
                    $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                    
                })->paginate(20);

                if( count($student) !=0 ){

                    foreach($student as $student_res){
                        $request->request->add(['student_id' => $student_res->student_id]);
                        $get_all_fees = api_get_complete_fees($student_res->student_id,$class_id);
                        $head_amt = 0;
                        $concession_amt = 0;
                        $concession_info = [];
                        foreach ($get_all_fees as $get_all_fees_res) {

                            $paid_data = Receipt::where([ ['class_id',$class_id], ['student_id',$student_res->student_id] ])
                            ->with(['getInstallments'=>function($q1) use ($get_all_fees_res) {

                                if ( $get_all_fees_res['concession_map_id'] !='' ) {
                                    $q1->where('r_concession_map_id', "=", $get_all_fees_res['concession_map_id']);
                                }
                                $q1->where('head_id', "=", $get_all_fees_res['head_id']);
                                $q1->where('head_type', "=", $get_all_fees_res['head_type']);
                                
                            }])->get();
                            $concession_paid = 0;
                            foreach( $paid_data as $paid_data_res ) {
                                foreach( $paid_data_res['getInstallments'] as $install_res ){
                                    if( $install_res['r_concession_map_id'] != '' ){
                                        $concession_paid = 1;
                                    }
                                }
                            }
                            

                            if($get_all_fees_res['concession_amt'] != ''){
                                $concession_amt = $get_all_fees_res['concession_amt'];
                            }
                            $head_amt = $get_all_fees_res['head_amt'] - $concession_amt;
                            $concession_info[] = array(
                                'head_id'           => check_string_empty($get_all_fees_res['head_id']),
                                'head_inst_id'      => check_string_empty($get_all_fees_res['head_inst_id']),
                                'head_name'         => check_string_empty($get_all_fees_res['head_name']),
                                'head_amt'          => check_string_empty($get_all_fees_res['head_amt']),
                                'head_type'         => check_string_empty($get_all_fees_res['head_type']),
                                'concession_map_id' => check_string_empty($get_all_fees_res['concession_map_id']),
                                'concession_amt'    => check_string_empty($get_all_fees_res['concession_amt']),
                                'reason_id'         => check_string_empty($get_all_fees_res['reason_id']),
                                'reason'            => check_string_empty(get_selected_reasons($get_all_fees_res['reason_id'])),
                                'paid_amt'          => check_string_empty($head_amt),
                                'concession_paid'   => $concession_paid,
                            );
                        }

                        $student_image = check_file_exist($student_res->student_image, 'student_image');

                        $student_info[] = array(
                            'student_id'            => check_string_empty($student_res->student_id),
                            'student_name'          => check_string_empty($student_res->student_name),
                            'student_enroll_number' => check_string_empty($student_res->student_enroll_number),
                            'student_roll_no'       => check_string_empty($student_res->student_roll_no),
                            'class_id'              => check_string_empty($student_res->current_class_id),
                            'section_id'            => check_string_empty($student_res->current_section_id),
                            'profile_image'         => check_string_empty($student_image),
                            'student_father_name'   => check_string_empty($student_res->student_father_name),
                            'concession_info'       => $concession_info,
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['student_info'] = $student_info;
                    $lastPage = $student->lastPage($student_res);
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : saveConcessionData
     *  To save Concession Data
     *  @Sumit on 28 JAN 2019    
     *  @parameters : device_id,csrf_token,student_id,head_id,concession_amount,reason_id,class_id,concession_map_id
     *  @V2R by Sumit
    */
    public function saveConcessionData(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $admin_id       = $request->input('admin_id');
        $class_id       = $request->input('class_id');
        $student_id     = $request->input('student_id');
        $head_id        = $request->input('head_id');
        $head_inst_id   = $request->input('head_inst_id');
        $head_type      = $request->input('head_type');
        $concession_amount     = $request->input('concession_amount');
        $reason_id             = $request->input('reason_id');
        $concession_map_id     = $request->input('concession_map_id');
        $concession     = $request->input('concession');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id)  && !empty($admin_id) && !empty($student_id)  && !empty($concession)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                
                $session = get_current_session();
                $concession_arr = json_decode($concession, true);
                
                foreach($concession_arr as $concession_res){
                    
                    DB::beginTransaction();
                    try
                    {
                        if($concession_res['concession_map_id'] != ""){
                            $concession = ConcessionMap::find($concession_res['concession_map_id']);
                            $admin_id   = $concession->admin_id;
                        } else {
                            $concession = New ConcessionMap;
                            $admin_id  = $admin_id;
                        }
                        if($concession_res['head_type'] == "ONETIME"){
                            $save_head_id = $concession_res['head_id'];
                        } else if($concession_res['head_type'] == "RECURRING"){
                            $save_head_id = $concession_res['head_inst_id'];
                        }

                        $concession->admin_id           = $admin_id;
                        $concession->update_by          = $admin_id;
                        $concession->session_id         = $session['session_id'];
                        $concession->class_id           = $class_id;
                        $concession->student_id         = $student_id;
                        $concession->reason_id          = $concession_res['reason_id'];
                        $concession->head_id            = $save_head_id;
                        $concession->head_type          = $concession_res['head_type'];
                        $concession->concession_amt     = $concession_res['concession_amount'];
                        $concession->save();
                        
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                }

                
                $status = true;
                $message = 'success';

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getConcessionReason
     *  To get Concession Reason
     *  @Sumit on 07 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getConcessionReason(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $lastPage = 0;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $reason = Reason::where('reason_status',1)->paginate(20);

                if( !empty($reason) ){

                    $reason_info = [];
                    foreach($reason as $reason_res){
                        
                        $reason_info[] = array(
                            'reason_id'            => check_string_empty($reason_res->reason_id),
                            'reason_text'          => check_string_empty($reason_res->reason_text),
                        );
                    }
                    
                    $status = true;
                    $message = 'success';
                    $result_data['reason_info'] = $reason_info;
                    $lastPage = $reason->lastPage($reason_res);  
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getStudentFeesDetails
     *  To get student fees details
     *  @Sumit on 07 FEB 2019    
     *  @parameters : device_id,csrf_token,class_id,section_id,student_id
     *  @V2R by Sumit
    */
    public function getStudentFeesDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $student_id     = $request->input('student_id');
        $lastPage = 0;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id)  && !empty($student_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $upcoming_fees_arr = [];
                $fees_data      = api_get_complete_fees_student($student_id,$class_id);
                $paid_fees_data = api_get_complete_paid_fees_student($student_id,$class_id);
                $due_fees_data  = api_get_complete_due_fees_student($student_id,$class_id);
                $fees_total     = api_get_fees_total_student($student_id,$class_id);
                $fees_summary   = api_get_fees_total_student($student_id,$class_id);

                $student_info        = Student::where(function($query) use ($request,$session,$student_id) 
                {
                    $query->where('students.student_status',1);
                    $query->where('students.student_id',$student_id);
                
                })->with('getStudentAcademic.getCurrentClass')
                ->first();
                

                $student_info_all = array(
                    'student_name'          => $student_info->student_name,
                    'student_enroll_number' => $student_info->student_enroll_number,
                    'class_name'            => $student_info->getStudentAcademic->getCurrentClass->class_name
                );

                if( !empty($fees_data) ){
                    
                    foreach ($fees_data as $fees_data_res) {
                        if( $fees_data_res['head_expire'] !=1 ){
                            $upcoming_fees_arr[] = array(
                                'fees_name'         => $fees_data_res['fees_name'],
                                'head_id'           => $fees_data_res['head_id'],
                                'head_name'         => $fees_data_res['head_name'],
                                'head_amt'          => $fees_data_res['head_amt'],
                                'head_type'         => $fees_data_res['head_type'],
                                'effective_date'    => $fees_data_res['head_date'],
                            );
                        }
                    }
                }

                $student_fees_info = array(
                    'student_info'  => $student_info_all,
                    'fees_summary'  => $fees_summary,
                    'upcoming_fees' => $upcoming_fees_arr,
                    'paid_fees'     => $paid_fees_data,
                    'due_fees'      => $due_fees_data,
                );
                
                if( !empty($student_fees_info) ){
                    $status = true;
                    $message = 'success';
                    $result_data['student_fees_info'] = $student_fees_info;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getLatestFeesDetails
     *  To get latest fees details
     *  @Sandeep on 23 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function getLatestFeesDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $lastPage = 0;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session    = get_current_session();

                $receipt_total_amount       = Receipt::where('session_id', '=',$session['session_id'])->orderby('receipt_id', 'DESC')->paginate(20)->sum('total_amount');
                $receipt_pay_later_amount   = Receipt::where('session_id', '=',$session['session_id'])->orderby('receipt_id', 'DESC')->paginate(20)->sum('pay_later_amount');
                $receipt_total_paid_amount  = $receipt_total_amount-$receipt_pay_later_amount;
                $receipt_concession_amount  = Receipt::where('session_id', '=',$session['session_id'])->orderby('receipt_id', 'DESC')->paginate(20)->sum('total_concession_amount');
                $receipt_fine_amount        = Receipt::where('session_id', '=',$session['session_id'])->orderby('receipt_id', 'DESC')->paginate(20)->sum('total_calculate_fine_amount');

                $all_receipt = Receipt::where('session_id', '=',$session['session_id'])->with('ReceiptDetail')->with('getStudent')->with('getClass')->with('getStudent.getStudentAcademic')->with('getStudent.getStudentAcademic.getCurrentSection')->orderby('receipt_id', 'DESC')->paginate(20)->toArray();

                foreach ($all_receipt['data'] as $all_receipts) {
                    
                    $student_id   = $all_receipts['student_id'];
                    $class_id     = $all_receipts['class_id'];

                    if($all_receipts['receipt_detail']['head_type'] == "RECURRING"){
                        
                        $fees_data  = RecurringHead::where('rc_head_id',$all_receipts['receipt_detail']['head_id'])->first();
                        $fees_name  = $fees_data->rc_particular_name;
                        $total_fees = $fees_data->rc_amount;
                    
                    } else if($all_receipts['receipt_detail']['head_type'] == "ONETIME") {
                        
                        $fees_data  = OneTime::where('ot_head_id',$all_receipts['receipt_detail']['head_id'])->first();
                        $fees_name  = $fees_data->ot_particular_name;
                        $total_fees = $fees_data->ot_amount;
                    }

                    $student_image = check_file_exist($all_receipts['get_student']['student_image'], 'student_image');  

                    $all_receipts_data[] = array( 
                        'receipt_id'        => $all_receipts['receipt_id'],
                        'receipt_number'        => $all_receipts['receipt_number'],
                        'student_name'      => $all_receipts['get_student']['student_name'],
                        'student_enroll_number'      => $all_receipts['get_student']['student_enroll_number'], 
                        'student_image'     => check_string_empty($student_image),
                        'class_name'        => check_string_empty($all_receipts['get_class']['class_name']),
                        'section_name'      => check_string_empty($all_receipts['get_student']['get_student_academic']['get_current_section']['section_name']), 
                        'fees_name'         => check_string_empty($fees_name),
                        'receipt_date'      =>  $all_receipts['receipt_date'],
                        'total_fees'        => $all_receipts['total_amount'],
                        'total_paid_amount' => $all_receipts['total_amount'] - $all_receipts['pay_later_amount'],
                        'due_amount'        => check_string_empty($all_receipts['pay_later_amount']),
                        'concession_amount' => check_string_empty($all_receipts['total_concession_amount']),
                        'fine_amount'       => check_string_empty($all_receipts['total_calculate_fine_amount']),
                    );
                }


                $fees_info = array(
                    'receipt_total_amount'          => $receipt_total_amount,
                    'receipt_pay_later_amount'      => $receipt_pay_later_amount,
                    'receipt_total_paid_amount'     => $receipt_total_paid_amount,
                    'receipt_concession_amount'     => $receipt_concession_amount,
                    'receipt_fine_amount'           => $receipt_fine_amount,
                    'all_receipts_data'             => $all_receipts_data,
                );
                
                if( !empty($fees_info) ){
                    $status = true;
                    $message = 'success';
                    $result_data['fees_info'] = $fees_info;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : getPaidFeesDetails
     *  To get paid fees details
     *  @Sandeep on 28 march 2019    
     *  @parameters : device_id,csrf_token,receipt_id
     *  @V2R by Sandeep
    */
    public function getPaidFeesDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $receipt_id     = $request->input('receipt_id');
        $lastPage = 0;

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($receipt_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $all_receipt = Receipt::where('receipt_id', '=',$receipt_id)->with('ReceiptDetailAll')->first();

                //p($all_receipt['ReceiptDetailAll']);
                foreach ($all_receipt['ReceiptDetailAll'] as $receipt_detail) {

                    if($receipt_detail['head_type'] == "RECURRING"){
                    
                        $fees_data  = RecurringHead::where('rc_head_id',$receipt_detail['head_id'])->first();
                        $fees_name  = $fees_data->rc_particular_name;
                        $total_fees = $fees_data->rc_amount;
                    
                    } else if($receipt_detail['head_type'] == "ONETIME") {
                        
                        $fees_data  = OneTime::where('ot_head_id',$receipt_detail['head_id'])->first();
                        $fees_name  = $fees_data->ot_particular_name;
                        $total_fees = $fees_data->ot_amount;
                    
                    }

                    $all_receipts_details[] = array( 
                        'fees_id'           => $receipt_detail['fee_receipt_detail_id'],
                        'fees_name'         => check_string_empty($fees_name),
                        'fees_amount'       =>  $receipt_detail['inst_paid_amt'],
                    );

                }  

                $all_receipts_data = array( 
                    'receipt_id'            => $all_receipt['receipt_id'],
                    'receipt_date'          => $all_receipt['receipt_date'],
                    'receipt_number'        => $all_receipt['receipt_number'],
                    'total_fees'            => $all_receipt['total_amount'],
                    'net_amount'            => $all_receipt['net_amount'],
                    'total_paid_amount'     => $all_receipt['total_amount'] - $all_receipt['pay_later_amount'],
                    'concession_amount'     => check_string_empty($all_receipt['total_concession_amount']),
                    'fine_amount'           => check_string_empty($all_receipt['total_calculate_fine_amount']),
                    'imprest_amount'        => check_string_empty($all_receipt['imprest_amount']),
                    'wallet_amount'         => check_string_empty($all_receipt['wallet_amount']),
                    'payment_mode'          => check_string_empty($all_receipt['pay_mode']),
                    'fees_details'          => $all_receipts_details
                );
                
                if( !empty($all_receipts_data) ){
                    $status = true;
                    $message = 'success';
                    $result_data = $all_receipts_data;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        
        return response()->json($result_response, 200);
    }


}
