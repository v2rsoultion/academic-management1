<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Staff\StaffLeaveApplication; // StaffLeaveApplication Model
use App\Model\StudentLeaveApplication\StudentLeaveApplication; // StudentLeaveApplication Model

use DB;
use Hash;
use DateTime;

class LeavesController extends Controller
{

    /*  Function name : getLeaveList
     *  To get all leaves and single leaves details according to user_type and user_id
     *  @Sumit on 28 NOV 2018    
     *  @parameters : device_id,csrf_token,user_type,user_id
     *  @V2R by Sumit
    */
    public function getLeaves(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_type      = $request->input('user_type');
        $user_id        = $request->input('user_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $designation_id = $request->input('designation_id');
        $name           = $request->input('name');
        $date           = $request->input('date');
        $status         = $request->input('status');
        $roll_no        = $request->input('roll_no');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Staff Leaves Data
                if( $user_type == 1 ){

                    $staff_leave = StaffLeaveApplication::leftJoin('staff', 'staff_leaves.staff_id', '=', 'staff.staff_id')->where(function($query) use($request, $user_id, $designation_id, $name, $date, $status){

                        if ( !empty($user_id) )
                        {
                            $query->where('staff.staff_id', $user_id);
                        }

                        if ( !empty($request->get('leave_id')) )
                        {
                            $query->where('staff_leaves.staff_leave_id', $request->get('leave_id'));
                        }

                        if ( !empty($designation_id) )
                        {
                            $query->where('staff.designation_id', $designation_id);
                        }

                        if ( !empty($date) )
                        {
                            $query->whereRaw('? between staff_leave_from_date and staff_leave_to_date', [$date]);
                        }

                        if ( !empty($name) )
                        {
                            $query->where('staff.staff_name', "like", "%{$name}%");
                        }

                        if ( $status !='' )
                        {
                            $query->where('staff_leaves.staff_leave_status', $status);
                        }

                    })->with('getStaff')->with('getStaff.getDesignation')->orderby('staff_leave_id', 'DESC')->select('staff_leaves.created_at as leave_applied_date', 'staff_leaves.staff_leave_id', 'staff.staff_name', 'staff.designation_id', 'staff_leaves.staff_leave_attachment', 'staff.staff_profile_img', 'staff_leaves.staff_leave_reason', 'staff_leaves.staff_leave_from_date', 'staff_leaves.staff_leave_to_date', 'staff_leaves.staff_leave_status', 'staff.staff_id')->paginate(20);

                    if(count($staff_leave) != 0){

                        foreach($staff_leave as $staff_leave_res){

                            $staff_leave_file = check_file_exist($staff_leave_res['staff_leave_attachment'], 'staff_leave_document_file');

                            $profile_image = check_file_exist($staff_leave_res['getStaff']['staff_profile_img'], 'staff_profile');

                            $staff_leave_info[] = array(
                                'leave_id' => check_string_empty($staff_leave_res->staff_leave_id),
                                'name' => check_string_empty($staff_leave_res['getStaff']['staff_name']),
                                'profile_image' => $profile_image,
                                'staff_designation' => check_string_empty($staff_leave_res['getStaff']['getDesignation']['designation_name']),
                                'leave_reason' => check_string_empty($staff_leave_res['staff_leave_reason']),
                                'leave_applied_date' => date('d F Y',strtotime($staff_leave_res['leave_applied_date'])),
                                'leave_from_date' => date('d F Y',strtotime($staff_leave_res['staff_leave_from_date'])),
                                'leave_to_date' => date('d F Y',strtotime($staff_leave_res['staff_leave_to_date'])),
                                'leaves_file' => $staff_leave_file,
                                'leave_status' => $staff_leave_res->staff_leave_status,
                            );
                        }

                        $result_data = $staff_leave_info;
                        $lastPage = $staff_leave->lastPage($staff_leave_res);      	
                        $status = true;
                        $message = 'success';

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                // Student Leaves Data

                } else if($user_type == 2){

                    $student_leaves = StudentLeaveApplication::leftJoin('student_academic_info', 'student_leaves.student_id', '=', 'student_academic_info.student_id')->leftJoin('students', 'student_leaves.student_id', '=', 'students.student_id')->where(function($query) use($request, $user_id, $class_id, $section_id, $name, $date, $status, $roll_no){

                        if ( !empty($user_id) )
                        {
                            $query->where('student_leaves.student_id', $user_id);
                        }

                        if ( !empty($request->get('leave_id')) )
                        {
                            $query->where('student_leaves.student_leave_id', $request->get('leave_id'));
                        }

                        if ( !empty($class_id) )
                        {
                            $query->where('student_academic_info.current_class_id', $class_id);
                        }

                        if ( !empty($section_id) )
                        {
                            $query->where('student_academic_info.current_section_id', $section_id);
                        }

                        if ( !empty($date) )
                        {
                            $query->whereRaw('? between student_leave_from_date and student_leave_to_date', [$date]);
                        }

                        if ( !empty($name) )
                        {
                            $query->where('students.student_name', "like", "%{$name}%");
                        }

                        if ( $status !='' )
                        {
                            $query->where('student_leaves.student_leave_status', $status);
                        }

                        if ( !empty($roll_no) )
                        {
                            $query->where('students.student_roll_no', $roll_no);
                        }

                    })->with('getStudent')->with('getStudent.getTitle')->with('getStudent.getStudentAcademic.getCurrentClass')->with('getStudent.getStudentAcademic.getCurrentSection')->select('student_leaves.student_leave_id','student_leaves.student_leave_reason', 'student_leaves.student_leave_from_date', 'student_leaves.student_leave_to_date', 'student_leaves.student_leave_attachment', 'student_leaves.student_leave_status','student_leaves.created_at as leave_applied_date',  'students.student_id')->orderby('student_leave_id', 'DESC')->paginate(20);

                    if(count($student_leaves) != 0){

                        foreach($student_leaves as $student_leaves_res){

                            $student_leaves_file = check_file_exist($student_leaves_res['student_leave_attachment'], 'student_leave_document_file');

                            $profile_image = check_file_exist($student_leaves_res['getStudent']['student_image'], 'student_image');

                            $student_leaves_info[] = array(
                                'leave_id' => check_string_empty($student_leaves_res->student_leave_id),
                                'student_title' => check_string_empty($student_leaves_res['getStudent']['getTitle']['title_name']),
                                'name' => check_string_empty($student_leaves_res['getStudent']['student_name']),
                                'profile_image' => $profile_image,
                                'roll_no' => check_string_empty($student_leaves_res['getStudent']['student_roll_no']),
                                'student_class' => check_string_empty($student_leaves_res['getStudent']['getStudentAcademic']['getCurrentClass']['class_name']),
                                'student_section' => check_string_empty($student_leaves_res['getStudent']['getStudentAcademic']['getCurrentSection']['section_name']),
                                'leave_reason' => check_string_empty($student_leaves_res['student_leave_reason']),
                                'leave_applied_date' => date('d F Y',strtotime($student_leaves_res['leave_applied_date'])),
                                'leave_from_date' => date('d F Y',strtotime($student_leaves_res['student_leave_from_date'])),
                                'leave_to_date' => date('d F Y',strtotime($student_leaves_res['student_leave_to_date'])),
                                'leaves_file'  => $student_leaves_file,
                                'leave_status' => $student_leaves_res->student_leave_status,
                            );
                        }

                        $result_data = $student_leaves_info;
                        $lastPage = $student_leaves->lastPage($student_leaves_res);
                        $status = true;
                        $message = 'success';

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : updateStatus
     *  To update leave status
     *  @Sumit on 14 DEC 2018    
     *  @parameters : device_id,csrf_token,leave_id,status
     *  @V2R by Sumit
    */
    public function updateStatus(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $user_type      = $request->input('user_type');
        $leave_id       = $request->input('leave_id');
        $status         = $request->input('status');
        $attachment     = $request->input('attachment');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type) && !empty($leave_id) && $status != "" ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Staff Leaves Data
                if( $user_type == 1 ){

                    $staff_leave  = StaffLeaveApplication::find($leave_id);

                    if( !empty($staff_leave) ){
                        DB::beginTransaction();
                        try
                        {

                            if (empty($attachment)){
                                $attachment = check_file_exist($staff_leave->staff_leave_attachment, 'staff_leave_attachment');
                                if (!empty($attachment))
                                {
                                    unlink($attachment);
                                }
                            }

                            $staff_leave->staff_leave_status      = $status;
                            $staff_leave->staff_leave_attachment  = $attachment;
                            $staff_leave->save();
                            $status = true;
                            $message = 'success';
                        }
                        catch (\Exception $e)
                        {
                            //failed logic here
                            DB::rollback();
                            $error_message = $e->getMessage();
                            $status = false;
                            $message = $error_message;
                        }
                        DB::commit();

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                // Student Leaves Data
                } else if($user_type == 2){

                    $student_leave  = StudentLeaveApplication::find($leave_id);

                    if( !empty($student_leave) ){
                        DB::beginTransaction();
                        try
                        {
                            if (empty($attachment)){
                                $attachment = check_file_exist($student_leave->student_leave_attachment, 'student_leave_document_file');

                                if (!empty($attachment))
                                {
                                    unlink($attachment);
                                }
                            }

                            $student_leave->student_leave_status      = $status;
                            $student_leave->student_leave_attachment  = $attachment;
                            $student_leave->save();
                            $status = true;
                            $message = 'success';
                        }
                        catch (\Exception $e)
                        {
                            //failed logic here
                            DB::rollback();
                            $error_message = $e->getMessage();
                            $status = false;
                            $message = $error_message;
                        }
                        DB::commit();
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
    
        return response()->json($result_response, 200);
    }

}

