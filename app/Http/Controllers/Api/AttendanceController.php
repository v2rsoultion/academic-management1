<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Staff\Staff; // Staff Model
use App\Model\Staff\StaffAttendance;  // StudentAttendance Model
use App\Model\Student\Student; // Student Model
use App\Model\Student\StudentAttendance; // Admin Model
use App\Model\Student\StudentAttendanceDetails; // StudentAttendanceDetails Model

use DateTime; 
use DB;
use Hash; 

class AttendanceController extends Controller
{

    /*  Function name : getAttendanceDateWise
     *  To get attendance date wise
     *  @Sumit on 30 NOV 2018
     *  @parameters : device_id,csrf_token,date,class_id,section_id,designation_id,user_type
     *  @V2R by Sumit
     */
    public function getAttendanceDateWise(Request $request) {
        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $user_type = $request->input('user_type');
        $class_id = $request->input('class_id');
        $section_id = $request->input('section_id');
        $designation_id = $request->input('designation_id');
        $date = $request->input('date');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type) && !empty($date)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                // Staff Attendance Data
                if ($user_type == 1) {

                    $holiday_list = get_school_all_holidays(1);

                    $staff_attendance = Staff::where(function ($query) use ($request, $designation_id) {

                        $query->where('staff.staff_status', 1);

                        if (!empty($designation_id)) {
                            $query->where('staff.designation_id', $designation_id);
                        }
                    })
                    ->leftJoin('staff_attend_details', function ($join) use ($request, $date) {

                        $join->on('staff_attend_details.staff_id', '=', 'staff.staff_id');

                        if ($date != '') {

                            $join->where('staff_attend_details.staff_attendance_date', '=', $date);

                        }

                    })->select('staff.staff_name', 'staff.staff_profile_img', 'staff.staff_attendance_unique_id', 'staff_attend_details.*', 'staff.staff_id')
                    ->get();

                    // 1 for staff
                    $attendance_mark_status = $this->check_attendance_exist(1, $date);

//                    p($staff_attendance);

                    if (count($staff_attendance) != 0) {

                        $staff_attendance_arr = [];

                        foreach ($staff_attendance as $staff_att_res) {

                            if ($staff_att_res['in_time'] != '') {
                                $in_time = check_string_empty(date("H:i A", strtotime($staff_att_res['in_time'])));
                            } else {
                                $in_time = '';
                            }

                            if ($staff_att_res['out_time'] != '') {
                                $out_time = check_string_empty(date("H:i A", strtotime($staff_att_res['out_time'])));
                            } else {
                                $out_time = '';
                            }

                            // if( check_string_empty($staff_att_res['staff_attendance_type']) !='' ){
                            //     $staff_attendance_type = check_string_empty($staff_att_res['staff_attendance_type']);
                            // } else {
                            //     $staff_attendance_type = 3;
                            // }

                            $staff_attendance_type = '';

                            if ($staff_att_res['staff_attendance_date'] == '' && $attendance_mark_status == 0) {
                                $staff_attendance_type = 2;
                            } else if ($staff_att_res['staff_attendance_id'] == '' && $staff_att_res['staff_attend_d_id'] == '' && $attendance_mark_status == 1) {
                                $staff_attendance_type = 4;
                            } else {
                                $staff_attendance_type = $staff_att_res['staff_attendance_type'];
                            }

                            if (in_array($date, $holiday_list)) {
                                $staff_attendance_type = 3;
                            }

                            $staff_attendance_arr[] = array(
                                'staff_attend_d_id' => check_string_empty($staff_att_res['staff_attend_d_id']),
                                'staff_id' => $staff_att_res['staff_id'],
                                'staff_name' => $staff_att_res['staff_name'],
                                'in_time' => $in_time,
                                'out_time' => $out_time,
                                'staff_attendance_type' => $staff_attendance_type,
                                'staff_overtime_type' => $staff_att_res['staff_overtime_type'],
                                'staff_overtime_value' => check_string_empty($staff_att_res['staff_overtime_value']),
                                'staff_attendance_status' => $staff_att_res['staff_attendance_status'],
                                'staff_attendance_date' => check_string_empty($staff_att_res['staff_attendance_date']),
                            );
                        }

                        $result_data = $staff_attendance_arr;
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No Record Found';
                    }

                    // Student Attendance Data

                } else if ($user_type == 2) {

                    $session = get_current_session();

                    $holiday_list = get_school_all_holidays(1);

                    $student_attendance = Student::
                        leftJoin('student_academic_info', function ($join) use ($request, $session) {
                        $join->on('student_academic_info.student_id', '=', 'students.student_id');

                        if ($request->get('class_id') != '') {
                            $join->where('student_academic_info.current_class_id', '=', $request->get('class_id'));
                        }

                        if ($request->get('section_id') != '') {
                            $join->where('student_academic_info.current_section_id', '=', $request->get('section_id'));
                        }

                        // $join->where('student_academic_info.current_session_id', '=',$session['session_id']);

                    })
                    ->leftJoin('student_attend_details', function ($join) use ($request, $session) {

                        $join->on('student_attend_details.student_id', '=', 'students.student_id');

                        if ($request->get('date') != '') {
                            $join->where('student_attend_details.student_attendance_date', '=', $request->get('date'));
                        }
                        // $join->where('student_attend_details.session_id', '=',$session['session_id']);
                    })
                    ->select('students.student_name', 'students.student_image', 'students.student_roll_no', 'students.student_enroll_number', 'student_attend_details.*', 'students.student_id','student_academic_info.current_section_id','student_academic_info.current_class_id')
                    ->get();

                    //p($student_attendance);

                    $attendance_mark_status = $this->check_attendance_exist(0, $date);

                    if (!empty($student_attendance)) {

                        $student_attendance_arr = [];

                        foreach ($student_attendance as $student_att_res) {

                            //if($student_att_res->student_attendance_date != ""){
                                if($student_att_res->current_section_id != ""){
                                    if($student_att_res->current_class_id != ""){

                                        $student_attendance_type = '';

                                        if ($student_att_res['student_attendance_date'] == '' && $attendance_mark_status == 0) {
                                            $student_attendance_type = 2;
                                        } else if ($student_att_res['student_attendance_id'] == '' && $student_att_res['student_attend_d_id'] == '' && $attendance_mark_status == 1) {
                                            $student_attendance_type = 4;
                                        } else {
                                            $student_attendance_type = $student_att_res['student_attendance_type'];
                                        }

                                        if (in_array($date, $holiday_list)) {
                                            $student_attendance_type = 3;
                                        }


                                        $student_attendance_arr[] = array(
                                            'student_attend_d_id' => check_string_empty($student_att_res['student_attend_d_id']),
                                            'student_id' => $student_att_res['student_id'],
                                            'student_name' => $student_att_res['student_name'],
                                            'student_roll_no' => $student_att_res['student_roll_no'],
                                            'attendance_flag' => $student_attendance_type,
                                        );
                                    }
                                }
                            //}
                        }
                        if(empty($student_attendance_arr)){
                            $result_data = $student_attendance_arr;
                            $status = false;
                            $message = 'No record found';
                        } else {
                            $result_data = $student_attendance_arr;
                            $status = true;
                            $message = 'success';
                        }

                        

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

    public function check_attendance_exist($type, $date){

        // $type = 1(Staff), $type = 0 (Student)
        $attendance_mark_status = 0;

        if ($type == 0) {
            $check_status = StudentAttendance::where(function ($query) use ($date) {
                $query->where('student_attendance_date', $date);
            })
            ->get()
            ->toArray();
        } else if ($type == 1) {
            $check_status = StaffAttendance::where(function ($query) use ($date) {
                $query->where('staff_attendance_date', $date);
            })
            ->get()
            ->toArray();
        }

        if (!empty($check_status)) {
            $attendance_mark_status = 1;
        }
        return $attendance_mark_status;
    }

    /*  Function name : getAttendance
     *  To get attendance
     *  @Sumit on 04 DEC 2018
     *  @parameters : device_id,csrf_token,user_id,user_type,month_number,year
     *  @V2R by Sumit
     */
    public function getAttendance(Request $request){

        /*
         * Variable initialization
         */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
         */
        $device_id = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $user_type = $request->input('user_type');
        $user_id = $request->input('user_id');
        $month_number = $request->input('month_number');
        $year = $request->input('year');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_type) && !empty($user_id) && !empty($month_number) && !empty($year)) {

            // API Auth
            if (validate_token($device_id, $csrf_token) == true) {

                // Get Last Two Month
                $last_two_month = Date("Y-m-d", strtotime($year . "-" . $month_number . "-01" . " -2 month"));

                // Get Next Two Month
                $next_two_month = Date("Y-m-t", strtotime($year . "-" . $month_number . "-01" . " +2 month"));

                // Staff Attendance Data
                if ($user_type == 1) {

                    // $staff_attendance = DB::select("SELECT staff_attendance_id,staff_ids,staff_attendance_date,staf.staff_name,staf.staff_id, IF(staff_ids !='',0,1) as att_status FROM `staff_attendance` as sta
                    // INNER JOIN staff as staf on staf.staff_id = '".$user_id."'
                    // WHERE FIND_IN_SET(".$user_id.", sta.staff_ids) OR NOT FIND_IN_SET(".$user_id.", sta.staff_ids) OR sta.staff_ids = '' AND sta.staff_attendance_date BETWEEN '".$last_two_month."' AND '".$next_two_month."' ");

                    $staff_attendance = Staff::where(function ($query) use ($request, $user_id) {
                        $query->where('staff.staff_id', $user_id);
                    })
                    ->leftJoin('staff_attend_details', function ($join) use ($request, $last_two_month, $next_two_month) {

                        $join->on('staff_attend_details.staff_id', '=', 'staff.staff_id');

                        if ($last_two_month != '' && $next_two_month != '') {
                            $from = date($last_two_month);
                            $to = date($next_two_month);
                            $join->whereBetween('staff_attend_details.staff_attendance_date', [$from, $to]);
                        }

                    })->select('staff.staff_name', 'staff.staff_profile_img', 'staff.staff_attendance_unique_id', 'staff_attend_details.*', 'staff.staff_id')
                    ->groupBy('staff_attendance_date')->get();

                    if (!empty($staff_attendance)) {

                        $staff_attendance_arr = [];

                        $holiday_list = get_school_all_holidays(2);
                        foreach ($holiday_list as $holiday_lists) {

                            $begin = new DateTime($last_two_month);
                            $end = new DateTime($next_two_month);

                            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {

                            //    p($holiday_lists);

                                $att_date = date('m-Y', strtotime($holiday_lists));

                                if ($i->format("m-Y") == $att_date) {

                                    $staff_attendance_arr[$i->format("m-Y")][] = array(
                                        'staff_attend_d_id' => check_string_empty($staff_att_res['staff_attend_d_id']),
                                        'in_time' => check_string_empty(date("H:i A", strtotime($staff_att_res['in_time']))),
                                        'out_time' => check_string_empty(date("H:i A", strtotime($staff_att_res['out_time']))),
                                        'attendance_flag' => 3,
                                        'staff_overtime_type' => check_string_empty($staff_att_res['staff_overtime_type']),
                                        'staff_overtime_value' => check_string_empty($staff_att_res['staff_overtime_value']),
                                        'staff_attendance_status' => $staff_att_res['staff_attendance_status'],
                                        'attendance_date' => date('d F Y', strtotime($holiday_lists)),
                                    );
                                }
                            }
                        }

                        foreach ($staff_attendance as $staff_att_res) {

                            $begin = new DateTime($last_two_month);
                            $end = new DateTime($next_two_month);

                            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {

                                $att_date = date('m-Y', strtotime($staff_att_res->staff_attendance_date));

                                if ($i->format("m-Y") == $att_date) {

                                    $staff_attendance_arr[$i->format("m-Y")][] = array(
                                        'staff_attend_d_id' => check_string_empty($staff_att_res['staff_attend_d_id']),
                                        'in_time' => check_string_empty(date("H:i A", strtotime($staff_att_res['in_time']))),
                                        'out_time' => check_string_empty(date("H:i A", strtotime($staff_att_res['out_time']))),
                                        'attendance_flag' => $staff_att_res['staff_attendance_type'],
                                        'staff_overtime_type' => check_string_empty($staff_att_res['staff_overtime_type']),
                                        'staff_overtime_value' => check_string_empty($staff_att_res['staff_overtime_value']),
                                        'staff_attendance_status' => $staff_att_res['staff_attendance_status'],
                                        'attendance_date' => date('d F Y', strtotime($staff_att_res->staff_attendance_date)),
                                    );
                                }
                            }
                        }

                        $result_data = $staff_attendance_arr;
                        $status = true;
                        $message = 'success';

                    } else {
                        $result_data = 'Holiday';
                        $status = true;
                        $message = 'success';
                    }

                    // Student Attendance Data

                } else if ($user_type == 2) {

                    $student_attendance = Student::where('students.student_id', $user_id)->
                        leftJoin('student_academic_info', function ($join) use ($request, $session) {
                        $join->on('student_academic_info.student_id', '=', 'students.student_id');

                        if ($request->get('class_id') != '') {
                            $join->where('student_academic_info.current_class_id', '=', $request->get('class_id'));
                        }

                        if ($request->get('section_id') != '') {
                            $join->where('student_academic_info.current_section_id', '=', $request->get('section_id'));
                        }
                        // $join->where('student_academic_info.current_session_id', '=',$session['session_id']);
                    })
                    ->leftJoin('student_attend_details', function ($join) use ($request, $session) {

                        $join->on('student_attend_details.student_id', '=', 'students.student_id');

                        // if ( $request->get('date') !='' ){
                        //     $join->where('student_attend_details.student_attendance_date', '=',$request->get('date'));
                        // }

                        if ($last_two_month != '' && $next_two_month != '') {
                            $from = date($last_two_month);
                            $to = date($next_two_month);
                            $join->whereBetween('student_attend_details.student_attendance_date', [$from, $to]);
                            $join->orderBy('student_attendance_date', 'ASC');
                        }
                        // $join->where('student_attend_details.session_id', '=',$session['session_id']);

                    })
                    ->select('students.student_name', 'students.student_image', 'students.student_roll_no', 'students.student_enroll_number', 'student_attend_details.*', 'students.student_id')
                    ->orderBy('student_attend_details.student_attendance_date', 'ASC')->groupBy('student_attendance_date')
                    ->get();

                    // $student_attendance = DB::select("SELECT student_attendance_id,student_ids,student_attendance_date,stud.student_name,stud.student_id, IF(student_ids !='',0,1) as att_status FROM `student_attendance` as sta
                    // INNER JOIN students as stud on stud.student_id = '".$user_id."'
                    // WHERE FIND_IN_SET(".$user_id.", sta.student_ids) OR NOT FIND_IN_SET(".$user_id.", sta.student_ids) OR sta.student_ids = '' AND sta.student_attendance_date BETWEEN '".$last_two_month."' AND '".$next_two_month."' ");

                    //   p($student_attendance);
                    if (!empty($student_attendance)) {

                        $student_attendance_arr = [];

                        $holiday_list = get_school_all_holidays(1);
                        foreach ($holiday_list as $holiday_lists) {

                            $begin = new DateTime($last_two_month);
                            $end = new DateTime($next_two_month);

                            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {

                            //    p($holiday_lists);

                                $att_date = date('m-Y', strtotime($holiday_lists));

                                if ($i->format("m-Y") == $att_date) {

                                    $student_attendance_arr[$i->format("m-Y")][] = array(
                                        'attendance_flag' => 3,
                                        'attendance_date' => date('d F Y', strtotime($holiday_lists)),
                                    );
                                }
                            }
                        }

                        foreach ($student_attendance as $student_att_res) {

                            $begin = new DateTime($last_two_month);
                            $end = new DateTime($next_two_month);

                            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {                                

                                $att_date = date('m-Y', strtotime($student_att_res->student_attendance_date));

                                if ($i->format("m-Y") == $att_date) {

                                    $student_attendance_arr[$i->format("m-Y")][] = array(
                                        'attendance_flag' => $student_att_res->student_attendance_type,
                                        'attendance_date' => date('d F Y', strtotime($student_att_res->student_attendance_date)),
                                    );
                                }
                            }
                        }

                        if (!empty($student_attendance_arr)) {
                            $result_data = $student_attendance_arr;
                            $status = true;
                            $message = 'success';
                        } else {
                            $status = false;
                            $message = 'No record found';
                        }

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] = $status;
        $result_response['message'] = $message;
        $result_response['data'] = $result_data;
        return response()->json($result_response, 200);
    }

}
