<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Staff\Staff;
use App\Model\Staff\StaffDocument;

use App\Model\Address\Country;
use App\Model\Address\State;
use App\Model\Address\City;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Excel;

class StaffController extends Controller
{
    /**
     *  View page for staff
     *  @Shree on 3 Aug 2018
    **/
   
    public function index()
    {
        $loginInfo                      = get_loggedin_user_data();
        $arr_designation                = get_all_designations();
        $listData                       = [];
        $listData['arr_designation']    = add_blank_option($arr_designation, 'Select Designation');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/view-staff'),
            'page_title'    => trans('language.view_staff'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.staff.index')->with($data);
    }

     /**
     *  Get Data for view staff page(Datatables)
     *  @Shree on 3 Aug 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $staff  = Staff::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('designation_id')) && $request->get('designation_id') != null && $request->get('designation_id') != "Select destination")
            {
                $query->where('designation_id', "=", $request->get('designation_id'));
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('staff_name', "like", "%{$request->get('name')}%");
            }
        })->select('staff_id','staff_name','designation_id','staff_status')->orderBy('staff_id', 'DESC')->get();
        return Datatables::of($staff)
            ->addColumn('staff_designation_name', function ($staff)
            {
                $arr_designations   = get_all_designations();
                $staff_designation_name  = $arr_designations[$staff->designation_id];
                return $staff_designation_name;
            })
            
            ->addColumn('action', function ($staff)
            {
                $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
                if($staff->staff_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a href="view-profile/' . $encrypted_staff_id . '" ">View Profile</a></li>
                    </ul>
                </div>
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="staff-status/'.$status.'/' . $encrypted_staff_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-staff/' . $encrypted_staff_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-staff/' . $encrypted_staff_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
               
                
            })->rawColumns(['action' => 'action', 'staff_designation_name'=>'staff_designation_name'])->addIndexColumn()->make(true);
    }

     /**
     *  Add page for staff
     *  @Shree on 3 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $staff  =  $arr_state_p = $arr_city_p = $arr_state_t = $arr_city_t = [];
        $data     = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_staff_id = get_decrypted_value($id, true);
            $staff              = Staff::where('staff_id','=',$decrypted_staff_id)->with('documents')->get();
            $staff              = isset($staff[0]) ? $staff[0] : [];
            
            if (!$staff)
            {
                return redirect('admin-panel/staff/add-staff')->withError('Staff not found!');
            }
            if($staff['staff_role_id'] != '') {
                $staff['staff_role_id'] = explode(',',$staff['staff_role_id']);
            }

            if($staff['shift_ids'] != '') {
                $staff['shift_ids'] = explode(',',$staff['shift_ids']);
            }
            
            // p($staff);
            $document_list = [];
            foreach ($staff['documents'] as $documents)
            {
                $document_data['staff_document_id']       = $documents['staff_document_id'];
                $document_data['document_category_id']      = $documents['document_category_id'];
                $document_data['document_category_name']    = $documents['getDocumentCategory']['document_category_name'];
                $document_data['staff_document_details']  = $documents['staff_document_details'];
                $document_data['staff_document_status']  = $documents['staff_document_status'];
                $document_data['document_submit_date']      = date("d F, Y ", strtotime($documents['created_at']->toDateString()));
                if (!empty($documents['staff_document_file']))
                {
                    $document_file = check_file_exist($documents['staff_document_file'], 'staff_document_file');
                    if (!empty($document_file))
                    {
                        $document_data['staff_document_file'] = $document_file;
                    } 
                }else {
                    $document_data['staff_document_file'] = "";
                }
                $document_list[] = $document_data;
            }
            $staff->documents = $document_list;
            $arr_state_p          = get_all_states($staff->staff_permanent_county);
            $arr_city_p           = get_all_city($staff->staff_permanent_state);
            $arr_state_t          = get_all_states($staff->staff_permanent_county);
            $arr_city_t           = get_all_city($staff->staff_permanent_state);
            $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
            $page_title         = trans('language.edit_staff');
            $save_url           = url('admin-panel/staff/save/' . $id);
            $submit_button      = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_staff');
            $save_url      = url('admin-panel/staff/save');
            $submit_button = 'Save';
        }
        $staff['arr_session'] = [];
        $staff_gender             = \Config::get('custom.staff_gender');
        $arr_marital              = \Config::get('custom.staff_marital');
        $arr_teaching_status      = \Config::get('custom.teaching_status');

        $staff['arr_gender']        = $staff_gender;
        $staff['arr_marital']       = $arr_marital;
        $staff['arr_teaching_status']   = $arr_teaching_status;
        
        $arr_shift                      = get_all_shift();
        $arr_title                      = get_titles();
        $arr_caste                      = get_caste();
        $arr_religion                   = get_religion();
        $arr_natioanlity                = get_nationality();
        $arr_staff_roles                = get_all_staff_roles();
        $arr_designations               = get_all_designations();
        $arr_document_category          = get_all_document_category(1);
        $arr_country                    = get_all_country();
        $staff['arr_shift']             = $arr_shift;
        $staff['arr_title']             = add_blank_option($arr_title, 'Select Title');
        $staff['arr_caste']             = add_blank_option($arr_caste, 'Select caste');
        $staff['arr_religion']          = add_blank_option($arr_religion, 'Select religion');
        $staff['arr_staff_roles']       = $arr_staff_roles;
        $staff['arr_designations']      = add_blank_option($arr_designations, 'Select designation');
        $staff['arr_natioanlity']       = add_blank_option($arr_natioanlity, 'Select nationality');
        $staff['arr_document_category'] = add_blank_option($arr_document_category, 'Document category');
        $staff['arr_country']           = add_blank_option($arr_country, 'Select Country');
        $staff['arr_state_p']           = add_blank_option($arr_state_p, 'Select State');
        $staff['arr_city_p']            = add_blank_option($arr_city_p, 'Select City');
        $staff['arr_state_t']           = add_blank_option($arr_state_t, 'Select State');
        $staff['arr_city_t']            = add_blank_option($arr_city_t, 'Select City');
        
        if (!empty($staff['staff_profile_img']))
        {
            $staff['staff_profile'] = check_file_exist($staff['staff_profile_img'], 'staff_profile');
        }

        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'staff'       => $staff,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/view-staff'),
        );
        return view('admin-panel.staff.add')->with($data);
    }

    /**
     *  Save Staff Data
     *  @Shree on 21 Oct 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        //p($request->all());
        $staff_id           = null;
        $decrypted_staff_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_staff_id   = get_decrypted_value($id, true);
            $staff                = Staff::find($decrypted_staff_id);
            $staffAdmin           = Admin::Find($staff->reference_admin_id);
            $admin_id = $staff['admin_id'];
            if (!$staff)
            {
                return redirect('/admin-panel/staff/add-staff')->withError('Staff not found!');
            }
            $success_msg = 'Staff updated successfully!';
        }
        else
        {
            $staff            = New Staff;
            $staffAdmin       = new Admin();
            $success_msg        = 'Staff saved successfully!';
        }
        
        $arr_input_fields = [
            'staff_name'                        => 'required',
           // 'staff_email'                       => 'required|unique:staff,staff_email,' . $decrypted_staff_id . ',staff_id',
            'staff_aadhar'                      => 'required',
            'designation_id'                    => 'required',
            'staff_role_id'                     => 'required',
            'staff_mobile_number'               => 'required|unique:staff,staff_mobile_number,' . $decrypted_staff_id . ',staff_id',
            'staff_dob'                         => 'required',
            'staff_gender'                      => 'required',
            'staff_father_name_husband_name'    => 'required',
            'staff_father_husband_mobile_no'    => 'required',
            'staff_mother_name'                 => 'required',
            'staff_marital_status'              => 'required',
            'title_id'                          => 'required',
            'caste_id'                          => 'required',
            'nationality_id'                    => 'required',
            'religion_id'                       => 'required',
            'staff_qualification'               => 'required',
            'staff_specialization'              => 'required',
            'staff_experience'                  => 'required',
            'staff_permanent_address'           => 'required',
            'staff_permanent_city'              => 'required',
            'staff_permanent_state'             => 'required',
            'staff_permanent_county'            => 'required',
            'staff_permanent_pincode'           => 'required'
        ];
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                    if (empty($id))
                    {

                        if($request->get('staff_email') != ''){
                            $arr_input_fields = [
                                'staff_email'   => 'required|unique:admins,email',
                                'staff_mobile_number'   => 'required|unique:admins,mobile_no',
                            ];
                        } else {
                            $arr_input_fields = [
                                // 'staff_email'   => 'unique:admins,email',
                                'staff_mobile_number'   => 'required|unique:admins,mobile_no',
                            ];
                        }
                       
                        $validatior = Validator::make($request->all(), $arr_input_fields);
                        if ($validatior->fails())
                        {
                            return redirect()->back()->withInput()->withErrors($validatior);
                        }
                        $staffAdmin->admin_name = Input::get('staff_name');
                        $staffAdmin->mobile_no = Input::get('staff_mobile_number');
                        $staffAdmin->email = Input::get('staff_email');
                        // $staffPassword = str_random(8);
                        $staffPassword = 'staff@123!';
                        $staffAdmin->admin_type = 2;
                        $staffAdmin->password = Hash::make($staffPassword);
                        $staffAdmin->save();
                        // $message = "";
                        // $subject = "Welcome to ".trans('language.project_title')." | Staff Panel";
                        // $emailData = [];
                        // $emailData['sender'] = trans('language.email_sender');
                        // $emailData['email'] = Input::get('staff_email');
                        // $emailData['password'] = $staffPassword;
                        // $emailData['receiver'] = Input::get('staff_name');
                        // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                        // {
                        //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                        
                        // });
                    }
                    $shift_ids = NULL;
                    if($request->get('shift_ids') != ""){
                        $shift_ids = implode(',',Input::get('shift_ids'));
                    }
                    $staffAdmin->mobile_no = Input::get('staff_mobile_number');
                    $staffAdmin->email = Input::get('staff_email');
                    $staffAdmin->save();
                    // staff table data
                    $staff->admin_id                        = $admin_id;
                    $staff->update_by                       = $loginInfo['admin_id'];
                    $staff->reference_admin_id              = $staffAdmin->admin_id;
                    $staff->staff_name                      = Input::get('staff_name');
                    $staff->staff_aadhar                    = Input::get('staff_aadhar');
                    $staff->staff_UAN                       = Input::get('staff_UAN');
                    $staff->staff_ESIN                      = Input::get('staff_ESIN');
                    $staff->staff_PAN                       = Input::get('staff_PAN');
                    $staff->designation_id                  = Input::get('designation_id');
                    $staff->teaching_status                 = Input::get('teaching_status');
                    $staff->shift_ids                       = $shift_ids;
                    $staff->staff_role_id                   = implode(',',Input::get('staff_role_id'));
                    $staff->staff_email                     = Input::get('staff_email');
                    $staff->staff_mobile_number             = Input::get('staff_mobile_number');
                    $staff->staff_dob                       = Input::get('staff_dob');
                    $staff->staff_gender                    = Input::get('staff_gender');
                    $staff->staff_father_name_husband_name  = Input::get('staff_father_name_husband_name');
                    $staff->staff_father_husband_mobile_no  = Input::get('staff_father_husband_mobile_no');
                    $staff->staff_mother_name               = Input::get('staff_mother_name');
                    $staff->staff_blood_group               = Input::get('staff_blood_group');
                    $staff->staff_marital_status            = Input::get('staff_marital_status');
                    $staff->title_id                        = Input::get('title_id');
                    $staff->caste_id                        = Input::get('caste_id');
                    $staff->nationality_id                  = Input::get('nationality_id');
                    $staff->religion_id                     = Input::get('religion_id');
                    $staff->staff_qualification             = Input::get('staff_qualification');
                    $staff->staff_specialization            = Input::get('staff_specialization');
                    $staff->staff_reference                 = Input::get('staff_reference');
                    $staff->staff_experience                = json_encode(Input::get('staff_experience'));
                    $staff->staff_temporary_address         = Input::get('staff_temporary_address');
                    $staff->staff_temporary_city            = Input::get('staff_temporary_city');
                    $staff->staff_temporary_state           = Input::get('staff_temporary_state');
                    $staff->staff_temporary_county          = Input::get('staff_temporary_county');
                    $staff->staff_temporary_pincode         = Input::get('staff_temporary_pincode');
                    $staff->staff_permanent_address         = Input::get('staff_permanent_address');
                    $staff->staff_permanent_city            = Input::get('staff_permanent_city');
                    $staff->staff_permanent_state           = Input::get('staff_permanent_state');
                    $staff->staff_permanent_county          = Input::get('staff_permanent_county');
                    $staff->staff_permanent_pincode         = Input::get('staff_permanent_pincode');
                   
                    if ($request->hasFile('staff_profile_img'))
                    {
                        if (!empty($id)){
                            $profile = check_file_exist($staff->staff_profile_img, 'staff_profile');
                            if (!empty($profile))
                            {
                                unlink($profile);
                            } 
                        }
                        $file                          = $request->file('staff_profile_img');
                        $config_upload_path            = \Config::get('custom.staff_profile');
                        $destinationPath               = public_path() . $config_upload_path['upload_path'];
                        $ext                           = substr($file->getClientOriginalName(),-4);
                        $name                          = substr($file->getClientOriginalName(),0,-4);
                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                        $file->move($destinationPath, $filename);
                        $staff->staff_profile_img       = $filename;
                    }

                    $staff->save();
                    $staff->staff_attendance_unique_id = "Staff_".$staff->staff_id;
                    $staff->save();
                    $documentKey = 0;
                    if(!empty($request->get('documents'))) {
                        foreach($request->get('documents') as $documents){ 
                            $fileData = $request->file('documents');
                            if(isset($documents['document_category_id']) && !empty($documents['document_category_id'])) {
                                if(isset($documents['staff_document_id']) ) {
                                    $staff_document_update       = StaffDocument::where('staff_document_id', $documents['staff_document_id'])->first();
                                    $staff_document_update->admin_id                 = $loginInfo['admin_id'];
                                    $staff_document_update->update_by                = $loginInfo['admin_id'];
                                    $staff_document_update->staff_id               = $staff->staff_id;
                                    $staff_document_update->document_category_id     = $documents['document_category_id'];
                                    $staff_document_update->staff_document_details = $documents['staff_document_details'];
                                    if (isset($fileData[$documentKey]['staff_document_file']))
                                    {
                                        if (!empty($staff_document_update->staff_document_file)){
                                            $staff_document_file = check_file_exist($staff_document_update->staff_document_file, 'staff_document_file');
                                            if (!empty($staff_document_file))
                                            {
                                                unlink($staff_document_file);
                                            } 
                                        }
                                        $file                          = $fileData[$documentKey]['staff_document_file'];
                                        $config_document_upload_path   = \Config::get('custom.staff_document_file');
                                        $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                        $ext                           = substr($file->getClientOriginalName(),-4);
                                        $name                          = substr($file->getClientOriginalName(),0,-4);
                                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                                        $file->move($destinationPath, $filename);
                                        $staff_document_update->staff_document_file = $filename;
                                    }
                                    
                                    $staff_document_update->save();
                                } else {
                                    $staff_document   = new StaffDocument();
                                    $staff_document->admin_id                 = $admin_id;
                                    $staff_document->update_by                = $loginInfo['admin_id'];
                                    $staff_document->staff_id               = $staff->staff_id;
                                    $staff_document->document_category_id     = $documents['document_category_id'];
                                    $staff_document->staff_document_details = $documents['staff_document_details'];
                                    if (isset($fileData[$documentKey]['staff_document_file']))
                                    {
                                        $file                          = $fileData[$documentKey]['staff_document_file'];
                                        $config_document_upload_path   = \Config::get('custom.staff_document_file');
                                        $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                        $ext                           = substr($file->getClientOriginalName(),-4);
                                        $name                          = substr($file->getClientOriginalName(),0,-4);
                                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                                        $file->move($destinationPath, $filename);
                                        $staff_document->staff_document_file = $filename;
                                    }
                                    $staff_document->save();
                                }
                            $documentKey++;
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/staff/view-staff')->withSuccess($success_msg);   
    }

     /**
     *  Get staff data according medium
     *  @Shree on 15 Sept 2018
    **/
    public function getStaffData()
    {
        $medium_type = Input::get('medium_type');
        $staff = get_all_staffs($medium_type);
        $data = view('admin-panel.staff.ajax-staff-select',compact('staff'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Destroy Staff data
     *  @Shree on 21 Oct 2018 
    **/
    public function destroy($id)
    {
        $staff_id = get_decrypted_value($id, true);
        $staff    = Staff::find($staff_id);
        
        $success_msg = $error_message =  "";
        if ($staff)
        {
            DB::beginTransaction();
            try
            {
                $staff->delete();
                $success_msg = "Staff deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/staff/view-staff')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/staff/view-staff')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Staff not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change staff's status
     *  @Shree on 21 Oct 2018 
    **/
    public function changeStatus($status,$id)
    {
        $staff_id = get_decrypted_value($id, true);
        $staff    = Staff::find($staff_id);
        if ($staff)
        {
            $staff->staff_status  = $status;
            $staff->save();
            $success_msg = "Staff status updated successfully!";
            return redirect('admin-panel/staff/view-staff')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Staff not found!";
            return redirect('admin-panel/staff/view-staff')->withErrors($error_message);
        }
    }

    /**
     *  View profile page for staff
     *  @Shree on 21 Oct 2018
    **/
    public function view_profile($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                = get_loggedin_user_data();
            $decrypted_staff_id       = get_decrypted_value($id, true);
            $staff                    = [];
            $staff                    = get_staff_signle_data($decrypted_staff_id);
            $staff                    = isset($staff[0]) ? $staff[0] : [];
            // p($staff);
            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.staff_profile'),
                'staff'               => $staff,
            );
            return view('admin-panel.staff.staff_profile')->with($data);
        } else {
            return redirect('admin-panel/staff/view-staff'); 
        }
    }

    public function importView(){
        $loginInfo          = get_loggedin_user_data();

        $info['arr_title']          = get_titles();
        $info['arr_caste']          = get_caste();
        $info['arr_religion']       = get_religion();
        $info['arr_natioanlity']    = get_nationality();
        $info['arr_shift']          = get_all_shift();
        $info['arr_staff_roles']    = get_all_staff_roles();
        $info['arr_designations']   = get_all_designations();
        
        $save_url      = url('admin-panel/staff/staff-import-save');
        $data = array(
            'login_info'    => $loginInfo,
            'page_title'    => trans('language.import_staff'),
            'info'          => $info,
            'save_url'      => $save_url,
        );
        return view('admin-panel.staff.import_staff')->with($data);
    }

    public function staff_import_save(Request $request){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $admin_id = $loginInfo['admin_id'];
        if($request->hasFile('import_file')){

            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value) {
                    $country_id = $state_id = $city_id = null;
                    // Get Country
                    $country = Country::where('country_name', "LIKE", "%{$value->country}%")->first();
                    if(isset($country['country_id'])) { $country_id = $country['country_id']; }
                    // Get State
                    $state = State::where('state_name', "LIKE", "%{$value->state}%")->first();
                    if(isset($state['state_id'])) { $state_id = $state['state_id']; }
                    // Get City
                    $city = City::where('city_name', "LIKE", "%{$value->city}%")->first();
                    if(isset($city['city_id'])) { $city_id = $city['city_id']; }

                    if($value->name != "") {
                        $arr[] = [  
                            'staff_name'          => $value->name,
                            'designation_id'         => $value->designation,
                            'staff_role_id'             => $value->roles,
                            'staff_email'             => $value->email,
                            'staff_mobile_number'             => $value->mobile_number,
                            'staff_dob'             => $value->dob,
                            'staff_gender'             => $value->gendermalefemale,
                            'staff_aadhar'             => $value->aadhar_number,
                            'staff_UAN'             => $value->universal_account_number,
                            'staff_ESIN'             => $value->employee_state_ins_number,
                            'staff_PAN'            => $value->permanent_account_number,
                            'staff_father_name_husband_name'            => $value->father_namehusband_name,
                            'staff_father_husband_mobile_no'            => $value->contact_number,
                            'staff_mother_name'            => $value->mother_name,
                            'staff_blood_group'            => $value->blood_group,
                            'caste_id'            => $value->casteid,
                            'religion_id'            => $value->religionid,
                            'nationality_id'            => $value->nationalityid,
                            'staff_marital_status'            => $value->martial_statusunmarriedmarried,
                            'title_id'            => $value->titleid,
                            'shift_ids'            => $value->shiftsid,
                            'teaching_status'            => $value->teaching_statusid,
                            'staff_specialization'            => $value->specialization,
                            'staff_qualification'            => $value->qualification,
                            'staff_reference'            => $value->reference,
                            'staff_experience'            => $value->experience,
                            'staff_permanent_address'            => $value->address,
                            'staff_permanent_county'            => $country_id,
                            'staff_permanent_state'              => $state_id,
                            'staff_permanent_city'             => $city_id,
                            'staff_permanent_pincode'             => $value->pincode,
                        ];
                    }
                }
                if(!empty($arr)){
                    DB::beginTransaction();
                    try
                    {
                        foreach ($arr as $staffD) {
                            $staff_exist = Staff::where('staff_mobile_number', $staffD['staff_mobile_number'])->get()->toArray();
                            if(COUNT($staff_exist) == 0) {
                                $staff            = New Staff;
                                $staffAdmin       = new Admin();
                                
                                $staffAdmin->admin_name = $staffD['staff_name'];
                                $staffAdmin->mobile_no = $staffD['staff_mobile_number'];
                                $staffAdmin->email = $staffD['staff_email'];
                                // $staffPassword = str_random(8);
                                $staffPassword = 'staff@123!';
                                $staffAdmin->admin_type = 2;
                                $staffAdmin->password = Hash::make($staffPassword);
                                $staffAdmin->save();
                                // $message = "";
                                // $subject = "Welcome to ".trans('language.project_title')." | Staff Panel";
                                // $emailData = [];
                                // $emailData['sender'] = trans('language.email_sender');
                                // $emailData['email'] = Input::get('staff_email');
                                // $emailData['password'] = $staffPassword;
                                // $emailData['receiver'] = Input::get('staff_name');
                                // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                                // {
                                //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                                
                                // });
                                if($staffD['staff_gender'] == "Male"){
                                    $staff_gender = 0;
                                } else if($staffD['staff_gender'] == "Female"){
                                    $staff_gender = 1;
                                }
                                if($staffD['staff_marital_status'] == "Unmarried"){
                                    $staff_marital_status = 0;
                                } else if($staffD['staff_marital_status'] == "Married"){
                                    $staff_marital_status = 1;
                                }
                                if($staffD['teaching_status'] == "Non Teaching"){
                                    $teaching_status = 0;
                                } else if($staffD['teaching_status'] == "Teaching"){
                                    $teaching_status = 1;
                                }

                                if($staffD['teaching_status'] == "Non Teaching"){
                                    $teaching_status = 0;
                                } else if($staffD['teaching_status'] == "Teaching"){
                                    $teaching_status = 1;
                                }
                               
                                $staff->admin_id                      = $admin_id;
                                $staff->update_by                     = $loginInfo['admin_id'];
                                $staff->reference_admin_id            = $staffAdmin->admin_id;
                                $staff->staff_name                    = $staffD['staff_name'];
                                $staff->staff_aadhar                  = $staffD['staff_aadhar'];
                                $staff->staff_UAN                   = $staffD['staff_UAN'];
                                $staff->staff_ESIN                 = $staffD['staff_ESIN'];
                                $staff->staff_PAN              = $staffD['staff_PAN'];
                                $staff->designation_id                  = $staffD['designation_id'];
                                $staff->staff_role_id                = $staffD['staff_role_id'];
                                $staff->staff_email                   = $staffD['staff_email'];
                                $staff->staff_mobile_number     = $staffD['staff_mobile_number'];
                                $staff->staff_dob              = $staffD['staff_dob'];
                                $staff->staff_gender                      = $staff_gender;
                                $staff->staff_father_name_husband_name                      = $staffD['staff_father_name_husband_name'];
                                $staff->staff_father_husband_mobile_no                   = $staffD['staff_father_husband_mobile_no'];
                                $staff->staff_mother_name                = $staffD['staff_mother_name'];
                                $staff->staff_blood_group          = $staffD['staff_blood_group'];
                                $staff->staff_marital_status      = $staff_marital_status;
                                $staff->title_id     = $staffD['title_id'];
                                $staff->caste_id        = $staffD['caste_id'];
                                $staff->nationality_id       = $staffD['nationality_id'];
                                $staff->religion_id      = $staffD['religion_id'];
                                $staff->staff_qualification     = $staffD['staff_qualification'];
                                $staff->staff_specialization     = $staffD['staff_specialization'];
                                $staff->staff_reference        = $staffD['staff_reference'];
                                $staff->staff_experience       = $staffD['staff_experience'];
                                $staff->staff_permanent_address      = $staffD['staff_permanent_address'];
                                $staff->staff_permanent_city     = $staffD['staff_permanent_city'];
                                
                                $staff->staff_permanent_state       = $staffD['staff_permanent_state'];
                                $staff->staff_permanent_county        = $staffD['staff_permanent_county'];
                                $staff->staff_permanent_pincode        = $staffD['staff_permanent_pincode'];
                                $staff->shift_ids      = $staffD['shift_ids'];
                                $staff->teaching_status       = $teaching_status;
                                $staff->save();
                                $staff->staff_attendance_unique_id = "Staff_".$staff->staff_id;
                                $staff->save();
                                
                            }
                        } 
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                    DB::commit();
                    return redirect('/admin-panel/staff/import-staff')->withSuccess('Staff import  successfully');
                    
                }
            }


        }
    }
}
