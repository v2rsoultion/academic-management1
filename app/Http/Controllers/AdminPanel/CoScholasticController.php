<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\CoScholastic\CoScholastic;

use Yajra\Datatables\Datatables;

class CoScholasticController extends Controller
{
    /**
     *  View page for type of co-Scholastic
     *  @Shree on 20 July 2018
    **/
    public function index()
    {
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_co_scholastic'),
            'redirect_url'  => url('admin-panel/subject/view-co-scholastic'),
            'login_info'    => $loginInfo, 
        );
        return view('admin-panel.co-scholastic.index')->with($data);
    }
    /**
     *  Add page for type of co-Scholastic
     *  @Shree on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $scholastic = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_co_scholastic_type_id = get_decrypted_value($id, true);
            $scholastic              = CoScholastic::Find($decrypted_co_scholastic_type_id);
            if (!$scholastic)
            {
                return redirect('add-co-scholastic')->withError('Co-Scholastic type not found!');
            }
            $page_title           = trans('language.edit_co_scholastic');
            $decrypted_co_scholastic_type_id = get_encrypted_value($scholastic->co_scholastic_type_id, true);
            $save_url             = url('admin-panel/subject/save-co-scholastic/' . $decrypted_co_scholastic_type_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_co_scholastic');
            $save_url      = url('admin-panel/subject/save-co-scholastic');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'scholastic'    => $scholastic,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/subject/view-co-scholastic'),
        );
        return view('admin-panel.co-scholastic.add')->with($data);
    }
    /**
     *  Add and update type of co scholastic's data
     *  @Shree on 17 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      = get_loggedin_user_data();
        $decrypted_co_scholastic_type_id = get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $scholastic = CoScholastic::find($decrypted_co_scholastic_type_id);
            $admin_id   = $scholastic['admin_id'];
            if (!$scholastic)
            {
                return redirect('/admin-panel/subject/add-co-scholastic/')->withError('Co-Scholastic subject not found!');
            }
            $success_msg = 'Co-Scholastic subject updated successfully!';
        }
        else
        {
            $scholastic     = New CoScholastic;
            $success_msg = 'Co-Scholastic subject saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'co_scholastic_type_name'   => 'required|unique:co_scholastic_types,co_scholastic_type_name,' . $decrypted_co_scholastic_type_id . ',co_scholastic_type_id'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $scholastic->admin_id                           = $admin_id;
                $scholastic->update_by                          = $loginInfo['admin_id'];
                $scholastic->co_scholastic_type_name            = Input::get('co_scholastic_type_name');
                $scholastic->co_scholastic_type_description     = Input::get('co_scholastic_type_description');
                $scholastic->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/subject/view-co-scholastic')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 20 July 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $scholastic = CoScholastic::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('co_scholastic_type_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('co_scholastic_type_name', 'ASC')->get();

        foreach ($scholastic as $key => $scholasticData)
        {
            $scholastic[$key]['co_scholastic_type_description'] = substr($scholasticData['co_scholastic_type_description'],0,250)." ..."; 
        }
        return Datatables::of($scholastic)
                
            ->addColumn('action', function ($scholastic)
            {
                $encrypted_co_scholastic_type_id = get_encrypted_value($scholastic->co_scholastic_type_id, true);
                if($scholastic->co_scholastic_type_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                    <div class="pull-left"><a href="'.url('admin-panel/subject/co-scholastic-status/'.$status.'/' . $encrypted_co_scholastic_type_id .' ').'">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/subject/add-co-scholastic/' . $encrypted_co_scholastic_type_id  .' ').'"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/subject/delete-co-scholastic/' . $encrypted_co_scholastic_type_id  .' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Destroy type of co scholastic's data
     *  @Shree on 20 July 2018
    **/
    public function destroy($id)
    {
        $co_scholastic_type_id = get_decrypted_value($id, true);
        $scholastic    = CoScholastic::find($co_scholastic_type_id);
        
        $success_msg = $error_message =  "";
        if ($scholastic)
        {
            DB::beginTransaction();
            try
            {
                $scholastic->delete();
                $success_msg = "Co scholastic subject deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/subject/view-co-scholastic')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/subject/view-co-scholastic')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Co scholastic subject not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change Type of co scholastic's status
     *  @Shree on 20 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $co_scholastic_type_id = get_decrypted_value($id, true);
        $scholastic    = CoScholastic::find($co_scholastic_type_id);
        if ($scholastic)
        {
            $scholastic->co_scholastic_type_status  = $status;
            $scholastic->save();
            $success_msg = "Co scholastic subject status update successfully!";
            return redirect('admin-panel/subject/view-co-scholastic')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Co scholastic subject not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
