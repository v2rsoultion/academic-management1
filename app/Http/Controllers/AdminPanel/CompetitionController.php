<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Competition\Competition; // Model 
use App\Model\Competition\CompetitionMapping; // Model 
use App\Model\Student\StudentAcademic;
use App\Model\Classes\Classes; // Model
use App\Model\School\School;
use Yajra\Datatables\Datatables;
use App\Model\Student\Student;

class CompetitionController extends Controller
{
    /**
     *  View page for Competition
     *  @Pratyush on 21 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $competition = [];
        $arr_medium                 = get_all_mediums();
        $competition['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.manage_competitions'),
            'redirect_url'  => url('admin-panel/competition/view-competitions'),
            'login_info'    => $loginInfo,
            'competition'   => $competition
        );
        return view('admin-panel.competition.index')->with($data);
    }

    /**
     *  Add page for Competition
     *  @Pratyush on 21 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $competition	= [];
        $loginInfo 		= get_loggedin_user_data();
        $arr_class      = get_all_classes_mediums();
        if (!empty($id))
        {
            $decrypted_competition_id 	= get_decrypted_value($id, true);
            $competition      			= Competition::Find($decrypted_competition_id);
            if (!$competition)
            {
                return redirect('admin-panel/competition/add-competition')->withError('Competition not found!');
            }
            
            $page_title             	= trans('language.edit_competitions');
            $encrypted_competition_id   		= get_encrypted_value($competition->competition_id, true);
            $save_url               	= url('admin-panel/competition/save/' . $encrypted_competition_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_competitions');
            $save_url      = url('admin-panel/competition/save');
            $submit_button = 'Save';
        }
        
        $competition['arr_class']  = $arr_class;
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'competition' 		=> $competition,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/competition/view-competitions'),
        );
        return view('admin-panel.competition.add')->with($data);
    }

    /**
     *  Add and update Competition's data
     *  @Pratyush on 21 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_competition_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $competition = Competition::find($decrypted_competition_id);
            if (!$competition)
            {
                return redirect('/admin-panel/competition/add-competition/')->withError('Competition not found!');
            }
            $success_msg = 'Competition updated successfully!';
            $admin_id = $competition['admin_id'];
        }
        else
        {
            $competition   	= New Competition;
            $success_msg 	= 'Competition saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'competition_name'   => 'required|unique:competitions,competition_name,' . $decrypted_competition_id . ',competition_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
         		$class_ids = null;	
         		if(Input::get('competition_level') == 1){
                    if(Input::get('competition_class_ids') !="") {
                        $class_ids = implode(',', Input::get('competition_class_ids'));
                    }
         		}
                $competition->admin_id       		      = $admin_id;
                $competition->update_by      			  = $loginInfo['admin_id'];
                $competition->competition_name 	          = Input::get('competition_name');
                $competition->competition_date 		      = date('Y-m-d',strtotime(Input::get('competition_date')));
                $competition->competition_issue_participation_certificate = Input::get('competition_issue_participation_certificate');
                $competition->competition_level 	      = Input::get('competition_level');
                $competition->competition_class_ids 	  = $class_ids;
                $competition->competition_description     = Input::get('competition_description');
                $competition->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
    }

    /**
     *  Get Competition's Data for view page(Datatables)
     *  @Pratyush on 23 July 2018.
    **/
    public function anyData(Request $request)
    {

        $loginInfo 			= get_loggedin_user_data();
        $competition		= Competition::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('competition_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->orderBy('competition_id', 'DESC')->get();
        return Datatables::of($competition)
        ->addColumn('competition_name', function ($competition)
        {
            $competition_name  = substr($competition->competition_name, 0, 50).'....';
            return $competition_name;
        })
        ->addColumn('medium_type', function ($competition)
        {
            $arr_medium  = get_all_mediums();
            return $arr_medium[$competition->medium_type];
        })
        ->addColumn('competition_date', function ($competition)
        {
            return date('d M Y',strtotime($competition->competition_date));
            
        })
        ->addColumn('no_of_winner', function ($competition)
        {
            // CompetitionMapping::where('competition_id',$competition->competition_id)->where('position_rank','!=','')->count();
            $demo =  CompetitionMapping::where('competition_id',$competition->competition_id)->where('position_rank','!=','')->get()->count();
            return $demo;
            
        })
        ->addColumn('competition_level', function ($competition)
        {
            if($competition->competition_level == 0){
                $temp_com_level = 'School';
            }else{
                $temp_com_level = 'Class';
            }
            return $temp_com_level;
            
        })
        ->addColumn('action', function ($competition)
        {
            $encrypted_competition_id = get_encrypted_value($competition->competition_id, true);
            if($competition->competition_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            $certificate = '';
            if($competition->competition_issue_participation_certificate == 1){
                $certificate = '
                <li>
                    <a title="Issue Certificates" href="issue-certificate/' . $encrypted_competition_id . '" "">Issue Certificates</a>
                </li>
                ';
            }
            return '
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Select Winner" href="map-students/' . $encrypted_competition_id . '" "">Map Students</a>
                    </li>
                    <li>
                        <a title="Select Winner" href="select-winner/' . $encrypted_competition_id . '" "">Select Winner</a>
                    </li>
                    '.$certificate.'
                </ul>
            </div>
            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="competition-status/'.$status.'/' . $encrypted_competition_id . '">'.$statusVal.'</a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-competition/' . $encrypted_competition_id . '"><i class="zmdi zmdi-edit"></i></a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-competition/' . $encrypted_competition_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Competition's data
     *  @Pratyush on 23 July 2018.
    **/
    public function destroy($id)
    {
        $competition_id = get_decrypted_value($id, true);
        $competition  	= Competition::find($competition_id);
        
        $success_msg = $error_message =  "";
        if ($competition)
        {
            DB::beginTransaction();
            try
            {
                $competition->delete();
                $success_msg = "Competition deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/competition/view-competitions')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Competition not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Competition's status
     *  @Pratyush on 23 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $competition_id = get_decrypted_value($id, true);
        $competition  	= Competition::find($competition_id);
        if($competition)
        {
            $competition->competition_status  = $status;
            $competition->save();
            $success_msg = "Competition status updated!";
            return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Competition not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Select winner List
     *  @Pratyush on 30 July 2018
    **/
    public function map_students($id)
    {
        
        $loginInfo          = get_loggedin_user_data();
        $competition_id     = get_decrypted_value($id, true);
        $competition  	    = Competition::find($competition_id);
        // For school level
        if($competition->competition_level == 0){
            $arr_class                  = get_all_classes_mediums();
        } else if($competition->competition_level == 1){
            // For class level
            $arr_class                  = get_all_classes_mediums($competition->competition_class_ids);
        }
        
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $competition_id             = get_decrypted_value($id, true);
        $data = array(
            'page_title'        => trans('language.map_students'),
            'redirect_url'      => url('admin-panel/competition/map-students/'.$id),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'competition_id'    => $competition_id,
        );
        return view('admin-panel.competition.map_students')->with($data);
    }

    /**
     *  View page for Select winner List
     *  @Pratyush on 30 July 2018
    **/
    public function select_winner($id)
    {
        $loginInfo          = get_loggedin_user_data();
        $competition_id     = get_decrypted_value($id, true);
        $competition  	    = Competition::find($competition_id);
        // For school level
        if($competition->competition_level == 0){
            $arr_class                  = get_all_classes_mediums();
        } else if($competition->competition_level == 1){
            // For class level
            $arr_class                  = get_all_classes_mediums($competition->competition_class_ids);
        }
        
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $competition_id             = get_decrypted_value($id, true);
        $data = array(
            'page_title'        => trans('language.select_winner'),
            'redirect_url'      => url('admin-panel/competition/select-winner/'.$id),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'competition_id'    => $competition_id,
            'competition'       => $competition,
        );
        return view('admin-panel.competition.select_winner')->with($data);
    }

    /**
     *  View page for Select winner List
     *  @Pratyush on 30 July 2018
    **/
    public function issue_certificates($id)
    {
        $loginInfo          = get_loggedin_user_data();
        $competition_id     = get_decrypted_value($id, true);
        $competition  	    = Competition::find($competition_id);
        // For school level
        if($competition->competition_level == 0){
            $arr_class                  = get_all_classes_mediums();
        } else if($competition->competition_level == 1){
            // For class level
            $arr_class                  = get_all_classes_mediums($competition->competition_class_ids);
        }
        
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $competition_id             = get_decrypted_value($id, true);
        $data = array(
            'page_title'        => trans('language.issue_certificates'),
            'redirect_url'      => url('admin-panel/competition/issue-certificate/'.$id),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'competition_id'    => $id,
        );
        return view('admin-panel.competition.issue_certificate')->with($data);
    }

    public function save_map_students(Request $request){        
      
        $loginInfo          = get_loggedin_user_data();
        // $competition_id     = get_decrypted_value(Input::get('competition_id'),true);
        $competition_id     = Input::get('competition_id');
        $competition      	= Competition::Find($competition_id);
        if($request->get('map') != ""){
            try
	            {
                    foreach($request->get('map') as $map){
                        
                        if(empty($map['student_competition_map_id']) && $map['student_id'] != ""){
                            // Add case
                            $mapping = new CompetitionMapping;		
                            $mapping->admin_id       = $loginInfo['admin_id'];
                            $mapping->update_by      = $loginInfo['admin_id'];
                            $mapping->competition_id = $competition_id;
                            $mapping->student_id 	 = $map['student_id'];
                            $mapping->save();
                         
                            $send_notification  = $this->send_push_notification($competition_id,$competition->competition_name,'competition_participate',$map['student_id'],'Competition Participation');
                            $success_msg = 'Student(s) Marked Winners Successfully.';

                        } else if(empty($map['student_id']) && !empty($map['student_competition_map_id']) && !empty($map['exist_student_id'])){
                            // Remove case
                            $map_delete = CompetitionMapping::where('student_competition_map_id', '=', $map['student_competition_map_id'])->where('student_id', '=', $map['exist_student_id'])->first();
                            $map_delete->delete();
                        } else if(!empty($map['student_competition_map_id']) && ($map['position_rank'] != '' || $map['position_rank'] == '')){
                            
                            if($map['position_rank'] != '' ) {
                                
                            $mapping = CompetitionMapping::FIND($map['student_competition_map_id']);
                            
                            $mapping->position_rank 	 = $map['position_rank'];
                            $mapping->save();
                            $success_msg = 'Position/Rank successfully set.';
                            $send_notification  = $this->send_push_notification($competition_id,$competition->competition_name,'competition_winner',$map['student_id'],'Competition Winner');
                            }
                        }
                    }
                }
	            catch (\Exception $e)
	            {   
	                //failed logic here
	                DB::rollback();
                    $error_message = $e->getMessage();
                    return $error_message;
                }
	            DB::commit();
				return "success";
        }
        
    }

    public function save_certificate_status(Request $request){
        $loginInfo          = get_loggedin_user_data();
        // $competition_id = get_decrypted_value(Input::get('competition_id'),true);
        $competition_id     = Input::get('competition_id');
        if($request->get('map') != ""){
            try
	            {
                    foreach($request->get('map') as $map){
                        if(!empty($map['student_competition_map_id'])){
                            // Add case
                            $mapping = CompetitionMapping::FIND($map['student_competition_map_id']);
                            if(isset($map['exist_student_id']) && $map['exist_student_id'] != '' && $map['student_id'] == ''){
                                $mapping->issue_certificate      = 0;
                            } else {
                                $mapping->issue_certificate      = 1;
                            }
                            
                            $mapping->save();
                            $success_msg = 'Records successfully stored.';

                        } 
                    }
                }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
                    $error_message = $e->getMessage();
                    return $error_message;
	            }
	            DB::commit();
				return "success";
        }
        
    }
    

    /**
     *  Get Student List Data for Select winner page(Datatables)
     *  @Pratyush on 30 July 2018
    **/
    public function getStudentData(Request $request)
    {
        $arr_students  =  $student  = [];
        if($request->get('class_id') != NULL && $request->get('section_id') != NULL) {
            $arr_students   = get_students_for_competition($request,$request->get('type'));
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student,$request)
            ->addColumn('checkbox', function ($student) use ($request)
            {
                $check = '';
                $exist = 0;
                if(!empty($student->student_competition_map_id)){
                    if($request->get('type') == 1){
                        if(!empty($student->position_rank)){
                            $check = 'checked';
                        }
                    } else {
                        $check = 'checked';
                    }
                }
                $exist_student = '';
                if($request->get('type') == 0){
                    $exist_student = '<input type="hidden" name="map['.$student->student_id.'][exist_student_id]" value="'.$student->student_id.'" >';
                } else {
                    $exist_student = '<input type="hidden" name="map['.$student->student_id.'][exist_winner_student_id]" value="'.$student->student_id.'" >';
                }
                return '
                <input type="hidden" name="map['.$student->student_id.'][student_competition_map_id]" value="'.$student->student_competition_map_id.'" >
                '.$exist_student.'
                <div class="checkbox" id="customid">
                    <input type="checkbox"  id="student'.$student->student_id.'" name="map['.$student->student_id.'][student_id]" class="check" value="'.$student->student_id.'" '.$check.' onClick="uncheck()">
                    <label  class="from_one1 students" student-id="'.$student->student_id.'"  style="margin-bottom: 4px !important;"  for="student'.$student->student_id.'"></label>
                </div>
                ';
                
            })
            ->addColumn('checkbox_certificate', function ($student) use ($request)
            {
                $check = '';
                $exist = 0;
                
                $exist_student = '';
                if(!empty($student->student_competition_map_id)){
                    if(!empty($student->issue_certificate == 1)){
                        $check = 'checked';
                        $exist_student = '<input type="hidden" name="map['.$student->student_id.'][exist_student_id]" value="'.$student->student_id.'" >';
                    }
                }
               
                return '
                <input type="hidden" name="map['.$student->student_id.'][student_competition_map_id]" value="'.$student->student_competition_map_id.'" >
                '.$exist_student.'
                <div class="checkbox" id="customid">
                    <input type="checkbox"  id="student'.$student->student_id.'" name="map['.$student->student_id.'][student_id]" class="check" value="'.$student->student_id.'" '.$check.' onClick="uncheck()">
                    <label  class="from_one1 students" student-id="'.$student->student_id.'"  style="margin-bottom: 4px !important;"  for="student'.$student->student_id.'"></label>
                </div>
                ';
                
            })
            ->addColumn('class_section', function ($student)
                {
                     return $student->class_section;
                    
                })
            ->addColumn('winner_rank', function ($student)
            {
                $disabled = "readonly";
                if($student->position_rank != "" && $student->position_rank != NULL){
                    $disabled = "";
                }
                return '
                <input type="text" class="form-control" style="width: 60%" id="position_rank'.$student->student_id.'" '.$disabled.' name="map['.$student->student_id.'][position_rank]" required value="'.$student->position_rank.'" >';
                
            })
            ->addColumn('profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                // $profile = "<img src=".$student->profile." height='30' />";
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('certificate', function ($student)
            {
                // if($student->issue_certificate == 1){
                //     return "----";
                // }
                $encrypted_competition_map_id   = get_encrypted_value($student->student_competition_map_id, true);
                return '<div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li><a  href="'.url('/admin-panel/competition/certificate/'.$encrypted_competition_map_id.' ').'" target="_blank" >Certificate</a></li>
                            </ul>
                         </div>';
               
                
            })
            ->rawColumns(['certificate'=>'certificate','checkbox_certificate'=>'checkbox_certificate','action' => 'action', 'profile'=>'profile', 'checkbox'=> 'checkbox', 'winner_rank'=> 'winner_rank'])->addIndexColumn()->make(true);
    }

    public function certificate($student_competition_map_id){
        $loginInfo   = get_loggedin_user_data();
        $decrypted_competition_map_id   = get_decrypted_value($student_competition_map_id, true);
        $competitionData = CompetitionMapping::where('student_competition_map_id','=',$decrypted_competition_map_id)->with('getStudent')->with('getStudent.getParent')->with('getCompetition')->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->get()->toArray();

        $school              = School::first()->toArray();
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $competitionData              = isset($competitionData[0]) ? $competitionData[0] : [];
        $certificate_info = array(
            'student_name' => $competitionData['get_student']['student_name'],
            'student_father_name' => $competitionData['get_student']['get_parent']['student_father_name'],
            'class' => $competitionData['get_student_academic']['get_current_class']['class_name'],
            'position_rank' => $competitionData['position_rank'],
            'competition_name' => $competitionData['get_competition']['competition_name'],
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'logo' => $school['logo'],
            'issue_date' => date("d-m-Y"),
        );
        if($competitionData['position_rank'] != ""){
            $page_title = "Winner Certificate";
        } else {
            $page_title = "Participation Certificate";
        }
        
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info
        );
        if($competitionData['position_rank'] != ""){
            return view('admin-panel.certificate-templates.winner_template1')->with($data);
        } else {
            return view('admin-panel.certificate-templates.participation_template1')->with($data);
        }
        
    }

    public function send_push_notification($module_id,$module_name,$notification_type,$student_id,$title){
        $loginInfo  = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $session = get_current_session();

        $device_ids = [];
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            $query->where('student_id', $student_id);
            $query->where('student_status', 1);
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('student_id','student_name','students.student_parent_id','students.reference_admin_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];
        
        if($student_info['get_admin_info']['fcm_device_id'] != "" && $student_info['get_admin_info']['notification_status'] == 1 ){
            $device_ids[] = $student_info['get_admin_info']['fcm_device_id'];
            $student_admin_id = $student_info['get_admin_info']['admin_id'];
        }
        if($student_info['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student_info['get_admin_info']['notification_status'] == 1){
            $device_ids[] = $student_info['get_parent']['get_parent_admin_info']['fcm_device_id'];
            $parent_admin_id = $student_info['get_parent']['get_parent_admin_info']['admin_id'];
        }
        if($notification_type == 'competition_participate'){
            $message = $student_info['student_name']." you have got a participation certificate of ".$module_name;
        } else if($notification_type == 'competition_participate'){
            $message = "Congratulations ".$student_info['student_name']." you are selected as winner in ".$module_name.", check your winning certificate.";
        }
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => $student_admin_id,
            'parent_admin_id' => $parent_admin_id,
            'staff_admin_id' => Null,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        
        $save_notification = save_notification($info);
       
        $send_notification = pushnotification($module_id,$notification_type,'',$student_info['student_parent_id'],$student_info['student_id'],$device_ids,$message,$title);
       
        
    }

    
}