<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\QuestionPaper\QuestionPaper; // Model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;
use URL;

class QuestionPaperController extends Controller
{
        /**
     *  View page for Question Paper
     *  @Ashish on 21 Aug 2018
    **/
    public function index()
    {
        $arr_class                  = get_all_classes_mediums();
        $arr_subject                = get_all_subject();
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_subject']    = add_blank_option($arr_subject, 'Select Subject');
        $loginInfo                  = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_question_paper'),
            'redirect_url'  => url('admin-panel/question-paper/view-question-paper'),
            'login_info'    => $loginInfo,
            'listData'      => $listData

        );
        return view('admin-panel.question-paper.index')->with($data);
    }

    /**
     *  Add page for Question Paper
     *  @Ashish on 21 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $question_paper = [];
        $class_id       = null;
        $doc_path       = '';
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_question_paper_id 	= get_decrypted_value($id, true);
            $question_paper      			= QuestionPaper::Find($decrypted_question_paper_id);
            if (!$question_paper)
            {
                return redirect('admin-panel/question-paper/add-question-paper')->withError('Question Paper not found!');
            }
            $page_title             	    = trans('language.edit_question_paper');
            $encrypted_question_paper_id    = get_encrypted_value($question_paper->question_paper_id, true);
            $save_url               	    = url('admin-panel/question-paper/save/' . $encrypted_question_paper_id);
            $submit_button          	    = 'Update';
            $arr_class                      = get_all_classes_mediums();
            $arr_section                    = get_class_section($question_paper->class_id);
            $arr_subject_mapping            = get_all_subject_mapping($question_paper->class_id);
            $config_document_upload_path    = \Config::get('custom.question_paper_document');
            $doc_path                       = $config_document_upload_path['display_path'].$question_paper->question_paper_file;


        }
        else
        {
            $page_title    = trans('language.add_question_paper');
            $save_url      = url('admin-panel/question-paper/save');
            $submit_button = 'Save';
            $arr_class                   = get_all_classes_mediums();
            $arr_section                 = [];
            $arr_subject_mapping         = [];
        }

        $arr_exam_type                           = get_all_exam_type();
        $question_paper['arr_class']             = add_blank_option($arr_class, 'Select class');
        $question_paper['arr_section']           = add_blank_option($arr_section, 'Select section');
        $question_paper['arr_subject_mapping']   = add_blank_option($arr_subject_mapping, 'Select subject');
        $question_paper['arr_exam_type']         = add_blank_option($arr_exam_type, 'Select Exam Type');
        
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'question_paper' 	=> $question_paper,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/question-paper/view-question-paper'),
            'doc_path'          => $doc_path,
        );
        return view('admin-panel.question-paper.add')->with($data);
    }

    /**
     *  Get sections data according class
     *  @Ashish on 21 Aug 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.question-paper.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get sections data according class
     *  @Ashish on 22 Aug 2018
    **/
    public function getSubjectData()
    {
        $class_id = Input::get('class_id');
        $subject = get_all_subject_mapping($class_id);
        $data = view('admin-panel.question-paper.ajax-select-subject',compact('subject'))->render();
        return response()->json(['options'=>$data]);
    }


    /**
     *  Add and update Question Paper data
     *  @Ashish on 21 Aug 2018
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			            = get_loggedin_user_data();
        $decrypted_question_paper_id			= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $question_paper = QuestionPaper::find($decrypted_question_paper_id);
            $admin_id = $question_paper['admin_id'];
            if (!$question_paper)
            {
                return redirect('/admin-panel/question-paper/add-question-paper/')->withError('question paper not found!');
            }
            $success_msg = 'question paper updated successfully!';
        }
        else
        {
            $question_paper = New QuestionPaper;
            $success_msg 	= 'question paper saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'name'   => 'required|unique:question_papers,question_paper_name,' . $decrypted_question_paper_id . ',question_paper_id',
                'class_id'           => 'required',
                'qp_section_id'         => 'required',
                'qp_subject_id'         => 'required',
                'exam_id'       => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $staff_id = null;
                if($loginInfo['staff_id']  != ""){
                    $staff_id = $loginInfo['staff_id'];
                }
                $question_paper->admin_id                = $loginInfo['admin_id'];
                $question_paper->update_by               = $loginInfo['admin_id'];
                $question_paper->staff_id                = $staff_id;
                $question_paper->question_paper_name     = Input::get('name');
                $question_paper->exam_id 	             = Input::get('exam_id');
                $question_paper->class_id 	             = Input::get('class_id');
                $question_paper->section_id 	         = Input::get('qp_section_id');
                $question_paper->subject_id 	         = Input::get('qp_subject_id');
                if ($request->hasFile('documents'))
		        {

                    if (!empty($question_paper->question_paper_file))
                        {
                            $profile = check_file_exist($question_paper->question_paper_file, 'question_paper_document');
                            if (!empty($profile))
                            {
                                if(file_exists($profile)){
                                    @unlink($profile);
                                }
                            }
                        }
                        
		            $file                          = $request->File('documents');
		            $config_document_upload_path   = \Config::get('custom.question_paper_document');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
		            $name                          = substr($file->getClientOriginalName(),0,-4);
		            $filename                      = $name.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
		            $question_paper->question_paper_file = $filename;

		        }
                $question_paper->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/question-paper/view-question-paper')->withSuccess($success_msg);
    }

    /**
     *  Get Question Paper Data for view page(Datatables)
     *  @Ashish on 21 Aug 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo	         = get_loggedin_user_data();
        $class_id            = null;
        $subject_id          = null;
        $class_id            = $request->get('class_id');
        $subject_id          = $request->get('subject_id');
        $question_paper      = QuestionPaper::where(function($query) use ($class_id,$subject_id) 
            { 
                if (!empty($class_id)) 
                {
                    $query->where('class_id', $class_id); 
                }
                if (!empty($subject_id)) 
                {
                    $query->where('subject_id', $subject_id); 
                }
            })
        ->with('getClass')->with('getSection')->with('getSubjects')->with('getExamType')->orderBy('question_paper_id','DESC')->get();
                    
        return Datatables::of($question_paper)
                ->addColumn('profile', function ($question_paper)
                {   
                    $image = '';
                    if($question_paper->question_paper_file == ''){
                        $image = 'No link available';
                    }else{
                        $config_document_upload_path   = \Config::get('custom.question_paper_document');
                        $doc_path = $config_document_upload_path['display_path'].$question_paper->question_paper_file;
                        $image = '<span class="profile_detail_span_6"><a href="'.url($doc_path).'" target="_blank">View Attachment</a></span>';
                    }
                    return $image;
                    
                })
                ->addColumn('class_subject', function ($question_paper)
                {
                     return $question_paper['getSubjects']['subject_name'];
                    
                })
                ->addColumn('exam_type', function ($question_paper)
                {
                     return $question_paper['getExamType']['exam_name'];
                    
                })
                ->addColumn('class_section', function ($question_paper)
                {
                     return $question_paper['getClass']['class_name'].' - '.$question_paper['getSection']['section_name'];
                    
                })
        		->addColumn('action', function ($question_paper)
                {
                    $encrypted_question_paper_id = get_encrypted_value($question_paper->question_paper_id, true);
                    if($question_paper->question_paper_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="pull-left"><a href="question-paper-status/'.$status.'/' . $encrypted_question_paper_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-question-paper/' . $encrypted_question_paper_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-question-paper/' . $encrypted_question_paper_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['profile' => 'profile','exam_type' => 'exam_type','class_subject' => 'class_subject','class_section' => 'class_section','action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy Question Paper data
     *  @Ashish on 21 Aug 2018
    **/
    public function destroy($id)
    {
        $question_paper_id 		= get_decrypted_value($id, true);
        $question_paper 		= QuestionPaper::find($question_paper_id);

        $success_msg = $error_message =  "";
        if ($question_paper)
        {
            DB::beginTransaction();
            try
            {
                $question_paper->delete();
                $success_msg = "Question paper deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/question-paper/view-question-paper')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/question-paper/view-question-paper')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Question paper not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Question Paper status
     *  @Ashish on 31 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $question_paper_id 		= get_decrypted_value($id, true);
        $question_paper 		= QuestionPaper::find($question_paper_id);
        if ($question_paper)
        {
            $question_paper->question_paper_status  = $status;
            $question_paper->save();
            $success_msg = "Question paper status update successfully!";
            return redirect('admin-panel/question-paper/view-question-paper')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "question paper not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
