<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Staff\Staff; // Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model
use App\Model\Remarks\Remarks; // Model
use App\Model\HomeworkGroup\HomeworkGroup; // Model
use App\Model\HomeworkGroup\HomeworkConv; // Model
use App\Model\Student\Student; // Model
use App\Model\Section\Section; // Model
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use URL;

class MySubjectsController extends Controller
{
    public function get_staff_map_info($class_id,$section_id){
        $loginInfo  = get_loggedin_user_data();
        $staff_id = $loginInfo['staff_id'];
        $map_info = SubjectTeacherMapping::where(function($query) use ($staff_id,$class_id,$section_id) 
        {
            if (!empty($class_id) && $class_id != null){
                $query->where('class_id', '=',$class_id);
            }
            $query->where('staff_ids', '=',$staff_id);
            if (!empty($section_id) && $section_id != null){
                $query->where('section_id', '=',$section_id);
            }
            
        })->with('getStaff')->with('getClass')->with('getSection')->with('getSubject')->get();
        return $map_info;
    }

    public function subjects(){
        $subjects = $arr_section =  [];
        $loginInfo  = get_loggedin_user_data();
        $arr_class = get_all_classes_mediums();
        $subjects['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $subjects['arr_section'] = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.menu_my_subject'),
            'redirect_url'  => url('admin-panel/my-subjects'),
            'login_info'    => $loginInfo,
            'subjects'      => $subjects,
        );
        return view('admin-panel.my-subjects.index')->with($data);
    }
    /**
     *  Get subject list according to staff
     *  @Shree on 2 Nov 2018
    **/
    public function get_subjects(Request $request)
    {
        $subjects     = [];
        $loginInfo  = get_loggedin_user_data();
        $staff_id = $loginInfo['staff_id'];
        $subjects = get_staff_subjects($request,$staff_id);
        $homework_group = NULL;
        
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        foreach ($subjects as $key => $arr_subject)
        {
            $homework_group = NULL;
            if(isset($arr_subject['class_id']) && isset($arr_subject['section_id'])){
                $homework_info = get_homework_group($arr_subject['class_id'],$arr_subject['section_id']);
                if(!empty($homework_info)){
                    $homework_group = $homework_info->homework_group_id;
                }
            }
            $arr_subject['homework_group'] = $homework_group; 
            $subjects[$key] = (object) $arr_subject;
        }
        return Datatables::of($subjects,$homework_group)
            
            ->addColumn('action', function ($subjects) 
            {
                $encrypted_section_id = get_encrypted_value($subjects->section_id, true);
                $encrypted_subject_id = get_encrypted_value($subjects->subject_id, true);
                $encrypted_class_id = get_encrypted_value($subjects->class_id, true);
                $encrypted_group_id = get_encrypted_value($subjects->homework_group, true);
                return ' 
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="'.url('admin-panel/my-subjects/view-students/'.$encrypted_section_id.'/'.$encrypted_subject_id.' ').'">View Students</a>
                    </li>
                    <li>
                        <a href="'.url('admin-panel/my-subjects/homework-group/'.$encrypted_subject_id.'/'.$encrypted_group_id.' ').'">Homework Group</a>
                    </li>
                    <li>
                        <a href="'.url('admin-panel/student-class/view-parents/'.$encrypted_section_id.'/'.$encrypted_subject_id.'/'.$encrypted_class_id.' ').'">Parent Communication</a>
                    </li>
                 </ul>
             </div>
            ';
            })
            ->rawColumns(['action'=>'action'])->addIndexColumn()->make(true);
    }

    public function view_students($section_id,$subject_id){
        $loginInfo  = get_loggedin_user_data();
        $decrypted_section_id = get_decrypted_value($section_id, true);
        $decrypted_subject_id = get_decrypted_value($subject_id, true);
        $data = array(
            'page_title'    => trans('language.view_students'),
            'redirect_url'  => url('admin-panel/my-class/view-students/'.$section_id),
            'login_info'    => $loginInfo,
            'section_id'    => $decrypted_section_id,
            'subject_id'    => $decrypted_subject_id,
        );
        return view('admin-panel.my-subjects.view_students')->with($data);
    }

    /**
     *  Get student list
     *  @Shree on 2 Nov 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && $request->get('section_id') != null )
        {
            $arr_students = get_student_list_by_subject($request);
            $arr_excluded_students = get_excluded_subject_list($request);
        } else {
            $arr_students = [];
            $arr_excluded_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student,$arr_excluded_students)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('action', function ($student) use ($arr_excluded_students)
            {
                $status = 1;
                $action = '';
                if(in_array($student->student_id,$arr_excluded_students)){
                    $status = 0;
                }
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                $encrypted_subject_id = get_encrypted_value($student->subject_id, true);
                if($status == 1){
                    $action = '
                    <li>
                        <a href="'.url('admin-panel/my-subjects/add-remark/'.$encrypted_student_id.'/'.$encrypted_subject_id.' ').'">Add Remark</a>
                    </li>
                    <li>
                        <a href="'.url('admin-panel/my-subjects/view-remarks/'.$encrypted_student_id.'/'.$encrypted_subject_id.' ').'">View Remark</a>
                    </li>
                    ';
                } else {
                    $action = '
                    <li>
                    <a href="#">NA</a>
                    </li>';
                }
                return ' 
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                   ' . $action . '
                 </ul>
             </div>
            ';
            })
            ->rawColumns(['student_profile'=>'student_profile', 'action'=> 'action'])->addIndexColumn()->make(true);
    }
    public function add_remark($student_id = NULL, $subject_id = NULL, $id = NULL){
        
        $data = $remark = $student_info =  [];
        $decrypted_student_id = get_decrypted_value($student_id, true);
        $decrypted_subject_id = get_decrypted_value($subject_id, true);
        $loginInfo  = get_loggedin_user_data();
        $student_info = get_student_list_by_subject('',$decrypted_student_id);
        $student_info = isset($student_info[0]) ? $student_info[0] : [];
        $encrypted_section_id = get_encrypted_value($student_info['section_id'], true);
        if (!empty($id))
        {
            $decrypted_remark_id = get_decrypted_value($id, true);
            $remark 			 = Remarks::Find($decrypted_remark_id);
            if (!$remark)
            {
                return redirect()->back()->withErrors('Remark not found!');
            }
            $page_title          = trans('language.edit_remark');
            $encrypted_remark_id = get_encrypted_value($remark->remark_id, true);
            $save_url            = url('admin-panel/my-subjects/save-remark/'. $encrypted_remark_id);
            $submit_button       = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_remark');
            $save_url      = url('admin-panel/my-subjects/save-remark');
            $submit_button = 'Save';
        }
        $subjectName = get_subject_name($decrypted_subject_id);
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'remark' 	        => $remark,
            'login_info'    	=> $loginInfo,
            'encrypted_section_id'  => $encrypted_section_id,
            'encrypted_student_id'  => $student_id,
            'encrypted_subject_id'  => $subject_id,
            'student_id'            => $decrypted_student_id,
            'class_id'              => $student_info['class_id'],
            'section_id'            => $student_info['section_id'],
            'subject_id'            => $decrypted_subject_id,
            'subject_name'          => $subjectName,
            'student_info'          => $student_info,
            'redirect_url'  	    => url('admin-panel/my-subjects/view-remarks/'.$student_id.'/'.$subject_id),
        );
        
        return view('admin-panel.remarks.subject_add')->with($data);
    }
    /**
     *  Add and update Remark's data
     *  @Shree on 6 Nov 2018.
    **/
    public function save_remark(Request $request, $id = NULL)
    {
        $loginInfo      		= get_loggedin_user_data();
        $decrypted_remark_id	= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        $session                = get_current_session();
        $encrypted_subject_id	= get_encrypted_value($request->get('subject_id'), true);
        $encrypted_student_id	= get_encrypted_value($request->get('student_id'), true);
        if (!empty($id))
        {
            $remark = Remarks::find($decrypted_remark_id);
            $admin_id = $remark['admin_id'];
            if (!$remark)
            {
                redirect()->back()->withInput()->withErrors('Remark not found!');
            }
            $success_msg = 'Remark updated successfully!';
        }
        else
        {
            $remark     	= New Remarks;
            $success_msg 	= 'Remark saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'remark_text'   => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $remark->admin_id       = $admin_id;
                $remark->update_by      = $loginInfo['admin_id'];
                $remark->session_id 	= $session['session_id'];
                $remark->class_id 	    = Input::get('class_id');
                $remark->section_id 	= Input::get('section_id');
                $remark->staff_id 	    = $loginInfo['staff_id'];
                $remark->student_id 	= Input::get('student_id');
                $remark->subject_id 	= Input::get('subject_id');
                $remark->remark_text 	= Input::get('remark_text');
                $remark->remark_date 	= date('Y-m-d');
                $remark->save();
                $subject_name = get_subject_name($request->get('subject_id'));
                $send_notification  = $this->send_push_notification($remark->remark_id,$loginInfo['staff_id'],$request->get('student_id'),$subject_name,"","",'student_dairy_remark_by_staff');
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/my-subjects/view-remarks/'.$encrypted_student_id.'/'.$encrypted_subject_id)->withSuccess($success_msg);
    }

    /**
     *  View page for remarks
     *  @Shree on 6 Nov 2018
    **/
    public function view_remarks(Request $request,$student_id = NULL, $subject_id = NULL)
    {
        $remark = [];
        $loginInfo              = get_loggedin_user_data();
        $decrypted_student_id = get_decrypted_value($student_id, true);
        $decrypted_subject_id = get_decrypted_value($subject_id, true);
        $student_info = get_student_list_by_subject('',$decrypted_student_id);
        $student_info = isset($student_info[0]) ? $student_info[0] : [];
        $encrypted_section_id = get_encrypted_value($student_info['section_id'], true);
        $subjectName = get_subject_name($decrypted_subject_id);
        $data = array(
            'page_title'   => trans('language.view_remarks'),
            'redirect_url' => url('admin-panel/my-subjects/view-remarks/'.$student_id.'/'.$subject_id),
            'login_info'    => $loginInfo,
            'remark'       => $remark,
            'encrypted_section_id'  => $encrypted_section_id,
            'encrypted_student_id'  => $student_id,
            'encrypted_subject_id'  => $subject_id,
            'student_id'            => $decrypted_student_id,
            'class_id'              => $student_info['class_id'],
            'section_id'            => $student_info['section_id'],
            'subject_id'            => $decrypted_subject_id,
            'subject_name'          => $subjectName,
            'student_info'          => $student_info
        );
        return view('admin-panel.remarks.subject_index')->with($data);
    }

    /**
     *  Get Remarks's Data for view page(Datatables)
     *  @Shree on 6 Nov 2018.
    **/
    public function reviewData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        
        $remark   = Remarks::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('student_id')) && $request->get('student_id') != null)
            {
                $query->where('student_id', "=", $request->get('student_id'));
            }
            if (!empty($request) && !empty($request->has('subject_id')) && $request->get('subject_id') != null)
            {
                $query->where('subject_id', "=", $request->get('subject_id'));
            }
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
        })->with('getStudent')->with('getSubject')->orderBy('remark_id', 'ASC')->get();
        
        return Datatables::of($remark)
        ->addColumn('subject_name', function ($remark)
            {
                $subjectName = $remark->getSubject['subject_name'].' - '.$remark->getSubject['subject_code'];
                return $subjectName;
                
            })
            ->addColumn('student_name', function ($remark)
            {
                $studentName = $remark->getStudent['student_name'];
                return $studentName;
                
            })
            ->addColumn('remark_date', function ($remark)
            {
                $remark_date = date("d F Y", strtotime($remark->remark_date));
                return $remark_date;
                
            })
        ->addColumn('action', function ($remark)
        {
            $encrypted_remark_id = get_encrypted_value($remark->remark_id, true);
            $encrypted_student_id = get_encrypted_value($remark->student_id, true);
            $encrypted_subject_id = get_encrypted_value($remark->subject_id, true);
            return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit">
                    <a href="'.url('admin-panel/my-subjects/add-remark/'.$encrypted_student_id.'/'.$encrypted_student_id.'/'.$encrypted_remark_id.' ').'"><i class="zmdi zmdi-edit"></i></a>
                </div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete">
                <a href="'.url('admin-panel/my-subjects/delete-remark/'.$encrypted_remark_id.' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a>
                </div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy remark's data
     *  @Shree on 6 Nov 2018.
    **/
    public function destroy_remark($id)
    {
        $remark_id  = get_decrypted_value($id, true);
        $remark     = Remarks::find($remark_id);
        
        $success_msg = $error_message =  "";
        if ($remark)
        {
            DB::beginTransaction();
            try
            {
                $remark->delete();
                $success_msg = "Remark deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Remark not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function  homework($subject = NULL, $id = NULL){
        $loginInfo = get_loggedin_user_data();
        $session                = get_current_session();
        $decrypted_homework_group_id = get_decrypted_value($id, true);
        $decrypted_subject_id = get_decrypted_value($subject, true);

        $homeworkData = [];
        $conversations = HomeworkGroup::where('homework_group_id', "=", $decrypted_homework_group_id)->with('getClass')->with('getSection')->with(['getMessages' => function($query) use ($decrypted_subject_id) {
            $query->where('subject_id', $decrypted_subject_id);
            $query->orderBy('h_conversation_id', 'ASC');
        }])->get()->first()->toArray();
        $all_hw = HomeworkConv::where('h_session_id', "=", $session['session_id'])->where('homework_group_id', "=", $decrypted_homework_group_id)->where('subject_id', "=", $decrypted_subject_id)->with('getStaff')->with('getSubject')->get()->toArray();
        
        foreach($all_hw as $hw){
            $staff_img = "public/images/default.png"; 
            if (!empty($hw['get_staff']['staff_profile_img']))
            {
                $profile = check_file_exist($hw['get_staff']['staff_profile_img'], 'staff_profile');
                if (!empty($profile))
                {
                    $staff_img = $profile;
                }
            }
            $homeworkData[] = array(
                'h_conversation_id' => $hw['h_conversation_id'],
                'homework_group_id' => $hw['homework_group_id'],
                'staff_id' => $hw['staff_id'],
                'subject_id' => $hw['subject_id'],
                'h_conversation_text' => $hw['h_conversation_text'],
                'time' => time_elapsed_string($hw['created_at']),
                'staff_name' => $hw['get_staff']['staff_name'],
                'staff_profile_img' => $staff_img,
                'attechment' => $hw['h_conversation_attechment'],
                'subject_name' => $hw['get_subject']['subject_name'].' - '.$hw['get_subject']['subject_code'],
            );
            
        }

        $data = array(
            'page_title'        => trans('language.homework_group'),
            'redirect_url'      => url('admin-panel/my-subjects/homework-group/'.$id),
            'login_info'        => $loginInfo,
            'homework_group_id' => $decrypted_homework_group_id,
            'subject_id' => $decrypted_subject_id,
            'conversations'     => $conversations,
            'homework_data'     => $homeworkData
        );
        return view('admin-panel.homework.index')->with($data);
    }

    public function  send_homework(Request $request){
        
        $loginInfo = get_loggedin_user_data();
        $staff_id        = $loginInfo['staff_id'];
        
        $message_text       = Input::get('message');
        $subject_id         = Input::get('subject_id');
        $homework_group_id  = Input::get('homework_group_id');
        $subject_name = get_subject_name($subject_id);
        $staff_info = get_staff_signle_data($staff_id);
        $staff_info = isset($staff_info[0]) ? $staff_info[0] : [];
        $session                = get_current_session();
        $group = HomeworkGroup::where('homework_group_id', "=", $homework_group_id)->with('getClass')->with('getSection')->first();


        $message_date = '';
        if ( !empty($message_text) && !empty($homework_group_id) ){

            $homework = New HomeworkConv();
            DB::beginTransaction();
            try
            {
                $homework->admin_id       = $loginInfo['admin_id'];
                $homework->update_by      = $loginInfo['admin_id'];
                $homework->h_session_id   = $session['session_id'];
                $homework->h_conversation_text  = $message_text;
                $homework->staff_id             = $staff_id;
                $homework->subject_id           = $subject_id;
                $homework->homework_group_id    = $homework_group_id;
                $homework->save();
                if ($request->hasFile('h_conversation_attechment'))
		        {
		            $file                          = $request->File('h_conversation_attechment');
		            $config_document_upload_path   = \Config::get('custom.conversation_attechment');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
                    $name                          = substr($file->getClientOriginalName(),0,-4);
                    if($ext == 'docx'){
                        $ext = ".docx";
                    }
                    if($ext == 'jpeg'){
                        $ext = ".jpeg";
                    }
                    if($ext == 'xlsx'){
                        $ext = ".xlsx";
                    }
		            $filename                      = $homework->h_conversation_id.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
                    $homework->h_conversation_attechment = $filename;
                    $homework->save();
                }
               
                $subject_name = get_subject_name($subject_id);
                $send_notification  = $this->send_push_notification_class($homework_group_id,$group->class_id,$group->section_id,'student_homework_by_staff');
                $message_date = $homework->created_at;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return response()->json(['status'=>false, 'error_message'=> $error_message]);
            }
            DB::commit();

            if(!empty($staff_info["staff_profile_img"])){
                $staff_img = '<img src="'.url($staff_info["staff_profile_img"]).'" class="float-right" title="preview" alt="preview">';
            } else {
                $default_img = "public/images/default.png"; 
                $staff_img = '<img src="'.url($default_img).'" class="float-right" title="preview" alt="preview">';
            }
            $user_message = '<li class="clearfix message-animation"> <div class="message my-message float-right"> Subject: '.$subject_name.' <br /><br /> '.$message_text.' <span class="chat-staff-name">'.$staff_info["staff_name"].'</span> </div><div class="clearfix"></div> '.$staff_img.' <div class="message-data float-right"> <span class="message-data-time"> '.time_elapsed_string($message_date).' </span> &nbsp; </div><div class="clearfix"></div></li>';
            
            return response()->json(['status'=>true, 'user_message'=> $user_message ]);

        }
    }

    public function send_push_notification($module_id,$staff_id,$student_id,$subject_name,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $session       = get_current_session();
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            if (!empty($student_id) && !empty($student_id) && $student_id != null){
                $query->where('students.student_id', $student_id);
            }
            $query->where('students.student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($class_id,$section_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('students.student_id','students.student_name','students.student_parent_id','students.reference_admin_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];

        if($student_info['get_admin_info']['fcm_device_id'] != "" && $student_info['get_admin_info']['notification_status'] == 1){
            $student_admin_id = $student_info['get_admin_info']['admin_id'];
            $device_ids[] = $student_info['get_admin_info']['fcm_device_id'];
        }
        if($student_info['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student_info['get_parent']['get_parent_admin_info']['notification_status'] == 1){
            $parent_admin_id = $student_info['get_parent']['get_parent_admin_info']['admin_id'];
            $device_ids[] = $student_info['get_parent']['get_parent_admin_info']['fcm_device_id'];
        }
        if($notification_type == 'student_dairy_remark_by_staff'){
            $message = $student_info['student_name']." received remark in ".$subject_name;
        }
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => $student_admin_id,
            'parent_admin_id' => $parent_admin_id,
            'staff_admin_id' => Null,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }

    public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();
        $message = "Today's homework is updated.";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name'].":".$section['section_class']['class_id']."-".$section['section_id'];

            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name'].":".$section['section_class']['class_id']."-".$section['section_id'];

            $students = pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],1);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],2);
        }
        
    }

    public function download_homework_attechment($id){
        $hw = HomeworkConv::find($id);
        $filePath = '';
        $file = check_file_exist($hw->h_conversation_attechment, 'conversation_attechment');
        if (!empty($file))
        {
            $filePath = $file;
        }
        return response()->download($filePath);
    }
}
