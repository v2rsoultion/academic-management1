<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\School\School; // Model
use App\Model\Student\StudentTC; // Model
use App\Model\Certificate\Certificate;
use App\Model\Student\StudentAttendanceDetails; // Model
use Yajra\Datatables\Datatables;

class TcController extends Controller
{
    /**
     *  View page for TC
     *  @Pratyush on 11 Aug 2018
    **/
    public function index()
    {
        $listData                   =  $arr_section  = [];
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.view_tc'),
            'redirect_url'  => url('admin-panel/student/view-issue-transfer-certificate'),
            'login_info'    => $loginInfo,
            'listData'          => $listData
        );
        return view('admin-panel.student-tc.index')->with($data);
    } 	 

    /**
     *  View page for TC
     *  @Pratyush on 11 Aug 2018
    **/
    public function add_tc()
    {
        $listData                   =  $arr_section  = [];
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.issue_transfer_certificate'),
            'redirect_url'  => url('admin-panel/student/view-issue-transfer-certificate'),
            'login_info'    => $loginInfo,
            'listData'      => $listData
        );
        return view('admin-panel.student-tc.add')->with($data);
    } 
    
    public function get_tc_records(Request $request){
        // p($request->all());
        $arr_student = $student_list = [];
        
        $arr_student        = Student::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('enroll_no')))
            {
                $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
            }
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
            $query->where('student_status',$request->get('student_status'));
        })->join('student_academic_info', function($join) use ($request){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
        })
        ->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getTc')
        ->orderBy('students.student_id', 'DESC')
        ->get();
        // p($arr_student);
        if(!empty($arr_student)){
            foreach ($arr_student as $key => $students) {
                $class_name = $section_name = $student_image = $class_id = $section_id = '';
                if (!empty($students->student_image))
                {
                    $profile = check_file_exist($students->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                }
                if(isset($students['getStudentAcademic']['getCurrentClass']['class_name'])){
                    $class_name = $students['getStudentAcademic']['getCurrentClass']['class_name'];
                    $class_id = $students['getStudentAcademic']['getCurrentClass']['class_id'];
                }
                if(isset($students['getStudentAcademic']['getCurrentSection']['section_name'])){
                    $section_name = $students['getStudentAcademic']['getCurrentSection']['section_name'];
                    $section_id = $students['getStudentAcademic']['getCurrentSection']['section_id'];
                }
                $class_section = $class_name.' - '.$section_name; 
                $tc_no = $tc_remark = $tc_date = $tc_reason = '';
                if(isset($students['getTc'])){
                    $tc_no = "TC".$students['getTc']['student_tc_id'];
                    $tc_date = date("d F Y", strtotime($students['getTc']['tc_date']));
                    $tc_remark = $students['getTc']['tc_remark'];
                    $tc_reason = $students['getTc']['tc_reason'];
                }
                $student_list[] = array(
                    'student_id'            => $students->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $students->student_enroll_number,
                    'student_roll_no'       => $students->student_roll_no,
                    'student_name'          => $students->student_name,
                    'class_id'              => $class_id,
                    'section_id'            => $section_id,
                    'class_section'         => $class_section,
                    'tc_no'                 => $tc_no,
                    'tc_date'               => $tc_date,
                    'tc_remark'             => $tc_remark,
                    'tc_reason'             => $tc_reason,
                );
            }
        }
        return Datatables::of($student_list)
        ->addColumn('student_profile', function ($student_list)
            {
                $profile = "";
                if($student_list['profile'] != ''){
                    $profile = "<img src=".$student_list['profile']." height='30' />";
                }   
                $name = $student_list['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
        ->addColumn('class_section', function ($student_list)
        {
            return $student_list['class_section'];
        })
        ->addColumn('action', function ($student_list)
        {
            $encrypted_student_id = get_encrypted_value($student_list['student_id'], true);
            return ' 
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li>
                    <a title="Generate TC" class="generate-tc" class-id="'.$student_list['class_id'].'" section-id="'.$student_list['section_id'].'" student-name = "'.$student_list['student_name'].'" class-section = "'.$student_list['class_section'].'" student-id="'.$student_list['student_id'].'" >Generate TC</a>
                </li>
                </ul>
            </div>';
        })
        ->addColumn('tc_action', function ($student_list)
        {
            $encrypted_student_id = get_encrypted_value($student_list['student_id'], true);
            return ' 
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li>
                    <li><a title="Cancel TC"  href="'.url('/admin-panel/student/cancel-transfer-certificate/'.$encrypted_student_id.' ').'" onclick="return confirm('."'Are you sure?'".')" >Cancel TC</a></li>
                    <li>
                        <a title="Transfer Certificate" href="'.url('/admin-panel/student/transfer-certificate/'.$encrypted_student_id.' ').'" >Transfer Certificate</a>
                    </li>
                </ul>
            </div>';
        })
        ->rawColumns(['action' => 'action','tc_action' => 'tc_action'])->addIndexColumn()->make(true);
    }

    public function save_tc(Request $request){
        $loginInfo  = get_loggedin_user_data();
        $session = get_current_session();
        DB::beginTransaction();
            try
            {
                $tc  = New StudentTC;
                $tc->admin_id      = $loginInfo['admin_id'];
                $tc->update_by     = $loginInfo['admin_id'];
                $tc->session_id    =  $session['session_id'];
                $tc->class_id      =  Input::get('s_class_id');
                $tc->section_id    =  Input::get('s_section_id');
                $tc->student_id    =  Input::get('s_student_id');
                $tc->tc_date       =  Input::get('tc_date');
                $tc->tc_remark     =  Input::get('tc_remark');
                $tc->tc_reason     =  Input::get('tc_reason');
                $tc->save();
                $student = Student::find($request->get('s_student_id'));
                $student->student_status = 0;
                $student->save();
                $msg = "success";
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                $msg = $error_message;
            }
        DB::commit();
        return $msg;
    }

    public function cancel_tc($id){
        $decrypted_student_id 	= get_decrypted_value($id, true);
        DB::beginTransaction();
        try
        {
            $tc  = StudentTC::where('student_id',$decrypted_student_id)->first();
            $tc->delete();
            DB::table('students') 
            ->where('student_id',$decrypted_student_id)
            ->update([ 'student_status' => 1 ]);
            $msg = "Student's TC successfully cancel.";
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            $msg = $error_message;
            return redirect()->back()->withInput()->withErrors($msg);
        }
        DB::commit();
        return redirect()->back()->withInput()->withSuccess($msg);
        
    }

    public function tc_certificate($id){
        $loginInfo   = get_loggedin_user_data();
        $decrypted_student_id 	= get_decrypted_value($id, true);
        $activeCertificate = Certificate::where('template_type',0)->Where('certificate_status',1)->first();
        if(empty($activeCertificate)){
            $activeCertificate = Certificate::where('template_type',0)->orWhere('certificate_status',1)->first();
        }
        $school              = School::with('getCountry','getState','getCity')->first()->toArray();
        
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $student_info = Student::where(function($query) use ($decrypted_student_id) 
        {
            $query->where('students.student_id',$decrypted_student_id);
        })->join('student_tc', function($join) use ($decrypted_student_id){
            $join->on('student_tc.student_id', '=', 'students.student_id');
            $join->where('student_tc.student_id', '=',$decrypted_student_id);
        })
        ->leftjoin('student_marksheet', function($join) use ($decrypted_student_id){
            $join->on('student_marksheet.student_id', '=', 'students.student_id');
            $join->orderBy('student_marksheet.student_marksheet_id', 'DESC');
        })
        ->select('students.*','student_tc.*','student_marksheet.*','student_tc.created_at as tc_create_date','students.student_id as student_id')
        ->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getParent','getStudentNationality')
        ->with('getStudentCaste')
        ->first();
        $templateName = $activeCertificate->template_link;
        $medium_type = get_medium_info($student_info['medium_type']);
        $total_attendance = $this->get_total_attendance($student_info['medium_type']);
        $total_days = 365 - count(get_school_all_holidays(1));
        
        $pass = "Pass";
        if($student_info['pass_fail'] == 0){
            $pass = "Fail";
        }
        $all_subjects = '';
        $subjects = get_all_section_subjects($student_info['getStudentAcademic']['getCurrentSection']['section_id']);
        if(!empty($subjects)){
            $all_subjects = implode(",",$subjects);
        }
        // p($school);
        $certificate_info = array(
            'student_name' => $student_info['student_name'],
            'student_enroll_number' => $student_info['student_enroll_number'],
            'student_father_name' => $student_info['getParent']['student_father_name'],
            'student_mother_name' => $student_info['getParent']['student_mother_name'],
            'father_contact' => $student_info['getParent']['student_father_mobile_number'],
            'class' => $student_info['getStudentAcademic']['getCurrentClass']['class_name'].'-'.$student_info['getStudentAcademic']['getCurrentSection']['section_name'],
            'nationality' => $student_info['getStudentNationality']['nationality_name'],
            'caste' => $student_info['getStudentCaste']['caste_name'],
            'admission_date' => date("d F Y",strtotime($student_info['student_reg_date'])),
            'student_dob' => date("d F Y",strtotime($student_info['student_dob'])),
            'pass_fail' => $pass,
            'tc_created_date' => date("d F Y",strtotime($student_info['tc_create_date'])),
            'tc_issue_date' => date("d F Y",strtotime($student_info['tc_date'])),
            'tc_remark' => $student_info['tc_remark'],
            'tc_reason' => $student_info['tc_reason'],
            'tc_no' => $student_info['student_tc_id'],
            'language'  => $medium_type,
            'serial_no' => $activeCertificate->current_serial_no,
            'book_no' => $activeCertificate->current_book_no,
            'total_days' => $total_days,
            'total_attendance' => $total_attendance,
            'all_subjects'  => $all_subjects,
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'logo' => $school['logo'],
            'school_registration_no' => $school['school_registration_no'],
            'school_url' => $school['school_url'],
            'school_email' => $school['school_email'],
            'school_mobile_number' => $school['school_mobile_number'],
            'school_address' => $school['school_address'],
            'school_pincode' => $school['school_pincode'],
            'country' => $school['get_country']['country_name'],
            'state' => $school['get_state']['state_name'],
            'city' => $school['get_city']['city_name'],
            'school_telephone' => $school['school_telephone'],
            'school_sno_numbers' => $school['school_sno_numbers'],
            'issue_date' => date("d-m-Y"),
        );
        // p($certificate_info);
        $page_title = "Transfer Certificate";
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info
        );
        // p($certificate_info);
        return view('admin-panel.certificate-templates.'.$templateName)->with($data);
    }

    public function get_total_attendance($student_id){
        $count = StudentAttendanceDetails::where('student_id',$student_id)->get()->count();
        return $count;
    }
}
