<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\File;
use Validator;
// use PDF;
// use Arius\NumberFormatter;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

use App\Model\School\School;
use App\Model\FeesCollection\Receipt;
use App\Model\FeesCollection\ReceiptDetail;
use App\Model\FeesCollection\OneTime; // Model
use App\Model\FeesCollection\RecurringHead; // Model

class FeesReceiptController extends Controller
{
    /**
     *  View page for view fees receipt
     *  @shree on 14 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = [];
        $data = array(
            'page_title' => trans('language.view_fees_receipt'),
            'redirect_url' => url('admin-panel/fees-collection/view-fees-receipt'),
            'login_info' => $loginInfo,
            'listData' => $listData,
        );
        return view('admin-panel.fees-receipt.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 14 Dec 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $receipt_data   = [];
        $receipt  = Receipt::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('receipt_number')))
            {
                $query->where('receipt_number', "like", "%{$request->get('receipt_number')}%");
            }
            if (!empty($request) && !empty($request->has('receipt_status')))
            {
                $query->where('receipt_status', "like", $request->get('receipt_status'));
            }
        })
        ->orderBy('receipt_id', 'DESC')
        ->with('getClass')
        ->with('getStudent')
        ->with('getBank')
        ->get();

        foreach ($receipt as $receiptData){
            $wallet_amount = $total_fine_amount = $total_concession_amount = $imprest_amount = $pay_later_amount = 0.00;
            

            if($receiptData->wallet_amount != ''){
                $wallet_amount = $receiptData->wallet_amount;
            }
            if($receiptData->total_concession_amount != ''){
                $total_concession_amount = $receiptData->total_concession_amount;
            }
            if($receiptData->total_fine_amount != ''){
                $total_fine_amount = $receiptData->total_fine_amount;
            }
            if($receiptData->imprest_amount != ''){
                $imprest_amount = $receiptData->imprest_amount;
            }
            if($receiptData->pay_later_amount != ''){
                $pay_later_amount = $receiptData->pay_later_amount;
            }
            $receipt_data[] = array(
                'receipt_id' => $receiptData->receipt_id,
                'class_name' => $receiptData['getClass']->class_name,
                'student_name' => $receiptData['getStudent']->student_name,
                'receipt_id' => $receiptData->receipt_id,
                'receipt_number' => $receiptData->receipt_number,
                'receipt_date' => date("d M Y",strtotime($receiptData->receipt_date)),
                'net_amt' => $receiptData->net_amount,
                'pay_amt' => $receiptData->total_amount,
                'wallet_amt' => $wallet_amount,
                'concession_amt' => $total_concession_amount,
                'fine_amt' => $total_fine_amount,
                'imprest_amt' => $imprest_amount,
                'pay_later_amt' => $pay_later_amount,
                'pay_mode' => $receiptData->pay_mode,
                'receipt_status' => $receiptData->receipt_status,
            );
        }
        
        return Datatables::of($receipt_data)
        ->addColumn('action', function ($receipt_data)
        {
            $encrypted_receipt_id = get_encrypted_value($receipt_data['receipt_id'], true);
            if($receipt_data['receipt_status'] == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                // $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Cancel Receipt"> <i class="fas fa-minus-circle"></i> </div>';
            }
            return '<div class="pull-left"><a href= "'.url('/admin-panel/fees-collection/fees-receipt-status/'.$status.'/' . $encrypted_receipt_id . ' ').'"     onclick="return confirm('."'Are you sure?'".')"  >'.$statusVal.'</a></div>

            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li><a  href="'.url('/admin-panel/fees-collection/fee-receipt/'.$encrypted_receipt_id.'?download=pdf ').'" target="_blank" >Fees Receipt</a></li>
                </ul>
            </div>
            ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Fees receipt's status
     *  @Shree on 14 Dec 2018.
    **/
    public function changeStatus($status,$id)
    {
        $receipt_id = get_decrypted_value($id, true);
        $receipt    = Receipt::find($receipt_id);
        if ($receipt)  {
            $receipt->receipt_status  = $status;
            $receipt->save();

            DB::table('fee_receipt_details') 
            ->where('receipt_id', $receipt_id)
            ->update([ 'cancel_status' => 1 ]);
            $success_msg = "Receipt sucessfully cancel!";
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Receipt not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function fees_receipt($receipt_id,Request $request){
        $loginInfo   = get_loggedin_user_data();
        $decrypted_receipt_id   = get_decrypted_value($receipt_id, true);
        $receiptData = Receipt::where('fee_receipt.receipt_id','=',$decrypted_receipt_id)
        ->with('getClass')->with('getBank')
        ->with('getStudent')->with('getStudent.getParent')->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')
        ->get()->toArray();

        foreach ($receiptData as $key => $receipt) {
            $all_installments = ReceiptDetail::where('receipt_id','=',$decrypted_receipt_id)->with('getInstallmentInfo')->get()->toArray();
            $installment_data = [];
            foreach($all_installments as $instKey => $installment){
                $installment['head_name'] = '';
                if($installment['head_type'] == 'RECURRING'){
                    $headInfo = RecurringHead::where('rc_head_id','=', $installment['head_id'])->get()->toArray();
                    $headInfo = isset($headInfo[0]) ? $headInfo[0] : [];
                
                    if(!empty($headInfo)){
                        $installment['head_name'] = $headInfo['rc_particular_name'];
                    }
                    // $installment['head_name'] = $headInfo['rc_particular_name'];
                } else if($installment['head_type'] == 'ONETIME'){
                    $headInfo   = OneTime::where('ot_head_id','=',$installment['head_id'])->get()->toArray();
                    $headInfo = isset($headInfo[0]) ? $headInfo[0] : [];
                    if(!empty($headInfo)){
                        $installment['head_name'] = $headInfo['ot_particular_name'];
                    }
                   
                }
                $installment_data[] = $installment;
               
            }
            $receiptData[0]['all_installments'] = $installment_data;
            
        }
        $receiptData              = isset($receiptData[0]) ? $receiptData[0] : [];
        
        $school              = School::first()->toArray();
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $total_amount = $wallet_amount = $total_concession_amount =  $total_calculate_fine_amount = $net_amount = $imprest_amount =   0.00;
        
        if($receiptData['total_amount'] != ''){
            $total_amount = $receiptData['total_amount'];
        }
        if($receiptData['wallet_amount'] != ''){
            $wallet_amount = $receiptData['wallet_amount'];
        }
        if($receiptData['total_concession_amount'] != ''){
            $total_concession_amount = $receiptData['total_concession_amount'];
        }
        if($receiptData['total_calculate_fine_amount'] != ''){
            $total_calculate_fine_amount = $receiptData['total_calculate_fine_amount'];
        }
        if($receiptData['net_amount'] != ''){
            $net_amount = $receiptData['net_amount'];
        }
        if($receiptData['imprest_amount'] != ''){
            $imprest_amount = $receiptData['imprest_amount'];
        }
        $certificate_info = array(
            'student_name' => $receiptData['get_student']['student_name'],
            'student_enroll_number' => $receiptData['get_student']['student_enroll_number'],
            'student_father_name' => $receiptData['get_student']['get_parent']['student_father_name'],
            'class' => $receiptData['get_student_academic']['get_current_class']['class_name'],
            'receipt_number' => $receiptData['receipt_number'],
            'receipt_date' => date("d M Y", strtotime($receiptData['receipt_date'])),
            'pay_mode' => $receiptData['pay_mode'],
            'total_amount' => $total_amount,
            'wallet_amount' => $wallet_amount,
            'total_concession_amount' => $total_concession_amount,
            'total_calculate_fine_amount' => $total_calculate_fine_amount,
            'net_amount' => $net_amount,
            'imprest_amount' => $imprest_amount,
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'school_mobile_number' => $school['school_mobile_number'],
            'school_email' => $school['school_email'],
            'logo' => $school['logo'],
            'all_installments' => $receiptData['all_installments'],
        );
        // p($certificate_info);
        $page_title = "Fees Receipt";
        
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info
        );
       
        // if($request->has('download')){
        //     $pdf = PDF::loadView('admin-panel.fees-receipt.fees_receipt_template')->with($data);
        //     return $pdf->stream('fee_receipt.pdf');
        // }
        return view('admin-panel.fees-receipt.fees_receipt_template')->with($data);
        
        
    }
}
