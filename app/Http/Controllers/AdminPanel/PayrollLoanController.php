<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Loan; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollLoanController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Loan
     * @Khushbu on 19 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $loan = $arr_staff = []; 
        $staff       = Staff::get()->toArray();
        foreach($staff as $key => $value) {
            $arr_staff[$value['staff_id']] = $value['staff_name'];
        }
        $loan['staff']  = add_blank_option($arr_staff, 'Select Employee');
        $loginInfo   = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_loan_id 	    = get_decrypted_value($id, true);
            $loan      		        = Loan::Find($decrypted_loan_id);
            $loan['staff']          = $arr_staff;
            $page_title             = trans('language.edit_loan');
        	$save_url    			= url('admin-panel/payroll/manage-loan-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title             = trans('language.add_loan');
	 		$save_url    			= url('admin-panel/payroll/manage-loan-save');
	 		$submit_button  		= 'Save';
        }
        $loan['search_staff']  = add_blank_option($arr_staff, 'Select Employee');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'loan'            => $loan,  
        );
        return view('admin-panel.payroll-loan.add')->with($data);
    }

    /**
     *	Add & Update of Payroll Loan
     *  @Khushbu on 20 Feb 2019
    **/
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_loan_id			= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $session                    = get_current_session();
        if(!empty($id)) {
            $loan        = Loan::Find($decrypted_loan_id);
            if(!$loan) {
                return redirect('admin-panel/payroll/manage-loan-save')->withErrors('Loan not found!');
            }
            $admin_id    = $loan->admin_id;
            $success_msg = 'Loan updated successfully!';
        } else {
            $loan    	    = New Loan;
            $success_msg    = 'Loan saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'staff_id'       => 'required',
                'loan_principal' => 'required',
                'loan_from_date' => 'required',
                'loan_to_date'   => 'required',
                'loan_interest_rate' => 'required',
                'loan_deduct_amount' => 'required',
            ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $loan->admin_id             = $admin_id;
                $loan->update_by            = $loginInfo['admin_id'];
                $loan->session_id           = $session['session_id'];
                $loan->staff_id             = Input::get('staff_id');
                $loan->loan_principal       = Input::get('loan_principal');
                $loan->loan_from_date       = Input::get('loan_from_date');
                $loan->loan_to_date         = Input::get('loan_to_date');
                $loan->loan_inrterest_rate  = Input::get('loan_interest_rate');
                $loan->loan_deduct_amount   = Input::get('loan_deduct_amount');
                $loan->loan_total_amount    = Input::get('loan_total_amount');
                $loan->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-loan')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Loan's Data fo view page
     *  @Khushbu on 20 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$loan 		        = Loan::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_loan_employee') !=  NULL)
            {
                $query->where('staff_id', $request->get('s_loan_employee'));
            }
        })->orderBy('pay_loan_id','DESC')->with('LoanStaff')->get();
        return Datatables::of($loan)
        ->addColumn('employee_profile', function($loan) {
            $profile = '';
            if($loan['LoanStaff']['staff_profile_img'] != '') {
                $staff_profile_path = check_file_exist($loan['LoanStaff']['staff_profile_img'], 'staff_profile');
                $profile = "<img src=".url($staff_profile_path)." height='30' />";
            }
            $staff_name = $loan['LoanStaff']['staff_name'];
            $employee_profile = $profile." ".$staff_name;
            return $employee_profile;
            })
        ->addColumn('loan_date', function($loan) {
            return $loan_date = date('d-m-Y',strtotime($loan->loan_from_date)). " - " .date('d-m-Y',strtotime($loan->loan_to_date));
        })
    	->addColumn('action', function($loan) use($request) {
            $encrypted_loan_id  = get_encrypted_value($loan->pay_loan_id, true);
      		return '<div class="text-center">
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-loan/'.$encrypted_loan_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-loan/' . $encrypted_loan_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['employee_profile' => 'employee_profile', 'loan_date' => 'loan_date', 'action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-loan');
    }  

    /**
	 *	Destroy Data of Payroll Loan
     *  @Khushbu on 20 Feb 2019
	**/
	public function destroy($id) {
		$loan_id 		= get_decrypted_value($id, true);
        $loan 	        = Loan::find($loan_id);
        if ($loan)
        {
            DB::beginTransaction();
            try
            {
                $loan->delete();
                $success_msg = "Loan deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Loan not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
