<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Admission\AdmissionForm; // Model
use App\Model\Admission\AdmissionData;
use Yajra\Datatables\Datatables;

class AdmissionReportController extends Controller
{
    /**
     *  View page for Classwise Admission Data
     *  @Khushbu on 05 March 2019
    **/
    public function classWiseAdmission() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_admission_report'),
            'redirect_url'  => url('admin-panel/admission-report/classwise-admission-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.admission-report.classwise_admission_report')->with($data);
    }

    public function showClassWiseAdmission(Request $request){
        $session        = get_current_session();
        $admission      = AdmissionForm::where([['session_id', $session['session_id']],['form_type',0]])->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',class_ids)');
            }
        })->with('AdmissionDataInfo')->get();
        return Datatables::of($admission)      
        ->addColumn('class_name', function($admission) {
            $class_name = get_selected_classes($admission['class_ids']);
            return $class_name;
        })
        ->addColumn('mediums', function($admission) {
            $mediums = get_medium_info($admission['medium_type']);
            return $mediums;
        })
        ->addColumn('no_of_applicant', function($admission) {
            return count($admission['AdmissionDataInfo']);
        })
       ->rawColumns(['class_name' => 'class_name', 'mediums' => 'mediums', 'no_of_applicant' => 'no_of_applicant'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Classwise Enquiry Data
     *  @Khushbu on 05 March 2019
    **/
    public function classWiseEnquiry() {
        $map                = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_admission_report'),
            'redirect_url'  => url('admin-panel/admission-report/classwise-enquiry-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.admission-report.classwise_enquiry_report')->with($data);
    }

    public function showClassWiseEnquiry(Request $request){
        $session        = get_current_session();
        $admission      = AdmissionData::where('form_type',1)->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->where('admission_class_id',$request->get('class_id'));
            }
        })->get();
        // p($admission);
        return Datatables::of($admission) 
        ->addColumn('student_profile', function ($admission)
        {
            $encrypted_admission_data_id = get_encrypted_value($admission->admission_data_id, true);
            $profile = "";
            if($admission->student_image != ''){
                $profile = check_file_exist($admission->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $admission->student_name;
            $complete = '<a href="'.url('admin-panel/admission-form/form-data/'.$encrypted_admission_data_id.' ').'">'.$st_profile."  ".$name.'</a>';
            return $complete;
        })
        ->addColumn('enquiry_date', function($admission) {
            $enquiry_date = $admission['created_at']->format('m-d-Y');
            return $enquiry_date;
        })
        ->addColumn('status', function($admission) {
            if($admission['admission_data_status'] == 0)
                $status = 'Pending';
            else if($admission['admission_data_status'] == 1)
                    $status = 'Contacted';
                 else if($admission['admission_data_status'] == 2)
                        $status = 'Admitted';

            return $status;
        })
       ->rawColumns(['student_profile' => 'student_profile', 'enquiry_date' => 'enquiry_date', 'status' => 'status'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Classwise Enquiry Data
     *  @Khushbu on 05 March 2019
    **/
    public function formWiseFees() {
        $map                = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_admission_report'),
            'redirect_url'  => url('admin-panel/admission-report/formwise-fees-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.admission-report.formwise_fees_report')->with($data);
    }

    public function showFormWiseFees(Request $request){
        $session        = get_current_session();
        $admission      = AdmissionForm::where('session_id', $session['session_id'])->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',class_ids)');
            }
        })->get();
        return Datatables::of($admission) 
        ->addColumn('form_name', function($admission) use($session){
            $form_name = $admission['form_name']. ' ' .$session['session_year'];
            return $form_name;
        })     
        ->addColumn('class_medium', function($admission) {
            $class_medium = get_selected_classes($admission['class_ids']). ' - ' .get_medium_info($admission['medium_type']);
            return $class_medium;
        })
        ->addColumn('no_of_applicant', function($admission) {
            return count($admission['AdmissionDataInfo']);
        })
       ->rawColumns(['form_name' => 'form_name', 'class_medium' => 'class_medium', 'mediums' => 'mediums', 'no_of_applicant' => 'no_of_applicant'])->addIndexColumn()
        ->make(true);
    }
}
