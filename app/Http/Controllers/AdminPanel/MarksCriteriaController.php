<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Examination\MarksCriteria; // Model
use Yajra\Datatables\Datatables;

class MarksCriteriaController extends Controller
{
    /**
     *  View page for Term
     *  @Pratyush on 11 Aug 2018
    **/
    public function index()
    {
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_terms'),
            'redirect_url'  => url('admin-panel/examination/manage-marks-criteria'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.marks-criteria.index')->with($data);
    } 	   

    /**
     *  Add page for Term
     *  @Pratyush on 11 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $markscriteria 			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_marks_criteria_id 	= get_decrypted_value($id, true);
            $markscriteria      			= MarksCriteria::Find($decrypted_marks_criteria_id);
            if (!$markscriteria)
            {
                return redirect('admin-panel/examination/manage-marks-criteria')->withError('Marks criteria not found!');
            }
            $page_title             	= trans('language.edit_marks_criteria');
            $encrypted_marks_criteria_id= get_encrypted_value($markscriteria->marks_criteria_id, true);
            $save_url               	= url('admin-panel/examination/save-marks-criteria/' . $encrypted_marks_criteria_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_marks_criteria');
            $save_url      = url('admin-panel/examination/save-marks-criteria');
            $submit_button = 'Save';
        }
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'markscriteria' 	=> $markscriteria,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/examination/manage-marks-criteria'),
        );
        return view('admin-panel.marks-criteria.add')->with($data);
    }

    /**
     *  Add and update Marks criteria's data
     *  @Shree on 9 oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			    = get_loggedin_user_data();
        $decrypted_marks_criteria_id    = get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $markscriteria = MarksCriteria::find($decrypted_marks_criteria_id);
            $admin_id = $markscriteria['admin_id'];
            if (!$markscriteria)
            {
                return redirect('/admin-panel/examination/manage-marks-criteria/')->withError('Marks criteria not found!');
            }
            $success_msg = 'Marks criteria updated successfully!';
        }
        else
        {
            $markscriteria  = New MarksCriteria;
            $success_msg 	= 'Marks criteria  saved successfully!';
        }
        
        $validatior = Validator::make($request->all(), [
            'criteria_name'   => 'required|unique:marks_criteria,criteria_name,' . $decrypted_marks_criteria_id . ',marks_criteria_id',
                
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $markscriteria->admin_id       	= $admin_id;
                $markscriteria->update_by      	= $loginInfo['admin_id'];
                $markscriteria->criteria_name 	= Input::get('criteria_name');
                $markscriteria->max_marks 	    = Input::get('max_marks');
                $markscriteria->passing_marks 	= Input::get('passing_marks');
                $markscriteria->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/examination/manage-marks-criteria')->withSuccess($success_msg);
    }

    /**
     *  Get Marks criteria's Data for view page(Datatables)
     *  @Shree on 9 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $markscriteria  = MarksCriteria::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('criteria_name', "like", "%{$request->get('name')}%");
            }
          
        })->orderBy('marks_criteria_id', 'DESC')->get();
        return Datatables::of($markscriteria,$request)
       
        ->addColumn('action', function ($markscriteria) use($request)
        {
            if($request->get('tempid') != '' && $request->get('tempid') != null){
                $edit_manage_path = '../manage-marks-criteria';                        
            }else{
                $edit_manage_path = 'manage-marks-criteria';
            }
            $encrypted_marks_criteria_id = get_encrypted_value($markscriteria->marks_criteria_id, true);
            if($markscriteria->criteria_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="marks-criteria-status/'.$status.'/' . $encrypted_marks_criteria_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.$edit_manage_path.'/'.$encrypted_marks_criteria_id.')"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-marks-criteria/' . $encrypted_marks_criteria_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Marks criteria's data
     *  @Shree on 9 Oct 2018.
    **/
    public function destroy($id)
    {
        $marks_criteria_id 	= get_decrypted_value($id, true);
        $marks_criteria     = MarksCriteria::find($marks_criteria_id);
       
        $success_msg = $error_message =  "";
        if ($marks_criteria)
        {
            DB::beginTransaction();
            try
            {
                $marks_criteria->delete();
                $success_msg = "Marks criteria deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/examination/manage-marks-criteria')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/examination/manage-marks-criteria')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Marks criteria not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change status
     *  @SHree on 9 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $marks_criteria_id 		= get_decrypted_value($id, true);
        $marks_criteria 		  	= MarksCriteria::find($marks_criteria_id);
        if ($marks_criteria)
        {
            $marks_criteria->criteria_status  = $status;
            $marks_criteria->save();
            $success_msg = "Marks criteria status update successfully!";
            return redirect('admin-panel/examination/manage-marks-criteria')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Marks criteria not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
