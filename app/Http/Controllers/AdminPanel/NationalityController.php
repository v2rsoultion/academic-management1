<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Nationality\Nationality; // Model
use Yajra\Datatables\Datatables;

class NationalityController extends Controller
{
    /**
     *  View page for Nationality
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_nationality'),
            'redirect_url'  => url('admin-panel/nationality/view-nationality'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.nationality.index')->with($data);
    }

    /**
     *  Add page for Nationality
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $nationality 	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_nationality_id 	= get_decrypted_value($id, true);
            $nationality      			= Nationality::Find($decrypted_nationality_id);
            if (!$nationality)
            {
                return redirect('admin-panel/designation/add-document-category')->withError('Nationality not found!');
            }
            $page_title             	= trans('language.edit_nationality');
            $encrypted_nationality_id   = get_encrypted_value($nationality->nationality_id, true);
            $save_url               	= url('admin-panel/nationality/save/' . $encrypted_nationality_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_nationality');
            $save_url      = url('admin-panel/nationality/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'nationality' 		=> $nationality,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/designation/view-designations'),
        );
        return view('admin-panel.nationality.add')->with($data);
    }

    /**
     *  Add and update Nationality's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_nationality_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $nationality = Nationality::find($decrypted_nationality_id);
            $admin_id = $nationality['admin_id'];
            if (!$nationality)
            {
                return redirect('/admin-panel/nationality/add-nationality/')->withError('Nationality not found!');
            }
            $success_msg = 'Nationality updated successfully!';
        }
        else
        {
            $nationality       = New Nationality;
            $success_msg       = 'Nationality saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'nationality_name'   => 'required|unique:nationality,nationality_name,' . $decrypted_nationality_id . ',nationality_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $nationality->admin_id            	= $admin_id;
                $nationality->update_by           	= $loginInfo['admin_id'];
                $nationality->nationality_name 		= Input::get('nationality_name');
                $nationality->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/nationality/view-nationality')->withSuccess($success_msg);
    }

    /**
     *  Get Nationality's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $nationality  		= Nationality::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('nationality_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('nationality_id', 'DESC')->get();

        return Datatables::of($nationality)
            ->addColumn('action', function ($nationality)
            {
                $encrypted_nationality_id = get_encrypted_value($nationality->nationality_id, true);
                if($nationality->nationality_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                        <div class="pull-left"><a href="nationality-status/'.$status.'/' . $encrypted_nationality_id . '">'.$statusVal.'</a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-nationality/' . $encrypted_nationality_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-nationality/' . $encrypted_nationality_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()
            ->make(true);
    }

    /**
     *  Destroy Nationality's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $nationality_id         = get_decrypted_value($id, true);
        $nationality            = Nationality::find($nationality_id);
        
        $success_msg = $error_message =  "";
        if ($nationality)
        {
            DB::beginTransaction();
            try
            {
                $nationality->delete();
                $success_msg = "Nationality deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/nationality/view-nationality')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/nationality/view-nationality')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Nationality not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Nationality's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $nationality_id 		= get_decrypted_value($id, true);
        $nationality 		  	= Nationality::find($nationality_id);
        if ($nationality)
        {
            $nationality->nationality_status  = $status;
            $nationality->save();
            $success_msg = "Nationality status updated!";
            return redirect('admin-panel/nationality/view-nationality')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Nationality not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
