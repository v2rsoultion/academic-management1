<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\Bank; // Model
use Yajra\Datatables\Datatables;

class BankController extends Controller
{
    /**
     *  View page for Bank
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        // $res = pushnotification();
        // p($res);
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_bank'),
            'redirect_url'  => url('admin-panel/bank/view-bank'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.bank.index')->with($data);
    }

    /**
     *  Add page for Bank
     *  @Shree on 2 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    	= [];
        $bank      	= [];
        $loginInfo 	= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_bank_id 	    = get_decrypted_value($id, true);
            $bank      			    = Bank::Find($decrypted_bank_id);
            if (!$bank)
            {
                return redirect('admin-panel/bank/add-bank')->withError('Bank not found!');
            }
            $page_title         = trans('language.edit_bank');
            $encrypted_bank_id  = get_encrypted_value($bank->bank_id, true);
            $save_url           = url('admin-panel/bank/save/' . $encrypted_bank_id);
            $submit_button      = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_bank');
            $save_url      = url('admin-panel/bank/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'bank' 		        => $bank,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/bank/view-bank'),
        );
        return view('admin-panel.bank.add')->with($data);
    }

    /**
     *  Add and update bank's data
     *  @Shree on 2 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      	= get_loggedin_user_data();
        $decrypted_bank_id  = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $bank       = Bank::find($decrypted_bank_id);
            if (!$bank)
            {
                return redirect('/admin-panel/bank/add-bank/')->withError('Bank not found!');
            }
            $success_msg = 'Bank updated successfully!';
        }
        else
        {
            $bank     = New Bank;
            $success_msg = 'Bank saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'bank_name'   => 'required|unique:banks,bank_name,' . $decrypted_bank_id . ',bank_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $bank->bank_name    = Input::get('bank_name');
                $bank->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/bank/view-bank')->withSuccess($success_msg);
    }

    /**
     *  Get Bank's Data for view page(Datatables)
     *  @Shree on 2 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $bank  		= Bank::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('bank_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('bank_id', 'DESC')->get();

        return Datatables::of($bank)
        ->addColumn('action', function ($bank)
        {
            $encrypted_bank_id = get_encrypted_value($bank->bank_id, true);
            if($bank->bank_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="bank-status/'.$status.'/' . $encrypted_bank_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-bank/' . $encrypted_bank_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-bank/' . $encrypted_bank_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Bank's data
     *  @Shree on 2 Oct 2018.
    **/
    public function destroy($id)
    {
        $bank_id 		= get_decrypted_value($id, true);
        $bank 		  	= Bank::find($bank_id);
        
        $success_msg = $error_message =  "";
        if ($bank)
        {
            DB::beginTransaction();
            try
            {
                $bank->delete();
                $success_msg = "Bank deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/bank/view-bank')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/bank/view-bank')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Bank not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Bank's status
     *  @Shree on 2 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $bank_id       = get_decrypted_value($id, true);
        $bank          = Bank::find($bank_id);
        if ($bank)
        {
            $bank->bank_status  = $status;
            $bank->save();
            $success_msg = "Bank status update successfully!";
            return redirect('admin-panel/bank/view-bank')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Bank not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
