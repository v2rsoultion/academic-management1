<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Model\Exam\Exam; // Model
use App\Model\Examination\ExamMap; // Model
use App\Model\Examination\ExamMarks; // Model
use Yajra\Datatables\Datatables;

class ExamMarksController extends Controller
{
    public function manage_marks(){
        $manage = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $arr_class          = get_all_classes_mediums();
        $manage['arr_class'] = add_blank_option($arr_class, "Select Class");
        $manage['arr_section'] = add_blank_option($arr_section, "Select Section");
        $manage['arr_exams'] = add_blank_option($arr_exams, 'Select Exam');
        $data = array(
            'page_title'    => trans('language.manage_marks'),
            'redirect_url'  => url('admin-panel/examination/manage-marks'),
            'login_info'    => $loginInfo,
            'manage'        => $manage
        );
        
        return view('admin-panel.exam-marks.index')->with($data);
    }

    public function get_marks_grid(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $all_subjects = get_subject_list_with_details($request);
        $all_students = get_student_list_for_marks($request,$all_subjects);
        // p($all_subjects);
        $data = array(
            'all_subjects'    => $all_subjects,
            'all_students'    => $all_students
        );
        return view('admin-panel.exam-marks.marks-grid')->with($data);
    }

    public function save_student_marks(Request $request){

        $loginInfo  = get_loggedin_user_data();
        $marks      = $request->get('marks');
        $session    = get_current_session();
        DB::beginTransaction();
        foreach($marks as $mark){
            if($mark['marks'] != '' || $mark['exam_mark_id'] != 0){
                try
                {
                    if(!empty($mark['exam_mark_id']) && $mark['exam_mark_id'] != null){
                        $marks_data     		= ExamMarks::find($mark['exam_mark_id']) ;
                    } else {
                        $marks_data     		= New ExamMarks;
                    }

                    if($mark['marks'] == "A"){
                        $marks = 0;
                        $attend_flag = 0;
                    } else {
                        $marks = $mark['marks'];
                        $attend_flag = 1;
                    }
                    $marks_data->admin_id       = $loginInfo['admin_id'];
                    $marks_data->update_by      = $loginInfo['admin_id'];
                    $marks_data->session_id     = $session['session_id'];
                    $marks_data->exam_id 	    = $mark['exam_id'];
                    $marks_data->class_id       = $mark['class_id'];
                    $marks_data->section_id     = $mark['section_id'];
                    $marks_data->student_id     = $mark['student_id'];
                    $marks_data->subject_id     = $mark['subject_id'];
                    $marks_data->marks          = $marks;
                    $marks_data->attend_flag    = $attend_flag;
                    $marks_data->save();
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return "fail";
                }
            }
            
        }
        DB::commit();
        return 'success';
    }

    public function marks_submission(Request $request){
        DB::table('exam_marks') 
        ->where('exam_id',$request->get('exam_id'))
        ->where('class_id',$request->get('class_id'))
        ->where('section_id',$request->get('section_id'))
        ->update([ 'marks_status' => 1 ]);
        
        return 'success';
    }
}
