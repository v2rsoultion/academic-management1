<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\LibraryFine\LibraryFine; // Model
use App\Model\LibraryMember\LibraryMember; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class LibraryFineController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for Exam
     *  @Pratyush on 11 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    				= [];
        $library_fine			= [];
        $member_type  = array(
        	'1' => 'Student',
        	'2'	=> 'Staff'
        );
        $listData['member_type']	= add_blank_option($member_type, 'Select Member Type');
        $listData['member'] 		= array('0' => 'Select Member' );
        $loginInfo 					= get_loggedin_user_data();
        $terms 						= get_all_terms();
        // $listData['arr_terms']  = add_blank_option($terms, 'Select Term');
        if (!empty($id))
        {
            $decrypted_library_id 	= get_decrypted_value($id, true);
            $library_fine      		= LibraryFine::Find($decrypted_library_id);
            if(!$library_fine)
            {
                return redirect('admin-panel/library-fine')->withError('Fine details not found!');
            }
            $page_title             	= trans('language.library_fine');
            $encrypted_exam_id   		= get_encrypted_value($exam->exam_id, true);
            $save_url               	= url('admin-panel/library-fine/save/' . $encrypted_exam_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.library_fine');
            $save_url      = url('admin-panel/library-fine/save');
            $submit_button = 'Save';
        }
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'library_fine'		=> $library_fine,
            'listData'			=> $listData,
            'member_type'		=> $member_type,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/library-fine'),
        );
        return view('admin-panel.library-fine.add')->with($data);
    }

    /**
     *  Get member details
     *  @Pratyush on 23 Aug 2018
    **/
    public function getMemberDetail(Request $request)
    {
    	// p($request->get('member_id'));
    	$loginInfo 	= get_loggedin_user_data();
    	$arr_member = [];
    	if($request->has('member_id') && $request->get('member_id') == 1){
    		$member_detail	= LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',1);
            })->join('students', function($join) use ($request){
                $join->on('students.student_id', '=', 'library_members.member_id');
            })->get();

            if (!empty($member_detail))
        	{
	            foreach ($member_detail as $member)
	            {
	                $arr_member[$member->library_member_id] = $member->student_name;
	            }
        	}
    	}elseif($request->has('member_id') && $request->get('member_id') == 2){

    		$member_detail	= LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',2);
            })->join('staff', function($join) use ($request){
                $join->on('staff.staff_id', '=', 'library_members.member_id');
            })->get();

            if (!empty($member_detail))
        	{
	            foreach ($member_detail as $member)
	            {
	                $arr_member[$member->library_member_id] = $member->staff_name;
	            }
        	}

    	}
    	
        $data  = view('admin-panel.library-fine.ajax-select',compact('arr_member'))->render();
        return response()->json(['options'=>$data]);
    }


    /**
     *  Add Library Fine data
     *  @Pratyush on 23 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_library_fine_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $library_fine = LibraryFine::find($decrypted_library_fine_id);

            if (!$library_fine)
            {
                return redirect('/admin-panel/library-fine/')->withError('Fine details not found!');
            }
            $success_msg = 'Fine updated successfully!';
        }
        else
        {
            $library_fine  	= New LibraryFine;
            $success_msg 	= 'Fine saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'member_type'   => 'required',
                'member_id'   	=> 'required',
                'amount'   		=> 'required',
                'remark'   		=> 'required',
                'status'   		=> 'required',
                'date'   		=> 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $library_fine->admin_id     = $loginInfo['admin_id'];
                $library_fine->update_by    = $loginInfo['admin_id'];
                $library_fine->member_id 	= Input::get('member_id');
                $library_fine->member_type 	= Input::get('member_type');
                $library_fine->fine_amount 	= Input::get('amount');
                $library_fine->remark 	   	= Input::get('remark');
                $library_fine->fine_status 	= Input::get('status');
                $library_fine->fine_date 	= Input::get('date');
                $library_fine->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/library-fine')->withSuccess($success_msg);
    }

    /**
     *  Get Fine's Data for Library fine page(Datatables)
     *  @Pratyush on 23 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        
        $loginInfo      = get_loggedin_user_data();
        $final_details  = [];
        // For member type student
        if($request->has('member_type') && $request->get('member_type') == 1){
            $fine = LibraryFine::where(function($query) use ($loginInfo,$request){
                	
                if($request->has('search_date') && $request->get('search_date') != null){
                    $query->where('library_fine.fine_date',"=", $request->get('search_date'));	
                }
                if($request->has('search_amount') && $request->get('search_amount') != null){
                    $query->where('library_fine.fine_amount',"=", $request->get('search_amount'));	
                }
                //For Student
                $query->where('member_type',1);
            })->join('library_members', function($join) use ($request){
                $join->on('library_members.library_member_id', '=', 'library_fine.member_id');
            })->join('students', function($join) use ($request){
            	$join->on('students.student_id', '=', 'library_members.member_id');
            	if($request->has('member_name') && $request->get('member_name') != null){
                    	$join->where('students.student_name',"like", "%{$request->get('member_name')}%");	
                }
            })->get();

            if(!empty($fine[0]->library_fine_id)){
                foreach($fine as $value) {
                	
                    $final_details[] = array(
                        'library_fine_id'   => $value->library_fine_id,
                        'name' 				=> $value->student_name,
                        'amount'   			=> $value->fine_amount,
                        'date'   			=> date('d M Y',strtotime($value->fine_date)),
                        'remark'   			=> $value->remark,
                        'fine_status'		=> $value->fine_status
                    );
                }
            }
            
        }elseif($request->has('member_type') && $request->get('member_type') == 2){
            $fine = LibraryFine::where(function($query) use ($loginInfo,$request){
                	
                    if($request->has('search_date') && $request->get('search_date') != null){
                    	$query->where('library_fine.fine_date',"=", $request->get('search_date'));	
                    }
                    if($request->has('search_amount') && $request->get('search_amount') != null){
                    	$query->where('library_fine.fine_amount',"=", $request->get('search_amount'));	
                    }
                    //For Student
                    $query->where('member_type',2);
            })->join('library_members', function($join) use ($request){
                $join->on('library_members.library_member_id', '=', 'library_fine.member_id');
            })->join('staff', function($join) use ($request){
            	$join->on('staff.staff_id', '=', 'library_members.member_id');
            	if($request->has('member_name') && $request->get('member_name') != null){
                    	$join->where('staff.staff_name',"like", "%{$request->get('member_name')}%");	
                }
            })->get();

            if(!empty($fine[0]->library_fine_id)){
                foreach($fine as $value) {
                    $final_details[] = array(
                        'library_fine_id'   => $value->library_fine_id,
                        'name' 				=> $value->staff_name,
                        'amount'   			=> $value->fine_amount, 
                        'date'   			=> date('d M Y',strtotime($value->fine_date)),
                        'remark'   			=> $value->remark,
                        'fine_status'		=> $value->fine_status
                    );
                }
            }
        }
        
        return Datatables::of($final_details)
        		->addColumn('remark', function ($final_details)
                {
                     return substr($final_details['remark'],0,50)." ...";
                    
                })
                ->addColumn('fine_status', function ($final_details)
                {
                  if($final_details['fine_status'] == 1){
                		$status = 'Due';
                	}else{
                		$status = 'Paid';
                	}
                	return $status;
                })
                ->addColumn('action', function ($final_details)
                {
                    
                    $encrypted_issue_book_id = get_encrypted_value($final_details['library_fine_id'], true);
                    if($final_details['fine_status'] == 1) {
                        $status = 2;
                        $statusVal = "Paid";
                    } else {
                        $status = 1;
                        $statusVal = "Due";
                    }
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="zmdi zmdi-label"></i>
                        <span class="caret"></span>
                        </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li><a href="library-fine/status/'.$status.'/' . $encrypted_issue_book_id . '">'.$statusVal.'</a></li>
                           
                            </ul>
                        </div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="library-fine/delete/' . $encrypted_issue_book_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                    
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }


    /**
     *  Destroy Fine's Records
     *  @Pratyush on 23 July 2018.
    **/
    public function destroy($id)
    {
        $fine_id 		= get_decrypted_value($id, true);
        $fine 		  	= LibraryFine::find($fine_id);
        if ($fine)
        {
            $fine->delete();
            $success_msg = "Fine deleted successfully!";
            return redirect('admin-panel/library-fine')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Fine not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $fine_id 		= get_decrypted_value($id, true);
        $fine 		  	= LibraryFine::find($fine_id);
        if ($fine)
        {
            $fine->fine_status  = $status;
            $fine->save();
            $success_msg = "Fine status updated!";
            return redirect('admin-panel/library-fine')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Fine not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
