<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\File;
use Validator;
// use PDF;
// use Arius\NumberFormatter;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

use App\Model\School\School;
use App\Model\Hostel\HostelFeeReceipt; // Model
use App\Model\Hostel\HostelFeeReceiptDetail; // Model
use App\Model\FeesCollection\OneTime; // Model
use App\Model\FeesCollection\RecurringHead; // Model
use Redirect;

class HostelFeesReceiptController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('14',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for view fees receipt
     *  @shree on 14 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = [];
        $data = array(
            'page_title' => trans('language.view_fees_receipt'),
            'redirect_url' => url('admin-panel/hostel/view-fees-receipt'),
            'login_info' => $loginInfo,
            'listData' => $listData,
        );
        return view('admin-panel.hostel-fees-receipt.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 14 Dec 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $receipt_data   = [];
        $receipt  = HostelFeeReceipt::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('receipt_number')))
            {
                $query->where('receipt_number', "like", "%{$request->get('receipt_number')}%");
            }
            if (!empty($request) && !empty($request->has('receipt_status')))
            {
                $query->where('cancel_status', $request->get('receipt_status'));
            }
        })
        ->orderBy('hostel_fee_id', 'DESC')
        ->with('getClass')
        ->with('getStudentMapping')
        ->with('getStudent')
        ->with('getBank')
        ->get();
        foreach ($receipt as $receiptData){
            $total_fine_amount = $total_concession_amount = $pay_later_amount = 0.00;
            
            if($receiptData->concession_amount != ''){
                $total_concession_amount = $receiptData->concession_amount;
            }
            if($receiptData->fine_amount != ''){
                $total_fine_amount = $receiptData->fine_amount;
            }
            if($receiptData->pay_later_amount != ''){
                $pay_later_amount = $receiptData->pay_later_amount;
            }
            $receipt_data[] = array(
                'receipt_id' => $receiptData->hostel_fee_id,
                'class_name' => $receiptData['getClass']->class_name,
                'student_name' => $receiptData['getStudent']->student_name,
                'receipt_number' => $receiptData->receipt_number,
                'receipt_date' => date("d M Y",strtotime($receiptData->receipt_date)),
                'net_amt' => $receiptData->net_amount,
                'pay_amt' => $receiptData->total_paid_amount,
                'concession_amt' => $total_concession_amount,
                'fine_amt' => $total_fine_amount,
                'pay_later_amt' => $pay_later_amount,
                'pay_mode' => $receiptData->pay_mode,
                'cancel_status' => $receiptData->cancel_status,
            );
        }
        return Datatables::of($receipt_data)
        ->addColumn('action', function ($receipt_data)
        {
            $encrypted_receipt_id = get_encrypted_value($receipt_data['receipt_id'], true);
            if($receipt_data['cancel_status'] == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                // $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Cancel Receipt"> <i class="fas fa-minus-circle"></i> </div>';
            }
            return '<div class="pull-left"><a href= "'.url('/admin-panel/hostel/fees-receipt-status/'.$status.'/' . $encrypted_receipt_id . ' ').'"     onclick="return confirm('."'Are you sure?'".')"  >'.$statusVal.'</a></div>

            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li><a  href="'.url('/admin-panel/hostel/fee-receipt/'.$encrypted_receipt_id.'?download=pdf ').'" target="_blank" >Fees Receipt</a></li>
                </ul>
            </div>
            ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Fees receipt's status
     *  @Shree on 14 Dec 2018.
    **/
    public function changeStatus($status,$id)
    {
        $receipt_id = get_decrypted_value($id, true);
        $receipt    = HostelFeeReceipt::find($receipt_id);
        if ($receipt)  {
            $receipt->cancel_status  = $status;
            $receipt->save();

            DB::table('hostel_fee_details') 
            ->where('hostel_fee_id', $receipt_id)
            ->update([ 'cancel_status' => 1 ]);
            $success_msg = "Receipt sucessfully cancel!";
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Receipt not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function fees_receipt($receipt_id,Request $request){
        $loginInfo   = get_loggedin_user_data();
        $decrypted_receipt_id   = get_decrypted_value($receipt_id, true);
        $receiptData = HostelFeeReceipt::where('hostel_fees.hostel_fee_id','=',$decrypted_receipt_id)
        ->with('getStudentMapping','getClass')->with('getBank')
        ->with('getStudent')->with('getStudent.getParent')->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')
        ->get()->toArray();

        foreach ($receiptData as $key => $receipt) {
            $all_installments = HostelFeeReceiptDetail::where('hostel_fee_id','=',$decrypted_receipt_id)->get()->toArray();
            $installment_data = [];
            foreach($all_installments as $instKey => $installment){
                $installment['head_name'] = date('Y - F', strtotime($installment['month_year']));
                
                $installment_data[] = $installment;
               
            }
            $receiptData[0]['all_installments'] = $installment_data;
            
        }
        $receiptData              = isset($receiptData[0]) ? $receiptData[0] : [];
        // p($receiptData);
        $school              = School::first()->toArray();
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $total_amount = $wallet_amount = $total_concession_amount =  $total_calculate_fine_amount = $net_amount = $imprest_amount =   0.00;
        
        if($receiptData['total_paid_amount'] != ''){
            $total_amount = $receiptData['total_paid_amount'];
        }
       
        if($receiptData['concession_amount'] != ''){
            $total_concession_amount = $receiptData['concession_amount'];
        }
        if($receiptData['fine_amount'] != ''){
            $total_calculate_fine_amount = $receiptData['fine_amount'];
        }
        if($receiptData['net_amount'] != ''){
            $net_amount = $receiptData['net_amount'];
        }
        
        $certificate_info = array(
            'student_name' => $receiptData['get_student']['student_name'],
            'student_enroll_number' => $receiptData['get_student']['student_enroll_number'],
            'student_father_name' => $receiptData['get_student']['get_parent']['student_father_name'],
            'class' => $receiptData['get_student_academic']['get_current_class']['class_name'],
            'receipt_number' => $receiptData['receipt_number'],
            'receipt_date' => date("d M Y", strtotime($receiptData['receipt_date'])),
            'pay_mode' => $receiptData['pay_mode'],
            'total_amount' => $total_amount,
            'wallet_amount' => $wallet_amount,
            'total_concession_amount' => $total_concession_amount,
            'total_calculate_fine_amount' => $total_calculate_fine_amount,
            'net_amount' => $net_amount,
            'imprest_amount' => $imprest_amount,
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'school_mobile_number' => $school['school_mobile_number'],
            'school_email' => $school['school_email'],
            'logo' => $school['logo'],
            'all_installments' => $receiptData['all_installments'],
        );
        // p($certificate_info);
        $page_title = "Fees Receipt";
        
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info
        );
       
        // if($request->has('download')){
        //     $pdf = PDF::loadView('admin-panel.fees-receipt.fees_receipt_template')->with($data);
        //     return $pdf->stream('fee_receipt.pdf');
        // }
        return view('admin-panel.hostel-fees-receipt.fees_receipt_template')->with($data);
        
        
    }
}
