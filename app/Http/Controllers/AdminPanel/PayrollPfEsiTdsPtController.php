<?php

namespace App\Http\Controllers\AdminPanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\PfSetting; // Model
use App\Model\Payroll\TdsSetting; // Model
use App\Model\Payroll\EsiSetting; // Model
use App\Model\Payroll\PtSetting; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollPfEsiTdsPtController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add Page of PF Setting
     *  @Khushbu on 07 Feb 2019
     */
    public function addPf(Request $request, $id = NULL) {
        $pf_setting = $arr_month = $arr_year = []; 
        $loginInfo           = get_loggedin_user_data();
        $arr_month           = \Config::get('custom.month');
        $arr_year            = \Config::get('custom.pf_year');
        if(!empty($id)) {
	 		$decrypted_pf_id 	    = get_decrypted_value($id, true);
            $pf_setting      		= PfSetting::Find($decrypted_pf_id);
            $page_title             = trans('language.edit_pf');
        	$save_url    			= url('admin-panel/payroll/manage-pf-setting-save/'. $id);
            $submit_button  		= 'Update';
            $pf_setting['month']    = $arr_month;
            $pf_setting['year']     = $arr_year;
	 	} else {
            $page_title          = trans('language.add_pf');
            $save_url   	     = url('admin-panel/payroll/manage-pf-setting-save');
            $submit_button       = 'Save';
            $pf_setting['month'] = add_blank_option($arr_month, 'Select Month');
            $pf_setting['year']  = add_blank_option($arr_year, 'Select Year'); 
        }
        
        $data                = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'pf_setting'      => $pf_setting    
        );
        return view('admin-panel.payroll-pf-setting.add')->with($data);
    }

    /**
     *  Add & Update of PF Setting
     *  @Khushbu on 07 Feb 2019
     */
    public function savePf(Request $request, $id=NULL) {
        // p($request->all());
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_pf_id			= get_decrypted_value($id, true);
        $session                    = get_current_session();
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id)) {
            $pf_setting        = PfSetting::Find($decrypted_pf_id);
            if(!$pf_setting) {
                return redirect('admin-panel/payroll/manage-pf-setting')->withErrors('PF Setting not found!');
            }
            $admin_id       = $pf_setting->admin_id;
            $success_msg    = 'PF Setting updated successfully!';
        } else {
            $pf_setting    	    = New PfSetting;
            $success_msg        = 'PF Setting saved successfully!';
        }
        $pf_month = null;
        if ($request->has('pf_month')) {
            $pf_month = Input::get('pf_month');
        }
            $validator           =  Validator::make($request->all(), [
                'pf_month'       => 'required',
                'pf_year'        => 'required|unique:pf_setting,pf_year,' . $decrypted_pf_id . ',pf_setting_id,pf_month,' . $pf_month,
            ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput(Input::except("epf_a","pension_fund_b"))->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                    $pf_setting->admin_id          = $admin_id;
                    $pf_setting->update_by         = $loginInfo['admin_id'];
                    $pf_setting->session_id 	   = $session['session_id'];
                    $pf_setting->pf_month   	   = Input::get('pf_month');
                    $pf_setting->pf_year           = Input::get('pf_year');
                    $pf_setting->epf_a             = Input::get('epf_a');
                    $pf_setting->pension_fund_b    = Input::get('pension_fund_b');
                    $pf_setting->epf_a_b           = Input::get('epf_fund');
                    $pf_setting->pf_cuff_off       = Input::get('pf_cut_off');
                    $pf_setting->acc_no_02         = Input::get('acc_no_02');
                    $pf_setting->acc_no_21         = Input::get('acc_no_21');
                    $pf_setting->acc_no_22         = Input::get('acc_no_22');
                    $pf_setting->round_off         = Input::get('round_off');
                    $pf_setting->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-pf-setting')->withSuccess($success_msg);
    }

    /**
     *	Get PF Setting's Data fo view page
     *  @Khushbu on 07 Feb 2019
    **/
    public function anyDataPf(Request $request)
    {
        $loginInfo 			    = get_loggedin_user_data();
        $arr_month              = \Config::get('custom.month');
        $arr_year               = \Config::get('custom.pf_year');
        $pf_setting 		    = PfSetting::orderBy('pf_setting_id','DESC')->get();
        return Datatables::of($pf_setting, $arr_month, $arr_year)
        ->addColumn('month_year', function($pf_setting) use ($arr_month, $arr_year) {
            $pf_setting['month']    = $arr_month[$pf_setting['pf_month']];
            $pf_setting['year']     = $arr_year[$pf_setting['pf_year']];
            $month_year             = $pf_setting['month']. "-" . $pf_setting['year'];
            return $month_year;
        })
    	->addColumn('action', function($pf_setting) use($request) {
            $encrypted_pf_id  = get_encrypted_value($pf_setting->pf_setting_id, true);
    		if($pf_setting->pf_setting_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-pf-setting-status/'.$status .'/'.$encrypted_pf_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-pf-setting/'.$encrypted_pf_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-pf-setting/' . $encrypted_pf_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
        })
        ->rawColumns(['month_year' => 'month_year', 'action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-pf-setting');
    } 

    /** 
     *	Change Status of Payroll PF Setting
     *  @Khushbu on 09 Feb 2019
	**/
	public function changeStatusPf($status,$id) {
		$pf_setting_id  = get_decrypted_value($id, true);
        $pf_setting 	= PfSetting::find($pf_setting_id);
        if($pf_setting) {
            $pf_setting->pf_setting_status  = $status;
            $pf_setting->save();
            $success_msg = "PF Setting status update successfully!";
            return redirect('admin-panel/payroll/manage-pf-setting')->withSuccess($success_msg);
        } else {
            $error_message = "PF Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll PF Setting
     *  @Khushbu on 09 Feb 2019
	**/
	public function destroyPf($id) {
		$pf_setting_id  = get_decrypted_value($id, true);
        $pf_setting 	= PfSetting::find($pf_setting_id);
        if($pf_setting) {
            DB::beginTransaction();
            try
            {
                $pf_setting->delete();
                $success_msg = "PF Setting deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "PF Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add Page of TDS Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function addTds(Request $request, $id = NULL) {
        $tds_setting         =  []; 
        $loginInfo           = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_tds_id 	    = get_decrypted_value($id, true);
        	$tds_setting      		= TdsSetting::Find($decrypted_tds_id);
            $page_title             = trans('language.edit_tds');
        	$save_url    			= url('admin-panel/payroll/manage-tds-setting-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title          = trans('language.add_tds');
            $save_url   	     = url('admin-panel/payroll/manage-tds-setting-save');
            $submit_button       = 'Save';
	 	}
        $data                = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'tds_setting'     => $tds_setting    
        );
        return view('admin-panel.payroll-tds-setting.add')->with($data);
    }

    /**
     *  Add & Update of TDS Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function saveTds(Request $request, $id=NULL) {
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_tds_id			= get_decrypted_value($id, true);
        $session                    = get_current_session();
        $admin_id                   = $loginInfo['admin_id'];
        // $salary  =  TdsSetting::whereBetween('salary_max', [$request->get('salary_range_min'), $request->get('salary_range_max')])->orWhereBetween('salary_min', [$request->get('salary_range_min'), $request->get('salary_range_max')])->get()->count();
        $salary  =  TdsSetting::where([['salary_min','<=', [$request->get('salary_range_min')]],['salary_max','>=', [$request->get('salary_range_min')]]])->orWhere([['salary_min','<=', [$request->get('salary_range_max')]],['salary_max','>=', [$request->get('salary_range_max')]]])->count();
        // p($salary);
        if($salary != 0) {
            return redirect('admin-panel/payroll/manage-tds-setting')->withErrors('Please enter correct salary range!');
        }
        if(!empty($id)) {
            $tds_setting        = TdsSetting::Find($decrypted_tds_id);
            if(!$tds_setting) {
                return redirect('admin-panel/payroll/manage-tds-setting')->withErrors('TDS Setting not found!');
            }
            $admin_id       = $tds_setting->admin_id;
            $success_msg    = 'TDS Setting updated successfully!';
        } else {
            $tds_setting    	= New TdsSetting;
            $success_msg        = 'TDS Setting saved successfully!';
        }
        $salary_range_min = null;
        if ($request->has('salary_range_min')) {
            $salary_range_min = Input::get('salary_range_min');
        }
            $validator           =  Validator::make($request->all(), [
                'salary_range_min'      => 'required',
                'salary_range_max'      => 'required|unique:tds_setting,salary_max,' . $decrypted_tds_id . ',tds_setting_id,salary_min,' . $salary_range_min,
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $tds_setting->admin_id         = $admin_id;
                $tds_setting->update_by        = $loginInfo['admin_id'];
                $tds_setting->session_id 	   = $session['session_id'];
                $tds_setting->salary_min   	   = Input::get('salary_range_min');
                $tds_setting->salary_max       = Input::get('salary_range_max');
                $tds_setting->salary_deduction = Input::get('sal_deduction');
                $tds_setting->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-tds-setting')->withSuccess($success_msg);
    }

    /**
     *	Get TDS Setting's Data fo view page
     *  @Khushbu on 08 Feb 2019
    **/
    public function anyDataTds(Request $request)
    {
        $loginInfo 			    = get_loggedin_user_data();
        $tds_setting 		    = TdsSetting::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_deduction') !=  NULL)
            {
                $query->where('salary_deduction', $request->get('s_deduction'));
            }
        })->orderBy('tds_setting_id','DESC')->get();
        return Datatables::of($tds_setting)
        ->addColumn('salary_range', function($tds_setting) {
            $salary_range = $tds_setting['salary_min']. "-" . $tds_setting['salary_max'];
            return $salary_range;
        })
    	->addColumn('action', function($tds_setting) use($request) {
            $encrypted_tds_id  = get_encrypted_value($tds_setting->tds_setting_id, true);
    		if($tds_setting->tds_setting_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-tds-setting-status/'.$status .'/'.$encrypted_tds_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-tds-setting/'.$encrypted_tds_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-tds-setting/' . $encrypted_tds_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
        })
        ->rawColumns(['salary_range' => 'salary_range', 'action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-tds-setting');
    } 

    /** 
     *	Change Status of Payroll TDS Setting
     *  @Khushbu on 08 Feb 2019
	**/
	public function changeStatusTds($status,$id) {
		$tds_setting_id = get_decrypted_value($id, true);
        $tds_setting 	= TdsSetting::find($tds_setting_id);
        if($tds_setting) {
            $tds_setting->tds_setting_status  = $status;
            $tds_setting->save();
            $success_msg = "TDS Setting status update successfully!";
            return redirect('admin-panel/payroll/manage-tds-setting')->withSuccess($success_msg);
        } else {
            $error_message = "TDS Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll TDS Setting
     *  @Khushbu on 08 Feb 2019
	**/
	public function destroyTds($id) {
		$tds_setting_id = get_decrypted_value($id, true);
        $tds_setting 	= TdsSetting::find($tds_setting_id);
        if ($tds_setting) {
            DB::beginTransaction();
            try
            {
                $tds_setting->delete();
                $success_msg = "TDS Setting deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "TDS Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add Page of ESI Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function addEsi(Request $request, $id = NULL) {
        $esi_setting         =  []; 
        $loginInfo           = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_esi_id 	    = get_decrypted_value($id, true);
        	$esi_setting      		= EsiSetting::Find($decrypted_esi_id);
            $page_title             = trans('language.edit_esi');
        	$save_url    			= url('admin-panel/payroll/manage-esi-setting-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title          = trans('language.add_esi');
            $save_url   	     = url('admin-panel/payroll/manage-esi-setting-save');
            $submit_button       = 'Save';
	 	}
        $data                = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'esi_setting'     => $esi_setting    
        );
        return view('admin-panel.payroll-esi-setting.add')->with($data);
    }

    /**
     *  Add & Update of ESI Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function saveEsi(Request $request, $id=NULL) {
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_esi_id			= get_decrypted_value($id, true);
        $session                    = get_current_session();
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id)) {
            $esi_setting        = EsiSetting::Find($decrypted_esi_id);
            if(!$esi_setting) {
                return redirect('admin-panel/payroll/manage-esi-setting')->withErrors('ESI Setting not found!');
            }
            $admin_id       = $esi_setting->admin_id;
            $success_msg    = 'ESI Setting updated successfully!';
        } else {
            $esi_setting    	= New EsiSetting;
            $success_msg        = 'ESI Setting saved successfully!';
        }
            $validator           =  Validator::make($request->all(), [
                'employee'      => 'required',
                'employer'      => 'required',
                'esi_cut_off'   => 'required',    
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $esi_setting->admin_id         = $admin_id;
                $esi_setting->update_by        = $loginInfo['admin_id'];
                $esi_setting->session_id 	   = $session['session_id'];
                $esi_setting->employee   	   = Input::get('employee');
                $esi_setting->employer         = Input::get('employer');
                $esi_setting->esi_cut_off      = Input::get('esi_cut_off');
                $esi_setting->round_off        = Input::get('round_off');
                $esi_setting->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-esi-setting')->withSuccess($success_msg);
    }

    /**
     *	Get ESI Setting's Data fo view page
     *  @Khushbu on 08 Feb 2019
    **/
    public function anyDataEsi(Request $request)
    {
        $loginInfo 			    = get_loggedin_user_data();
        $esi_setting 		    = EsiSetting::orderBy('esi_setting_id','DESC')->get();
        return Datatables::of($esi_setting)
    	->addColumn('action', function($esi_setting) use($request) {
            $encrypted_esi_id  = get_encrypted_value($esi_setting->esi_setting_id, true);
    		if($esi_setting->esi_setting_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-esi-setting-status/'.$status .'/'.$encrypted_esi_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-esi-setting/'.$encrypted_esi_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-esi-setting/' . $encrypted_esi_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
        })
        ->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-esi-setting');
    } 

    /** 
     *	Change Status of Payroll Esi Setting
     *  @Khushbu on 08 Feb 2019
	**/
	public function changeStatusEsi($status,$id) {
		$esi_setting_id = get_decrypted_value($id, true);
        $esi_setting 	= EsiSetting::find($esi_setting_id);
        if($esi_setting) {
            $esi_setting->esi_setting_status  = $status;
            $esi_setting->save();
            $success_msg = "ESI Setting status update successfully!";
            return redirect('admin-panel/payroll/manage-esi-setting')->withSuccess($success_msg);
        } else {
            $error_message = "ESI Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll ESI Setting
     *  @Khushbu on 08 Feb 2019
	**/
	public function destroyEsi($id) {
		$esi_setting_id = get_decrypted_value($id, true);
        $esi_setting 	= EsiSetting::find($esi_setting_id);
        if ($esi_setting) {
            DB::beginTransaction();
            try
            {
                $esi_setting->delete();
                $success_msg = "ESI Setting deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "ESI Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add Page of PT Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function addPt(Request $request, $id = NULL) {
        $pt_setting          =  []; 
        $loginInfo           = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_pt_id 	    = get_decrypted_value($id, true);
            $pt_setting      		= PtSetting::Find($decrypted_pt_id);
            $page_title             = trans('language.edit_pt');
        	$save_url    			= url('admin-panel/payroll/manage-pt-setting-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title          = trans('language.add_pt');
            $save_url   	     = url('admin-panel/payroll/manage-pt-setting-save');
            $submit_button       = 'Save';
	 	}
        $data                = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'pt_setting'      => $pt_setting    
        );
        return view('admin-panel.payroll-pt-setting.add')->with($data);
    }

    /**
     *  Add & Update of PT Setting
     *  @Khushbu on 08 Feb 2019
     */
    public function savePt(Request $request, $id=NULL) {
        $loginInfo      	= get_loggedin_user_data();
        $decrypted_pt_id    = get_decrypted_value($id, true);
        $session            = get_current_session();
        $admin_id           = $loginInfo['admin_id'];
        // $salary             =  PtSetting::whereBetween('salary_max', [$request->get('salary_range_min'), $request->get('salary_range_max')])->orWhereBetween('salary_min', [$request->get('salary_range_min'), $request->get('salary_range_max')])->get()->count();
        $salary  =  PtSetting::where([['salary_min','<=', [$request->get('salary_range_min')]],['salary_max','>=', [$request->get('salary_range_min')]]])->orWhere([['salary_min','<=', [$request->get('salary_range_max')]],['salary_max','>=', [$request->get('salary_range_max')]]])->count();
        if($salary != 0) {
            return redirect('admin-panel/payroll/manage-pt-setting')->withErrors('Please enter correct salary range!');
        }
        if(!empty($id)) {
            $pt_setting        = PtSetting::Find($decrypted_pt_id);
            if(!$pt_setting) {
                return redirect('admin-panel/payroll/manage-pt-setting')->withErrors('PT Setting not found!');
            }
            $admin_id       = $pt_setting->admin_id;
            $success_msg    = 'PT Setting updated successfully!';
        } else {
            $pt_setting    	    = New PtSetting;
            $success_msg        = 'PT Setting saved successfully!';
        }
        $salary_range_min = null;
        if ($request->has('salary_range_min')) {
            $salary_range_min = Input::get('salary_range_min');
        }
            $validator           =  Validator::make($request->all(), [
                'salary_range_min'      => 'required',
                'salary_range_max'      => 'required|unique:pt_setting,salary_max,' . $decrypted_pt_id . ',pt_setting_id,salary_min,' . $salary_range_min, 
                'month'                 => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $pt_setting->admin_id         = $admin_id;
                $pt_setting->update_by        = $loginInfo['admin_id'];
                $pt_setting->session_id 	  = $session['session_id'];
                $pt_setting->salary_min   	  = Input::get('salary_range_min');
                $pt_setting->salary_max       = Input::get('salary_range_max');
                $pt_setting->monthly_amount   = json_encode(Input::get('month'));
                $pt_setting->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-pt-setting')->withSuccess($success_msg);
    }

    /**
     *	Get PT Setting's Data fo view page
     *  @Khushbu on 08 Feb 2019
    **/
    public function anyDataPt(Request $request)
    {
        $loginInfo 			    = get_loggedin_user_data();
        $pt_setting 		    = PtSetting::orderBy('pt_setting_id','DESC')->get();
        return Datatables::of($pt_setting)
        ->addColumn('salary_range', function($pt_setting) {
            $salary_range = $pt_setting['salary_min']. "-" . $pt_setting['salary_max'];
            return $salary_range;
        })
        ->addColumn('monthly_amount', function($pt_setting) {
            $monthly_amount = '<button class="btn btn-raised btn-primary monthly" data-toggle="modal" data-target="#MonthlyAmountModel" pt_setting_id= '.$pt_setting->pt_setting_id.'>Monthly Amount</button>';
            return $monthly_amount;
        })
    	->addColumn('action', function($pt_setting) use($request) {
            $encrypted_pt_id  = get_encrypted_value($pt_setting->pt_setting_id, true);
    		if($pt_setting->pt_setting_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon m_top_button" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-pt-setting-status/'.$status .'/'.$encrypted_pt_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-pt-setting/'.$encrypted_pt_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-pt-setting/' . $encrypted_pt_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
        })
        ->rawColumns(['salary_range' => 'salary_range', 'monthly_amount' => 'monthly_amount','action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-pt-setting');
    } 

    /** 
     *	Change Status of Payroll PT Setting
     *  @Khushbu on 09 Feb 2019
	**/
	public function changeStatusPt($status,$id) {
		$pt_setting_id  = get_decrypted_value($id, true);
        $pt_setting 	= PtSetting::find($pt_setting_id);
        if($pt_setting) {
            $pt_setting->pt_setting_status  = $status;
            $pt_setting->save();
            $success_msg = "PT Setting status update successfully!";
            return redirect('admin-panel/payroll/manage-pt-setting')->withSuccess($success_msg);
        } else {
            $error_message = "PT Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll PT Setting
     *  @Khushbu on 09 Feb 2019
	**/
	public function destroyPt($id) {
		$pt_setting_id  = get_decrypted_value($id, true);
        $pt_setting 	= PtSetting::find($pt_setting_id);
        if ($pt_setting) {
            DB::beginTransaction();
            try
            {
                $pt_setting->delete();
                $success_msg = "PT Setting deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "PT Setting not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /** 
     *  Get Pt Setting Data
     *  @Khushbu on 09 Feb 2019
    **/
    public function getPtSettingData(Request $request) {
        $pt_setting = PtSetting::where('pt_setting_id',$request->get('pt_setting_id'))->get();
        return json_encode($pt_setting[0]);
    } 
}
