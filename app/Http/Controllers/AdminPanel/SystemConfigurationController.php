<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemConfiguration\SystemConfiguration;
use App\Admin;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class SystemConfigurationController extends Controller
{
    /**
     * Add Page of System Configuration
     * @Khushbu on 01 Apr 2019.
    **/
    public function add()
    {
        $system_config  =  $data = [];
        $loginInfo      = get_loggedin_user_data();
            $system_config              = SystemConfiguration::get()->toArray();
            $system_config              = isset($system_config[0]) ? $system_config[0] : [];
            if (!$system_config)
            {
                return redirect('admin-panel/system-configuration/add-system-configuration')->withError('System Configuration not found!');
            }
            $encrypted_system_config_id = get_encrypted_value($system_config['system_config_id'], true);
            $page_title           = trans('language.edit_system_config');
            $save_url             = url('admin-panel/system-configuration/add-system-configuration/save/' . $encrypted_system_config_id);
            $submit_button        = 'Update';
        
        $data = array(
            'page_title'    => $page_title,
            'redirect_url'  => url('admin-panel/system-configuration/add-system-configuration'),
            'save_url'      => $save_url,
            'submit_button' => $submit_button       ,
            'login_info'    => $loginInfo,
            'system_config' => $system_config 
        );
        return view('admin-panel.system-configuration.add')->with($data);
    }

    /**
     * Add & Update of System Configuration
     * @Khushbu on 01 Apr 2019.
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_system_config_id = null;
        $loginInfo      = get_loggedin_user_data();
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_system_config_id = get_decrypted_value($id, true);

            $system_config       = SystemConfiguration::find($decrypted_system_config_id);
            if (!$system_config)
            {
                return redirect('/admin-panel/system-configuration/add-system-configuration/')->withError('System Configuration not found!');
            }
            $success_msg = 'System Configuration updated successfully!';
        }
        else
        {
            $system_config   = New SystemConfiguration;
            $success_msg    = 'System Configuration saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'config_email'      => 'required',
                'config_password'   => 'required',
                'sms_gateway_url'   => 'required',
                'sms_username'      => 'required',
                'sms_password'      => 'required',
                'sms_sender'        => 'required',
                'sms_priority'      => 'required',
                'sms_stype'         => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try
            {   
                // System Configuration table data
                $system_config->update_by         = $admin_id;
                $system_config->email             = Input::get('config_email');
                $system_config->password          = Input::get('config_password');
                $system_config->sms_gateway_url   = Input::get('sms_gateway_url');
                $system_config->sms_username      = Input::get('sms_username');
                $system_config->sms_password      = Input::get('sms_password');
                $system_config->sms_sender        = Input::get('sms_sender');
                $system_config->sms_priority      = Input::get('sms_priority');
                $system_config->sms_stype         = Input::get('sms_stype');
                $system_config->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
    }
}
