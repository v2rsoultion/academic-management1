<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Purchase\Purchase; // Model
use App\Model\Purchase\PurchaseItems; // Model
use App\Model\Sale\Sale; // Model
use App\Model\Sale\SaleItems; // Model
use App\Model\InvoiceConfig\InvoiceConfig; // Model
use App\Model\InventoryItem\Items; // Model
use App\Model\School\School; // Model
use Yajra\Datatables\Datatables;
use PDF;
use Redirect;

class SaleController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Sale
     * @Sandeep on 1 March 2019.
    **/
    public function add(Request $request, $id = NULL) 
    {

    	$sale = $arr_category = $arr_vendor = $invoice_type = $arr_subcategory_data = $arr_items = $sale_item = [];
        $loginInfo     		  = get_loggedin_user_data();
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        $arr_sale_type        = \Config::get('custom.task_type');
        $arr_staff            = get_all_staffs();
        $arr_class            = get_all_classes_mediums();
        $arr_staff_roles      = get_all_staff_roles();
        
        if (!empty($id)) 
        {
            $decrypted_sale_id       = get_decrypted_value($id, true);
            $sale                    = Sale::Find($decrypted_sale_id);
            $sale_item               = SaleItems::where('sale_id', $sale->sale_id)->where('sales_type',1)->with('items', 'units','category','subCategory')->get();
            $sale['tax_amount']      = ($sale['total_amount']*$sale['tax'])/100;
            $sale['add_tax_amount']  = $sale['total_amount']+$sale['tax_amount'];
            $sale['discount_amount'] = ($sale['add_tax_amount']*$sale['discount'])/100;
            $sale['round_off']       = number_format($sale['net_amount']-$sale['gross_amount'],2);
            $sale['payment_mode_data']    = $arr_inv_payment_mode;

            $arr_section            = get_class_section($sale->class_id);
            $arr_student            = get_student_list_class_section($sale->class_id,$sale->section_id);
            // p($sale_item);
            if (!$sale)
            {
                return redirect('admin-panel/inventory/sale/add-sale')->withError('Sale not found!');
            }
            $page_title                 = trans('language.edit_sale');
            $save_url                   = url('admin-panel/inventory/sale/save/' . $id);
            $submit_button              = 'Update';
            $arr_items                  = get_all_items($sale['subcategory_id']);
            $sale['items']          = add_blank_option($arr_items, 'Select Item');
        }
        else {
        $invoice_type             = InvoiceConfig::where('invoice_type', 3)->get()->toArray();
        $invoice_format = $invoice_type[0]['invoice_prefix'];
        if($invoice_type[0]['year'] == 1 ? $invoice_format .= date('Y') : $invoice_format)
        if($invoice_type[0]['month'] == 1 ? $invoice_format .= date('m') : $invoice_format)
        if($invoice_type[0]['day'] == 1 ? $invoice_format .= date('d') : $invoice_format) 
        $invoice_format .= $invoice_type[0]['invoice_config_id'];
        $invoice_format .= $invoice_type[0]['total_invoice']+1;
        $sale['system_invoice_no'] = $invoice_format;
        $page_title                = trans('language.add_sale');
        $save_url                  = url('admin-panel/inventory/sale/save');
        $submit_button             = 'Save';
        $arr_items                 = get_all_items($sale['sub_category_id']);
        $sale['items']             = add_blank_option($arr_items, 'Select Item');
        $arr_section               = [];
        $arr_student               = [];
        $sale['payment_mode_data'] = add_blank_option($arr_inv_payment_mode, 'Select Payment Mode');
        }
        
        $arr_category             = get_all_category(1);
        $sale['category']         = add_blank_option($arr_category, 'Select Category');
        $arr_vendor               = get_all_vendors();
        $sale['vendor']           = add_blank_option($arr_vendor, 'Select Vendor');
        $arr_subcategory_data     = get_sub_categories($sale['category_id']);
        $sale['arr_subcategory_data']  = add_blank_option($arr_subcategory_data, 'Select Sub Category');
        
        $sale['arr_sale_type']    = $arr_sale_type;
        $arr_bank                 = get_all_bank();
        $sale['bank']             = add_blank_option($arr_bank, 'Select Bank Name'); 

        $sale['arr_class']          = add_blank_option($arr_class, 'Select Class');
        $sale['arr_section']        = add_blank_option($arr_section, 'Select Section');
        $sale['arr_student']        = add_blank_option($arr_student, 'Select Student');
        $sale['arr_staff']          = add_blank_option($arr_staff, 'Select Staff'); 
        $sale['arr_staff_roles']    = add_blank_option($arr_staff_roles, 'Select Staff Role'); 
    
    	$data                     =  array(
            'page_title'          => $page_title,
            'login_info'          => $loginInfo,
            'save_url'            => $save_url,  
            'sale'		      => $sale,	
            'submit_button'	      => $submit_button,
            'sale_item'       => $sale_item,  
        );
    	return view('admin-panel.inventory-sale.add')->with($data);
    } 

    /** 
     *  Add & Update Sale
     *  @Sandeep on 1 March 2019.
    **/
    public function save(Request $request, $id = NULL) {

        $loginInfo                  = get_loggedin_user_data();
        $decrypted_sale_id          = get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];

        if(empty($request->get('items')))
        {
            return redirect('admin-panel/inventory/sale/add-sale')->withErrors('Atleast one item should be inserted!');
        }
        if(!empty($id))
        {
            $sale        = Sale::Find($decrypted_sale_id);
            if(!$sale) {
                return redirect('admin-panel/inventory/sale/add-sale')->withErrors('Sale not found!');
            }

            $admin_id    = $sale->admin_id;
            $success_msg = 'Sale updated successfully!';
        }
        else
        {
            $sale        = New Sale;
            $success_msg     = 'Sale saved successfully!';
        }

        $validator              =  Validator::make($request->all(), [
            'ref_invoice_no'    => 'required|unique:sales,ref_invoice_no,' . $decrypted_sale_id .',sale_id',
            'invoice_date'      => 'required'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $sale->admin_id          = $admin_id;
                $sale->update_by         = $loginInfo['admin_id'];
                $sale->system_invoice_no = Input::get('system_invoice_no');
                $sale->ref_invoice_no    = Input::get('ref_invoice_no');
                $sale->date              = Input::get('invoice_date');
                $sale->sale_type         = Input::get('sale_type');
                $sale->class_id          = Input::get('class_id');
                $sale->section_id        = Input::get('section_id');
                $sale->student_id        = Input::get('student_id');
                $sale->role_id           = Input::get('role_id');
                $sale->staff_id          = Input::get('staff_id');
                $sale->payment_mode      = Input::get('payment_mode');
                $sale->bank_id           = Input::get('bank_id');
                $sale->cheque_no         = Input::get('cheque_no');
                $sale->cheque_date       = Input::get('cheque_date');
                $sale->cheque_amount     = Input::get('cheque_amount');
                $sale->total_amount      = Input::get('total_amount');
                $sale->tax               = Input::get('tax');
                $sale->discount          = Input::get('discount');
                $sale->gross_amount      = Input::get('gross_amount');
                $sale->net_amount        = Input::get('net_amount');
                $sale->save();

                if(!empty($request->get('items'))) {
                    foreach($request->get('items') as $items) {
                        
                        $getItemData     = Items::find($items['item_id']);
                        if(isset($items['sale_items_id'])) {
                            $sale_item   = SaleItems::find($items['sale_items_id']);
                        } else {
                            $sale_item = new SaleItems;
                            $getItemData->available_quantity = $getItemData->available_quantity - $items['quantity'];
                        }
                            $sale_item->admin_id        = $admin_id;
                            $sale_item->update_by       = $loginInfo['admin_id'];
                            $sale_item->sale_id         = $sale->sale_id;
                            $sale_item->item_id         = $items['item_id'];
                            $sale_item->category_id     = $items['category_id'];
                            $sale_item->sub_category_id = $items['sub_category_id'];
                            $sale_item->sales_type       = 1;
                            $sale_item->unit_id         = $items['unit_id'];
                            $sale_item->rate            = $items['rate'];
                            $sale_item->quantity        = $items['quantity'];
                            $sale_item->amount          = $items['amount'];
                            $sale_item->save();
                            
                            $getItemData->save();
                    }
                }    

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/sale/view-sale')->withSuccess($success_msg);  
    }

    /**
     *  View Page of sale
     *  @Sandeep on 1 March 2019.
    **/
    public function index() {
        $loginInfo             = get_loggedin_user_data();
        $arr_vendor    = $sale =  $arr_category = [];
        $arr_vendor            = get_all_vendors();
        $arr_sale_type         = \Config::get('custom.task_type');

        $sale['vendor']        = add_blank_option($arr_vendor, 'Select Vendor');
        $arr_category          = get_all_category(1);
        $sale['category']      = add_blank_option($arr_category, 'Select Category');
        $sale['arr_sale_type'] = $arr_sale_type;
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/inventory/sale/view-sale'),
            'page_title'    => trans('language.view_sale'),
            'sale'          => $sale    
        );
        
        return view('admin-panel.inventory-sale.index')->with($data);
    }  

    /** 
     *  Get sale's Data for view page(Datatables)
     *  @Sandeep on 1 March 2019.
    **/
    public function anyData(Request $request) {
        $loginInfo     = get_loggedin_user_data();
        $sale          = Sale::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('s_system_invoice_no')))
            {
                $query->where('system_invoice_no', 'Like', $request->get('s_system_invoice_no').'%');
            }
            if (!empty($request) && !empty($request->get('s_ref_invoice_no')))
            {
                $query->where('ref_invoice_no', 'Like', $request->get('s_ref_invoice_no').'%');
            }
            if (!empty($request) && !empty($request->get('s_invoice_date')))
            {
                $query->where('date', '=', $request->get('s_invoice_date'));
            }
            if (!empty($request) && !empty($request->get('sale_type')))
            {
                $query->where('sale_type', '=', $request->get('sale_type'));
            }
            if (!empty($request) && !empty($request->get('s_category_id')))
            {
                $query->where('category_id', '=', $request->get('s_category_id'));
            }
        })->orderBy('sale_id','DESC')->get();
        // p($sale);
        return Datatables::of($sale)
        ->addColumn('calculation_amount', function($sale) use($request) {
            $round_off = number_format($sale['net_amount'] - $sale['gross_amount'],2);
            return '
                <div>
                <b>Total Amount: </b>₹ ' .$sale['total_amount']. '   <br>
                <b>Tax: </b> ' .$sale['tax']. '% <br>
                <b>Discount: </b> ' .$sale['discount']. '% <br>
                <b>Round off: </b> ' .$round_off. ' <br>
                <b>Net Amount: </b>₹ ' .$sale['net_amount']. ' 
                </div>';
        })
        ->addColumn('view_details', function($sale) use($request) {
            $modal_details = '<button class="btn btn-raised btn-primary sale" data-toggle="modal" data-target="#MoreDetailsModel" sale_id= '.$sale->sale_id.'>More Details</button>';
            return $modal_details;
        })
        ->addColumn('sale_type', function($sale) use($request) {
            if($sale->sale_type == 1){
                return "Student";
            } else {
                return "Staff";
            }
            
        })
        ->addColumn('action', function ($sale) use($request)
        {
            $encrypted_sale_id = get_encrypted_value($sale->sale_id, true);
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-label"></i>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li><a title="Item Details" href="'.url('/admin-panel/inventory/sale/view-sale/item-details/' . $encrypted_sale_id. ' ').'">Item Details</a></li> 
                            <li><a target="new_blank" title="View Invoice" href="'.route('salespdfview',['download'=>'pdf','id' =>$sale->sale_id]).'">View Invoice</a></li> 
                        </ul> 
                </div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/inventory/sale/add-sale/' . $encrypted_sale_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/sale/delete-sale/' . $encrypted_sale_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>';
        })->rawColumns(['action' => 'action', 'view_details' => 'view_details', 'calculation_amount' => 'calculation_amount'])->addIndexColumn()
        ->make(true);
    } 

    /** 
     *  Destroy sale's data
     *  @Sandeep on 1 march 2019.
     **/
    public function destroy($id) {
        $sale_id        = get_decrypted_value($id, true);
        $sale_item      = SaleItems::where('sale_id',$sale_id)->get();
        $sale           = Sale::find($sale_id);

        if ($sale)
        {
            DB::beginTransaction();
            try
            {
                foreach ($sale_item as $key => $value) {

                    $sale_items         = SaleItems::find($value['sale_items_id']);
                    $getItemData        = Items::find($sale_items['item_id']);

                    if($sale_items['sales_type'] == 1){
                        $getItemData->available_quantity = $getItemData->available_quantity + $sale_items['quantity'];
                    } else {
                        $getItemData->available_quantity = $getItemData->available_quantity - $sale_items['quantity'];  
                    }
                    $getItemData->save();
                    
                    $sale_item[$key]->delete();
                }
                $sale->delete();
                $success_msg = "Sale deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Sale not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /** 
     *  Get sale Items Details Data 
     *  @Sandeep on 1 March 2019.
    **/
    public function itemDetails($id) {
        $sale               = [];
        $sale_id            = get_decrypted_value($id, true);
        $sale               = Sale::where('sale_id',$sale_id)->with('getStudent','getStaff','getSiblingClass','getSections')->get();
        $arr_all_items      = get_all_items();
        $sale['all_items']  = add_blank_option($arr_all_items, 'Select Item');
        // p($sale);
        $data               = array(
            'page_title'    => trans('language.view_item_details'),
            'sale'          => $sale,
            'sale_id'       => $sale_id,
        );
        return view('admin-panel.inventory-sale.view-item-details')->with($data);
    } 

    /** 
     *  Get sale Item's Data for view page(Datatables)
     *  @Sandeep on 1 March 2019.
    **/
    public function anyDataItemDetails(Request $request) {
        $loginInfo       = get_loggedin_user_data();
        $sale_item   = SaleItems::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('s_item_name')))
            {
                $query->where('item_id', '=', $request->get('s_item_name'));
            }
        })
        ->where('sale_id',$request->get('sale_id'))
        ->orderBy('sale_items_id')->with('items', 'units','category','subCategory')->get();

        //p($sale_item);
        return Datatables::of($sale_item)
        ->addColumn('item_name', function($sale_item) {
            return $sale_item['items']->item_name;
        })
        ->addColumn('category_name', function($sale_item) {
            return $sale_item['category']->category_name;
        })
        ->addColumn('sub_category_name', function($sale_item) {
            return $sale_item['subCategory']->category_name;
        })
        ->addColumn('unit_name', function($sale_item) {
            return $sale_item['units']->unit_name;
        })
        ->addColumn('sale_type', function($sale_item) {
            if($sale_item->sales_type == 1){
                return "Sales";
            } else {
                return "Sales Return";
            }  
        })

        ->rawColumns(['item_name' => 'item_name', 'unit_name' => 'unit_name', 'action' => 'action'])->addIndexColumn()
        ->make(true);
    } 

    /** 
     *  Get Sale Data
     *  @Khushbu on 29 Jan 2019.
    **/
    public function getSaleData(Request $request) {
        $sale        = Sale::where('sale_id',$request->get('sale_id'))->with('getStudent','getStaff','getSiblingClass','getSections','bank')->get();
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        $sale[0]['payment_mode'] = $arr_inv_payment_mode[$sale[0]['payment_mode']];
        // p($sale);
        $data = compact('sale');
        //return json_encode($sale[0]);

        return view('admin-panel.inventory-sale.ajax_sale_details', $data);
    } 
    /**
     *  View PDF Invoice Data
     *  @Sandeep on 01 march 2019.
    **/
    public function salespdfview(Request $request)
    {
        $invoice_details = $item_details = [];
        $school = School::first()->toArray();
        $logo = check_file_exist($school['school_logo'], 'school_logo');
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        $sale = Sale::find($request->get(id));
        $sale['payment_mode']= $arr_inv_payment_mode[$sale['payment_mode']];
        // p($sale);
            $invoice_details= array(
                'sys_invoice_no' => $sale->system_invoice_no,
                'invoice_no'     => $sale->ref_invoice_no,
                'date'           => $sale->date,
                'payment_mode'   => $sale->payment_mode,
            );
        $sale_items = SaleItems::where('sale_id',$sale->sale_id)->where('sales_type',1)->with('items')->get()->toArray();
        if($request->has('download')){
            $pdf = PDF::loadView('admin-panel/inventory-sale/pdfview', compact('school', 'logo' , 'invoice_details', 'sale_items', 'sale'));
            return $pdf->stream('pdfview.pdf');
        }
        
        return view('admin-panel.inventory-sale.pdfview', $data);
    }
    /**
     *  Delete sale Item  Data
     *  @Sandeep on 01 march 2019.
    **/
    public function saleItemDelete(Request $request) {

        $sale_items     = SaleItems::find($request->get(p_item_id));
        $getItemData    = Items::find($sale_items['item_id']);

        $getItemData->available_quantity = $getItemData->available_quantity + $sale_items['quantity'];    

        if(!empty($sale_items)) {
            $getItemData->save();
            $sale_items->delete();
        }
    }

    /**
     *  View Page of Sale Return
     *  @Sandeep on 1 march 2019.
    **/
    public function manage_sale_return() {

        $sale = $arr_vendor  = $invoice_type = [];

        $loginInfo       = get_loggedin_user_data();
        $invoice_type    = InvoiceConfig::where('invoice_type', 2)->get()->toArray();
        $invoice_format  = $invoice_type[0]['invoice_prefix'];
        
        if($invoice_type[0]['year'] == 1 ? $invoice_format .= date('Y') : $invoice_format)
        if($invoice_type[0]['month'] == 1 ? $invoice_format .= date('m') : $invoice_format)
        if($invoice_type[0]['day'] == 1 ? $invoice_format .= date('d') : $invoice_format) 
        
        $invoice_format .= $invoice_type[0]['invoice_config_id'];
        $invoice_format .= $invoice_type[0]['total_invoice']+1;
        $sale['system_invoice_no'] = $invoice_format;

        $arr_vendor            = get_all_vendors();
        $sale['vendor']    = add_blank_option($arr_vendor, 'Select Vendor');

        $data = array(
            'login_info'    => $loginInfo,
            'save_url'      => url('admin-panel/inventory/sale-return/save'),
            'page_title'    => trans('language.manage_sale_return'),
            'sale'      => $sale    
        );
        
        return view('admin-panel.inventory-sale.manage_sale_return')->with($data);
    }


    /** 
     *  Get sale Return Data
     *  @Sandeep on 1 march 2019.
    **/
    public function get_sale_return(Request $request) {

        $sale  = Sale::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('invoice_no')))
            {
                $query->where('system_invoice_no', '=', $request->get('invoice_no'));
            }
            if (!empty($request) && !empty($request->get('date')))
            {
                $query->where('date', '=', $request->get('date'));
            }
            if (!empty($request) && !empty($request->get('vendor_id')))
            {
                $query->where('vendor_id', '=', $request->get('vendor_id'));
            }
        })

        ->join('sale_items', function($join) use ($request){
            $join->on('sale_items.sale_id', '=', 'sales.sale_id');
            $join->where('sales_type', 1);
        })->groupBy('item_id')->orderBy('sales.sale_id','DESC')->get();

        foreach ($sale as $value) {

            $getItemData        = Items::find($value['item_id']);

            $get_sale_items = SaleItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'sale_id' => $value['sale_id'],'sales_type' => 1])->sum('quantity');

                $sale_return_data[]  = array(
                    'sale_items_id'        => $value->sale_items_id,
                    'sale_id'              => $value->sale_id,
                    'item_id'              => $value->item_id,
                    'unit_id'              => $value->unit_id,
                    'category_id'          => $value->category_id,
                    'sub_category_id'      => $value->sub_category_id,
                    'sale_items_name'      => $getItemData->item_name,
                    'quantity'             => $get_sale_items,
                    'available_quantity'   => $getItemData->available_quantity,
                    'rate'                 => $value->rate,
                );     
            
        }

        //p($sale_return_data);
        $data = compact('sale_return_data');
        return view('admin-panel.inventory-sale.ajax_get_sale_return', $data);
    }   


    /** 
     *  Add sale Return
     *  @Sandeep on 1 March 2019.
    **/
    public function sale_return_save(Request $request) {

        $loginInfo       = get_loggedin_user_data();
        $admin_id        = $loginInfo['admin_id'];
       
        DB::beginTransaction();
        try
        {
            
            foreach($request->get('sale_return') as $items) {
                
                if(!empty($items['return_quantity'])){

                    $getItemData     = Items::find($items['item_id']);
                    $sale_item   = new SaleItems;
                    
                    $sale_item->admin_id            = $admin_id;
                    $sale_item->update_by           = $loginInfo['admin_id'];
                    $sale_item->sale_id             = $items['sale_id'];
                    $sale_item->item_id             = $items['item_id'];
                    $sale_item->category_id         = $items['category_id'];
                    $sale_item->sub_category_id     = $items['sub_category_id'];
                    $sale_item->unit_id             = $items['unit_id'];
                    $sale_item->sales_type          = 2;
                    $sale_item->rate                = $items['rate'];
                    $sale_item->quantity            = $items['return_quantity'];
                    $sale_item->amount              = $items['amount'];
                    $sale_item->save();
                    
                    $getItemData->available_quantity = $getItemData->available_quantity + $items['return_quantity'];
                    $getItemData->save();
                }
            }
            $success_msg     = 'Sale return successfully!';

        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        
        return redirect('admin-panel/inventory/manage-sales-return')->withSuccess($success_msg);  
    }



    /**
     *  Manage  Stock Register
     *  @Sandeep on 04 march 2019.
    **/
    public function manage_stock_register(Request $request) {

        $manage_stock = $purchase_item = [];
        $loginInfo    = get_loggedin_user_data();

        $purchase  = Purchase::join('purchase_items', function($join) use ($request){
            $join->on('purchase_items.purchase_id', '=', 'purchases.purchase_id');
            $join->where('purchase_type', 1);
        })->groupBy('item_id')->orderBy('purchases.purchase_id','DESC')->get();

        foreach ($purchase as $value) {

            $getItemData         = Items::find($value['item_id']);
            $get_purchase_items  = PurchaseItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'purchase_type' => 1])->sum('quantity');

            $get_return_items  = PurchaseItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'purchase_type' => 2])->sum('quantity');

            $purchase_return_data[]  = array(
                'purchase_items_id'    => $value->purchase_items_id,
                'purchase_id'          => $value->purchase_id,
                'item_id'              => $value->item_id,
                'purchase_items_name'  => $getItemData->item_name,
                'purchase_quantity'    => $get_purchase_items,
                'return_quantity'      => $get_return_items,
                'available_quantity'   => $getItemData->available_quantity,
                'rate'                 => $value->rate,
                'stock_date'           => date('d M Y',strtotime($value->created_at)),
            );     
            
        }

        $sales  = Sale::join('sale_items', function($join) use ($request){
            $join->on('sale_items.sale_id', '=', 'sales.sale_id');
            $join->where('sales_type', 1);
        })->groupBy('item_id')->orderBy('sales.sale_id','DESC')->get();

        foreach ($sales as $value) {

            $getItemData     = Items::find($value['item_id']);
            $get_sale_items  = SaleItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'sales_type' => 1])->sum('quantity');

            $get_return_items  = SaleItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'sales_type' => 2])->sum('quantity');

            $sale_return_data[]  = array(
                'sale_items_id'        => $value->sale_items_id,
                'sale_id'              => $value->sale_id,
                'item_id'              => $value->item_id,
                'sale_items_name'      => $getItemData->item_name,
                'sale_quantity'        => $get_sale_items,
                'return_quantity'      => $get_return_items,
                'available_quantity'   => $getItemData->available_quantity,
                'rate'                 => $value->rate,
                'stock_date'           => date('d M Y',strtotime($value->created_at)),
            );     
        }


        $getItemData   = Items::where('item_status' , 1)->get();

        foreach ($getItemData as $key => $value) {

            $get_purchase_items = PurchaseItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'purchase_type' => 1])->sum('quantity');

            $get_purchase_return = PurchaseItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'purchase_type' => 2])->sum('quantity');

            $get_sale_items = SaleItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'sales_type' => 1])->sum('quantity');

            $get_sale_return = SaleItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'sales_type' => 2])->sum('quantity');

            $stock_return_data[]  = array(
                'item_id'               => $value->item_id,
                'stock_items_name'      => $value->item_name,
                'purchase_quantity'     => $get_purchase_items,
                'return_pur_quantity'   => $get_purchase_return,
                'sale_quantity'         => $get_sale_items,
                'return_sale_quantity'  => $get_sale_return,
                'available_quantity'    => $value->available_quantity
            );  

        }

        $manage_stock['purchase_item']  = $purchase_return_data;
        $manage_stock['sale_item']      = $sale_return_data;
        $manage_stock['stock_item']     = $stock_return_data;


        //p($sale_return_data);

        $data = array(
            'login_info'       => $loginInfo,
            'page_title'       => trans('language.manage_stock_register'),
            'manage_stock'     => $manage_stock    
        );
        
        return view('admin-panel.inventory-sale.stock-register')->with($data);
    }









} 
