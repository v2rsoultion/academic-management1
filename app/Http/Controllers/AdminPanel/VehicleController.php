<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Transport\Vehicle;
use App\Model\Transport\VehicleDocument;
use App\Model\Transport\VehicleAttendence;
use App\Model\Transport\VehicleAttendDetails;
use App\Model\Student\Student;
use App\Model\Staff\Staff;
use App\Model\Transport\Route;
use App\Model\Transport\RouteLocation;
use App\Model\Transport\AssignDriverConductorRoute;
use App\Model\Transport\MapStudentStaffVehicle;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;

class VehicleController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('13',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }

    /**
     *  Add page for vehicle
     *  @Sandeep on 25 Jan 2019
    **/

    public function add(Request $request, $id = NULL)
    {
        $data     = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_vehicle_id = get_decrypted_value($id, true);
            $vehicle              = Vehicle::where('vehicle_id','=',$decrypted_vehicle_id)->with('get_vehicle_documents')->get();
            $vehicle              = isset($vehicle[0]) ? $vehicle[0] : [];
            
            if (!$vehicle)
            {
                return redirect('admin-panel/transport/vehicle/add-vehicle')->withError('Vehicle not found!');
            }
            
            $document_list = [];
            foreach ($vehicle['get_vehicle_documents'] as $documents)
            {
                $document_data['vehicle_document_id']       = $documents['vehicle_document_id'];
                $document_data['document_category_id']      = $documents['document_category_id'];
                $document_data['document_category_name']    = $documents['getDocumentCategory']['document_category_name'];
                $document_data['vehicle_document_details']  = $documents['vehicle_document_details'];
                $document_data['vehicle_document_status']  = $documents['vehicle_document_status'];
                $document_data['document_submit_date']      = date("d F, Y ", strtotime($documents['created_at']->toDateString()));
                if (!empty($documents['vehicle_document_file']))
                {
                    $document_file = check_file_exist($documents['vehicle_document_file'], 'vehicle_document_file');
                    if (!empty($document_file))
                    {
                        $document_data['vehicle_document_file'] = $document_file;
                    } 
                }else {
                    $document_data['vehicle_document_file'] = "";
                }
                $document_list[] = $document_data;
            }
            $vehicle->documents = $document_list;
            $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_id, true);
            $page_title         = trans('language.edit_vehicle');
            $save_url           = url('admin-panel/transport/vehicle/save/' . $id);
            $submit_button      = 'Update';
        
        } else {
            $page_title    = trans('language.add_vehicle');
            $save_url      = url('admin-panel/transport/vehicle/save');
            $submit_button = 'Save';
        }

        $arr_vehicle                      = \Config::get('custom.vehicle_ownership');
        $vehicle['arr_vehicle']           = $arr_vehicle;
        $arr_document_category            = get_all_document_category(1);
        $vehicle['arr_document_category'] = add_blank_option($arr_document_category, 'Document category');

        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'vehicle'       => $vehicle,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/vehicle/view-vehicle'),
        );
        return view('admin-panel.vehicle.add')->with($data);
    }


    /**
     *  Save Vehicle Data
     *  @Sandeep on 26 Jan 2019
    **/

    public function save(Request $request, $id = NULL)
    {
      //  p($request->all());
        $vehicle_id           = null;
        $decrypted_vehicle_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_vehicle_id = get_decrypted_value($id, true);
            $vehicle              = Vehicle::find($decrypted_vehicle_id);
            $vehicleAdmin         = Admin::Find($vehicle->reference_admin_id);
            $admin_id = $vehicle['admin_id'];
            if (!$vehicle)
            {
                return redirect('/admin-panel/transport/vehicle/add-vehicle')->withError('Vehicle not found!');
            }
            $success_msg = 'Vehicle updated successfully!';
        }
        else
        {
            $vehicle            = New Vehicle;
            $vehicleAdmin       = new Admin();
            $success_msg        = 'Vehicle saved successfully!';
        }

        $arr_input_fields = [
            'vehicle_name'                        => 'required',
            'vehicle_registration_no'             => 'required',
            'vehicle_ownership'                   => 'required',
            'vehicle_capacity'                    => 'required',
        ];
        
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                $vehicle->admin_id                        = $admin_id;
                $vehicle->update_by                       = $loginInfo['admin_id'];
                $vehicle->vehicle_name                    = Input::get('vehicle_name');
                $vehicle->vehicle_registration_no         = Input::get('vehicle_registration_no');
                $vehicle->vehicle_ownership               = Input::get('vehicle_ownership');
                $vehicle->vehicle_capacity                = Input::get('vehicle_capacity');
                $vehicle->contact_person                  = Input::get('contact_person');
                $vehicle->contact_number                  = Input::get('contact_number');
                $vehicle->save();
//p($request->all());
                $documentKey = 0;
                if(!empty($request->get('documents'))) {
                    foreach($request->get('documents') as $documents){ 
                        $fileData = $request->file('documents');
                        if(isset($documents['document_category_id']) && !empty($documents['document_category_id'])) {
                            if(isset($documents['vehicle_document_id']) ) {
                                $vehicle_document_update       = VehicleDocument::where('vehicle_document_id', $documents['vehicle_document_id'])->first();
                                $vehicle_document_update->admin_id                 = $loginInfo['admin_id'];
                                $vehicle_document_update->update_by                = $loginInfo['admin_id'];
                                $vehicle_document_update->vehicle_id               = $vehicle->vehicle_id;
                                $vehicle_document_update->document_category_id     = $documents['document_category_id'];
                                $vehicle_document_update->vehicle_document_details = $documents['vehicle_document_details'];
                                if (isset($fileData[$documentKey]['vehicle_document_file']))
                                {
                                    if (!empty($vehicle_document_update->vehicle_document_file)){
                                        $vehicle_document_file = check_file_exist($vehicle_document_update->vehicle_document_file, 'vehicle_document_file');
                                        if (!empty($vehicle_document_file))
                                        {
                                            unlink($vehicle_document_file);
                                        } 
                                    }
                                    $file                          = $fileData[$documentKey]['vehicle_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.vehicle_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $vehicle_document_update->vehicle_document_file = $filename;
                                }
                                
                                $vehicle_document_update->save();
                            } else {
                                $vehicle_document   = new vehicleDocument();
                                $vehicle_document->admin_id                 = $admin_id;
                                $vehicle_document->update_by                = $loginInfo['admin_id'];
                                $vehicle_document->vehicle_id               = $vehicle->vehicle_id;
                                $vehicle_document->document_category_id     = $documents['document_category_id'];
                                $vehicle_document->vehicle_document_details = $documents['vehicle_document_details'];
                                if (isset($fileData[$documentKey]['vehicle_document_file']))
                                {
                                    $file                          = $fileData[$documentKey]['vehicle_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.vehicle_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $vehicle_document->vehicle_document_file = $filename;
                                }
                                $vehicle_document->save();
                            }
                        }
                        $documentKey++;
                    }
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }   
        return redirect('admin-panel/transport/vehicle/view-vehicle')->withSuccess($success_msg);   
    }


    /**
     *  View page for vehicle
     *  @Sandeep on 26 Jan 2019
    **/
   
    public function index()
    {
        $loginInfo                            = get_loggedin_user_data();
        $arr_designation                      = get_all_designations();
        $listData                             = [];
        $listData['vehicle_ownership']        = \Config::get('custom.vehicle_ownership');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/vehicle/view-vehicle'),
            'save_url'      => url('admin-panel/transport/vehicle/manage-vehicle/save'),
            'page_title'    => trans('language.view_vehicle'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.vehicle.index')->with($data);
    }

    /**
     *  Get Data for view vehicle page(Datatables)
     *  @Sandeep on 30 Jan 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $vehicle  = Vehicle::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('vehicle_ownership')) && $request->get('vehicle_ownership') != null && $request->get('vehicle_ownership') != "Select Ownership")
            {
                $query->where('vehicle_ownership', "=", $request->get('vehicle_ownership'));
            }
            if (!empty($request) && !empty($request->get('vehicle_name')))
            {
                $query->where('vehicle_name', "like", "%{$request->get('vehicle_name')}%");
            }

            if (!empty($request) && !empty($request->get('vehicle_registration_no')))
            {
                $query->where('vehicle_registration_no', "like", "%{$request->get('vehicle_registration_no')}%");
            }
            if (!empty($request) && !empty($request->get('contact_person')))
            {
                $query->where('contact_person', "like", "%{$request->get('contact_person')}%");
            }

        })->with('get_vehicle_documents')->orderBy('vehicle_id', 'DESC')->get();

        return Datatables::of($vehicle)
            ->addColumn('vehicle_ownership', function ($vehicle)
            {
                if($vehicle->vehicle_ownership == 0) {
                    $vehicle_ownership = 'Owner';
                } else {
                    $vehicle_ownership = 'Contractor';
                }
                return $vehicle_ownership;
            })
            ->addColumn('person_info', function ($vehicle)
            {
                $person_info = $vehicle->contact_person.' - '.$vehicle->contact_number;
                return $person_info;
            })
            ->addColumn('total_documents', function ($vehicle)
            {
                $total_documents = '<button class="btn btn-raised btn-primary vehicle" vehicle_document_id='.$vehicle->vehicle_id.'>'.count($vehicle->get_vehicle_documents).' Documents </button>';
                return $total_documents;
            })
            
            ->addColumn('action', function ($vehicle)
            {
                $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_id, true);
                if($vehicle->vehicle_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li style="padding-left:10px;" class="vehicle_mapping" vehicle_mapping_id='.$vehicle->vehicle_id.' > Map Driver / Conductor </li>
                    </ul>
                </div>

                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="vehicle-status/'.$status.'/' . $encrypted_vehicle_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-vehicle/' . $encrypted_vehicle_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-vehicle/' . $encrypted_vehicle_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','total_documents'=> 'total_documents'])->addIndexColumn()->make(true);
    }

    
    /**
     *  Change vehicle's status
     *  @Sandeep on 30 Jan 2019 
    **/
    public function changeStatus($status,$id)
    {
        $vehicle_id = get_decrypted_value($id, true);
        $vehicle    = Vehicle::find($vehicle_id);
        if ($vehicle)
        {
            $vehicle->vehicle_status  = $status;
            $vehicle->save();
            $success_msg = "Vehicle status updated successfully!";
            return redirect('admin-panel/transport/vehicle/view-vehicle')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Vehicle not found!";
            return redirect('admin-panel/transport/vehicle/view-vehicle')->withErrors($error_message);
        }
    }

    /**
     *  Destroy vehicle data
     *  @Sandeep on 30 Jan 2019  
    **/
    public function destroy($id)
    {
        $vehicle_id = get_decrypted_value($id, true);
        $vehicle    = Vehicle::find($vehicle_id);
        
        $success_msg = $error_message =  "";
        if ($vehicle)
        {
            DB::beginTransaction();
            try
            {
                $vehicle->delete();
                $success_msg = "Vehicle deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/transport/vehicle/view-vehicle')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/transport/vehicle/view-vehicle')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Vehicle not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Get Data for view vehicle page(Datatables)
     *  @Sandeep on 30 Jan 2019
    **/
    public function vehicle_documents(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $vehicle  = VehicleDocument::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('vehicle_document_id'))) {
                $query->where('vehicle_id', "=", $request->get('vehicle_document_id'));
            }
        })->with('getDocumentCategory')->orderBy('vehicle_document_id', 'DESC')->get();

        return Datatables::of($vehicle)

            ->addColumn('document_category_name', function ($vehicle)
            {

                $document_category_name = $vehicle['getDocumentCategory']['document_category_name'];
                
                return $document_category_name;
            })

            ->addColumn('vehicle_document_file', function ($vehicle)
            {
                if (!empty($vehicle->vehicle_document_file)) {
                    $document_file = check_file_exist($vehicle->vehicle_document_file, 'vehicle_document_file');
                    if (!empty($document_file))
                    {
                        $vehicle_document_file = '<a href='.url($document_file).'  target="_blank" >View Document</a>';
                    } 
                } else {
                    $vehicle_document_file = '<a href="#" target="_blank" >No Document Found</a>';
                }

                return $vehicle_document_file;
            })


            ->addColumn('action', function ($vehicle)
            {
                $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_document_id, true);
                
                return '
                    <div class="btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete" style="margin-top:-8px;"><a href="delete-vehicle-document/' . $encrypted_vehicle_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','vehicle_document_file'=> 'vehicle_document_file'])->addIndexColumn()->make(true);
    }


    /**
     *  Destroy vehicle documents data
     *  @Sandeep on 30 Jan 2019  
    **/
    public function destroy_document($id)
    {
        $vehicle_id = get_decrypted_value($id, true);
        $vehicle    = VehicleDocument::find($vehicle_id);
        
        $success_msg = $error_message =  "";
        if ($vehicle)
        {
            DB::beginTransaction();
            try
            {
                $vehicle->delete();
                $success_msg = "Vehicle Document deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/transport/vehicle/view-vehicle')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/transport/vehicle/view-vehicle')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Vehicle Document not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Get Driver conductor router mapping's Data for view page(Datatables)
     *  @Sandeep on 4 Feb 2019
    **/
    public function mapping_vehicle(Request $request)
    {

        $loginInfo       = get_loggedin_user_data();
        $get_vehicles    = get_all_vehicles($request->get('vehicle_mapping_id'));
        $get_drivers     = get_all_driver_conductors('14');
        $get_conductors  = get_all_driver_conductors('15');
        $get_routes      = get_all_routes();

        $listData           = [];
        $table = 
        "<table class='table m-b-0 c_list' style='width:98%'>
        <thead>
        <tr>
            <th style='padding-left:15px !important;' >Driver</th>
            <th style='padding-left:15px !important;' >Conductor</th>
        </tr>
        </thead>
        <tbody>";

        $i=0;
        foreach ($get_vehicles as $key => $value){
        $i++;
            $driver_id_arr = $conductor_id_arr = $route_id_arr = array();
            $assign_data = AssignDriverConductorRoute::where('vehicle_id',$value->vehicle_id)->get()->toArray();
            $conductor_driver_route_map_id = NULL;
            $assign_id_arr = "";
            foreach ($assign_data as $assignkey){  
                $driver_id_arr[]    = $assignkey['driver_id'];
                $assign_id_arr      = $assignkey['assign_id'];
                $conductor_id_arr[] = $assignkey['conductor_id'];
                $route_id_arr[]     = $assignkey['route_id'];
                $conductor_driver_route_map_id = $assignkey['conductor_driver_route_map_id'];
            }
            $table .= "<tr>";
            
            $table .= "<td>";
            
            $table  .= "
            <input type='hidden' name=routeMap[$key][assign_id]' value='".$assign_id_arr."' />
            <input type='hidden' name=routeMap[$key][vehicle_id]' value='".$value->vehicle_id."' />
            <div class='form-group'>
            <label class='field select' style='width:150px;'><select class='form-control show-tick select_form3 select2' name='routeMap[$key][driver_id]'  id='teacher_ids_".$value->subject_id."' required>";
            $table .= "<option value='' >Select Driver</option>";
            foreach($get_drivers as $key2 => $value2){ // For Teacher's details
                $disable = '';

                if($key2 == ''){
                    $disable = 'disabled="disabled"';
                }
                $default_select = '';
                if(in_array($key2, $driver_id_arr)){
                    $default_select = 'selected';
                }
                $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
            } 
            $table .= "</select></label></div></td>";

            $table  .= "<td>
            <div class='form-group'>
            <label class='field select' style='width:150px;'><select class='form-control show-tick select_form3 select2' name='routeMap[$key][conductor_id]'  id='teacher_ids_".$value->subject_id."' required>";
            $table .= "<option value='' >Select Conductor</option>";
            foreach($get_conductors as $key2 => $value2){ // For Teacher's details
                $disable = '';

                if($key2 == ''){
                    $disable = 'disabled="disabled"';
                }
                $default_select = '';
                if(in_array($key2, $conductor_id_arr)){
                    $default_select = 'selected';
                }
                $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
            } 
            $table .= "</select></label></div></td>";

            $table .= "</tr>";                          
        }

        $table .= "</tbody></table>";
        return response()->json(['status'=>1001,'data'=>$table]);
    }


    /**
     *  Add and update driver conductor route mapping's data
     *  @Sandeep on 4 Feb 2019.
    **/
    public function save_map_vehicle(Request $request)
    {
        
        $loginInfo          = get_loggedin_user_data();
        $admin_id           = $loginInfo['admin_id'];


        foreach ($request->get('routeMap') as $key => $map) {

            if(($map['assign_id'] != '')){

                $arr_input_fields = [
                    'driver_id'       => 'required|unique:assign_driver_conductor_routes,driver_id,' . $map['assign_id'] . ',assign_id',
                    'conductor_id'    => 'required|unique:assign_driver_conductor_routes,conductor_id,' . $map['assign_id'] . ',assign_id',
                ];

                $validatior = Validator::make($map, $arr_input_fields);

                if ($validatior->fails()) {
                    return redirect()->back()->withInput()->withErrors($validatior);
                } else {
                    DB::beginTransaction(); //Start transaction!
                    try
                {

                    // edit case
                    $mapping                = AssignDriverConductorRoute::find($map['assign_id']);      
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->vehicle_id    = $map['vehicle_id'];
                    $mapping->driver_id     = $map['driver_id'];
                    $mapping->conductor_id  = $map['conductor_id'];
                    $mapping->save();
                
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                } }

            } else if($map['vehicle_id'] != ''){

                $arr_input_fields = [
                    'driver_id'               => 'required|unique:assign_driver_conductor_routes,driver_id',
                    'conductor_id'            => 'required|unique:assign_driver_conductor_routes,conductor_id',
                ];

                $validatior = Validator::make($map, $arr_input_fields);

                if ($validatior->fails()) {
                    return redirect()->back()->withInput()->withErrors($validatior);
                } else {
                    DB::beginTransaction(); //Start transaction!
                    try
                {

                    // add case
                    $mapping                = new AssignDriverConductorRoute;        
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->vehicle_id    = $map['vehicle_id'];
                    $mapping->driver_id     = $map['driver_id'];
                    $mapping->conductor_id  = $map['conductor_id'];
                    $mapping->save();

                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                } }
            }

        }
            
        DB::commit();
        $success_msg = "Driver / Conductor successfully assign.";
        return redirect()->back()->withSuccess($success_msg);
        
    }


    /**
     *  View page for Student staff mapping to vehicle 
     *  @Sandeep on 6 Feb 2019
    **/
    public function manage_student_staff()
    {
        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_student_staff'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.vehicle.manage_student_staff')->with($data);
    }


    /**
     *  Get Data for view vehicle page(Datatables)
     *  @Sandeep on 6 Feb 2019
    **/
    public function manage_student_staff_data(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $vehicle  = Vehicle::where(function($query) use ($request) 
        {
            
            if (!empty($request) && !empty($request->get('vehicle_name')))
            {
                $query->where('vehicle_name', "like", "%{$request->get('vehicle_name')}%");
            }

        })

        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->with('getRoute')
        ->with('getRoute.get_route_locations')
        ->with(['get_student'=>function($q) use ($request) {
                $q->where('student_staff_type', "=", 1);
        }])
        ->with(['get_staff'=>function($q) use ($request) {
                $q->where('student_staff_type', "=", 2);
        }])
        ->orderBy('vehicles.vehicle_id', 'DESC')
        ->get();

        return Datatables::of($vehicle)
            ->addColumn('driver_coductor_info', function ($vehicle)
            {   

                if($vehicle['getDriver'] != ""){
                    $driver_coductor_info = "Driver - ".$vehicle['getDriver']->staff_name;
                }    

                $driver_coductor_info = $driver_coductor_info." <br/>";
                
                if($vehicle['getConductor'] != ""){
                    $driver_coductor_info = $driver_coductor_info." Conductor - ".$vehicle['getConductor']->staff_name;
                }

                if($vehicle['getDriver'] == "" && $vehicle['getConductor'] == ""){
                    $driver_coductor_info = "Not Available";
                }
                return $driver_coductor_info;
            })
            ->addColumn('no_of_student', function ($vehicle)
            {
                $no_of_student = count($vehicle['get_student']).' Student';
                return $no_of_student;
            })
            ->addColumn('no_of_staff', function ($vehicle)
            {
                $no_of_staff = count($vehicle['get_staff']).' Staff';
                return $no_of_staff;
            })
            
            ->addColumn('map_views', function ($vehicle)
            {   
                foreach ($vehicle['getRoute']['get_route_locations'] as $key => $value){

                    $get_route_locations[] = $value;

                    $wayptsnew[] = array(
                        "location" =>
                        array(
                            'lat' => str_replace('"', '', $value->location_latitude),
                            'lng' => str_replace('"', '', $value->location_logitude)
                        ),
                        'stopover'  => true
                    );
                }

                $start = "{lat: ".$get_route_locations[0]->location_latitude.", lng: ".$get_route_locations[0]->location_logitude."}";
                $end   = array_values(array_slice($get_route_locations, -1))[0];
                $end_new   = "{lat: ".$end->location_latitude.", lng: ".$end->location_logitude."}";

                $newarray = array_slice($wayptsnew, 1, -1);

                $wayptsnew = json_encode($newarray);
                $wayptsnews = str_replace('"','',$wayptsnew);

                if($start == ""){ $start = []; }
                if($end == ""){ $end = []; }
                if($end_new == ""){ $end_new = []; }

                $map_views = '<button class="btn btn-raised btn-primary map_views" onclick="calculateAndDisplayRoute('.$start.','.$end_new.','.$wayptsnews.')" route_location_id='.$vehicle['getRoute']->route_id.'> Map View </button>';
                return $map_views;
            })

            ->addColumn('action', function ($vehicle)
            {
                $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_id, true);
                
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li style="padding-left:10px;" class="" ><a href="manage-staff/'.$encrypted_vehicle_id.'" >  Manage Staff </a> </li>
                        <li style="padding-left:10px;" class="" ><a href="manage-student/'.$encrypted_vehicle_id.'">  Manage Student </a> </li>
                        <li style="padding-left:10px;" class="" ><a href="view-all-staff/'.$encrypted_vehicle_id.'">  View All Staff </a> </li>
                        <li style="padding-left:10px;" class="" ><a href="view-all-student/'.$encrypted_vehicle_id.'">  View All Student </a> </li>
                    </ul>
                </div>                    
                ';
            })->rawColumns(['action' => 'action','driver_coductor_info'=> 'driver_coductor_info','no_of_student'=> 'no_of_student','no_of_staff'=> 'no_of_staff', 'map_views'=> 'map_views'])->addIndexColumn()->make(true);
    }


    /**
     *  View page for Manage Staff 
     *  @Sandeep on 7 Feb 2019
    **/
    public function manage_staff($id)
    {

        $decrypted_vehicle_id = get_decrypted_value($id, true);
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];
            
        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/manage-student-staff')->withError('Vehicle not found!');
        }


        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_staff'),
            'login_info'    => $loginInfo,
            'vehicle'       => $vehicle,
            'save_url'     => url('admin-panel/transport/vehicle/get-manage-route-save/' . $id)
        );
        return view('admin-panel.vehicle.manage_staff')->with($data);
    }


    /**
     *  get data for Manage Staff 
     *  @Sandeep on 7 Feb 2019
    **/

    public function manage_staff_data(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        

    //    p($get_stu_data);
         
        $staff  = Staff::where(function($query) use ($request,$get_stu_data) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('staff_name', "like", "%{$request->get('name')}%")->whereRaw('FIND_IN_SET(2,staff_role_id)');
                $query->orwhere('staff_name', "like", "%{$request->get('name')}%")->whereRaw('FIND_IN_SET(3,staff_role_id)');
            } else {
                $query->whereRaw('FIND_IN_SET(3,staff_role_id) or FIND_IN_SET(2,staff_role_id)');
            }
        })->select('staff_id','staff_name','designation_id','staff_status')->orderBy('staff_id', 'DESC')->get();
        return Datatables::of($staff)
            ->addColumn('staff_designation_name', function ($staff)
            {
                $arr_designations   = get_all_designations();
                $staff_designation_name  = $arr_designations[$staff->designation_id];
                return $staff_designation_name;
            })
            
            ->addColumn('action', function ($staff) use($request,$session)
            {

                $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
                
                $student_data  = MapStudentStaffVehicle::where(function($query) use ($request,$staff,$session) 
                {
                    $query->where('vehicle_id', "!=", $request->vehicle_id );
                    $query->where('student_staff_type', "=", 2);
                    $query->where('student_staff_id', "=", $staff->staff_id);
                    $query->where('student_staff_status', "=", 1);
                    $query->where('session_id', "=", $session['session_id']);
                })->count();

               // p($student_data);

                if($student_data == 0){
                    return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li style="padding-left:20px;" class="register_staff" staff_id='.$staff->staff_id.' > Register Staff </li>
                    </ul>
                </div>
                ';

                } else {
                
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li style="padding-left:20px;"> Staff Already Registered with other vehicle </li>
                    </ul>
                </div>
                ';
                }

                
               
                
            })->rawColumns(['action' => 'action', 'staff_designation_name'=>'staff_designation_name'])->addIndexColumn()->make(true);
    }


    /**
     *  Get route mapping's Data for view page(Datatables)
     *  @Sandeep on 7 Feb 2019
    **/
    public function get_manage_route(Request $request)
    {

        //p($request->all());
        $loginInfo       = get_loggedin_user_data();
        $get_routes      = Route::join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.route_id', '=', 'routes.route_id');
        })->where('assign_driver_conductor_routes.vehicle_id','=',$request->vehicle_id)->get();

        $listData           = [];
        $table = 
        "<table class='table m-b-0 c_list' style='width:98%'>
        <thead>
        <tr>
            <th style='padding-left:15px !important;'> Routes</th>
            <th style='padding-left:15px !important;'> Routes Locations</th>
        </tr>
        </thead>
        <tbody>";

        if($request->staff_id != ""){
            $request->staff_student_id = $request->staff_id;
        } else {
            $request->staff_student_id = $request->student_id;
        }

        //p($request->staff_student_id);

             $route_id_arr = array();
             $assign_data = MapStudentStaffVehicle::where(['vehicle_id' => $request->vehicle_id,'student_staff_id' => $request->staff_student_id,'student_staff_type' => $request->type ])->first();
            
            $route_id_arr[]     = $assign_data->route_id;
            $table .= "<tr>";
            
            $table .= "<td>";
            
            $table  .= "
            <input type='hidden' name='map_student_staff_id' value='".$assign_data->map_student_staff_id."' />
            <input type='hidden' name='staff_id' value='".$request->staff_student_id."' />
            <input type='hidden' name='vehicle_id' value='".$request->vehicle_id."' />
            <input type='hidden' name='type' value='".$request->type."' />
            <div class='form-group'>
            <label class='field select' style='width:200px;'><select class='form-control show-tick select_form3 select2' name='route_id'  id='route_ids' onChange ='main_route(this.value)' required>";
            $table .= "<option value='' >Select Route</option>";
            foreach($get_routes as $key2 => $value2){ // For Teacher's details
                $disable = '';

                if($value2->route_id == ''){
                    $disable = 'disabled="disabled"';
                }
                $default_select = '';
                if(in_array($value2->route_id, $route_id_arr)){
                    $default_select = 'selected';
                }
                $table .= "<option value=".$value2->route_id." ".$default_select." ".$disable.">".$value2->route_name."</option>";
            } 
            $table .= "</select></label></div></td>";

            $table .= "<td>";
            $table  .= "
            <div class='form-group'>
            <label class='field select' style='width:200px;'><select class='form-control show-tick select_form3 select2' name='route_location_id'  id='route_location_id' required>";
            $table .= "<option value=''> Select Route Location </option>";
            
            if($assign_data->map_student_staff_id != ""){

                $route_id_arr = array();
                $get_routes      = RouteLocation::where('route_id', "=", $assign_data->route_id)->get();
               
                $route_id_arr[]     = $assign_data->route_location_id;

                foreach($get_routes as $key2 => $value2){
                    $disable = '';

                    if($value2->route_location_id == ''){
                        $disable = 'disabled="disabled"';
                    }
                    $default_select = '';
                    if(in_array($value2->route_location_id, $route_id_arr)){
                        $default_select = 'selected';
                    }

                    $table .= "<option value=".$value2->route_location_id." ".$default_select." ".$disable.">".$value2->location_name."</option>";
                }
            }

            $table .= "</select></label></div></td>";


            $table .= "</tr>";                          
        

        $table .= "</tbody></table>";
        return response()->json(['status'=>1001,'data'=>$table]);
    }


    /**
     *  save data for Staff Route 
     *  @Sandeep on 7 Feb 2019
    **/

    public function get_manage_route_save(Request $request , $id){
        
        $loginInfo          = get_loggedin_user_data();
        $admin_id           = $loginInfo['admin_id'];

        if(($request->map_student_staff_id != '')) {
            $mapping  = MapStudentStaffVehicle::find($request->map_student_staff_id);  
            $success_msg = "Route successfully updated.";   
        } else {
            $mapping  = New MapStudentStaffVehicle;
            $success_msg = "Route successfully assigned.";
        }

        $mapping->admin_id              = $admin_id;
        $mapping->update_by             = $loginInfo['admin_id'];
        $mapping->vehicle_id            = $request->vehicle_id;
        $mapping->route_id              = $request->route_id;
        $mapping->route_location_id     = $request->route_location_id;
        $mapping->student_staff_id      = $request->staff_id;
        $mapping->student_staff_type    = $request->type;
        $mapping->save();

        return redirect()->back()->withSuccess($success_msg);
    }



    /**
     *  Get route location Data for view page(Datatables)
     *  @Sandeep on 7 Feb 2019
    **/
    public function get_manage_route_location(Request $request)
    {

        //p($request->all());
        $loginInfo       = get_loggedin_user_data();
        $get_routes      = RouteLocation::where('route_id', "=", $request->route_id)->get();

        $listData           = [];
            
            $table = "<option value=''> Select Route Location </option>";
            foreach($get_routes as $key2 => $value2){ // For Teacher's details
                $table .= "<option value=".$value2->route_location_id." > ".$value2->location_name."</option>";
            } 
        return response()->json(['status'=>1001,'data'=>$table]);
    }


    /**
     *  View page for Manage Student
     *  @Sandeep on 7 Feb 2019
    **/
    public function manage_student($id)
    {

        $decrypted_vehicle_id = get_decrypted_value($id, true);
        
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();

        $vehicle              = isset($vehicle) ? $vehicle : [];
            
        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/manage-student-staff')->withError('Vehicle not found!');
        }


        $loginInfo                  = get_loggedin_user_data();
        $arr_medium                 = get_all_mediums();
        $arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');

        $data = array(
            'page_title'    => trans('language.manage_student'),
            'login_info'    => $loginInfo,
            'vehicle'       => $vehicle,
            'listData'      => $listData,
            'save_url'      => url('admin-panel/transport/vehicle/get-manage-route-save/' . $id)
        );
        return view('admin-panel.vehicle.manage_student')->with($data);
    }


    /**
     *  get data for Manage Student
     *  @Sandeep on 7 Feb 2019
    **/

    public function student_list_data(Request $request)
    { 
        $student            = [];
        $arr_student_list   = [];
        $session            = get_current_session();
        $arr_medium         = get_all_mediums();

        $student_data  = MapStudentStaffVehicle::where(function($query) use ($request,$session) 
        {
            $query->where('vehicle_id', "!=", $request->vehicle_id );
            $query->where('student_staff_type', "=", 1);
            $query->where('student_staff_status', "=", 1);
            $query->where('session_id', "=", $session['session_id']);
        })->get();

        foreach ($student_data as $student_datas) {
            $get_stu_data .= $student_datas->student_staff_id.',';
        }
        
        $get_stu_data = explode(',', $get_stu_data);
        $get_stu_data = array_filter($get_stu_data);
        
        $arr_student  = Student::where(function($query) use ($request,$session,$get_stu_data) 
        {
            $query->where('students.student_status',1);
            $query->whereNotIn('students.student_id',$get_stu_data);    
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            $join->where('current_session_id',$session['session_id']);
        })
        ->orderBy('students.student_id', 'DESC')
        ->with('getParent')
        ->get();
        //p($arr_student);
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $arr_student_list[] = array(
                    'student_id'            => $student->student_id,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_name'          => $student->student_name,
                    'student_parent_id'     => $student['getParent']->student_parent_id,
                    'student_father_name'   => $student['getParent']->student_father_name,
                    'student_father_mobile_number'   => $student['getParent']->student_father_mobile_number,
                );
            }
        }

    //    p($arr_student_list);
        $arr_student = $arr_student_list;

        $data = array(
            'arr_student'    => $arr_student
        );
        return view('admin-panel.vehicle.student-grid')->with($data);
    }

    /**
     *  View page for all Staff 
     *  @Sandeep on 8 Feb 2019
    **/
    public function view_all_staff($id)
    {

        $decrypted_vehicle_id = get_decrypted_value($id, true);
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];
            
        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/manage-student-staff')->withError('Vehicle not found!');
        }


        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.staff_list'),
            'login_info'    => $loginInfo,
            'vehicle'       => $vehicle,
        );
        return view('admin-panel.vehicle.all_staff_list')->with($data);
    }


    /**
     *  get data for view all Staff 
     *  @Sandeep on 8 Feb 2019
    **/

    public function view_all_staff_data(Request $request)
    {   
        //p($request->input());
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $staff  = MapStudentStaffVehicle::where(function($query) use ($request,$session) 
        {
            $query->where('vehicle_id', "=", $request->vehicle_id );
            $query->where('student_staff_type', "=", $request->type);
            $query->where('student_staff_status', "=", 1);
            $query->where('session_id', "=", $session['session_id']);
        })

        ->join('staff', function($join) use ($request)  {
            $join->on('staff.staff_id', '=', 'map_student_staff_vehicle.student_staff_id');
            if (!empty($request) && !empty($request->get('name')))
            {
                $join->where('staff_name', "like", "%{$request->get('name')}%");
            }
        })->get();

        return Datatables::of($staff)
            ->addColumn('staff_designation_name', function ($staff)
            {
                $arr_designations   = get_all_designations();
                $staff_designation_name  = $arr_designations[$staff->designation_id];
                return $staff_designation_name;
            })
            
            ->addColumn('action', function ($staff)
            {
                $encrypted_student_staff_id = get_encrypted_value($staff->map_student_staff_id, true);
                return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-vehicle-staff/' . $encrypted_student_staff_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
                
            })->rawColumns(['action' => 'action', 'staff_designation_name'=>'staff_designation_name'])->addIndexColumn()->make(true);
    }


    /**
     *  Destroy staff vehicle data
     *  @Sandeep on 8 Feb 2019  
    **/
    public function destroy_staff_vehicle($id)
    {
        $vehicle_id = get_decrypted_value($id, true);
        $vehicle    = MapStudentStaffVehicle::find($vehicle_id);
        
        $success_msg = $error_message =  "";
        if ($vehicle)
        {
            DB::beginTransaction();
            try
            {
                $vehicle->student_staff_status  = 0;
                $vehicle->save();
                $success_msg = "Staff deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Staff not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  View page for all student 
     *  @Sandeep on 8 Feb 2019
    **/
    public function view_all_student($id)
    {

        $decrypted_vehicle_id = get_decrypted_value($id, true);
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];
            
        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/manage-student-staff')->withError('Vehicle not found!');
        }


        $loginInfo                  = get_loggedin_user_data();
        $arr_medium                 = get_all_mediums();
        $arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');

        $data = array(
            'page_title'    => trans('language.student_list'),
            'login_info'    => $loginInfo,
            'vehicle'       => $vehicle,
            'listData'      => $listData
        );
        return view('admin-panel.vehicle.all_student_list')->with($data);
    }


    /**
     *  get data for view all student 
     *  @Sandeep on 8 Feb 2019
    **/

    public function view_all_student_data(Request $request)
    {   
        //p($request->input());
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();

        $arr_students  = MapStudentStaffVehicle::where(function($query) use ($request,$session) 
        {
            $query->where('vehicle_id', "=", $request->vehicle_id );
            $query->where('student_staff_type', "=", $request->type);
            $query->where('student_staff_status', "=", 1);
            $query->where('session_id', "=", $session['session_id']);
        })

        ->join('students', function($join) use ($request)  {
            $join->on('students.student_id', '=', 'map_student_staff_vehicle.student_staff_id');
        })
        ->join('student_academic_info', function($join) use ($request){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
        })->join('student_parents', function($join) use ($request)  {
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })->get();

//        p($arr_students);
        return Datatables::of($arr_students)
            ->addColumn('action', function ($arr_students)
            {
                $encrypted_student_staff_id = get_encrypted_value($arr_students->map_student_staff_id, true);
                return ' 
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-vehicle-student/' . $encrypted_student_staff_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
            ';
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile'])->addIndexColumn()->make(true);
    }


    /**
     *  Destroy student vehicle data
     *  @Sandeep on 8 Feb 2019  
    **/
    public function destroy_student_vehicle($id)
    {
        $vehicle_id = get_decrypted_value($id, true);
        $vehicle    = MapStudentStaffVehicle::find($vehicle_id);
        
        $success_msg = $error_message =  "";
        if ($vehicle)
        {
            DB::beginTransaction();
            try
            {
                $vehicle->student_staff_status  = 0;
                $vehicle->save();
                //$vehicle->delete();
                $success_msg = "Student deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Vehicle attendence  
     *  @Sandeep on 8 Feb 2019
    **/
    public function vehicle_attendence()
    {
        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.vehicle_attendence'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.vehicle.vehicle_attendence')->with($data);
    }


    /**
     *  Get Data for vehicle Attendence(Datatables)
     *  @Sandeep on 8 Feb 2019
    **/
    public function vehicle_attendence_data(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $vehicle  = Vehicle::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('vehicle_name')))
            {
                $query->where('vehicle_name', "like", "%{$request->get('vehicle_name')}%");
            }
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->with('getRoute')
        ->with('getRoute.get_route_locations')
        ->orderBy('vehicles.vehicle_id', 'DESC')
        ->get();

        return Datatables::of($vehicle)
            ->addColumn('driver_coductor_info', function ($vehicle)
            {   

                if($vehicle['getDriver'] != ""){
                    $driver_coductor_info = "Driver - ".$vehicle['getDriver']->staff_name;
                }    

                $driver_coductor_info = $driver_coductor_info." <br/>";
                
                if($vehicle['getConductor'] != ""){
                    $driver_coductor_info = $driver_coductor_info." Conductor - ".$vehicle['getConductor']->staff_name;
                }

                if($vehicle['getDriver'] == "" && $vehicle['getConductor'] == ""){
                    $driver_coductor_info = "Not Available";
                }
                return $driver_coductor_info;
            })
            
            ->addColumn('map_views', function ($vehicle)
            {   
                foreach ($vehicle['getRoute']['get_route_locations'] as $key => $value){

                    $get_route_locations[] = $value;

                    $wayptsnew[] = array(
                        "location" =>
                        array(
                            'lat' => str_replace('"', '', $value->location_latitude),
                            'lng' => str_replace('"', '', $value->location_logitude)
                        ),
                        'stopover'  => true
                    );
                }

                $start = "{lat: ".$get_route_locations[0]->location_latitude.", lng: ".$get_route_locations[0]->location_logitude."}";
                $end   = array_values(array_slice($get_route_locations, -1))[0];
                $end_new   = "{lat: ".$end->location_latitude.", lng: ".$end->location_logitude."}";

                $newarray = array_slice($wayptsnew, 1, -1);

                $wayptsnew = json_encode($newarray);
                $wayptsnews = str_replace('"','',$wayptsnew);

                if($start == ""){ $start = []; }
                if($end == ""){ $end = []; }
                if($end_new == ""){ $end_new = []; }

                $map_views = '<button class="btn btn-raised btn-primary map_views" onclick="calculateAndDisplayRoute('.$start.','.$end_new.','.$wayptsnews.')" route_location_id='.$vehicle['getRoute']->route_id.'> Map View </button>';
                return $map_views;
            })

            ->addColumn('action', function ($vehicle)
            {
                $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_id, true);
                
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li style="padding-left:10px;" class="" ><a href="add-attendance/'.$encrypted_vehicle_id.'" >  Add Attendance </a> </li>
                        <li style="padding-left:10px;" class="" ><a href="view-attendance/'.$encrypted_vehicle_id.'">  View Attendance </a> </li>
                    </ul>
                </div>                    
                ';
            })->rawColumns(['action' => 'action','driver_coductor_info'=> 'driver_coductor_info','no_of_student'=> 'no_of_student','no_of_staff'=> 'no_of_staff', 'map_views'=> 'map_views'])->addIndexColumn()->make(true);
    }



    /**
     *  View page vehicle add attendence 
     *  @Sandeep on 8 Feb 2019
    **/
    public function add_attendence(Request $request,$id,$attendence_id = false)
    {
        $holiday_list = get_school_all_holidays(0);
        $decrypted_vehicle_id = get_decrypted_value($id, true);
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];

        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/vehicle-attendance')->withError('Vehicle not found!');
        }

        $data = $attendance = [];
        $loginInfo  = get_loggedin_user_data();
        $holiday_list = get_school_all_holidays(2);
        if (!empty($attendence_id))
        {
            $decrypted_attendance_id = get_decrypted_value($attendence_id, true);
            $attendance              = VehicleAttendence::Find($decrypted_attendance_id)->toArray();

            if (!$attendance)
            {
                return redirect()->back()->withErrors('Attendance not found!');
            }
            $page_title          = trans('language.edit_attendance');
            $encrypted_attendance_id = get_encrypted_value($attendance['vehicle_attendence_id'], true);
            $save_url            = url('admin-panel/transport/vehicle/save-attendance/' . $encrypted_attendance_id);
            $submit_button       = 'Update';
            $currentDate = $attendance['vehicle_attendence_date'];
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;

        }else{
            $page_title    = trans('language.add_attendance');
            $save_url      = url('admin-panel/transport/vehicle/save-attendance');
            $submit_button = 'Save';
            $currentDate = date('Y-m-d');
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;
        }
        if(in_array($currentDate,$holiday_list)){
            $attendance['holiday'] = 1;
        }

        $data   = array(
            'page_title'            => $page_title,
            'save_url'              => $save_url,
            'submit_button'         => $submit_button,
            'attendance'            => $attendance,
            'login_info'            => $loginInfo,
            'vehicle'               => $vehicle,
            'decrypted_vehicle_id'  => $id,
        );


        return view('admin-panel.vehicle.add_attendence')->with($data);
    }


    /**
     *  View data page vehicle add attendence 
     *  @Sandeep on 8 Feb 2019
    **/

    public function attendance_data(Request $request){
        //p($request->all());

        $arr_staff   = [];
        $vehicle_attendance = MapStudentStaffVehicle::where(['vehicle_id' => $request->vehicle_id])->get();

        return Datatables::of($vehicle_attendance)
        ->addColumn('student_staff_name', function ($vehicle_attendance)
        {
            if($vehicle_attendance->student_staff_type == 1){
                $student_staff_data = Student::where('student_id',$vehicle_attendance->student_staff_id)->first();
                $student_staff_data->student_staff_name = $student_staff_data->student_name;
            } else {
                $student_staff_data = Staff::where('staff_id',$vehicle_attendance->student_staff_id)->first(); 
                $student_staff_data->student_staff_name = $student_staff_data->staff_name;
            }
            return $student_staff_data->student_staff_name;
            
        })
        ->addColumn('student_staff_type_new', function ($vehicle_attendance)
        {
            if($vehicle_attendance->student_staff_type == 1 ){
                return "Student";
            } else {
                return "Staff";
            }            
        })

        ->addColumn('attendance', function ($vehicle_attendance) use ($request)
        {
            $present = $absent = '';
            $present = "checked";

            $get_vehicle_attendence = VehicleAttendDetails::where(function($query) use ($request,$vehicle_attendance) 
            {
                $query->where('vehicle_attendence_date',$request->get('attendance_date'));
                if($request->get('vehicle_attendence_id') != ""){
                    $query->where('vehicle_attendence_id',$request->get('vehicle_attendence_id'));
                }
                $query->where('student_staff_id',$vehicle_attendance->student_staff_id);
                $query->where('student_staff_type',$vehicle_attendance->student_staff_type);
            })
            ->first();

            if($get_vehicle_attendence != ""){

                if($get_vehicle_attendence->vehicle_attendence_type == 1){
                    $present = "checked";
                } else if($get_vehicle_attendence->vehicle_attendence_type == 0){
                    $absent = "checked";
                }
            }

            $attendance = '
                <input type="hidden" name="attendance['.$vehicle_attendance->map_student_staff_id.'][student_staff_id]"  value="'.$vehicle_attendance->student_staff_id.'" > 
                <input type="hidden" name="attendance['.$vehicle_attendance->map_student_staff_id.'][student_staff_type]"  value="'.$vehicle_attendance->student_staff_type.'" >   

                <input type="hidden" name="attendance['.$vehicle_attendance->map_student_staff_id.'][vehicle_attend_d_id]"  value="'.$get_vehicle_attendence->vehicle_attend_d_id.'" > 

                <div class="green float-left attendence_type" style="margin-top:6px !important;" counter= "'.$vehicle_attendance->map_student_staff_id.'">
                    <input id="attendance_status1'.$vehicle_attendance->map_student_staff_id.'"  name="attendance['.$vehicle_attendance->map_student_staff_id.'][attendance]" '.$present.' type="radio" value="1" >
                    <label for="attendance_status1'.$vehicle_attendance->map_student_staff_id.'" class="document_staff">Present</label>
                </div>
                <div class="red float-left attendence_type" style="margin-top:6px !important;" counter= "'.$vehicle_attendance->map_student_staff_id.'">
                    <input id="attendance_status2'.$vehicle_attendance->map_student_staff_id.'" name="attendance['.$vehicle_attendance->map_student_staff_id.'][attendance]" '.$absent.' type="radio" value="0" />
                    <label for="attendance_status2'.$vehicle_attendance->map_student_staff_id.'" class="document_staff">Absent</label> 
                </div>
                ';
            return $attendance;
        })
        ->addColumn('overtime_type', function ($vehicle_attendance) use ($request)
        {

            $get_vehicle_attendence = VehicleAttendDetails::where(function($query) use ($request,$vehicle_attendance) 
            {
                $query->where('vehicle_attendence_date',$request->get('attendance_date'));
                $query->where('vehicle_attendence_id',$request->get('vehicle_attendence_id'));
                $query->where('student_staff_id',$vehicle_attendance->student_staff_id);
                $query->where('student_staff_type',$vehicle_attendance->student_staff_type);
            })
            ->first();

            $pick_up = $drop = '';

            if($get_vehicle_attendence->attendence_pick_up == 1){
                $pick_up = "checked";
            }

            if($get_vehicle_attendence->attendence_drop == 1){
                $drop = "checked";
            }

            if($get_vehicle_attendence != ""){
                if($get_vehicle_attendence->vehicle_attendence_type == 1){

                } else if($get_vehicle_attendence->vehicle_attendence_type == 0){
                    $disabled = "disabled";
                }
            }

            $overtime_type = '
                <div counter= "'.$vehicle_attendance->map_student_staff_id.'"  class="red float-left checkbox" style="margin-top:6px !important;">
                    <input id="overtime_type_status2'.$vehicle_attendance->map_student_staff_id.'" name="attendance['.$vehicle_attendance->map_student_staff_id.'][pick_up]" '.$pick_up.' '.$disabled.'  type="checkbox" value="1" />
                    <label for="overtime_type_status2'.$vehicle_attendance->map_student_staff_id.'" class="document_staff">Pick Up</label> 
                </div>
                <div counter= "'.$vehicle_attendance->map_student_staff_id.'"  class="green float-left checkbox" style="margin-top:6px !important;">
                <input  id="overtime_type_status1'.$vehicle_attendance->map_student_staff_id.'" name="attendance['.$vehicle_attendance->map_student_staff_id.'][drop]" '.$drop.' '.$disabled.' type="checkbox" value="1" class="check">
                    <label for="overtime_type_status1'.$vehicle_attendance->map_student_staff_id.'" class="document_staff">Drop</label>
                </div>
                
                ';
            return $overtime_type;
        })

        ->rawColumns(['action' => 'action','student_staff_type'=>'student_staff_type','overtime_type'=> 'overtime_type', 'attendance'=> 'attendance' ,'overtime_value'=> 'overtime_value'])->addIndexColumn()->make(true);

        
    }


    /**
     *  View page vehicle view attendence 
     *  @Sandeep on 8 Feb 2019
    **/
    public function view_attendence($id)
    {
        $decrypted_vehicle_id = get_decrypted_value($id, true);
        $vehicle  = Vehicle::where(function($query) use ($request,$decrypted_vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$decrypted_vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];
            
        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/vehicle-attendance')->withError('Vehicle not found!');
        }

        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_attendence'),
            'login_info'    => $loginInfo,
            'vehicle'       => $vehicle,
            'decrypted_vehicle_id' => $id,
        );
        return view('admin-panel.vehicle.view_attendence')->with($data);
    }

    /**
     *  Save page vehicle add attendence 
     *  @Sandeep on 9 Feb 2019
    **/
    public function save_attendence(Request $request){

        $loginInfo  = get_loggedin_user_data();
        $admin_id   = $loginInfo['admin_id'];
        $session    = get_current_session();
        $vehicle_id = Input::get('vehicle_id');
        $decrypted_vehicle_attendance_id  = get_decrypted_value($id, true);
        $decrypted_vehicle_id = get_encrypted_value($vehicle_id, true);
        $total_present_staff = $total_present_student = $total_strength = 0;
               
        if (!empty($id))
        {
            $attendance = VehicleAttendence::find($decrypted_vehicle_attendance_id);
            $admin_id   = $attendance['admin_id'];
            if (!$attendance)
            {
                return redirect()->back()->withError('Attendance not found!');
            }
            $success_msg = 'Attendancee updated successfully!';
        }
        else
        {
            $attendance     = New VehicleAttendence;
            $success_msg = 'Attendance saved successfully!';
        }

        DB::beginTransaction();
        try
        {
            $attendance = VehicleAttendence::where('vehicle_attendence_date',Input::get('attendance_date'))->where('vehicle_id',Input::get('vehicle_id'))->first();
            if(empty($attendance)){
                $attendance     = New VehicleAttendence;
            }
            $attendance->admin_id       = $admin_id;
            $attendance->update_by      = $loginInfo['admin_id'];
            $attendance->session_id     = $session['session_id'];
            $attendance->vehicle_id     = Input::get('vehicle_id');
            $attendance->vehicle_attendence_date  = Input::get('attendance_date');
            $attendance->save();

            foreach($request->get('attendance') as $details){ 

                if($details['student_staff_type'] == 1 && $details['attendance'] == 1){
                    $total_present_student++;
                }

                if($details['student_staff_type'] == 2 && $details['attendance'] == 1){
                    $total_present_staff++;
                }

                $total_strength++;
                
                if(isset($details['vehicle_attend_d_id']) ) {
                    
                    $staff_attend = VehicleAttendDetails::where('vehicle_attend_d_id', $details['vehicle_attend_d_id'])->first();
                    $staff_attend->admin_id                 = $admin_id;
                    $staff_attend->update_by                = $loginInfo['admin_id'];
                    $staff_attend->session_id               = $session['session_id'];
                    $staff_attend->vehicle_id               = Input::get('vehicle_id');
                    $staff_attend->vehicle_attendence_id    = $attendance->vehicle_attendence_id;
                    $staff_attend->vehicle_attendence_type  = $details['attendance'];
                    $staff_attend->student_staff_id         = $details['student_staff_id'];
                    $staff_attend->student_staff_type       = $details['student_staff_type'];
                    $staff_attend->attendence_pick_up       = $details['pick_up'];
                    $staff_attend->attendence_drop          = $details['drop'];
                    $staff_attend->vehicle_attendence_date  = Input::get('attendance_date');                    
                    $staff_attend->save();
                } else {
                    $staff_attend = New VehicleAttendDetails();
                    $staff_attend->admin_id                 = $admin_id;
                    $staff_attend->update_by                = $loginInfo['admin_id'];
                    $staff_attend->session_id               = $session['session_id'];
                    $staff_attend->vehicle_id               = Input::get('vehicle_id');
                    $staff_attend->vehicle_attendence_id    = $attendance->vehicle_attendence_id;
                    $staff_attend->vehicle_attendence_type  = $details['attendance'];
                    $staff_attend->student_staff_id         = $details['student_staff_id'];
                    $staff_attend->student_staff_type       = $details['student_staff_type'];
                    $staff_attend->attendence_pick_up       = $details['pick_up'];
                    $staff_attend->attendence_drop          = $details['drop'];
                    $staff_attend->vehicle_attendence_date  = Input::get('attendance_date'); 
                    $staff_attend->save();
                }
            }
   
            $attendance->total_present_student     = $total_present_student;
            $attendance->total_present_staff       = $total_present_staff;
            $attendance->total_strength            = $total_strength;
            $attendance->save();
            
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/transport/vehicle/view-attendance/'.$decrypted_vehicle_id)->withSuccess($success_msg);
    }


    /**
     *  View page vehicle attendence records 
     *  @Sandeep on 9 Feb 2019
    **/
    public function attendance_records(Request $request){

        $arr_attendance   = [];
        $session    = get_current_session();
        $loginInfo = get_loggedin_user_data();
        
        $arr_attendance   = VehicleAttendence::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            $query->where('vehicle_id', "=", $request->vehicle_id);
            if (!empty($request) && !empty($request->has('vehicle_attendance')) && $request->get('vehicle_attendance') != null)
            {
                $query->where('vehicle_attendence_date', "=", $request->get('vehicle_attendance'));
            }
        })->orderBy('vehicle_attendence_date', 'DESC')->get();
       
        return Datatables::of($arr_attendance) 
            ->addColumn('vehicle_attendence_date', function ($arr_attendance)
            {
                return date('d M Y', strtotime($arr_attendance->vehicle_attendence_date));
            })
            
            ->addColumn('action', function ($arr_attendance) use ($request,$session) 
            {
                $vehicle_attendence_id = get_encrypted_value($arr_attendance->vehicle_attendence_id, true);
                $vehicle_id            = get_encrypted_value($request->vehicle_id, true);
                
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit">
                        <a href="'.url('admin-panel/transport/vehicle/add-attendance/'.$vehicle_id.'/'.$vehicle_attendence_id.' ').'"><i class="zmdi zmdi-edit"></i></a>
                    </div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View" style="margin-top: 10px !important;">
                        <a href="'.url('admin-panel/transport/vehicle/view-attendance-details/'.$vehicle_id.'/'.$vehicle_attendence_id.' ').'"><i class="zmdi zmdi-eye"></i></a>
                    </div>
                    ';
            })
            
            ->rawColumns(['vehicle_attendence_date'=>'vehicle_attendence_date',  'action'=> 'action'])->addIndexColumn()->make(true);
    }


    /**
     *  Vehicle attendence Details 
     *  @Sandeep on 9 Feb 2019
    **/
    public function vehicle_attendence_details($vehicle_id,$vehicle_attendence_id)
    {
        $loginInfo       = get_loggedin_user_data();

        $vehicle_id             = get_decrypted_value($vehicle_id, true);
        $vehicle_attendence_id  = get_decrypted_value($vehicle_attendence_id, true);

        $vehicle  = Vehicle::where(function($query) use ($request,$vehicle_id) 
        {
            $query->where('vehicles.vehicle_id','=',$vehicle_id);
        })
        ->join('assign_driver_conductor_routes', function($join) {
            $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
        })
        ->with('getDriver')
        ->with('getConductor')
        ->first();
        $vehicle              = isset($vehicle) ? $vehicle : [];

        if (!$vehicle){
            return redirect('admin-panel/transport/vehicle/vehicle-attendance')->withError('Vehicle not found!');
        }

        $data = array(
            'page_title'                => trans('language.vehicle_attendence_details'),
            'login_info'                => $loginInfo,
            'vehicle'                   => $vehicle,
            'vehicle_id'                => $vehicle_id,
            'vehicle_attendence_id'     => $vehicle_attendence_id,
        );
        return view('admin-panel.vehicle.vehicle_attendence_details')->with($data);
    }


    /**
     *  Get Data for vehicle Attendence Details (Datatables)
     *  @Sandeep on 9 Feb 2019
    **/
    public function vehicle_attendence_details_data(Request $request)
    {
        $arr_staff   = [];
        $vehicle_attendance = VehicleAttendDetails::where(['vehicle_id' => $request->vehicle_id , 'vehicle_attendence_id' => $request->vehicle_attendence_id])->get();

        return Datatables::of($vehicle_attendance)
        ->addColumn('student_staff_name', function ($vehicle_attendance)
        {
            if($vehicle_attendance->student_staff_type == 1){
                $student_staff_data = Student::where('student_id',$vehicle_attendance->student_staff_id)->first();
                $student_staff_data->student_staff_name = $student_staff_data->student_name;
            } else {
                $student_staff_data = Staff::where('staff_id',$vehicle_attendance->student_staff_id)->first(); 
                $student_staff_data->student_staff_name = $student_staff_data->staff_name;
            }
            return $student_staff_data->student_staff_name;
            
        })
        ->addColumn('student_staff_type_new', function ($vehicle_attendance)
        {
            if($vehicle_attendance->student_staff_type == 1 ){
                return "Student";
            } else {
                return "Staff";
            }            
        })

        ->addColumn('attendance', function ($vehicle_attendance) use ($request)
        {
            $present = $absent = '';
            //$present = "checked";


            if($vehicle_attendance->vehicle_attendence_type == 1){
                $present = "checked";
            } else if($vehicle_attendance->vehicle_attendence_type == 0){
                $absent = "checked";
            }
            $attendance = '
                <div class="green float-left attendence_type" style="margin-top:6px !important;" counter= "'.$vehicle_attendance->map_student_staff_id.'">
                    <input '.$present.' type="radio" value="1" disabled>
                    <label class="document_staff">Present</label>
                </div>
                <div class="red float-left attendence_type" style="margin-top:6px !important;" counter= "'.$vehicle_attendance->map_student_staff_id.'">
                    <input '.$absent.' type="radio" value="0" disabled/>
                    <label class="document_staff">Absent</label> 
                </div>
                ';
            return $attendance;
        })

        ->rawColumns(['action' => 'action','student_staff_type'=>'student_staff_type','overtime_type'=> 'overtime_type', 'attendance'=> 'attendance' ,'overtime_value'=> 'overtime_value'])->addIndexColumn()->make(true);
    }

    /**
     *  View page for attendence report 
     *  @Sandeep on 11 Feb 2019
    **/
    public function attendence_report()
    {
        $loginInfo      = get_loggedin_user_data();
        $arr_vehicle    = get_all_vehicle_list();
        $list['arr_vehicle']        = add_blank_option($arr_vehicle, 'Select Vehicle');
        $data = array(
            'page_title'    => trans('language.attendence_report'),
            'login_info'    => $loginInfo,
            'list'          => $list
        );
        return view('admin-panel.vehicle.attendence_report')->with($data);
    }


    /**
     *  View page report attendence records 
     *  @Sandeep on 11 Feb 2019
    **/
    public function report_attendance_records(Request $request){

        $arr_attendance   = [];
        $session    = get_current_session();
        $loginInfo = get_loggedin_user_data();

        $arr_attendance   = VehicleAttendence::join('vehicles', function($join) {
            $join->on('vehicle_attendence.vehicle_id', '=', 'vehicles.vehicle_id');
        })->where(function($query) use ($request,$session) {
        
            $query->where('session_id', "=", $session['session_id']);
            // $query->where('vehicle_id', "=", $request->vehicle_id);
            if (!empty($request) && !empty($request->has('vehicle_name')) && $request->get('vehicle_name') != null)
            {
                $query->where('vehicles.vehicle_name', "like", "%{$request->get('vehicle_name')}%");
            }
            if (!empty($request) && !empty($request->has('vehicle_id')) && $request->get('vehicle_id') != null)
            {
                $query->where('vehicle_attendence.vehicle_id', "=", $request->get('vehicle_id'));
            }
            if (!empty($request) && !empty($request->has('vehicle_attendance')) && $request->get('vehicle_attendance') != null)
            {
                $query->where('vehicle_attendence_date', "=", $request->get('vehicle_attendance'));
            }

        })->orderBy('vehicle_attendence_date', 'DESC')->get();


        return Datatables::of($arr_attendance) 
            ->addColumn('vehicle_attendence_date', function ($arr_attendance)
            {
                return date('d M Y', strtotime($arr_attendance->vehicle_attendence_date));
            })
            ->addColumn('vehicle_name', function ($arr_attendance)
            {
                return $arr_attendance['GetVehicle']->vehicle_name;
            })
            ->addColumn('action', function ($arr_attendance) use ($request,$session) 
            {
                $vehicle_attendence_id = get_encrypted_value($arr_attendance->vehicle_attendence_id, true);
                $vehicle_id            = get_encrypted_value($arr_attendance['GetVehicle']->vehicle_id, true);
                
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View" style="margin-top: 10px !important;">
                        <a href="'.url('admin-panel/transport/vehicle/view-attendance-details/'.$vehicle_id.'/'.$vehicle_attendence_id.' ').'"><i class="zmdi zmdi-eye"></i></a>
                    </div>
                    ';
            })
            
            ->rawColumns(['vehicle_attendence_date'=>'vehicle_attendence_date',  'action'=> 'action'])->addIndexColumn()->make(true);
    }




}




