<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Brochure\Brochure;

use Yajra\Datatables\Datatables;

class BrochureController extends Controller
{
    /**
     *  View page for brochures
     *  @Shree on 10 Sept 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $brochure = [];
        $brochure['arr_session'] = $arr_sessions;
        $data = array(
            'page_title'    => trans('language.view_brochure'),
            'redirect_url'  => url('admin-panel/brochure/view-brochures'),
            'login_info'    => $loginInfo,
            'brochure'      => $brochure
        );
        return view('admin-panel.brochure.index')->with($data);
    }

    /**
     *  Add page for brochure
     *  @Shree on 10 Sept 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    = [];
        $brochure = [];
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        if (!empty($id))
        {
            $decrypted_brochure_id = get_decrypted_value($id, true);
            
            $brochure              = Brochure::Find($decrypted_brochure_id);
            if (!$brochure)
            {
                return redirect('admin-panel/brochure/add-brochure')->withError('Brochure not found!');
            }
            if (!empty($brochure->brochure_file)){
                $brochure_file = check_file_exist($brochure->brochure_file, 'brochure_file');
                if(!empty($brochure_file)){
                    $brochure->brochure_file1 = $brochure_file;
                } 
            }  
            $page_title           = trans('language.edit_brochure');
            $decrypted_brochure_id = get_encrypted_value($brochure->brochure_id, true);
            $save_url             = url('admin-panel/brochure/save/' . $decrypted_brochure_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_brochure');
            $save_url      = url('admin-panel/brochure/save');
            $submit_button = 'Save';
        }
        $brochure['arr_session'] = $arr_sessions;
       
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'brochure'       => $brochure,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/brochure/view-brochures'),
        );
        return view('admin-panel.brochure.add')->with($data);
    }

    
    /**
     *  Add and update brochure's data
     *  @Shree on 10 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo              = get_loggedin_user_data();
        $decrypted_brochure_id  = get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $brochure = Brochure::find($decrypted_brochure_id);
            $admin_id = $brochure['admin_id'];
            if (!$brochure)
            {
                return redirect('/admin-panel/brochure/add-brochure/')->withError('Brochure not found!');
            }
            $success_msg = 'Brochure updated successfully!';
        }
        else
        {
            $brochure     = New Brochure;
            $success_msg = 'Brochure saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'brochure_name'   => 'required|unique:brochures,brochure_name,' . $decrypted_brochure_id . ',brochure_id',
                'session_id'      => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $brochure->admin_id              = $admin_id;
                $brochure->update_by             = $loginInfo['admin_id'];
                $brochure->session_id            = Input::get('session_id');
                $brochure->brochure_name         = Input::get('brochure_name');
                $brochure->brochure_upload_date  = date('Y-m-d H:i:s');
                if ($request->hasFile('brochure_file'))
                {
                    if (!empty($id)){
                        $brochure_file = check_file_exist($brochure->brochure_file, 'brochure_file');
                        if (!empty($brochure_file))
                        {
                            unlink($brochure_file);
                        } 
                    }
                    $file                          = $request->file('brochure_file');
                    $config_upload_path            = \Config::get('custom.brochure_file');
                    $destinationPath               = public_path() . $config_upload_path['upload_path'];
                    $ext                           = substr($file->getClientOriginalName(),-4);
                    $name                          = substr($file->getClientOriginalName(),0,-4);
                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                    $file->move($destinationPath, $filename);
                    $brochure->brochure_file = $filename;
                }
                $brochure->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/brochure/view-brochures')->withSuccess($success_msg);
    }


    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 10 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $brochure = Brochure::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('brochure_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('brochure_id', 'DESC')->with('getSessionInfo')->get();
        // p($brochure);
        if (!empty($brochure['brochure_file'])){
            $brochure_file = check_file_exist($brochure['brochure_file'], 'brochure_file');
            if (!empty($brochure_file))
            {
                unlink($brochure_file);
            } 
        }
        return Datatables::of($brochure)
        ->addColumn('brochure_file', function ($brochure)
        {
            $brochure_file = '---';
            if (!empty($brochure->brochure_file)){
                $brochure_file = check_file_exist($brochure->brochure_file, 'brochure_file');
                if(!empty($brochure_file)){
                    return '<div class="" data-toggle="tooltip" title="View Brochure File"><a href="../../' . $brochure_file . '" target="_blank">View Brochure File</div>';
                } else {
                    return '----';
                }
            } else {
                return '----';
            }
           
        }) 

        ->addColumn('session_name', function ($brochure)
        {
           return $brochure->getSessionInfo['session_name'];
        }) 
        ->addColumn('brochure_upload_date', function ($brochure)
        {
           return date("d F Y", strtotime($brochure->brochure_upload_date));
        }) 
        ->addColumn('action', function ($brochure)
        {
            $encrypted_brochure_id = get_encrypted_value($brochure->brochure_id, true);
            if($brochure->brochure_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="brochure-status/'.$status.'/' . $encrypted_brochure_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-brochure/' . $encrypted_brochure_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-brochure/' . $encrypted_brochure_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['brochure_file' => 'brochure_file','action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy brochure's data
     *  @Shree on 10 Sept 2018
    **/
    public function destroy($id)
    {
        $brochure_id = get_decrypted_value($id, true);
        $brochure    = Brochure::find($brochure_id);
        if ($brochure)
        {
            if (!empty($brochure['brochure_file'])){
                $brochure_file = check_file_exist($brochure['brochure_file'], 'brochure_file');
                if (!empty($brochure_file))
                {
                    unlink($brochure_file);
                } 
            }
            $brochure->delete();
            $success_msg = "Brochure deleted successfully!";
            return redirect('admin-panel/brochure/view-brochures')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Brochure not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change brochure's status
     *  @Shree on 10 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $brochure_id = get_decrypted_value($id, true);
        $brochure    = Brochure::find($brochure_id);
        if ($brochure)
        {
            $brochure->brochure_status  = $status;
            $brochure->save();
            $success_msg = "Brochure status update successfully!";
            return redirect('admin-panel/brochure/view-brochures')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Brochure not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

     /**
     *  Get brochure data according session id
     *  @Shree on 12 Sept 2018
    **/
    public function getBrochureData()
    {
        $session_id = Input::get('session_id');
        $brochures = get_all_brochure($session_id);
        $data = view('admin-panel.brochure.ajax-brochure-select',compact('brochures'))->render();
        return response()->json(['options'=>$data]);
    }
}
