<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Bonus; // Model
use App\Model\Payroll\BonusMap; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollBonusController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Bonus
     * @Khushbu on 15 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $bonus = $staff = $staff_mapped = $staff_ids_arr = []; 
        $loginInfo   = get_loggedin_user_data();
        $staff       = Staff::get(); 
        if(!empty($id)) {
	 		$decrypted_bonus_id 	= get_decrypted_value($id, true);
        	$bonus      		    = Bonus::Find($decrypted_bonus_id);
            $page_title             = trans('language.edit_bonus');
        	$save_url    			= url('admin-panel/payroll/manage-bonus-save/'. $id);
            $submit_button  		= 'Update';
            $staff_mapped           = BonusMap::select('staff_id')->where('pay_bonus_id', $decrypted_bonus_id)->get()->toArray();
            foreach($staff_mapped as $key => $value){
                $staff_ids_arr[] = $value['staff_id'];
            }
	 	} else {
            $page_title             = trans('language.add_bonus');
	 		$save_url    			= url('admin-panel/payroll/manage-bonus-save');
	 		$submit_button  		= 'Save';
	 	}
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'bonus'           => $bonus,
            'staff'           => $staff,
            'staff_ids_arr'   => $staff_ids_arr,    
        );
        return view('admin-panel.payroll-bonus.add')->with($data);
    }

    /**
     *  Add & Update of Payroll Bonus
     *  @Khushbu on 16 Feb 2019
     */
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_bonus_id		    = get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $session                    = get_current_session();
        if(!empty($id)) {
            $bonus              = Bonus::Find($decrypted_bonus_id);
            if(!$bonus) {
                return redirect('admin-panel/payroll/manage-bonus')->withErrors('Bonus not found!');
            }
            $admin_id    = $bonus->admin_id;
            $success_msg = 'Bonus updated successfully!';
        } else {
            $bonus    	     = New Bonus;
            $success_msg     = 'Bonus saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'bonus_name'    	   => 'required|unique:pay_bonus,bonus_name,' . $decrypted_bonus_id . ',pay_bonus_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
                $success_msg = 'Staffs are not selected.';
                $error_messages = [];

            DB::beginTransaction();
            try
            {
                $bonus->admin_id         = $admin_id;
                $bonus->update_by        = $loginInfo['admin_id'];
                $bonus->bonus_name   	 = Input::get('bonus_name');
                $bonus->bonus_amount     = Input::get('bonus_amount');
                $bonus->save();

                foreach ($request->get('staffs') as $value){
                    if($value['exist'] == 1 && !isset($value['staff_id']))
                    {
                        // Delete Case
                        $success_msg = "Record Updated successfully";
                        $bonus_map  = BonusMap::where('staff_id',$value['exist_id'])->first();
                        $bonus_map->delete();
                
                    }
                    if($value['exist'] == 0 && isset($value['staff_id']))
                    {
                        //Add Case
                        $bonus_map         = New BonusMap;
                        $success_msg       = 'Staff Mapped successfully.';
                        $bonus_map->admin_id         = $admin_id;
                        $bonus_map->update_by        = $loginInfo['admin_id'];
                        $bonus_map->session_id 	     = $session['session_id'];
                        $bonus_map->pay_bonus_id 	 = $bonus->pay_bonus_id;
                        $bonus_map->staff_id   	     = $value['staff_id'];
                        $bonus_map->save();
                    }    
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            }
        }
        return redirect('admin-panel/payroll/manage-bonus')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Bonus's Data fo view page
     *  @Khushbu on 16 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
    	$bonus 		    = Bonus::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_bonus_name') !=  NULL)
            {
                $query->where('bonus_name', 'Like', $request->get('s_bonus_name').'%');
            }
        })->orderBy('pay_bonus_id','DESC')->with('BonusMap')->get();
        return Datatables::of($bonus)
        ->addColumn('staff_mapped', function($bonus) {
            $encrypted_bonus_id  = get_encrypted_value($bonus->pay_bonus_id, true);
            $map_staff = '<a href="" class="btn btn-raised btn-primary bonus-staff" data-toggle="modal" data-target="#viewStaffModel" bonus_id='.$bonus->pay_bonus_id.'>View Staff</a>';
            return $map_staff;
        })
    	->addColumn('action', function($bonus) use($request) {
            $encrypted_bonus_id  = get_encrypted_value($bonus->pay_bonus_id, true);
              return '
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-bonus/'.$encrypted_bonus_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-bonus/' . $encrypted_bonus_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['action' => 'action', 'staff_mapped' => 'staff_mapped'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-bonus');
    }  

    /**
	 *	Destroy Data of Payroll Bonus
     *  @Khushbu on 16 Feb 2019
	**/
	public function destroy($id) {
        $bonus_id 	= get_decrypted_value($id, true);
        $bonus_map  = BonusMap::where('pay_bonus_id',$bonus_id)->get();
        $bonus 	    = Bonus::find($bonus_id);
        if ($bonus)
        {
            DB::beginTransaction();
            try
            {
                foreach ($bonus_map as $key => $value) {
                    $bonus_map[$key]->delete();
                }
                $bonus->delete();
                $success_msg = "Bonus deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Bonus not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page of Staff
     *  @Khushbu on 16 Feb 2019
     */
    // public function viewMapEmployees($id) {
    //     // $arr_arrear          = [];
    //     $loginInfo      	 = get_loggedin_user_data();
    //     $bonus_id 		     = get_decrypted_value($id, true);
    //     $page_title          = trans('language.map_employees');
    //     // $arr_arrear          = get_all_arrear();
    //     // $arr_arrear_list     = add_blank_option($arr_arrear,'Select Arrear');
    //     $data                = array(
    //         'loginInfo'      => $loginInfo,
    //         'page_title'     => $page_title,
    //         'save_url'		 => url('admin-panel/payroll/manage-bonus-map-employees-save/'.$id),
    //         'bonus_id'       => $bonus_id,
    //     );
    //     return view('admin-panel.payroll-bonus.map-employees')->with($data);
    // } 

    /**
     * Get Employee's Map Data for view page
     *  @Khushbu on 16 Feb 2019 
     */
    // public function anyDataMapEmployees(Request $request) {  
    //     $staff_mapped = BonusMap::select('staff_id')->where('pay_bonus_id', $request->get('bonus_id'))->get()->toArray();
    //     $staff = Staff::get(); 
    //     $staff_ids_arr 	    = array();
    //     foreach($staff_mapped as $key => $value){
    //         $staff_ids_arr[] = $value['staff_id'];
    //     }
    //     return Datatables::of($staff, $staff_ids_arr)
    //     ->addColumn('checkbox', function($staff) use($staff_ids_arr){
    //         $check = '';
    //         $exist = 0;
    //         if(!empty($staff_ids_arr)){
    //             if(in_array($staff->staff_id, $staff_ids_arr)){
    //                 $check = 'checked';
    //                 $exist = 1;
    //             }
    //         }
    //         return '
    //             <input type="hidden" name="staffs['.$staff->staff_id.'][exist]" value="'.$exist.'" >
    //             <input type="hidden" name="staffs['.$staff->staff_id.'][exist_id]" value="'.$staff->staff_id.'" >

    //             <div class="checkbox" id="customid">
    //                 <input type="checkbox" id="staff'.$staff->staff_id.'" name="staffs['.$staff->staff_id.'][staff_id]" class="check" value="'.$staff->staff_id.'" '.$check.'>
    //                 <label class="from_one1 " style="margin-bottom: 4px !important;"  for="staff'.$staff->staff_id.'"></label>
    //             </div>';
    //     })
    //     ->addColumn('employee_profile', function($staff) {
    //         $profile = '';
    //         if($staff->staff_profile_img != '') {
    //             $staff_profile_path = check_file_exist($staff->staff_profile_img, 'staff_profile');
    //             $profile = "<img src=".url($staff_profile_path)." height='30' />";
    //         }
    //         $staff_name = $staff->staff_name;
    //         $employee_profile = $profile." ".$staff_name;
    //         return $employee_profile;
    //     })
    //     ->addColumn('previous_bonus', function($staff) {
    //         $previous_bonus =  2000;
    //         return $previous_bonus;
    //     })->rawColumns(['checkbox' => 'checkbox', 'employee_profile' => 'employee_profile','previous_bonus' => 'previous_bonus'])->addIndexColumn()
    //     ->make(true); 
    //     return redirect('/payroll/manage-bonus');
    // }

    /**
     *  Add Page of Employee's Map 
     *  @Khushbu on 16 Feb 2019 
     */
    // public function saveMapEmployees(Request $request, $id= NULL) {
    //     $loginInfo      = get_loggedin_user_data();
    //     $session        = get_current_session();
    //     $admin_id       = $loginInfo['admin_id'];
    //     $bonus_id 		= get_decrypted_value($id, true);
    //     if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
    //         $success_msg = 'Staffs are not selected.';
    //         $error_messages = [];
    //         DB::beginTransaction();
    //         try
    //         {
    //             foreach ($request->get('staffs') as $value){
    //                 if($value['exist'] == 1 && !isset($value['staff_id']))
    //                 {
    //                     // Delete Case
    //                     $success_msg = "Record Updated successfully";
    //                     $bonus_map   = BonusMap::where('staff_id',$value['exist_id'])->first();
    //                     $bonus_map->delete();
                
    //                 }
    //                 if($value['exist'] == 0 && isset($value['staff_id']))
    //                 {
    //                     //Add Case
    //                     $bonus_map     = New BonusMap;
    //                     $success_msg = 'Staff Mapped successfully.';
    //                     $bonus_map->admin_id      = $admin_id;
    //                     $bonus_map->update_by     = $loginInfo['admin_id'];
    //                     $bonus_map->session_id 	  = $session['session_id'];
    //                     $bonus_map->pay_bonus_id  = $bonus_id ;
    //                     $bonus_map->staff_id   	  = $value['staff_id'];
    //                     $bonus_map->save();
    //                 }    
    //             }
    //         }
    //         catch (\Exception $e)
    //         {
    //             //failed logic here
    //             DB::rollback();
    //             $error_message = $e->getMessage();
    //             return redirect()->back()->withErrors($error_message);
    //         }
    //         DB::commit();
    //         return redirect()->back()->withSuccess($success_msg)->withErrors($error_messages);   
    //     } else {
    //         return redirect()->back();
    //     }         
    // }

    /** 
     *  Get Staff Map Data
     *  @Khushbu on 18 Jan 2019.
    **/
    public function getBonusStaffData(Request $request) {
        $bonus_staff_map        = BonusMap::where('pay_bonus_id',$request->get('bonus_id'))->with('BonusStaff')->get();
        return Datatables::of($bonus_staff_map)
        ->addColumn('employee_profile', function($bonus_staff_map) {
        $profile = '';
        if($bonus_staff_map['BonusStaff']['staff_profile_img'] != '') {
            $staff_profile_path = check_file_exist($bonus_staff_map['BonusStaff']['staff_profile_img'], 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $bonus_staff_map['BonusStaff']['staff_name'];
        $employee_profile = $profile." ".$staff_name;
        return $employee_profile;
        })
        ->addColumn('previous_bonus', function($bonus_staff_map) {
            $previous_arrear =  2000;
            return $previous_arrear;
        })->rawColumns(['employee_profile' => 'employee_profile','previous_bonus' => 'previous_bonus'])->addIndexColumn()
        ->make(true); 
    } 
}
