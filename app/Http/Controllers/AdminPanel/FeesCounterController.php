<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use App\Model\Student\ImprestAc;
use App\Model\Student\WalletAc; 
use App\Model\FeesCollection\Receipt;
use App\Model\FeesCollection\ReceiptDetail;
use App\Model\FeesCollection\ConcessionMap;
use App\Model\FeesCollection\STCheques;
use App\Model\FeesCollection\AcEntryMap;

class FeesCounterController extends Controller
{
    /**
     *  View page for heads listing
     *  @shree on 13 Oct 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = $arr_section = [];
        $arr_class = get_all_classes_mediums();
        $listData['arr_class'] = add_blank_option($arr_class, "Select Class");
        $listData['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title' => trans('language.fees_counter'),
            'redirect_url' => url('admin-panel/fees-counter'),
            'login_info' => $loginInfo,
            'listData' => $listData,
        );
        return view('admin-panel.fees-counter.index')->with($data);
    }

    /**
     *  Get student list for fees counter
     *  @Shree on 15 Oct 2018
     **/
    public function get_student_list(Request $request)
    {
        $session = get_current_session();
        $student = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null)) {
            $request->request->add(['student_type', 'rte_apply_status']);
            $request->merge(['student_type' => '0', 'rte_apply_status' => '0']);

            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }

        foreach ($arr_students as $key => $arr_student) {
            $student[$key] = (object) $arr_student;
        }

        return Datatables::of($student)
            ->addColumn('student_profile', function ($student) {
                $profile = "";
                if ($student->profile != '') {
                    $profile = "<img src=" . $student->profile . " height='30' />";
                }
                $name = $student->student_name;
                $complete = $profile . "  " . $name;
                return $complete;
            })
            ->addColumn('action', function ($student) {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);

                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Add" href="fees-counter/add/' . $encrypted_student_id . '" "">Add</a>
                    </li>

                    </ul>
                </div>';

            })
            ->rawColumns(['action' => 'action', 'student_profile' => 'student_profile'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for fees counter
     *  @Shree on 15 Oct 2018
     **/
    public function add(Request $request, $id = null)
    {
        $student = [];
        $class_id = null;
        $admit_class_id = null;
        $loginInfo = get_loggedin_user_data();
       
        if (!empty($id)) {
            $decrypted_student_id = get_decrypted_value($id, true);
            $students = get_student_basic_details_with_siblings($decrypted_student_id);
        } else {
            $error_message = "student not found!";
            return redirect()->back()->withErrors($error_message);
        }

        $save_url = url('admin-panel/fees-counter/save-fees-data/' . $id);
        $data = array(
            'page_title' => trans('language.fees_counter'),
            'redirect_url' => url('admin-panel/fees-counter'),
            'login_info' => $loginInfo,
            'students' => $students,
            'save_url' => $save_url,
        );
        // p($students);
        return view('admin-panel.fees-counter.fee_counter')->with($data);
    }

    /**
     *  Add page for fees counter
     *  @Shree on 15 Oct 2018
     **/
    public function save_fees_data(Request $request, $id = null)
    {
        $receipt            = New Receipt();
        $session = get_current_session();
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $decrypted_student_id   = get_decrypted_value($id, true);
        // p($request->all());
        if(!empty($id)) {
            $student_info = get_student_signle_data($decrypted_student_id);
            $student_info = isset($student_info[0]) ? $student_info[0] : [];
            $imprestAc    = ImprestAc::where('student_id', $decrypted_student_id)->first();
            $walletAc    = WalletAc::where('student_id', $decrypted_student_id)->first();
            DB::beginTransaction(); //Start transaction!

            try
            {
                $fine_amt = null;
                $fine_calculate_amt = null;
                $concession_amt = null;
                $wallet_amt = null;
                $pay_lator_amt = null;
                $bank_id = null;
                $total_amount = 0;
                $imprest_amt = null;
                if($request->get('fine_use') != null && $request->get('fine_use') == 1){
                    $fine_amt = $request->get('fine_actual_amt');
                    $fine_calculate_amt = $request->get('fine_head_amt');
                }
                if($request->get('concession_use') != null && $request->get('concession_use') == 1){
                    $concession_amt = $request->get('concession_final_amt');
                   
                }
                if($request->get('wallet_use') != null && $request->get('wallet_use') == 1){
                    $wallet_amt = $request->get('wallet_calculate_amt');
                    $total_wallet_amt = 0;
                    $total_wallet_amt = $walletAc->wallet_ac_amt - $wallet_amt; 
                    $walletAc->wallet_ac_amt = $total_wallet_amt;
                    $walletAc->save();
                    
                }
                if($request->get('pay_lator_use') != null && $request->get('pay_lator_use') == 1){
                    $pay_lator_amt = $request->get('pay_lator_amt_hide');
                }
                if($request->get('imprest_use') != null && $request->get('imprest_use') == 1){
                    $imprest_amt = $request->get('imprest_amt_hide');
                    $imprest_ac_amt = 0;
                    $imprest_ac_amt = $imprestAc->imprest_ac_amt + $imprest_amt; 
                    $imprestAc->imprest_ac_amt = $imprest_ac_amt;
                    $imprestAc->save();
                }
              
                $total_amount = $request->get('pay_final_calculate_amt') + $concession_amt + $wallet_amt; 
                $pay_amount = $request->get('pay_final_amt'); 
                $receipt->admin_id                      = $admin_id;
                $receipt->update_by                     = $loginInfo['admin_id'];
                $receipt->session_id                    = $student_info['current_session_id'];
                $receipt->class_id                      = $student_info['current_class_id'];
                $receipt->student_id                    = $decrypted_student_id;
                $receipt->receipt_date                  = $request->get('select_date');
                $receipt->total_amount                  = $pay_amount;
                $receipt->wallet_amount                 = $wallet_amt;
                $receipt->total_concession_amount       = $concession_amt;
                $receipt->total_fine_amount             = $fine_amt;
                $receipt->total_calculate_fine_amount   = $fine_calculate_amt;
                $receipt->net_amount                    = $total_amount;
                $receipt->pay_later_amount              = $pay_lator_amt;
                $receipt->imprest_amount                = $imprest_amt;
                $receipt->bank_id                       = $bank_id;
                $receipt->pay_mode                      = $request->get('pay_mode');
                $receipt->cheque_date                   = $request->get('cheque_date');
                $receipt->cheque_number                 = $request->get('cheque_number');
                $receipt->cheque_amt                    = $request->get('cheque_amt');
                $receipt->transaction_id                = $request->get('transaction_id');
                $receipt->transaction_date              = $request->get('transaction_date');
                $receipt->description                   = $request->get('description');
                
                $receipt->save();
                
                $receipt->receipt_number                = $receipt->receipt_id;
                $receipt->save();

                if($request->get('pay_mode') != null && $request->get('pay_mode') == 'cheque'){
                    $bank_id = $request->get('bank_id');
                    $cheque   = New STCheques();
                    $cheque->admin_id               = $admin_id;
                    $cheque->update_by              = $loginInfo['admin_id'];
                    $cheque->student_id             = $decrypted_student_id;
                    $cheque->bank_id                = $bank_id;
                    $cheque->cheques_session_id     = $session['session_id'];
                    $cheque->cheques_class_id       = $student_info['current_class_id'];
                    $cheque->cheques_receipt_id     = $receipt->receipt_id;
                    $cheque->student_cheque_no      = $request->get('cheque_number');
                    $cheque->student_cheque_date    = $request->get('cheque_date');
                    $cheque->student_cheque_amt     = $request->get('cheque_amt');
                    $cheque->save();
                }
                $headCounter = 0;
                foreach($request->get('pay') as $feesHeads){
                    if(isset($feesHeads['head_id'])){
                        $headCounter++;
                        $inst_status = 0;
                        $fine_id = null;
                        $fine_detail_id = null;
                        $fine_detail_days = null;
                        $fine_detail_amt = null;
                       
                        
                        if($fine_amt != 0) {
                            if($feesHeads['fine_status'] == 1){
                                $fine_id = $feesHeads['fine_id'];
                                $fine_detail_id = $feesHeads['fine_detail_id'];
                                $fine_detail_days = $feesHeads['fine_detail_days'];
                                $fine_detail_amt = $feesHeads['fine_detail_amt'];
                            }
                        }
                        $concession_map_id = null;
                        $concession_amt = null;
                        $final_amt = 0;
                        $final_amt = $feesHeads['pay_amt'];
                        if($request->get('concession_use') != null && $request->get('concession_use') == 1 && $feesHeads['concession_amt'] != ''){
                            $final_pay_amt = 0;
                            $concession_map_id = $feesHeads['concession_map_id'];
                            $concession_amt = $feesHeads['concession_amt'];
                            if($feesHeads['concession_map_id'] != ""){
                                $concession_map = ConcessionMap::where('concession_map_id',$feesHeads['concession_map_id'])->where('student_id', $decrypted_student_id)->where('session_id', $session['session_id'])->first();
                                $concession_map->concession_status = 1;
                                $concession_map->save();
                            }
                            $final_pay_amt = $feesHeads['concession_amt'] + $feesHeads['pay_amt'];
                            if($final_pay_amt == $feesHeads['head_pay_amt']){
                                $inst_status = 1;
                            }
                            $final_amt += $feesHeads['concession_amt'];
                        }

                        if($pay_lator_amt != 0 && $pay_lator_amt != ''){
                            if($pay_lator_amt >= $final_amt){
                                $remainingAmt = $pay_lator_amt - $final_amt;
                                $feesHeads['pay_amt'] = 0;
                                $pay_lator_amt = $remainingAmt;
                            } else if($pay_lator_amt < $final_amt){
                                $feesHeads['pay_amt'] = $final_amt - $pay_lator_amt;
                                $pay_lator_amt = 0;
                            }
                        }
                        if($final_amt == $feesHeads['head_pay_amt']){
                            $inst_status = 1;
                        } else {
                            $inst_status = 0;
                        }
                        $receipt_detail = new ReceiptDetail();
                        $receipt_detail->admin_id           = $admin_id;
                        $receipt_detail->update_by          = $loginInfo['admin_id'];
                        $receipt_detail->receipt_id         = $receipt->receipt_id;
                        $receipt_detail->head_id            = $feesHeads['head_id'];
                        $receipt_detail->head_type          = $feesHeads['head_type'];
                        $receipt_detail->head_month_year    = $feesHeads['head_month_year'];
                        $receipt_detail->head_inst_id       = $feesHeads['head_inst_id'];
                        $receipt_detail->inst_amt           = $feesHeads['head_pay_amt'];
                        $receipt_detail->inst_paid_amt      = $feesHeads['pay_amt'];
                        $receipt_detail->inst_status        = $inst_status;
                        $receipt_detail->r_concession_map_id  = $concession_map_id;
                        $receipt_detail->r_concession_amt     = $concession_amt;
                        $receipt_detail->r_fine_id            = $fine_id;
                        $receipt_detail->r_fine_detail_id     = $fine_detail_id;
                        $receipt_detail->r_fine_detail_days   = $fine_detail_days;
                        $receipt_detail->r_fine_amt           = $fine_detail_amt;
                        $receipt_detail->map_student_staff_id = $feesHeads['map_student_staff_id'];
                        
                        $receipt_detail->save();

                        if($feesHeads['head_type'] == 'IMPREST'){
                            $imprest_detail = new AcEntryMap();
                            $imprest_detail->admin_id           = $admin_id;
                            $imprest_detail->update_by          = $loginInfo['admin_id'];
                            $imprest_detail->ac_entry_id        = $feesHeads['head_id'];
                            $imprest_detail->session_id         = $student_info['current_session_id'];
                            $imprest_detail->class_id           = $student_info['current_class_id'];
                            $imprest_detail->section_id         = $student_info['current_section_id'];
                            $imprest_detail->student_id         = $decrypted_student_id;
                            $imprest_detail->ac_entry_map_amt       = $feesHeads['head_pay_amt'];
                            $imprest_detail->ac_entry_map_amt_paid  = $feesHeads['head_pay_amt'];
                            $imprest_detail->save();
                            $getImprestAc    = ImprestAc::where('student_id', $decrypted_student_id)->first();
                            $imprest_ac_amt = $getImprestAc->imprest_ac_amt + $feesHeads['head_pay_amt']; 
                            $getImprestAc->imprest_ac_amt = $imprest_ac_amt;
                            $getImprestAc->save();
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        } else {
            $error_message = "student not found!";
            return redirect()->back()->withErrors($error_message);
        }
        $success_msg = "Fees successfully paid !!";
        return redirect()->back()->withSuccess($success_msg);
    }  
}
