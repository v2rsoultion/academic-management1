<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\SalaryHead; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollSalaryHeadController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Salary Head
     * @Khushbu on 19 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $salary_head  = []; 
        $loginInfo   = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_sal_id 	    = get_decrypted_value($id, true);
        	$salary_head      		= SalaryHead::Find($decrypted_sal_id);
            $page_title             = trans('language.edit_sal_head');
        	$save_url    			= url('admin-panel/payroll/manage-salary-head-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title             = trans('language.add_sal_head');
	 		$save_url    			= url('admin-panel/payroll/manage-salary-head-save');
	 		$submit_button  		= 'Save';
	 	}
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'salary_head'     => $salary_head    
        );
        return view('admin-panel.payroll-salary-head.add')->with($data);
    }

    /**
     *	Add & Update of Payroll Salary Head
     *  @Khushbu on 19 Feb 2019
    **/
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_sal_id			= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id)) {
            $salary_head        = SalaryHead::Find($decrypted_sal_id);
            if(!$salary_head) {
                return redirect('admin-panel/payroll/manage-salary-head-save')->withErrors('Salary Head not found!');
            }
            $admin_id    = $salary_head->admin_id;
            $success_msg = 'Salary Head updated successfully!';
        } else {
            $salary_head    	 = New SalaryHead;
            $success_msg         = 'Salary Head saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'sal_head_name'    => 'required|unique:pay_salary_head,sal_head_name,' . $decrypted_sal_id . ',pay_salary_head_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $salary_head->admin_id         = $admin_id;
                $salary_head->update_by        = $loginInfo['admin_id'];
                $salary_head->sal_head_name    = Input::get('sal_head_name');
                $salary_head->head_type        = Input::get('head_type');
                $salary_head->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-salary-head')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Salary Head's Data fo view page
     *  @Khushbu on 19 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$salary_head 		= SalaryHead::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_sal_head_name') !=  NULL)
            {
                $query->where('sal_head_name', 'Like', $request->get('s_sal_head_name').'%');
            }
        })->orderBy('pay_salary_head_id','DESC')->get();
        // p($salary_head);
        return Datatables::of($salary_head)
        ->addColumn('head_type', function($salary_head) {
            if($salary_head['head_type'] == 0) {
                $head_type = 'Deduction';
            } else {
                $head_type = 'Allowance';
            }
            return $head_type;
        })
    	->addColumn('action', function($salary_head) use($request) {
            $encrypted_sal_id  = get_encrypted_value($salary_head->pay_salary_head_id, true);
    		if($salary_head->pay_salary_head_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-salary-head-status/'.$status .'/'.$encrypted_sal_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-salary-head/'.$encrypted_sal_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-salary-head/' . $encrypted_sal_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-salary-head');
    }  

    /** 
     *	Change Status of Payroll Salary Head
     *  @Khushbu on 19 Feb 2019
	**/
	public function changeStatus($status,$id) 
	{
		$sal_id 		= get_decrypted_value($id, true);
        $salary_head 	= SalaryHead::find($sal_id);
        if ($salary_head)
        {
            if($salary_head->pay_salary_head_id == 1)
            {
                $error_message = "Sorry we can't Update it because it's basic pay!!";
                return redirect('admin-panel/payroll/manage-salary-head')->withErrors($error_message);
            }
            $salary_head->pay_salary_head_status  = $status;
            $salary_head->save();
            $success_msg = "Salary Head status update successfully!";
            return redirect('admin-panel/payroll/manage-salary-head')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Salary Head not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll Salary Head
     *  @Khushbu on 19 Feb 2019
	**/
	public function destroy($id) {
		$sal_id 		= get_decrypted_value($id, true);
        $salary_head 	= SalaryHead::find($sal_id);
        if ($salary_head)
        {
            DB::beginTransaction();
            try
            {
                if($salary_head->pay_salary_head_id == 1)
                {
                    $error_message = "Sorry we can't Delete it because it's basic pay!!";
                    return redirect('admin-panel/payroll/manage-salary-head')->withErrors($error_message);
                }
                $salary_head->delete();
                $success_msg = "Salary Head deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Salary Head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
