<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Remarks\Remarks; // Model
use Yajra\Datatables\Datatables;


class RemarkController extends Controller
{
    /**
     *  View page for remark listing
     *  @Shree on 1 Nov 2018
    **/
    public function remark_list()
    {
        $remark = $arr_section = $arr_subject = [];
        $loginInfo  = get_loggedin_user_data();
        $arr_class  = get_all_classes_mediums();
        $arr_staff  = get_all_staffs();
        $arr_subject            = get_all_student_subjects();
        $remark['arr_class']    = add_blank_option($arr_class, "Select Class");
        $remark['arr_section']  = add_blank_option($arr_section, "Select Section");
        $remark['arr_staff']    = add_blank_option($arr_staff, "Select Teacher");
        $remark['arr_subject']  = add_blank_option($arr_subject, "Select Subject");
        $data = array(
            'page_title'    => trans('language.remarks'),
            'redirect_url'  => url('admin-panel/dairy-remarks/remarks'),
            'login_info'    => $loginInfo,
            'remark'        => $remark
        );
        return view('admin-panel.remarks.remark_list')->with($data);
    }

    /**
     *  Get remark listing
     *  @Shree on 1 Nov 2018
    **/
    public function remark_data(Request $request)
    {
        $remark     = [];
        $session       = get_current_session();
        $remark     = Remarks::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $query->where('class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('staff_id')) && $request->get('staff_id') != null){
                $query->where('staff_id', '=',$request->get('staff_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }
            if (!empty($request) && !empty($request->get('remark_date')) && $request->get('remark_date') != null){
                $query->where('remark_date', '=',$request->get('remark_date'));
            }
            
        })->join('students', function($join) use ($request){
            $join->on('students.student_id', '=', 'remarks.student_id');
             // For Enroll No
             if (!empty($request) && !empty($request->get('enroll_no')))
             {
                 $join->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
             }
             // For Student Name
             if (!empty($request) && !empty($request->get('student_name')))
             {
                 $join->where('student_name', "like", "%{$request->get('student_name')}%");
             }
            
        })->orderBy('remark_id', 'DESC')
        ->with('getClass')->with('getSection')->with('getStaff')->with('getSubject')->get()->toArray();
        
        return Datatables::of($remark)
            ->addColumn('student_profile', function ($remark)
            {
                $profileImg = '';
                $profile = check_file_exist($remark['student_image'], 'student_image');
                if (!empty($profile))
                {
                    $profileImg = $profile;
                }
                if($profileImg != ''){
                    $profile = "<img src=".$profileImg." height='30' />";
                }   
                $name = $remark['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('class_section', function ($remark)
            {
                $class_section = $remark['get_class']['class_name'].' - '.$remark['get_section']['section_name'];
                return $class_section;
                
            })
            ->addColumn('subject', function ($remark)
            {
                $subjectName = $remark['get_subject']['subject_name'].' - '.$remark['get_subject']['subject_code'];
                return $subjectName;
                
            })
            ->addColumn('staff', function ($remark)
            {
                $staffName = $remark['get_staff']['staff_name'];
                return $staffName;
                
            })
            ->addColumn('remark_date', function ($remark)
            {
                $remark_date = date("d F Y", strtotime($remark['remark_date']));
                return $remark_date;
                
            })
            ->addColumn('remark', function ($remark)
            {
                return '<button type="button" class="btn btn-raised btn-primary remarks"  remark-id="'.$remark['remark_id'].'" >
                Remark
                </button>';
            })
            ->rawColumns(['student_profile'=>'student_profile', 'class_section'=> 'class_section','subject'=> 'subject','staff'=> 'staff','remark_date'=> 'remark_date','remark'=>'remark'])->addIndexColumn()->make(true);
    }

    /**
     *  Get remark single data
     *  @Shree on 1 Nov 2018
    **/
    public function remark_single_data(Request $request)
    {
        $remark     = [];
        $session       = get_current_session();
        $remark     = Remarks::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('remark_id')) && $request->get('remark_id') != null){
                $query->where('remark_id', '=',$request->get('remark_id'));
            }
        })->join('students', function($join) use ($request){
            $join->on('students.student_id', '=', 'remarks.student_id');
        })->orderBy('remark_id', 'DESC')->with('getSubject')->get()->toArray();
        $remark = isset($remark[0]) ? $remark[0] : [];
        $data      = view('admin-panel.remarks.remark_info',compact('remark','remark'))->render();
        return $data;
        
    }


    /**
     *  Get My Remarks
     *  @Sandeep on 8 Jan 2019
    **/
    public function student_view_remark(Request $request)
    {
        $loginInfo              = get_loggedin_user_data();
        $session                = get_current_session();
        $arr_teacher            = get_all_student_staffs($loginInfo['class_id'],$loginInfo['section_id']);
        $arr_subject            = get_subject_list_class_section($loginInfo['class_id'],$loginInfo['section_id']);
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

        $listData['arr_teacher']    = add_blank_option($arr_teacher, 'Select Teacher');
        $listData['arr_subject']    = add_blank_option($arr_subject, 'Select Subject');

        $data = array(
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/student/view-remarks'),
            'page_title'        => trans('language.student_remark'),
            'listData'          => $listData,
            'parent_student'    => $parent_student,
        );
        
        return view('admin-panel.remarks.student_remarks')->with($data);
    }



    /**
     *  Get My Remarks
     *  @Sandeep on 8 Jan 2019
    **/
    public function student_remark_data(Request $request)
    {
        $remark     = [];
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();

        $remark     = Remarks::where(function($query) use ($request,$session,$loginInfo) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($loginInfo['class_id']) && $loginInfo['class_id'] != null){
                $query->where('class_id', '=',$loginInfo['class_id']);
            }
            if (!empty($loginInfo['section_id']) && $loginInfo['section_id'] != null){
                $query->where('section_id', '=',$loginInfo['section_id']);
            }
            if (!empty($loginInfo['student_id']) && $loginInfo['student_id'] != null){
                $query->where('remarks.student_id', '=',$loginInfo['student_id']);
            }

            if (!empty($request) && !empty($request->get('staff_id')) && $request->get('staff_id') != null){
                $query->where('staff_id', '=',$request->get('staff_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }
        })->join('students', function($join) use ($request){
            $join->on('students.student_id', '=', 'remarks.student_id');
        })->orderBy('remark_id', 'DESC')
        ->with('getClass')->with('getSection')->with('getStaff')->with('getSubject')->get()->toArray();

        return Datatables::of($remark)
            ->addColumn('remark_date', function ($remark)
            {
                $remark_date = date("d F Y", strtotime($remark['remark_date']));
                return $remark_date;
            })
            ->addColumn('subject_name', function ($remark)
            {
                $subjectName = $remark['get_subject']['subject_name'].' - '.$remark['get_subject']['subject_code'];
                return $subjectName;  
            })
            ->addColumn('teacher_name', function ($remark)
            {
                $staffName = $remark['get_staff']['staff_name'];
                return $staffName;  
            })
            ->addColumn('remark_description', function ($remark)
            {
                return '<button type="button" class="btn btn-raised btn-primary remarks"  remark-id="'.$remark['remark_id'].'" >
                Remark
                </button>';
            })
            ->rawColumns(['remark_description'=>'remark_description'])->addIndexColumn()->make(true);
    }


    /**
     *  Get remark student single data
     *  @Shree on 9 Jan 2019
    **/
    public function remark_student_single_data(Request $request)
    {
        $remark     = [];
        $session       = get_current_session();
        $remark     = Remarks::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('remark_id')) && $request->get('remark_id') != null){
                $query->where('remark_id', '=',$request->get('remark_id'));
            }
        })->join('students', function($join) use ($request){
            $join->on('students.student_id', '=', 'remarks.student_id');
        })->orderBy('remark_id', 'DESC')->with('getSubject')->get()->toArray();
        $remark = isset($remark[0]) ? $remark[0] : [];
        $data      = view('admin-panel.remarks.remark_info_student',compact('remark','remark'))->render();
        return $data;
        
    }



}
