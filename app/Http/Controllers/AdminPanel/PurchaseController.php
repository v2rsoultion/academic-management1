<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Purchase\Purchase; // Model
use App\Model\Purchase\PurchaseItems; // Model
use App\Model\Sale\Sale; // Model
use App\Model\Sale\SaleItems; // Model
use App\Model\InvoiceConfig\InvoiceConfig; // Model
use App\Model\InventoryItem\Items; // Model
use App\Model\School\School; // Model
use Yajra\Datatables\Datatables;
use PDF;
use Redirect;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Purchase
     * @Khushbu on 24 Jan 2019.
    **/
    public function add(Request $request, $id = NULL) 
    {

    	$purchase = $arr_category = $arr_vendor = $invoice_type = $arr_subcategory_data = $arr_items = $purchase_item = [];
        $loginInfo     		  = get_loggedin_user_data();
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        if (!empty($id)) 
        {
            $decrypted_purchase_id       = get_decrypted_value($id, true);
            $purchase                    = Purchase::Find($decrypted_purchase_id);
            $purchase_item               = PurchaseItems::where('purchase_id', $purchase->purchase_id)->where('purchase_type',1)->with('items', 'units','category','subCategory')->get();
            $purchase['tax_amount']      = ($purchase['total_amount']*$purchase['tax'])/100;
            $purchase['add_tax_amount']  = $purchase['total_amount']+$purchase['tax_amount'];
            $purchase['discount_amount'] = ($purchase['add_tax_amount']*$purchase['discount'])/100;
            $purchase['round_off']       = number_format($purchase['net_amount']-$purchase['gross_amount'],2);
            $purchase['payment_mode_data']    = $arr_inv_payment_mode;
            // p($purchase_item);
            if (!$purchase)
            {
                return redirect('admin-panel/inventory/purchase/add-purchase')->withError('Purchase not found!');
            }
            $page_title                 = trans('language.edit_purchase');
            $save_url                   = url('admin-panel/inventory/purchase/save/' . $id);
            $submit_button              = 'Update';
            $arr_items                  = get_all_items($purchase['subcategory_id']);
            $purchase['items']          = add_blank_option($arr_items, 'Select Item');
        }
        else {
        $invoice_type             = InvoiceConfig::where('invoice_type', 1)->get()->toArray();
        $invoice_format = $invoice_type[0]['invoice_prefix'];
        if($invoice_type[0]['year'] == 1 ? $invoice_format .= date('Y') : $invoice_format)
        if($invoice_type[0]['month'] == 1 ? $invoice_format .= date('m') : $invoice_format)
        if($invoice_type[0]['day'] == 1 ? $invoice_format .= date('d') : $invoice_format) 
        $invoice_format .= $invoice_type[0]['invoice_config_id'];
        $invoice_format .= $invoice_type[0]['total_invoice']+1;
        $purchase['system_invoice_no'] = $invoice_format;
            $page_title               = trans('language.add_purchase');
            $save_url                 = url('admin-panel/inventory/purchase/save');
            $submit_button            = 'Save';
            $arr_items                = get_all_items($purchase['sub_category_id']);
        $purchase['items']        = add_blank_option($arr_items, 'Select Item');
        $purchase['payment_mode_data'] = add_blank_option($arr_inv_payment_mode, 'Select Payment Mode');
        }
        
        $arr_category             = get_all_category(1);
        $purchase['category']     = add_blank_option($arr_category, 'Select Category');
        $arr_vendor               = get_all_vendors();
        $purchase['vendor']       = add_blank_option($arr_vendor, 'Select Vendor');
        $arr_subcategory_data     = get_sub_categories($purchase['category_id']);
        $purchase['arr_subcategory_data']  = add_blank_option($arr_subcategory_data, 'Select Sub Category');
        
        $arr_bank                 = get_all_bank();
        $purchase['bank']         = add_blank_option($arr_bank, 'Select Bank Name');  
    
    	$data                     =  array(
            'page_title'          => $page_title,
            'login_info'          => $loginInfo,
            'save_url'            => $save_url,  
            'purchase'		      => $purchase,	
            'submit_button'	      => $submit_button,
            'purchase_item'       => $purchase_item,  
        );
    	return view('admin-panel.inventory-purchase.add')->with($data);
    } 

    /** 
     *  Add & Update Purchase
     *  @Khushbu on 28 Jan 2019.
    **/
    public function save(Request $request, $id = NULL) {

        $loginInfo                  = get_loggedin_user_data();
        $decrypted_purhcase_id      = get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];

        if(empty($request->get('items')))
        {
            return redirect('admin-panel/inventory/purchase/add-purchase')->withErrors('Atleast one item should be inserted!');
        }
        if(!empty($id))
        {
            $purchase        = Purchase::Find($decrypted_purhcase_id);
            if(!$purchase) {
                return redirect('admin-panel/inventory/purchase/add-purchase')->withErrors('Purchase not found!');
            }

            $admin_id    = $purchase->admin_id;
            $success_msg = 'Purchase updated successfully!';
        }
        else
        {
            $purchase        = New Purchase;
            $success_msg     = 'Purchase saved successfully!';
        }

        $validator              =  Validator::make($request->all(), [
            'ref_invoice_no'    => 'required|unique:purchases,ref_invoice_no,' . $decrypted_purhcase_id .',purchase_id',
            'invoice_date'      => 'required'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $purchase->admin_id          = $admin_id;
                $purchase->update_by         = $loginInfo['admin_id'];
                $purchase->system_invoice_no = Input::get('system_invoice_no');
                $purchase->ref_invoice_no    = Input::get('ref_invoice_no');
                $purchase->date              = Input::get('invoice_date');
                $purchase->vendor_id         = Input::get('vendor_id');
                $purchase->category_id       = Input::get('category_id');
                $purchase->subcategory_id    = Input::get('sub_category_id');
                $purchase->payment_mode      = Input::get('payment_mode');
                $purchase->bank_id           = Input::get('bank_id');
                $purchase->cheque_no         = Input::get('cheque_no');
                $purchase->cheque_date       = Input::get('cheque_date');
                $purchase->cheque_amount     = Input::get('cheque_amount');
                $purchase->total_amount      = Input::get('total_amount');
                $purchase->tax               = Input::get('tax');
                $purchase->discount          = Input::get('discount');
                $purchase->gross_amount      = Input::get('gross_amount');
                $purchase->net_amount        = Input::get('net_amount');
                $purchase->save();

                if(!empty($request->get('items'))) {
                    foreach($request->get('items') as $items) {
                        // p($purchase_items_id[$key]['purchase_items_id']);
                        $getItemData     = Items::find($items['item_id']);
                        if(isset($items['purchase_items_id'])) {
                            $purchase_item   = PurchaseItems::find($items['purchase_items_id']);
                        } else {
                            $purchase_item = new PurchaseItems;
                            $getItemData->available_quantity = $getItemData->available_quantity + $items['quantity'];
                        }
                            $purchase_item->admin_id     = $admin_id;
                            $purchase_item->update_by    = $loginInfo['admin_id'];
                            $purchase_item->purchase_id  = $purchase->purchase_id;
                            $purchase_item->item_id      = $items['item_id'];
                            $purchase_item->category_id      = $items['category_id'];
                            $purchase_item->sub_category_id      = $items['sub_category_id'];
                            $purchase_item->purchase_type      = 1;
                            $purchase_item->unit_id      = $items['unit_id'];
                            $purchase_item->rate         = $items['rate'];
                            $purchase_item->quantity     = $items['quantity'];
                            $purchase_item->amount       = $items['amount'];
                            $purchase_item->save();
                            
                            $getItemData->save();
                    }
                }    

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/purchase/view-purchase')->withSuccess($success_msg);  
    }

    /**
     *  View Page of Purchase
     *  @Khushbu on 28 Jan 2019.
    **/
    public function index() {
        $loginInfo     = get_loggedin_user_data();
        $arr_vendor   = $purchase =  $arr_category = [];
        $arr_vendor               = get_all_vendors();
        $purchase['vendor']       = add_blank_option($arr_vendor, 'Select Vendor');
        $arr_category             = get_all_category(1);
        $purchase['category']     = add_blank_option($arr_category, 'Select Category');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/inventory/purchase/view-purchase'),
            'page_title'    => trans('language.view_purchase'),
            'purchase'      => $purchase    
        );
        
        return view('admin-panel.inventory-purchase.index')->with($data);
    }  

    /** 
     *  Get Purchase's Data for view page(Datatables)
     *  @Khushbu on 28 Jan 2019.
    **/
    public function anyData(Request $request) {
        $loginInfo     = get_loggedin_user_data();
        $purchase      = Purchase::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('s_system_invoice_no')))
            {
                $query->where('system_invoice_no', 'Like', $request->get('s_system_invoice_no').'%');
            }
            if (!empty($request) && !empty($request->get('s_ref_invoice_no')))
            {
                $query->where('ref_invoice_no', 'Like', $request->get('s_ref_invoice_no').'%');
            }
            if (!empty($request) && !empty($request->get('s_invoice_date')))
            {
                $query->where('date', '=', $request->get('s_invoice_date'));
            }
            if (!empty($request) && !empty($request->get('s_vendor_id')))
            {
                $query->where('vendor_id', '=', $request->get('s_vendor_id'));
            }
            if (!empty($request) && !empty($request->get('s_category_id')))
            {
                $query->where('category_id', '=', $request->get('s_category_id'));
            }
        })->orderBy('purchase_id','DESC')->with('category','subCategory','vendor')->get();
        // p($purchase);
        return Datatables::of($purchase)
        ->addColumn('calculation_amount', function($purchase) use($request) {
            $round_off = number_format($purchase['net_amount'] - $purchase['gross_amount'],2);
            return '
                <div>
                <b>Total Amount: </b>₹ ' .$purchase['total_amount']. '   <br>
                <b>Tax: </b> ' .$purchase['tax']. '% <br>
                <b>Discount: </b> ' .$purchase['discount']. '% <br>
                <b>Round off: </b> ' .$round_off. ' <br>
                <b>Net Amount: </b>₹ ' .$purchase['net_amount']. ' 
                </div>';
        })
        ->addColumn('view_details', function($purchase) use($request) {
            $modal_details = '<button class="btn btn-raised btn-primary purchase" data-toggle="modal" data-target="#MoreDetailsModel" purchase_id= '.$purchase->purchase_id.'>More Details</button>';
            return $modal_details;
        })
        ->addColumn('action', function ($purchase) use($request)
        {
            $encrypted_purchase_id = get_encrypted_value($purchase->purchase_id, true);
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-label"></i>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li><a title="Item Details" href="'.url('/admin-panel/inventory/purchase/view-purchase/item-details/' . $encrypted_purchase_id. ' ').'">Item Details</a></li> 
                            <li><a target="new_blank" title="View Invoice" href="'.route('pdfview',['download'=>'pdf','id' =>$purchase->purchase_id]).'">View Invoice</a></li> 
                        </ul> 
                </div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/inventory/purchase/add-purchase/' . $encrypted_purchase_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/purchase/delete-purchase/' . $encrypted_purchase_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>';
        })->rawColumns(['action' => 'action', 'view_details' => 'view_details', 'calculation_amount' => 'calculation_amount'])->addIndexColumn()
        ->make(true);
    } 

    /** 
     *  Destroy Purchase's data
     *  @Khushbu on 29 Jan 2019.
     **/
    public function destroy($id) {
        $purchase_id        = get_decrypted_value($id, true);
        $purchase_item      = PurchaseItems::where('purchase_id',$purchase_id)->get();
        $purchase           = Purchase::find($purchase_id);

        if ($purchase)
        {
            DB::beginTransaction();
            try
            {
                foreach ($purchase_item as $key => $value) {

                    $purchase_items     = PurchaseItems::find($value['purchase_items_id']);
                    $getItemData        = Items::find($purchase_items['item_id']);

                    $get_purchase_items = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_type' => 1])->sum('quantity');

                    $get_purchase_return = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_type' => 2])->sum('quantity');

                    $get_sale_items = SaleItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'sales_type' => 1])->sum('quantity');

                    $get_sale_return = SaleItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'sales_type' => 2])->sum('quantity');

                    $available_quantity = $get_purchase_items - $get_purchase_return - $get_sale_items + $get_sale_return - $purchase_items['quantity'];
                    
                    if($available_quantity >= 0){
                        $getItemData->available_quantity = $get_purchase_items - $get_purchase_return - $get_sale_items + $get_sale_return - $purchase_items['quantity'];
                        $getItemData->save();
                    } else {
                        $error_message = "Sorry we can't delete it because your available quantity is ".$getItemData->available_quantity." !!";
                        return redirect()->back()->withErrors($error_message);
                    }
                    
                    $purchase_item[$key]->delete();
                }
                $purchase->delete();
                $success_msg = "Purchase deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Purchase not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /** 
     *  Get Purchase Items Details Data 
     *  @Khushbu on 29 Jan 2019.
    **/
    public function itemDetails($id) {
        $purchase               = [];
        $purchase_id            = get_decrypted_value($id, true);
        $purchase               = Purchase::where('purchase_id',$purchase_id)->with('vendor')->get();
        $arr_all_items          = get_all_items();
        $purchase['all_items']  = add_blank_option($arr_all_items, 'Select Item');
        $data                   = array(
            'page_title'        => trans('language.view_item_details'),
            'purchase'          => $purchase,
            'purchase_id'       => $purchase_id,
        );

       // p($arr_all_items);
        return view('admin-panel.inventory-purchase.view-item-details')->with($data);
    } 

    /** 
     *  Get Purchase Item's Data for view page(Datatables)
     *  @Khushbu on 29 Jan 2019.
    **/
    public function anyDataItemDetails(Request $request) {
        $loginInfo       = get_loggedin_user_data();
        $purchase_item   = PurchaseItems::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('s_item_name')))
            {
                $query->where('item_id', '=', $request->get('s_item_name'));
            }
        })
        ->where('purchase_id',$request->get('purchase_id'))
        ->orderBy('purchase_items_id')->with('items', 'units','category','subCategory')->get();

        //p($purchase_item);
        return Datatables::of($purchase_item)
        ->addColumn('item_name', function($purchase_item) {
            return $purchase_item['items']->item_name;
        })
        ->addColumn('category_name', function($purchase_item) {
            return $purchase_item['category']->category_name;
        })
        ->addColumn('sub_category_name', function($purchase_item) {
            return $purchase_item['subCategory']->category_name;
        })
        ->addColumn('unit_name', function($purchase_item) {
            return $purchase_item['units']->unit_name;
        })
        ->addColumn('purchase_type', function($purchase_item) {
            if($purchase_item->purchase_type == 1){
                return "Purchase";
            } else {
                return "Purchase Return";
            }
            
        })


        // ->addColumn('action', function($purchase_item) {
        
        //     $encrypted_purchase_id = get_encrypted_value($purchase_item->purchase_items_id, true);
        //     return '
        //         <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/delete-purchase-item/' . $encrypted_purchase_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>';
        // })

        ->rawColumns(['item_name' => 'item_name', 'unit_name' => 'unit_name', 'action' => 'action'])->addIndexColumn()
        ->make(true);
    } 

    /** 
     *  Get Purchase Data
     *  @Khushbu on 29 Jan 2019.
    **/
    public function getPurchaseData(Request $request) {
        $purchase        = Purchase::where('purchase_id',$request->get('purchase_id'))->with('category','subCategory','vendor', 'bank')->get();
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        $purchase[0]['payment_mode'] = $arr_inv_payment_mode[$purchase[0]['payment_mode']];
        // p($purchase);
        $data = compact('purchase');
        // return json_encode($purchase[0]);

        return view('admin-panel.inventory-purchase.ajax_purchase_details', $data);
    } 
    /**
     *  View PDF Invoice Data
     *  @Khushbu on 01 Feb 2019.
    **/
    public function pdfview(Request $request)
    {
        $invoice_details = $item_details = [];
        $school = School::first()->toArray();
        $logo = check_file_exist($school['school_logo'], 'school_logo');
        $arr_inv_payment_mode = \Config::get('custom.inv_payment_mode');
        $purchase = Purchase::find($request->get(id));
        $purchase['payment_mode']= $arr_inv_payment_mode[$purchase['payment_mode']];
        // p($purchase);
            $invoice_details= array(
                'sys_invoice_no' => $purchase->system_invoice_no,
                'invoice_no'     => $purchase->ref_invoice_no,
                'date'           => $purchase->date,
                'payment_mode'   => $purchase->payment_mode,
            );
        $purchase_items = PurchaseItems::where('purchase_id',$purchase->purchase_id)->where('purchase_type',1)->with('items')->get()->toArray();
        if($request->has('download')){
            $pdf = PDF::loadView('admin-panel/inventory-purchase/pdfview', compact('school', 'logo' , 'invoice_details', 'purchase_items', 'purchase'));
            return $pdf->stream('pdfview.pdf');
        }
        
        return view('admin-panel.inventory-purchase.pdfview', $data);
    }
    /**
     *  Delete Purchase Item  Data
     *  @Khushbu on 03 Feb 2019.
    **/
    public function puchaseItemDelete(Request $request) {

        $purchase_items     = PurchaseItems::find($request->get(p_item_id));
        $getItemData        = Items::find($purchase_items['item_id']);

        $get_purchase_items = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_type' => 1])->sum('quantity');

        $get_purchase_return = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_type' => 2])->sum('quantity');

        $get_sale_items = SaleItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'sales_type' => 1])->sum('quantity');

        $get_sale_return = SaleItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'sales_type' => 2])->sum('quantity');

        $getItemData->available_quantity = $get_purchase_items - $get_purchase_return - $get_sale_items + $get_sale_return - $purchase_items['quantity'];    
        

        if(!empty($purchase_items)) {
            $getItemData->save();
            $purchase_items->delete();
        }
    }

    // public function puchaseItemDeletedata(Request $request, $id) {

    //     $decrypted_purchase_id   = get_decrypted_value($id, true);

    //     $purchase_items     = PurchaseItems::find($decrypted_purchase_id);
    //     $getItemData        = Items::find($purchase_items['item_id']);
    //     $get_purchase_items = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_id' => $purchase_items['purchase_id'],'purchase_type' => 1])->sum('quantity');

    //     $get_return_items   = PurchaseItems::where(['item_id' => $purchase_items['item_id'],'category_id' => $purchase_items['category_id'],'sub_category_id' => $purchase_items['sub_category_id'],'unit_id' => $purchase_items['unit_id'],'purchase_id' => $purchase_items['purchase_id'],'purchase_type' => 2])->sum('quantity');

    //     $getItemData->available_quantity = $getItemData->available_quantity - ($get_purchase_items - $get_return_items);    
    //     $getItemData->save();
        
    //     if(!empty($purchase_items)) {
    //       $purchase_items->delete();
    //     }

    //     $success_msg = "Purchase item deleted successfully!";    
    //     return redirect()->back()->withSuccess($success_msg);
    // }


    /**
     *  View Page of Purchase Return
     *  @Sandeep on 27 Feb 2019.
    **/
    public function manage_purchase_return() {

        $purchase = $arr_vendor  = $invoice_type = [];

        $loginInfo       = get_loggedin_user_data();
        $invoice_type    = InvoiceConfig::where('invoice_type', 2)->get()->toArray();
        $invoice_format  = $invoice_type[0]['invoice_prefix'];
        
        if($invoice_type[0]['year'] == 1 ? $invoice_format .= date('Y') : $invoice_format)
        if($invoice_type[0]['month'] == 1 ? $invoice_format .= date('m') : $invoice_format)
        if($invoice_type[0]['day'] == 1 ? $invoice_format .= date('d') : $invoice_format) 
        
        $invoice_format .= $invoice_type[0]['invoice_config_id'];
        $invoice_format .= $invoice_type[0]['total_invoice']+1;
        $purchase['system_invoice_no'] = $invoice_format;

        $arr_vendor            = get_all_vendors();
        $purchase['vendor']    = add_blank_option($arr_vendor, 'Select Vendor');

        $data = array(
            'login_info'    => $loginInfo,
            'save_url'      => url('admin-panel/inventory/purchase-return/save'),
            'page_title'    => trans('language.manage_purchase_return'),
            'purchase'      => $purchase    
        );
        
        return view('admin-panel.inventory-purchase.manage_purchase_return')->with($data);
    }


    /** 
     *  Get Purchase Return Data
     *  @Sandeep on 27 Feb 2019.
    **/
    public function get_purchase_return(Request $request) {

        $purchase  = Purchase::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('invoice_no')))
            {
                $query->where('system_invoice_no', '=', $request->get('invoice_no'));
            }
            if (!empty($request) && !empty($request->get('date')))
            {
                $query->where('date', '=', $request->get('date'));
            }
            if (!empty($request) && !empty($request->get('vendor_id')))
            {
                $query->where('vendor_id', '=', $request->get('vendor_id'));
            }
        })

        ->join('purchase_items', function($join) use ($request){
            $join->on('purchase_items.purchase_id', '=', 'purchases.purchase_id');
            $join->where('purchase_type', 1);
        })->groupBy('item_id')->orderBy('purchases.purchase_id','DESC')->get();

        foreach ($purchase as $value) {

            $getItemData        = Items::find($value['item_id']);

            $get_purchase_items = PurchaseItems::where(['item_id' => $value['item_id'],'category_id' => $value['category_id'],'sub_category_id' => $value['sub_category_id'],'unit_id' => $value['unit_id'],'purchase_id' => $value['purchase_id'],'purchase_type' => 1])->sum('quantity');

                $purchase_return_data[]  = array(
                    'purchase_items_id'    => $value->purchase_items_id,
                    'purchase_id'          => $value->purchase_id,
                    'item_id'              => $value->item_id,
                    'unit_id'              => $value->unit_id,
                    'category_id'          => $value->category_id,
                    'sub_category_id'      => $value->sub_category_id,
                    'purchase_items_name'  => $getItemData->item_name,
                    'quantity'             => $get_purchase_items,
                    'available_quantity'   => $getItemData->available_quantity,
                    'rate'                 => $value->rate,
                );     
            
        }

        //p($purchase_return_data);
        $data = compact('purchase_return_data');
        return view('admin-panel.inventory-purchase.ajax_get_purchase_return', $data);
    }   


    /** 
     *  Add Purchase Return
     *  @Sandeep on 29 Feb 2019.
    **/
    public function purchase_return_save(Request $request) {

        $loginInfo       = get_loggedin_user_data();
        $admin_id        = $loginInfo['admin_id'];
       
        DB::beginTransaction();
        try
        {
            
            foreach($request->get('purchase_return') as $items) {
                
                if(!empty($items['return_quantity'])){

                    $getItemData     = Items::find($items['item_id']);
                    $purchase_item   = new PurchaseItems;
                    
                    $purchase_item->admin_id            = $admin_id;
                    $purchase_item->update_by           = $loginInfo['admin_id'];
                    $purchase_item->purchase_id         = $items['purchase_id'];
                    $purchase_item->item_id             = $items['item_id'];
                    $purchase_item->category_id         = $items['category_id'];
                    $purchase_item->sub_category_id     = $items['sub_category_id'];
                    $purchase_item->unit_id             = $items['unit_id'];
                    $purchase_item->purchase_type       = 2;
                    $purchase_item->rate                = $items['rate'];
                    $purchase_item->quantity            = $items['return_quantity'];
                    $purchase_item->amount              = $items['amount'];
                    $purchase_item->save();
                    
                    $getItemData->available_quantity = $getItemData->available_quantity - $items['return_quantity'];
                    $getItemData->save();
                }
            }
            $success_msg     = 'Purchase return successfully!';

        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        
        return redirect('admin-panel/inventory/manage-purchase-return')->withSuccess($success_msg);  
    }









} 
