<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use URL;
use App\Model\Exam\Exam; // Model
use App\Model\Examination\ExamMap; // Model
use App\Model\Examination\ExamMarks; // Model
use App\Model\Exam\ExamScheduleMap; // 
use App\Model\Examination\StMarksheet;
use App\Model\Examination\StudentExamResult;
use App\Model\MarksheetTemplate\MarksheetTemplate;
use App\Model\Student\Student;
use Yajra\Datatables\Datatables;
use App\Model\School\School; // Model

class ExamReportCardController extends Controller
{
    public function manage_report_card(){
        $manage = $arr_type = $arr_terms = [];
        $arr_type           = \Config::get('custom.exam_type');
        $loginInfo          = get_loggedin_user_data();
        $arr_terms          = get_all_terms();
        $arr_exams          = get_all_exam_type();
        $arr_class          = get_all_classes_mediums();
        $manage['arr_type'] = $arr_type;
        $manage['arr_class'] = add_blank_option($arr_class, "Select Class");
        $manage['arr_exams'] = add_blank_option($arr_exams, 'Select Exam');
        $manage['arr_terms'] = add_blank_option($arr_terms, 'Select Terms');
        $data = array(
            'page_title'    => trans('language.manage_report_card'),
            'redirect_url'  => url('admin-panel/examination/manage-report-card'),
            'login_info'    => $loginInfo,
            'manage'        => $manage
        );
        
        return view('admin-panel.exam-report-card.index')->with($data);
    }

    public function classList(Request $request){
        $type = $request->get('type');
        // type = 1 for Exam Wise
        // type = 2 for Term Wise
       
        $list = [];
        $session       = get_current_session();
        if($request->get('exam_id') != ""){
            $scheduleMap = ExamScheduleMap::where(function($query) use ($request, $loginInfo,$session) 
            {
                $query->where('schedule_map.exam_id',$request->get('exam_id') );
                if (!empty($request) && !empty($request->get('class_id')))
                {
                    $query->where('schedule_map.class_id', "=", $request->get('class_id'));
                }
                if (!empty($request) && !empty($request->get('section_id')))
                {
                    $query->where('schedule_map.section_id', "=", $request->get('section_id'));
                }
                $query->where('schedule_map.publish_status', 1);
            })
            
            ->with('getClass')
            ->with('getSection')
            ->with('getExam')
            ->with('getExam.getTerm')
            ->orderBy('schedule_map.class_id','ASC')
            // ->groupBy('exam_id')
            ->groupBy('schedule_map.class_id')
            ->get()
            ->toArray();
            foreach($scheduleMap as $map){
                $list[] = array(
                    'schedule_map_id' => $map['schedule_map_id'],
                    'session_id' => $map['session_id'],
                    'exam_schedule_id' => $map['exam_schedule_id'],
                    'exam_id' => $map['exam_id'],
                    'class_id' => $map['class_id'],
                    'section_id' => $map['section_id'],
                    'schedule_map_id' => $map['schedule_map_id'],
                    'class_name'     => $map['get_class']['class_name'],
                    'total_strength'    => get_total_class_students($map['class_id']),
                    'total_pass_students'   => $this->get_total_pass_fail(1,$map['exam_id'],$map['class_id']),
                    'total_fail_students'   => $this->get_total_pass_fail(0,$map['exam_id'],$map['class_id']),
                );
            }
            // p($list);
            
        }
        return Datatables::of($list)
        
        ->addColumn('action', function ($list)
        {
            $encrypted_class_id = get_encrypted_value($list['class_id'], true);
            $encrypted_exam_id = get_encrypted_value($list['exam_id'], true);
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a  href="'.url('/admin-panel/examination/generate-marksheet/'.$encrypted_exam_id.'/'.$encrypted_class_id.' ').'"  >Generate Marksheet</a></li>
                        <li><a  href="'.url('/admin-panel/examination/view-marksheet/'.$encrypted_exam_id.'/'.$encrypted_class_id.' ').'"  >View Marksheet</a></li>
                    </ul>
                </div>
                ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    public function generate_marksheet($encrypted_exam_id,$encrypted_class_id){
        $loginInfo  = get_loggedin_user_data();
        $exam_id	= get_decrypted_value($encrypted_exam_id, true);
        $class_id	= get_decrypted_value($encrypted_class_id, true);
        $scheduleInfo = ExamScheduleMap::where(function($query) use ($exam_id, $class_id,$session) 
        {
            $query->where('exam_id',$exam_id );
            if (!empty($class_id) && !empty($class_id))
            {
                $query->where('class_id', "=", $class_id);
            }
            $query->where('publish_status', 1);
        })
        ->with('getClass')
        ->with('getSection')
        ->with('getExam')
        ->with('getExam.getTerm')
        ->orderBy('class_id','ASC')
        ->groupBy('class_id')
        ->first();
        $total_students = get_total_class_students($scheduleInfo->class_id);
        
        $all_subjects = $this->get_subject_list_with_details($scheduleInfo->exam_id,$scheduleInfo->class_id);
        $all_students = $this->get_student_list_for_marks($scheduleInfo->exam_id,$scheduleInfo->class_id,$all_subjects);
       
        $all_ranks =  $this->get_student_rank_records($scheduleInfo->exam_id,$scheduleInfo->class_id,'');
        $data = array(
            'page_title'    => trans('language.generate_marksheet'),
            'redirect_url'  => url('admin-panel/examination/generate-marksheet/'.$encrypted_exam_id.'/'.$encrypted_section_id),
            'login_info'    => $loginInfo,
            'scheduleInfo'  => $scheduleInfo,
            'total_students'    => $total_students,
            'all_subjects'      => $all_subjects,
            'all_students'      => $all_students['arr_student_list'],
            'arr_pending_marks_list'      => $all_students['arr_pending_marks_list'],
            'all_ranks'         => $all_ranks
        );
        // p($all_students['arr_pending_marks_list']);
        
        return view('admin-panel.exam-report-card.generate_marksheet')->with($data);
        
    }

    public function view_marksheet($encrypted_exam_id,$encrypted_class_id){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $exam_id	= get_decrypted_value($encrypted_exam_id, true);
        $class_id	= get_decrypted_value($encrypted_class_id, true);
        $examResult = StudentExamResult::where(function($query) use ($exam_id, $class_id,$session) 
        {
            $query->where('exam_id',$exam_id );
            $query->where('class_id', "=", $class_id);
            $query->where('session_id', "=", $session['session_id']);
        })
        ->with('classInfo','examInfo')
        ->first();
        $total_students = get_total_class_students($class_id);
        $data = array(
            'page_title'    => trans('language.view_marksheet'),
            'redirect_url'  => url('admin-panel/examination/view-marksheet/'.$encrypted_exam_id.'/'.$encrypted_section_id),
            'login_info'    => $loginInfo,
            'exam_result'       => $examResult,
            'total_students'    => $total_students
        );
        return view('admin-panel.exam-report-card.view_marksheet')->with($data);
        
    }

    public function get_students_result(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $all_ranks =  $this->get_student_rank_records($exam_id,$class_id,$request);
        
        return Datatables::of($all_ranks)
        ->addColumn('pass_fail_status', function ($all_ranks)
        {
            $status = "Pass";
            if($all_ranks['pass_fail'] == 0){
                $status = "Fail";
            }
            return $status;
        })
        ->addColumn('action', function ($all_ranks)
        {
            $encrypted_class_id = get_encrypted_value($all_ranks['class_id'], true);
            $encrypted_exam_id = get_encrypted_value($all_ranks['exam_id'], true);
            $encrypted_student_id = get_encrypted_value($all_ranks['student_id'], true);
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a target="_blank" href="'.url('/admin-panel/examination/marksheet/'.$encrypted_exam_id.'/'.$encrypted_class_id.'/'.$encrypted_student_id.' ').'"  >Show Marksheet</a></li>
                    </ul>
                </div>
                ';
        })->rawColumns(['action' => 'action','pass_fail_status'=> 'pass_fail_status'])->addIndexColumn()
        ->make(true);
    }

    public function show_marksheet($encrypted_exam_id,$encrypted_class_id,$encrypted_student_id){
        
        $session    = get_current_session();
        $exam_id	= get_decrypted_value($encrypted_exam_id, true);
        $class_id	= get_decrypted_value($encrypted_class_id, true);
        $student_id	= get_decrypted_value($encrypted_student_id, true);
        $arr_student        = Student::where(function($query) use ($exam_id,$class_id,$student_id) 
        {
            $query->where('students.student_id', $student_id);
            $query->where('students.student_status', 1);
        })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getParent')
        ->with(['getMarksheet' => function($query) use ($class_id,$exam_id,$session){
            $query->where('session_id', "=", $session['session_id']);
            $query->where('class_id', "=", $class_id);
            $query->where('exam_id', "=", $exam_id);
            $query->with('examInfo.getTerm');
           
        }])
       ->orderBy('students.student_name', 'ASC')
        ->get()->toArray();
        $student_info = isset($arr_student[0]) ? $arr_student[0] : [];

        $arr_subject_list = ExamMap::where(function($query) use ($exam_id,$class_id) 
        {
            if ( !empty($exam_id) && $exam_id != null )
            {
                $query->where('exam_map.exam_id', "=", $exam_id);
            }    
            if ( !empty($class_id) && $class_id != null )
            {
                $query->where('exam_map.class_id', "=", $class_id);
            } 
            $query->where('marks_criteria_id', "!=", '');
        })
        ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
        ->with('subjectInfo')->with('marksCriteriaInfo')->with('gradeSchemeInfo.getGrades')
        ->get()->toArray();
        foreach($arr_subject_list as $subject){
            $get_student_marks = $this->get_student_marks($exam_id,$class_id,$student_id,$subject['subject_id']);
            $arr_subjects[] = array(
                'subject_is_coscholastic'   => $subject['subject_info']['subject_is_coscholastic'],
                'subject_name'          => $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'],
                'criteria_name'         => $subject['marks_criteria_info']['criteria_name'],
                'max_marks'             => $subject['marks_criteria_info']['max_marks'],
                'passing_marks'     => $subject['marks_criteria_info']['passing_marks'],
                'optain_marks'  => $get_student_marks
            );
        }

        $activeTemplate = MarksheetTemplate::where('template_type',0)->Where('template_status',1)->first();
        if(empty($activeTemplate)){
            $activeTemplate = MarksheetTemplate::where('template_type',0)->orWhere('template_status',1)->first();
        }
        $templateName = str_replace('-','_',$activeTemplate->template_link);
        $school              = School::with('getCountry','getState','getCity')->first()->toArray();
        
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $exam_name = '';
        $term_name = '';
        if(isset($student_info['get_marksheet']['exam_info'])){
            $exam_name = $student_info['get_marksheet']['exam_info']['exam_name'];
            if(isset($student_info['get_marksheet']['exam_info']['get_term'])){
                $term_name = $student_info['get_marksheet']['exam_info']['get_term']['term_exam_caption'];
            }
        }
        $certificate_info = array(
            'student_name' => $student_info['student_name'],
            'student_roll_no' => $student_info['student_roll_no'],
            'student_enroll_number' => $student_info['student_enroll_number'],
            'student_father_name' => $student_info['get_parent']['student_father_name'],
            'student_mother_name' => $student_info['get_parent']['student_mother_name'],
            'father_contact' => $student_info['get_parent']['student_father_mobile_number'],
            'student_address' => $student_info['student_permanent_address'],
            'class' => $student_info['get_student_academic']['get_current_class']['class_name'].'-'.$student_info['get_student_academic']['get_current_section']['section_name'],
            'student_dob' => date("d-m-Y",strtotime($student_info['student_dob'])),
            'term_name' => $term_name,
            'exam_name' => $exam_name,
            'all_subjects'  => $arr_subjects,
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'logo' => $school['logo'],
            'school_registration_no' => $school['school_registration_no'],
            'school_url' => $school['school_url'],
            'school_email' => $school['school_email'],
            'school_mobile_number' => $school['school_mobile_number'],
            'school_address' => $school['school_address'].', '.$school['get_city']['city_name'].', '.$school['get_state']['state_name'].'. '.$school['get_country']['country_name'].' - '. $school['school_pincode'],
            
            'school_telephone' => $school['school_telephone'],
            'school_sno_numbers' => $school['school_sno_numbers'],
            'session_name'  => $session['session_year']
        );
        
        $page_title = "Marksheet";
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info
        );
        return view('admin-panel.marksheet-templates.'.$templateName)->with($data);
    }

    public function get_student_marks($exam_id,$class_id,$student_id,$subject_id){
        $marks = ExamMarks::where(function($query) use ($exam_id,$class_id,$student_id,$subject_id) 
        {
            $query->where('exam_id', "=", $exam_id);
            $query->where('class_id', "=", $class_id);
            $query->where('student_id', "=", $student_id);
            $query->where('subject_id', "=", $subject_id);
        })->first();
        if(isset($marks->marks)){
            return $marks->marks;
        } else {
            return 0;
        }
    }
    public function get_subject_list_with_details($exam_id,$class_id){
        $arr_subjects = [];
        $arr_subject_list = ExamMap::where(function($query) use ($exam_id,$class_id) 
        {
            if ( !empty($exam_id) && $exam_id != null )
            {
                $query->where('exam_map.exam_id', "=", $exam_id);
            }    
            if ( !empty($class_id) && $class_id != null )
            {
                $query->where('exam_map.class_id', "=", $class_id);
            } 
            $query->where('marks_criteria_id', "!=", '');
        })
        ->join('schedule_map', function($join) use ($request){
            $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('schedule_map.class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('schedule_map.section_id', '=',$request->get('section_id'));
            }
            $join->where('schedule_map.publish_status', 1);
        })
        ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
        ->with('subjectInfo')->with('marksCriteriaInfo')->with('gradeSchemeInfo.getGrades')
        ->get()->toArray();
        foreach($arr_subject_list as $subject){
            $arr_subjects[] = array(
                'subject_id'                => $subject['subject_id'],
                'marks_criteria_id'         => $subject['marks_criteria_id'],
                'subject_is_coscholastic'   => $subject['subject_info']['subject_is_coscholastic'],
                'grade_scheme_id'       => $subject['grade_scheme_id'],
                'subject_name'          => $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'],
                'criteria_name'         => $subject['marks_criteria_info']['criteria_name'],
                'max_marks'             => $subject['marks_criteria_info']['max_marks'],
                'passing_marks'     => $subject['marks_criteria_info']['passing_marks'],
                'exam_date'         => date("d M Y", strtotime($subject['exam_date'])),
                'exam_time'         => date("h:i A", strtotime($subject['exam_time_from'])).' - '.date("h:i A", strtotime($subject['exam_time_to'])),
                'grade_scheme'      => $subject['grade_scheme_info']
            );
        }
        
        return $arr_subjects;
    }

    function get_student_list_for_marks($exam_id,$class_id,$all_subjects)
    {
        
        $arr_student_list = $arr_pending_marks_list = $final_arr = [];
        $session       = get_current_session();
        $arr_student        = Student::where(function($query) use ($exam_id,$class_id) 
        {   
            $query->where('student_status', 1);
        })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })->with('getStudentAcademic')
        ->join('student_parents', function($join) use ($exam_id,$class_id){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })->orderBy('students.student_name', 'ASC')
        ->with('getParent')
        ->with(['get_subject_with_marks' => function($query) use ($exam_id,$class_id,$session){
            $query->where('session_id', "=", $session['session_id']);
            $query->where('class_id', "=", $class_id);
            $query->where('exam_id', "=", $exam_id);
           
        }])
        ->with('get_subject_with_marks')->get()->toArray();
       
        if (!empty($arr_student))
        {
            foreach ($arr_student as $student)
            {
                $marksPending = 0;
                $student_image = '';
                if (!empty($student['student_image']))
                {
                    $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student['student_roll_no'])){
                    $rollNo = $student['student_roll_no'];
                } else {
                    $rollNo = "----";
                }
                $get_excluded_subjects = [];
                $student_subjects = [];
                $get_excluded_subjects = get_excluded_subjects($class_id,$section_id,$student['student_id']);
                
                $total_exam_marks = 0;
                $total_obtain_marks = 0;
                $student_pass_fail = '';
                foreach ($all_subjects as $subject) {
                    
                    $subject['exclude'] = 0;
                    $subject['exam_mark_id'] = NULL;
                    $subject['marks'] = '';
                    $subject['attend_flag'] = 1;
                    $subject['marks_status'] = 0;
                   
                    if(in_array($subject['subject_id'],$get_excluded_subjects)){
                        $subject['exclude'] = 1;
                    }
                    
                    $subject['exist_subject_for_student'] = 0;
                    if(!empty($student['get_subject_with_marks'])) {
                        foreach ($student['get_subject_with_marks'] as $keyMarks => $marksData) {
                            
                            if($subject['subject_id'] == $marksData['subject_id'] && $student['student_id'] == $marksData['student_id'] && $exam_id == $marksData['exam_id']){
                                $subject['exam_mark_id'] = $marksData['exam_mark_id'];
                                $subject['marks'] = $marksData['marks'];
                                $subject['attend_flag'] = $marksData['attend_flag'];
                                $subject['marks_status'] = $marksData['marks_status'];
                                if($subject['subject_is_coscholastic'] == 0 && $subject['exclude'] == 0){
                                    $total_obtain_marks += $marksData['marks'];
                                }
                                $subject['exist_subject_for_student'] = 1;
                            }
                            if($marksData['marks'] === ''){
                                $marksPending = 1;
                            } 
                        }
                    }
                    // Pass/Fail calculation
                    if($subject['marks'] >= $subject['passing_marks']){
                        $subject_status = "Pass";
                    } else if($subject['marks'] < $subject['passing_marks']){
                        $subject_status = "Fail";
                        $student_pass_fail = "Fail";
                    }
                    
                    if($subject['marks'] === ''){
                        $subject['exist_subject_for_student'] = 0;
                        $subject_status = "Pass";
                        $student_pass_fail = "Pass";
                    }
                    if($subject['subject_is_coscholastic'] == 0 && $subject['exclude'] == 0 && $subject['exist_subject_for_student'] == 1){
                        $total_exam_marks += $subject['max_marks'];
                    }
                    // Grade Calculation
                    $grade_name = '-';
                 

                    if(!empty($subject['grade_scheme'])){
                        if(!empty($subject['grade_scheme']['get_grades'])){
                            foreach ($subject['grade_scheme']['get_grades'] as $gradeKey => $grades) {
                                if($grades['grade_max'] >= $subject['marks'] && $grades['grade_min'] <= $subject['marks']){
                                    $grade_name = $grades['grade_name'];
                                }
                            }
                        }
                    }
                    
                    $subject['subject_status'] =  $subject_status;
                    $subject['grade_name'] =  $grade_name;
                    
                    $student_subjects[] = $subject;
                }
                
                if($marksPending == 1){
                    $arr_pending_marks_list[] = array(
                        'student_id'            => $student['student_id'],
                        'class_id'            => $class_id,
                        'section_id'            => $section_id,
                        'exam_id'            => $exam_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student['student_enroll_number'],
                        'student_roll_no'       => $rollNo,
                        'student_name'          => $student['student_name'],
                        'student_status'        => $student['student_status'],
                        'marks_pending'         => $marksPending,
                        'student_subjects'      => $student_subjects,
                    );
                } 
                $student_percentage = 0;
                $student_percentage = number_format(($total_obtain_marks/$total_exam_marks)*100,2);
                $arr_student_list[] = array(
                    'student_id'            => $student['student_id'],
                    'class_id'              => $class_id,
                    'section_id'            => $student['current_section_id'],
                    'exam_id'               => $exam_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student['student_enroll_number'],
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student['student_name'],
                    'student_status'        => $student['student_status'],
                    'marks_pending'         => $marksPending,
                    'student_subjects'      => $student_subjects,
                    'total_exam_marks'      => $total_exam_marks,
                    'total_obtain_marks'    => $total_obtain_marks,
                    'student_percentage'    => $student_percentage,
                    'student_pass_fail'     => $student_pass_fail
                );
            }
        }
        // p($student_subjects);
        if(empty($arr_pending_marks_list)){
            $store_result = $this->store_student_result($arr_student_list);
            if($store_result == 1){
                $generate_class_rank = $this->generate_class_rank($exam_id,$class_id);
                if($generate_class_rank == 1){
                    $generate_section_rank = $this->generate_section_rank($exam_id,$class_id);
                }
            }
        }
        $final_arr = array(
            'arr_student_list' => $arr_student_list,
            'arr_pending_marks_list'    => $arr_pending_marks_list
        );
        return $final_arr;
    }

    public function store_student_result($student_list){
        
        $session    = get_current_session();
        $loginInfo  = get_loggedin_user_data();
        $admin_id   = $loginInfo['admin_id'];
        $total_students = COUNT($student_list);
        $student_counter = 0;
        foreach ($student_list as $key => $students) {
            $student_counter++;
            unset($students['student_subjects']);
            $marksheet = StMarksheet::where('session_id',$session['session_id'])
            ->where('student_id',$students['student_id'])
            ->where('class_id',$students['class_id'])
            ->where('section_id',$students['section_id'])
            ->where('exam_id',$students['exam_id'])
            ->first();
            if(empty($marksheet)){
                $marksheet = New StMarksheet();
            }

            $marksheet->admin_id = $admin_id;
            $marksheet->update_by = $admin_id;
            $marksheet->session_id = $session['session_id'];
            $marksheet->exam_id = $students['exam_id'];
            $marksheet->class_id = $students['class_id'];
            $marksheet->section_id = $students['section_id'];
            $marksheet->student_id = $students['student_id'];
            $marksheet->total_marks = $students['total_exam_marks'];
            $marksheet->obtain_marks = $students['total_obtain_marks'];
            $marksheet->percentage = $students['student_percentage'];
            if($students['student_pass_fail'] == 'Fail'){
                $marksheet->pass_fail = 0;
            } else {
                $marksheet->pass_fail = 1;
            }
            $marksheet->save();

        }
        if($total_students == $student_counter){
            return 1;
        } else {
            return 0;
        }
    }

    public function generate_class_rank($exam_id,$class_id){
        $session    = get_current_session();
        $all_students = StMarksheet::where('session_id',$session['session_id'])
        ->where('class_id',$class_id)
        ->where('exam_id',$exam_id)
        ->groupBy('percentage')
        ->orderBy('percentage','DESC')
        ->get();
        $total_record = COUNT($all_students);
        $record_update = 0;
        foreach ($all_students as $key => $student) {
            $record_update++;
            DB::table('student_marksheet') 
            ->where('exam_id',$exam_id)
            ->where('class_id',$class_id)
            ->where('percentage',$student->percentage)
            ->update([ 'class_rank' => $record_update ]);
            
        }
        if($total_record == $record_update){
            return 1;
        } else {
            return 0;
        }
    }

    public function generate_section_rank($exam_id,$class_id){
        $session    = get_current_session();
        $all_sections = StMarksheet::where('session_id',$session['session_id'])
        ->where('class_id',$class_id)
        ->where('exam_id',$exam_id)
        ->groupBy('section_id')
        ->select('section_id')
        ->get();

        if(!empty($all_sections)){
            foreach ($all_sections as $section) {
                if($section->section_id != ""){
                    $all_students = StMarksheet::where('session_id',$session['session_id'])
                    ->where('class_id',$class_id)
                    ->where('exam_id',$exam_id)
                    ->where('section_id',$section->section_id)
                    ->groupBy('percentage')
                    ->orderBy('percentage','DESC')
                    ->get();
                    $total_record = COUNT($all_students);
                    $record_update = 0;
                    foreach ($all_students as $key => $student) {
                        $record_update++;
                        DB::table('student_marksheet') 
                        ->where('exam_id',$exam_id)
                        ->where('class_id',$class_id)
                        ->where('percentage',$student->percentage)
                        ->update([ 'section_rank' => $record_update ]);
                        
                    }
                }
            }
        }
    }

    public function get_student_rank_records($exam_id,$class_id,$request){
        
        $session       = get_current_session();
        $arr_student        = Student::where(function($query) use ($exam_id,$class_id) 
        {
            if (!empty($request) && !empty($request->get('enroll_no')) && $request->get('enroll_no') != null){
                $join->where('student_enroll_number', '=',$request->get('enroll_no'));
            }
            if (!empty($request) && !empty($request->get('student_name')) && $request->get('student_name') != null){
                $join->where('student_name', '=',$request->get('student_name'));
            }
            $query->where('student_status', 1);
        })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->join('student_marksheet', function($join) use ($exam_id,$class_id){
            $join->on('student_marksheet.student_id', '=', 'students.student_id');
            $join->where('student_marksheet.exam_id',$exam_id);
            $join->where('student_marksheet.class_id',$class_id);
        })->orderBy('students.student_name', 'ASC')
        ->get()->toArray();

        return $arr_student;
    }

    public function save_result(Request $request){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $admin_id   = $loginInfo['admin_id'];
        $result = StudentExamResult::where('session_id',$session['session_id'])
        ->where('class_id',$request->get('class_id'))
        ->where('exam_id',$request->get('exam_id'))
        ->first();
        if(empty($result)){
            $result = New StudentExamResult();
        }
        $result->admin_id = $admin_id;
        $result->update_by = $admin_id;
        $result->session_id = $session['session_id'];
        $result->exam_id = $request->get('exam_id');
        $result->class_id = $request->get('class_id');
        $result->result_date = $request->get('result_date');
        $result->result_time = $request->get('result_time');
        $result->save();
        return 'success';
    }

    public function get_total_pass_fail($type,$exam_id,$class_id){
        $session    = get_current_session();
        $count = 0;
        $count = StMarksheet::where(function($query) use ($exam_id,$class_id,$type) 
        {
            if ( !empty($exam_id) && $exam_id != null )
            {
                $query->where('exam_id', "=", $exam_id);
            }    
            if ( !empty($class_id) && $class_id != null )
            {
                $query->where('class_id', "=", $class_id);
            } 
            $query->where('pass_fail',$type);
            
        })->get()->count();
        return $count;
    }

    public function tabsheet(){
        $manage = $arr_type = $arr_terms = [];
        $arr_type           = \Config::get('custom.tabsheet_type');
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $arr_class          = get_all_classes_mediums();
        $manage['arr_type'] = $arr_type;
        $manage['arr_class'] = add_blank_option($arr_class, "Select Class");
        $manage['arr_exams'] = add_blank_option($arr_exams, 'Select Exam');
        $data = array(
            'page_title'    => trans('language.tabsheet'),
            'redirect_url'  => url('admin-panel/examination/tabsheet'),
            'login_info'    => $loginInfo,
            'manage'        => $manage
        );
        
        return view('admin-panel.exam-report-card.tabsheet')->with($data);
    }

    public function get_tabsheet_grid(Request $request){
    //    p($request->all());
        $exam_ids = [];
        $arr_exams  = get_all_exam_type();
        if($request->get('type') == 1){
            $exam_type = '';
            $exam_ids[] = $request->get('exam_id');
        } else {
            $exam_type = 'Consolidated';
            foreach ($arr_exams as $examKey => $exam) {
                $exam_ids[] = $examKey;
            }
        }
        $class_id = $request->get('class_id');
        $session    = get_current_session();
       
       
        $arr_student        = Student::where(function($query) use ($exam_id,$class_id) 
        {   
            $query->where('student_status', 1);
        })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->with('get_subject_with_marks')->get()->toArray();
       
        $common_exam_list = [];
        foreach ($arr_student as $student) {
            $section_id = $student['current_section_id'];
            $student_exam_list = [];
            foreach ($exam_ids as $examId) {
                $arr_subject_list = ExamMap::where(function($query) use ($request,$examId) 
                {
                    if ( !empty($request) && $request->get('class_id') != null )
                    {
                        $query->where('exam_map.class_id', "=", $request->get('class_id'));
                    }  
                    $query->where('exam_map.exam_id', "=", $examId);
                    $query->where('marks_criteria_id', "!=", '');
                })
                ->join('schedule_map', function($join) use ($request,$examId){
                    $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
                    if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                        $join->where('schedule_map.class_id', '=',$request->get('class_id'));
                    }
                    $join->where('schedule_map.exam_id', '=',$examId);
                    $join->where('schedule_map.publish_status', 1);
                })
                ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
                ->with('subjectInfo')
                ->get()->toArray();

                $get_excluded_subjects = get_excluded_subjects($class_id,$section_id,$student['student_id']);
                
                $arr_subjects = [];
               
                foreach($arr_subject_list as $subject){
                    $subjectD = [];
                    $subjectD['subject_id'] = $subject['subject_id'];
                    $subjectD['subject_name'] =  $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'];
                    $subjectD['exclude'] = 0;
                    $subjectD['marks'] = '';
                    $subjectD['attend_flag'] = 1;
                    $subjectD['marks_status'] = 0;
                    if(in_array($subject['subject_id'],$get_excluded_subjects)){
                        $subjectD['exclude'] = 1;
                    }
                    if(!empty($student['get_subject_with_marks'])) {
                        foreach ($student['get_subject_with_marks'] as $keyMarks => $marksData) {
                            if($subject['subject_id'] == $marksData['subject_id'] && $student['student_id'] == $marksData['student_id'] && $examId == $marksData['exam_id']){
                                
                                $subjectD['marks'] = $marksData['marks'];
                                $subjectD['attend_flag'] = $marksData['attend_flag'];
                                $subjectD['marks_status'] = $marksData['marks_status'];
                            }
                        }
                    }
                    $arr_subjects[] = $subjectD;
                }
                $student_exam_list[] =  array(
                    'exam_id'      => $examId,
                    'exam_name'    => get_exam_name($examId),
                    'exam_subjects'    => $arr_subjects,
                );
                if(COUNT($common_exam_list) < COUNT($exam_ids)){
                    $common_exam_list[] =  array(
                        'exam_id'      => $examId,
                        'exam_name'    => get_exam_name($examId),
                        'exam_subjects'    => $arr_subjects,
                        'all_subject_count' => COUNT($arr_subjects),
                    );
                }
                
            }
            $student_image = '';
            if (!empty($student['student_image']))
            {
                $profile = check_file_exist($student['student_image'], 'student_image');
                if (!empty($profile))
                {
                    $student_image = URL::to($profile);
                }
            } else {
                $student_image = "";
            }
            if(!empty($student['student_roll_no'])){
                $rollNo = $student['student_roll_no'];
            } else {
                $rollNo = "----";
            }
            $arr_student_list[] = array(
                'student_id'            => $student['student_id'],
                'profile'               => $student_image,
                'student_enroll_number' => $student['student_enroll_number'],
                'student_roll_no'       => $rollNo,
                'student_name'          => $student['student_name'],
                'student_status'        => $student['student_status'],
                'student_exam_list'      => $student_exam_list
            );
        }
        // p($common_exam_list);
        // p($arr_student_list);
        $data = array(
            'common_exam_list'    => $common_exam_list,
            'all_students'        => $arr_student_list,
            'exam_type'           => $exam_type,
        );
        return view('admin-panel.exam-report-card.tabsheet-grid')->with($data);
    }

}
