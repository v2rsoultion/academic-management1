<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Staff\Staff; // Model
use App\Model\Staff\StaffAttendance; // Model
use App\Model\Staff\StaffAttendanceDetails; // Model
use Yajra\Datatables\Datatables;

class StaffAttendanceController extends Controller
{
    public function add_attendance(Request $request, $id = NULL){
        
        $data = $attendance = [];
        $loginInfo  = get_loggedin_user_data();
        $holiday_list = get_school_all_holidays(2);
        if (!empty($id))
        {
            $decrypted_attendance_id = get_decrypted_value($id, true);
            $attendance 			 = StaffAttendance::Find($decrypted_attendance_id)->toArray();
            if (!$attendance)
            {
                return redirect()->back()->withErrors('Attendance not found!');
            }
            $page_title          = trans('language.edit_attendance');
            $encrypted_attendance_id = get_encrypted_value($attendance['staff_attendance_id'], true);
            $save_url            = url('admin-panel/staff/attendance/save-attendance/' . $encrypted_attendance_id);
            $submit_button       = 'Update';
            $currentDate = $attendance['staff_attendance_date'];
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;
        }
        else
        {
            $page_title    = trans('language.add_attendance');
            $save_url      = url('admin-panel/staff/attendance/save-attendance');
            $submit_button = 'Save';
            $currentDate = date('Y-m-d');
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;
        }
        if(in_array($currentDate,$holiday_list)){
            $attendance['holiday'] = 1;
        }
        // p($attendance);
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'attendance' 	    => $attendance,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/staff/attendance/view-attendance'),
        );

        return view('admin-panel.staff-attendance.add')->with($data);
    }

    public function attendance_data(Request $request){
        // p($request->all());
        $arr_staff   = [];
        $attendance_mark_status = $this->check_attendance_exist($request);
        if (!empty($request) && $request->get('holiday_status') != null  )
        {
            if($request->get('holiday_status') != null && $request->get('holiday_status') == 0){
                $arr_staff = get_all_staff_for_attendance($request);
            } else {
                $arr_staff = [];
            }
        }
        // p($arr_staff);
        return Datatables::of($arr_staff,$attendance_mark_status) 
            ->addColumn('staff_profile', function ($arr_staff)
            {
                $profile = '';
                if($arr_staff['profile'] != ''){
                    $profile = "<img src=".url($arr_staff['profile'])." height='30' />";
                }   
                $name = $arr_staff['staff_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('attendance_id', function ($arr_staff)
            {
                $attendance_id = "";
                $attendance_id = $arr_staff['attendance_id'];
                return $attendance_id;
            })
            ->addColumn('time_range', function ($arr_staff) use ($attendance_mark_status)
            {
                if($attendance_mark_status == 1 && $arr_staff['staff_attend_d_id'] == NULL){
                    return "Not Available";
                }
                $in_time = $out_time = '';
                $in_time = $arr_staff['in_time'];
                $out_time = $arr_staff['out_time'];
                $time_range = '
                    <input type="time" counter= "'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][in_time]"  id="in_time'.$arr_staff['staff_id'].'" class="form-control times " value="'.$in_time.'" style="max-width: 90px; float:left"  >
                
                    <input type="time" counter= "'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][out_time]"  id="out_time'.$arr_staff['staff_id'].'" class="form-control times"  value="'.$out_time.'" style="max-width: 90px; float:left; margin-left: 2%;" >
                    <br /><br /><p class="red" id="out_time_err'.$arr_staff['staff_id'].'"></p>
                    ';
                return $time_range;
            })
            ->addColumn('attendance', function ($arr_staff) use ($attendance_mark_status)
            {
                if($attendance_mark_status == 1 && $arr_staff['staff_attend_d_id'] == NULL){
                    return "Not Available";
                }
                $present = $absent = $half_day = '';
                $present = "checked";
                if($arr_staff['staff_attendance_type'] == 1){
                    $present = "checked";
                } else if($arr_staff['staff_attendance_type'] == '0' ){
                    $absent = "checked";
                } else if($arr_staff['staff_attendance_type'] == 2){
                    $half_day = "checked";
                } else if($arr_staff['staff_attendance_type'] == ''){
                    $present = "checked";
                }
            
                //return $checked;
                $attendance = '
                    <input type="hidden" name="attendance['.$arr_staff['staff_id'].'][staff_attend_d_id]"  value="'.$arr_staff['staff_attend_d_id'].'" >

                    <input type="hidden" name="attendance['.$arr_staff['staff_id'].'][staff_id]"  value="'.$arr_staff['staff_id'].'" >

                    <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status1'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][attendance]" '.$present.' type="radio" value="1" >
                        <label for="attendance_status1'.$arr_staff['staff_id'].'" class="document_staff">Present</label>
                    </div>
                    <div class="red float-left" style="margin-top:6px !important;">
                        <input id="attendance_status2'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][attendance]" '.$absent.' type="radio" value="0" />
                        <label for="attendance_status2'.$arr_staff['staff_id'].'" class="document_staff">Absent</label> 
                    </div>
                    <div class="green float-left" style="margin-top:6px !important;">
                        <input id="attendance_status3'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][attendance]" '.$half_day.' type="radio" value="2" />
                        <label for="attendance_status3'.$arr_staff['staff_id'].'" class="document_staff">Half Day</label> 
                    </div>
                    ';
                return $attendance;
            })
            ->addColumn('overtime_type', function ($arr_staff) use ($attendance_mark_status)
            {
                if($attendance_mark_status == 1 && $arr_staff['staff_attend_d_id'] == NULL){
                    return "Not Available";
                }
                $nowork = $hours = $lectures = '';
                $nowork = "checked";
                if($arr_staff['staff_overtime_type'] == '0' ){
                    $hours = "checked";
                } else if($arr_staff['staff_overtime_type'] == 1){
                    $lectures = "checked";
                } else {
                    $nowork = "checked";
                }
                $overtime_type = '
                    <div counter= "'.$arr_staff['staff_id'].'"  class="red float-left overtime_type" style="margin-top:6px !important;">
                        <input counter= "'.$arr_staff['staff_id'].'"  id="overtime_type_status0'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][overtime_type]" '.$nowork.' type="radio" value="2" />
                        <label for="overtime_type_status2'.$arr_staff['staff_id'].'" class="document_staff">No</label> 
                    </div>
                    <div counter= "'.$arr_staff['staff_id'].'"  class="green float-left overtime_type" style="margin-top:6px !important;">
                    <input  id="overtime_type_status1'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][overtime_type]" '.$hours.' type="radio" value="0" >
                        <label for="overtime_type_status1'.$arr_staff['staff_id'].'" class="document_staff">Hours</label>
                    </div>
                    <div counter= "'.$arr_staff['staff_id'].'"  class="green float-left overtime_type" style="margin-top:6px !important;">
                        <input id="overtime_type_status2'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][overtime_type]" '.$lectures.' type="radio" value="1" />
                        <label for="overtime_type_status2'.$arr_staff['staff_id'].'" class="document_staff">Lectures</label> 
                    </div>
                    
                    ';
                return $overtime_type;
            })
            ->addColumn('overtime_value', function ($arr_staff) use ($attendance_mark_status)
            {
                if($attendance_mark_status == 1 && $arr_staff['staff_attend_d_id'] == NULL){
                    return "Not Available";
                }
                $disabled = 'disabled';
                if($arr_staff['staff_overtime_type'] == '0' || $arr_staff['staff_overtime_type'] == 1){
                    $disabled = '';
                }
                $staff_overtime_value = '';
                $staff_overtime_value = $arr_staff['staff_overtime_value'];
                $overtime_value = '
                    <input type="number" counter= "'.$arr_staff['staff_id'].'" name="attendance['.$arr_staff['staff_id'].'][overtime_value]"  id="overtime_value'.$arr_staff['staff_id'].'" class="form-control " value="'.$staff_overtime_value.'" step= "any" style="max-width: 80px; float:left" '.$disabled.'  >
                    ';
                return $overtime_value;
            })
            
            ->rawColumns(['staff_profile'=>'staff_profile','overtime_type'=> 'overtime_type','overtime_value'=>'overtime_value', 'time_range'=> 'time_range', 'attendance'=> 'attendance', 'student_enroll_number'=> 'student_enroll_number', 'student_roll_no'=> 'student_roll_no'])->addIndexColumn()->make(true);
    }

    public function check_attendance_exist(Request $request){
        $attendance_mark_status = 0;
        $check_status = StaffAttendance::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $query->where('staff_attendance_date',$request->get('attendance_date'));
                }
                if (!empty($request) && !empty($request->get('staff_attendance_id')) && $request->get('staff_attendance_id') != null){
                    $query->where('staff_attendance_id',$request->get('staff_attendance_id'));
                }
            })
            ->get()->toArray();
        if(!empty($check_status)){
            $attendance_mark_status = 1;
        }
        return $attendance_mark_status;
    }
    public function attendance_save(Request $request, $id = NULL){
        $loginInfo = get_loggedin_user_data();
        $admin_id  = $loginInfo['admin_id'];
        $session    = get_current_session();
        $decrypted_staff_attendance_id	= get_decrypted_value($id, true);
        $total_present_staff = $total_absent_staff = $total_halfday_staff = 0;
        if (!empty($id))
        {
            $attendance = StaffAttendance::find($decrypted_staff_attendance_id);
            $admin_id   = $attendance['admin_id'];
            if (!$attendance)
            {
                return redirect('/admin-panel/staff/attendance/view-attendance/')->withError('Attendance not found!');
            }
            $success_msg = 'Attendancee updated successfully!';
        }
        else
        {
            $attendance     = New StaffAttendance;
            $success_msg = 'Attendance saved successfully!';
        }
        DB::beginTransaction();
        try
        {
            $attendance = StaffAttendance::where('staff_attendance_date',Input::get('attendance_date'))->first();
            if(empty($attendance)){
                $attendance     = New StaffAttendance;
            }
            $attendance->admin_id       = $admin_id;
            $attendance->update_by      = $loginInfo['admin_id'];
            $attendance->session_id 	= $session['session_id'];
            $attendance->staff_attendance_date 	= Input::get('attendance_date');
            $attendance->save();
            // p($attendance);
            
            foreach($request->get('attendance') as $details){ 
                if($details['attendance'] == 0){
                    $total_absent_staff++;
                }
                if($details['attendance'] == 1){
                    $total_present_staff++;
                }
                if($details['attendance'] == 2){
                    $total_halfday_staff++;
                }
                if(isset($details['staff_attend_d_id']) ) {
                    
                    $staff_attend = StaffAttendanceDetails::where('staff_attend_d_id', $details['staff_attend_d_id'])->first();
                    $staff_attend->admin_id             = $admin_id;
                    $staff_attend->update_by            = $loginInfo['admin_id'];
                    $staff_attend->staff_attendance_id  = $attendance->staff_attendance_id;
                    $staff_attend->session_id           = $session['session_id'];
                    $staff_attend->staff_id             = $details['staff_id'];
                    $staff_attend->in_time              = $details['in_time'];
                    $staff_attend->out_time             = $details['out_time'];
                    $staff_attend->staff_attendance_date    = Input::get('attendance_date');
                    $staff_attend->staff_attendance_type    = $details['attendance'];
                    if($details['overtime_type'] != 2){
                        $staff_attend->staff_overtime_type  = $details['overtime_type'];
                        $staff_attend->staff_overtime_value = $details['overtime_value'];
                    } else {
                        $staff_attend->staff_overtime_type  = 2;
                        $staff_attend->staff_overtime_value = NULL;
                    }
                    $staff_attend->save();
                } else {
                    $staff_attend = New StaffAttendanceDetails();
                    $staff_attend->admin_id             = $admin_id;
                    $staff_attend->update_by            = $loginInfo['admin_id'];
                    $staff_attend->staff_attendance_id  = $attendance->staff_attendance_id;
                    $staff_attend->session_id           = $session['session_id'];
                    $staff_attend->staff_id             = $details['staff_id'];
                    $staff_attend->in_time              = $details['in_time'];
                    $staff_attend->out_time             = $details['out_time'];
                    $staff_attend->staff_attendance_date    = Input::get('attendance_date');
                    $staff_attend->staff_attendance_type    = $details['attendance'];
                    if($details['overtime_type'] != 2){
                        $staff_attend->staff_overtime_type  = $details['overtime_type'];
                        $staff_attend->staff_overtime_value = $details['overtime_value'];
                    } else {
                        $staff_attend->staff_overtime_type  = 2;
                        $staff_attend->staff_overtime_value = NULL;
                    }
                    $staff_attend->save();
                }
            }
            $attendance->total_absent_staff 	= $total_absent_staff;
            $attendance->total_present_staff 	= $total_present_staff;
            $attendance->total_halfday_staff 	= $total_halfday_staff;
            $attendance->save();
            
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/staff/attendance/view-attendance')->withSuccess($success_msg);
    }

    /**
     *  View page for remarks
     *  @Shree on 6 Nov 2018
    **/
    public function view_attendance(Request $request)
    {
        $attendance = [];
        $loginInfo              = get_loggedin_user_data();
        
        $data = array(
            'page_title'   => trans('language.view_attendance'),
            'redirect_url' => url('admin-panel/staff/attendance/view-attendance'),
            'login_info'    => $loginInfo,
            'attendance'       => $attendance
        );
        return view('admin-panel.staff-attendance.index')->with($data);
    }

    public function attendance_records(Request $request){
        $arr_attendance   = [];
        $session    = get_current_session();
        $loginInfo = get_loggedin_user_data();
        
        $arr_attendance   = StaffAttendance::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->has('staff_attendance')) && $request->get('staff_attendance') != null)
            {
                $query->where('staff_attendance_date', "=", $request->get('staff_attendance'));
            }
        })->orderBy('staff_attendance_id', 'DESC')->get();
       
        // p($arr_attendance);
        return Datatables::of($arr_attendance) 
            ->addColumn('staff_attendance_date', function ($arr_attendance)
            {
                return date('d M Y', strtotime($arr_attendance->staff_attendance_date));
            })
            
            ->addColumn('action', function ($arr_attendance)
            {
                $encrypted_staff_attendance_id = get_encrypted_value($arr_attendance->staff_attendance_id, true);
                
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit">
                        <a href="'.url('admin-panel/staff/attendance/add-attendance/'.$encrypted_staff_attendance_id.' ').'"><i class="zmdi zmdi-edit"></i></a>
                    </div>
                    ';
            })
            
            ->rawColumns(['staff_attendance_date'=>'staff_attendance_date',  'action'=> 'action'])->addIndexColumn()->make(true);
    }



}
