<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Designation\Designation; // Model
use Yajra\Datatables\Datatables;

class DesignationController extends Controller
{
    /**
     *  View page for Designations
     *  @Pratyush on 19 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'   => trans('language.view_designation'),
            'redirect_url' => url('admin-panel/designation/view-designations'),
            'login_info'   => $loginInfo
        );
        return view('admin-panel.designation.index')->with($data);
    }

    /**
     *  Add page for Designations
     *  @Pratyush on 19 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $designation 	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_designation_id 	= get_decrypted_value($id, true);
            $designation      			= Designation::Find($decrypted_designation_id);
            if (!$designation)
            {
                return redirect('admin-panel/designation/add-designation')->withError('Designation not found!');
            }
            $page_title             	= trans('language.edit_designation');
            $encrypted_designation_id   = get_encrypted_value($designation->designation_id, true);
            $save_url               	= url('admin-panel/designation/save/' . $encrypted_designation_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_designation');
            $save_url      = url('admin-panel/designation/save');
            $submit_button = 'Save';
        }
        $teaching_status            = \Config::get('custom.teaching_status');
        $designation['arr_teaching_status']   = $teaching_status;
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'designation' 		=> $designation,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/designation/view-designations'),
        );
        return view('admin-panel.designation.add')->with($data);
    }

    /**
     *  Add and update Designation's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_designation_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $designation = Designation::find($decrypted_designation_id);
            $admin_id = $designation['admin_id'];
            if (!$designation)
            {
                return redirect('/admin-panel/designation/add-designation/')->withError('Designation not found!');
            }
            $success_msg = 'Designation updated successfully!';
        }
        else
        {
            $designation     = New Designation;
            $success_msg = 'Designation saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'designation_name'   => 'required|unique:designations,designation_name,' . $decrypted_designation_id . ',designation_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $designation->admin_id            	= $admin_id;
                $designation->update_by           	= $loginInfo['admin_id'];
                $designation->designation_name 		= Input::get('designation_name');
                $designation->teaching_status 		= Input::get('teaching_status');
                $designation->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/designation/view-designations')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $designation  		= Designation::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('designation_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('designation_id', 'DESC')->get();
        $teaching_status            = \Config::get('custom.teaching_status');
        return Datatables::of($designation)
        ->addColumn('teaching_status', function ($designation)
        {
            $teaching_status            = \Config::get('custom.teaching_status');
            return $teaching_status[$designation->teaching_status];
            
        })
        ->addColumn('action', function ($designation)
        {
            $encrypted_designation_id = get_encrypted_value($designation->designation_id, true);
            if($designation->designation_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="designation-status/'.$status.'/' . $encrypted_designation_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-designation/' . $encrypted_designation_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-designation/' . $encrypted_designation_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'teaching_status'=> 'teaching_status'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Designation's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $designation_id 		= get_decrypted_value($id, true);
        $designation 		  	= Designation::find($designation_id);
        
        $success_msg = $error_message =  "";
        if ($designation)
        {
            DB::beginTransaction();
            try
            {
                $designation->delete();
                $success_msg = "Designation deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/designation/view-designations')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/designation/view-designations')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Designation not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Designation's status
     *  @Pratyush on 19 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $designation_id 		= get_decrypted_value($id, true);
        $designation 		  	= Designation::find($designation_id);
        if ($designation)
        {
            $designation->designation_status  = $status;
            $designation->save();
            $success_msg = "Designation status updated!";
            return redirect('admin-panel/designation/view-designations')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Designation not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
