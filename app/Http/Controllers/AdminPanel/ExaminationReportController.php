<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use URL;
use App\Model\Exam\Exam; // Model
use App\Model\Examination\ExamMarks; // Model
use App\Model\Examination\ExamMap; // Model
use App\Model\Exam\ExamScheduleMap; // 
use App\Model\Examination\StMarksheet;
use App\Model\Examination\StudentExamResult;
use App\Model\MarksheetTemplate\MarksheetTemplate;
use App\Model\Student\Student;
use Yajra\Datatables\Datatables;

class ExaminationReportController extends Controller
{
    /**
     *  View page for Exam Wise Marks
     *  @Khushbu on 27 Feb 2019
    **/
    public function examWiseMarks() {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.menu_examination_report'),
            'redirect_url'  => url('admin-panel/examination-report/examwise-marks-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.examination-report.examwise_mark_report')->with($data);
    }

    /**
     *  View page for Subject Wise Marks
     *  @Khushbu on 27 Feb 2019
    **/
    public function subjectWiseMarks() {
        $map = $arr_section = $arr_subject = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $map['arr_subject'] = add_blank_option($arr_subject, "Select Subject");
        $data               = array(
            'page_title'    => trans('language.menu_examination_report'),
            'redirect_url'  => url('admin-panel/examination-report/subjectwise-marks-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.examination-report.subjectwise_mark_report')->with($data);
    }

    public function showSubjectwiseMarksData(Request $request){
        // p($request->all());
        $session = get_current_session();
        $exam  = ExamMarks::where([['exam_marks.session_id','=',$session['session_id']],['exam_marks.exam_id','=',$request->get('exam_id')],['exam_marks.class_id','=',$request->get('class_id')],['exam_marks.section_id','=',$request->get('section_id')],['exam_marks.subject_id','=',$request->get('subject_id')]])->orderBy('exam_marks.class_id', 'DESC')->with('getStudentDetails')->join('exam_map', function($join) use($session,$request) {
            $join->on('exam_map.exam_id', '=', 'exam_marks.exam_id');
            $join->where('exam_map.session_id', '=', $session['session_id']);
            $join->where('exam_map.class_id', '=', $request->get('class_id'));
            $join->where('exam_map.section_id', '=', $request->get('section_id'));
            $join->where('exam_map.subject_id', '=', $request->get('subject_id'));
        })->with('marksCriteriaInfo')->get()->toArray();
        // p($exam);
        return Datatables::of($exam,$request)      
        ->addColumn('student_enroll_number', function ($exam)
        {
            if($exam['get_student_details']['student_enroll_number'] == '') {
               $student_enroll_number = '---';
            } else {
                $student_enroll_number = $exam['get_student_details']['student_enroll_number'];
            }
            return $student_enroll_number;
        })
        ->addColumn('student_profile', function ($exam)
        {
            $profile = "";
            if($exam['get_student_details']['student_image'] != ''){
                $profile = check_file_exist($exam['get_student_details']['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $exam['get_student_details']['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('total_marks', function ($exam)
        {
            return $exam['marks_criteria_info']['max_marks'];
        })
       ->rawColumns(['student_enroll_number' => 'student_enroll_number', 'student_profile' => 'student_profile', 'total_marks' => 'total_marks'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Get sections data according section
     *  @Khushbu on 28 Feb 2019
    **/
    public function getSubjectData()
    {
        $section_id = Input::get('section_id');
        $subject = get_all_section_subjects($section_id);
        $data = view('admin-panel.examination-report.ajax-select-subject',compact('subject'))->render();
        return response()->json(['options'=>$data]);
    }

    public function get_tabsheet_grid(Request $request){
        //    p($request->all());
            $exam_ids = [];
            $arr_exams  = get_all_exam_type();
            if($request->get('type') == 1){
                $exam_ids[] = $request->get('exam_id');
            } else {
                foreach ($arr_exams as $examKey => $exam) {
                    $exam_ids[] = $examKey;
                }
            }
            $class_id = $request->get('class_id');
            $session    = get_current_session();
           
           
            $arr_student        = Student::where(function($query) use ($exam_id,$class_id) 
            {   
                $query->where('student_status', 1);
            })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                
                if (!empty($class_id) && !empty($class_id) && $class_id != null){
                    $join->where('current_class_id', '=',$class_id);
                }
                $join->where('current_session_id', '=',$session['session_id']);
            })
            ->with('get_subject_with_marks')->get()->toArray();
        //    p($arr_student);
            $common_exam_list = [];
            foreach ($arr_student as $student) {
                $section_id = $student['current_section_id'];
                $student_exam_list = [];
                foreach ($exam_ids as $examId) {
                    $arr_subject_list = ExamMap::where(function($query) use ($request,$examId) 
                    {
                        if ( !empty($request) && $request->get('class_id') != null )
                        {
                            $query->where('exam_map.class_id', "=", $request->get('class_id'));
                        }  
                        $query->where('exam_map.exam_id', "=", $examId);
                        $query->where('marks_criteria_id', "!=", '');
                    })
                    ->join('schedule_map', function($join) use ($request,$examId){
                        $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
                        if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                            $join->where('schedule_map.class_id', '=',$request->get('class_id'));
                        }
                        $join->where('schedule_map.exam_id', '=',$examId);
                        $join->where('schedule_map.publish_status', 1);
                    })
                    ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
                    ->with('subjectInfo','marksCriteriaInfo')
                    ->get()->toArray();

                    $get_excluded_subjects = get_excluded_subjects($class_id,$section_id,$student['student_id']);
                    
                    $arr_subjects = [];
                   
                    foreach($arr_subject_list as $subject){
                        $subjectD = [];
                        $subjectD['subject_id'] = $subject['subject_id'];
                        $subjectD['subject_name'] =  $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'];
                        $subjectD['exclude'] = 0;
                        $subjectD['max_marks'] = $subject['marks_criteria_info']['max_marks'];
                        $subjectD['marks'] = '';
                        $subjectD['attend_flag'] = 1;
                        $subjectD['marks_status'] = 0;
                        if(in_array($subject['subject_id'],$get_excluded_subjects)){
                            $subjectD['exclude'] = 1;
                        }
                        if(!empty($student['get_subject_with_marks'])) {
                            foreach ($student['get_subject_with_marks'] as $keyMarks => $marksData) {
                                if($subject['subject_id'] == $marksData['subject_id'] && $student['student_id'] == $marksData['student_id'] && $examId == $marksData['exam_id']){
                                    
                                    $subjectD['marks'] = $marksData['marks'];
                                    $subjectD['attend_flag'] = $marksData['attend_flag'];
                                    $subjectD['marks_status'] = $marksData['marks_status'];
                                }
                            }
                        }
                        $arr_subjects[] = $subjectD;
                    }
                    $student_exam_list[] =  array(
                        'exam_id'      => $examId,
                        'exam_name'    => get_exam_name($examId),
                        'exam_subjects'    => $arr_subjects,
                    );
                    if(COUNT($common_exam_list) < COUNT($exam_ids)){
                        $common_exam_list[] =  array(
                            'exam_id'      => $examId,
                            'exam_name'    => get_exam_name($examId),
                            'exam_subjects'    => $arr_subjects,
                        );
                    }
                    
                }
                $student_image = '';
                if (!empty($student['student_image']))
                {
                    $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student['student_roll_no'])){
                    $rollNo = $student['student_roll_no'];
                } else {
                    $rollNo = "----";
                }
                $arr_student_list[] = array(
                    'student_id'            => $student['student_id'],
                    'profile'               => $student_image,
                    'student_enroll_number' => $student['student_enroll_number'],
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student['student_name'],
                    'student_status'        => $student['student_status'],
                    'student_exam_list'      => $student_exam_list
                );
            }
            $data = array(
                'common_exam_list'    => $common_exam_list,
                'all_students'    => $arr_student_list
            );
            return view('admin-panel.examination-report.examwise-marks-grid')->with($data);
    }

    public function getPerwiseReport(){
        $manage = $arr_type = $arr_terms = [];
        $arr_type           = \Config::get('custom.percentage_type');
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $arr_class          = get_all_classes_mediums();
        $manage['arr_type'] = $arr_type;
        $manage['arr_class'] = add_blank_option($arr_class, "Select Class");
        $manage['arr_exams'] = add_blank_option($arr_exams, 'Select Exam');
        $data = array(
            'page_title'    => trans('language.exam_report'),
            'redirect_url'  => url('admin-panel/examination-report/percentage-report'),
            'login_info'    => $loginInfo,
            'manage'        => $manage
        );
        
        return view('admin-panel.examination-report.percentage-report')->with($data);
    }

    public function getPerwiseReportData(Request $request) {
        // p($request->all());
        $session = get_current_session();
        $exam_ids = [];
        $arr_exams  = get_all_exam_type();
        if($request->get('type') == 1){
            $exam_ids[] = $request->get('exam_id');
        } else {
            foreach ($arr_exams as $examKey => $exam) {
                $exam_ids[] = $examKey;
            }
        }
        
        $arr_student= Student::with(['marksheetRecords' => function($query) use($session,$exam_ids){
            $query->where('session_id',$session['session_id']);
            $query->whereIn('exam_id',$exam_ids);
        }])->get()->toArray();
        $final_student_list = [];
        foreach($arr_student as $student) {
            $total_marks = $obtain_marks = 0;
            if(!empty($student['marksheet_records'])){
                foreach ($student['marksheet_records'] as $key => $marksRecord) {
                    $total_marks += $marksRecord['total_marks'];
                    $obtain_marks += $marksRecord['obtain_marks'];
                }
                unset($student['marksheet_records']);
            }
        
            $student['total_marks'] = $total_marks;
            $student['obtain_marks'] = $obtain_marks;
            $final_student_list[] = $student;
        }
        // p($final_student_list);
        return Datatables::of($final_student_list,$request)      
        ->addColumn('student_profile', function ($final_student_list)
        {
            $profile = "";
            if($final_student_list['student_image'] != ''){
                $profile = check_file_exist($final_student_list['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $final_student_list['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('student_enroll_number', function ($final_student_list)
        {
            if($final_student_list['student_enroll_number'] == '') {
                $student_enroll_number = '---';
            } else {
                $student_enroll_number = $final_student_list['student_enroll_number'];
            }
            return $student_enroll_number;
        })
        ->addColumn('percentage', function ($final_student_list)
        {
            $total_marks    = $final_student_list['total_marks'];
            $obtain_marks   = $final_student_list['obtain_marks'];
            $percentage     = ($obtain_marks*100)/$total_marks;
            if($final_student_list['obtain_marks'] == 0)
            {
                $spercentage     = '0%';
            } else {
                $spercentage     = number_format($percentage,2).'%';
            }
            return $spercentage;
        })
        ->rawColumns(['student_enroll_number' => 'student_enroll_number', 'student_profile' => 'student_profile','percentage' => 'percentage'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Student Rank Data
     *  @Khushbu on 08 March 2019
    **/
    public function getRankReport() {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data               = array(
            'page_title'    => trans('language.menu_examination_report'),
            'redirect_url'  => url('admin-panel/examination-report/rank-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.examination-report.student_rank_report')->with($data);
    }

    public function getRankReportData(Request $request) {
        // p($request->all());
        $session = get_current_session();
        $arr_student= StMarksheet::where([['session_id', $session['session_id']],['exam_id',$request->get('exam_id')],['class_id',$request->get('class_id')]])->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null)
            {
                $query->where('section_id',$request->get('section_id'));
            }
        })->with('classInfo','sectionInfo','studentInfo')->orderBy('class_rank', 'ASC')->get()->toArray();
        // p($arr_student);
        return Datatables::of($arr_student)      
        ->addColumn('student_profile', function ($arr_student)
        {
            $profile = "";
            if($arr_student['student_info']['student_image'] != ''){
                $profile = check_file_exist($arr_student['student_info']['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $arr_student['student_info']['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('student_enroll_number', function ($arr_student)
        {
            if($arr_student['student_info']['student_enroll_number'] == '') {
                $student_enroll_number = '---';
            } else {
                $student_enroll_number = $arr_student['student_info']['student_enroll_number'];
            }
            return $student_enroll_number;
        })
        ->rawColumns(['student_enroll_number' => 'student_enroll_number', 'student_profile' => 'student_profile'])->addIndexColumn()
        ->make(true);
    }  
    
    /**
     *  View page for Student Topper List Data
     *  @Khushbu on 08 March 2019
    **/
    public function getTopperReport() {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data               = array(
            'page_title'    => trans('language.menu_examination_report'),
            'redirect_url'  => url('admin-panel/examination-report/topper-list-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.examination-report.topper_rank_list_report')->with($data);
    }

    public function getTopperReportData(Request $request) {
        // p($request->all());
        $session = get_current_session();
        $arr_student= StMarksheet::where([['session_id', $session['session_id']],['exam_id',$request->get('exam_id')],['class_id',$request->get('class_id')]])->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null)
            {
                $query->where('section_id',$request->get('section_id'));
            }
        })->with('classInfo','sectionInfo','studentInfo')->orderBy('class_rank', 'ASC')->take(10)->get()->toArray();
        // p($arr_student);
        return Datatables::of($arr_student)      
        ->addColumn('student_profile', function ($arr_student)
        {
            $profile = "";
            if($arr_student['student_info']['student_image'] != ''){
                $profile = check_file_exist($arr_student['student_info']['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $arr_student['student_info']['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('student_enroll_number', function ($arr_student)
        {
            if($arr_student['student_info']['student_enroll_number'] == '') {
                $student_enroll_number = '---';
            } else {
                $student_enroll_number = $arr_student['student_info']['student_enroll_number'];
            }
            return $student_enroll_number;
        })
        ->rawColumns(['student_enroll_number' => 'student_enroll_number', 'student_profile' => 'student_profile'])->addIndexColumn()
        ->make(true);
    }  
}
