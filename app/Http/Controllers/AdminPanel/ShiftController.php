<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Shift\Shift;

use Yajra\Datatables\Datatables;

class ShiftController extends Controller
{
    /**
     *  View page for shifts
     *  @Shree on 18 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_shift'),
            'redirect_url'  => url('admin-panel/shift/view-shifts'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.shift.index')->with($data);
    }

    /**
     *  Add page for shifts
     *  @Shree on 17 July 2018
     *  @modify by Pratyush on 19 July 2018.   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $shift      = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_shift_id = get_decrypted_value($id, true);
            $shift              = Shift::Find($decrypted_shift_id);
            if (!$shift)
            {
                return redirect('admin-panel/shift/add-shift')->withError('shift not found!');
            }
            $page_title             = trans('language.edit_shift');
            $decrypted_shift_id     = get_encrypted_value($shift->shift_id, true);
            $save_url               = url('admin-panel/shift/save/' . $decrypted_shift_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_shift');
            $save_url      = url('admin-panel/shift/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'shift'         => $shift,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/shift/view-shifts'),
        );
        return view('admin-panel.shift.add')->with($data);
    }

    /**
     *  Add and update shift's data
     *  @Shree on 17 July 2018
     *  @modify by Pratyush on 19 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      = get_loggedin_user_data();
        $admin_id       = $loginInfo['admin_id'];
        $decrypted_shift_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $shift = Shift::find($decrypted_shift_id);
            $admin_id = $shift['admin_id'];
            if (!$shift)
            {
                return redirect('/admin-panel/shift/add-shift/')->withError('Shift not found!');
            }
            $success_msg = 'Shift updated successfully!';
        }
        else
        {
            $shift     = New Shift;
            $success_msg = 'Shift saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'shift_name'   => 'required|unique:shifts,shift_name,' . $decrypted_shift_id . ',shift_id',
                'shift_start_time'     => 'required|unique:shifts,shift_start_time,' . $decrypted_shift_id . ',shift_id',
                'shift_end_time'       => 'required|unique:shifts,shift_end_time,' . $decrypted_shift_id . ',shift_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {

                $shift->admin_id            = $admin_id;
                $shift->update_by           = $loginInfo['admin_id'];
                $shift->shift_name          = Input::get('shift_name');
                $shift->shift_start_time    = date('H:i', strtotime(Input::get('shift_start_time')));
                $shift->shift_end_time      = date('H:i', strtotime(Input::get('shift_end_time')));
                // p($shift);
                $shift->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/shift/view-shifts')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 17 July 2018
     *  @modify by Pratyush on 19 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo      = get_loggedin_user_data();
        $temp_shift     = Shift::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('shift_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('shift_id', 'DESC')->get();
        $temp_shift_arr = array();
        $shift          = array();
        foreach ($temp_shift as $temp_key1 => $temp_value1) {
            
            $temp_shift_arr[$temp_key1]['shift_id'] = $temp_value1['shift_id'];
            $temp_shift_arr[$temp_key1]['shift_name'] = $temp_value1['shift_name'];
            $temp_shift_arr[$temp_key1]['shift_start_time'] = date('h:i a',strtotime($temp_value1['shift_start_time']));
            $temp_shift_arr[$temp_key1]['shift_end_time'] = date('h:i a',strtotime($temp_value1['shift_end_time']));;
            $temp_shift_arr[$temp_key1]['shift_status'] = $temp_value1['shift_status'];

        }
        foreach ($temp_shift_arr as $key => $value) {
            $shift[$key] = (object)$value;
        }
        return Datatables::of($shift)
            ->addColumn('action', function ($shift)
            {

                $encrypted_shift_id = get_encrypted_value($shift->shift_id, true);
                if($shift->shift_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                        <div class="pull-left" data-toggle="tooltip" title="Active"><a href="shift-status/'.$status.'/' . $encrypted_shift_id . '">'.$statusVal.'</a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-shift/' . $encrypted_shift_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-shift/' . $encrypted_shift_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';

            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Shift's data
     *  @Shree on 17 July 2018
     *  @modify by Pratyush on 19 July 2018.
    **/
    public function destroy($id)
    {
        $shift_id = get_decrypted_value($id, true);
        $shift    = Shift::find($shift_id);
       
        $success_msg = $error_message =  "";
        if ($shift)
        {
            DB::beginTransaction();
            try
            {
                $shift->delete();
                $success_msg = "Shift deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/shift/view-shifts')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/shift/view-shifts')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Shift not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Shift's status
     *  @Shree on 18 July 2018
     *  @modify by Pratyush on 19 July 2018.
    **/
    public function changeStatus($status,$id)
    {

        $shift_id = get_decrypted_value($id, true);
        $shift    = Shift::find($shift_id);
        if ($shift)
        {
            $shift->shift_status  = $status;
            $shift->save();
            $success_msg = "Shift status updated!";
            return redirect('admin-panel/shift/view-shifts')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Shift not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
