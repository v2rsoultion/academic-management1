<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookVendor\BookVendor; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class BookVendorController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Book Vendor
     *  @Pratyush on 13 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_book_vendor'),
            'redirect_url'  => url('admin-panel/book-vendor/view-book-vendors'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.book-vendor.index')->with($data);
    }

    /**
     *  Add page for Book Vendor
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $book_vendor	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_book_vendor_id 	= get_decrypted_value($id, true);
            $book_vendor      			= BookVendor::Find($decrypted_book_vendor_id);
            if (!$book_vendor)
            {
                return redirect('admin-panel/book-vendor/add-book-vendor')->withError('Book vendor not found!');
            }
            $page_title             	= trans('language.edit_book_vendor');
            $encrypted_book_vendor_id	= get_encrypted_value($book_vendor->book_vendor_id, true);
            $save_url               	= url('admin-panel/book-vendor/save/' . $encrypted_book_vendor_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_book_vendor');
            $save_url      = url('admin-panel/book-vendor/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'book_vendor' 		=> $book_vendor,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/book-vendor/view-book-vendors'),
        );
        return view('admin-panel.book-vendor.add')->with($data);
    }

    /**
     *  Add and update Book Vendor's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_book_vendor_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $book_vendor = BookVendor::find($decrypted_book_vendor_id);

            if (!$book_vendor)
            {
                return redirect('/admin-panel/book-vendor/add-book-vendor/')->withError('Book Vendor not found!');
            }
            $admin_id = $book_vendor['admin_id'];
            $success_msg = 'Book Vendor updated successfully!';
        }
        else
        {
            $book_vendor    = New BookVendor;
            $success_msg 	= 'Book Vendor saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_vendor_name'   	=> 'required',
                'book_vendor_number'   	=> 'required',
                'book_vendor_email'   	=> 'required|unique:book_vendors,book_vendor_email,' . $decrypted_book_vendor_id . ',book_vendor_id',
                'book_vendor_address'   => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $book_vendor->admin_id       		 = $admin_id;
                $book_vendor->update_by      		 = $loginInfo['admin_id'];
                $book_vendor->book_vendor_name 	 	 = Input::get('book_vendor_name');
                $book_vendor->book_vendor_number 	 = Input::get('book_vendor_number');
                $book_vendor->book_vendor_email 	 = Input::get('book_vendor_email');
                $book_vendor->book_vendor_address 	 = Input::get('book_vendor_address');
                $book_vendor->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/book-vendor/view-book-vendors')->withSuccess($success_msg);
    }

    /**
     *  Get Book Vendor's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $book_vendor      = BookVendor::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('book_vendor_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('vendor_number')))
            {
                $query->where('book_vendor_number', "=", $request->get('vendor_number'));
            }
        })->orderBy('book_vendor_id', 'DESC')->get();
        return Datatables::of($book_vendor)
        ->addColumn('action', function ($book_vendor)
        {
            $encrypted_book_vendor_id = get_encrypted_value($book_vendor->book_vendor_id, true);
            if($book_vendor->book_vendor_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="book-vendor-status/'.$status.'/' . $encrypted_book_vendor_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book-vendor/' . $encrypted_book_vendor_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book-vendor/' . $encrypted_book_vendor_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Book vendor's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function destroy($id)
    {
        $book_vendor_id 		= get_decrypted_value($id, true);
        $book_vendor 		  	= BookVendor::find($book_vendor_id);
        
        $success_msg = $error_message =  "";
        if ($book_vendor)
        {
            DB::beginTransaction();
            try
            {
                $book_vendor->delete();
                $success_msg = "Book vendor deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/book-vendor/view-book-vendors')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/book-vendor/view-book-vendors')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Book vendor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Book vendor's status
     *  @Pratyush on 13 Aug 2018.
    **/
    public function changeStatus($status,$id)
    {
        $book_vendor_id 		= get_decrypted_value($id, true);
        $book_vendor 		  	= BookVendor::find($book_vendor_id);
        if ($book_vendor)
        {
            $book_vendor->book_vendor_status  = $status;
            $book_vendor->save();
            $success_msg = "Book vendor status update successfully!";
            return redirect('admin-panel/book-vendor/view-book-vendors')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Book vendor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
