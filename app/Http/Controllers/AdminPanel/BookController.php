<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Book\Book; // Model
use App\Model\IssueBook\IssueBook; // Model 
use App\Model\Book\BookCopiesInfo; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class BookController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Book
     *  @Pratyush on 14 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_bbook'),
            'redirect_url'  => url('admin-panel/book/view-books'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.book.index')->with($data);
    }

    /**
     *  Add page for Book
     *  @Pratyush on 14 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data                       = [];
        $book                       = [];
        $listData                   = [];
        $cupboard_arr               = get_all_cupbpard_data();
        $book_category              = get_all_book_categoory_data();
        $book_vendor                = get_all_book_vendor_data();
        $book_type                  = get_book_type();
        $listData['arr_cubboard']   = add_blank_option($cupboard_arr, 'Select CupBoard');
        $listData['arr_category']   = add_blank_option($book_category, 'Select Category');
        $listData['arr_vendor']     = add_blank_option($book_vendor, 'Select Vendor');
        $cupboard_shelf             = [];
        $listData['book_type']      = add_blank_option($book_type, 'Select Type');
        $loginInfo                  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_book_id      = get_decrypted_value($id, true);
            $book                   = Book::Find($decrypted_book_id)->with('getBookCopies')->get();
            $book = isset($book[0]) ? $book[0] : [];
           
            if (!$book)
            {
                return redirect('admin-panel/book/add-book')->withError('book not found!');
            }
            $copy_list = [];
            
            foreach ($book['getBookCopies'] as $copy)
            {
                $copy_data['book_info_id']          = $copy['book_info_id'];
                $copy_data['book_id']               = $copy['book_id'];
                $copy_data['book_unique_id']        = $copy['book_unique_id'];
                $copy_data['exclusive_for_staff']   = $copy['exclusive_for_staff'];
                $copy_list[] = $copy_data;
            }
            $book->addition_copy = $copy_list;
            
            $cupboard_id            = $book->book_cupboard_id; 
            $cupboard_shelf         = get_cupboard_shelf($cupboard_id);

            $page_title             = trans('language.edit_bbook');
            $encrypted_book_id      = get_encrypted_value($book->book_id, true);
            $save_url               = url('admin-panel/book/save/' . $encrypted_book_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_bbook');
            $save_url      = url('admin-panel/book/save');
            $submit_button = 'Save';
        }

        $listData['cupboard_shelf']    = add_blank_option($cupboard_shelf, 'Select CupBoard Shelf');
        
        $data                          = array(
            'page_title'        => $page_title,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'book'              => $book,
            'listData'          => $listData,
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/book/view-books'),
        );
        return view('admin-panel.book.add')->with($data);
    }

    /**
     *  Get CupBoard Shelf data according CupBoard
     *  @Shree on 16 Aug 2018
    **/
    public function getCupboardShelfData()
    {
        $cupboard_id    = Input::get('cupboard_id');
        $cupboard_shelf = get_cupboard_shelf($cupboard_id);
        $data           = view('admin-panel.book.ajax-select',compact('cupboard_shelf'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add and update Book's data
     *  @Pratyush on 16 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        //dd($request->has('')? 1 : 0);    
        $loginInfo                  = get_loggedin_user_data();
        $decrypted_book_id          = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $book = Book::find($decrypted_book_id);

            if (!$book)
            {
                return redirect('/admin-panel/book/add-book/')->withError('Book not found!');
            }
            $success_msg = 'Book updated successfully!';
        }
        else
        {
            $book           = New Book;
            $success_msg    = 'Book saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_name'   => 'required|unique:books,book_name,' . $decrypted_book_id . ',book_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $book->admin_id                 = $loginInfo['admin_id'];
                $book->update_by                = $loginInfo['admin_id'];
                $book->book_name                = Input::get('book_name');
                $book->book_category_id         = Input::get('book_category_id');
                $book->book_type                = Input::get('book_type');
                $book->book_subtitle            = Input::get('book_subtitle');
                $book->book_isbn_no             = Input::get('book_isbn_no');
                $book->author_name              = Input::get('author_name');
                $book->publisher_name           = Input::get('publisher_name');
                $book->edition                  = Input::get('edition');
                $book->book_vendor_id           = Input::get('book_vendor_id');
                $book->book_copies_no           = 0;
                $book->book_cupboard_id         = Input::get('book_cupboard_id');
                $book->book_cupboardshelf_id    = Input::get('book_cupboardshelf_id');
                $book->book_price               = Input::get('book_price');
                $book->book_remark              = Input::get('book_remark');
                $book->save();
                $temp_id = $book->book_id;

            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $temp_id = '';
                //p($error_message);
                return redirect()->back()->withErrors($error_message);
            }
            
            if($temp_id != ''){
                
                foreach ($request->get('addition_copy') as $value){
                    if(isset($value['staff'])) {
                        $for_staff = 1;
                    }else{
                        $for_staff = 0;
                    }
                   
                    if(isset($value['unique_no']) && !empty($value['unique_no'])) {
                        if(isset($value['book_info_id']) ) {
                           
                            $book_copies                            = BookCopiesInfo::where('book_info_id', $value['book_info_id'])->first();
                            $validatior = Validator::make($value, [
                                'unique_no'   => 'required|unique:book_copies_info,book_unique_id,' . $book_copies->book_info_id . ',book_info_id',
                            ]);
                    
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $book_copies->admin_id               = $loginInfo['admin_id'];
                            $book_copies->update_by              = $loginInfo['admin_id'];
                            $book_copies->book_id                = $temp_id;
                            $book_copies->book_unique_id         = $value['unique_no'];
                            $book_copies->exclusive_for_staff    = $for_staff;
                            $book_copies->save();
                        } else {
                            $validatior = Validator::make($value, [
                                'unique_no'   => 'required|unique:book_copies_info,book_unique_id,' . $decrypted_book_id . ',book_info_id',
                            ]);
                    
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $book_copies = new BookCopiesInfo; 
                            $book_copies->admin_id               = $loginInfo['admin_id'];
                            $book_copies->update_by              = $loginInfo['admin_id'];
                            $book_copies->book_id                = $temp_id;
                            $book_copies->book_unique_id         = $value['unique_no'];
                            $book_copies->exclusive_for_staff    = $for_staff;
                            $book_copies->save();
                        }
                    }
                }
            }

            DB::commit();
        }
        return redirect('admin-panel/book/view-books')->withSuccess($success_msg);
    }

    /**
     *  Get Book's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        
        $loginInfo          = get_loggedin_user_data();
        
        $book = Book::where(function($query) use ($loginInfo,$request){
                
            if (!empty($request) && $request->has('book_name') && $request->get('book_name') != null)
            {
                $query->where('book_name', "like", "%{$request->get('book_name')}%");
            }
            if (!empty($request) && $request->has('publisher') && $request->get('publisher') != null)
            {
                $query->where('publisher_name', "like", "%{$request->get('publisher')}%");
            }
            if (!empty($request) && $request->has('subtitle') && $request->get('subtitle') != "")
            {
                $query->where('book_subtitle', "like", "%{$request->get('subtitle')}%");
            }
        })->with('getCategory')->with('getCupBoard')->with('getCupBoardShelf')->orderBy('book_id', 'DESC')->get();
        
        return Datatables::of($book)
                ->addColumn('issued_copies', function ($book)
                {
                    return IssueBook::where('book_id',$book->book_id)->where('issued_book_status',1)->count();

                })
                ->addColumn('category', function ($book)
                {
                    $category = "";
                    if(isset($book['getCategory']['book_category_name']) && !empty($book['getCategory']['book_category_name'])) {
                        $category = $book['getCategory']['book_category_name'];
                    }
                    return $category;

                })
                ->addColumn('info', function ($book)
                {
                    $no_of_copy = 0;
                    $no_of_copy = BookCopiesInfo::where(function($query) use ($book) 
                    {
                        $query->where('book_id', "=", $book->book_id);
                    
                    })->get()->count();  
                    if($no_of_copy == 0) {
                        return "No Copy available.";
                    } else { 

                        return '<button type="button" class="btn btn-raised btn-primary copies" book-name="'.$book->book_name.'"  book-id="'.$book->book_id.'" >
                    '.$no_of_copy.' Copies
                    </button>';
                    }
                })
                ->addColumn('cupboard_shelf', function ($book)
                {
                    return $book['getCupBoard']->book_cupboard_name.' + '. $book['getCupBoardShelf']->book_cupboardshelf_name;
                })
                ->addColumn('action', function ($book)
                {
                    $encrypted_book_id = get_encrypted_value($book->book_id, true);
                    if($book->book_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="book-status/'.$status.'/' . $encrypted_book_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book/' . $encrypted_book_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book/' . $encrypted_book_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action', 'info'=>'info'])->addIndexColumn()
                ->make(true);
    }

    public function displayBook($id){
        $loginInfo          = get_loggedin_user_data();
        $decrypted_book_id  = get_decrypted_value($id, true);
        $book               = Book::where('book_id',$decrypted_book_id)->with('getCupBoard')->with('getCupBoardShelf')->orderBy('book_id', 'ASC')->first();

        $data = array(
            'page_title'    => trans('language.view_bbook'),
            'redirect_url'  => url('admin-panel/book/view-books'),
            'login_info'    => $loginInfo,
            'book'          => $book,
            'id'            => $id    
        );
        return view('admin-panel.book.view_book_details')->with($data);

    }

    /**
     *  Get Book's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function singleBookData(Request $request)
    {
        $decrypted_book_id  = get_decrypted_value($request->get('b_id'), true);
        
        $loginInfo          = get_loggedin_user_data();
        $book               = BookCopiesInfo::where('book_id',$decrypted_book_id)->orderBy('book_info_id', 'ASC')->get();
        
        return Datatables::of($book)
                ->addColumn('issued_copies', function ($book)
                {
                    if($book->book_copy_status == 1) {
                        $status = 'Issued';
                    } else {
                        $status = 'Available';
                    }
                    return $status;
                })
                ->addColumn('action', function ($book)
                {
                    $encrypted_book_id = get_encrypted_value($book->book_info_id, true);
                    
                    return '<div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="../delete-single-book/' . $encrypted_book_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy book's data
     *  @Pratyush on 17 Aug 2018.
    **/
    public function destroy($id)
    {
        $book_id       = get_decrypted_value($id, true);
        $book          = Book::find($book_id);
        
        $success_msg = $error_message =  "";
        if ($book)
        {
            DB::beginTransaction();
            try
            {
                $book->delete();
                $success_msg = "Book deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/book/view-books')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/book/view-books')->withErrors($error_message);
            }
            
        }
        else
        {
            $error_message = "Book not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy book Copies's data
     *  @Pratyush on 17 Aug 2018.
    **/
    public function destroySingleBook($id)
    {
        //$book_copy_id    = get_decrypted_value($id, true);
        $book_copy       = BookCopiesInfo::find($id);
       
        $success_msg = $error_message =  "";
        if ($book_copy)
        {
            DB::beginTransaction();
            try
            {
                $book_copy->delete();
                $success_msg = "Book copy deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }
        else
        {
            $error_message = "Book's copy not found!";
            return $error_message;
        }
    }

     /**
     *  Get copies's Data for view page(Datatables)
     *  @shree on 13 Oct 2018.
    **/
    public function copies(Request $request)
    {
        $book_copy  	= BookCopiesInfo::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('book_id', "=", $request->get('id'));
            }
        })->orderBy('book_id', 'DESC')->get();

        return Datatables::of($book_copy)
        ->addColumn('exclusive_for_staff', function ($book_copy)
        {
            if($book_copy->exclusive_for_staff == 0){
                $status = "No";
            } else {
                $status = "Yes";
            }
            return $status;
        })
        ->addColumn('book_copy_status', function ($book_copy)
        {
            if($book_copy->book_copy_status == 0){
                $status = "Available";
            } else {
                $status = "Issued";
            }
            return $status;
        })
        ->addColumn('action', function ($book_copy)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini book-records" book-id="'.$book_copy->book_id.'" data-toggle="tooltip" title="Delete" copy-record="'.$book_copy->book_info_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action', 'exclusive_for_staff'=> 'exclusive_for_staff', 'book_copy_status'=>'book_copy_status'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Change Book's status
     *  @Shree on 19 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $book_id       = get_decrypted_value($id, true);
        $book          = Book::find($book_id);
        if ($book)
        {
            $book->book_status  = $status;
            $book->save();
            $success_msg = "Book status update successfully!";
            return redirect('admin-panel/book/view-books')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "book not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
