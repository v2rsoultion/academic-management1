<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use URL;
use App\Model\LibraryMember\LibraryMember;
use App\Model\Book\Book;
use App\Model\IssueBook\IssueBook;
use Yajra\Datatables\Datatables;
use Redirect;

class LibraryReportController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Registered Student Data
     *  @Khushbu on 07 March 2019
    **/
    public function registeredStudent() {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.menu_library_report'),
            'redirect_url'  => url('admin-panel/library-report/registered-student-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.library-report.registered_student_report')->with($data);
    }

    public function anyDataRegisteredStudent(Request $request){
        // p($request->all());
        $session    = get_current_session();
        $library    = LibraryMember::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->where('admission_class_id',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null)
            {
                $query->where('admission_section_id',$request->get('section_id'));
            }
        })
        ->where('library_member_type',1)->leftjoin('students', function($join) use($request) {
            $join->on('library_members.member_id', '=', 'students.student_id');
        })
        ->leftjoin('student_parents', function($join) use($request) {
            $join->on('students.student_parent_id', '=', 'student_parents.student_parent_id');
        })
        ->leftjoin('student_academic_info', function($join) use($request) {
            $join->on('students.student_id', '=', 'student_academic_info.student_id');
        })
        ->leftjoin('sections', function($join) use($request) {
            $join->on('student_academic_info.admission_section_id', '=', 'sections.section_id');
        })
        ->get()->toArray();
        // p($library);
        return Datatables::of($library,$request)      
        ->addColumn('student_enroll_number', function ($library)
        {
            if($library['student_enroll_number'] == '') {
               $student_enroll_number = '---';
            } else {
                $student_enroll_number = $library['student_enroll_number'];
            }
            return $student_enroll_number;
        })
        ->addColumn('student_profile', function ($library)
        {
            $profile = "";
            if($library['student_image'] != ''){
                $profile = check_file_exist($library['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $library['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('class_section', function ($library)
        {
            return get_selected_classes($library['admission_class_id']). ' - ' . $library['section_name'];
        })
       ->rawColumns(['student_enroll_number' => 'student_enroll_number', 'student_profile' => 'student_profile', 'class_section' => 'class_section'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Books Data
     *  @Khushbu on 07 March 2019
    **/
    public function getBooks() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $arr_book           = get_all_books();
        $map['arr_book']    = add_blank_option($arr_book, "Select Book");
        $data = array(
            'page_title'    => trans('language.menu_library_report'),
            'redirect_url'  => url('admin-panel/library-report/books-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.library-report.books_report')->with($data);
    }

    public function anyDataBooks(Request $request){
        // p($request->all());
        $session    = get_current_session();
        $book       = Book::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('book_id')) && $request->get('book_id') != null)
            {
                $query->where('book_id',$request->get('book_id'));
            }
        })->with('getCategory')->get()->toArray();
        // p($book);
        return Datatables::of($book)      
        ->addColumn('book_category', function($book) {
            return $book['get_category']['book_category_name'];
        })
       ->rawColumns(['book_category' => 'book_category'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Issued Books Data
     *  @Khushbu on 07 March 2019
    **/
    public function issuedBooks() {
        $map = $arr_student = $arr_staff =  [];
        $loginInfo          = get_loggedin_user_data();
        $arr_book           = get_all_books();
        $map['arr_book']    = add_blank_option($arr_book, "Select Book");
        $map['member_type'] = \Config::get('custom.book_member_type');
        $student_member_name = IssueBook::where([['issued_book_status',1],['member_type',1]])->leftjoin('students', function($query) {
            $query->on('issued_books.member_id','students.student_id');
        })->get()->toArray();
        foreach($student_member_name as $key => $value) {
            $arr_student[$value['student_id']] = $value['student_name'];
        }

        $staff_member_name = IssueBook::where([['issued_book_status',1],['member_type',2]])->leftjoin('staff', function($query) {
            $query->on('issued_books.member_id','staff.staff_id');
        })->get()->toArray(); 
        foreach($staff_member_name as $key => $value) {
            $arr_staff[$value['staff_id']] = $value['staff_name'];
        }
        $map['arr_student'] = add_blank_option($arr_student, "Select Student");
        $map['arr_staff']   = add_blank_option($arr_staff, "Select Staff");
        $data = array(
            'page_title'    => trans('language.menu_library_report'),
            'redirect_url'  => url('admin-panel/library-report/issued-books-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.library-report.issued_books_report')->with($data);
    }

    public function anyDataIssuedBooks(Request $request){
        // p($request->all());
        $arr_issued_book =  [];
        $issue_book       = IssueBook::where('issued_book_status',1)->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('book_id')) && $request->get('book_id') != null)
            {
                $query->where('book_id',$request->get('book_id'));
            }
            if (!empty($request) && !empty($request->get('member_type')) && $request->get('member_type') != null)
            {
                $query->where('member_type',$request->get('member_type'));
            }
            if (!empty($request) && !empty($request->get('student_id')) && $request->get('student_id') != null)
            {
                $query->where('student_id',$request->get('student_id'));
            }
            if (!empty($request) && !empty($request->get('staff_id')) && $request->get('staff_id') != null)
            {
                $query->where('staff_id',$request->get('staff_id'));
            }
        })->with('getBook.getCategory','getBookInfo')->leftjoin('students', function($query) {
            $query->on('issued_books.member_id','students.student_id')->where('issued_books.member_type',1);
        })
        ->leftjoin('staff', function($query) {
            $query->on('issued_books.member_id','staff.staff_id')->where('issued_books.member_type',2);
        })
        ->get()->toArray();
        foreach($issue_book as $key => $value) {
            if($value['member_type'] == 1) {
                $member_type = 'Student';
                $member_name = $value['student_name'];
            } else {
                $member_type = 'Staff';
                $member_name = $value['staff_name'];
            }
            $arr_issued_book[]      = array(
                'book_name'         =>  $value['get_book']['book_name'],
                'book_unique_id'    =>  $value['get_book_info']['book_unique_id'],
                'book_category'     =>  $value['get_book']['get_category']['book_category_name'],
                'book_isbn_no'      =>  $value['get_book']['book_isbn_no'],
                'author_name'       =>  $value['get_book']['author_name'],
                'member_type'       =>  $member_type,
                'member_name'       =>  $member_name,
                'duration'          =>  date('d-m-Y', strtotime($value['issued_from_date'])). ' - ' .date('d-m-Y', strtotime($value['issued_to_date'])),
            );
        }
        // p($arr_issued_book);
        return Datatables::of($arr_issued_book)      
       ->rawColumns([])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Expired Dues Date Data
     *  @Khushbu on 08 March 2019
    **/
    public function duesReport() {
        $map = $arr_student = $arr_staff =  [];
        $loginInfo          = get_loggedin_user_data();
        $arr_book           = get_all_books();
        $map['arr_book']    = add_blank_option($arr_book, "Select Book");
        $map['member_type'] = \Config::get('custom.book_member_type');
        $student_member_name = IssueBook::where([['issued_book_status',1],['member_type',1]])->leftjoin('students', function($query) {
            $query->on('issued_books.member_id','students.student_id');
        })->get()->toArray();
        foreach($student_member_name as $key => $value) {
            $arr_student[$value['student_id']] = $value['student_name'];
        }

        $staff_member_name = IssueBook::where([['issued_book_status',1],['member_type',2]])->leftjoin('staff', function($query) {
            $query->on('issued_books.member_id','staff.staff_id');
        })->get()->toArray(); 
        foreach($staff_member_name as $key => $value) {
            $arr_staff[$value['staff_id']] = $value['staff_name'];
        }
        $map['arr_student'] = add_blank_option($arr_student, "Select Student");
        $map['arr_staff']   = add_blank_option($arr_staff, "Select Staff");
        $data = array(
            'page_title'    => trans('language.menu_library_report'),
            'redirect_url'  => url('admin-panel/library-report/dues-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.library-report.dues_report')->with($data);
    }

    public function anyDataDues(Request $request){
        // p($request->all());
        $arr_expire_date_book =  [];
        $expire_date_book    = IssueBook::where([['issued_book_status',1],['issued_to_date','<',date('Y-m-d')]])->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('book_id')) && $request->get('book_id') != null)
            {
                $query->where('book_id',$request->get('book_id'));
            }
            if (!empty($request) && !empty($request->get('member_type')) && $request->get('member_type') != null)
            {
                $query->where('member_type',$request->get('member_type'));
            }
            if (!empty($request) && !empty($request->get('student_id')) && $request->get('student_id') != null)
            {
                $query->where('student_id',$request->get('student_id'));
            }
            if (!empty($request) && !empty($request->get('staff_id')) && $request->get('staff_id') != null)
            {
                $query->where('staff_id',$request->get('staff_id'));
            }
        })->with('getBook.getCategory','getBookInfo')->leftjoin('students', function($query) {
            $query->on('issued_books.member_id','students.student_id')->where('issued_books.member_type',1);
        })
        ->leftjoin('staff', function($query) {
            $query->on('issued_books.member_id','staff.staff_id')->where('issued_books.member_type',2);
        })
        ->get()->toArray();
        foreach($expire_date_book as $key => $value) {
            if($value['member_type'] == 1) {
                $member_type = 'Student';
                $member_name = $value['student_name'];
            } else {
                $member_type = 'Staff';
                $member_name = $value['staff_name'];
            }
            $arr_expire_date_book[]      = array(
                'book_name'         =>  $value['get_book']['book_name'],
                'book_unique_id'    =>  $value['get_book_info']['book_unique_id'],
                'book_category'     =>  $value['get_book']['get_category']['book_category_name'],
                'book_isbn_no'      =>  $value['get_book']['book_isbn_no'],
                'author_name'       =>  $value['get_book']['author_name'],
                'member_type'       =>  $member_type,
                'member_name'       =>  $member_name,
                'expired_date'      =>  date('d-m-Y', strtotime($value['issued_to_date'])),
            );
        }
        // p($arr_expire_date_book);
        return Datatables::of($arr_expire_date_book)      
       ->rawColumns([])->addIndexColumn()
        ->make(true);
    }

    // public function getMemberName() {
    //     // p(Input::get('member_type'));
    //     $arr_student = $arr_staff = [];
    //     $member_type = Input::get('member_type');
    //     if($member_type == 1) {
    //         $student_member_name = IssueBook::where([['issued_book_status',1],['member_type',1]])
    //         ->leftjoin('students', function($query) {
    //             $query->on('issued_books.member_id','students.student_id');
    //         })
    //         ->get()->toArray(); 
    //     }
    //     // p($student_member_name);
    //     foreach($student_member_name as $key => $value) {
    //         $arr_student[$value['student_id']] = $value['student_name'];
    //     }
    //     // p($arr_student);
    //     if($member_type == 2) {
    //         $staff_member_name = IssueBook::where([['issued_book_status',1],['member_type',2]])
    //         ->leftjoin('staff', function($query) {
    //             $query->on('issued_books.member_id','staff.staff_id');
    //         })
    //         ->get()->toArray(); 
    //     }
    //     foreach($staff_member_name as $key => $value) {
    //         $arr_staff[$value['staff_id']] = $value['staff_name'];
    //     }
        
    //     // $arr_member_name = ;
    //     // $data = view('admin-panel.library-report.ajax-select-member-name',compact('arr_student','arr_staff'))->render();
    //     // return response()->json(['options'=>$data]);
    // }
}
