<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Support\Facades\Input;
use App\Model\Student\StudentAttendance;  // Model
use App\Model\Staff\StaffAttendance;  // Model
use App\Model\Admission\AdmissionData;  // Model
use App\Model\Notice\Notice;  // Model
use App\Model\Visitor\Visitor;  // Model
use App\Admin;  // Model
use App\Model\FeesCollection\Receipt;  // Model
use App\Model\Student\Student;  // Model
use App\Model\Notes\Notes;  // Model
use App\Model\Hostel\HostelRooms;   // Model
use App\Model\Hostel\Hostel;   // Model
use App\Model\Hostel\HostelStudentMap;  // Model
use App\Model\Hostel\HostelFeeReceipt;  // Model
use App\Model\IssueBook\IssueBook;
use App\Model\Staff\Staff;  // Model
use App\Model\Purchase\Purchase; // Model
use App\Model\Purchase\PurchaseItems; // Model
use App\Model\Hostel\HostelFeeReceiptDetail; // Model
use App\Model\PlanSchedule\PlanSchedule; // PlanSchedule Model
use Hash;
use Session as LSession;

class DashboardController extends Controller
{
    public function __construct()
    {
        
    }

    public function unauthorized(){
        $loginInfo = get_loggedin_user_data();
        $data                   = array(
            'page_title'    => trans('language.unauthorized'),
            'login_info' => $loginInfo,
            'msg'    	=> 'You cannot access this page!',
        );
        return view('admin-panel.unauthorized')->with($data);
    }
    public function index()
    {
        $dashboard     = $student_attendance =  $student_attend_count  = $online_content = $student_fees_paid = $class_notice_list =  [];
        $loginInfo        = get_loggedin_user_data();
        $session          = get_current_session();
        // p($loginInfo);
        $parent_info      = parent_info($loginInfo['admin_id']);
        $parent_student   = get_parent_student($parent_info['student_parent_id']);
        $present_student  = StudentAttendance::where('student_attendance_date', date('Y-m-d'))->sum('total_present_student');
        $dashboard['student'] = $present_student;
        $present_staff    = StaffAttendance::where('staff_attendance_date', date('Y-m-d'))->sum('total_present_staff');
        $dashboard['staff'] = $present_staff;
        $total_admission    = AdmissionData::where('admission_data_status',2)->get()->count();
        $dashboard['total_admission'] = $total_admission;
        $student_login    = Admin::where('admin_type',4)->get()->count();
        $dashboard['student_login'] = $student_login;
        $parent_login    = Admin::where('admin_type',3)->get()->count();
        $dashboard['parent_login'] = $parent_login;
        $staff_login    = Admin::where('admin_type',2)->get()->count();
        $dashboard['staff_login'] = $staff_login;
        $total_fees = Receipt::where('session_id', '=',$session['session_id'])->sum('net_amount');
        $dashboard['total_fees'] = $total_fees;
        $fees_pending_total   = Receipt::where('session_id', '=',$session['session_id'])->sum('pay_later_amount');
        $fees_collected       = $total_fees-$fees_pending_total;
        $purchase_net_amount = Purchase::sum('net_amount');
        $purchase_return_amount = PurchaseItems::where('purchase_type', 2)->sum('amount');
        $total_expense = $purchase_net_amount - $purchase_return_amount;
        $dashboard['total_expense'] = $total_expense;
        // p($dashboard);
        $admission_list    = AdmissionData::where([['admission_data_status',2],['form_type',0]])->orderBy('admission_data_id', 'DESC')->get();
        $plan_schedule  = PlanSchedule::with('getStaff')->orderBy('plan_schedule_id','DESC')->get();
        // p($plan_schedule);
        $notice_list    = Notice::where('notice_status',1)->orderBy('notice_id', 'DESC')->take(5)->get();
        $visitor_list    = Visitor::orderBy('visitor_id', 'DESC')->take(5)->get();
        $enquiry_list    = AdmissionData::where('form_type',1)->orderBy('admission_data_id', 'DESC')->get();
        if($loginInfo['admin_type'] == 3 || $loginInfo['admin_type'] == 4) {
        $student_attendance = Student::where([['student_parent_id',get_decrypted_value($loginInfo['parent_id'], true)], ['student_id', $loginInfo['student_id']]])->with(['getStudentAttendence' => function($query) use($loginInfo) {
            $query->whereMonth('student_attendance_date', date('m'))->where([['student_id', $loginInfo['student_id']], ['student_attendance_type', 1]]);
        }])->select('student_id', 'student_name')->get();
        $student_attend_count = count($student_attendance[0]['getStudentAttendence']);
        $online_content = Notes::where([['class_id', $loginInfo['class_id']],['section_id', $loginInfo['section_id']]])->with('getSubject')->orderBy('online_note_id', 'DESC')->take(5)->get();
        $student_fees_paid = Receipt::where('student_id', $loginInfo['student_id'])->sum('total_amount');
        $fees_data          = api_get_complete_fees($loginInfo['student_id'],$loginInfo['class_id']);
        $total_fees = 0;
        foreach ($fees_data as $fees_data_res) {
            $total_fees += $fees_data_res['head_amt'];
        }
        $total_due_amount = $total_fees - $student_fees_paid;
        // $student_fees_paid = Receipt::where('student_id', $loginInfo['student_id'])->sum('total_amount');
        $class_notice_list    = Notice::where('notice_status',1)->whereRaw('FIND_IN_SET('.$loginInfo["class_id"].', class_ids) ')->orderBy('notice_id', 'DESC')->take(5)->get();
        }

        if($loginInfo['admin_type'] == 2) {
            $h_room_capacity = HostelRooms::where('h_room_status',1)->sum('h_room_capacity');
            $hostel = Hostel::where('hostel_status',1)->count('hostel_id');
            $occupied_rooms = HostelStudentMap::where([['leave_status', 0], ['h_room_id', '!=', '']])->count();
            $student_allocate_room = HostelStudentMap::where([['leave_status', 0], ['h_room_id', '!=', '']])->with('getStudent','getHostel','getBlock','getFloor','getRoom')->orderBy('h_student_map_id', 'DESC')->get();
            $student_leave_room = HostelStudentMap::where([['leave_status', 1], ['h_room_id', '!=', '']])->with('getStudent','getHostel','getBlock','getFloor','getRoom')->orderBy('h_student_map_id', 'DESC')->get();
            // $h_total_fees = HostelFeeReceipt::where('h_fee_session_id', '=',$session['session_id'])->sum('net_amount');
            $h_fees_collected = HostelFeeReceipt::where('h_fee_session_id', '=',$session['session_id'])->sum('total_paid_amount');
            // $h_fees_collected       = $h_total_fees - $h_student_fees_pending;
            // p($student_leave_room);
            $book_issued = IssueBook::where('issued_book_status',1)->count();
            $book_issued_student = IssueBook::where([['member_type', 1],['issued_book_status',1]])->count();
            $book_issued_staff = IssueBook::where([['member_type', 2],['issued_book_status',1]])->count();
            $renew_date_expire_today = IssueBook::where([['issued_book_status',1],['issued_to_date',date('Y-m-d')]])->count();
            $renew_date_expired = IssueBook::where([['issued_book_status',1],['issued_to_date','<',date('Y-m-d')]])->count();
            $book_expired_list = IssueBook::where([['issued_book_status',1],['issued_to_date','<',date('Y-m-d')]])->with('getBook.getCategory','getBookInfo')->get();
           
            foreach($book_expired_list as $key => $value) {
                
                if($value['member_type'] == 1) {
                    $member_type = 'Student';
                    $member_name = Student::where('student_id',$value['member_id'])->pluck('student_name')->first();
                } else {
                    $member_type = 'Staff';
                    $member_name = Staff::where('staff_id',$value['member_id'])->pluck('staff_name')->first();
                }

                $arr_book_expired_list_data[]  = array(
                    'book_name'         =>  $value['getBook']['book_name'],
                    'book_unique_id'    =>  $value['getBookInfo']['book_unique_id'],
                    'book_category'     =>  $value['getBook']['getCategory']['book_category_name'],
                    'book_isbn_no'      =>  $value['getBook']['book_isbn_no'],
                    'author_name'       =>  $value['getBook']['author_name'],
                    'member_type'       =>  $member_type,
                    'member_name'       =>  $member_name,
                    'expire_date'       =>  $value['issued_to_date'],

                );
            
            }
            $book_issued_list = IssueBook::where([['issued_book_status', 1], ['issued_from_date',date('Y-m-d')]])->with('getBook.getCategory','getBookInfo')->get();
            // p($book_issued_list);
            foreach($book_issued_list as $key => $value) {
                
                if($value['member_type'] == 1) {
                    $member_type = 'Student';
                    $member_name = Student::where('student_id',$value['member_id'])->pluck('student_name')->first();
                } else {
                    $member_type = 'Staff';
                    $member_name = Staff::where('staff_id',$value['member_id'])->pluck('staff_name')->first();
                }

                $arr_book_issued_list_data[]  = array(
                    'book_name'         =>  $value['getBook']['book_name'],
                    'book_unique_id'    =>  $value['getBookInfo']['book_unique_id'],
                    'book_category'     =>  $value['getBook']['getCategory']['book_category_name'],
                    'book_isbn_no'      =>  $value['getBook']['book_isbn_no'],
                    'author_name'       =>  $value['getBook']['author_name'],
                    'member_type'       =>  $member_type,
                    'member_name'       =>  $member_name,
                );
            
            }
            $fees_pending_total   = Receipt::where('session_id', '=',$session['session_id'])->sum('pay_later_amount');
            $fees_collected       = $total_fees-$fees_pending_total;
            $total_concession_applied  = Receipt::where('session_id', '=',$session['session_id'])->sum('total_concession_amount');
            $fees_collected_today   = Receipt::where([['session_id', '=',$session['session_id']],['receipt_date', date('Y-m-d')]])->join('fee_receipt_details', function($query){
                $query->on('fee_receipt_details.receipt_id','fee_receipt.receipt_id');
            })->sum('fee_receipt_details.inst_paid_amt');
            $fees_paid_today_list   = Receipt::where([['session_id', '=',$session['session_id']],['receipt_date', date('Y-m-d')]])->join('fee_receipt_details', function($query){
                $query->on('fee_receipt_details.receipt_id','fee_receipt.receipt_id');
            })->with('getStudent','getClass')->get();
            // $fees_dues_list   = Receipt::where('session_id', '=',$session['session_id'])->get();
            // p($fees_paid_today_list);
            $fees_due_student_list = [];
        }
        
        $data = array(
            'page_title'        => trans('language.dashboard'),
            'login_info'        => $loginInfo,
            'parent_student'    => $parent_student,
            'dashboard'         => $dashboard,
            'admission_list'    => $admission_list,
            'notice_list'       => $notice_list,
            'visitor_list'      => $visitor_list,
            'enquiry_list'      => $enquiry_list,
            'student_attend_count' => $student_attend_count,
            'online_content'    => $online_content,
            'class_notice_list' => $class_notice_list,
            'student_fees_paid' => $student_fees_paid,
            'h_room_capacity'   => $h_room_capacity,
            'hostel'            => $hostel,
            'occupied_rooms'    => $occupied_rooms,
            'student_allocate_room' => $student_allocate_room,
            'student_leave_room' => $student_leave_room,
            'h_fees_collected' => $h_fees_collected,
            'book_issued'       => $book_issued,
            'book_issued_student' => $book_issued_student,
            'book_issued_staff' => $book_issued_staff,
            'renew_date_expire_today' => $renew_date_expire_today,
            'renew_date_expired'    => $renew_date_expired,
            'arr_book_expired_list'     => $arr_book_expired_list_data,
            'arr_book_issued_list'      => $arr_book_issued_list_data,
            'fees_collected'        => $fees_collected,
            'fees_pending_total'  => $fees_pending_total,
            'total_concession_applied'  => $total_concession_applied,
            'fees_collected_today'   => $fees_collected_today,
            'fees_paid_today_list'  => $fees_paid_today_list,
            'fees_due_student_list' => $fees_due_student_list,
            'total_due_amount' => $total_due_amount,
            'plan_schedule'  => $plan_schedule

        );
        // p($loginInfo);
        if($loginInfo['admin_type'] == 0 || $loginInfo['admin_type'] == 1) {
            return view('admin-panel.dashboard.index')->with($data);
        }
        if($loginInfo['admin_type'] == 2) {
            return view('admin-panel.dashboard.staff-dashboard')->with($data);
        }
        if($loginInfo['admin_type'] == 3 || $loginInfo['admin_type'] == 4) {
            return view('admin-panel.dashboard.student-parent-dashboard')->with($data);
        }    
    }


    /**
     *  Set Student Data
     *  @Sandeep on 21 Jan 2019
    **/

    public function set_student_session(Request $request)
    {
        $student_id = Input::get('student_id');
        if (isset($student_id))
        {
            $set_data   = set_student_session_data($student_id);

            $return_arr = array(
                'status'  => 'success',
                'message' => 'Student set successfully!'
            );

        } else {
            $return_arr = array(
                'status'  => 'success',
                'message' => 'Set student as current!'
            );
        }
        return response()->json($return_arr);
    }
}

