<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookCupboard\BookCupboard; // Model
use App\Model\BookCupboardshelf\BookCupboardshelf; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class BookCupboardController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Cub Board
     *  @Pratyush on 13 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_cupboard'),
            'redirect_url'  => url('admin-panel/book-cupboard/view-book-cupboard'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.cupboard.index')->with($data);
    }

    /**
     *  Add page for Cub Board
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $cupboard		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_cupboard_id 	= get_decrypted_value($id, true);
            $cupboard      			= BookCupboard::where('book_cupboard_id', $decrypted_cupboard_id)->with('getCupBoardShelf')->get();
            $cupboard = isset($cupboard[0]) ? $cupboard[0] : [];
            if (!$cupboard)
            {
                return redirect('admin-panel/book-cupboard/add-book-cupboard')->withError('CupBoard not found!');
            }
            $cupboardshelfData = [];
            
            foreach ($cupboard['getCupBoardShelf'] as $cupboardshelf)
            {
                $cupboardshelf_list['book_cupboardshelf_id']      = $cupboardshelf['book_cupboardshelf_id'];
                $cupboardshelf_list['book_cupboard_id']           = $cupboardshelf['book_cupboard_id'];
                $cupboardshelf_list['book_cupboardshelf_name']    = $cupboardshelf['book_cupboardshelf_name'];
                $cupboardshelf_list['book_cupboard_capacity']     = $cupboardshelf['book_cupboard_capacity']; 
                $cupboardshelf_list['book_cupboard_shelf_detail'] = $cupboardshelf['book_cupboard_shelf_detail']; 
                $cupboardshelfData[] = $cupboardshelf_list;
            }
            $cupboard->cupboardshelfData = $cupboardshelfData;
            $page_title             	= trans('language.edit_cupboard');
            $encrypted_book_cupboard_id	= get_encrypted_value($cupboard->book_cupboard_id, true);
            $save_url               	= url('admin-panel/book-cupboard/save/' . $encrypted_book_cupboard_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_cupboard');
            $save_url      = url('admin-panel/book-cupboard/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'cupboard' 			=> $cupboard,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/book-cupboard/view-book-cupboard'),
        );
        return view('admin-panel.cupboard.add')->with($data);
    }

    /**
     *  Add and update CupBoard's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_cupboard_id		= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $cupboard = BookCupboard::find($decrypted_cupboard_id);

            if (!$cupboard)
            {
                return redirect('/admin-panel/book-cupboard/add-book-cupboard/')->withError('CupBoard not found!');
            }
            $success_msg = 'CupBoard updated successfully!';
            $admin_id = $cupboard['admin_id'];
        }
        else
        {
            $cupboard     	= New BookCupboard;
            $success_msg 	= 'CupBoard saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_cupboard_name'   => 'required|unique:book_cupboards,book_cupboard_name,' . $decrypted_cupboard_id . ',book_cupboard_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $cupboard->admin_id       		= $admin_id;
                $cupboard->update_by      		= $loginInfo['admin_id'];
                $cupboard->book_cupboard_name 	= Input::get('book_cupboard_name');
                $cupboard->save();

                foreach($request->get('cupboardshelfData') as $cupboardshelfData){ 
                    if(isset($cupboardshelfData['book_cupboardshelf_name']) && !empty($cupboardshelfData['book_cupboardshelf_name'])) {
                        if(isset($cupboardshelfData['book_cupboardshelf_id']) ) {
                           
                            $cupboardshelf_update   = BookCupboardshelf::where('book_cupboardshelf_id', $cupboardshelfData['book_cupboardshelf_id'])->first();
                            $validatior     = Validator::make($cupboardshelfData, [
                                'book_cupboardshelf_name'   => 'required'
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $cupboardshelf_update->admin_id                   = $admin_id;
                            $cupboardshelf_update->update_by                  = $loginInfo['admin_id'];
                            $cupboardshelf_update->book_cupboard_id           = $cupboardshelf_update->book_cupboard_id;
                            $cupboardshelf_update->book_cupboardshelf_name    = $cupboardshelfData['book_cupboardshelf_name'];
                            $cupboardshelf_update->book_cupboard_capacity     = $cupboardshelfData['book_cupboard_capacity'];
                            $cupboardshelf_update->book_cupboard_shelf_detail = $cupboardshelfData['book_cupboard_shelf_detail'];
                            $cupboardshelf_update->save();
                        } else {
                            
                            $cupboardshelf   = new BookCupboardshelf;
                            $validatior = Validator::make($cupboardshelfData, [
                                'book_cupboardshelf_name'   => 'required'
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $cupboardshelf->admin_id                   = $admin_id;
                            $cupboardshelf->update_by                  = $loginInfo['admin_id'];
                            $cupboardshelf->book_cupboard_id           = $cupboard->book_cupboard_id;
                            $cupboardshelf->book_cupboardshelf_name    = $cupboardshelfData['book_cupboardshelf_name'];
                            $cupboardshelf->book_cupboard_capacity     = $cupboardshelfData['book_cupboard_capacity'];
                            $cupboardshelf->book_cupboard_shelf_detail = $cupboardshelfData['book_cupboard_shelf_detail'];
                            $cupboardshelf->save();
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
    }

    /**
     *  Get Cupboard's Data for view page(Datatables)
     *  @Pratyush on 13 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        
        $cupboard      = BookCupboard::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('book_cupboard_name', "like", "%{$request->get('name')}%");
            }
           
        })->orderBy('book_cupboard_id', 'DESC')->get();
        return Datatables::of($cupboard)
        ->addColumn('cupboard_shelf', function ($cupboard)
        {
            $cupboardshelf = 0;
            $cupboardshelf = BookCupboardshelf::where(function($query) use ($cupboard) 
            {
                $query->where('book_cupboard_id', "=", $cupboard->book_cupboard_id);
            })->get()->count();  
            if($cupboardshelf == 0) {
                return "No Cupboard Shelf available.";
            } else {      
                return '<button type="button" class="btn btn-raised btn-primary cupboardshelfs"  cupboardshelf-id="'.$cupboard->book_cupboard_id.'" >
            '.$cupboardshelf.' Cupboard Shelf
            </button>';
            }
        })
        ->addColumn('action', function ($cupboard)
        {
            $encrypted_cupboard_id = get_encrypted_value($cupboard->book_cupboard_id, true);
            if($cupboard->book_cupboard_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="book-cupboard-status/'.$status.'/' . $encrypted_cupboard_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book-cupboard/' . $encrypted_cupboard_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book-cupboard/' . $encrypted_cupboard_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['cupboard_shelf' => 'cupboard_shelf', 'action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy CupBoard's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function destroy($id)
    {
        $cupboard_id	= get_decrypted_value($id, true);
        $cupboard	  	= BookCupboard::find($cupboard_id);
       
        $success_msg = $error_message =  "";
        if ($cupboard)
        {
            DB::beginTransaction();
            try
            {
                $cupboard->delete();
                $success_msg = "CupBoard deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/book-cupboard/view-book-cupboard')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "CupBoard not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $cupboard_id	= get_decrypted_value($id, true);
        $cupboard	  	= BookCupboard::find($cupboard_id);
        if ($cupboard)
        {
            $cupboard->book_cupboard_status  = $status;
            $cupboard->save();
            $success_msg = "CupBoard status update successfully!";
            return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "CupBoard not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get cupboard shelf's Data for view page(Datatables)
     *  @Khushbu on 04 Apr 2019.
    **/
    public function cupboardshelf(Request $request)
    {
        $cupboardshelf  	= BookCupboardshelf::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('book_cupboard_id', "=", $request->get('id'));
            }
        })->orderBy('book_cupboardshelf_id', 'DESC')->get();
        // p($cupboardshelf);
        return Datatables::of($cupboardshelf)
        ->addColumn('action', function ($cupboardshelf)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini cupboardshelf-records" data-toggle="tooltip" title="Delete" cupboardshelf-record="'.$cupboardshelf->book_cupboardshelf_id.'" cupboard-record="'.$cupboardshelf->book_cupboard_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action', 'cupboardshelf'=> 'cupboardshelf'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Cupboard shelf's data
     *  @Khushbu on 04 Apr 2019.
    **/
    public function destroyCupboardshelf($id)
    {
        $cupboardshelf       = BookCupboardshelf::find($id);
       
        $success_msg = $error_message =  "";
        if ($cupboardshelf)
        {
            DB::beginTransaction();
            try
            {
                $cupboardshelf->delete();
                $success_msg = "Cupboard shelf deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        } else {
            $error_message = "Cupboard shelf not found!";
            return $error_message;
        }
    }

}
