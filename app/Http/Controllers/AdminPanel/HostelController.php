<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\Hostel\Hostel; // Model
use App\Model\Hostel\HostelFeesHead; // Model
use App\Model\Hostel\HostelBlock; // Model
use App\Model\Hostel\HostelFloor; // Model
use App\Model\Hostel\HostelRoomCategory; // Model
use App\Model\Hostel\HostelRooms; // Model
use App\Model\Hostel\HostelStudentMap; // Model
use App\Model\Hostel\HostelFeeReceipt; // Model
use App\Model\Hostel\HostelFeeReceiptDetail; // Model
use Yajra\Datatables\Datatables;
use URL;
use DateTime;
use DateInterval;
use DatePeriod;
use Carbon\Carbon;
use Redirect;

class HostelController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('14',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for Hostel
     *  @Shree on 13 Nov 2018
     *  @modify by Khushbu on 18 Jan 2019
    **/
    public function add_hostel(Request $request, $id = NULL)
    {
        $data    		= [];
        $hostel 		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_hostel_id 	 = get_decrypted_value($id, true);
            $hostel      			 = Hostel::Find($decrypted_hostel_id);
            $hostel['arr_h_fees_head'] = HostelController::getFeesHeadByHostel($hostel->hostel_id);
            if (!$hostel)
            {
                return redirect('admin-panel/hostel/configuration/manage-hostel')->withError('Hostel not found!');
            }
            $page_title             	= trans('language.edit_hostel');
            $encrypted_hostel_id   		= get_encrypted_value($hostel->hostel_id, true);
            $save_url               	= url('admin-panel/hostel/configuration/save-hostel/' . $encrypted_hostel_id);
            $submit_button          	= 'Update';
            // p($hostel);
        }
        else
        {
            $page_title    = trans('language.manage_hostel');
            $save_url      = url('admin-panel/hostel/configuration/save-hostel');
            $submit_button = 'Save';
            // p($hostel);
        }
        $arr_hostel_type            = \Config::get('custom.hostel_type');
        $hostel['arr_hostel_type']  = $arr_hostel_type;
        $hostel['arr_fees_head']    = HostelController::getFeesHead();
        // p($hostel['arr_fees_head']); 
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'hostel' 			=> $hostel,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/hostel/configuration/manage-hostel'),
        );
        return view('admin-panel.hostel.add')->with($data);
    }

    /**
     *  Add and update Hostel's data
     *  @Shree on 13 Nov 2018.
    **/
    public function save_hostel(Request $request, $id = NULL)
    {
        $loginInfo      		   = get_loggedin_user_data();
        $decrypted_hostel_id	   = get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $hostel = Hostel::find($decrypted_hostel_id);
            $admin_id = $hostel['admin_id'];
           
            if (!$hostel)
            {
                return redirect('/admin-panel/hostel/configuration/manage-hostel/')->withError('Hostel not found!');
            }
            $success_msg = 'Hostel updated successfully!';
        }
        else
        {
            $hostel     	= New Hostel;
            $success_msg 	= 'Hostel saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'hostel_name'   => 'required|unique:hostels,hostel_name,' . $decrypted_hostel_id . ',hostel_id',
                
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $hkeys = [];
                foreach ($request->get('fee_head') as $key => $value) {
                    if(isset($value['ex_fees_head_id']) && !isset($value['fees_head_id']))
                    {
                        $hostel_fees_head = HostelStudentMap::whereRaw('FIND_IN_SET('.$key.',h_fees_head_ids)')->get()->count();
                        if($hostel_fees_head != 0) {
                            $fees_head = HostelFeesHead::where('h_fees_head_id', $key)->pluck('h_fees_head_name')->toArray();
                            $error[] = $fees_head[0] . ", this fees head allocated by student so can't update it.";
                            $hkeys[] = $value['ex_fees_head_id'];
                        }
                    }
                    else{
                        $hkeys[] = $value['fees_head_id'];
                    }
                }
                $hostel->admin_id       	= $admin_id;
                $hostel->update_by      	= $loginInfo['admin_id'];
                $hostel->hostel_name 		= Input::get('hostel_name');
                $hostel->hostel_type 	    = Input::get('hostel_type');
                $hostel->h_fees_head_id     = implode(',',$hkeys);
                $hostel->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/hostel/configuration/manage-hostel')->withSuccess($success_msg)->withErrors($error);
    }

    /**
     *  Get hostel's Data for view page(Datatables)
     *  @Shree on 14 Nov 2018
     *  Modify by Khushbu on 18 Jan 2019.
    **/
    public function anyDataHostel(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $hostel  		= Hostel::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('hostel_name', "like", "%{$request->get('name')}%");
            }
          
        })->orderBy('hostel_id', 'DESC')->get();
        // p($hostel[0]->h_fees_head_id);
        
        $arr_hostel_type       = \Config::get('custom.hostel_type');
        return Datatables::of($hostel,$request,$arr_hostel_type)
        ->addColumn('hostel_type', function ($hostel) use($arr_hostel_type)
        {
            return $arr_hostel_type[$hostel->hostel_type];
        })
        ->addColumn('hostel_fees_head', function ($hostel)
        {
            $arr_h_fees_head = '---';
            if($hostel->h_fees_head_id != ''){
                $arr_fees_head_id = explode(',',$hostel->h_fees_head_id);
                $arr_fees_head_name = HostelFeesHead::select('h_fees_head_name')->whereIn('h_fees_head_id',$arr_fees_head_id)->get()->toArray();
                $arr_fees_head_name_list = array_column($arr_fees_head_name, 'h_fees_head_name');
                $arr_h_fees_head = implode(', ', $arr_fees_head_name_list);
            }
            return $arr_h_fees_head;
        })
        ->addColumn('action', function ($hostel) use($request)
        {
            if($request->get('hostel_id') != '' && $request->get('hostel_id') != null){
                $edit_manage_path = '../hostel/configuration/manage-hostel';                        
            }else{
                $edit_manage_path = 'hostel/configuration/manage-hostel';
            }
            $encrypted_hostel_id = get_encrypted_value($hostel->hostel_id, true);
            if($hostel->hostel_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/hostel/configuration/hostel-status/'.$status.'/' . $encrypted_hostel_id. ' ').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/hostel/configuration/manage-hostel/' . $encrypted_hostel_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/hostel/configuration/delete-hostel/' . $encrypted_hostel_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'hostel_type' => 'hostel_type', 'hostel_fees_head' => 'hostel_fees_head'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Hostel's data
     *  @Shree on 14 Nov 2018
     *  Modify by Khushbu on 23 Jan 2019.
    **/
    public function destroyHostel($id)
    {
        $hostel_id 	= get_decrypted_value($id, true);
        $hostel 	= Hostel::find($hostel_id);
        $hostel_fees_head = HostelStudentMap::whereRaw('FIND_IN_SET('.$hostel_id.',hostel_id)')->get()->count();
        $success_msg = $error_message =  "";
        if($hostel_fees_head == 0) {
            if ($hostel)
            {
                DB::beginTransaction();
                try
                { 
                    $hostel->delete();
                    $success_msg = "Hostel deleted successfully!";
                }
                catch (\Exception $e)
                {   
                    DB::rollback();
                    $error_message = "Sorry we can't delete it because it's already in used!!";
                }
                DB::commit();
                if($success_msg != ""){
                    return redirect('admin-panel/hostel/configuration/manage-hostel')->withSuccess($success_msg);
                } else {
                    return redirect('admin-panel/hostel/configuration/manage-hostel')->withErrors($error_message);
                }
            }
            else
            {
                $error_message = "Room category not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't delete it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change hostel's status
     *  @Shree on 14 Nov 2018
     *  Modify by Khushbu on 23 Jan 2019.
    **/
    public function changeStatusHostel($status,$id)
    {
        $hostel_id 		= get_decrypted_value($id, true);
        $hostel 		= Hostel::find($hostel_id);
        $hostel_fees_head = HostelStudentMap::whereRaw('FIND_IN_SET('.$hostel_id.',hostel_id)')->get()->count();
        if($hostel_fees_head == 0) {
            if ($hostel)    
            {
                $hostel->hostel_status  = $status;
                $hostel->save();
                $success_msg = "Hostel status update successfully!";
                return redirect('admin-panel/hostel/configuration/manage-hostel')->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Hostel not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't update status it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get hostel data 
     *  @Shree on 14 Nov 2018
    **/
    public function getHostelData()
    {
        $hostels = get_all_hostels();
        $data = view('admin-panel.hostel.ajax-hostel-select',compact('hostels'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add page for Hostel room category
     *  @Shree on 14 Nov 2018
    **/
    public function add_room_cate(Request $request, $id = NULL)
    {
        $data    		= [];
        $room_cate 		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_h_room_cate_id 	= get_decrypted_value($id, true);
            $room_cate      			= HostelRoomCategory::Find($decrypted_h_room_cate_id);
            if (!$room_cate)
            {
                return redirect('admin-panel/hostel/rooms/manage-room-category')->withError('Room category not found!');
            }
            $page_title             	= trans('language.edit_room_cate');
            $encrypted_h_room_cate_id   		= get_encrypted_value($room_cate->h_room_cate_id, true);
            $save_url               	= url('admin-panel/hostel/rooms/save-room-category/' . $encrypted_h_room_cate_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_room_cate');
            $save_url      = url('admin-panel/hostel/rooms/save-room-category');
            $submit_button = 'Save';
        }
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'room_cate' 			=> $room_cate,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/hostel/rooms/manage-room-category'),
        );
        return view('admin-panel.hostel-room-cate.add')->with($data);
    }

    /**
     *  Add and update Hostel Room category's data
     *  @Shree on 14 Nov 2018.
    **/
    public function save_room_cate(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_h_room_cate_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $room_cate = HostelRoomCategory::find($decrypted_h_room_cate_id);
            $admin_id = $room_cate['admin_id'];
            if (!$room_cate)
            {
                return redirect('/admin-panel/hostel/rooms/manage-room-category/')->withError('Room category not found!');
            }
            $success_msg = 'Room category updated successfully!';
        }
        else
        {
            $room_cate     	= New HostelRoomCategory;
            $success_msg 	= 'Room category saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'h_room_cate_name'   => 'required|unique:hostel_room_cate,h_room_cate_name,' . $decrypted_h_room_cate_id . ',h_room_cate_id',
                
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $room_cate->admin_id       	    = $admin_id;
                $room_cate->update_by      	    = $loginInfo['admin_id'];
                $room_cate->h_room_cate_name 	= Input::get('h_room_cate_name');
                $room_cate->h_room_cate_fees 	= Input::get('h_room_cate_fees');
                $room_cate->h_room_cate_des 	= Input::get('h_room_cate_des');
                $room_cate->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/hostel/rooms/manage-room-category')->withSuccess($success_msg);
    }

    /**
     *  Get hostel room category's Data for view page(Datatables)
     *  @Shree on 14 Nov 2018.
    **/
    public function anyDataRoomCate(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $room_cate  	= HostelRoomCategory::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('h_room_cate_name', "like", "%{$request->get('name')}%");
            }
          
        })->orderBy('h_room_cate_id', 'DESC')->get();
        return Datatables::of($room_cate,$request)
        
        ->addColumn('action', function ($room_cate) use($request)
        {
            if($request->get('h_room_cate_id') != '' && $request->get('h_room_cate_id') != null){
                $edit_manage_path = '../hostel/rooms/manage-room-category';                        
            }else{
                $edit_manage_path = 'hostel/rooms/manage-room-category';
            }
            $encrypted_h_room_cate_id = get_encrypted_value($room_cate->h_room_cate_id, true);
            if($room_cate->h_room_cate_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/hostel/rooms/room-category-status/'.$status.'/' . $encrypted_h_room_cate_id. ' ').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/hostel/rooms/manage-room-category/' . $encrypted_h_room_cate_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/hostel/rooms/delete-room-category/' . $encrypted_h_room_cate_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'hostel_type' => 'hostel_type'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Hostel room category's data
     *  @Shree on 14 Nov 2018.
    **/
    public function destroyRoomCate($id)
    {
        $h_room_cate_id = get_decrypted_value($id, true);
        $room_cate 	    = HostelRoomCategory::find($h_room_cate_id);
       
        $success_msg = $error_message =  "";
        if ($room_cate)
        {
            DB::beginTransaction();
            try
            {
                $room_cate->delete();
                $success_msg = "Room category deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/hostel/rooms/manage-room-category')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/hostel/rooms/manage-room-category')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Room category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change hostel room category's status
     *  @Shree on 14 Nov 2018.
    **/
    public function changeStatusRoomCate($status,$id)
    {
        $h_room_cate_id = get_decrypted_value($id, true);
        $room_cate 		= HostelRoomCategory::find($h_room_cate_id);
        if ($room_cate)
        {
            $room_cate->h_room_cate_status  = $status;
            $room_cate->save();
            $success_msg = "Room category status update successfully!";
            return redirect('admin-panel/hostel/rooms/manage-room-category')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Room category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get Room category data 
     *  @Shree on 14 Nov 2018
    **/
    public function getRoomCategoryData()
    {
        $room_cate = get_all_room_categories();
        $data = view('admin-panel.hostel-room-cate.ajax-room-cate-select',compact('room_cate'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add page for Hostel block
     *  @Shree on 13 Nov 2018
    **/
    public function add_block(Request $request, $id = NULL)
    {
        $data    		= [];
        $block 			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_h_block_id 	= get_decrypted_value($id, true);
            $block      			= HostelBlock::Find($decrypted_h_block_id);
            $block      		    = HostelBlock::where('h_block_id', $decrypted_h_block_id)->with('getFloors')->get();
            $block = isset($block[0]) ? $block[0] : [];
            if (!$block)
            {
                return redirect('admin-panel/hostel/configuration/manage-hostel-block')->withError('Hostel block not found!');
            }
            $floorData = [];
            
            foreach ($block['getFloors'] as $floors)
            {
                $floor_list['h_floor_id']         = $floors['h_floor_id'];
                $floor_list['h_block_id']  = $floors['h_block_id'];
                $floor_list['h_floor_no']        = $floors['h_floor_no'];
                $floor_list['h_floor_rooms']        = $floors['h_floor_rooms']; 
                $floor_list['h_floor_description']       = $floors['h_floor_description']; 
                $floorData[] = $floor_list;
            }
            $block->floorData = $floorData;
            $page_title             	= trans('language.edit_hostel_block');
            $encrypted_h_block_id   	= get_encrypted_value($block->h_block_id, true);
            $save_url               	= url('admin-panel/hostel/configuration/save-hostel-block/' . $encrypted_h_block_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_hostel_block');
            $save_url      = url('admin-panel/hostel/configuration/save-hostel-block');
            $submit_button = 'Save';
        }
        $arr_hostels       = get_all_hostels();
        $block['arr_hostels']   = add_blank_option($arr_hostels, 'Select Hostel');
        //p($block);
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'block' 			=> $block,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/hostel/configuration/manage-hostel-block'),
        );
        return view('admin-panel.hostel-block.add')->with($data);
    }

    /**
     *  Add and update Hostel's data
     *  @Shree on 13 Nov 2018.
    **/
    public function save_block(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_h_block_id		= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $block    = HostelBlock::find($decrypted_h_block_id);
            $admin_id = $block['admin_id'];
            if (!$block)
            {
                return redirect('/admin-panel/hostel/configuration/manage-hostel-block/')->withError('Hostel block not found!');
            }
            $success_msg = 'Hostel block updated successfully!';
        }
        else
        {
            $block     		= New HostelBlock;
            $success_msg 	= 'Hostel block saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'h_block_name'   => 'required|unique:hostel_blocks,h_block_name,' . $decrypted_h_block_id . ',h_block_id,hostel_id,' . $request->get('hostel_id'),
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $block->admin_id       	= $admin_id;
                $block->update_by      	= $loginInfo['admin_id'];
                $block->hostel_id 		= Input::get('hostel_id');
                $block->h_block_name 	= Input::get('h_block_name');
                $block->save();
                // p($request->get('floorData'));
                foreach($request->get('floorData') as $floorData){ 
                    
                    if(isset($floorData['h_floor_no']) && !empty($floorData['h_floor_no'])) {
                        if(isset($floorData['h_floor_id']) ) {
                           
                            $floor_update   = HostelFloor::where('h_floor_id', $floorData['h_floor_id'])->first();
                            
                            $validatior     = Validator::make($floorData, [
                                'h_floor_no'   => 'required|unique:hostel_floors,h_floor_no,' . $floor_update->h_floor_id . ',h_floor_id,h_block_id,' . $floor_update->h_block_id,
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $floor_update->admin_id         = $admin_id;
                            $floor_update->update_by        = $loginInfo['admin_id'];
                            $floor_update->h_block_id       = $floor_update->h_block_id;
                            $floor_update->h_floor_no        = $floorData['h_floor_no'];
                            $floor_update->h_floor_rooms        = $floorData['h_floor_rooms'];
                            $floor_update->h_floor_description  = $floorData['h_floor_description'];
                            $floor_update->save();
                        } else {
                            
                            $floor   = new HostelFloor();
                            $validatior = Validator::make($floorData, [
                                'h_floor_no'   => 'required|unique:hostel_floors,h_floor_no,' . $floor->h_floor_id . ',h_floor_id,h_block_id,' . $floor_update->h_block_id,
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $floor->admin_id        = $admin_id;
                            $floor->update_by       = $loginInfo['admin_id'];
                            $floor->h_block_id      = $block->h_block_id;
                            $floor->h_floor_no       = $floorData['h_floor_no'];
                            $floor->h_floor_rooms       = $floorData['h_floor_rooms'];
                            $floor->h_floor_description      = $floorData['h_floor_description'];
                            $floor->save();
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/hostel/configuration/manage-hostel-block')->withSuccess($success_msg);
    }

    /**
     *  Get hostel's Data for view page(Datatables)
     *  @Shree on 14 Nov 2018.
    **/
    public function anyDataBlock(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $block  	= HostelBlock::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('h_block_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null) 
            {
                $query->where('hostel_id', "=", $request->get('hostel_id'));
            }
        })->with('getHostel')->orderBy('h_block_id', 'DESC')->get();
        // p($block);
        return Datatables::of($block)
        
        ->addColumn('floors', function ($block)
        {
            $floors = 0;
            $floors = HostelFloor::where(function($query) use ($block) 
            {
                $query->where('h_block_id', "=", $block->h_block_id);
            })->get()->count();  
            // p($floors);
            if($floors == 0) {
                return "No floor available.";
            } else {      
                return '<button type="button" class="btn btn-raised btn-primary floors"  h_floor_id="'.$block->h_block_id.'" >
            '.$floors.' Floors
            </button>';
            }
        })
        ->addColumn('hostel_name', function ($block)
        {
            return $block['getHostel']->hostel_name;
        })
        
        ->addColumn('action', function ($block)
        {
            $encrypted_h_block_id = get_encrypted_value($block->h_block_id, true);
            if($block->h_block_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/hostel/configuration/hostel-block-status/'.$status.'/' . $encrypted_h_block_id. ' ').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/hostel/configuration/manage-hostel-block/' . $encrypted_h_block_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/hostel/configuration/delete-hostel-block/' . $encrypted_h_block_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                
        })->rawColumns(['action' => 'action', 'floors'=> 'floors'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Hostel's data
     *  @Shree on 14 Nov 2018.
    **/
    public function destroyBlock($id)
    {
        $h_block_id = get_decrypted_value($id, true);
        $block 	    = HostelBlock::find($h_block_id);
       
        $success_msg = $error_message =  "";
        if ($block)
        {
            DB::beginTransaction();
            try
            {
                $block->delete();
                $success_msg = "Hostel block deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/hostel/configuration/manage-hostel-block')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/hostel/configuration/manage-hostel-block')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Hostel block not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change hostel block's status
     *  @Shree on 14 Nov 2018.
    **/
    public function changeStatusBlock($status,$id)
    {
        $h_block_id = get_decrypted_value($id, true);
        $block 		= HostelBlock::find($h_block_id);
        if ($block)
        {
            $block->h_block_status  = $status;
            $block->save();
            $success_msg = "Hostel block status update successfully!";
            return redirect('admin-panel/hostel/configuration/manage-hostel-block')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Hostel block not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Destroy floor's data
     *  @Shree on 14 Nov 2018.
    **/
    public function destroyFloor($id)
    {
        $h_floor_id    = $id;
        $floor       = HostelFloor::find($h_floor_id);
        
        $success_msg = $error_message =  "";
        if ($floor)
        {
            DB::beginTransaction();
            try
            {
                $floor->delete();
                $success_msg = "Floor deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }else{
            $error_message = "Floor not found!";
            return $error_message;
        }
    }

    /**
     *  Get floor's Data for view page(Datatables)
     *  @shree on 14 Nov 2018.
    **/
    public function floors(Request $request)
    {
        $floors  	= HostelFloor::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('h_block_id', "=", $request->get('id'));
            }
        })->orderBy('h_floor_id', 'DESC')->get();
        // p($floors);
        return Datatables::of($floors)
        
        ->addColumn('action', function ($floors)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini floor-records" data-toggle="tooltip" title="Delete" floor-record="'.$floors->h_floor_id.'" block-record="'.$floors->h_block_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Get hostel block data 
     *  @Shree on 14 Nov 2018
    **/
    public function getHostelBlockData()
    {
        $blocks = get_all_hostel_blocks();
        $data = view('admin-panel.hostel-block.ajax-hostel-block-select',compact('blocks'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add page for Hostel room
     *  @Shree on 14 Nov 2018
    **/
    public function add_room(Request $request, $id = NULL)
    {
        $data    		= [];
        $room 	= $arr_block = $arr_floor		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_h_room_id 	= get_decrypted_value($id, true);
            $room      			    = HostelRooms::Find($decrypted_h_room_id);
            if (!$room)
            {
                return redirect('admin-panel/hostel/rooms/manage-room')->withError('Room not found!');
            }
            $arr_block = get_block_by_hostel($room->hostel_id);
            $arr_floor = get_floor_by_block($room->h_block_id);
            $page_title             	= trans('language.edit_room');
            $encrypted_h_room_id   		= get_encrypted_value($room->h_room_id, true);
            $save_url               	= url('admin-panel/hostel/rooms/save-room/' . $encrypted_h_room_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_room');
            $save_url      = url('admin-panel/hostel/rooms/save-room');
            $submit_button = 'Save';
        }
        $arr_hostels      = get_all_hostels();
        $arr_room_cate    = get_all_room_categories();

        $room['arr_hostels'] = add_blank_option($arr_hostels, 'Select Hostel');
        $room['arr_room_cate'] = add_blank_option($arr_room_cate, 'Room Category');
        $room['arr_block'] = add_blank_option($arr_block, 'Select Block');
        $room['arr_floor'] = add_blank_option($arr_floor, 'Selects Floor');
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'room' 			    => $room,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/hostel/rooms/manage-room'),
        );
        return view('admin-panel.hostel-room.add')->with($data);
    }

    /**
     *  Add and update Hostel room's data
     *  @Shree on 13 Nov 2018.
    **/
    public function save_room(Request $request, $id = NULL)
    {

        $loginInfo      		= get_loggedin_user_data();
        $decrypted_h_room_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $room = HostelRooms::find($decrypted_h_room_id);
            $admin_id = $room['admin_id'];
            if (!$room)
            {
                return redirect('/admin-panel/hostel/rooms/manage-room/')->withError('Room not found!');
            }
            $success_msg = 'Room updated successfully!';
        }
        else
        {
            $room     		= New HostelRooms;
            $success_msg 	= 'Room saved successfully!';
        }
        $hostel_id = null;
        if ($request->has('hostel_id'))
        {
            $hostel_id = Input::get('hostel_id');
        }
        $h_block_id = null;
        if ($request->has('h_block_id'))
        {
            $h_block_id = Input::get('h_block_id');
        }
        $h_floor_id = null;
        if ($request->has('h_floor_id'))
        {
            $h_floor_id = Input::get('h_floor_id');
        }
        $validatior = Validator::make($request->all(), [
            'h_room_no'   => 'required|unique:hostel_rooms,h_room_no,' . $decrypted_h_room_id . ',h_room_id,hostel_id,' . $hostel_id.',h_block_id,' . $h_block_id.',h_floor_id,' . $h_floor_id,
            'h_room_alias'   => 'required',
            'h_room_capacity'   => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $room->admin_id       	= $admin_id;
                $room->update_by      	= $loginInfo['admin_id'];
                $room->hostel_id 		= Input::get('hostel_id');
                $room->h_block_id 	    = Input::get('h_block_id');
                $room->h_floor_id 	    = Input::get('h_floor_id');
                $room->h_room_cate_id 	= Input::get('h_room_cate_id');
                $room->h_room_no 	    = Input::get('h_room_no');
                $room->h_room_alias 	= Input::get('h_room_alias');
                $room->h_room_capacity  = Input::get('h_room_capacity');
                $room->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/hostel/rooms/manage-room')->withSuccess($success_msg);
    }

    /**
     *  Get hostel room's Data for view page(Datatables)
     *  @Shree on 14 Nov 2018.
    **/
    public function anyDataRoom(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $room  		= HostelRooms::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('h_room_no', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null) 
            {
                $query->where('hostel_id', "=", $request->get('hostel_id'));
            }
          
        })->with('getHostel')->with('getBlock')->with('getFloor')->with('getRoomCate')->orderBy('h_room_id', 'DESC')->get();

        return Datatables::of($room,$request)
        ->addColumn('hostel_name', function ($room)
        {
            return $room['getHostel']->hostel_name;
        })
        ->addColumn('block_name', function ($room)
        {
            return $room['getBlock']->h_block_name;
        })
        ->addColumn('floor_name', function ($room)
        {
            return $room['getFloor']->h_floor_no;
        })
        ->addColumn('room_category', function ($room)
        {
            return $room['getRoomCate']->h_room_cate_name;
        })
        ->addColumn('action', function ($room) use($request)
        {
            if($request->get('h_room_id') != '' && $request->get('h_room_id') != null){
                $edit_manage_path = '../hostel/rooms/manage-room';                        
            }else{
                $edit_manage_path = 'hostel/rooms/manage-room';
            }
            $encrypted_h_room_id = get_encrypted_value($room->h_room_id, true);
            if($room->h_room_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/hostel/rooms/room-status/'.$status.'/' . $encrypted_h_room_id. ' ').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/hostel/rooms/manage-room/' . $encrypted_h_room_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/hostel/rooms/delete-room/' . $encrypted_h_room_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'hostel_type' => 'hostel_type'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Hostel room's data
     *  @Shree on 14 Nov 2018.
    **/
    public function destroyRoom($id)
    {
        $h_room_id 	= get_decrypted_value($id, true);
        $room 	= HostelRooms::find($h_room_id);
       
        $success_msg = $error_message =  "";
        if ($room)
        {
            DB::beginTransaction();
            try
            {
                $room->delete();
                $success_msg = "Room deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/hostel/rooms/manage-room')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/hostel/rooms/manage-room')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Room not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change room's status
     *  @Shree on 14 Nov 2018.
    **/
    public function changeStatusRoom($status,$id)
    {
        $h_room_id 	= get_decrypted_value($id, true);
        $room 		= HostelRooms::find($h_room_id);
        if ($room)
        {
            $room->h_room_status  = $status;
            $room->save();
            $success_msg = "Room status update successfully!";
            return redirect('admin-panel/hostel/rooms/manage-room')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Room not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get block data according hostel
     *  @Shree on 14 Nov 2018
    **/
    public function getBlockData()
    {
        $hostel_id = Input::get('hostel_id');
        $block = get_block_by_hostel($hostel_id);
        if(!empty(Input::get('hostel_block_id'))) {
            $hostel_block_id = Input::get('hostel_block_id');
        } else {
            $hostel_block_id= '';
        }
            $data = view('admin-panel.hostel-block.ajax-select',compact('block','hostel_block_id'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get floor data according block
     *  @Shree on 14 Nov 2018
    **/
    public function getFloorData()
    {
        $h_block_id = Input::get('h_block_id');
        $floor = get_floor_by_block($h_block_id);
        if(!empty(Input::get('hostel_floor_id'))) {
            $hostel_floor_id = Input::get('hostel_floor_id');
        } else {
            $hostel_floor_id= '';
        }
        $data = view('admin-panel.hostel-block.ajax-select-floor',compact('floor','hostel_floor_id'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get room data according floor
     *  @Shree on 21 Nov 2018
    **/
    public function getRoomData()
    {
        $h_floor_id = Input::get('h_floor_id');
        $room = get_room_by_floor($h_floor_id);
        if(!empty(Input::get('hostel_room_id'))) {
            $hostel_room_id = Input::get('hostel_room_id');
        } else {
            $hostel_room_id= '';
        }
        $data = view('admin-panel.hostel-room.ajax-select-room',compact('room','hostel_room_id'))->render();
        return response()->json(['options'=>$data]);
    }
    /**
     *  Get room availability data according all data
     *  @Shree on 21 Nov 2018
     *  Modify by Khushbu on 21 Jan 2019
    **/
    public function getRoomAvailability()
    {
        $room_capacity = 0;
        $h_room_id = Input::get('h_room_id');
        $h_student_map_id = Input::get('h_student_map_id');
        $h_fees_heads = HostelStudentMap::where('h_student_map_id', $h_student_map_id)->select('h_fees_head_ids')->get();
        $arr_fees_head_id = explode(',',$h_fees_heads[0]['h_fees_head_ids']);
        $arr_fees_head_name = HostelFeesHead::select('h_fees_head_id', 'h_fees_head_name')->whereIn('h_fees_head_id',$arr_fees_head_id)->get()->toArray();
        $arr_fees_head_name_list = array_column($arr_fees_head_name, 'h_fees_head_name', 'h_fees_head_id');
        $arr_h_fees_head = implode(', ', $arr_fees_head_name_list);

        $roomInfo = get_room_info($h_room_id);
        $room_capacity = $roomInfo['h_room_capacity'];
        $roomOccupantsCount = get_room_occupant_count($h_room_id);
        $room_available = $room_capacity - $roomOccupantsCount;
        $result = array(
            'room_capacity' => $room_capacity,
            'room_available' => $room_available,
            'room_occupied' => $roomOccupantsCount,
            'hostel_block_floor' => $roomInfo['hostel_block_floor'],
            'room_category' => $roomInfo['room_category'],
            'h_fees_heads'  => $arr_h_fees_head,
        );
        
        return json_encode($result);
    }

    /**
     *  View page for manage student registration
     *  @shree on 20 Nov 2018
    **/
    public function manage_registration()
    {
        $loginInfo  = get_loggedin_user_data();
        $manage = $arr_section =  [];
        $arr_class  = get_all_classes_mediums();
        $manage['arr_class']    = add_blank_option($arr_class, "Select Class");
        $manage['arr_section']  = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.manage_registration'),
            'redirect_url'  => url('admin-panel/hostel/manage-registration'),
            'login_info'    => $loginInfo,
            'manage'    => $manage,
        );
        return view('admin-panel.hostel-registration.index')->with($data);
    }

    /**
     *  Get student list for student registration
     *  @Shree on 20 Nov 2018
    **/
    public function get_student_list(Request $request)
    {
        $students     = [];
        $students = get_students_for_hostel($request, '0');
        // p($students);
        return Datatables::of($students)
            ->addColumn('student_profile', function ($students)
            {
                $profile = "";
                if($students['profile'] != ''){
                    $profile = "<img src=".$students['profile']." height='30' />";
                }   
                $name = $students['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('join_status', function ($students)
            {
                $join_status = "Registered";
                if($students['join_status'] == 0){
                    $join_status = "------";
                }
                return $join_status;
            })
            ->addColumn('action', function ($students)
            {
                $encrypted_student_id = get_encrypted_value($students['student_id'], true);
                if($students['join_status'] == 1){
                    return '----';
                } else {
                    return '<div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a href="'.url('/admin-panel/hostel/register-student/' . $encrypted_student_id. ' ').'">Register</a></li>
                        </ul>
                    </div>';
                }
               
                
            })
            ->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'action'=> 'action', 'join_status'=> 'join_status'])->addIndexColumn()->make(true);
    }

    function register_student($id){
        $loginInfo              = get_loggedin_user_data();
        $decrypted_student_id 	= get_decrypted_value($id, true);
        $register = new HostelStudentMap();
        $success_msg = '';
        DB::beginTransaction();
        try
        {
            $register->admin_id             = $loginInfo['admin_id'];
            $register->update_by            = $loginInfo['admin_id'];
            $register->student_id           = $decrypted_student_id;
            $register->save();
            $success_msg = "Student register successfully!";
        }
        catch (\Exception $e)
        {  
            DB::rollback();
            $error_message = "Sorry we can't delete it because it's already in used!!";
        }
        DB::commit();
        if($success_msg != ""){
            return redirect()->back()->withSuccess($success_msg);
        } else {
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for room allocation
     *  @shree on 20 Nov 2018
    **/
    public function room_allocation()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $arr_room_cate    = get_all_room_categories();
        $allocate['arr_room_cate'] = add_blank_option($arr_room_cate, 'Room Category');
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_block']  = add_blank_option($arr_block, "Select Block");
        $allocate['arr_floor']  = add_blank_option($arr_floor, "Select Floor");
        $allocate['arr_room']  = add_blank_option($arr_room, "Select Room");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.room_allocation'),
            'redirect_url'  => url('admin-panel/hostel/room-allocation'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-allocation.room_allocation')->with($data);
    }

    /**
     *  Get student list for room allocation
     *  @Shree on 20 Nov 2018
    **/
    public function get_register_student_list(Request $request)
    {
        $students     = [];
        if($request->get('type') != null){
            $students = get_students_for_hostel($request, '2');
        } else {
            $students = get_students_for_hostel($request, '1');
        }

        // p($students);
        return Datatables::of($students)
            ->addColumn('h_fees_head', function ($students)
            {
                $arr_h_fees_head = '---';
                $h_student_map_details = HostelStudentMap::where('h_student_map_id', $students['h_student_map_id'])->select('h_fees_head_ids')->get();
                if($h_student_map_details != '') {
                    $arr_fees_head_id = explode(',',$h_student_map_details[0]['h_fees_head_ids']);
                    $arr_fees_head_name = HostelFeesHead::select('h_fees_head_name')->whereIn('h_fees_head_id',$arr_fees_head_id)->get()->toArray();
                    $arr_fees_head_name_list = array_column($arr_fees_head_name, 'h_fees_head_name');
                    $arr_h_fees_head = implode(', ', $arr_fees_head_name_list);
                }
                return $arr_h_fees_head;
            })
            ->addColumn('student_profile', function ($students)
            {
                $profile = "";
                if($students['profile'] != ''){
                    $profile = "<img src=".$students['profile']." height='30' />";
                }   
                $name = $students['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('action', function ($students)
            {
                $encrypted_student_id = get_encrypted_value($students['student_id'], true);
                    return '<button type="button" class="btn btn-raised btn-primary allocates" hostel-map="'.$students['h_student_map_id'].'"  student-id="'.$students['student_id'].'" >
                    Allocate
                    </button>';
            })
            ->addColumn('student_transfer', function ($students)
            {
                $encrypted_student_id = get_encrypted_value($students['student_id'], true);
                    return '<button type="button" class="btn btn-raised btn-primary transfered" hostel-map="'.$students['h_student_map_id'].'"  student-id="'.$students['student_id'].'" room-id="'.$students['h_room_id'].'" >
                    Transfer
                    </button>';
            })
            ->addColumn('room_leave', function ($students)
            {
                $encrypted_h_student_map_id = get_encrypted_value($students['h_student_map_id'], true);
                return '<div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a href="'.url('/admin-panel/hostel/room-leave-data/' . $encrypted_h_student_map_id. ' ').'">Leave</a></li>
                        </ul>
                    </div>';
            })
            ->addColumn('collect_fees', function ($students)
            {
                $encrypted_h_student_map_id = get_encrypted_value($students['h_student_map_id'], true);
                return '<div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a href="#">Collect Fees</a></li>
                        </ul>
                    </div>';
            })
            ->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'student_transfer' => 'student_transfer','room_leave'=> 'room_leave', 'collect_fees'=>'collect_fees'])->addIndexColumn()->make(true);
    }

    /**
     *  Get student list for manage concession
     *  @Shree on 9 Oct 2018
    **/
    public function room_allocate(Request $request)
    {
        // p($request->all());
        $loginInfo  = get_loggedin_user_data();
        
        $allocate = HostelStudentMap::find($request->get('h_student_map_id'));
        DB::beginTransaction();
            try
            {
                if($request->get('hostel_id') != ''){
                $allocate->hostel_id    = $request->get('hostel_id');
                }
                if($request->get('h_block_id') != ''){
                $allocate->h_block_id   = $request->get('h_block_id');
                }
                if($request->get('h_floor_id') != ''){
                    $allocate->h_floor_id   = $request->get('h_floor_id');
                }
                if($request->get('h_room_cate_id') != ''){
                    $allocate->h_room_cate_id    = $request->get('h_room_cate_id');
                }
                if($request->get('h_room_id') != ''){
                    $allocate->h_room_id    = $request->get('h_room_id');
                }
                if($request->get('student_id') != ''){
                    $allocate->student_id   = $request->get('student_id');
                }
                if($request->get('join_date') != ''){
                    $allocate->join_date    = $request->get('join_date');
                }
                if($request->get('join_date') != ''){
                    $allocate->leave_date   = $request->get('leave_date');
                }
                if($request->get('leave_status') != ''){
                    $allocate->leave_status   = $request->get('leave_status');
                }
                if($request->get('fee_head') != '') {
                    $allocate->h_fees_head_ids  = implode(',',$request->input("fee_head"));
                }
                $allocate->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return $error_message;
                return "fail";
            }
            DB::commit();
            return "success";
    }

    /**
     *  View page for student transfer
     *  @shree on 20 Nov 2018
    **/
    public function student_transfer()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $arr_room_cate    = get_all_room_categories();
        $allocate['arr_room_cate'] = add_blank_option($arr_room_cate, 'Room Category');
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_block']  = add_blank_option($arr_block, "Select Block");
        $allocate['arr_floor']  = add_blank_option($arr_floor, "Select Floor");
        $allocate['arr_room']  = add_blank_option($arr_room, "Select Room");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.student_transfer'),
            'redirect_url'  => url('admin-panel/hostel/student-transfer'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-allocation.student_transfer')->with($data);
    }

    /**
     *  View page for student room leave
     *  @shree on 22 Nov 2018
    **/
    public function room_leave()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.room_leave'),
            'redirect_url'  => url('admin-panel/hostel/room-leave'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-allocation.room_leave')->with($data);
    }

    /**
     *  Get student list for manage concession
     *  @Shree on 9 Oct 2018
    **/
    public function room_leave_by_id($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $student_map = get_decrypted_value($id, true);
        $allocate = HostelStudentMap::find($student_map);
        $success_msg = '';
        DB::beginTransaction();
            try
            {
                $allocate->leave_status   = 1;
                $allocate->save();
                $success_msg = "Student successfully leave room.";
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
    }

    /**
     *  View page for student room leave
     *  @shree on 22 Nov 2018
    **/
    public function collect_fees()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.collect_fees'),
            'redirect_url'  => url('admin-panel/hostel/collect-fees'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-allocation.collect_fees')->with($data);
    }

    /**
     *  View page for register student report
     *  @shree on 20 Nov 2018
    **/
    public function register_student_report()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $allocate['arr_hostels']  = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.registered_student_report'),
            'redirect_url'  => url('admin-panel/hostel/registered-student-report'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-reports.register_student_report')->with($data);
    }

    /**
     *  View page for allocation report
     *  @shree on 20 Nov 2018
    **/
    public function allocation_report()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.allocation_report'),
            'redirect_url'  => url('admin-panel/hostel/allocation-report'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-reports.allocation_report')->with($data);
    }

    /**
     *  View page for leave student report
     *  @shree on 20 Nov 2018
    **/
    public function leave_student_report()
    {
        $loginInfo  = get_loggedin_user_data();
        $allocate = $arr_section = $arr_block = $arr_floor = $arr_room = [];
        $arr_class  = get_all_classes_mediums();
        $arr_hostels = get_all_hostels();
        $allocate['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $allocate['arr_class']    = add_blank_option($arr_class, "Select Class");
        $allocate['arr_section']  = add_blank_option($arr_section, "Select Section");
        // p($allocate);
        $data = array(
            'page_title'    => trans('language.leave_student_report'),
            'redirect_url'  => url('admin-panel/hostel/leave-student-report'),
            'login_info'    => $loginInfo,
            'allocate'    => $allocate,
        );
        return view('admin-panel.hostel-reports.leave_student_report')->with($data);
    }

    /**
     *  View page for Free Space Room report
     *  @shree on 20 Nov 2018
    **/
    public function free_space_room_report()
    {
        $loginInfo  = get_loggedin_user_data();
        $report = $arr_block = $arr_floor = $arr_room = [];
        $arr_hostels = get_all_hostels();
        $report['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $report['arr_block']  =  add_blank_option($arr_block, "Select Block");
        $report['arr_floor']  =  add_blank_option($arr_floor, "Select Floor");
        $report['arr_room']   =  add_blank_option($arr_room, "Select Room");  
        $data = array(
            'page_title'    => trans('language.allocation_report'),
            'redirect_url'  => url('admin-panel/hostel/free-space-room-report'),
            'login_info'    => $loginInfo,
            'report'    => $report,
        );
        return view('admin-panel.hostel-reports.free_space_room_report')->with($data);
    }

    /**
     *  Get hostel room's Data for view page(Datatables)
     *  @Shree on 14 Nov 2018.
    **/
    public function freeSpaceRoomAnyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        // $room       = DB::table("hostel_rooms")->select("hostel_rooms.*",'hostels.hostel_name','hostel_blocks.h_block_name','hostel_floors.h_floor_no','hostel_room_cate.h_room_cate_name', "hostel_student_map.*", "students.student_name",
        //                 DB::raw("(SELECT COUNT(*) FROM hostel_student_map
        //                       WHERE hostel_student_map.h_room_id = hostel_rooms.h_room_id ) as room_occupied"))
                 
        // ->where(function($query) use ($request) 
        // {
        //     if (!empty($request) && !empty($request->get('name')))
        //     {
        //         $query->where('h_room_no', "like", "%{$request->get('name')}%");
        //     }
        //     if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null) 
        //     {
        //         $query->where('hostel_id', "=", $request->get('hostel_id'));
        //     }
        //     if (!empty($request) && !empty($request->get('h_block_id')) && $request->get('h_block_id') != null) 
        //     {
        //         $query->where('h_block_id', "=", $request->get('h_block_id'));
        //     }
        //     if (!empty($request) && !empty($request->get('h_floor_id')) && $request->get('h_floor_id') != null) 
        //     {
        //         $query->where('h_floor_id', "=", $request->get('h_floor_id'));
        //     }
        // })->join('hostels', 'hostel_rooms.h_room_id', '=', 'hostels.hostel_id')
        // ->join('hostel_blocks', 'hostel_rooms.h_block_id', '=', 'hostel_blocks.h_block_id')
        // ->join('hostel_floors', 'hostel_rooms.h_floor_id', '=', 'hostel_floors.h_floor_id')
        // ->join('hostel_room_cate', 'hostel_rooms.h_room_cate_id', '=', 'hostel_room_cate.h_room_cate_id')
        // ->join('hostel_student_map', 'hostel_rooms.h_room_id', '=', 'hostel_student_map.h_room_id')
        // ->join('students', 'hostel_student_map.student_id', '=', 'students.student_id')
        // ->get();

        $room       = HostelRooms::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('h_room_no', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null) 
            {
                $query->where('hostel_id', "=", $request->get('hostel_id'));
            }
            if (!empty($request) && !empty($request->get('h_block_id')) && $request->get('h_block_id') != null) 
            {
                $query->where('h_block_id', "=", $request->get('h_block_id'));
            }
            if (!empty($request) && !empty($request->get('h_floor_id')) && $request->get('h_floor_id') != null) 
            {
                $query->where('h_floor_id', "=", $request->get('h_floor_id'));
            }
            if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null) 
            {
                $query->where('h_room_id', "=", $request->get('h_room_id'));
            }
        })->with('getHostel','getBlock','getFloor','getRoomCate','getStudentsMap.getStudent')->select("*", DB::raw("(SELECT COUNT(*) FROM hostel_student_map
        WHERE hostel_student_map.h_room_id = hostel_rooms.h_room_id ) as room_occupied"))
        ->get()->toArray();
        return Datatables::of($room,$request)
        ->addColumn('hostel_block', function ($room) {
            return $room['get_hostel']['hostel_name'].' - '.$room['get_block']['h_block_name'];
        })
        ->addColumn('h_floor_no', function($room) {
            return $room['get_floor']['h_floor_no'];
        })
        ->addColumn('h_room_cate_name', function($room) {
            return $room['get_room_cate']['h_room_cate_name'];
        })
        ->addColumn('student_name', function($room) {
            if($room['get_students_map'] != ''){
                foreach($room['get_students_map'] as $key => $record) {
                    $encrypted_student_id = get_encrypted_value($record['get_student']['student_id'], true);
                    $student_name .= '<a href="'.url('admin-panel/student/student-profile/'.$encrypted_student_id.'').'">'.$record['get_student']['student_name']."</a></br>";
                }
                if(!empty($student_name)) {
                    return $student_name;
                } else {
                    return '----';
                }
            }
        })
        ->addColumn('room_available', function ($room) {
                return $room['h_room_capacity'] - $room['room_occupied'];
        })
        ->rawColumns(['student_name' => 'student_name', 'room_available' => 'room_available'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Add page for Hostel Fees Head
     *  @Khushbu on 18 Jan 2019
    **/
    public function add_h_fees_head(Request $request, $id = NULL)
    {
        $data               = [];
        $h_fees_head        = [];
        $loginInfo          = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_h_fees_head_id   = get_decrypted_value($id, true);
            $h_fees_head                = HostelFeesHead::find($decrypted_h_fees_head_id);
            if (!$h_fees_head)
            {
                return redirect('admin-panel/hostel/manage-hostel-fees-head')->withError('Hostel Fees Head not found!');
            }
            $page_title                 = trans('language.edit_fee_head');
            $encrypted_h_fees_head_id   = get_encrypted_value($h_fees_head->h_fees_head_id, true);
            $save_url                   = url('admin-panel/hostel/save-hostel-fees-head/' . $encrypted_h_fees_head_id);
            $submit_button              = 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_h_fees_head');
            $save_url      = url('admin-panel/hostel/save-hostel-fees-head');
            $submit_button = 'Save';
        }
        $data = array(
            'page_title'        => $page_title,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'h_fees_head'       => $h_fees_head,
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/hostel/manage-hostel-fees-head'),
        );
        return view('admin-panel.hostel-fees-head.add')->with($data);
    }

    /**
     *  Add and update Hostel Fees Head's data
     *  @Khushbu on 18 Jan 2019.
    **/
    public function save_h_fees_head(Request $request, $id = NULL)
    {

        $loginInfo                  = get_loggedin_user_data();
        $decrypted_h_fees_head_id   = get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $h_fees_head = HostelFeesHead::find($decrypted_h_fees_head_id);
            $admin_id    = $h_fees_head['admin_id'];
            if (!$h_fees_head)
            {
                return redirect('/admin-panel/hostel/manage-hostel-fees-head')->withError('Hostel Fees Head not found!');
            }
            $success_msg = 'Hostel fees head updated successfully!';
        }
        else
        {
            $h_fees_head    = New HostelFeesHead;
            $success_msg    = 'Hostel fees head saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'h_fees_head_name'   => 'required|unique:hostel_fees_head,h_fees_head_name,' . $decrypted_h_fees_head_id . ',h_fees_head_id',
        ]);

        if($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $h_fees_head->admin_id           = $admin_id;
                $h_fees_head->update_by          = $loginInfo['admin_id'];
                $h_fees_head->h_fees_head_name   = Input::get('h_fees_head_name');
                $h_fees_head->h_fees_head_price  = Input::get('h_fees_head_price');
                $h_fees_head->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/hostel/manage-hostel-fees-head')->withSuccess($success_msg);
    }

    /**
     *  Get hostel fees head's Data for view page(Datatables)
     *  @Khushbu on 18 Jan 2019.
    **/
    public function anyDataHostelFeesHead(Request $request)
    {
        $loginInfo      = get_loggedin_user_data();
        $h_fees_head    = HostelFeesHead::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('h_fees_head_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('h_fees_head_id', 'DESC')->get();
        return Datatables::of($h_fees_head)
        ->addColumn('action', function ($h_fees_head) use($request)
        {
            if($request->get('h_fees_head_id') != '' && $request->get('h_fees_head_id') != null){
                $edit_manage_path = '../hostel/manage-hostel-fees-head';                        
            }else{
                $edit_manage_path = 'hostel/manage-hostel-fees-head';
            }
            $encrypted_h_fees_head_id = get_encrypted_value($h_fees_head->h_fees_head_id, true);
            // $hostel_fees_head = Hostel::whereRaw('FIND_IN_SET('.$h_fees_head->h_fees_head_id.',h_fees_head_id)')->get()->count();
            // p($hostel_fees_head);
            if($h_fees_head->h_fees_head_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/hostel/hostel-fess-head-status/'.$status.'/' . $encrypted_h_fees_head_id. ' ').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/hostel/manage-hostel-fees-head/' . $encrypted_h_fees_head_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/hostel/delete-hostel-fees-head/' . $encrypted_h_fees_head_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Hostel Fees Head's data
     *  @Khushbu on 18 Jan 2019.
    **/
    public function destroyHostelFeesHead($id)
    {
        $h_fees_head_id  = get_decrypted_value($id, true);
        $h_fees_head     = HostelFeesHead::find($h_fees_head_id);
        $hostel_fees_head = Hostel::whereRaw('FIND_IN_SET('.$h_fees_head_id.',h_fees_head_id)')->get()->count();
        $success_msg = $error_message =  "";
        if($hostel_fees_head == 0) {
            if ($h_fees_head)
            {
                DB::beginTransaction();
                try
                {
                    $h_fees_head->delete();
                    $success_msg = "Hostel Fees Head deleted successfully!";
                }
                catch (\Exception $e)
                {  
                    DB::rollback();
                    $error_message = "Sorry we can't delete it because it's already in used!!";
                }
                DB::commit();
                if($success_msg != ""){
                    return redirect('admin-panel/hostel/manage-hostel-fees-head')->withSuccess($success_msg);
                } else {
                    return redirect('admin-panel/hostel/manage-hostel-fees-head')->withErrors($error_message);
                }
            }
            else
            {
                $error_message = "Hostel Fees Head not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
             $error_message = "Sorry we can't delete it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Hostel Fees Head status
     *  @Khushbu on 18 Jan 2019.
    **/
    public function changeStatusHostelFeesHead($status,$id)
    {
        $h_fees_head_id  = get_decrypted_value($id, true);
        $h_fees_head     = HostelFeesHead::find($h_fees_head_id);
        $hostel_fees_head = Hostel::whereRaw('FIND_IN_SET('.$h_fees_head->h_fees_head_id.',h_fees_head_id)')->get()->count();

        if($hostel_fees_head == 0) {
            if ($h_fees_head)
            {
                $h_fees_head->h_fees_head_status  = $status;
                $h_fees_head->save();
                $success_msg = "Hostel Fees Head status update successfully!";
                return redirect('admin-panel/hostel/manage-hostel-fees-head')->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Hostel Fees Head not found!";
                return redirect()->back()->withErrors($error_message);
            }
        }
        else {
            $error_message = "Sorry we can't update status it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get Fees Heads list
     *  @Khushbu on 18 Jan 2019
    **/
    function getFeesHead() {
        $arr_fees_head = [];
        $arr_fees_head_list = HostelFeesHead::where(array('h_fees_head_status' => 1))->get();
        if(!empty($arr_fees_head_list))
        {
            foreach ($arr_fees_head_list as $arr_fees_head_data)
                {
                    $arr_fees_head[$arr_fees_head_data['h_fees_head_id']] = $arr_fees_head_data['h_fees_head_name'];
                }
        }
        return $arr_fees_head;
    } 

    /** 
     *  Get Fees Head Data according hostel
     *  @Khushbu on 21 Jan 2019
    **/
    public function getFeesHeadData() {
        $hostel_id = Input::get('hostel_id');
        $arr_hostel = Hostel::where('hostel_id', $hostel_id)->select('h_fees_head_id')->get();
        $arr_fees_head_id = explode(',',$arr_hostel[0]['h_fees_head_id']);
        $arr_fees_head_name = HostelFeesHead::select('h_fees_head_id', 'h_fees_head_name')->whereIn('h_fees_head_id',$arr_fees_head_id)->get()->toArray();
        $arr_fees_head_name_list = array_column($arr_fees_head_name, 'h_fees_head_name', 'h_fees_head_id');
        $data = view('admin-panel.hostel-fees-head.ajax-show-fees-head',compact('arr_fees_head_name_list'))->render();
        return response()->json($data);
    }

    /**
     *  Get Hostel Fees Heads according to hostel
     *  @Khushbu on 22 Jan 2019
    **/
    function getFeesHeadByHostel($hostel_id) {
        $arr_h_fees_head = [];
        $arr_h_fees_head_list = Hostel::where(array('hostel_id' => $hostel_id))->select('h_fees_head_id')->get()->toArray();
        $arr_h_fees_head = explode(',',$arr_h_fees_head_list[0]['h_fees_head_id']);
        
        return $arr_h_fees_head;
    }

    /**
     *  View page for Hostel Fees Due Data
     *  @Khushbu on 25 March 2019
    **/
    public function feesDueAnyData(Request $request) {
        // p($request->all());
        if(!empty($request->get('h_block_id'))) {
            $hostel_block_id = $request->get('h_block_id');
        } else {
            $hostel_block_id = '';
        }
        if(!empty($request->get('h_floor_id'))) {
            $hostel_floor_id = $request->get('h_floor_id');
        } else {
            $hostel_floor_id = '';
        }
        if(!empty($request->get('h_room_id'))) {
            $hostel_room_id = $request->get('h_room_id');
        } else {
            $hostel_room_id = '';
        }
        $arr_class = $arr_class1 = $student = $report = $arr_block = $arr_floor = $arr_room = [];
        $loginInfo           = get_loggedin_user_data();
        $session             = get_current_session();
        $arr_class1          = get_all_classes_mediums();
        $report['arr_class'] = add_blank_option($arr_class1, "Select Class");
        $arr_hostels         = get_all_hostels();
        $report['arr_hostels']    = add_blank_option($arr_hostels, "Select Hostel");
        $report['arr_block']  =  add_blank_option($arr_block, "Select Block");
        $report['arr_floor']  =  add_blank_option($arr_floor, "Select Floor");
        $report['arr_room']   =  add_blank_option($arr_room, "Select Room");  
        $start                = (new DateTime($session['start_date']));
        $end                  = (new DateTime($session['end_date']));
        $interval             = DateInterval::createFromDateString('1 month');
        $period               = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            $sessionPeriodArr[] = array(
                'month'    => $dt->format("m"),
                'year'     => $dt->format("Y"),
                'month_year_string' => $dt->format("F-Y")
            );
        }
        if(!empty($request->get('class_id')) && $request->get('class_id') != null ) {
            // p($request->get('class_id'));
            $student = Student::whereHas('getStudentAcademic', function($query) use($request) {
                $query->where('admission_class_id', $request->get('class_id'))->with('getAdmitClass');
            })
            ->with(['getStudentAcademic' => function($query) use($request) {
                $query->where('admission_class_id', $request->get('class_id'))->with('getAdmitClass');
            }])
            ->join('hostel_student_map', function($join) use($request) {
                $join->on('hostel_student_map.student_id', 'students.student_id');
                if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null) 
                {
                    $join->where('hostel_student_map.hostel_id', $request->get('hostel_id'));
                }
                if (!empty($request) && !empty($request->get('h_block_id')) && $request->get('h_block_id') != null) 
                {
                    $join->where('hostel_student_map.h_block_id', $request->get('h_block_id'));
                }
                if (!empty($request) && !empty($request->get('h_floor_id')) && $request->get('h_floor_id') != null) 
                {
                    $join->where('hostel_student_map.h_floor_id', $request->get('h_floor_id'));
                }
                if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null) 
                {
                    $join->where('hostel_student_map.h_room_id', $request->get('h_room_id'));
                }
            
            })->with('getStudentHostelMap.getHostel','getStudentHostelMap.getBlock','getStudentHostelMap.getFloor','getStudentHostelMap.getRoom')
            ->get()->toArray();
        } else {
            $student = Student::with('getStudentAcademic.getAdmitClass')->join('hostel_student_map', function($join) {
                $join->on('hostel_student_map.student_id', 'students.student_id');
            })->with('getStudentHostelMap.getHostel','getStudentHostelMap.getBlock','getStudentHostelMap.getFloor','getStudentHostelMap.getRoom')
            ->get()->toArray();
        }
        // p($student);
        foreach($student as $key => $record) {
            $studentMonthlyArr = [];
            foreach($period as $dt) {
                $student_monthly     = HostelFeeReceipt::where([['h_fee_student_id',$record['student_id']], [DB::raw("MONTH(receipt_date)"), $dt->format("m")],[DB::raw("YEAR(receipt_date)"), $dt->format("Y")]])->select('h_fee_class_id', 'h_fee_student_id', DB::raw("MONTH(receipt_date) as month"), DB::raw("(SUM(total_paid_amount)) as total_paid_amount"), DB::raw("(SUM(concession_amount)) as total_concession_amount"), DB::raw("(SUM(net_amount))-(SUM(total_paid_amount)) as total_dues_amount"))->get()->toArray();
                
                $studentMonthlyArr[] = array(
                    'month'        => $dt->format("m"),
                    'year'         => $dt->format("Y"),
                    'month_year_string' => $dt->format("F-Y"),
                    'total_paid_amount' => $student_monthly[0]['total_paid_amount'],
                    'total_dues_amount' => $student_monthly[0]['total_dues_amount'],
                    'total_concession_amount' => $student_monthly[0]['total_concession_amount']
                );
            }
            $final_sheet_record[] = array(
                'student_id'    => $record['student_id'],
                'student_name'  => $record['student_name'],
                'class_id'     => $record['get_student_academic']['admission_class_id'],
                'class_name'   => $record['get_student_academic']['get_admit_class']['class_name'],
                'hostel_block_floor_room' => $record['get_student_hostel_map']['get_hostel']['hostel_name'].' - ' .$record['get_student_hostel_map']['get_block']['h_block_name'].' - '.$record['get_student_hostel_map']['get_floor']['h_floor_no'].' - '.$record['get_student_hostel_map']['get_room']['h_room_no'],
                'fees_data' => $studentMonthlyArr
            );
        }    
        // p($final_sheet_record);
        $data = array(
            'page_title'    => trans('language.fees_due_report'),
            'save_url'      => url('admin-panel/hostel/fees-due-report'),
            'redirect_url'  => url('admin-panel/hostel/fees-due-report'),
            'login_info'    => $loginInfo,
            'report'           => $report,
            'sessionPeriodArr' => $sessionPeriodArr,
            'final_sheet_record' => $final_sheet_record,
            'hostel_block_id'    => $hostel_block_id,
            'hostel_floor_id'    => $hostel_floor_id,
            'hostel_room_id'     => $hostel_room_id,
        );
        return view('admin-panel.hostel-reports.fees_dues_report')->with($data);
    }

    /**
     *  Get room data according Room Category
     *  @Khushbu on 03 Apr 2019
    **/
    public function getCateRoomData()
    {
        $hostel_id = Input::get('hostel_id');
        $h_block_id = Input::get('h_block_id');
        $h_floor_id = Input::get('h_floor_id');
        $h_room_cate_id = Input::get('h_room_cate_id');
        $room = get_room_by_roomCategory($hostel_id,$h_block_id,$h_floor_id,$h_room_cate_id);
        if(!empty(Input::get('hostel_room_id'))) {
            $hostel_room_id = Input::get('hostel_room_id');
        } else {
            $hostel_room_id= '';
        }
        $data = view('admin-panel.hostel-room.ajax-select-room',compact('room','hostel_room_id'))->render();
        return response()->json(['options'=>$data]);
    }
}
