<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\Hostel\Hostel; // Model
use App\Model\Hostel\HostelFeesHead; // Model
use App\Model\Hostel\HostelBlock; // Model
use App\Model\Hostel\HostelFloor; // Model
use App\Model\Hostel\HostelRoomCategory; // Model
use App\Model\Hostel\HostelRooms; // Model
use App\Model\Hostel\HostelStudentMap; // Model
use App\Model\Hostel\HostelFeeReceipt; // Model
use App\Model\Hostel\HostelFeeReceiptDetail; // Model
use App\Model\FeesCollection\STCheques; // Model
use Yajra\Datatables\Datatables;
use URL;
use DateTime;
use DateInterval;
use DatePeriod;
use Redirect;


class HostelFeeController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('14',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for heads listing
     *  @shree on 13 Oct 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = $arr_section = [];
        $arr_class = get_all_classes_mediums();
        $listData['arr_class'] = add_blank_option($arr_class, "Select Class");
        $listData['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title' => trans('language.fees_counter'),
            'redirect_url' => url('admin-panel/fees-counter'),
            'login_info' => $loginInfo,
            'listData' => $listData,
        );
        return view('admin-panel.hostel-fees-counter.index')->with($data);
    }
    /**
     *  Get student list for student registration
     *  @Shree on 20 Nov 2018
    **/
    public function get_student_list(Request $request)
    {
        $students     = [];
        $students = get_students_for_hostel($request, '2');
        // p($students);
        return Datatables::of($students)
            ->addColumn('student_profile', function ($students)
            {
                $profile = "";
                if($students['profile'] != ''){
                    $profile = "<img src=".$students['profile']." height='30' />";
                }   
                $name = $students['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('join_status', function ($students)
            {
                $join_status = "Registered";
                if($students['join_status'] == 0){
                    $join_status = "------";
                }
                return $join_status;
            })
            ->addColumn('action', function ($students)
            {
                $encrypted_student_id = get_encrypted_value($students['student_id'], true);
                $encrypted_h_student_map_id = get_encrypted_value($students['h_student_map_id'], true);
                if($students['join_date'] == '----'){
                    return 'Please assign join date';
                } else {
                    return '<div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a href="'.url('/admin-panel/hostel/fees-counter/add/'.$encrypted_h_student_map_id.'/' . $encrypted_student_id. ' ').'">Add</a></li>
                        </ul>
                    </div>';
                }
               
                
            })
            ->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'action'=> 'action', 'join_status'=> 'join_status'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for fees counter
     *  @Shree on 15 Oct 2018
     **/
    public function add($mapid,$encrypted_student_id)
    {
        $student = [];
        $loginInfo = get_loggedin_user_data();
        $student_id = get_decrypted_value($encrypted_student_id, true);
        $h_student_map_id = get_decrypted_value($mapid, true);
        $arr_bank = get_all_bank();

        $arr_student = Student::where(function($query) use ($student_id)
            {
                if (!empty($student_id))
                {
                    $query->where('student_id', $student_id);
                }
            })->with('getParent')->with('getStudentAcademic')
            ->with('getStudentAcademic.getAdmitSession')
            ->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')
            ->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')
            ->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getWalletAc')->get();
        foreach ($arr_student as $key => $student) {
            if (!empty($student['student_id']))
            {
                $complete_fees = [];
                $complete_fees = $this->studentFeesDetails($student_id,$h_student_map_id);
               
                $student_array[] = array(
                    'student_enroll_number'         => $student['student_enroll_number'],
                    'student_roll_no'               => $student['student_roll_no'],
                    'student_id'                    => $student['student_id'],
                    'h_student_map_id'              => $h_student_map_id,
                    'student_name'                  => $student['student_name'],
                    'student_image'                 => check_file_exist($student['student_image'], 'student_image'),
                    'student_father_name'           => $student['getParent']['student_father_name'],
                    'student_father_mobile_number'  => $student['getParent']['student_father_mobile_number'],
                    'current_class'                 => $student['getStudentAcademic']['getCurrentClass']['class_name'],
                    'current_section'               => $student['getStudentAcademic']['getCurrentSection']['section_name'],
                    'complete_fees'                 => $complete_fees,
                    'arr_bank'                      => $arr_bank
                );
                $student_parent_id = $student['getParent']['student_parent_id'];
            }
        }
        if($student_parent_id != null && $student_parent_id != "") {
            $siblings = Student::where(function($query) use ($student_id,$student_parent_id)
            {
                $query->where('student_parent_id','=', $student_parent_id);
                $query->where('student_id','!=', $student_id);
            })
            ->join('map_student_staff_vehicle', function($join) use ($session){
                $join->on('map_student_staff_vehicle.student_staff_id', '=', 'students.student_id');
                $join->where('student_staff_type', 1);
                $join->where('student_staff_status', 1);
                $join->where('session_id', $session['session_id']);
            })
            ->with('getParent')->with('getStudentAcademic')->with('getStudentAcademic.getAdmitSession')->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getWalletAc')->get();
            
            foreach ($siblings as $key => $sibling)
            {
                if (!empty($sibling['student_id']))
                {
                    $complete_fees = [];
                    $complete_fees = $this->studentFeesDetails($sibling['student_id'],$sibling['map_student_staff_id']);
                    
                    $student_array[] = array(
                        'student_enroll_number'         => $sibling['student_enroll_number'],
                        'student_roll_no'               => $sibling['student_roll_no'],
                        'student_id'                    => $sibling['student_id'],
                        'h_student_map_id'              => $sibling['h_student_map_id'],
                        'student_name'                  => $sibling['student_name'],
                        'student_image'                 => check_file_exist($student['sibling'], 'student_image'),
                        'student_father_name'           => $sibling['getParent']['student_father_name'],
                        'student_father_mobile_number'  => $sibling['getParent']['student_father_mobile_number'],
                        'current_class'                 => $sibling['getStudentAcademic']['getCurrentClass']['class_name'],
                        'current_section'               => $sibling['getStudentAcademic']['getCurrentSection']['section_name'],
                        'complete_fees'                 => $complete_fees,
                        'arr_bank'                      => $arr_bank
                    );
                }
            }
        }
        // p($student_array);
        $save_url = url('admin-panel/hostel/fees-counter/save-fees-data/' . $id);
        $data = array(
            'page_title' => trans('language.fees_counter'),
            'redirect_url' => url('admin-panel/hostel/fees-counter'),
            'login_info' => $loginInfo,
            'students' => $student_array,
            'save_url' => $save_url,
        );
        return view('admin-panel.hostel-fees-counter.fee_counter')->with($data);
    }

    public function studentFeesDetails($student_id,$h_student_map_id){
        $headPrices = [];
        $session       = get_current_session();
        $mapping_info = HostelStudentMap::where(function($query) use ($student_id,$h_student_map_id)
        {
            $query->where('h_student_map_id','=', $h_student_map_id);
            $query->where('student_id','=', $student_id);
        })
        ->with('getStudent','getRoom','getRoom.getRoomCate','getStudent.getStudentAcademic')
        ->get();
        $mapping_info = isset($mapping_info[0]) ? $mapping_info[0] : [];
        $finalFees = $this->hostelHeadPrices($mapping_info);
        return $finalFees;
    }

    public function hostelHeadPrices($mapping_info){
        $feesNames = [];
        $finalPriceArr = [];
        $ids = explode(',',$mapping_info->h_fees_head_ids);
        $headPrices = HostelFeesHead::where(function($query) use ($ids)
        {
            $query->whereIn('h_fees_head_id', $ids);
            $query->where('h_fees_head_status',1 );
        })
        ->get();
        $base_price = 0;
        if(isset($mapping_info['getRoom']['getRoomCate'])){
            $feesNames[] = 'Base Price';
            $base_price = $mapping_info['getRoom']['getRoomCate']->h_room_cate_fees;
        }
        foreach ($headPrices as $key => $prices) {
            $feesNames[] = $prices->h_fees_head_name;
            $base_price += $prices->h_fees_head_price;
        }

        $start    = (new DateTime($mapping_info->join_date));
        $end      = (new DateTime($mapping_info->leave_date));
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        $arr_heads = [];
        $currentDate = date('Y-m-d');
        $session    = get_current_session();
        $class_id = $mapping_info['getStudent']['getStudentAcademic']->current_class_id;
      
        foreach ($period as $dt) {
            // p($mapping_info);
            $map_receipt = HostelFeeReceiptDetail::
            where('month_year', '=',$dt->format("Y-m"))
            ->where('hostel_fee_details.cancel_status', '=','0')
            // ->where('month_date', '=',$dt->format("Y-m-d"))
            ->join('hostel_fees', function($join) use($session,$class_id,$mapping_info) {
                $join->on('hostel_fees.hostel_fee_id', '=', 'hostel_fee_details.hostel_fee_id');
                $join->where('h_fee_session_id', '=',$session['session_id']);
                $join->where('h_fee_class_id', '=',$class_id);
                $join->where('h_fee_student_id', '=',$mapping_info->student_id);
                $join->where('h_fee_student_map_id', '=',$mapping_info->h_student_map_id);
                $join->where('hostel_fees.cancel_status', '=',0);
            })
            ->orderBy('hostel_fee_detail_id', 'DESC')->get()->toArray();
            
            $paid_fee_amount = $fee_pay_amount = $total_delay_days = $feesExpire1 = 0;
            $fee_pay_amount = $base_price;
            if($currentDate > date("Y-m-t", strtotime($dt->format("F-m-d")))){
                $feesExpire = 1;
                $datediff = strtotime($currentDate) - strtotime(date("Y-m-t", strtotime($dt->format("F-m-d"))));
                $total_delay_days =  round($datediff / (60 * 60 * 24));
            }
            if(!empty($map_receipt)){
                foreach ($map_receipt as $receiptValues) {
                    if($receiptValues['paid_fee_amount'] != "" && $receiptValues['month_year'] == $dt->format("Y-m")){
                        $paid_fee_amount += $receiptValues['paid_fee_amount'];
                    }
                }
            }
            if($paid_fee_amount != $fee_pay_amount){
                $fee_pay_amount  = $fee_pay_amount - $paid_fee_amount;
                $counter = COUNT($arr_heads);
                $headData['counter'] = $counter;
                $headData['month_year'] = $dt->format("Y-m");
                $headData['month_year_string'] = $dt->format("Y - F");
                $headData['head_name'] = implode(',',$feesNames);
                $headData['fee_amount'] = $base_price;
                $headData['paid_fee_amount'] = $paid_fee_amount;
                $headData['fee_pay_amount'] = $fee_pay_amount;
                $headData['month_date'] = $dt->format("Y-m-t");
                $headData['fee_expire'] = $feesExpire1;
                $headData['class_id'] = $class_id;
                $headData['student_id'] = $student_id;
                $headData['total_delay_days'] = $total_delay_days;
                $headData['editable'] = 1;
                $arr_heads[] = $headData;
            }
            
            
        }
        return $arr_heads;
    }

    public function save_fees_data(Request $request, $id = null)
    {
        $receipt = New HostelFeeReceipt();
        $session = get_current_session();
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        // p($request->all());
        if(!empty($request->get('student_id'))) {
            $student_id = $request->get('student_id');
            $student_info = get_student_signle_data($student_id);
            $student_info = isset($student_info[0]) ? $student_info[0] : [];
            DB::beginTransaction(); //Start transaction!
            try
            {
                $fine_amt = $request->get('fine_amt');
                $concession_amt = $request->get('conc_amt');
                $pay_lator_amt = $request->get('pay_lator_amt_hide');
                
                $bank_id = null;
                $total_amount = $net_amount = 0;
                $net_amount = $request->get('pay_final_calculate_amt');
                $total_amount = $request->get('pay_final_amt'); 

                $receipt->admin_id              = $admin_id;
                $receipt->update_by             = $loginInfo['admin_id'];
                $receipt->h_fee_session_id      = $student_info['current_session_id'];
                $receipt->h_fee_class_id        = $student_info['current_class_id'];
                $receipt->h_fee_student_id      = $student_id;
                $receipt->h_fee_student_map_id  = $request->get('h_student_map_id');
                $receipt->receipt_date          = $request->get('select_date');
                $receipt->net_amount            = $net_amount;
                $receipt->total_paid_amount     = $total_amount;
                $receipt->pay_later_amount      = $pay_lator_amt;
                $receipt->concession_amount     = $concession_amt;
                $receipt->fine_amount           = $fine_amt;
                $receipt->bank_id               = $bank_id;
                $receipt->pay_mode              = $request->get('pay_mode');
                $receipt->cheque_date           = $request->get('cheque_date');
                $receipt->cheque_number         = $request->get('cheque_number');
                $receipt->cheque_amt            = $request->get('cheque_amt');
                $receipt->transaction_id        = $request->get('transaction_id');
                $receipt->transaction_date      = $request->get('transaction_date');
                $receipt->description           = $request->get('description');
                // p($receipt);
                $receipt->save();
                
                $receipt->receipt_number                = $receipt->hostel_fee_id;
                $receipt->save();

                if($request->get('pay_mode') != null && $request->get('pay_mode') == 'cheque'){
                    $bank_id = $request->get('bank_id');
                    $cheque   = New STCheques();
                    $cheque->admin_id               = $admin_id;
                    $cheque->update_by              = $loginInfo['admin_id'];
                    $cheque->student_id             = $student_id;
                    $cheque->bank_id                = $bank_id;
                    $cheque->cheques_session_id     = $session['session_id'];
                    $cheque->cheques_class_id       = $student_info['current_class_id'];
                    $cheque->cheques_receipt_id     = $receipt->hostel_fee_id;
                    $cheque->student_cheque_no      = $request->get('cheque_number');
                    $cheque->student_cheque_date    = $request->get('cheque_date');
                    $cheque->student_cheque_amt     = $request->get('cheque_amt');
                    $cheque->cheques_for            = 1;
                    $cheque->save();
                }
                $headCounter = 0;
                foreach($request->get('pay') as $feesHeads){
                    if(isset($feesHeads['month_year'])){
                        $headCounter++;
                        $inst_status = 0;

                        $final_amt = $feesHeads['pay_amt'];
                        $actual_amt = $feesHeads['pay_amt'];
                        if($pay_lator_amt != 0 && $pay_lator_amt != ''){
                            if($pay_lator_amt >= $final_amt){
                                $remainingAmt = $pay_lator_amt - $final_amt;
                                $feesHeads['pay_amt'] = 0;
                                $pay_lator_amt = $remainingAmt;
                            } else if($pay_lator_amt < $final_amt){
                                $feesHeads['pay_amt'] = $final_amt - $pay_lator_amt;
                                $pay_lator_amt = 0;
                            }
                        }
                        if($final_amt == $feesHeads['pay_amt']){
                            $inst_status = 1;
                        } else {
                            $inst_status = 0;
                        }
                        $receipt_detail = new HostelFeeReceiptDetail();
                        $receipt_detail->admin_id           = $admin_id;
                        $receipt_detail->update_by          = $loginInfo['admin_id'];
                        $receipt_detail->hostel_fee_id      = $receipt->hostel_fee_id;
                        $receipt_detail->month_year         = $feesHeads['month_year'];
                        $receipt_detail->fee_amount         = $actual_amt;
                        $receipt_detail->month_year         = $feesHeads['month_year'];
                        $receipt_detail->paid_fee_amount    = $feesHeads['pay_amt'];
                        $receipt_detail->amount_status        = $inst_status;
                        // p($receipt_detail);
                        $receipt_detail->save();
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        } else {
            $error_message = "student not found!";
            return redirect()->back()->withErrors($error_message);
        }
        $success_msg = "Fees successfully paid !!";
        return redirect()->back()->withSuccess($success_msg);
    }  

}
