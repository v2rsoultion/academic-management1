<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\HomeworkGroup\HomeworkGroup; // Model
use App\Model\HomeworkConv\HomeworkConv; // Model
use Yajra\Datatables\Datatables;


class HomeWorkController extends Controller
{
    /**
     *  Get My Home Work
     *  @Sandeep on 9 Jan 2019
    **/
    public function student_view_homework(Request $request)
    {
        $loginInfo              = get_loggedin_user_data();
        $session                = get_current_session();
        $arr_teacher            = get_all_student_staffs($loginInfo['class_id'],$loginInfo['section_id']);
        $arr_subject            = get_subject_list_class_section($loginInfo['class_id'],$loginInfo['section_id']);

        $listData['arr_teacher']    = add_blank_option($arr_teacher, 'Select Teacher');
        $listData['arr_subject']    = add_blank_option($arr_subject, 'Select Subject');

        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/student/view-homework'),
            'page_title'    => trans('language.student_homework'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.homework.student_homework')->with($data);
    }



    public function student_homework_data(Request $request){
        // p($request->all());
        $homework = HomeworkGroup::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != NULL)
            {
                $query->where('class_id',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != NULL)
            {
                $query->where('section_id',$request->get('section_id'));
            }
             if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != NULL)
            {
                $query->where('section_id',$request->get('section_id'));
            }
        })
        ->join('homework_conversations', function($join) {
            $join->on('homework_conversations.homework_group_id', '=', 'homework_groups.homework_group_id');
        })->with('getStaff','getSubject')
        ->select('homework_groups.class_id','homework_groups.section_id','homework_groups.homework_group_name','homework_conversations.*')
        ->get();
        // p($homework);
        return Datatables::of($homework) 
            ->addColumn('subject_name', function ($homework)
            {
                $subject_name = $homework['getSubject']->subject_name;
                return $subject_name;
            })
            ->addColumn('staff_name', function ($homework)
            {
                $staff_name = $homework['getStaff']->staff_name;
                return $staff_name;
            })
            ->addColumn('homework', function ($homework)
            {
                $homework = '<button class="btn btn-raised btn-primary homework" h-conversation-id='.$homework->h_conversation_id.'>Homework</button>';
                return $homework;
            })
            ->addColumn('homework_date', function ($homework)
            {
                $homework_date =  date("d m Y", strtotime($homework->created_at));
                return $homework_date;
            })
            
            ->rawColumns(['subject_name'=>'subject_name', 'staff_name'=> 'staff_name','homework'=> 'homework','homework_date'=> 'homework_date'])->addIndexColumn()->make(true);
    }

    public function get_homework_info(Request $request){
       
        $info = [];
        $homeworkInfo = HomeworkConv::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('h_conversation_id')) && $request->get('h_conversation_id') != NULL)
            {
                $query->where('h_conversation_id',$request->get('h_conversation_id'));
            }
        })
        ->with('getGroup','getGroup.getClass','getStaff','getSubject') 
        ->with('getGroup.getSection')
        ->first();
        if(!empty($homeworkInfo)) {
            $info = array(
                'subject_name' => $homeworkInfo['getSubject']->subject_name.' - '.$homeworkInfo['getSubject']->subject_code,
                'staff_name' => $homeworkInfo['getStaff']->staff_name,
                'class_name' => $homeworkInfo['getGroup']['getClass']->class_name,
                'section_name' => $homeworkInfo['getGroup']['getSection']->section_name,
                'h_conversation_text' => $homeworkInfo->h_conversation_text,
                'date' => date("d M Y",strtotime($homeworkInfo->created_at)),
            );
        }
        return $info;
    }




    /**
     *  Get My Homework
     *  @Sandeep on 9 Jan 2019
    **/
    public function student_homework_data_old(Request $request)
    {
        $homework   = [];
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();

        $homework   = HomeworkGroup::where(function($query) use ($request,$session,$loginInfo) 
        {
        //    $query->where('session_id', "=", $session['session_id']);
            if (!empty($loginInfo['class_id']) && $loginInfo['class_id'] != null){
                $query->where('class_id', '=',$loginInfo['class_id']);
            }
            if (!empty($loginInfo['section_id']) && $loginInfo['section_id'] != null){
                $query->where('section_id', '=',$loginInfo['section_id']);
            }
            // if (!empty($loginInfo['student_id']) && $loginInfo['student_id'] != null){
            //     $query->where('remarks.student_id', '=',$loginInfo['student_id']);
            // }

            if (!empty($request) && !empty($request->get('staff_id')) && $request->get('staff_id') != null){
                $query->where('staff_id', '=',$request->get('staff_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }

            
        })->join('students', function($join) use ($request){
          //  $join->on('students.student_id', '=', 'remarks.student_id');
        })->orderBy('homework_group_id', 'DESC')
        ->with('getClass')->with('getSection')->with('getStaff')->with('getSubject')->get()->toArray();

        return Datatables::of($remark)
            ->addColumn('remark_date', function ($remark)
            {
                $remark_date = date("d F Y", strtotime($remark['remark_date']));
                return $remark_date;
            })
            ->addColumn('subject_name', function ($remark)
            {
                $subjectName = $remark['get_subject']['subject_name'].' - '.$remark['get_subject']['subject_code'];
                return $subjectName;  
            })
            ->addColumn('teacher_name', function ($remark)
            {
                $staffName = $remark['get_staff']['staff_name'];
                return $staffName;  
            })
            ->addColumn('remark_description', function ($remark)
            {
                return '<button type="button" class="btn btn-raised btn-primary remarks"  remark-id="'.$remark['remark_id'].'" >
                Remark
                </button>';
            })
            ->rawColumns(['remark_description'=>'remark_description'])->addIndexColumn()->make(true);
    }


    /**
     *  Get remark student single data
     *  @Shree on 9 Jan 2019
    **/
    public function remark_student_single_data(Request $request)
    {
        $remark     = [];
        $session       = get_current_session();
        $remark     = Remarks::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('remark_id')) && $request->get('remark_id') != null){
                $query->where('remark_id', '=',$request->get('remark_id'));
            }
        })->join('students', function($join) use ($request){
            $join->on('students.student_id', '=', 'remarks.student_id');
        })->orderBy('remark_id', 'DESC')->with('getSubject')->get()->toArray();
        $remark = isset($remark[0]) ? $remark[0] : [];
        $data      = view('admin-panel.remarks.remark_info_student',compact('remark','remark'))->render();
        return $data;
        
    }



}
