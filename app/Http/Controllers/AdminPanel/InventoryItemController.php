<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InventoryCategory\Category; // Model
use App\Model\InventoryUnit\Unit; // Model
use App\Model\InventoryItem\Items; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class InventoryItemController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
	 *  Add Page of Item
     *  @Khushbu on 22 Dec 2018
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$items 	= $arr_unit_data = $arr_subcategory_data = [];
        $loginInfo               = get_loggedin_user_data();

	 	if(!empty($id))
	 	{
	 		$decrypted_item_id 		= get_decrypted_value($id, true);
        	$items      			= Items::Find($decrypted_item_id);
            $page_title             = trans('language.edit_item');
        	$arr_subcategory_data   = get_sub_categories($items['category_id']);

        	$save_url    			= url('admin-panel/inventory/manage-items-save/'. $id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
            $page_title                 = trans('language.add_item');
	 		$save_url    				= url('admin-panel/inventory/manage-items-save');
	 		$submit_button  			= 'Save';
	 	}
        $getUnitData                    = get_all_units();
	 	$items['arr_unit_data'] 		= add_blank_option($getUnitData,'Select Measuring Unit');
        $get_all_categories     		= get_all_categories(1);
	 	$items['arr_category']  		= add_blank_option($get_all_categories, 'Select Category');
	 	$items['arr_subcategory_data']  = add_blank_option($arr_subcategory_data, 'Select Sub Category');

        $data                   	=  array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'items'		    		=> $items,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.inventory-item.add')->with($data);
    }
    /**
     *	Add & Update of Item
     *  @Khushbu on 22 Dec 2018
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      		= get_loggedin_user_data();
        $decrypted_item_id		= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $items        		= Items::Find($decrypted_item_id);
            if(!$items) {
                return redirect('admin-panel/inventory/manage-items')->withError('Item not found!');
            }
            $admin_id    		= $items->admin_id;
            $success_msg 		= 'Item updated successfully!';
        }
        else
        {
            $items    	 		= New Items;
            $success_msg 		= 'Item saved successfully!';
        }

        $category_id = null;
        if ($request->has('category_id'))
        {
            $category_id = Input::get('category_id');
        }

        $validator 				=  Validator::make($request->all(), [
        	'category_id'		=> 'required',
        	'sub_category_id'	=> 'required',
    		'item_name'  		=> 'required|unique:inv_items,item_name,' . $decrypted_item_id . ',item_id,category_id,' . $category_id,
    		'unit_id'			=> 'required'
    	]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $items->admin_id         	= $admin_id;
                $items->update_by       	= $loginInfo['admin_id'];
                $items->category_id  		= Input::get('category_id');
                $items->sub_category_id    	= Input::get('sub_category_id');
                $items->item_name    		= Input::get('item_name');
                $items->unit_id    			= Input::get('unit_id');
                $items->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/manage-items')->withSuccess($success_msg);
    }
    /**
     *	Get Item's Data fo view page
     *  @Khushbu on 29 Dec 2018
    **/
    public function anyData(Request $request)
    {

    	$loginInfo 			 = get_loggedin_user_data();
    	$getUnitsData		 = [];
    	$items 				 = Items::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('search_category') !=  NULL)
            {
                $query->where('category_id', '=', $request->get('search_category'));
            }
           if (!empty($request) && $request->get('s_item_name') !=  NULL)
            {
                $query->where('item_name', 'Like', $request->get('s_item_name').'%');
            }
        })->orderBy('item_id','DESC')->with('category', 'unit', 'getSubCategory')->get();

    	return Datatables::of($items)
        ->addColumn('category_id', function($items) {
            return $items['category']->category_name;
        })
        ->addColumn('sub_category_id', function($items) {
            return $items['getSubCategory']->category_name;
        })
        ->addColumn('unit_id', function($items){
            return $items['unit']->unit_name;
        })
    	->addColumn('action', function($items) use($request) {
    		$encrypted_item_id  = get_encrypted_value($items->item_id, true);
            
    		if($items->item_status == 0) {

                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/inventory/manage-items-status/'.$status .'/'.$encrypted_item_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/inventory/manage-items/'.$encrypted_item_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/delete-manage-items/' . $encrypted_item_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/inventory/manage-items');
    } 

    /** 
     *  Change Status of Item
     *  @Khushbu on 22 Dec 2018
    **/
    public function changeStatus($status,$id) 
    {
        $item_id        = get_decrypted_value($id, true);
        $items          = Items::find($item_id);
        if ($items)
        {
            $items->item_status  = $status;
            $items->save();
            $success_msg = "Item status update successfully!";
            return redirect('admin-panel/inventory/manage-items')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Item not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Data of Item
     *  @Khushbu on 22 Dec 2018
    **/
    public function destroy($id) {
        $item_id        = get_decrypted_value($id, true);
        $items          = Items::find($item_id);

        if ($items)
        {
            DB::beginTransaction();
            try
            {
                $items->delete();
                $success_msg = "Item deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Item not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get items data according to category
     *  @Khushbu on 25 Jan 2019.
    **/
    public function getItemsData($sub_category_id=null)
    {
        $sub_category_id    = Input::get('sub_category_id');
        $items              = get_all_items($sub_category_id);
        $data               = view('admin-panel.inventory-item.ajax-item-select',compact('items'))->render();
        return response()->json(['options'=>$data]);
    }
}
