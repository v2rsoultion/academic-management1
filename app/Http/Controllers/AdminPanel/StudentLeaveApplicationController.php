<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use DateTime;
use App\Model\StudentLeaveApplication\StudentLeaveApplication; // Model
use App\Model\Staff\Staff; // Model
use App\Model\Student\Student; // Model
use Yajra\Datatables\Datatables;
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model

class StudentLeaveApplicationController extends Controller
{
    /**
     *  View page for Title
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $parent_info        = parent_info($loginInfo['admin_id']);
        $parent_student     = get_parent_student($parent_info['student_parent_id']);

        $data = array(
            'page_title'        => trans('language.view_leave_application'),
            'redirect_url'      => url('admin-panel/student-leave-application/view-leave-application'),
            'login_info'        => $loginInfo,
            'parent_student'    => $parent_student,
        );
        return view('admin-panel.student-leave-application.index')->with($data);
    }

    /**
     *  Add page for Student Leave Application
     *  @Pratyush on 10 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    			= [];
        $leave_application 	= [];
        $loginInfo 		    = get_loggedin_user_data();
        $parent_info        = parent_info($loginInfo['admin_id']);
        $parent_student     = get_parent_student($parent_info['student_parent_id']);

        if (!empty($id))
        {
            $decrypted_student_leave_id = get_decrypted_value($id, true);
            $leave_application			= StudentLeaveApplication::where('student_id',$loginInfo['student_id'])->Find($decrypted_student_leave_id);
            if (!$leave_application)
            {
                return redirect('admin-panel/student-leave-application/add-leave-application')->withError('Leave Application not found!');
            }
            $page_title             	= trans('language.edit_leave_application');
            $encrypted_student_leave_id   		= get_encrypted_value($leave_application->student_leave_id, true);
            $save_url               	= url('admin-panel/student-leave-application/save/' . $encrypted_student_leave_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_leave_application');
            $save_url      = url('admin-panel/student-leave-application/save');
            $submit_button = 'Save';
        }
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'leave_application' => $leave_application,
            'parent_student'    => $parent_student,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/student-leave-application/view-leave-application'),
        );
        return view('admin-panel.student-leave-application.add')->with($data);
    }

    /**
     *  Add and update Leave Application's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_student_leave_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];


        if (!empty($id))
        {
            $leave_application = StudentLeaveApplication::find($decrypted_student_leave_id);
            $admin_id = $leave_application['admin_id'];
            if (!$leave_application)
            {
                return redirect('/admin-panel/student-leave-application/add-leave-application/')->withError('Leave Application not found!');
            }
            $success_msg = 'Leave Application updated successfully!';
        }
        else
        {
            $leave_application  = New StudentLeaveApplication;
            $success_msg 		= 'Leave Application saved successfully!';
        }
        $student_id = $loginInfo['student_id'];
        $validatior = Validator::make($request->all(), [
                'student_leave_reason'   	=> 'required',
                'student_leave_from_date' => 'required|unique:student_leaves,student_leave_from_date,' . $decrypted_student_leave_id . ',student_leave_id,student_id,' . $student_id,
                'student_leave_to_date'   	=> 'required',
                'documents'   				=> 'mimes:pdf,jpg,jpeg,png',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $leave_application->admin_id      			   = $loginInfo['admin_id'];
                $leave_application->update_by      			   = $loginInfo['admin_id'];
                $leave_application->student_id     			   = $loginInfo['student_id'];
                $leave_application->student_leave_reason 	   = Input::get('student_leave_reason');
                $leave_application->student_leave_from_date    = Input::get('student_leave_from_date');
                $leave_application->student_leave_to_date 	   = Input::get('student_leave_to_date');
                if ($request->hasFile('documents'))
		        {
                    if (!empty($id)){
                        $attachment = check_file_exist($leave_application->student_leave_attachment, 'student_leave_document_file');
                        if (!empty($attachment))
                        {
                            unlink($attachment);
                        } 
                    }
		            $file                          = $request->File('documents');
		            $config_document_upload_path   = \Config::get('custom.student_leave_document_file');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
		            $name                          = substr($file->getClientOriginalName(),0,-4);
		            $filename                      = $name.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
		            $leave_application->student_leave_attachment = $filename;
		        }
                $leave_application->save();

                $student_name = get_student_name($loginInfo['student_id']);
                $send_notification  = $this->send_push_notification($leave_application->student_leave_id,"student_leave_by_parent",$student_name,$loginInfo['student_id'],'student_leave_by_parent');
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/student-leave-application/view-leave-application')->withSuccess($success_msg);
    }

    /**
     *  Get Title's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData()
    {
        $loginInfo 				= get_loggedin_user_data();
        if($loginInfo['admin_type'] == 4){
            $leave_application      = StudentLeaveApplication::where('student_id',$loginInfo['student_id'])->orderBy('student_leave_id', 'ASC')->get();    
        }else if($loginInfo['admin_type'] == 3){
            $leave_application      = StudentLeaveApplication::where('student_id',$loginInfo['student_id'])->orderBy('student_leave_id', 'ASC')->get();    
        }else{
            $leave_application      = StudentLeaveApplication::orderBy('student_leave_id', 'ASC')->get();    
        }
        return Datatables::of($leave_application)
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->student_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->student_leave_to_date));
            
        })
        ->addColumn('leave_days', function ($leave_application)
        {	
            $datetime1 = new DateTime($leave_application->student_leave_from_date);
            $datetime2 = new DateTime($leave_application->student_leave_to_date);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');//now do whatever you like with $days
            return $days.' Days';
            
        })
        ->addColumn('student_leave_status', function ($leave_application)
        {	
            $status = '';
            if($leave_application->student_leave_status == 0){
                $status = 'Pending';
            }elseif($leave_application->student_leave_status == 1){
                $status = 'Approved';
            }elseif($leave_application->student_leave_status == 2){
                $status = 'Disapproved';
            }
            return $status;
            
        })
        ->addColumn('student_leave_attachment', function ($leave_application)
        {	
            $attachment_link = '';
            if($leave_application->student_leave_attachment == ''){
                $attachment_link = 'Attachment not available';
            }else{
                $attachment_link = check_file_exist($leave_application['student_leave_attachment'], 'student_leave_document_file');
                $attachment_link = '<span class="profile_detail_span_6"><a href="'.url($attachment_link).'" target="_blank">View Attachment</a></span>';
            }
            return $attachment_link;
            
        })
        ->addColumn('action', function ($leave_application)
        {
            if($leave_application->student_leave_status == 0) {
                $encrypted_student_leave_id = get_encrypted_value($leave_application->student_leave_id, true);
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-leave-application/' . $encrypted_student_leave_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete/' . $encrypted_student_leave_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            } else {
                return '-----';
            }
            
        })
        ->rawColumns(['action' => 'action','student_leave_attachment' => 'student_leave_attachment'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Delete Leave Application's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function destroy($id)
    {
        $student_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StudentLeaveApplication::find($student_leave_id);
        if ($leave_application)
        {
            $leave_application->delete();
            $success_msg = "Leave Application deleted successfully!";
            return redirect('admin-panel/student-leave-application/view-leave-application')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $student_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StudentLeaveApplication::find($student_leave_id);
        if ($leave_application)
        {
            $leave_application->student_leave_status  = $status;
            $leave_application->save();
            $success_msg = "Leave Application status updated!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Leave application
     *  @Shree on 21 Oct 2018
    **/
    public function manage()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_leave_application'),
            'redirect_url'  => url('admin-panel/student-leave-application/manage-leave-application'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.student-leave-application.manage')->with($data);
    }

    /**
     *  Manage leaves's Data for manage page(Datatables)
     *  @Shree on 21 Oct 2018.
    **/
    public function manage_leaves()
    {
        $loginInfo 				= get_loggedin_user_data();
        $leave_application      = StudentLeaveApplication::orderBy('student_leave_id', 'DESC')->get();
        
        return Datatables::of($leave_application)
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->student_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->student_leave_to_date));
            
        })
        ->addColumn('leave_days', function ($leave_application)
        {	
            $datetime1 = new DateTime($leave_application->student_leave_from_date);
            $datetime2 = new DateTime($leave_application->student_leave_to_date);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');//now do whatever you like with $days
            return $days.' Days';
            
        })
        ->addColumn('student_name', function ($leave_application)
        {	
            return get_student_name($leave_application->student_id);
            
        })
        ->addColumn('student_leave_status', function ($leave_application)
        {	
            $status = '';
            if($leave_application->student_leave_status == 0){
                $status = 'Pending';
            }elseif($leave_application->student_leave_status == 1){
                $status = 'Approved';
            }elseif($leave_application->student_leave_status == 2){
                $status = 'Disapproved';
            }
            return $status;
            
        })
        ->addColumn('student_leave_attachment', function ($leave_application)
        {	
            $attachment_link = '';
            if($leave_application->student_leave_attachment == ''){
                $attachment_link = 'Attachment not available';
            }else{
                $attachment_link = check_file_exist($leave_application['student_leave_attachment'], 'student_leave_document_file');
                $attachment_link = '<span class="profile_detail_span_6"><a href="'.url($attachment_link).'" target="_blank">View Attachment</a></span>';
            }
            return $attachment_link;
            
        })
        ->addColumn('action', function ($leave_application)
        {
            $encrypted_student_leave_id = get_encrypted_value($leave_application->student_leave_id, true);
            return '
            <div class="dropdown">
                <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a onclick="return confirm('."'Are you sure?'".')" href="student-leave-application-status/0/' . $encrypted_student_leave_id . '">Pending</a>
                    </li>
                    <li>
                        <a onclick="return confirm('."'Are you sure?'".')" href="student-leave-application-status/1/' . $encrypted_student_leave_id . '">Approved</a>
                    </li>
                    <li>
                        <a onclick="return confirm('."'Are you sure?'".')" href="student-leave-application-status/2/' . $encrypted_student_leave_id . '">Disapproved</a>
                    </li>
                </ul>
            </div>';
            
        })
        ->rawColumns(['action' => 'action','student_name' => 'student_name','student_leave_attachment' => 'student_leave_attachment'])->addIndexColumn()
        ->make(true);
    }


    /**
     *  View page for Student Leave
     *  @Sandeep on 18 Jan 2019
    **/
    public function view_student_leaves()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_leave_application'),
            'redirect_url'  => url('admin-panel/student-leave-application/view-leave-application'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.student-leave-application.student_leaves')->with($data);
    }

    public function send_push_notification($module_id,$module_name,$student_name,$student_id,$notification_type){
        $device_ids = [];
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            if (!empty($student_id) && !empty($student_id) && $student_id != null){
                $query->where('students.student_id', $student_id);
            }
            $query->where('students.student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($class_id,$section_id){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('students.current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('students.current_class_id', '=',$class_id);
            }
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('students.student_id','students.student_name','students.student_parent_id','students.reference_admin_id','student_academic_info.current_section_id','student_academic_info.current_class_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];

        $staff_info = ClassTeacherAllocation::where('class_id', $student_info['current_class_id'])->where('section_id',$student_info['current_section_id'])->with('getStaff')->with('getStaff.getStaffAdminInfo')->get();
        $staff_info = isset($staff_info[0]) ? $staff_info[0] : [];
        if($staff_info['getStaff']['getStaffAdminInfo']->fcm_device_id != "" && $staff_info['getStaff']['getStaffAdminInfo']->notification_status == 1){
            $device_ids[] = $staff_info['getStaff']['getStaffAdminInfo']->fcm_device_id;
            $staff_admin_id = $staff_info['getStaff']['getStaffAdminInfo']->admin_id;
        }

        $message = $student_name." applied for leave";
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,$staff_id,'','',$device_ids,$message);
        //p($send_notification);
    }


}
