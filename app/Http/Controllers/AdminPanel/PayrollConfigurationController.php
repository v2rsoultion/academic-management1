<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Configuration; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollConfigurationController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add Page of Configuration
     *  @Khushbu on 06 Feb 2019
     */
    public function add(Request $request) {
        $pay_config     = []; 
        $loginInfo      = get_loggedin_user_data();
        $page_title     = trans('language.configuration');
        $save_url   	= url('admin-panel/payroll/configuration-save');
        $submit_button  = 'Update';
        $data           = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url   
        );
        return view('admin-panel.payroll-configuration.add')->with($data);
    }

    /**
     *  Get Configuration's Data fo view page
     *  @Khushbu on 06 Feb 2019
     */
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$pay_config 		= Configuration::orderBy('pay_config_id')->get();
        // p($pay_config);
        return Datatables::of($pay_config)
        ->addColumn('pay_config_status', function($pay_config) {
            $check1 = $check0 = '';
            if($pay_config['pay_config_status'] == 1)
            {
                $check1='checked';
            } else 
            {
                $check0='checked';
            }
            return '
                <input type="hidden" name="pay_config['.$pay_config->pay_config_id.'][pay_config_id]" value="'.$pay_config->pay_config_id.'" />
                <div class="radio">
                    <input id="radio1'.$pay_config->pay_config_id.'" name="pay_config['.$pay_config->pay_config_id.'][value]" '.$check1.' type="radio" value="1">
                    <label for="radio1'.$pay_config->pay_config_id.'" class="document_staff" style="padding-top:3px!important;">Yes</label>
                    <input id="radio2'.$pay_config->pay_config_id.'" name="pay_config['.$pay_config->pay_config_id.'][value]" '.$check0.' type="radio" value="0">
                    <label for="radio2'.$pay_config->pay_config_id.'" class="document_staff" style="padding-top:3px!important;">No</label>
                </div>';
        })
    	->rawColumns(['pay_config_status' => 'pay_config_status'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/configuration');
    } 

    /**
     *  Add & Update page of configuration
     *  @Khushbu on 07 Feb 2019
     */
    public function save(Request $request, $id= NULL) {
        $loginInfo      = get_loggedin_user_data();
        $admin_id       = $loginInfo['admin_id'];
        if (!$request->get('pay_config'))
        {
            return redirect('/admin-panel/payroll/configuration')->withError('Configuration not found!');
        }
        $success_msg = 'Configuration updated successfully!';

        DB::beginTransaction();
        try
        { 
            foreach($request->get('pay_config') as $value) {
                $pay_config = Configuration::where('pay_config_id', $value['pay_config_id'])->first();
                $pay_config->pay_config_status =  $value['value'];
                $pay_config->save();
            }
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/payroll/configuration')->withSuccess($success_msg);
    }

}
