<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InvoiceConfig\InvoiceConfig; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class InventoryConfigurationController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
	 *  Add Page of Invoice Configuration
     *  @Khushbu on 23 Jan 2019
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$configuration 			= []; 
	 	$loginInfo           	= get_loggedin_user_data();
        $page_title             = trans('language.manage_invoice_config');
	 	if(!empty($id))
	 	{
	 		$decrypted_configuration_id = get_decrypted_value($id, true);
        	$configuration      		= InvoiceConfig::Find($decrypted_configuration_id);
            $page_title             	= trans('language.edit_invoice_config');
        	$save_url    				= url('admin-panel/inventory/manage-invoice-configuration-save/'. $id);
        	$submit_button  			= 'Update';
	 	}
	 	$arr_invoice_type				= \Config::get('custom.invoice_type');
	 	$configuration['arr_invoice_type']  = add_blank_option($arr_invoice_type,'Select Invoice Type');
	 	$data                   	=  array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'configuration'		    => $configuration,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.inventory-configuration.add')->with($data);
    }

    /** 
     *	Get Data for view page(Datatables)
     *  @Khushbu on 24 Jan 2019
    **/ 
    public function anyData(Request $request) {
    	$loginInfo     = get_loggedin_user_data();
    	$configuration = InvoiceConfig::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('s_invoice_type')))
            {
                $query->where('invoice_type', $request->get('s_invoice_type'));
            }
        })->orderBy('invoice_config_id')->get();
        $arr_invoice_type       = \Config::get('custom.invoice_type');
        return Datatables::of($configuration,$arr_invoice_type)
        ->addColumn('invoice_type', function ($configuration) use($arr_invoice_type)
        {
            return $arr_invoice_type[$configuration->invoice_type];
        })
        ->addColumn('invoice_format', function ($configuration)
        {
            $invoice_format = $configuration['invoice_prefix'];
        	if($configuration['year'] == 1 ? $invoice_format .= date('Y') : $invoice_format)
            if($configuration['month'] == 1 ? $invoice_format .= date('m') : $invoice_format)
            if($configuration['day'] == 1 ? $invoice_format .= date('d') : $invoice_format) 
            $invoice_format .= $configuration['invoice_config_id']; 
            return $invoice_format;   
        })
        ->addColumn('action', function ($configuration) use($request)
        {
            $encrypted_configuration_id = get_encrypted_value($configuration->invoice_config_id, true);
            return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/inventory/manage-invoice-configuration/' . $encrypted_configuration_id. ' ').'"> <i class="zmdi zmdi-edit"></i></a></div>';
        })->rawColumns(['action' => 'action', 'invoice_type' => 'invoice_type', 'invoice_format' => 'invoice_format'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Update Invoice Configuration's data
     *  @Khushbu on 24 Jan 2019.
    **/
    public function save(Request $request, $id) {
        $loginInfo                    = get_loggedin_user_data();
        $decrypted_configuration_id   = get_decrypted_value($id, true);
        $admin_id                     = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $configuration = InvoiceConfig::find($decrypted_configuration_id);
            $admin_id = $configuration['admin_id'];
           
            if (!$configuration)
            {
                return redirect('/admin-panel/manage-invoice-configuration/')->withError('Invoice Configuration not found!');
            }
            $success_msg = 'Invoice Configuration updated successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'invoice_prefix'   => 'required|unique:invoice_config,invoice_prefix,' . $decrypted_configuration_id . ',invoice_config_id',
        ]);

        if($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $day    = Input::get('day') == 1 ? 1 : 0;
                $month  = Input::get('month') == 1 ? 1 : 0;
                $year   = Input::get('year') == 1 ? 1 : 0;
                $configuration->admin_id          = $admin_id;
                $configuration->update_by         = $loginInfo['admin_id'];
                $configuration->invoice_prefix    = Input::get('invoice_prefix');
                $configuration->day               = $day;
                $configuration->month             = $month;
                $configuration->year              = $year;
                $configuration->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/manage-invoice-configuration')->withSuccess($success_msg);
    }
     
}
