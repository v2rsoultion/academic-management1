<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Advance; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollAdvanceController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Advance
     * @Khushbu on 20 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $advance = $arr_staff = []; 
        $staff       = Staff::get()->toArray();
        foreach($staff as $key => $value) {
            $arr_staff[$value['staff_id']] = $value['staff_name'];
        }
        $advance['staff']  = add_blank_option($arr_staff, 'Select Employee');
        $loginInfo         = get_loggedin_user_data();
        if(!empty($id)) {
	 		$decrypted_advance_id 	= get_decrypted_value($id, true);
            $advance      		    = Advance::Find($decrypted_advance_id);
            $advance['staff']       = $arr_staff;
            $page_title             = trans('language.edit_advance');
        	$save_url    			= url('admin-panel/payroll/manage-advance-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title             = trans('language.add_advance');
	 		$save_url    			= url('admin-panel/payroll/manage-advance-save');
	 		$submit_button  		= 'Save';
        }
        $advance['search_staff']  = add_blank_option($arr_staff, 'Select Employee');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'advance'         => $advance,  
        );
        return view('admin-panel.payroll-advance.add')->with($data);
    }
    /**
     *	Add & Update of Payroll Advance
     *  @Khushbu on 20 Feb 2019
    **/
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_advance_id		= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $session                    = get_current_session();
        if(!empty($id)) {
            $advance        = Advance::Find($decrypted_advance_id);
            if(!$advance) {
                return redirect('admin-panel/payroll/manage-advance-save')->withErrors('Advance not found!');
            }
            $admin_id    = $advance->admin_id;
            $success_msg = 'Advance updated successfully!';
        } else {
            $advance    	= New Advance;
            $success_msg    = 'Advance saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'staff_id'       => 'required',
                'advance_amount' => 'required',
                'suggested_amount' => 'required',
            ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $advance->admin_id             = $admin_id;
                $advance->update_by            = $loginInfo['admin_id'];
                $advance->session_id           = $session['session_id'];
                $advance->staff_id             = Input::get('staff_id');
                $advance->advance_amount       = Input::get('advance_amount');
                $advance->suggested_amount     = Input::get('suggested_amount');
                $advance->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-advance')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Advance's Data fo view page
     *  @Khushbu on 20 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$advance 		    = Advance::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_advance_employee') !=  NULL)
            {
                $query->where('staff_id', $request->get('s_advance_employee'));
            }
        })->orderBy('pay_advance_id','DESC')->with('AdvanceStaff')->get();
        return Datatables::of($advance)
        ->addColumn('employee_profile', function($advance) {
            $profile = '';
            if($advance['AdvanceStaff']['staff_profile_img'] != '') {
                $staff_profile_path = check_file_exist($advance['AdvanceStaff']['staff_profile_img'], 'staff_profile');
                $profile = "<img src=".url($staff_profile_path)." height='30' />";
            }
            $staff_name = $advance['AdvanceStaff']['staff_name'];
            $employee_profile = $profile." ".$staff_name;
            return $employee_profile;
        })
        ->addColumn('paid_amount', function($advance) {
            return $paid_amount = '---';
        })
    	->addColumn('action', function($advance) use($request) {
            $encrypted_advance_id  = get_encrypted_value($advance->pay_advance_id, true);
      		return '<div class="text-center">
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-advance/'.$encrypted_advance_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-advance/' . $encrypted_advance_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['employee_profile' => 'employee_profile', 'paid_amount' => 'paid_amount', 'action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-advance');
    }  

    /**
	 *	Destroy Data of Payroll Advance
     *  @Khushbu on 20 Feb 2019
	**/
	public function destroy($id) {
		$advance_id 	= get_decrypted_value($id, true);
        $advance 	    = Advance::find($advance_id);
        if ($advance)
        {
            DB::beginTransaction();
            try
            {
                $advance->delete();
                $success_msg = "Advance deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Advance not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
