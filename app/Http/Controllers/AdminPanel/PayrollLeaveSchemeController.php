<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\LeaveScheme; // Model
use App\Model\Payroll\LeaveSchemeMap; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollLeaveSchemeController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Leave Scheme
     * @Khushbu on 22 March 2019
    **/
    public function add(Request $request, $id = NULL) {
        $lscheme = $leave_period = $staff = $staff_mapped = $staff_ids_arr = []; 
        $loginInfo   = get_loggedin_user_data();
        $staff       = Staff::get(); 
        $arr_leave_period   = \Config::get('custom.leave_period');
        
        if(!empty($id)) {
            $decrypted_lscheme_id 	= get_decrypted_value($id, true);
        	$lscheme      		    = LeaveScheme::Find($decrypted_lscheme_id);
            // p($lscheme);
            $page_title             = trans('language.edit_lscheme');
        	$save_url    			= url('admin-panel/payroll/manage-leave-scheme-save/'. $id);
            $submit_button  		= 'Update';
            $leave_period['lscheme_period'] = $arr_leave_period; 
            $staff_mapped           = LeaveSchemeMap::select('staff_id')->where('pay_leave_scheme_id', $decrypted_lscheme_id)->get()->toArray();
            foreach($staff_mapped as $key => $value){
                $staff_ids_arr[] = $value['staff_id'];
            }
        } else {
            $page_title                = trans('language.add_lscheme');
	 		$save_url    			   = url('admin-panel/payroll/manage-leave-scheme-save');
            $submit_button  		   = 'Save';
            $leave_period['lscheme_period'] = $arr_leave_period; 
        }
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'lscheme'         => $lscheme,
            'staff'           => $staff,
            'leave_period'    => $leave_period,
            'staff_ids_arr'   => $staff_ids_arr,
        );
        return view('admin-panel.payroll-leave-scheme.add')->with($data);
    }

    /**
     *  Add & Update of Payroll Leave Scheme
     *  @Khushbu on 22 March 2019
     */
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_lscheme_id		= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $session                    = get_current_session();
        if(!empty($id)) {
            $lscheme             = LeaveScheme::Find($decrypted_lscheme_id);
            if(!$lscheme) {
                return redirect('admin-panel/payroll/manage-leave-scheme')->withErrors('Leave Scheme not found!');
            }
            $admin_id    = $lscheme->admin_id;
            $success_msg = 'Leave Scheme updated successfully!';
        } else {
            $lscheme    	 = New LeaveScheme;
            $success_msg     = 'Leave Scheme saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'lscheme_name'    	   => 'required|unique:pay_leave_scheme,lscheme_name,' . $decrypted_lscheme_id . ',pay_leave_scheme_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
                $success_msg = 'Staffs are not selected.';
                $error_messages = [];

            DB::beginTransaction();
            try
            {
                $lscheme->admin_id            = $admin_id;
                $lscheme->update_by           = $loginInfo['admin_id'];
                $lscheme->lscheme_name        = Input::get('lscheme_name');
                $lscheme->lscheme_period      = Input::get('leave_period');
                $lscheme->carry_forward_limit = Input::get('carry_forward_limit');
                $lscheme->no_of_leaves        = Input::get('no_of_leaves');
                $lscheme->special_instruction = Input::get('special_instruction');
                $lscheme->save();

                foreach ($request->get('staffs') as $value){
                    if($value['exist'] == 1 && !isset($value['staff_id']))
                    {
                        // Delete Case
                        $success_msg = "Record Updated successfully";
                        $lscheme_map  = LeaveSchemeMap::where('staff_id',$value['exist_id'])->first();
                        $lscheme_map->delete();
                
                    }
                    if($value['exist'] == 0 && isset($value['staff_id']))
                    {
                        //Add Case
                        $lscheme_map                   = New LeaveSchemeMap;
                        $success_msg = 'Staff Mapped successfully.';
                        $lscheme_map->admin_id            = $admin_id;
                        $lscheme_map->update_by           = $loginInfo['admin_id'];
                        $lscheme_map->session_id 	      = $session['session_id'];
                        $lscheme_map->pay_leave_scheme_id = $lscheme->pay_leave_scheme_id;
                        $lscheme_map->staff_id   	      = $value['staff_id'];
                        $lscheme_map->save();
                    }    
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
                DB::commit();
            } 
        }
        return redirect('admin-panel/payroll/manage-leave-scheme')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Leave Scheme's Data fo view page
     *  @Khushbu on 22 March 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_leave_period   = \Config::get('custom.leave_period');
    	$lscheme 		    = LeaveScheme::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_lscheme_name') !=  NULL)
            {
                $query->where('lscheme_name', 'Like', $request->get('s_lscheme_name').'%');
            }
        })->orderBy('pay_leave_scheme_id','DESC')->with('LeaveSchemeMap')->get();
        return Datatables::of($lscheme,$arr_leave_period)
        ->addColumn('lscheme_period', function($lscheme) use($arr_leave_period) {
            return $arr_leave_period[$lscheme['lscheme_period']];
        })
        ->addColumn('carry_forward_limit', function($lscheme) {
            if($lscheme['carry_forward_limit'] == '') {
                $carry_forward_limit = '---';
            } else {
                $carry_forward_limit = $lscheme['carry_forward_limit'];
            }
            return $carry_forward_limit;
        })
        ->addColumn('staff_mapped', function($lscheme) {
            $encrypted_lscheme_id  = get_encrypted_value($lscheme->pay_leave_scheme_id, true);
            $map_staff = '<a href="" class="btn btn-raised btn-primary lscheme-staff" data-toggle="modal" data-target="#viewStaffModel" lscheme_id='.$lscheme->pay_leave_scheme_id.'>View Staff</a>';
            return $map_staff;
        })
    	->addColumn('action', function($lscheme) use($request) {
            $encrypted_lscheme_id  = get_encrypted_value($lscheme->pay_leave_scheme_id, true);
              return '<div class="text-center">
                    
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-leave-scheme/'.$encrypted_lscheme_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-leave-scheme/' . $encrypted_lscheme_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['carry_forward_limit' => 'carry_forward_limit', 'lscheme_period' => 'lscheme_period', 'action' => 'action', 'staff_mapped' => 'staff_mapped'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-leave-scheme');
    }  
    /**
	 *	Destroy Data of Payroll Leave Scheme
     *  @Khushbu on 22 March 2019
	**/
	public function destroy($id) {
        $lscheme_id 	 = get_decrypted_value($id, true);
        $lscheme_map     = LeaveSchemeMap::where('pay_leave_scheme_id',$lscheme_id)->get();
        $lscheme 	     = LeaveScheme::find($lscheme_id);
        if ($lscheme)
        {
            DB::beginTransaction();
            try
            {
                foreach ($lscheme_map as $key => $value) {
                    $lscheme_map[$key]->delete();
                }
                $lscheme->delete();
                $success_msg = "Leave Scheme deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Leave Scheme not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /** 
     *  Get Staff Map Data
     *  @Khushbu on 22 march 2019.
    **/
    public function getLschemeStaffData(Request $request) {
        $lscheme_staff_map = LeaveSchemeMap::where('pay_leave_scheme_id',$request->get('lscheme_id'))->with('LeaveSchemeStaff')->get()->toArray();
        return Datatables::of($lscheme_staff_map)
        ->addColumn('employee_profile', function($lscheme_staff_map) {
        $profile = '';
        if($lscheme_staff_map['leave_scheme_staff']['staff_profile_img'] != '') {
            $staff_profile_path = check_file_exist($lscheme_staff_map['leave_scheme_staff']['staff_profile_img'], 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $lscheme_staff_map['leave_scheme_staff']['staff_name'];
        $employee_profile = $profile." ".$staff_name;
        return $employee_profile;
        })
        ->rawColumns(['employee_profile' => 'employee_profile'])->addIndexColumn()
        ->make(true); 
    } 
}
