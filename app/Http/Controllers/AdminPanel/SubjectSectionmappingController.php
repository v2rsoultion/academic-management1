<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SubjectClassMapping\SubjectClassmapping; // Model 
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\StudentSubjectManage\StudentSubjectManage; // Model
use App\Model\SubjectTeacherMapping\SubjectTeachermapping; // Model 
use App\Model\Subject\Subject; // Model 
use App\Model\Classes\Classes; // Model
use App\Model\Section\Section; // Model 
use Yajra\Datatables\Datatables;

class SubjectSectionMappingController extends Controller
{
    /**
     *  View page for Subject Class mapping
     *  @Pratyush on 07 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $mapp  = $arr_class = [];
        $arr_class = get_all_classes_mediums();
        $mapp['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'    => trans('language.view_map_subject_section'),
            'redirect_url'  => url('admin-panel/subject-subject-mapping/view-subject-section-mapping'),
            'login_info'    => $loginInfo,
            'mapp'          => $mapp
        );
        return view('admin-panel.map-subject-to-section.index')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
        {
            $section = Section::where(function($query) use ($request)  {
                $query->where('class_id', "=", $request->get('class_id'));
            })->with('sectionClass')->get();
        } else {
            $section = [];
        }
        
        return Datatables::of($section)
        ->addColumn('class_name', function ($section)
        {
            $arr_medium             = get_all_mediums();
            $class_name = $section['sectionClass']->class_name.' - '.$arr_medium[$section['sectionClass']->medium_type];
            return $class_name;
            
        })
        ->addColumn('no_of_subject', function ($section)
        {
            return SubjectSectionMapping::where('section_id',$section->section_id)->count();
            
        })
        
        ->addColumn('action', function ($section)
        {
            $encrypted_section_id = get_encrypted_value($section->section_id, true);
                return '<div class="dropdown">
            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="zmdi zmdi-label"></i>
            <span class="caret"></span>
            </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li><a href="map-subject/' . $encrypted_section_id . '" ">Map Subject</a></li>
                <li><a href="view-report/' . $encrypted_section_id . '" ">View Report</a></li>
                </ul>
            </div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Subject Class mapping - Map Subject
     *  @Pratyush on 07 Aug 2018
    **/
    public function getMapSubject($id)
    {
        $loginInfo = get_loggedin_user_data();
        $section_name = get_class_section_name_by_sectionid($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-section-mapping/view-subject-section-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/section-subject-mapping/save'),
            'section_id'		=> $id,
            'section_name'    => $section_name
        );
        return view('admin-panel.map-subject-to-section.map_subject')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
    	$session = get_current_session();
        $decrypted_section_id	= get_decrypted_value($request->get('section_id'), true);
        $section    = Section::find($decrypted_section_id);
        if ($section)
        {
            $subject  			= SubjectClassmapping::where('class_id',$section->class_id)->where('session_id', '=', $session['session_id'])->orderBy('subject_id', 'ASC')->with('getSubjects')->get();
            $subject_ids 		= SubjectSectionmapping::select('subject_id')->where('section_id','=',$decrypted_section_id)->where('session_id', '=', $session['session_id'])->where('class_id','=',$section->class_id)->get();
            
            $subjectArr 		= array(); 
            $subject_ids_Arr 	= array();
            
            foreach($subject_ids as $subject_id){
                $subject_ids_Arr[] = $subject_id->subject_id;
            }
            
            foreach($subject as $key => $value){
                $subjectArr[$key] = $value;
                $subjectArr[$key]['subject_arr'] = $subject_ids_Arr;
            }
            
            return Datatables::of($subjectArr)
        		->addColumn('checkbox', function ($subjectArr)
                {
                	$check = '';
                    $exist = 0;
                	if(!empty($subjectArr['subject_arr'])){

                		if(in_array($subjectArr->subject_id, $subjectArr['subject_arr'])){
                            $check = 'checked';
                            $exist = 1;
                		}
                	}
                    
                    return '
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist]" value="'.$exist.'" >
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist_id]" value="'.$subjectArr->subject_id.'" >
                    
                        <div class="checkbox" id="customid">
                            <input type="checkbox" id="subject'.$subjectArr->subject_id.'" name="subjects['.$subjectArr->subject_id.'][subject_id]" class="check" value="'.$subjectArr->subject_id.'" '.$check.'>
                            <label  class="from_one1" style="margin-bottom: 4px !important;"  for="subject'.$subjectArr->subject_id.'"></label>
                        </div>
                    ';
                    // return '<div class="checkbox"><input name="check[]" type="checkbox" class="check" value="'.$subjectArr->subject_id.'" '.$check.'><label for="checkbox10"></label></div>';
                    
                })
                ->addColumn('subjects', function ($subjectArr)
                {
        			return $subjectArr['getSubjects']->subject_code.' - '.$subjectArr['getSubjects']->subject_name;
                })->rawColumns(['checkbox' => 'checkbox','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
        }
        else
        {
            $error_message = "Section not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add and update subject class mapping's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request)
    {
        // p($request->all());
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_section_id		= get_decrypted_value($request->get('section_id'), true);
        $section                    = Section::find($decrypted_section_id);
        $session = get_current_session();
        $admin_id = $loginInfo['admin_id'];
        //p($request->all());
        
		if(!empty($request->get('section_id'))){
            
            if(!empty(Input::has('subjects')) && count(Input::get('subjects')) != 0){
                $success_msg = 'No new subjects are not selected.';
                try
                {
                    foreach (Input::get('subjects') as $value){
                        if($value['exist'] == 1 && !isset($value['subject_id']))
                        {
                            $get_student_subject = [];
                            $get_student_subject = StudentSubjectManage::where('class_id', '=', $section->class_id)->where('section_id', '=', $decrypted_section_id)->where('subject_id', '=', $value['exist_id'])->where('session_id', '=', $session['session_id'])->get()->toArray();
                            
    
                            $get_staff_subject = [];
                            $get_staff_subject = SubjectTeachermapping::where('class_id', '=', $section->class_id)->where('section_id', '=', $decrypted_section_id)->where('subject_id', '=', $value['exist_id'])->get()->toArray();

                            if(empty($get_student_subject) && empty($get_staff_subject)){
                                $get_section_subject = [];
                                $get_section_subject = SubjectSectionmapping::where('class_id', '=', $section->class_id)->where('section_id', '=', $decrypted_section_id)->where('subject_id', '=', $value['exist_id'])->where('session_id', '=', $session['session_id'])->first();
                                $get_section_subject->delete();
                            } else {
                                $subjectInfo = Subject::FIND($value['exist_id']);
                                $error_messages[] = "'".$subjectInfo->subject_name.'-'.$subjectInfo->subject_code."' subject is already used in further mapping so we cannot delete this mapping.";
                            }
                            
                        }
                        if($value['exist'] == 0 && isset($value['subject_id']))
                        {
                            $success_msg = 'Subjects assign successfully.';	
                            // Add case
                            $subject_mapping = new SubjectSectionmapping;		
                            $subject_mapping->admin_id       = $admin_id;
                            $subject_mapping->update_by      = $loginInfo['admin_id'];
                            $subject_mapping->class_id 	 	 = $section->class_id;
                            $subject_mapping->section_id 	 = $decrypted_section_id;
                            $subject_mapping->subject_id 	 = $value['subject_id'];
                            $subject_mapping->session_id 	 = $session['session_id'];
                           
                            $subject_mapping->save();
                        }
                    }
                    
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
                return redirect('admin-panel/section-subject-mapping/map-subject/'.$request->get('section_id'))->withSuccess($success_msg)->withErrors($error_messages);
            }
    	}else{
    		return redirect()->back();
    	}
	}

	/**
     *  View page for view report
     *  @Pratyush on 07 Aug 2018
    **/
    public function getViewReport($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_section_name_by_sectionid($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-section-mapping/view-subject-section-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/section-subject-mapping/save'),
            'section_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-section.view_report')->with($data);
    }

    /**
     *  Get Subject for view report page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyViewReportData(Request $request)
    {
        $session = get_current_session();
        $loginInfo 				= get_loggedin_user_data();
        $decrypted_section_id		= get_decrypted_value($request->get('section_id'), true);
        $subject  				= SubjectSectionmapping::where('section_id',$decrypted_section_id)->where('session_id', '=', $session['session_id'])->with('getSubjects')->orderBy('subject_section_map_id', 'ASC')->get();
        
        return Datatables::of($subject)
                ->addColumn('subjects', function ($subject)
                {
        			return $subject['getSubjects']->subject_code.' - '.$subject['getSubjects']->subject_name;
                })->rawColumns(['subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
    }
}