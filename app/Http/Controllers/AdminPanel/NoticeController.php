<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Notice\Notice;
use App\Model\Staff\Staff;
use App\Model\Section\Section;
use Yajra\Datatables\Datatables;

class NoticeController extends Controller
{
    /**
     *  View page for Notice
     *  @Shree on 22 Nov 2018
    **/
    public function index()
    {
        $notice = [];
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_notice'),
            'redirect_url'  => url('admin-panel/notice-board/view-notice'),
            'login_info'    => $loginInfo,
            'notice'         => $notice
        );
        return view('admin-panel.notice.index')->with($data);
    }

    /**
     *  View page for Notice
     *  @Shree on 23 Nov 2018
    **/
    public function staff_notice()
    {
        $notice = [];
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.notice'),
            'redirect_url'  => url('admin-panel/staff/notice'),
            'login_info'    => $loginInfo,
            'notice'         => $notice
        );
        return view('admin-panel.notice.staff_notice')->with($data);
    }

    /**
     *  View page for Notice
     *  @Shree on 23 Nov 2018
    **/
    public function student_notice()
    {
        $notice = [];
        $loginInfo              = get_loggedin_user_data();
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

    //  p($loginInfo);

        $data = array(
            'page_title'        => trans('language.notice'),
            'redirect_url'      => url('admin-panel/student/notice'),
            'login_info'        => $loginInfo,
            'parent_student'    => $parent_student,
            'notice'            => $notice
        );
        return view('admin-panel.notice.student_notice')->with($data);
    }

    /**
     *  Add page for Notice
     *  @Shree on 22 Nov 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   = [];
        $notice  = [];
        $loginInfo = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_notice_id   = get_decrypted_value($id, true);
            $notice                = Notice::Find($decrypted_notice_id);
            if (!$notice)
            {
                return redirect('admin-panel/notice-board/add-notice')->withError('Notice not found!');
            }
            if($notice->class_ids != ''){
                $notice->class_ids = explode(',', $notice->class_ids);
            }
            if($notice->staff_ids != ''){
                $notice->staff_ids = explode(',', $notice->staff_ids);
            }
            $page_title            = trans('language.edit_notice');
            $decrypted_notice_id   = get_encrypted_value($notice->notice_id, true);
            $save_url              = url('admin-panel/notice-board/save/' . $decrypted_notice_id);
            $submit_button         = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_notice');
            $save_url      = url('admin-panel/notice-board/save');
            $submit_button = 'Save';
        }
        $arr_selection  = \Config::get('custom.selection_status');
        $arr_class      = get_all_classes_mediums();
        $arr_staff      = get_all_staffs();
        $notice['arr_selection']    = add_blank_option($arr_selection,'Select for');
        $notice['arr_class']        = $arr_class;
        $notice['arr_staff']        = $arr_staff;
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'notice'         => $notice,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/notice-board/view-notice'),
        );
        return view('admin-panel.notice.add')->with($data);
    }

    /**
     *  Add and update notice's data
     *  @Shree on 23 Nov 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        // p($request->all());
        $loginInfo  = get_loggedin_user_data();
        $decrypted_notice_id = get_decrypted_value($id, true);
        $admin_id   = $loginInfo['admin_id'];
        $session = get_current_session();
        if (!empty($id))
        {
            $notice     = Notice::find($decrypted_notice_id);
            $admin_id   = $notice['admin_id'];
            if (!$notice)
            {
                return redirect('/admin-panel/notice-board/add-notice/')->withError('Notice not found!');
            }
            $success_msg = 'Notice updated successfully!';
        }
        else
        {
            $notice     = New Notice;
            $success_msg = 'Notice saved successfully!';
        }
        
        $validatior = Validator::make($request->all(), [
                'notice_name' => 'required|unique:notice,notice_name,' . $decrypted_notice_id . ',notice_id',
                'notice_text' => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            $class_ids = NULL;
            if($request->get('class_ids') != null){
                $class_ids =  implode(',',$request->get('class_ids') );
            }
            $staff_ids = NULL;
            if($request->get('staff_ids') != null){
                $staff_ids =  implode(',',$request->get('staff_ids') );
            }
            DB::beginTransaction();
            try
            {
                $notice->admin_id       = $admin_id;
                $notice->update_by      = $loginInfo['admin_id'];
                $notice->session_id     = $session['session_id'];
                $notice->notice_name    = Input::get('notice_name');
                $notice->notice_text    = Input::get('notice_text');
                $notice->class_ids      = $class_ids;
                $notice->staff_ids      = $staff_ids;
                $notice->for_class      = Input::get('for_class');
                $notice->for_staff      = Input::get('for_staff');
                $notice->save();

                $title = Input::get('notice_name');

                if($request->get('for_staff') === '0' ){
                    $notification = pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_staff',$notice->notice_text,'','',$title);
                } else {
                    $notification = $this->send_push_notification_staff($notice->notice_id,$staff_ids,'notice_issue',$title,$notice->notice_text);
                }
                if($request->get('for_class') === '0'){
                    $notification = pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_student',$notice->notice_text,'','',1,$title);
                    $notification = pushnotification_by_topic($notice->notice_id,'notice_issue','','','','active_parent',$notice->notice_text,'','',2,$title);
                } else {
                    $notification = $this->send_push_notification_class($notice->notice_id,$class_ids,'notice_issue',$title,$notice->notice_text);
                }
                // p($request->get('for_class'));
                // die;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        if($success_msg != ''){
            return redirect('admin-panel/notice-board/view-notice')->withSuccess($success_msg);
        } else {
            return redirect('admin-panel/notice-board/view-notice')->withSuccess($success_msg);
        }
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 23 Nov 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        // p($loginInfo);
        $notice = Notice::where(function($query) use ($request,$loginInfo) 
        {
            if (!empty($request) && !empty($request->get('name')) && $request->get('name') != null)
            {
                $query->where('notice_name', "like", "%{$request->get('name')}%");
            }
            if($loginInfo['admin_type'] == 2){
                if($loginInfo['staff_id'] != '') {
                    $query->orWhereRaw('FIND_IN_SET('.$loginInfo['staff_id'].',staff_ids)');
                }
                $query->orWhere('for_staff', "=", 0);
            }
            if($loginInfo['admin_type'] == 4){
                if($loginInfo['class_id'] != '') {
                    $query->orWhereRaw('FIND_IN_SET('.$loginInfo['class_id'].',class_ids)');
                }
                $query->orWhere('for_class', "=", 0);
            }
            if($loginInfo['admin_type'] == 3){
                if($loginInfo['class_id'] != '') {
                    $query->orWhereRaw('FIND_IN_SET('.$loginInfo['class_id'].',class_ids)');
                }
                $query->orWhere('for_class', "=", 0);
            }
           
        })->orderBy('notice_id', 'DESC')->get();
       
        return Datatables::of($notice)
        ->addColumn('for_class', function ($notice)
        {
            $status = '';
            if( is_null($notice->for_class) ){
                $status = 'No';
            } else if($notice->for_class == 1){
                $status = 'Selected Classes';
            } else {
                $status = 'All';
            }
            return $status;
        })
        ->addColumn('class_ids', function ($notice)
        {
            if($notice->class_ids != ''){
                $classes = get_selected_classes($notice->class_ids);
            } else {
                $classes = '-----';
            }
            return $classes;
           
        })
        ->addColumn('staff_ids', function ($notice)
        {
            if($notice->staff_ids != ''){
                $staff = get_selected_staff($notice->staff_ids);
            } else {
                $staff = '-----';
            }
            return $staff;
           
        })
        ->addColumn('for_staff', function ($notice)
        {
            $status = '';
            if(is_null($notice->for_staff)){
                $status = 'No';
            } else if($notice->for_staff == 1){
                $status = 'Selected Staff';
            } else {
                $status = 'All';
            }
            return $status;
        })
        ->addColumn('publish_date', function ($notice)
        {
            return date('d F Y', strtotime($notice->created_at));
        })
        ->addColumn('notice_text', function ($notice)
        {
            return substr($notice->notice_text,0,100);
        })
        ->addColumn('notice_full_text', function ($notice)
        {
            return $notice->notice_text;
        })
        ->addColumn('action', function ($notice)
        {
            $encrypted_notice_id = get_encrypted_value($notice->notice_id, true);
            if($notice->notice_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="'.url('/admin-panel/notice-board/notice-status/'.$status.'/'. $encrypted_notice_id. ' ').'" >'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/notice-board/add-notice/'. $encrypted_notice_id. ' ').'" ><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('/admin-panel/notice-board/delete-notice/'. $encrypted_notice_id. ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            
        })->rawColumns(['action' => 'action','staff_ids' => 'staff_ids','class_ids' => 'class_ids','publish_date'=>'publish_date', 'notice_full_text'=> 'notice_full_text'])->addIndexColumn()
        ->make(true);
    }

    
    /**
     *  Destroy Notice's data
     *  @Shree on 23 Nov 2018
    **/
    public function destroy($id)
    {
        $notice_id = get_decrypted_value($id, true);
        $notice    = Notice::find($notice_id);
       
        $success_msg = $error_message =  "";
        if ($notice)
        {
            DB::beginTransaction();
            try
            {
                $notice->delete();
                $success_msg = "Notice deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Notice not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change notice's status
     *  @Shree on 22 Nov 2018
    **/
    public function changeStatus($status,$id)
    {
        $notice_id = get_decrypted_value($id, true);
        $notice    = Notice::find($notice_id);
        if ($notice)
        {
            $notice->notice_status  = $status;
            $notice->save();
            $success_msg = "Notice status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Notice not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function send_push_notification_staff($module_id,$staff_ids,$notification_type,$title=null,$message=null){
        $device_ids = [];
        $loginInfo  = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($staff_ids) 
        {
            $query->whereIn('staff_id',explode(",",$staff_ids));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $staff['get_staff_admin_info']['admin_id'],
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = save_notification($info);
        }

        //$title = "New Notice Issued";
        //$message = "New Notice Issued";
        
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }

    public function send_push_notification_class($module_id,$class_ids,$notification_type,$title=null,$message=null){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_ids) 
        {
            $query->whereIn('class_id',explode(",",$class_ids));
        })
        ->with('sectionClass')
        ->get()->toArray();
        //$title = "New Notice Issued";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            //p($studentTopicName);

            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],1,$title);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],2,$title);
        }
        
    }
}
