<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\ConcessionMap; // Model
use App\Model\FeesCollection\RecurringInst; // Model
use Yajra\Datatables\Datatables;

class ManageStudentAccount extends Controller
{
    /**
     *  View page for wallet account
     *  @shree on 12 Oct 2018
    **/
    public function wallet()
    {
        $loginInfo  = get_loggedin_user_data();
        $account = $arr_section =  [];
        $arr_class  = get_all_classes_mediums();
        $account['arr_class']    = add_blank_option($arr_class, "Select Class");
        $account['arr_section']  = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.wallet_ac'),
            'redirect_url'  => url('admin-panel/manage-account/wallet-account'),
            'login_info'    => $loginInfo,
            'account'    => $account,
        );
        return view('admin-panel.manage-account.wallet')->with($data);
    }

    /**
     *  View page for imprest account
     *  @shree on 12 Oct 2018
    **/
    public function imprest()
    {
        $loginInfo  = get_loggedin_user_data();
        $account = $arr_section =  [];
        $arr_class  = get_all_classes_mediums();
        $account['arr_class']    = add_blank_option($arr_class, "Select Class");
        $account['arr_section']  = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.imprest_ac'),
            'redirect_url'  => url('admin-panel/manage-account/imprest-account'),
            'login_info'    => $loginInfo,
            'account'    => $account,
        );
        return view('admin-panel.manage-account.imprest')->with($data);
    }

    /**
     *  Get student list 
     *  @Shree on 9 Oct 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('wallet_amt', function ($student)
            {
                $wallet_amt = "Rs. ".$student->wallet_amt;
                return $wallet_amt;
            })
            ->addColumn('imprest_amt', function ($student)
            {
                $imprest_amt = "Rs. ".$student->imprest_amt;
                return $imprest_amt;
            })
            ->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'wallet_amt'=> 'wallet_amt', 'imprest_amt'=> 'imprest_amt'])->addIndexColumn()->make(true);
    }
}
