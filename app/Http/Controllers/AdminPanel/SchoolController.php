<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\School\School;
use App\Model\School\SchoolBoard;
use App\Model\School\ImprestAcConfig;
use App\Admin;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class SchoolController extends Controller
{
    public function __construct()
    {
        
    }
    public function add(Request $request, $id = NULL)
    {
        $school  =  $data = $arr_state = $arr_city  = [];
        $loginInfo      = get_loggedin_user_data();
        $arr_board      = get_all_school_boards();
        $arr_medium     = get_all_mediums();
        $arr_status     = \Config::get('custom.status');
        $arr_country    = get_all_country();
        if (!empty($id))
        {
            $decrypted_school_id = get_decrypted_value($id, true);
            $school              = School::where('school_id','=' ,$decrypted_school_id)->get()->toArray();
            $school              = isset($school[0]) ? $school[0] : [];
            $school['school_board_of_exams']    = explode(',', $school['school_board_of_exams']);
            $school['school_medium']            = explode(',', $school['school_medium']);
            if($school['country_id'] != '') {
                $arr_state = get_all_states($school['country_id']);
            }
            
            if($school['state_id'] != '') {
                $arr_city = get_all_city($school['state_id']);
            }
            $school['logo'] = check_file_exist($school['school_logo'], 'school_logo');
           
            if (!$school)
            {
                return redirect('admin-panel/school/add-school')->withError('School not found!');
            }
            $encrypted_school_id = get_encrypted_value($school['school_id'], true);
            $page_title           = 'Edit School';
            $save_url             = url('admin-panel/school/add-school/save/' . $encrypted_school_id);
            $submit_button        = 'Update';
        } else {
            $page_title           = trans('language.add_school');
            $save_url      = url('admin-panel/school/add-school/save');
            $submit_button = 'Save';
        }
        $school['arr_board']    = $arr_board;
        $school['arr_status']   = $arr_status;
        $school['arr_medium']   = $arr_medium;
        $school['arr_country']  = add_blank_option($arr_country, 'Select Country');
        $school['arr_state']    = add_blank_option($arr_state, 'Select State');
        $school['arr_city']     = add_blank_option($arr_city, 'Select City');
        
        // Check for Logo
        $school['school'] = check_file_exist($school['school_logo'], 'school_logo');
        // Check for school images 
        for($i = 1; $i <= 6; $i++) {
            if (!empty($school['school_img'.$i]))
            {
                $imageName = 'school_img'.$i;
                $school[$imageName] = check_file_exist($school['school_img'.$i], 'school_img');
            }
        }
        // Data for form page
        $data = array(
            'page_title' => $page_title,
            'redirect_url' =>url('admin-panel/add-school'),
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'login_info' => $loginInfo,
            'school'        => $school
            
        );
        return view('admin-panel.school.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        
        $decrypted_school_id = null;
        if (!empty($id))
        {
            $decrypted_school_id = get_decrypted_value($id, true);

            $school              = School::find($decrypted_school_id);
            $admin               = Admin ::Find(2);
            if (!$school)
            {
                return redirect('/admin-panel/school/add-school/')->withError('School not found!');
            }
            $success_msg = 'School information updated successfully!';
        }
        else
        {
            $school        = New school;
            $admin           = new Admin();
            $success_msg    = 'School saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'school_name'               => 'required',
                'school_registration_no'    => 'required',
                'school_address'            => 'required',
                'school_board_of_exams'     => 'required',
                'school_medium'             => 'required',
                'school_class_from'         => 'required',
                'school_class_to'           => 'required',
                'school_fee_range_from'     => 'required',
                'school_fee_range_to'       => 'required',
                'school_email'              => 'required',
                'school_mobile_number'      => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try
            {   
                $admin->admin_name = Input::get('school_name');
                $admin->email = Input::get('school_email');
                $admin->mobile_no = Input::get('school_mobile_number');
                $admin->admin_type = 1;
                $admin->save();
                // School table data
                $school->admin_id                   = $admin->admin_id;
                $school->update_by                  = $admin->admin_id;
                $school->school_name                = Input::get('school_name');
                $school->school_registration_no     = Input::get('school_registration_no');
                $school->school_sno_numbers         = Input::get('school_sno_numbers');
                $school->school_description         = Input::get('school_description');
                $school->school_address             = Input::get('school_address');
                $school->city_id                    = Input::get('city_id');
                $school->state_id                   = Input::get('state_id');
                $school->country_id                 = Input::get('country_id');
                $school->school_pincode             = Input::get('school_pincode');
                $school->school_board_of_exams      = implode(',',Input::get('school_board_of_exams'));
                $school->school_medium              = implode(',',Input::get('school_medium'));
                $school->school_class_from          = Input::get('school_class_from');
                $school->school_class_to            = Input::get('school_class_to');
                $school->school_total_students      = Input::get('school_total_students');
                $school->school_total_staff         = Input::get('school_total_staff');
                $school->school_fee_range_from      = Input::get('school_fee_range_from');
                $school->school_fee_range_to        = Input::get('school_fee_range_to');
                $school->school_facilities          = Input::get('school_facilities');
                $school->school_url                 = Input::get('school_url');
                $school->school_email               = Input::get('school_email');
                $school->school_mobile_number       = Input::get('school_mobile_number');
                $school->school_telephone           = Input::get('school_telephone');
                $school->school_fax_number          = Input::get('school_fax_number');
                $school->imprest_ac_status          = Input::get('imprest_ac_status');
                $school->school_latitude            = Input::get('school_latitude');
                $school->school_longitude           = Input::get('school_longitude');
                
                if ($request->hasFile('school_logo'))
                {
                    $file                          = $request->file('school_logo');
                    $config_upload_path            = \Config::get('custom.school_logo');
                    $destinationPath               = public_path() . $config_upload_path['upload_path'];
                    $ext                           = substr($file->getClientOriginalName(),-4);
                    $name                           = substr($file->getClientOriginalName(),0,-4);
                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                    $file->move($destinationPath, $filename);
                    $school->school_logo = $filename;
                }
                for($i = 1; $i <= 6; $i++) {
                    if ($request->hasFile('school_img'.$i))
                    {
                        $imageName = 'school_img'.$i;
                        $file                          = $request->file('school_img'.$i);
                        $config_upload_path            = \Config::get('custom.school_img');
                        $destinationPath               = public_path() . $config_upload_path['upload_path'];
                        $ext                           = substr($file->getClientOriginalName(),-4);
                        $name                           = substr($file->getClientOriginalName(),0,-4);
                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                        $file->move($destinationPath, $filename);
                        $school->$imageName = $filename;
                    }
                }
                $inserted_data = $admin->adminSchool()->save($school);
                $school_id    = $inserted_data->school_id;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            $encrypted_school_id = get_encrypted_value($school_id, true);
            return redirect('admin-panel/school/view-school-detail/'.$encrypted_school_id)->withSuccess($success_msg);
        }
    }

    public function profile($id = NULL)
    {
        $arr_medium     = get_all_mediums();
        if (!empty($id))
        {
            get_academic_year();
            $decrypted_school_id    = get_decrypted_value($id, true);
            $session                = get_arr_session();
            $school                 = School::find($decrypted_school_id)->with('getCountry')->with('getState')->with('getCity')->get()->toArray();
            $school                 = isset($school[0]) ? $school[0] : [];
            $school['medium_name']  = get_medium_info($school['school_medium']);
            $school['country_name'] = $school['get_country']['country_name'];
            $school['state_name']   = $school['get_state']['state_name'];
            $school['city_name']    = $school['get_city']['city_name'];
            $school['boards']       = get_boards_info($school['school_board_of_exams']);
            if (!$school)
            {
                return redirect('/admin-panel/school/add-school/')->withError('School not found!');
            }
        } 
        
        $school['logo'] = check_file_exist($school['school_logo'], 'school_logo');
        $school['school_img1'] = check_file_exist($school['school_img1'], 'school_img');
        $school['school_img2'] = check_file_exist($school['school_img2'], 'school_img');
        $school['school_img3'] = check_file_exist($school['school_img3'], 'school_img');
        $school['school_img4'] = check_file_exist($school['school_img4'], 'school_img');
        $school['school_img5'] = check_file_exist($school['school_img5'], 'school_img');
        $school['school_img6'] = check_file_exist($school['school_img6'], 'school_img');
        
        $loginInfo = get_loggedin_user_data();
        if($school['school_medium'] != ""){
            $school['school_medium'] = get_medium_info($school['school_medium']);
        }
        // p($school);
        $data = array(
            'page_title'    => trans('language.school_detail'),
            'login_info'    => $loginInfo,
            'school'        => $school,
            'session'       => $session,
        );
        return view('admin-panel.school.profile')->with($data);
    }

    /**
     *  Imprest account setting
     *  @Shree on 21 Oct 2018
    **/
    public function show_setting()
    {
        $data    		= [];
        $setting	= [];
        $loginInfo 		= get_loggedin_user_data();

        $setting = ImprestAcConfig::first();
        $page_title    					= trans('language.imprest_ac_setting');
        $encrypted_imprest_ac_confi_id  	= get_encrypted_value($setting->imprest_ac_confi_id, true);
        $save_url               		= url('admin-panel/school/setting/save/' . $encrypted_imprest_ac_confi_id);
        $submit_button = 'Save';
        
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'setting' 	        => $setting,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/school/setting/save/'.$encrypted_imprest_ac_confi_id),
        );
        return view('admin-panel.school.setting')->with($data);
    }

    /**
     *  Update Imprest ac setting's data
     *  @Shree on 21 Oct 2018.
    **/
    public function save_setting(Request $request, $id = NULL)
    {
        $loginInfo      				= get_loggedin_user_data();
        $decrypted_imprest_ac_confi_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $setting = ImprestAcConfig::find($decrypted_imprest_ac_confi_id);
            $admin_id = $setting['admin_id'];
            if (!$setting)
            {
                return redirect('/admin-panel/configuration/')->withError('Account setting not found!');
            }
            $success_msg = 'Imprest account setting updated successfully!';
        }
        

        $validatior = Validator::make($request->all(), [
            'imprest_ac_confi_amt'  => 'required',

        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $setting->admin_id       		= $admin_id;
                $setting->update_by      		= $loginInfo['admin_id'];
                $setting->imprest_ac_confi_amt 	= Input::get('imprest_ac_confi_amt');
                $setting->save();
                
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect()->back()->withSuccess($success_msg);
    }

}
