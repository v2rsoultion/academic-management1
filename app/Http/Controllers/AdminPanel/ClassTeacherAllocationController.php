<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model
use Yajra\Datatables\Datatables;

class ClassTeacherAllocationController extends Controller
{
    /**
     *  View page for Class Teacher Allocation
     *  @Pratyush on 06 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_class                  = [];
        $arr_section                = [];
        $listData                   = [];
        $arr_class                  = get_all_classes_mediums();
        $arr_staff                  = get_all_class_teachers();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_staff']      = add_blank_option($arr_staff, 'Select Teacher');
        $data = array(
            'page_title'    => trans('language.class_teacher_allocation'),
            'redirect_url'  => url('admin-panel/class-teacher-allocation/view-allocations'),
            'login_info'    => $loginInfo,
            'listData'      => $listData
        );
        return view('admin-panel.class-teacher-allocation.index')->with($data);
    }
   
    /**
     *  Add page for Class Teacher Allocation
     *  @Pratyush on 06 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data = $allocation = $arr_class = $arr_section =  [];
        $loginInfo 				= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_staff_class_allocation_id 	= get_decrypted_value($id, true);
            $allocation					= ClassTeacherAllocation::Find($decrypted_staff_class_allocation_id);
            $class_id       = $allocation['class_id'];
            $arr_staff      = get_all_class_teachers();
            $arr_section    = get_class_section($class_id);
            if (!$allocation)
            {
                return redirect('admin-panel/class-teacher-allocation/allocate-class')->withError('Allocation not found!');
            }
            $page_title             			 = trans('language.edit_allocation');
            $encrypted_staff_class_allocation_id = get_encrypted_value($allocation->staff_class_allocation_id, true);
            $save_url               			 = url('admin-panel/class-teacher-allocation/save/' . $encrypted_staff_class_allocation_id);
            $submit_button          			 = 'Update';
        }
        else
        {
            $page_title    = trans('language.allot_teacher');
            $save_url      = url('admin-panel/class-teacher-allocation/save');
            $submit_button = 'Save';
        }
        
        $arr_class                  = get_all_classes_mediums();
        $arr_staff                  = get_all_class_teachers();
        $allocation['arr_class']    = add_blank_option($arr_class, 'Select Class');
        $allocation['arr_staff']    = add_blank_option($arr_staff, 'Select Teacher');
        $allocation['arr_section']  = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'          => $save_url,
            'submit_button' 	=> $submit_button,
            'allocation'        => $allocation,
            'login_info'    	=> $loginInfo,
            'redirect_url'      => url('admin-panel/class-teacher-allocation/view-allocations'),
        );
        return view('admin-panel.class-teacher-allocation.add')->with($data);
    }

    /**
     *  Get sections data according class
     *  @Pratyush on 07 Aug 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.student.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add and update Class Teacher Allocation's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo                            = get_loggedin_user_data();
        $decrypted_staff_class_allocation_id  = null;
        $staff_id = null;
        $section_id = null;
        $decrypted_staff_class_allocation_id  = get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $staff_class_allocation = ClassTeacherAllocation::find($decrypted_staff_class_allocation_id);
            $admin_id = $staff_class_allocation['admin_id'];
            if (!$staff_class_allocation)
            {
                return redirect('/admin-panel/class-teacher-allocation/allocate-class/')->withError('Allocation not found!');
            }
            $success_msg = 'Class teacher allocation updated successfully!';
        }
        else
        {
            $staff_class_allocation = New ClassTeacherAllocation;
            $success_msg            = 'Class teacher allocation saved successfully!';
        }
        if ($request->has('teacher_id'))
        {
            $staff_id = Input::get('teacher_id');
        }
        if ($request->has('section_id'))
        {
            $section_id = Input::get('section_id');
        }
        $validatior = Validator::make($request->all(), [
                'class_id'     => 'required',
                'section_id'   => 'required|unique:staff_class_allocations,section_id,' . $decrypted_staff_class_allocation_id . ',staff_class_allocation_id,section_id,' . $section_id,
                'teacher_id'   => 'required|unique:staff_class_allocations,staff_id,' . $decrypted_staff_class_allocation_id . ',staff_class_allocation_id,staff_id,' . $staff_id,
        ],
        [
            'section_id.unique' => 'Staff already assign to this section.', 
            'teacher_id.unique' => 'Staff already assign to other class.'
        ]);

        if ($validatior->fails())
        {
           // p($validatior);
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $staff_class_allocation->admin_id       = $admin_id;
                $staff_class_allocation->update_by      = $loginInfo['admin_id'];
                $staff_class_allocation->class_id       = Input::get('class_id');
                $staff_class_allocation->section_id     = Input::get('section_id');
                $staff_class_allocation->staff_id       = Input::get('teacher_id');
                $staff_class_allocation->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/class-teacher-allocation/view-allocations')->withSuccess($success_msg);
    }

    /**
     *  Get Class Teacher Allocation's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyData(Request $request)
    {

        $loginInfo              = get_loggedin_user_data();
        $allocation = ClassTeacherAllocation::where(function($query) use ($loginInfo,$request){
                                 
            if (!empty($request) && $request->has('class_id') && $request->get('class_id') != null)
            {
                $query->where('staff_class_allocations.class_id',$request->get('class_id'));
            }
            if (!empty($request) && $request->has('section_id') && $request->get('section_id') != null)
            {
                $query->where('staff_class_allocations.section_id',$request->get('section_id'));
            }
            if (!empty($request) && $request->has('staff_id') && $request->get('staff_id') != null)
            {
                $query->where('staff_class_allocations.staff_id',$request->get('staff_id'));
            }
        })->with('getCurrentClass')->with('getCurrentClass.getMedium')->with('getCurrentClass.getBoard')
        ->with('getCurrentSection')->with('getStaff')->orderBy('staff_class_allocation_id', 'DESC')->get();
        return Datatables::of($allocation)
            ->addColumn('class_section', function ($allocation)
            {
                return $allocation['getCurrentClass']->class_name.' - '.$allocation['getCurrentClass']['getMedium']->medium_type.' - '.$allocation['getCurrentClass']['getBoard']->board_name.' - '.$allocation['getCurrentSection']->section_name;
            })
            ->addColumn('staff_name', function ($allocation)
            {
                return $allocation['getStaff']->staff_name;
            })
            ->addColumn('action', function ($allocation)
            {
                $encrypted_staff_class_allocation_id = get_encrypted_value($allocation->staff_class_allocation_id, true);

                return '<div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="allocate-class/' . $encrypted_staff_class_allocation_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Deallocate"><a href="delete-teacher/' . $encrypted_staff_class_allocation_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()
            ->make(true);
    }

    /**
     *  Destroy Class Teacher Allocation's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function destroy($id)
    {
        $staff_class_allocation_id       = get_decrypted_value($id, true);
        $staff_class_allocation          = ClassTeacherAllocation::find($staff_class_allocation_id);
        
        $success_msg = $error_message =  "";
        if ($staff_class_allocation)
        {
            DB::beginTransaction();
            try
            {
                $staff_class_allocation->delete();
                $success_msg = "Allocation deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/class-teacher-allocation/view-allocations')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/class-teacher-allocation/view-allocations')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Allocation not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
