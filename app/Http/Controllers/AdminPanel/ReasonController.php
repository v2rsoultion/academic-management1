<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\Reason; 
use Yajra\Datatables\Datatables;

class ReasonController extends Controller
{
    /**
     *  View page for Reason
     *  @Shree on 20 Oct 2018
    **/
    public function index()
    {
        $reason = [];
        $loginInfo      = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_reason'),
            'redirect_url'  => url('admin-panel/reason/view-reasons'),
            'login_info'    => $loginInfo,
            'reason'        => $reason
        );
        return view('admin-panel.reasons.index')->with($data);
    }

    /**
     *  Add page for Reason
     *  @Shree on 20 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $reason			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_reason_id 	= get_decrypted_value($id, true);
            $reason      			= Reason::Find($decrypted_reason_id);
            
            if (!$reason)
            {
                return redirect('admin-panel/reason/add-reason')->withError('Reason not found!');
            }
            $page_title             	= trans('language.edit_reason');
            $encrypted_reason_id   		= get_encrypted_value($reason->reason_id, true);
            $save_url               	= url('admin-panel/reason/save/' . $encrypted_reason_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_reason');
            $save_url      = url('admin-panel/reason/save');
            $submit_button = 'Save';
        }
        // p($reason);
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'reason' 			=> $reason,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/reason/view-reasons'),
        );
        return view('admin-panel.reasons.add')->with($data);
    }

    /**
     *  Add and update Reason's data
     *  @Shree on 20 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      		= get_loggedin_user_data();
        $decrypted_reason_id	= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $reason     = Reason::find($decrypted_reason_id);
            $admin_id   = $reason['admin_id'];
            if (!$reason)
            {
                return redirect('/admin-panel/reason/add-reason/')->withError('Reason not found!');
            }
            $success_msg = 'Reason updated successfully!';
        }
        else
        {
            $reason     	= New Reason;
            $success_msg 	= 'Reason saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'reason_text'   => 'required|unique:reasons,reason_text,' . $decrypted_reason_id . ',reason_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $reason->admin_id       = $admin_id;
                $reason->update_by      = $loginInfo['admin_id'];
                $reason->reason_text 	= Input::get('reason_text');
                $reason->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();

                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/reason/view-reasons')->withSuccess($success_msg);
    }


    /**
     *  Get Reason's Data for view page(Datatables)
     *  @Shree on 20 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $reason  			= Reason::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('reason_text')))
            {
                $query->where('reason_text', "like", "%{$request->get('reason_text')}%");
            }
           
        })->get();
        // p($reason);
        return Datatables::of($reason)
        
        ->addColumn('action', function ($reason)
        {
            $encrypted_reason_id = get_encrypted_value($reason->reason_id, true);
            if($reason->reason_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="reason-status/'.$status.'/' . $encrypted_reason_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-reason/' . $encrypted_reason_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-reason/' . $encrypted_reason_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Reason's data
     *  @Shree on 20 Oct 2018.
    **/
    public function destroy($id)
    {
        $reason_id 		= get_decrypted_value($id, true);
        $reason 		= Reason::find($reason_id);
        
        $success_msg = $error_message =  "";
        if ($reason)
        {
            DB::beginTransaction();
            try
            {
                $reason->delete();
                $success_msg = "Reason deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/reason/view-reasons')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/reason/view-reasons')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Reason not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change Reason's status
     *  @Shree on 20 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $reason_id 		= get_decrypted_value($id, true);
        $reason 		= Reason::find($reason_id);
        if ($reason)
        {
            $reason->reason_status  = $status;
            $reason->save();
            $success_msg = "Reason status update successfully!";
            return redirect('admin-panel/reason/view-reasons')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Reason not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
