<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InventoryVendor\Vendor; // Model
use Yajra\Datatables\Datatables;
use Redirect;
class InventoryVendorController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
	 *  Add Page of Vendor
     *  @Khushbu on 22 Dec 2018
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$vendor 				 	= []; 
	 	$loginInfo           		= get_loggedin_user_data();
	 	if(!empty($id))
	 	{
	 		$decrypted_vendor_id 	= get_decrypted_value($id, true);
        	$vendor      			= Vendor::Find($decrypted_vendor_id);
            $page_title             = trans('language.edit_vendor');
        	$save_url    			= url('admin-panel/inventory/manage-vendor-save/'. $id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
            $page_title             = trans('language.add_vendor');
	 		$save_url    			= url('admin-panel/inventory/manage-vendor-save');
	 		$submit_button  		= 'Save';
	 	}
	 	$data                   	= array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'vendor'		        => $vendor,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.inventory-vendor.add')->with($data);
    }

    /**
     *	Add & Update of Vendor
     *  @Khushbu on 22 Dec 2018
    **/
    public function save(Request $request, $id = NULL) 
    {
    	$loginInfo 				= get_loggedin_user_data();
    	$decrypted_vendor_id	= get_decrypted_value($id, true);
    	$admin_id				= $loginInfo['admin_id'];

    	if(!empty($id))
    	{
    		$vendor 			= Vendor::Find($decrypted_vendor_id);
    		if(!$vendor) {
    			return redirect('admin-panel/inventory/manage-vendor')->withErrors('Vendor not found');
    		}

    		$admin_id			= $vendor->admin_id;
    		$success_msg 		= "Vendor updated successfully!"; 
    	}
    	else
    	{
    		$vendor 		    = New Vendor;
    		$success_msg 	    = "Vendor saved successfully!"; 
    	}

    	$validator 				     =  Validator::make($request->all(), [
    		'vendor_name'  		     => 'required',
    		'vendor_gstin'			 => 'required',
    		'vendor_contact_no'		 => 'required|unique:inv_vendor,vendor_contact_no,' . $decrypted_vendor_id . ',vendor_id',
    		'vendor_email'			 => 'required|email|unique:inv_vendor,vendor_email,' . $decrypted_vendor_id . ',vendor_id',
    		'vendor_address'		 => 'required'
    	]);

    	if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $vendor->admin_id         	= $admin_id;
                $vendor->update_by        	= $loginInfo['admin_id'];
                $vendor->vendor_name   		= Input::get('vendor_name');
                $vendor->vendor_gstin   	= Input::get('vendor_gstin');
                $vendor->vendor_contact_no  = Input::get('vendor_contact_no');
                $vendor->vendor_email   	= Input::get('vendor_email');
                $vendor->vendor_address   	= Input::get('vendor_address');
                $vendor->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/manage-vendor')->withSuccess($success_msg);
    }

    /**
     *	Get Vendor's Data fo view page
     *  @Khushbu on 22 Dec 2018
    **/
    public function anyData(Request $request)
    {
    	$loginInfo 			= get_loggedin_user_data();
    	$vendor 			= Vendor::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('s_vendor_name') !=  NULL)
            {
                $query->where('vendor_name', 'Like', $request->get('s_vendor_name').'%');
            }
            if (!empty($request) && $request->get('s_vendor_email') !=  NULL)
            {
                $query->where('vendor_email', '=', $request->get('s_vendor_email'));
            }
            if (!empty($request) && $request->get('s_vendor_contact_no') !=  NULL)
            {
                $query->where('vendor_contact_no', '=', $request->get('s_vendor_contact_no'));
            }
        })->orderBy('vendor_id','DESC')->get();

    	return Datatables::of($vendor)
    	->addColumn('action', function($vendor) use($request) {
    		$encrypted_vendor_id  = get_encrypted_value($vendor->vendor_id, true);
            
    		if($vendor->vendor_status == 0) {

                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/inventory/manage-vendor-status/'.$status .'/'.$encrypted_vendor_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/inventory/manage-vendor/'.$encrypted_vendor_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/delete-manage-vendor/' . $encrypted_vendor_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/inventory/manage-vendor');
    }

    /** 
     *  Change Status of Vendor
     *  @Khushbu on 22 Dec 2018
    **/
    public function changeStatus($status,$id) 
    {
        $vendor_id        = get_decrypted_value($id, true);
        $vendor           = Vendor::find($vendor_id);
        if ($vendor)
        {
            $vendor->vendor_status  = $status;
            $vendor->save();
            $success_msg = "Vendor status update successfully!";
            return redirect('admin-panel/inventory/manage-vendor')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Vendor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Data of Vendor
     *  @Khushbu on 22 Dec 2018
    **/
    public function destroy($id) {
        $vendor_id        = get_decrypted_value($id, true);
        $vendor           = Vendor::find($vendor_id);

        if ($vendor)
        {
            DB::beginTransaction();
            try
            {
                $vendor->delete();
                $success_msg = "Vendor deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Vendor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
