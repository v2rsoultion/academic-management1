<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\Student\StudentAttendanceDetails; // Model
use Yajra\Datatables\Datatables;

class StudentReportController extends Controller
{
    /**
     *  View page for Student Attendance Data
     *  @Khushbu on 05 March 2019
    **/
    public function studentAttendance() {
        $map                = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_student_report'),
            'redirect_url'  => url('admin-panel/student-report/student-attendance-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.student-report.student_attendance_report')->with($data);
    }

    public function anyDataStudentAttendance(Request $request){
        $session        = get_current_session();
        if(!empty($request->get('class_id'))) {
            $student      = Student::with(['getStudentAttendence' => function($query) use($request) {
                $query->where([['student_attendance_type',1], ['class_id', $request->get('class_id')]]);
            }])->whereHas('getStudentAttendence', function($query) use($request) {
                $query->where([['student_attendance_type',1], ['class_id', $request->get('class_id')]]);
            })->get()->toArray();
        } else {
            $student      = Student::with(['getStudentAttendence' => function($query) {
                $query->where('student_attendance_type',1);
            }])->get()->toArray();
        }
        $total_days = 365 - count(get_school_all_holidays(1));
        return Datatables::of($student)   
        ->addColumn('student_profile', function ($student)
        {
            $profile = "";
            if($student['student_image'] != ''){
                $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $student['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })   
        ->addColumn('total_working_days', function($student) use($total_days) {
            return $total_days;
        })
        ->addColumn('total_present', function($student) {
            $encrypted_student_id = get_encrypted_value($student['student_id'], true);
            $total_present = '<a href="'.url('admin-panel/student/student-profile/'.$encrypted_student_id.'').'">'.count($student['get_student_attendence']).'</a>';
            return $total_present;
        })
       ->rawColumns(['student_profile' => 'student_profile', 'total_working_days' => 'total_working_days', 'total_present' => 'total_present'])->addIndexColumn()
        ->make(true);
    }
}
