<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\RTEHead; // Model
use App\Model\FeesCollection\RTEHeadMap; // Model
use App\Model\Student\Student; // Model
use Yajra\Datatables\Datatables;

class RTEHeadController extends Controller
{
    /**
     *  View page for RTE head
     *  @shree on 3 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_recurring_h'),
            'redirect_url'  => url('admin-panel/rte-head/rte-fees-head'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.rte-head.add')->with($data);
    }

    /**
     *  Add page for RTE Head
     *  @Shree on 3 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $rtehead      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_rte_head_id 	= get_decrypted_value($id, true);
            $rtehead      		= RTEHead::where('rte_head_id', $decrypted_rte_head_id)->get();
            $rtehead = isset($rtehead[0]) ? $rtehead[0] : [];
            if (!$rtehead)
            {
                return redirect('admin-panel/rte-head/rte-fees-head')->withError('RTE head not found!');
            }
           
            $page_title             	= trans('language.manage_rte_h');
            $encrypted_rc_head_id       = get_encrypted_value($rtehead->rte_head_id, true);
            $save_url               	= url('admin-panel/rte-head/save-rte-head/' . $id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_rte_h');
            $save_url      = url('admin-panel/rte-head/save-rte-head');
            $submit_button = 'Save';
        }
       
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'rtehead' 		    => $rtehead,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/rte-head/rte-fees-head'),
        );
        return view('admin-panel.rte-head.add')->with($data);
    }

     /**
     *  Add and update one time's data
     *  @Shree on 1 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo              = get_loggedin_user_data();
        $decrypted_rte_head_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $rtehead = RTEHead::find($decrypted_rte_head_id);
            $admin_id = $rtehead['admin_id'];
            if (!$rtehead)
            {
                return redirect('/admin-panel/rte-head/rte-fees-head/')->withError('RTE head not found!');
            }
            $success_msg = 'RTE head updated successfully!';
        }
        else
        {
            $rtehead     = New RTEHead;
            $success_msg = 'RTE head saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'rte_head'   => 'required|unique:rte_heads,rte_head,' . $decrypted_rte_head_id . ',rte_head_id',
            'rte_amount' => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $rtehead->admin_id      = $admin_id;
                $rtehead->update_by     = $loginInfo['admin_id'];
                $rtehead->rte_head 	    = Input::get('rte_head');
                $rtehead->rte_amount 	= Input::get('rte_amount');
                $rtehead->save();
              
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/rte-head/rte-fees-head')->withSuccess($success_msg);
    }

    /**
     *  Get RTE head's Data for view page(Datatables)
     *  @shree on 3 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $rtehead  	= RTEHead::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('rte_head', "like", "%{$request->get('name')}%");
            }
        })->orderBy('rte_head_id', 'DESC')->get();

        return Datatables::of($rtehead)
        
        ->addColumn('rte_amount', function ($rtehead)
        {
            $amount = "RS. ".$rtehead->rte_amount;
            return $amount;
        })
        ->addColumn('action', function ($rtehead)
        {

            $encrypted_rte_head_id = get_encrypted_value($rtehead->rte_head_id, true);
            if($rtehead->rte_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="rte-head-status/'.$status.'/' . $encrypted_rte_head_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="rte-fees-head/' . $encrypted_rte_head_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-rte-head/' . $encrypted_rte_head_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'no_of_inst'=> 'no_of_inst'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy RTE head's data
     *  @Shree on 3 Oct 2018.
    **/
    public function destroy($id)
    {
        $rte_head_id 	= get_decrypted_value($id, true);
        $rtehead 		= RTEHead::find($rte_head_id);
        
        $success_msg = $error_message =  "";
        if ($rtehead)
        {
            DB::beginTransaction();
            try
            {
                $rtehead->delete();
                $success_msg = "RTE head deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/rte-head/rte-fees-head')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/rte-head/rte-fees-head')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "RTE head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change RTE head's status
     *  @Shree on 3 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $rc_head_id       = get_decrypted_value($id, true);
        $rtehead          = RTEHead::find($rc_head_id);
        if ($rtehead)
        {
            $rtehead->rte_status  = $status;
            $rtehead->save();
            $success_msg = "RTE head status update successfully!";
            return redirect('admin-panel/rte-head/rte-fees-head')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "RTE head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for map RTE students
     *  @shree on 5 Oct 2018
    **/
    public function free_by_rte()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = $arr_section = [];
        $arr_class = get_all_classes_mediums();
        $listData['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']  = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.free_by_rte'),
            'redirect_url'  => url('admin-panel/map-student/free-by-rte'),
            'login_info'    => $loginInfo,
            'listData'      => $listData
        );
        return view('admin-panel.rte-head.student_list')->with($data);
    }

    /**
     *  Get Student List Data for view student page(Datatables)
     *  @Shree on 28 July 2018
    **/
    public function studentListData(Request $request)
    {
        
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $request->request->add(['student_type']);
            $request->merge(['student_type' => '1']);
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
       
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                if($student->rte_apply_status == 0) {
                    $statusVal ='<a class="btn btn-raised btn-primary"  href="rte-status/1/' . $encrypted_student_id . '" "">Apply free by RTE</a>';
                } else {
                    $statusVal ='<a class="btn btn-raised btn-primary"  href="rte-status/0/' . $encrypted_student_id . '" "">Remove form RTE </a>';
                }
              
                return $statusVal;
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile'])->addIndexColumn()->make(true);
    }

     /**
     *  Change RTE status of student
     *  @Shree on 6 Oct 2018.
    **/
    public function rteStatus($status,$id)
    {
        $student_id = get_decrypted_value($id, true);
        $student    = STUDENT::find($student_id);
        if ($student)
        {
            $student->rte_apply_status  = $status;
            $student->save();
            $success_msg = "RTE status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Apply fees on head
     *  @shree on 9 Oct 2018
    **/
    public function apply_fees_on_head()
    {
        $loginInfo = get_loggedin_user_data();
        $fess = $arr_section =  [];
        $arr_rte_head = get_all_rte_heads();
        $arr_class = get_all_classes_mediums();
        $fees['arr_rte_head'] = add_blank_option($arr_rte_head, "Select Head");
        $fees['arr_class'] = add_blank_option($arr_class, "Select Class");
        $fees['arr_section'] = add_blank_option($arr_section, "Select Section");
        $submit_button          	= 'Apply Fees Head';
        $data = array(
            'page_title'    => trans('language.apply_rte_fees'),
            'redirect_url'  => url('admin-panel/rte-head/rte-fees-head'),
            'login_info'    => $loginInfo,
            'fees'          => $fees,
            'submit_button' => $submit_button
        );
        return view('admin-panel.rte-head.apply_fees')->with($data);
    }

    /**
     *  Get student list for apply rte fees head
     *  @Shree on 9 Oct 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $request->request->add(['student_type', 'rte_apply_status']);
            $request->merge(['student_type' => '1', 'rte_apply_status'=>'1']);
           
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
       
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('Select', function ($student)
            {
                $check = "";
                if($student->rte_fees_apply_status == "Yes"){
                    $check = "checked";
                }
                $checkBox ='
                <div class="checkbox">
                <input id="checkbox'.$student->student_id.'" type="checkbox" name="students[]" value="'.$student->student_id.'" '.$check.'>
                <label class="from_one1" for="checkbox'.$student->student_id.'" style="margin-bottom: 4px !important;"></label>
                </div>';
              
                return $checkBox;
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'Select'=> 'Select'])->addIndexColumn()->make(true);
    }

    public function get_student_counts(Request $request){
        $session       = get_current_session();
        $total_students =  Student::where(function($query) use ($request)
        {
            $query->where('student_status', 1);
            $query->where('student_type', "=", '1');
            $query->where('rte_apply_status', "=", '1');
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $join->where('current_class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $join->where('current_section_id', "=", $request->get('section_id'));
            }
        })->get()->count();

        $map_students =  RTEHeadMap::where(function($query) use ($request,$session)
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('rte_head_id')))
            {
                $query->where('rte_head_id', "=", $request->get('rte_head_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
        })->get()->count();
        $remaining_students = 0;
        $remaining_students = $total_students - $map_students;
        $finalArray = array(
            'total_students' => $total_students,
            'remaining_students' => $remaining_students,
            'map_students'  => $map_students
        );
        return json_encode($finalArray);
    }

    function map_students(Request $request){
        $loginInfo = get_loggedin_user_data();
        $session       = get_current_session();
        $rte_head_id = $request->get('rte_head_id');
        $class_id = $request->get('student_class_id');
        $students = $request->get('students');
        $get_all_records = RTEHeadMap::where(function($query) use ($rte_head_id,$class_id,$session)
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($rte_head_id))
            {
                $query->where('class_id', "=", $class_id);
            }
            if (!empty($rte_head_id))
            {
                $query->where('rte_head_id', "=", $rte_head_id);
            }
        })->delete();
       
        foreach($students as $student){
            $rteMap   = new RTEHeadMap();
            $rteMap->admin_id       = $loginInfo['admin_id'];
            $rteMap->update_by      = $loginInfo['admin_id'];
            $rteMap->rte_head_id    = $rte_head_id;
            $rteMap->session_id     = $session['session_id'];
            $rteMap->class_id       = $class_id;
            $rteMap->student_id     = $student;
            $rteMap->save();
        }
        return "success";
    }
    
}
