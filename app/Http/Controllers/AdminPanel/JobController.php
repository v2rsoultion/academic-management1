<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Recruitment\Job;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;

class JobController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('18',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for job
     *  @Sandeep on 11 Feb 2019
    **/

    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_job_id = get_decrypted_value($id, true);
            $job              = Job::where('job_id','=',$decrypted_job_id)->get();
            $job              = isset($job[0]) ? $job[0] : [];
            
            if (!$job)
            {
                return redirect('admin-panel/recruitment/job/view-job')->withError('Job not found!');
            }
            
            $encrypted_job_id = get_encrypted_value($job->job_id, true);
            $page_title         = trans('language.edit_job');
            $save_url           = url('admin-panel/recruitment/job/save/' . $id);
            $submit_button      = 'Update';
        
        } else {
            $page_title    = trans('language.add_job');
            $save_url      = url('admin-panel/recruitment/job/save');
            $submit_button = 'Save';
        }

        $arr_medium                 = get_all_mediums();
        $job['arr_medium']          = add_blank_option($arr_medium, 'Select Medium');
        $arr_duration               = \Config::get('custom.job_durations');
        $job['arr_duration']        = $arr_duration;


        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'job'           => $job,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/job/view-job'),
        );
        return view('admin-panel.job.add')->with($data);
    }


    /**
     *  Save job Data
     *  @Sandeep on 11 Feb 2019
    **/

    public function save(Request $request, $id = NULL)
    {
        // p($request->all());
        $job_id           = null;
        $decrypted_job_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_job_id   = get_decrypted_value($id, true);
            $job                = Job::find($decrypted_job_id);
            $jobAdmin           = Admin::Find($job->reference_admin_id);
            $admin_id = $job['admin_id'];
            if (!$job)
            {
                return redirect('/admin-panel/recruitment/job/add-job')->withError('Job not found!');
            }
            $success_msg = 'Job updated successfully!';
        }
        else
        {
            $job                = New Job;
            $jobAdmin           = new Admin();
            $success_msg        = 'Job saved successfully!';
        }

        $arr_input_fields = [
            'job_name'                => 'required',
            'medium_type'             => 'required',
            'job_type'                => 'required',
            'job_no_of_vacancy'       => 'required',
            // 'job_recruitment'         => 'required',
            'job_description'         => 'required',
        ];
        
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                $job->admin_id                = $admin_id;
                $job->update_by               = $loginInfo['admin_id'];
                $job->job_name                = Input::get('job_name');
                $job->job_medium_type         = Input::get('medium_type');
                $job->job_type                = Input::get('job_type');
                $job->job_durations           = Input::get('job_durations');
                $job->job_no_of_vacancy       = Input::get('job_no_of_vacancy');
                $job->job_recruitment         = Input::get('job_recruitment');
                $job->job_description         = Input::get('job_description');
                $job->save();

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }   
        return redirect('admin-panel/recruitment/job/view-job')->withSuccess($success_msg);   
    }


    /**
     *  View page for job
     *  @Sandeep on 11 Feb 2019
    **/
   
    public function index()
    {
        $loginInfo                  = get_loggedin_user_data();
        $listData                   = [];
        $arr_medium                 = get_all_mediums();
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/job/view-job'),
            'page_title'    => trans('language.view_job'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.job.index')->with($data);
    }

    /**
     *  Get Data for view job page(Datatables)
     *  @Sandeep on 11 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $job  = Job::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null && $request->get('medium_type') != "Select Medium")
            {
                $query->where('job_medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('job_name')))
            {
                $query->where('job_name', "like", "%{$request->get('job_name')}%");
            }

        })->with('get_medium')
        ->with(['get_remaining_jobs'=>function($q) use ($request) {
            $q->where('status_type', "=", 2);
        }])
        ->orderBy('job_id', 'DESC')->get();

    //    p(count($job[0]['get_remaining_jobs']));

        return Datatables::of($job)
            ->addColumn('job', function ($job)
            {

                if($job->job_type == 0) {
                    $job_type = 'Temporary ('.$job->job_durations.')';
                } else {
                    $job_type = 'Permanent';
                }

                return '
                    Name :- '.$job->job_name.' <br/>
                    Medium :- '.$job['get_medium']->medium_type.' <br/>
                    Job Type :- '.$job_type.' <br/>
                ';
            })

            ->addColumn('remaining_vacancy', function ($job)
            {
                return ' '.$job->job_no_of_vacancy - count($job['get_remaining_jobs']).' ';
            })

            ->addColumn('job_description', function ($job)
            {
                return '  
                <button type="button" class="btn btn-raised btn-primary description" style="color:white;" job-id="'.$job['job_id'].'" >
                Description
                </button>
                ';
            })
            
            ->addColumn('action', function ($job)
            {
                $encrypted_job_id = get_encrypted_value($job->job_id, true);
                if($job->job_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="job-status/'.$status.'/' . $encrypted_job_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-job/' . $encrypted_job_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-job/' . $encrypted_job_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','job'=> 'job','job_description' => 'job_description'])->addIndexColumn()->make(true);
    }

    
    /**
     *  Change job's status
     *  @Sandeep on 11 Feb 2019
    **/
    public function changeStatus($status,$id)
    {
        $job_id = get_decrypted_value($id, true);
        $job    = job::find($job_id);
        if ($job)
        {
            $job->job_status  = $status;
            $job->save();
            $success_msg = "Job status updated successfully!";
            return redirect('admin-panel/recruitment/job/view-job')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Job not found!";
            return redirect('admin-panel/recruitment/job/view-job')->withErrors($error_message);
        }
    }

    /**
     *  Destroy Job data
     *  @Sandeep on 11 Feb 2019
    **/
    public function destroy($id)
    {
        $job_id = get_decrypted_value($id, true);
        $job    = Job::find($job_id);
        
        $success_msg = $error_message =  "";
        if ($job)
        {
            DB::beginTransaction();
            try
            {
                $job->delete();
                $success_msg = "Job deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/recruitment/job/view-job')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/recruitment/job/view-job')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Job not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Get recruitment single data
     *  @Sandeep on 11 Feb 2019
    **/
    public function single_recruitment_description(Request $request)
    {
        $job     = [];
        $job     = Job::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('job_id')) && $request->get('job_id') != null){
                $query->where('job_id', '=',$request->get('job_id'));
            }
        })->first();

        if($request->type == 1){
            $recruitment_description = $job->job_recruitment;
        } else {
            $recruitment_description = $job->job_description;
        }


        $data      = view('admin-panel.job.job_info',compact('recruitment_description','recruitment_description'))->render();
        return $data;
        
    }



}




