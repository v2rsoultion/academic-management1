<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SchoolGroup\SchoolGroup; // Model
use Yajra\Datatables\Datatables;

class SchoolGroupController extends Controller
{
    /**
     *  View page for SchoolHouse
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_school_group'),
            'redirect_url'  => url('admin-panel/schoolgroup/view-groups'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.school_group.index')->with($data);
    }

    /**
     *  Add page for SchoolHouse
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $schoolgroup 	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_schoolgroup_id 	= get_decrypted_value($id, true);
            $schoolgroup      			= SchoolGroup::Find($decrypted_schoolgroup_id);
            if (!$schoolgroup)
            {
                return redirect('admin-panel/schoolgroup/add-group')->withError('Group not found!');
            }
            $page_title             	= trans('language.edit_school_group');
            $encrypted_schoolgroup_id   = get_encrypted_value($schoolgroup->group_id, true);
            $save_url               	= url('admin-panel/schoolgroup/save/' . $encrypted_schoolgroup_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_school_group');
            $save_url      = url('admin-panel/schoolgroup/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'schoolgroup' 		=> $schoolgroup,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/schoolgroup/view-groups'),
        );
        return view('admin-panel.school_group.add')->with($data);
    }

    /**
     *  Add and update SchoolHouse's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_schoolgroup_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $schoolgroup = SchoolGroup::find($decrypted_schoolgroup_id);
            $admin_id = $schoolgroup['admin_id'];
            if (!$schoolgroup)
            {
                return redirect('/admin-panel/schoolhouse/add-schoolhouse/')->withError('School Group not found!');
            }
            $success_msg = 'School Group updated successfully!';
        }
        else
        {
            $schoolgroup     = New SchoolGroup;
            $success_msg = 'School Group saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'group_name'   => 'required|unique:groups,group_name,' . $decrypted_schoolgroup_id . ',group_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $schoolgroup->admin_id            	= $admin_id;
                $schoolgroup->update_by           	= $loginInfo['admin_id'];
                $schoolgroup->group_name 		    = Input::get('group_name');
                $schoolgroup->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/schoolgroup/view-groups')->withSuccess($success_msg);
    }

    /**
     *  Get SchoolHouse's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $schoolgroup  		= SchoolGroup::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('group_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('group_id', 'DESC')->get();

        return Datatables::of($schoolgroup)
        		->addColumn('action', function ($schoolgroup)
                {
                    $encrypted_schoolgroup_id = get_encrypted_value($schoolgroup->group_id, true);
                    if($schoolgroup->group_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="pull-left"><a href="group-status/'.$status.'/' . $encrypted_schoolgroup_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-group/' . $encrypted_schoolgroup_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-group/' . $encrypted_schoolgroup_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy SchoolHouse's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $schoolgroup_id         = get_decrypted_value($id, true);
        $schoolgroup            = SchoolGroup::find($schoolgroup_id);
        
        $success_msg = $error_message =  "";
        if ($schoolgroup)
        {
            DB::beginTransaction();
            try
            {
                $schoolgroup->delete();
                $success_msg = "School Group deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/schoolgroup/view-groups')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/schoolgroup/view-groups')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "School Group not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change SchoolHouse's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $schoolgroup_id 		= get_decrypted_value($id, true);
        $schoolgroup 		  	= SchoolGroup::find($schoolgroup_id);
        if ($schoolgroup)
        {
            $schoolgroup->group_status  = $status;
            $schoolgroup->save();
            $success_msg = "School Group status updated!";
            return redirect('admin-panel/schoolgroup/view-groups')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "School Group not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
