<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InventoryUnit\Unit; // Model
use App\Model\InventoryItem\Items; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class InventoryUnitController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
	 *  Add Page of Unit
     *  @Khushbu on 21 Dec 2018
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$unit 				 		= []; 
	 	$loginInfo           		= get_loggedin_user_data();
	 	if(!empty($id))
	 	{
	 		$decrypted_unit_id 	    = get_decrypted_value($id, true);
        	$unit      				= Unit::Find($decrypted_unit_id);
            $page_title             = trans('language.edit_unit');
        	$save_url    			= url('admin-panel/inventory/manage-unit-save/'. $id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
            $page_title             = trans('language.add_unit');
	 		$save_url    			= url('admin-panel/inventory/manage-unit-save');
	 		$submit_button  		= 'Save';
	 	}
	 	$data                   	=  array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'unit'		        	=> $unit,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.inventory-unit.add')->with($data);
    }

    /**
     *	Add & Update of Unit
     *  @Khushbu on 21 Dec 2018
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_unit_id			= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $unit        = Unit::Find($decrypted_unit_id);
            if(!$unit) {
                return redirect('admin-panel/inventory/manage-unit')->withErrors('Unit not found!');
            }

            $admin_id    = $unit->admin_id;
            $success_msg = 'Unit updated successfully!';
        }
        else
        {
            $unit    	 = New Unit;
            $success_msg = 'Unit saved successfully!';
        }

            $validator             =  Validator::make($request->all(), [
                'unit_name'    	   => 'required|unique:inv_unit,unit_name,' . $decrypted_unit_id . ',unit_id'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $unit->admin_id         = $admin_id;
                $unit->update_by        = $loginInfo['admin_id'];
                $unit->unit_name   		= Input::get('unit_name');
                $unit->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/manage-unit')->withSuccess($success_msg);
    }

    /**
     *	Get Unit's Data fo view page
     *  @Khushbu on 21 Dec 2018
    **/
    public function anyData(Request $request)
    {
    	$loginInfo 			= get_loggedin_user_data();
    	$unit 				= Unit::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('search_unit_name') !=  NULL)
            {
                $query->where('unit_name', 'Like', $request->get('search_unit_name').'%');
            }
        })->orderBy('unit_id','DESC')->get();

    	return Datatables::of($unit)
    	->addColumn('action', function($unit) use($request) {
    		$encrypted_unit_id  = get_encrypted_value($unit->unit_id, true);
            
    		if($unit->unit_status == 0) {

                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/inventory/manage-unit-status/'.$status .'/'.$encrypted_unit_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/inventory/manage-unit/'.$encrypted_unit_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/delete-manage-unit/' . $encrypted_unit_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/inventory/manage-unit');
    }    

    /** 
     *	Change Status of Unit
     *  @Khushbu on 21 Dec 2018
	**/
	public function changeStatus($status,$id) 
	{
		$unit_id 		= get_decrypted_value($id, true);
        $unit 		  	= Unit::find($unit_id);
        $find_unit_in_items = Items::whereRaw('FIND_IN_SET('.$unit_id.',unit_id)')->get()->count(); 
        if($find_unit_in_items == 0) {
            if ($unit)
            {
                $unit->unit_status  = $status;
                $unit->save();
                $success_msg = "Unit status update successfully!";
                return redirect('admin-panel/inventory/manage-unit')->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Unit not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't update status it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Unit
     *  @Khushbu on 21 Dec 2018
	**/
	public function destroy($id) {
		$unit_id 		= get_decrypted_value($id, true);
        $unit 		  	= Unit::find($unit_id);
        $find_unit_in_items = Items::whereRaw('FIND_IN_SET('.$unit_id.',unit_id)')->get()->count(); 
        if($find_unit_in_items == 0) {
            if ($unit)
            {
                DB::beginTransaction();
                try
                {
                    $unit->delete();
                    $success_msg = "Unit deleted successfully!";
                }  
                catch(\Exception $e)
                {
                    DB::rollback();
                    $error_message = "Sorry we can't delete it because it's already in used!!";
                    return redirect()->back()->withErrors($error_message);
                }  
                DB::commit();
                return redirect()->back()->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Unit not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't delete it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
	}

    /** 
     *  Get Unit Data according to item
     *  @Khushbu on 25 Jan 2019. 
    **/
    public function getUnitData($item_id=null) {
        $item_id    = Input::get('item_id');
        $get_unit   = Items::where('item_id', $item_id)->with('unit')->get();
        $unit_data = array(
            'unit_id' => $get_unit[0]['unit']['unit_id'],
            'unit_name' => $get_unit[0]['unit']['unit_name'],
            'available_quantity' => $get_unit[0]['available_quantity'],
            'item_name'   => $get_unit[0]['item_name'],
        );
        return json_encode($unit_data);
    }
}
