<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notes\Notes; // Model
use App\Model\Section\Section; // Model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;

class NoteController extends Controller
{
    /**
     *  View page for Notes
     *  @Ashish on 31 July 2018
    **/
    public function index()
    {
        $listData  = $arr_class = $arr_subject  = [];
        $arr_subject                = get_all_subject();
        $arr_class                  = get_all_classes_mediums();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_subject']    = add_blank_option($arr_subject, 'Select Subject');
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_notes'),
            'redirect_url'  => url('admin-panel/notes/view-notes'),
            'login_info'    => $loginInfo,
            'listData'      => $listData

        );
        return view('admin-panel.notes.index')->with($data);
    }

    /**
     *  Add page for Notes
     *  @Ashish on 31 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data  = $notes = 	$arr_subject = $arr_class = $arr_section = [];
        $notes 			= [];
        $class_id       = null;
        $loginInfo 		= get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_notes_id 	= get_decrypted_value($id, true);
            $notes      			= Notes::Find($decrypted_notes_id);
            if (!$notes)
            {
                return redirect('admin-panel/notes/add-notes')->withError('Notes not found!');
            }
            $page_title             	= trans('language.edit_notes');
            $encrypted_notes_id   		= get_encrypted_value($notes->online_note_id, true);
            $save_url               	= url('admin-panel/notes/save/' . $encrypted_notes_id);
            $submit_button          	= 'Update';
            $arr_section                 = get_class_section($notes->class_id);

        }
        else
        {
            $page_title    = trans('language.add_notes');
            $save_url      = url('admin-panel/notes/save');
            $submit_button = 'Save';
        }
        $arr_subject                 = get_all_subject();
        $arr_class                   = get_all_classes_mediums();
        $notes['arr_class']          = add_blank_option($arr_class, 'Select class');
        $notes['arr_section']        = add_blank_option($arr_section, 'Select section');
        $notes['arr_subject']        = add_blank_option($arr_subject, 'Select subject');

        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'notes' 			=> $notes,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/notes/view-notes'),
        );
        return view('admin-panel.notes.add')->with($data);
    }

    /**
     *  Get sections data according class
     *  @Ashish on 30 July 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.notes.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }


    /**
     *  Add and update Notes's data
     *  @Ashish on 31 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_notes_id			= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $notes = Notes::find($decrypted_notes_id);
            $admin_id = $notes['admin_id'];
            if (!$notes)
            {
                return redirect('/admin-panel/notes/add-notes/')->withError('notes not found!');
            }
            $success_msg = 'Notes updated successfully!';
        }
        else
        {
            $notes     		= New Notes;
            $success_msg 	= 'Notes saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                't_name'   => 'required|unique:online_notes,online_note_name,' . $decrypted_notes_id . ',online_note_id',
                'notes_class_id'    => 'required',
                'notes_section_id'  => 'required',
                'notes_subject_id'  => 'required',
                'unit'              => 'required',
                'topic'             => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $notes->admin_id             = $admin_id;
                $notes->update_by            = $loginInfo['admin_id'];
                $notes->staff_id             = $loginInfo['admin_id'];
                $notes->online_note_name 	 = Input::get('t_name');
                $notes->online_note_unit 	 = Input::get('unit');
                $notes->online_note_topic 	 = Input::get('topic');
                $notes->class_id 	         = Input::get('notes_class_id');
                $notes->section_id 	         = Input::get('notes_section_id');
                $notes->subject_id 	         = Input::get('notes_subject_id');

                if ($request->hasFile('online_note_url'))
                {
                    if (!empty($id)){
                        $note_file = check_file_exist($notes->online_note_url, 'online_notes');
                        if (!empty($note_file))
                        {
                            unlink($note_file);
                        } 
                    }
                    $file                          = $request->file('online_note_url');
                    $config_upload_path            = \Config::get('custom.online_notes');
                    $destinationPath               = public_path() . $config_upload_path['upload_path'];
                    $ext                           = substr($file->getClientOriginalName(),-4);
                    $name                          = substr($file->getClientOriginalName(),0,-4);
                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                    $file->move($destinationPath, $filename);
                    $notes->online_note_url = $filename;
                }

                $notes->save();
                $notification = $this->send_push_notification_class($notes->notes_id,$request->get('notes_class_id'),$request->get('notes_section_id'),'notes_add');
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
    }

    /**
     *  Get Notes's Data for view page(Datatables)
     *  @Ashish on 31 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        $class_id   = null;
        $subject_id = null;
        $class_id   = $request->get('class_id');
        $subject_id = $request->get('subject_id');
        $section_id = $request->get('section_id');
        $medium_type = $request->get('medium_type');

        $notes      = Notes::where(function($query) use ($class_id,$subject_id,$medium_type,$section_id) 
        { 
            if (!empty($class_id)) 
            {
                $query->where('class_id', "=", $class_id);
            }
            if (!empty($subject_id)) 
            {
                $query->where('subject_id', "=", $subject_id); 
            }
            if (!empty($section_id)) 
            {
                $query->where('section_id', "=", $section_id); 
            }
            if (!empty($medium_type)) 
            {
                $query->where('medium_type',"=",  $medium_type); 
            }
        })->orderBy('online_note_id', 'DESC')->with('getClass')->with('getSection')->with('getSubject')->get();
        return Datatables::of($notes)
        ->addColumn('online_note_topic', function ($notes)
        {
            return substr($notes->online_note_topic,0,50)."...";
        })
        ->addColumn('class_subject', function ($notes)
        {
            return $notes['getSubject']['subject_name'];
        })
        ->addColumn('class_section', function ($notes)
        {
            $arr_medium         = get_all_mediums();
            return $notes['getClass']['class_name'].' - '.$arr_medium[$notes['getClass']['medium_type']].' - '.$notes['getSection']['section_name'];
        })
        ->addColumn('file_url', function ($notes)
            {	
                $attachment_link = '';
                if($notes->online_note_url == ''){
                    $attachment_link = 'No file available';
                }else{
                    $config_document_upload_path   = \Config::get('custom.online_notes');
                    $doc_path = $config_document_upload_path['display_path'].$notes->online_note_url;
                    $attachment_link = '<span class="profile_detail_span_6"><a href="'.url($doc_path).'" target="_blank">View file</a></span>';
                }
                return $attachment_link;
                
            })
        ->addColumn('action', function ($notes)
        {
            $encrypted_notes_id = get_encrypted_value($notes->online_note_id, true);
            if($notes->online_note_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="notes-status/'.$status.'/' . $encrypted_notes_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-notes/' . $encrypted_notes_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-notes/' . $encrypted_notes_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['online_note_topic' => 'online_note_topic', 'class_subject' => 'class_subject','class_section' => 'class_section','action' => 'action','file_url'=> 'file_url'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Notes's data
     *  @Ashish on 31 July 2018.
    **/
    public function destroy($id)
    {
        $notes_id 		= get_decrypted_value($id, true);
        $notes 		  	= Notes::find($notes_id);
        
        $success_msg = $error_message =  "";
        if ($notes)
        {
            DB::beginTransaction();
            try
            {
                $notes->delete();
                $success_msg = "Notes deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/notes/view-notes')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Notes not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Notes's status
     *  @Ashish on 31 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $notes_id 		= get_decrypted_value($id, true);
        $notes 		  	= Notes::find($notes_id);
        if ($notes)
        {
            $notes->online_note_status  = $status;
            $notes->save();
            $success_msg = "Notes status update successfully!";
            return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Notes not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  View page for Student Notes
     *  @Sandeep on 18 Jan 2019
    **/
    public function student_notes()
    {
        $listData       = $arr_class = $section_id  = [];
        $loginInfo      = get_loggedin_user_data();
        $arr_class      = $loginInfo['class_id'];
        $section_id     = $loginInfo['section_id'];
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

        $listData['arr_class']      = $arr_class;
        $listData['section_id']     = $section_id;
 
        $data = array(
            'page_title'        => trans('language.view_notes'),
            'redirect_url'      => url('admin-panel/notes/view-notes'),
            'parent_student'    => $parent_student,
            'login_info'        => $loginInfo,
            'listData'          => $listData
        );

        return view('admin-panel.notes.student_notes')->with($data);
    }

    public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();
        $message = "New online content is published, click here to check.";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            
            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['class_id'],$section['section_id'],1);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['class_id'],$section['section_id'],2);
           
        }
        
    }
}
