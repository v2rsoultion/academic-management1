<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\School\SchoolBoard;
use Yajra\Datatables\Datatables;

class BoardController extends Controller
{
    /**
     *  View page for Board
     *  @shree on 29 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_school_board'),
            'redirect_url'  => url('admin-panel/school-board/view-school-boards'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.school-board.index')->with($data);
    }

    /**
     *  Add page for Board
     *  @shree on 29 sept 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $board      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_board_id 	    = get_decrypted_value($id, true);
            $board      			    = SchoolBoard::Find($decrypted_board_id);
            if (!$board)
            {
                return redirect('admin-panel/school-board/add-school-board')->withError('School Board not found!');
            }
            $page_title             	= trans('language.edit_school_board');
            $encrypted_board_id         = get_encrypted_value($board->board_id, true);
            $save_url               	= url('admin-panel/school-board/save/' . $encrypted_board_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_school_board');
            $save_url      = url('admin-panel/school-board/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'board' 		    => $board,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/school-board/view-school-boards'),
        );
        return view('admin-panel.school-board.add')->with($data);
    }

    /**
     *  Add and update Board's data
     *  @shree on 29 sept 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_board_id	        = get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $board = SchoolBoard::find($decrypted_board_id);
            $admin_id = $board['admin_id'];
            if (!$board)
            {
                return redirect('/admin-panel/school-board/add-school-board/')->withError('School Board not found!');
            }
            $success_msg = 'School Board updated successfully!';
        }
        else
        {
            $board     = New SchoolBoard;
            $success_msg = 'School Board saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'board_name'   => 'required|unique:school_boards,board_name,' . $decrypted_board_id . ',board_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $board->admin_id            	= $admin_id;
                $board->update_by           	= $loginInfo['admin_id'];
                $board->board_name 		        = Input::get('board_name');
                $board->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/school-board/view-school-boards')->withSuccess($success_msg);
    }

    /**
     *  Get Board's Data for view page(Datatables)
     *  @shree on 29 sept 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $board  		    = SchoolBoard::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('board_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('board_id', 'DESC')->get();

        return Datatables::of($board)
        		->addColumn('action', function ($board)
                {

                    $encrypted_board_id = get_encrypted_value($board->board_id, true);
                    if($board->board_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="pull-left"><a href="school-board-status/'.$status.'/' . $encrypted_board_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-school-board/' . $encrypted_board_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-school-board/' . $encrypted_board_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy Board's data
     *  @shree on 29 sept 2018.
    **/
    public function destroy($id)
    {
        $board_id 		= get_decrypted_value($id, true);
        $board 		  	= SchoolBoard::find($board_id);
        
        $success_msg = $error_message =  "";
        if ($board)
        {
            DB::beginTransaction();
            try
            {
                $board->delete();
                $success_msg = "School Board deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/school-board/view-school-boards')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/school-board/view-school-board')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "School Board not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Board's status
     *  @Pratyush on 29 sept 2018.
    **/
    public function changeStatus($status,$id)
    {
        $board_id       = get_decrypted_value($id, true);
        $board          = SchoolBoard::find($board_id);
        if ($board)
        {
            $board->board_status  = $status;
            $board->save();
            $success_msg = "School Board status update successfully!";
            return redirect('admin-panel/school-board/view-school-boards')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "School Board not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
