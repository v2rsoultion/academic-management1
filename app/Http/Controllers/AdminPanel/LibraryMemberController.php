<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\Staff\Staff; // Model 
use App\Model\LibraryMember\LibraryMember; // Model 
use App\Model\IssueBook\IssueBook; // Model
use Yajra\Datatables\Datatables;
use URL;
use Redirect;


class LibraryMemberController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
	/**
     *  Get Student's page for register
     *  @Pratyush on 19 Aug 2018.
    **/
    public function addStudent(){

    	$loginInfo = get_loggedin_user_data();
    	$arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.member_student'),
            'redirect_url'  => url('admin-panel/member/register-student'),
            'login_info'    => $loginInfo,
            'listData'		=> $listData,
            'save_url'		=> url('admin-panel/member/save-students')
        );
        return view('admin-panel.library-member.register_student_new')->with($data);
    }

    /**
     *  Get Student's Data for register page(Datatables)
     *  @Pratyush on 19 Aug 2018.
    **/
    public function anyStudnetData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        $session       = get_current_session();
        $arr_student_list = array();
        $arr_student 	  =  Student::where(function($query) use ($loginInfo,$request) 
            {
               
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')))
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')))
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                $query->where('student_status', 1);
            })->join('student_academic_info', function($join) use ($request,$session){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
                 $join->where('current_session_id', $session['session_id']);
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')))
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')
            ->whereNotExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('library_members')
                        ->whereRaw('students.student_id = library_members.member_id')
                        ->where('library_members.library_member_type',1);
                })->get();


            if (!empty($arr_student[0]->student_id))
        	{
	            foreach ($arr_student as $student)
	            {
	                $student_image = "";
	                if (!empty($student->student_image))
	                {
	                    $profile = check_file_exist($student->student_image, 'student_image');
	                    if (!empty($profile))
	                    {
	                        $student_image = URL::to($profile);
	                    }
	                } else {
	                    $student_image = "";
	                }
		                $arr_student_list[] = array(
	                        'student_id'            => $student->student_id,
	                        'profile'               => $student_image,
	                        'student_name'          => $student->student_name,
	                        'student_status'        => $student->student_status,
	                        'student_father_name'   => $student['getParent']->student_father_name,
	                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
	                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
	                    );
	            }
        	}

        
        return Datatables::of($arr_student_list)
        ->addColumn('student_profile', function ($arr_student_list)
        {
            $encrypted_student_id = get_encrypted_value($arr_student_list['student_id'], true);
            $profile = "";
            if($arr_student_list['profile'] != ''){
                $profile = "<img src=".$arr_student_list['profile']." height='30' />";
            }   
            $name = $arr_student_list['student_name'];
            $complete = $profile."  ".$name;
            return '<a href="../student/student-profile/'.$encrypted_student_id.'" target="_blank">'.$complete.'</a>';
            
        })
        ->addColumn('checkbox', function ($arr_student_list)
        {
            return '
                <div class="checkbox" id="customid">
                    <input id="checkbox'.$arr_student_list['student_id'].'" class="checkboxes check"  type="checkbox" name="check[]" value="'.$arr_student_list['student_id'].'" >
                    <label  class="from_one1" style="margin-bottom: 4px !important;"  for="checkbox'.$arr_student_list['student_id'].'"></label></div>';
        })
        ->addColumn('class_section', function ($arr_student_list)
        {
            return $arr_student_list['student_class'].' - '.$arr_student_list['student_session'];
        })
        ->addColumn('student_name', function ($arr_student_list)
        {
            $encrypted_student_id = get_encrypted_value($arr_student_list['student_id'], true);

            return '<a href="../student/student-profile/'.$encrypted_student_id.'" target="_blank">'.$arr_student_list['student_name'].'</a>';
        })
        ->addColumn('action', function ($arr_student_list)
        {
            $encrypted_student_id = get_encrypted_value($arr_student_list['student_id'], true);

            return '<div class="dropdown">
                        <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="zmdi zmdi-label"></i>
                        <span class="caret"></span>
                        </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li><a href="save-students/' . $encrypted_student_id . '" ">Register</a></li>
                            </ul>
                        </div>';
        })->rawColumns(['action' => 'action','checkbox' => 'checkbox','student_profile' => 'student_profile'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Get Staff's page for register
     *  @Pratyush on 19 Aug 2018.
    **/
    public function addStaff(){

    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.member_staff'),
            'redirect_url'  => url('admin-panel/member/register-staff'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/member/save-staff')
        );
        return view('admin-panel.library-member.register_staff')->with($data);
    }

    /**
     *  Get Student's Data for register page(Datatables)
     *  @Pratyush on 19 Aug 2018.
    **/
    public function anyStaffData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        $staff 		= Staff::where(function($query) use ($loginInfo,$request) 
            {
                // For Roll No
                if (!empty($request) && !empty($request->has('member_staff_emp_id')))
                {
                    $query->where('staff_attendance_unique_id', "like", "%{$request->get('member_staff_emp_id')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('member_staff_name')))
                {
                    $query->where('staff_name', "like", "%{$request->get('member_staff_name')}%");
                }
            })->with('getDesignation')
        	->whereNotExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('library_members')
                        ->whereRaw('staff.staff_id = library_members.member_id')
                        ->where('library_members.library_member_type',2);
                })->get();
        
        return Datatables::of($staff)
        		->addColumn('checkbox', function ($staff)
                {
                        return '
                        <div class="checkbox" id="customid">
                            <input id="checkbox'.$staff->staff_id.'" class="checkboxes check"  type="checkbox" name="check[]" value="'.$staff->staff_id.'" >
                            <label  class="from_one1" style="margin-bottom: 4px !important;"  for="checkbox'.$staff->staff_id.'"></label></div>';
                })
                ->addColumn('profile', function ($staff)
                {
                    $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
                    $profileImg = "----";
                    if($staff->staff_image != ''){
                        $profile = check_file_exist($staff->staff_image, 'staff_image');
	                    if (!empty($profile))
	                    {
                            $staff_image = URL::to($profile);
                            $profileImg = "<img src=".$staff_image." height='30' />";
	                    }
                    }   
                    return $profileImg;
                    
                })
        		->addColumn('emp_id', function ($staff)
                {
                   return $staff->staff_attendance_unique_id;
                })
                ->addColumn('designation_name', function ($staff)
                {
                   return $staff['getDesignation']->designation_name;
                })
        		->addColumn('action', function ($staff)
                {
                    $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);

                    return '<div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                    <li><a href="save-staff/' . $encrypted_staff_id . '" ">Register</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action','checkbox' => 'checkbox','profile'=>'profile'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Add and update Member-student's data
     *  @Pratyush on 21 Aug 2018.
    **/
    public function saveStudent(Request $request, $id = NULL)
    {
    	// p($request->all());
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_title_id			= get_decrypted_value($id, true);
        $student_arr = [];
        if(!empty($id)){
        	$decrypted_student_id 	= get_decrypted_value($id, true);
        	$student_arr[0] = $decrypted_student_id;
        }else{
        	$student_arr = $request->get('check');
        }
        if(count($student_arr) != 0){
            
            $success_msg 	= 'member Added successfully!';
           
            DB::beginTransaction();
            try
            {
            	foreach ($student_arr as $value){
            		$member     					= New LibraryMember;
	          		$member->admin_id       		= $loginInfo['admin_id'];
	                $member->update_by      		= $loginInfo['admin_id'];
	                $member->member_id 				= $value;
	                $member->library_member_type	= 1;
                    $member->save();
                    
            	}
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();

        return redirect('admin-panel/member/view-students')->withSuccess($success_msg);
        }else{
        	$error_message = 'Operation failed.';
        	return redirect()->back()->withErrors($error_message);
        }	
	}	

	/**
     *  Get View Student page
     *  @Pratyush on 21 Aug 2018.
    **/
    public function viewStudent(){

    	$loginInfo 					= get_loggedin_user_data();
    	$arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.member_student_view'),
            'redirect_url'  => url('admin-panel/member/register-student'),
            'login_info'    => $loginInfo,
            'listData'		=> $listData,
        );
        return view('admin-panel.library-member.view_student')->with($data);
    }


    /**
     *  Get member Student's Data for view student page(Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function viewStudentData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        
        $arr_student_list = array();
        $arr_student 	  = LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',1);
            })->join('students', function($join) use ($request){
                $join->on('students.student_id', '=', 'library_members.member_id');
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')))
                {
                    $join->where('students.student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $join->where('students.student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')))
                {
                    $join->where('students.student_name', "like", "%{$request->get('student_name')}%");
                }
            })
            ->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'library_members.member_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')))
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->join('classes', function($join) use ($request){
                $join->on('classes.class_id', '=', 'student_academic_info.current_class_id');
            })
            ->join('sections', function($join) use ($request){
                $join->on('sections.section_id', '=', 'student_academic_info.current_section_id');
            })->get();
            
            // p($arr_student);
            if (!empty($arr_student[0]->library_member_id))
        	{
	            foreach ($arr_student as $student)
	            {
	                $student_image = "";
	                if (!empty($student->student_image))
	                {
	                    $profile = check_file_exist($student->student_image, 'student_image');
	                    if (!empty($profile))
	                    {
	                        $student_image = URL::to($profile);
	                    }
	                } else {
	                    $student_image = "";
	                }
		                $arr_student_list[] = array(
	                        'student_id'            => $student->student_id,
	                        'roll_no'				=> $student->student_roll_no,
	                        'profile'               => $student_image,
	                        'student_name'          => $student->student_name,
	                        'student_status'        => $student->student_status,
	                        'student_father_name'   => $student->student_father_name,
	                        'student_session'       => $student->section_name,
	                        'student_class'         => $student->class_name,
                            'member_id'             => $student->library_member_id,
	                    );
	            }
        	}
        	// p($arr_student_list);
        
        return Datatables::of($arr_student_list)
        		->addColumn('no_of_book_issued', function ($arr_student_list)
                {
                   return IssueBook::where('member_id',$arr_student_list['member_id'])->where('issued_book_status',1)->count();
                })
        		->addColumn('class_section', function ($arr_student_list)
                {
                   return $arr_student_list['student_class'].' - '.$arr_student_list['student_session'];
                })
                ->addColumn('student_name', function ($arr_student_list)
                {
                   $encrypted_student_id = get_encrypted_value($arr_student_list['student_id'], true);

                   return '<a href="../student/student-profile/'.$encrypted_student_id.'" target="_blank">'.$arr_student_list['student_name'].'</a>';
                })
        		->addColumn('action', function ($arr_student_list)
                {
                    $encrypted_student_id = get_encrypted_value($arr_student_list['student_id'], true);

                    return '<div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                    <li><a href="view-students/issued-books/' . $encrypted_student_id . '" ">View Issued Book</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action','student_name' => 'student_name'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Add Member-staff's data
     *  @Pratyush on 21 Aug 2018.
    **/
    public function saveStaff(Request $request, $id = NULL)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_title_id			= get_decrypted_value($id, true);
        $staff = [];
        if(!empty($id)){
        	$decrypted_staff_id 	= get_decrypted_value($id, true);
        	$staff[0] = $decrypted_staff_id;
        }else{
        	$staff = $request->get('check');
        }
        
        if(count($staff) != 0){
        	
            $success_msg 	= 'member Added successfully!';
           
            DB::beginTransaction();
            try
            {
            	foreach ($staff as $value){
            		$member     					= New LibraryMember;
	          		$member->admin_id       		= $loginInfo['admin_id'];
	                $member->update_by      		= $loginInfo['admin_id'];
	                $member->member_id 				= $value;
	                $member->library_member_type	= 2;
	                $member->save();	
            	}
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();

        return redirect('admin-panel/member/view-staff')->withSuccess($success_msg);
        }else{
        	$error_message = 'Operation failed.';
        	return redirect()->back()->withErrors($error_message);
        }	
	}	

	/**
     *  Get member staff's Data for view staff page(Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function viewStaffData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        
        $arr_student_list = array();
        $arr_staff 	  = LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',2);

            })->join('staff', function($join) use ($request){
                $join->on('staff.staff_id', '=', 'library_members.member_id');
                // For staff name
                if (!empty($request) && !empty($request->has('member_staff_name')))
                {
                    $join->where('staff.staff_name', "like", "%{$request->get('member_staff_name')}%");
                }
            })->join('designations', function($join) use ($request){
                $join->on('designations.designation_id', '=', 'staff.designation_id');
            })
            ->get();
            
        return Datatables::of($arr_staff)
        		->addColumn('no_of_book_issued', function ($arr_staff)
                {
                   return IssueBook::where('member_id',$arr_staff['library_member_id'])->where('issued_book_status',1)->count();
                })
        		->addColumn('action', function ($arr_staff)
                {
                    $encrypted_staff_id = get_encrypted_value($arr_staff->staff_id, true);

                    return '<div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                     <li><a href="view-staff/issued-books/' . $encrypted_staff_id . '" ">View Issued Book</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action','student_name' => 'student_name'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Get View Staff page
     *  @Pratyush on 21 Aug 2018.
    **/
    public function viewStaff(){

    	$loginInfo 					= get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.member_staff_view'),
            'redirect_url'  => url('admin-panel/member/register-student'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.library-member.view_staff')->with($data);
    }

    /**
     *  View page for Student Issued Books
     *  @Pratyush on 22 Aug 2018
    **/
    public function studentIssuedBooks($id = false)
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.member_issued_books'),
            'redirect_url'  => url('admin-panel/member/view-students'),
            'login_info'    => $loginInfo,
            'student_id'    => $id
        );
        return view('admin-panel.library-member.view_student_issued_books')->with($data);
    }

    /**
     *  Get member Student's issued books details (Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function studentIssuedBooksData(Request $request)
    {

        $loginInfo            = get_loggedin_user_data();
        $arr_books            = [];
        $books_details        = [];
        $decrypted_student_id = get_decrypted_value($request->get('student_id'), true);
        $arr_books      = Student::where(function($query) use ($loginInfo,$request,$decrypted_student_id) 
            {
                // For student id
                $query->where('students.student_id',$decrypted_student_id);
                $query->where('students.student_status',1);
            })->join('library_members', function($join) use ($request){
                $join->on('library_members.member_id', '=', 'students.student_id');
               
                    $join->where('library_members.library_member_type',1);

            })->first();
            
            if($arr_books->library_member_id != ''){
                $books_details = IssueBook::where(function($query) use ($loginInfo,$request,$arr_books) 
                {
                    // for book status
                    $query->where('issued_books.issued_book_status',1);
                    // for book status
                    $query->where('issued_books.member_id',$arr_books->library_member_id);

                })->join('books', function($join) use ($request){
                    $join->on('books.book_id', '=', 'issued_books.book_id');
                })->get();
            }
        return Datatables::of($books_details)
                ->addColumn('book_name', function ($books_details)
                {
                   return $books_details->book_name;
                })
                ->addColumn('issue_from', function ($books_details)
                {
                   return date('d M Y',strtotime($books_details->issued_from_date));
                })
                ->addColumn('issue_to', function ($books_details)
                {
                   return date('d M Y',strtotime($books_details->issued_to_date));
                })->addIndexColumn()->make(true);
    }

    /**
     *  View page for Staff Issued Books
     *  @Pratyush on 22 Aug 2018
    **/
    public function staffIssuedBooks($id = false)
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.member_issued_books'),
            'redirect_url'  => url('admin-panel/member/view-staff'),
            'login_info'    => $loginInfo,
            'staff_id'      => $id
        );
        return view('admin-panel.library-member.view_staff_issued_books')->with($data);
    }

    /**
     *  Get member Student's issued books details (Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function staffIssuedBooksData(Request $request)
    {
        $loginInfo            = get_loggedin_user_data();
        $arr_books            = [];
        $books_details        = [];
        $decrypted_staff_id = get_decrypted_value($request->get('staff_id'), true);
        $arr_books      = Staff::where(function($query) use ($loginInfo,$request,$decrypted_staff_id) 
            {
                // For student id
                $query->where('staff.staff_id',$decrypted_staff_id);

            })->join('library_members', function($join) use ($request){
                $join->on('library_members.member_id', '=', 'staff.staff_id');
               
                    $join->where('library_members.library_member_type',2);

            })->first();

            if($arr_books->library_member_id != ''){
                $books_details = IssueBook::where(function($query) use ($loginInfo,$request,$arr_books) 
                {
                    // for book status

                    $query->where('issued_books.issued_book_status',1);

                    // for book status
                    $query->where('issued_books.member_id',$arr_books->library_member_id);

                })->join('books', function($join) use ($request){
                    $join->on('books.book_id', '=', 'issued_books.book_id');
                })->get();
            }
        
        return Datatables::of($books_details)
                ->addColumn('book_name', function ($books_details)
                {
                   return $books_details->book_name;
                })
                ->addColumn('issue_from', function ($books_details)
                {
                   return date('d M Y',strtotime($books_details->issued_from_date));
                })
                ->addColumn('issue_to', function ($books_details)
                {
                   return date('d M Y',strtotime($books_details->issued_to_date));
                })->addIndexColumn()->make(true);
    }

}