<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\Fine; // Model
use App\Model\FeesCollection\FineDetails; // Model
use Yajra\Datatables\Datatables;

class FineController extends Controller
{
    /**
     *  View page for Fine
     *  @shree on 8 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $fine = [];
        $data = array(
            'page_title'    => trans('language.view_fine'),
            'redirect_url'  => url('admin-panel/fine/view-fine'),
            'login_info'    => $loginInfo,
            'fine'     => $fine
        );
        return view('admin-panel.fine.index')->with($data);
    }

    /**
     *  Add page for Fine
     *  @Shree on 8 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    	= [];
        $fine      	= [];
        $loginInfo 		= get_loggedin_user_data();
        
        $arr_heads              = [];
        if (!empty($id))
        {
            $decrypted_fine_id 	= get_decrypted_value($id, true);
            $fine      		= Fine::where('fine_id', $decrypted_fine_id)->with('getFineDetails')->get();
            $fine = isset($fine[0]) ? $fine[0] : [];
            if (!$fine)
            {
                return redirect('admin-panel/fine/add-fine')->withError('Fine not found!');
            }
           
            $detail_list = [];
            // p($fine['getFineDetails']);
            foreach ($fine['getFineDetails'] as $detail)
            {
                $detail_data['fine_detail_id']      = $detail['fine_detail_id'];
                $detail_data['fine_id']             = $detail['fine_id'];
                $detail_data['fine_detail_days']    = $detail['fine_detail_days'];
                $detail_data['fine_detail_amt']     = $detail['fine_detail_amt'];
                $detail_list[] = $detail_data;
            }
            $fine->fineDetails = $detail_list;
            $page_title             	= trans('language.edit_fine');
            $encrypted_fine_id       = get_encrypted_value($fine->fine_id, true);
            $save_url               	= url('admin-panel/fine/save/' . $encrypted_fine_id);
            $submit_button          	= 'Update';
            $arr_heads = get_all_heads($fine->fine_for);
        }
        else
        {
            $page_title    = trans('language.add_fine');
            $save_url      = url('admin-panel/fine/save');
            $submit_button = 'Save';
        }
        $fine['arr_head_type']  = \Config::get('custom.head_type');
        $fine['arr_heads']      = add_blank_option($arr_heads, 'Select Head');
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'fine' 		=> $fine,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/fine/view-fine'),
        );
        return view('admin-panel.fine.add')->with($data);
    }

    /**
     *  Add and update fine's data
     *  @Shree on 8 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo          = get_loggedin_user_data();
        $decrypted_fine_id	= get_decrypted_value($id, true);
        $admin_id           = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $fine = Fine::find($decrypted_fine_id);
            $admin_id = $fine['admin_id'];
            if (!$fine)
            {
                return redirect('/admin-panel/fine/add-fine/')->withError('Fine not found!');
            }
            $success_msg = 'Fine updated successfully!';
        }
        else
        {
            $fine     = New Fine;
            $success_msg = 'Fine saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'fine_name' => 'required|unique:fine,fine_name,' . $decrypted_fine_id . ',fine_id',
            'fine_for'  => 'required',
            'head_id'   => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $fine->admin_id     = $admin_id;
                $fine->update_by    = $loginInfo['admin_id'];
                $fine->fine_name    = Input::get('fine_name');
                $fine->fine_for 	= Input::get('fine_for');
                $fine->head_id 	    = Input::get('head_id');
                $fine->save();
              
                foreach($request->get('fineDetails') as $details){ 
                    if(isset($details['fine_detail_days']) && !empty($details['fine_detail_days'])) {
                        if(isset($details['fine_detail_id']) ) {
                           
                            $detail_update                            = FineDetails::where('fine_detail_id', $details['fine_detail_id'])->first();
                            $detail_update->admin_id            = $admin_id;
                            $detail_update->update_by           = $loginInfo['admin_id'];
                            $detail_update->fine_id             = $fine->fine_id;
                            $detail_update->fine_detail_days    = $details['fine_detail_days'];
                            $detail_update->fine_detail_amt     = $details['fine_detail_amt'];
                            $detail_update->save();
                        } else {
                            $detail_update   = new FineDetails();
                            $detail_update->admin_id            = $admin_id;
                            $detail_update->update_by           = $loginInfo['admin_id'];
                            $detail_update->fine_id             = $fine->fine_id;
                            $detail_update->fine_detail_days    = $details['fine_detail_days'];
                            $detail_update->fine_detail_amt     = $details['fine_detail_amt'];
                            $detail_update->save();
                        }
                    }
                }
                
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/fine/view-fine')->withSuccess($success_msg);
    }

    /**
     *  Get fine's Data for view page(Datatables)
     *  @shree on 8 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $fine  	= Fine::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('fine_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('fine_id', 'DESC')->get();

        return Datatables::of($fine)
        ->addColumn('fees_head', function ($fine)
        {
            $type = $fine->fine_for;
            $headName = get_head_name($type,$fine->head_id);
            return $headName; 
        })
        ->addColumn('fine_for', function ($fine)
        {
            $type  = \Config::get('custom.head_type');
            return $type[$fine->fine_for]; 
        })
        ->addColumn('details', function ($fine)
        {
            $no = 0;
            $no = FineDetails::where(function($query) use ($fine) 
            {
                $query->where('fine_id', "=", $fine->fine_id);
               
            })->get()->count();  
            if($no == 0) {
                return "No detail available.";
            } else {      
                return '<button type="button" class="btn btn-raised btn-primary details"  detail-id="'.$fine->fine_id.'" >
           Details
            </button>';
            }
        })
        ->addColumn('action', function ($fine)
        {

            $encrypted_fine_id = get_encrypted_value($fine->fine_id, true);
            if($fine->fine_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="fine-status/'.$status.'/' . $encrypted_fine_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-fine/' . $encrypted_fine_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-fine/' . $encrypted_fine_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'details'=> 'details'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy fine's data
     *  @Shree on 8 Oct 2018.
    **/
    public function destroy($id)
    {
        $fine_id 	= get_decrypted_value($id, true);
        $fine 		= Fine::find($fine_id);
        
        $success_msg = $error_message =  "";
        if ($fine)
        {
            DB::beginTransaction();
            try
            {
                $fine->delete();
                $success_msg = "Fine deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/fine/view-fine')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/fine/view-fine')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Fine not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change fine's status
     *  @Shree on 8 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $fine_id       = get_decrypted_value($id, true);
        $fine          = Fine::find($fine_id);
        if ($fine)
        {
            $fine->fine_status  = $status;
            $fine->save();
            $success_msg = "Fine status update successfully!";
            return redirect('admin-panel/fine/view-fine')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Fine not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Fine detail's data
     *  @Shree on 8 Oct 2018.
    **/
    public function destroyDetail($id)
    {
        $fine_detail_id    = $id;
        $details       = FineDetails::find($fine_detail_id);
        $success_msg = $error_message =  "";
        if ($details)
        {
            DB::beginTransaction();
            try
            {
                $details->delete();
                $success_msg = "Fine detail deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }else{
            $error_message = "Fine details not found!";
            return $error_message;
        }
    }

    /**
     *  Get fine details's Data for view page(Datatables)
     *  @shree on 8 Oct 2018.
    **/
    public function fine_details(Request $request)
    {
        $details  	= FineDetails::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('fine_id', "=", $request->get('id'));
            }
        })->orderBy('fine_detail_id', 'DESC')->get();

        return Datatables::of($details)
        
        ->addColumn('action', function ($details)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini fine-detail-records" data-toggle="tooltip" title="Delete" fine-detail-record="'.$details->fine_detail_id.'" fine-record="'.$details->fine_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }
}
