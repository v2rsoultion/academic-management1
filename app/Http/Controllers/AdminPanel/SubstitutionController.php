<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;
use App\Model\Staff\StaffLeaveApplication; // Model
use App\Model\Staff\StaffSubstitution; // Model
use App\Model\TimeTable\TimeTable; // TimeTable Model
use App\Model\TimeTable\TimeTableMap; // TimeTableMap Model
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model
use Redirect;
use DateInterval;
use DatePeriod;
use DateTime;
use Carbon\CarbonPeriod;

class SubstitutionController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('19',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Manage Substitution
     *  @Shree on 1 April 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_substitution'),
            'redirect_url'  => url('admin-panel/substitute-management/manage-substitution'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.manage-substitution.index')->with($data);
    }

    /**
     *  Get leaves's Data for view page(Datatables)
     *  @Shree on 1 April 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $session    = get_current_session();
        $leave_application      = StaffLeaveApplication::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', $session['session_id']);
            $query->where('staff_leave_status', 1);
        })
        ->whereHas('getStaff', function($query) use($request) {
            $query->where('teaching_status',1);
        })
        ->with(['getStaff' => function($query) use($request) {
            $query->where('teaching_status',1);
            $query->with('getClassAllocation.getCurrentClass');
            $query->with('getClassAllocation.getCurrentSection');
        }])
        ->orderBy('staff_leave_id', 'DESC')->get(); 

        return Datatables::of($leave_application)
        ->addColumn('staff_name', function ($leave_application)
        {	
            return $leave_application['getStaff']->staff_name;
            
        })
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->staff_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->staff_leave_to_date));
        })
        ->addColumn('class_name', function ($leave_application)
        {	
            $class_name = '-- ';
            if(!empty($leave_application['getStaff']['getClassAllocation'])){
                $class_name = $leave_application['getStaff']['getClassAllocation']['getCurrentClass']->class_name.' - '.$leave_application['getStaff']['getClassAllocation']['getCurrentSection']->section_name;
            }
            return $class_name;
        })
        ->addColumn('class_wise', function ($leave_application)
        {
            $type = '1';
            $class_wise = $this->get_all_substitute($leave_application->staff_leave_id,$type);
            return $class_wise;
        })
        ->addColumn('action', function ($leave_application)
        {
            $encrypted_staff_leave_id = get_encrypted_value($leave_application->staff_leave_id, true);
            return '
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Allocate" href="'.url('/admin-panel/substitute-management/allocate/' . $encrypted_staff_leave_id.' ').'">Allocate</a>
                    </li>
                    <li>
                        <a title="View Students" href="'.url('/admin-panel/substitute-management/view-allocations/' . $encrypted_staff_leave_id.' ').'">View Allocations</a>
                    </li>
                </ul>
            </div>';
            
        })
        ->rawColumns(['action' => 'action','class_wise' => 'class_wise','class_name'=>'class_name','staff_name'=>'staff_name','date_range'=>'date_range'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Allocate Page
     *  @Shree on 2 April 2019
    **/
    public function allocate($id,$sub_id = NULL)
    {
        $data  = $substitute = [];
        $loginInfo 		= get_loggedin_user_data();
        $session        = get_current_session();
        $decrypted_staff_leave_id 	= get_decrypted_value($id, true);
        
        $arr_reverse_week_days = \Config::get('custom.reverse_week_days');
        $class_arr = $arr_section = $arr_lecture = $staff_apoint = $pay_extra = $arr_staff = [];
        if (!empty($sub_id))
        {
            $decrypted_substitute_id = get_decrypted_value($sub_id, true);
            $substitute                  = StaffSubstitution::find($decrypted_substitute_id);
            if (!$substitute)
            {
                return redirect()->back()->withError('State not found!');
            }
            $substitute->lecture_no1 = $substitute->day.'-'.$substitute->lecture_no;
            $class_id = $substitute->class_id;
            $staff_id = $substitute->leave_staff_id;
            $section_id = $substitute->section_id;

            $start    = (new DateTime($substitute->from_date));
            $end      = (new DateTime($substitute->to_date));
            $interval = DateInterval::createFromDateString('1 day');
            $period = CarbonPeriod::create($start,$end);
            $dateRange = [];
            $weekdays = [];
            foreach ($period as $dt) {
                $dateRange[$dt->format("Y-m-d")] = $dt->format("Y-m-d");
                $weekdays[] = $arr_reverse_week_days[$dt->format("l")];
            }

            // Get Sections
            $time_table_map = TimeTableMap::
            join('time_tables', function($join) use($class_id){
                $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
                $join->where('class_id',$class_id);
            })->where('staff_id', '=', $staff_id)
            ->whereIn('time_table_map.day', $weekdays)
            ->where('time_table_map.session_id', '=', $session['session_id'])
            ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
            ->orderBy('map_lecture_no', 'ASC')
            ->with('getSubject')->with('getStaff')
            ->with('getClass')
            ->with('getSection')
            ->get()
            ->toArray();

            foreach($time_table_map as $map){
                $arr_section[$map['section_id']] = $map['get_section']['section_name'];
            }

            // Get Lectures
            $arr_week_days = \Config::get('custom.week_days');

            $time_table_map1 = TimeTableMap::
            join('time_tables', function($join) use ($class_id,$section_id){
                $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
                $join->where('class_id',$class_id);
                $join->where('section_id',$section_id);
            })->where('staff_id', '=', $staff_id)
            ->whereIn('time_table_map.day', $weekdays)
            ->where('time_table_map.session_id', '=', $session['session_id'])
            ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
            ->orderBy('day', 'ASC')
            ->get()
            ->toArray();

            foreach($time_table_map1 as $map){
                $arr_lecture[$map[day].'-'.$map['map_lecture_no']] = $arr_week_days[$map[day]].'-'.$map['map_lecture_no'].' Lecture';
            }

            // Get Staff

            $lecture_no = $substitute->lecture_no;
            $day = $substitute->day;
            $time_table_map2 = TimeTableMap::
            join('time_tables', function($join) use ($class_id,$section_id){
                $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
                $join->where('class_id',$class_id);
                $join->where('section_id',$section_id);
            })->where('staff_id', '=', $staff_id)
            ->where('time_table_map.day', '=', $day)
            ->where('time_table_map.lecture_no', '=', $lecture_no)
            ->where('time_table_map.session_id', '=', $session['session_id'])
            ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
            ->orderBy('day', 'ASC')
            ->get()
            ->toArray();
            $time_table_map2 = isset($time_table_map2[0]) ? $time_table_map2[0] : [];
            
            $start_time = $time_table_map2['start_time'];
            $end_time = $time_table_map2['end_time'];

            $get_staff = $this->get_subject_staff($class_id,$section_id);
            $exist_staff_id = $staff_id;
            
            $staffIds = [];
            foreach($get_staff as $staff_id){
                
                if($exist_staff_id != $staff_id){
                    $all_staff_map = $this->check_staff_availability($day,$staff_id);
                } 
                if(!empty($all_staff_map)){
                    foreach($all_staff_map as $map){
                        
                        if( ( ( (strtotime($start_time) > strtotime($map['start_time'])) && (strtotime($start_time) >= strtotime($map['end_time']))  ) || ( (strtotime($start_time) < strtotime($map['start_time'])) &&  (strtotime($end_time) >= strtotime($map['start_time'])) ) ) || ( (strtotime($start_time) < strtotime($map['start_time'])) && (strtotime($end_time) < strtotime($map['start_time']))  )  ||  ( (strtotime($start_time) < strtotime($map['start_time'])) && (strtotime($end_time) > strtotime($map['start_time'])) && ( (strtotime($end_time) > strtotime($map['end_time'])) &&  (strtotime($end_time) == strtotime($map['start_time'])) )  )  ){

                            if( ( strtotime($start_time) <  strtotime($map['start_time']))  && ( strtotime($end_time) > strtotime($map['start_time'])) && ( strtotime($start_time) <  strtotime($map['end_time'])) &&  ( strtotime($end_time) <=  strtotime($map['end_time']))){
                                /// 
                            } else {
                                $staffIds[] = $map['staff_id'];
                            }
                        } else {
                            // Faill case
                            $staffIds[] = '';
                        }
                    }
                } else {
                    if($exist_staff_id != $staff_id){
                        $staffIds[] = $staff_id;
                    }
                }
            }
            $arr_staff = get_multiple_staff($staffIds);

            $page_title               = trans('language.edit_allocate');
            $save_url                 = url('admin-panel/substitute-management/save-allocate/' . $id.'/'.$sub_id);
            $submit_button            = 'Update';
            
        } else {
            $page_title             	= trans('language.allocate');
            $save_url               	= url('admin-panel/substitute-management/save-allocate/' . $id);
            $submit_button          	= 'Save';
        }
        $leave_application          = StaffLeaveApplication::where(function($query) use ($decrypted_staff_leave_id,$session) 
        {
            $query->where('staff_leave_id', $decrypted_staff_leave_id);
            $query->where('session_id', $session['session_id']);
            $query->where('staff_leave_status', 1);
        })
        // ->with('getStaff.getClassAllocation')
        ->whereHas('getStaff', function($query) use($request) {
            $query->where('teaching_status',1);
        })
        ->with(['getStaff' => function($query) use($request) {
            $query->where('teaching_status',1);
            $query->with('getClassAllocation.getCurrentClass');
            $query->with('getClassAllocation.getCurrentSection');
        }])
        ->orderBy('staff_leave_id', 'DESC')->first();
        
        $staff_id = $leave_application->staff_id;
        $start    = (new DateTime($leave_application->staff_leave_from_date));
        $end      = (new DateTime($leave_application->staff_leave_to_date));
        $interval = DateInterval::createFromDateString('1 day');
        // $period   = new DatePeriod($start, $interval, $end);
        $period = CarbonPeriod::create($start,$end);
        $dateRange = [];
        $weekdays = [];
        foreach ($period as $dt) {
            $dateRange[$dt->format("Y-m-d")] = $dt->format("Y-m-d");
            $weekdays[] = $arr_reverse_week_days[$dt->format("l")];
        }

        $time_table_map = TimeTableMap::where(function($query) use ($weekdays,$staff_id,$session) 
        {
            $query->whereIn('time_table_map.day',$weekdays);
            $query->where('time_table_map.staff_id',$staff_id);
            $query->where('time_table_map.session_id',$session['session_id']);
        })
        ->join('time_tables', function($join){
            $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
        })
        ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
        ->orderBy('map_lecture_no', 'ASC')
        ->with('getSubject')->with('getStaff')
        ->with('getClass')
        ->with('getSection')
        ->get()
        ->groupBy('class_id')
        ->toArray();

        
        foreach ($time_table_map as $key => $map) {
            if(!empty($map)){
                $class_arr[$map[0]['get_class']['class_id']] = $map[0]['get_class']['class_name'];
            }
        }
        
        $pay_extra      =  \Config::get('custom.pay_extra');
        $staff_apoint   =  \Config::get('custom.staff_apoint');

        $listData['staff_id']       = $staff_id;
        $listData['id']             = $id;
        $listData['class_arr']      = add_blank_option($class_arr, "Select Class");
        $listData['dateRange']      = add_blank_option($dateRange, "Select Date");
        $listData['arr_section']    = add_blank_option($arr_section, "Select Section");
        $listData['arr_lecture']    = add_blank_option($arr_lecture, "Select Lecture");
        $listData['arr_staff']      = add_blank_option($arr_staff, "Select Staff");
        $listData['pay_extra']      = $pay_extra;
        $listData['staff_apoint']   = $staff_apoint;

        // p($leave_application);
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'listData' 		    => $listData,
            'login_info'    	=> $loginInfo,
            'substitute'        => $substitute,
            'leave_application' => $leave_application,
            'redirect_url'  	=> url('admin-panel/substitute-management/view-allocations/'. $id),
        );
        // p($data);
        return view('admin-panel.manage-substitution.add')->with($data);
    }

    /**
     *  Add & Update allocation
     *  @Shree on 2 April 2019.
    **/
    public function save_allocate(Request $request, $id,$subid = NULL)
    {
        $session    = get_current_session();
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_staff_leave_id	= get_decrypted_value($id, true);
        $decrypted_substitute_id	= get_decrypted_value($subid, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($subid))
        {
            $substitute = StaffSubstitution::find($decrypted_substitute_id);

            if (!$substitute)
            {
                return redirect()->back()->withError('Substitution not found!');
            }
            $admin_id = $substitute['admin_id'];
            $success_msg = 'Substitution updated successfully!';
        }
        else
        {
            $substitute    = New StaffSubstitution;
            $success_msg 	= 'Substitution saved successfully!';
        }
        $lectcureInfo = explode('-',$request->get('lecture_no'));
        $checkSubstituteExistance = $this->checkSubstituteExistance($request,$subid);
        if($checkSubstituteExistance > 0){
            return redirect()->back()->withInput()->withErrors('Substitution dates already exist!');
        }

        $validatior = Validator::make($request->all(), [
            'from_date'   	=> 'required',
            'to_date'   	=> 'required',
            'class_id'   	=> 'required',
            'section_id'    => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $substitute->admin_id       		 = $admin_id;
                $substitute->update_by      		 = $loginInfo['admin_id'];
                $substitute->session_id      		 = $session['session_id'];
                $substitute->ref_leave_id 	 	     = $decrypted_staff_leave_id;
                $substitute->leave_staff_id 	 = Input::get('leave_staff_id');
                $substitute->class_id 	 = Input::get('class_id');
                $substitute->section_id 	 = Input::get('section_id');
                $substitute->lecture_no 	 = $lectcureInfo[1];
                $substitute->day 	 = $lectcureInfo[0];
                $substitute->staff_id 	 = Input::get('staff_id');
                $substitute->from_date 	 = Input::get('from_date');
                $substitute->to_date 	 = Input::get('to_date');
                $substitute->as_class_teacher 	 = Input::get('as_class_teacher');
                $substitute->pay_extra 	 = Input::get('pay_extra');
                $substitute->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/substitute-management/view-allocations/'.$id)->withSuccess($success_msg);
    }

    /**
     *  Manage Substitution
     *  @Shree on 1 April 2018
    **/
    public function view_allocations($id)
    {
        $session        = get_current_session();
        $decrypted_staff_leave_id 	= get_decrypted_value($id, true);
        $loginInfo = get_loggedin_user_data();
        $leave_application          = StaffLeaveApplication::where(function($query) use ($decrypted_staff_leave_id,$session) 
        {
            $query->where('staff_leave_id', $decrypted_staff_leave_id);
            $query->where('session_id', $session['session_id']);
            $query->where('staff_leave_status', 1);
        })
        // ->with('getStaff.getClassAllocation')
        ->whereHas('getStaff', function($query) use($request) {
            $query->where('teaching_status',1);
        })
        ->with(['getStaff' => function($query) use($request) {
            $query->where('teaching_status',1);
            $query->with('getClassAllocation.getCurrentClass');
            $query->with('getClassAllocation.getCurrentSection');
        }])
        ->orderBy('staff_leave_id', 'DESC')->first();
        $data = array(
            'page_title'    => trans('language.view_allocations'),
            'redirect_url'  => url('admin-panel/substitute-management/view-allocations/'.$id),
            'login_info'    => $loginInfo,
            'id'            => $id,
            'leave_application' => $leave_application,
            'ref_leave_id'  => $decrypted_staff_leave_id
        );
        return view('admin-panel.manage-substitution.view_allocations')->with($data);
    }

    /**
     *  allocation Data
     *  @Shree on 1 April 2018.
    **/
    public function allocationData(Request $request)
    {
        $pay_extra      =  \Config::get('custom.pay_extra');

        $loginInfo 	= get_loggedin_user_data();
        $session    = get_current_session();
        $substitution      = StaffSubstitution::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', $session['session_id']);
            $query->where('ref_leave_id', $request->get('ref_leave_id'));
        })
        ->with('getStaff','getClass','getSection','getLeaveInfo')
        ->orderBy('substitute_id', 'DESC')->get(); 
        // p($substitution);
        return Datatables::of($substitution) 
        ->addColumn('class_lecture', function ($substitution) 
        {
            return $substitution['getClass']->class_name.' - '.$substitution['getSection']->section_name.' - '.$substitution->lecture_no.' Lecture';
            
        })
        ->addColumn('staff_name', function ($substitution) 
        {	
            return $substitution['getStaff']->staff_name;
            
        })
        ->addColumn('date_range', function ($substitution)
        {	
            return date('d M Y',strtotime($substitution->from_date)).' - '.date('d M Y',strtotime($substitution->to_date));
        })
        ->addColumn('pay_extra', function ($substitution) use ($pay_extra) 
        {	
            return $pay_extra[$substitution->pay_extra];
            
        })
        ->addColumn('action', function ($substitution)
        {
            $encrypted_ref_leave_id = get_encrypted_value($substitution->ref_leave_id, true);
            $encrypted_substitute_id = get_encrypted_value($substitution->substitute_id, true);
            return '
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/substitute-management/allocate/' . $encrypted_ref_leave_id.'/' . $encrypted_substitute_id.' ').'" ><i class="zmdi zmdi-edit"></i></a></div>
            ';
            
        })
        ->rawColumns(['action' => 'action','class_lecture' => 'class_lecture','pay_extra'=>'pay_extra','staff_name'=>'staff_name','date_range'=>'date_range'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Book vendor's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function destroy($id)
    {
        $book_vendor_id 		= get_decrypted_value($id, true);
        $book_vendor 		  	= BookVendor::find($book_vendor_id);
        
        $success_msg = $error_message =  "";
        if ($book_vendor)
        {
            DB::beginTransaction();
            try
            {
                $book_vendor->delete();
                $success_msg = "Book vendor deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/book-vendor/view-book-vendors')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/book-vendor/view-book-vendors')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Book vendor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get sections data according class
     *  @Shree on 2 April 2019
    **/
    public function getSectionData()
    {
        $session    = get_current_session();
        $staff_id = Input::get('staff_id');
        $class_id = Input::get('class_id');
        $from_date = Input::get('from_date');
        $to_date = Input::get('to_date');
        $arr_reverse_week_days = \Config::get('custom.reverse_week_days');
        $start    = (new DateTime($from_date));
        $end      = (new DateTime($to_date));
        $interval = DateInterval::createFromDateString('1 day');
        $period = CarbonPeriod::create($start,$end);
        $dateRange = [];
        $weekdays = [];
        foreach ($period as $dt) {
            $dateRange[$dt->format("Y-m-d")] = $dt->format("Y-m-d");
            $weekdays[] = $arr_reverse_week_days[$dt->format("l")];
        }

        $time_table_map = TimeTableMap::
        join('time_tables', function($join) use($class_id){
            $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
            $join->where('class_id',$class_id);
        })->where('staff_id', '=', $staff_id)
        ->whereIn('time_table_map.day', $weekdays)
        ->where('time_table_map.session_id', '=', $session['session_id'])
        ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
        ->orderBy('map_lecture_no', 'ASC')
        ->with('getSubject')->with('getStaff')
        ->with('getClass')
        ->with('getSection')
        ->get()
        ->toArray();

        $section = [];
        foreach($time_table_map as $map){
            $section[$map['section_id']] = $map['get_section']['section_name'];
        }

        $data = view('admin-panel.class-teacher-allocation.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get sections data according class
     *  @Shree on 2 April 2019
    **/
    public function getLectureData()
    {
        $arr_week_days = \Config::get('custom.week_days');
        $arr_reverse_week_days = \Config::get('custom.reverse_week_days');
        $session    = get_current_session();
        $staff_id = Input::get('staff_id');
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $from_date = Input::get('from_date');
        $to_date = Input::get('to_date');
        $start    = (new DateTime($from_date));
        $end      = (new DateTime($to_date));
        $interval = DateInterval::createFromDateString('1 day');
        $period = CarbonPeriod::create($start,$end);
        $dateRange = [];
        $weekdays = [];
        foreach ($period as $dt) {
            $dateRange[$dt->format("Y-m-d")] = $dt->format("Y-m-d");
            $weekdays[] = $arr_reverse_week_days[$dt->format("l")];
        }

        $time_table_map = TimeTableMap::
        join('time_tables', function($join) use ($class_id,$section_id){
            $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
            $join->where('class_id',$class_id);
            $join->where('section_id',$section_id);
        })->where('staff_id', '=', $staff_id)
        ->whereIn('time_table_map.day', $weekdays)
        ->where('time_table_map.session_id', '=', $session['session_id'])
        ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
        ->orderBy('day', 'ASC')
        ->get()
        ->toArray();

        $lectures = [];
        foreach($time_table_map as $map){
            $lectures[$map[day].'-'.$map['map_lecture_no']] = $arr_week_days[$map[day]].'-'.$map['map_lecture_no'].' Lecture';
        }

        $data = view('admin-panel.manage-substitution.ajax-lecture',compact('lectures'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get available Staff
     *  @Shree on 2 April 2019
    **/
    public function getStaffData()
    {
        $lectureInfo = explode("-",Input::get('lecture_no'));

        $session    = get_current_session();
        $staff_id = Input::get('staff_id');
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $day = $lectureInfo[0];
        $lecture_no = $lectureInfo[1];

        $time_table_map = TimeTableMap::
        join('time_tables', function($join) use ($class_id,$section_id){
            $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
            $join->where('class_id',$class_id);
            $join->where('section_id',$section_id);
        })->where('staff_id', '=', $staff_id)
        ->where('time_table_map.day', '=', $day)
        ->where('time_table_map.lecture_no', '=', $lecture_no)
        ->where('time_table_map.session_id', '=', $session['session_id'])
        ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
        ->orderBy('day', 'ASC')
        ->get()
        ->toArray();
        $time_table_map = isset($time_table_map[0]) ? $time_table_map[0] : [];
        
        $start_time = $time_table_map['start_time'];
        $end_time = $time_table_map['end_time'];

        $get_staff = $this->get_subject_staff($class_id,$section_id);
        $exist_staff_id = $staff_id;
        $staffIds = [];
        foreach($get_staff as $staff_id){
            
            if($exist_staff_id != $staff_id){
                $all_staff_map = $this->check_staff_availability($day,$staff_id);
            } 
            if(!empty($all_staff_map)){
                foreach($all_staff_map as $map){
                    
                    if( ( ( (strtotime($start_time) > strtotime($map['start_time'])) && (strtotime($start_time) >= strtotime($map['end_time']))  ) || ( (strtotime($start_time) < strtotime($map['start_time'])) &&  (strtotime($end_time) >= strtotime($map['start_time'])) ) ) || ( (strtotime($start_time) < strtotime($map['start_time'])) && (strtotime($end_time) < strtotime($map['start_time']))  )  ||  ( (strtotime($start_time) < strtotime($map['start_time'])) && (strtotime($end_time) > strtotime($map['start_time'])) && ( (strtotime($end_time) > strtotime($map['end_time'])) &&  (strtotime($end_time) == strtotime($map['start_time'])) )  )  ){

                        if( ( strtotime($start_time) <  strtotime($map['start_time']))  && ( strtotime($end_time) > strtotime($map['start_time'])) && ( strtotime($start_time) <  strtotime($map['end_time'])) &&  ( strtotime($end_time) <=  strtotime($map['end_time']))){
                            /// 
                        } else {
                            $staffIds[] = $map['staff_id'];
                        }
                    } else {
                        // Faill case
                        $staffIds[] = '';
                    }
                }
            } else {
                if($exist_staff_id != $staff_id){
                    $staffIds[] = $staff_id;
                }
            }
        }
        $staff = get_multiple_staff($staffIds);
        $data = view('admin-panel.staff.ajax-staff-select',compact('staff'))->render();
        return response()->json(['options'=>$data]);
    }

    function get_subject_staff($class_id,$section_id)
    {
        $staff     = [];
        $arr_staff     = SubjectTeacherMapping::where(function($query) use ($class_id,$section_id) 
        {
            if (!empty($class_id) &&  $class_id != null){
                $query->where('class_id', '=',$class_id);
            }
            if (!empty($section_id) &&  $section_id != null){
                $query->where('section_id', '=',$section_id);
            }
        })->select('staff_ids')->groupBy('staff_ids')->get()->toArray();
        foreach($arr_staff as $staffData){
            $staff[] = $staffData['staff_ids'];
        }
        return $staff;
    }

    // Check availability for time table
    function check_staff_availability($day,$staff_id){
        $staff     = [];
        $session          = get_current_session();
        $arr_staff     = TimeTableMap::where(function($query) use ($day,$session,$staff_id) 
        {
            $query->where('session_id', '=',$session['session_id']);
            $query->where('staff_id', '=',$staff_id);
            $query->where('day', '=',$day);
            $query->where('expire_status', '=',1);
        })->get()->toArray();
        return $arr_staff;
    }

    public function checkSubstituteExistance(Request $request,$subid = Null){
        
        $decrypted_substitute_id	= get_decrypted_value($subid, true);
        $begin = $request->get('from_date');
        $end = $request->get('to_date');
        $lectcureInfo = explode('-',$request->get('lecture_no'));
        $total = 0;
        $total = StaffSubstitution::where('leave_staff_id', $request->get('leave_staff_id')) // probably scoping to a single user??
        ->where(function ($query) use ($begin, $end,$request,$lectcureInfo,$decrypted_substitute_id) {
            $query->where(function ($q) use ($begin, $end) {
                $q->where('from_date', '>=', $begin)
                ->where('from_date', '<', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('from_date', '<=', $begin)
                ->where('to_date', '>', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('to_date', '>', $begin)
                ->where('to_date', '<=', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('from_date', '>=', $begin)
                ->where('to_date', '<=', $end);
            });
            $query->where('class_id', $request->get('class_id'));
            $query->where('section_id', $request->get('section_id'));
            $query->where('day', $lectcureInfo[0]);
            $query->where('lecture_no', $lectcureInfo[1]);
            $query->where('substitute_id','!=', $decrypted_substitute_id);
        })
        ->count();
        return $total;
    }

     /**
     *  Substitute report
     *  @Shree on 1 April 2018
    **/
    public function substitute_report()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.substitute_report'),
            'redirect_url'  => url('admin-panel/substitute-management/manage-substitution'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.manage-substitution.substitute_report')->with($data);
    }

    /**
     *  Missed Substitute report
     *  @Shree on 1 April 2018
    **/
    public function missed_substitute_report()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.missed_substitute_report'),
            'redirect_url'  => url('admin-panel/substitute-management/manage-substitution'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.manage-substitution.missed_substitute_report')->with($data);
    }

    /**
     *  Get leaves's Data for view page(Datatables)
     *  @Shree on 1 April 2018.
    **/
    public function substituteData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $session    = get_current_session();
        $type = $request->get('type');
        $arr_leave_application      = StaffLeaveApplication::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', $session['session_id']);
            $query->where('staff_leave_status', 1);
        })
        ->whereHas('getStaff', function($query) use($request) {
            $query->where('teaching_status',1);
        })
        ->with(['getStaff' => function($query) use($request) {
            $query->where('teaching_status',1);
            $query->with('getClassAllocation.getCurrentClass');
            $query->with('getClassAllocation.getCurrentSection');
        }])
       
        ->orderBy('staff_leave_id', 'DESC')->get(); 
        $leave_application = [];
        foreach ($arr_leave_application as $key => $leave) {
            $substitution = $this->get_all_substitute($leave->staff_leave_id,$type);
            if($type == 2 && !empty($substitution)){
                $leave->substitution = $substitution;
                $leave_application[] = $leave;
            }
            if($type == 3 && !empty($substitution)){
                $leave->substitution = $substitution;
                $leave_application[] = $leave;
            }
        }
        
        return Datatables::of($leave_application)
        ->addColumn('staff_name', function ($leave_application)
        {	
            return $leave_application['getStaff']->staff_name;
            
        })
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->staff_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->staff_leave_to_date));
        })
        ->addColumn('substitution', function ($leave_application) use ($type)
        {
            return $leave_application->substitution;
        })
        ->rawColumns(['substitution' => 'substitution','class_name'=>'class_name','staff_name'=>'staff_name','date_range'=>'date_range'])->addIndexColumn()
        ->make(true);
    }

    public function get_all_substitute($ref_leave_id,$type){
        // type = 1(For all data), type = 2(For substitution), type = 3(For missed substitution)
        $session        = get_current_session();
        $leave_application  = StaffLeaveApplication::where(function($query) use ($ref_leave_id,$session) 
        {
            $query->where('staff_leave_id', $ref_leave_id);
            $query->where('session_id', $session['session_id']);
            $query->where('staff_leave_status', 1);
        })
        ->whereHas('getStaff', function($query) use($request) {
            $query->where('teaching_status',1);
        })
        ->with(['getStaff' => function($query) use($request) {
            $query->where('teaching_status',1);
            $query->with('getClassAllocation.getCurrentClass');
            $query->with('getClassAllocation.getCurrentSection');
        }])
        ->orderBy('staff_leave_id', 'DESC')->first();

        $staff_id = $leave_application->staff_id;
        $start    = (new DateTime($leave_application->staff_leave_from_date));
        $end      = (new DateTime($leave_application->staff_leave_to_date));
        $interval = DateInterval::createFromDateString('1 day');
        $period = CarbonPeriod::create($start,$end);

        $arr_reverse_week_days = \Config::get('custom.reverse_week_days');
        $arr_week_days = \Config::get('custom.week_days');
        $dateRange = [];
        $weekdays = [];
        foreach ($period as $dt) {
            $dateRange[$dt->format("Y-m-d")] = $dt->format("Y-m-d");
            $weekdays[] = $arr_reverse_week_days[$dt->format("l")];
        }

        $time_table_map = TimeTableMap::where(function($query) use ($weekdays,$staff_id,$session) 
        {
            $query->whereIn('time_table_map.day',$weekdays);
            $query->where('time_table_map.staff_id',$staff_id);
            $query->where('time_table_map.session_id',$session['session_id']);
        })
        ->join('time_tables', function($join){
            $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
        })
        ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
        ->orderBy('map_lecture_no', 'ASC')
        ->with('getSubject')->with('getStaff')
        ->with('getClass')
        ->with('getSection')
        ->get();
        
        $final_array = '';
        foreach ($time_table_map as $key => $map) {
            $substitute  = StaffSubstitution::where(function($query) use ($ref_leave_id,$map,$session) 
            {
                $query->where('ref_leave_id', $ref_leave_id);
                $query->where('session_id', $session['session_id']);
                $query->where('leave_staff_id', $map->staff_id);
                $query->where('class_id', $map->class_id);
                $query->where('section_id', $map->section_id);
                $query->where('lecture_no', $map->lecture_no);
                $query->where('day', $map->day);
            })
            ->with('getStaff')
            ->first();
           
            if($type == 1) {
                $final_array .= "Class-Section: ".$map['getClass']->class_name.'-'.$map['getSection']->section_name.' <br />';
                $final_array .= "Time: ".$map->start_time.'-'.$map->end_time.' <br />';
                $final_array .= "Lecture No - Day: ".$map->lecture_no.' - '.$arr_week_days[$map->day].' <br />';
                if(!empty($substitute)){
                    $final_array .= "Substitute Staff: ".$substitute['getStaff']->staff_name.' <br />';
                }
                $final_array .= ' <br />';
            }
            if($type == 2 && !empty($substitute)) {
                $final_array .= "Class-Section: ".$map['getClass']->class_name.'-'.$map['getSection']->section_name.' <br />';
                $final_array .= "Time: ".$map->start_time.'-'.$map->end_time.' <br />';
                $final_array .= "Lecture No - Day: ".$map->lecture_no.' - '.$arr_week_days[$map->day].' <br />';
                $final_array .= "Substitute Staff: ".$substitute['getStaff']->staff_name.' <br />';
                $final_array .= ' <br />';
            }
            if($type == 3 && empty($substitute)){
                $final_array .= "Class-Section: ".$map['getClass']->class_name.'-'.$map['getSection']->section_name.' <br />';
                $final_array .= "Time: ".$map->start_time.'-'.$map->end_time.' <br />';
                $final_array .= "Lecture No - Day: ".$map->lecture_no.' - '.$arr_week_days[$map->day].' <br />';
                $final_array .= ' <br />';
            }

        }
        // p($final_array);
        return $final_array;
    }

}
