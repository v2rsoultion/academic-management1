<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BookAllowance\BookAllowance; // Model
use App\Model\Payroll\Configuration; // Model
use Redirect;


class MenuController extends Controller
{
    public function configuration() {
        $loginInfo   = get_loggedin_user_data();
        // p($loginInfo);
        $page_title = trans('language.menu_configuration');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.configuration')->with($data);
    }

    public function academic() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_academic');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.academic')->with($data);
    }

    public function certificates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_certificates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.certificates')->with($data);
    }

    public function admission() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_admission');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.admission')->with($data);
    }

    public function student() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_student');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.student')->with($data);
    }

    public function online_content() {
        $loginInfo      = get_loggedin_user_data();
        $page_title     = trans('language.menu_online_content');
        $parent_info    = parent_info($loginInfo['admin_id']);
        $parent_student = get_parent_student($parent_info['student_parent_id']);

        $data                 = array(
            'login_info'      => $loginInfo,
            'parent_student'  => $parent_student,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.onlinecontent')->with($data);
    }

    public function inventory() {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title  = trans('language.menu_inventory');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.inventory')->with($data);
    }

    public function library() {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_library');
        $book_allowance = BookAllowance::first();
        $encrypted_book_allowance_id  	= get_encrypted_value($book_allowance->book_allowance_id, true);
        
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'encrypted_book_allowance_id' => $encrypted_book_allowance_id,
            'book_allowance_id' => $book_allowance->book_allowance_id
        );
        return view('admin-panel.menu.library')->with($data);
    }

    public function visitor() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_visitor');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.visitor')->with($data);
    }

    public function fees_collection() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_fee_collection');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.fees-collection')->with($data);
    }

    public function examination() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_examination');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.examination')->with($data);
    }

    public function staff() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_staff');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.staff')->with($data);
    }

    public function hostel() {
        $permissions = get_permissions();
        if(!in_array('14',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_hostel');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.hostel')->with($data);
    }

    public function transport() {
        $permissions = get_permissions();
        if(!in_array('13',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_transport');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.transport')->with($data);
    }

    public function noticeboard() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_notice_board');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.noticeboard')->with($data);
    }

    public function taskmanager() {
        $permissions = get_permissions();
        if(!in_array('12',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_task_manager');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.taskmanager')->with($data);
    }

    public function recruitment() {
        $permissions = get_permissions();
        if(!in_array('18',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_recruitment');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.recruitment')->with($data);
    }

    public function staff_leave_management() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_leave_management');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.staff_leave_management')->with($data);
    }

    public function student_leave_management() {
        $loginInfo          = get_loggedin_user_data();
        $page_title         = trans('language.menu_leave_management');
        $parent_info        = parent_info($loginInfo['admin_id']);
        $parent_student     = get_parent_student($parent_info['student_parent_id']);

        $data                 = array(
            'login_info'      => $loginInfo,
            'parent_student'  => $parent_student,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.student_leave_management')->with($data);
    }

    public function my_class() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_my_class');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.staff-menu.my_class')->with($data);
    }

    public function my_subject() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_my_subject');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.staff-menu.my_subject')->with($data);
    }

    /**
     *  View Payroll Page
     */
    public function payroll() {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title  = trans('language.payroll');
        $pay_config  = Configuration::where('pay_config_status', 1)->get()->toArray();
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'pay_config'      => $pay_config,  
        );
        return view('admin-panel.menu.payroll')->with($data);
    }
    // Marksheet Templates
    public function marksheet_templates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_marksheet_templates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.marksheet_templates')->with($data);
    }

    // Examination Reports
    public function examination_reports() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_examination_report');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.examination-reports')->with($data);
    }

    // Admission Reports
    public function admission_reports() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_admission_report');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.admission-reports')->with($data);
    }

    // Fees Collection Reports
    public function fees_collection_reports() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_fees_collection_report');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.fees-collection-reports')->with($data);
    }

    // Library Reports
    public function library_reports() {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_library_report');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.library-reports')->with($data);
    }

    // Student Reports
    public function student_reports() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_student_report');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.student-reports')->with($data);
    }
    public function substitute_management() {
        $permissions = get_permissions();
        if(!in_array('19',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.menu_substitute_management');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.substitute_management')->with($data);
    }

    // For Accounts Module
    public function account() {
        $loginInfo   = get_loggedin_user_data();
        $page_title  = trans('language.menu_account');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.menu.account')->with($data);
    }
}
