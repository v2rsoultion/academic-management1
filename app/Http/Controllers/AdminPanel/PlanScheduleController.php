<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PlanSchedule\PlanSchedule; // PlanSchedule Model
use App\Model\Staff\Staff; //Staff Model
use App\Model\Admin\Admin; //Staff Model
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Excel;

class PlanScheduleController extends Controller
{
    /**
     *  View page for Plan Schedule
     *  @Sandeep on 14 March 2019
    **/
   
    public function index()
    {
        $loginInfo                      = get_loggedin_user_data();
        $arr_staff                      = get_all_staffs();
        $listData                       = [];
        $listData['arr_staffs']         = add_blank_option($arr_staff, 'Select Staff');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/plan-schedule/view-plan-schedule'),
            'page_title'    => trans('language.view_plan_schedule'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.plan_schedule.index')->with($data);
    }

     /**
     *  Get Data for view Plan Schedule page(Datatables)
     *  @Sandeep on 14 March 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo      = get_loggedin_user_data();
        $session        = get_current_session();
        $plan_schedule  = PlanSchedule::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->has('staff_id')) && $request->get('staff_id') != null && $request->get('staff_id') != "Select Staff")
            {
                $query->where('staff_id', "=", $request->get('staff_id'));
            }
            if (!empty($request) && !empty($request->get('plan_date')))
            {
                $query->where('plan_date', "=", $request->get('plan_date'));
            }
            $query->where('current_session_id', $session['session_id']); 
        })
        ->with('getStaff')
        ->orderBy('plan_schedule_id','DESC')
        ->get();
        return Datatables::of($plan_schedule)
            ->addColumn('staff_name', function ($plan_schedule)
            {
                $arr_staff   = get_all_staffs();
                $staff_name  = $arr_staff[$plan_schedule->staff_id];
                return $staff_name;
            })

            ->addColumn('plan_date', function ($plan_schedule)
            {
                return date('d M Y',strtotime($plan_schedule['plan_date']));
            })

            ->addColumn('plan_time', function ($plan_schedule)
            {
                return date("g:i a", strtotime($plan_schedule['plan_time']));
            })
            
            ->addColumn('action', function ($plan_schedule)
            {
                $encrypted_plan_schedule_id = get_encrypted_value($plan_schedule->plan_schedule_id, true);
                if($plan_schedule->plan_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="plan-schedule-status/'.$status.'/' . $encrypted_plan_schedule_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-plan-schedule/' . $encrypted_plan_schedule_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-plan-schedule/' . $encrypted_plan_schedule_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  Add page for Plan Schedule
     *  @Sandeep on 14 March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        $plan_schedule  = [];
        $data           = [];
        $loginInfo      = get_loggedin_user_data();
        $arr_staff      = get_all_staffs();

        if (!empty($id))
        {
            $decrypted_plan_schedule_id = get_decrypted_value($id, true);
            $plan_schedule              = PlanSchedule::where('plan_schedule_id','=',$decrypted_plan_schedule_id)->get();
            $plan_schedule              = isset($plan_schedule[0]) ? $plan_schedule[0] : [];
            
            if(!$plan_schedule)
            {
                return redirect('admin-panel/staff/plan-schedule/add-plan_schedule')->withError('Plan Schedule not found!');
            }
            
            $encrypted_plan_schedule_id = get_encrypted_value($plan_schedule->plan_schedule_id, true);
            $page_title                 = trans('language.edit_plan_schedule');
            $save_url                   = url('admin-panel/staff/plan-schedule/save/' . $id);
            $submit_button              = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_plan_schedule');
            $save_url      = url('admin-panel/staff/plan-schedule/save');
            $submit_button = 'Save';
        }

        $plan_schedule['arr_staffs']  = add_blank_option($arr_staff, 'Select Staff');
        
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'plan_schedule' => $plan_schedule,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/plan-schedule/view-plan-schedule'),
        );
        return view('admin-panel.plan_schedule.add')->with($data);
    }

    /**
     *  Save Plan Schedule Data
     *  @Sandeep on 14 March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        //p($request->all());
        $plan_schedule_id           = null;
        $decrypted_plan_schedule_id = null;
        $loginInfo = get_loggedin_user_data();
        $session   = get_current_session();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_plan_schedule_id   = get_decrypted_value($id, true);
            $plan_schedule                = PlanSchedule::find($decrypted_plan_schedule_id);
            $plan_scheduleAdmin           = Admin::Find($plan_schedule->reference_admin_id);
            $admin_id = $plan_schedule['admin_id'];
            $title = "Plan Updated";
            if (!$plan_schedule)
            {
                return redirect('/admin-panel/staff/plan-schedule/add-plan-schedule')->withError('Plan Schedule not found!');
            }
            $success_msg = 'Plan Schedule updated successfully!';
        }
        else
        {
            $plan_schedule            = New PlanSchedule;
            $plan_scheduleAdmin       = new Admin();
            $success_msg              = 'Plan Schedule saved successfully!';
            $title                    = "New Plan Received";
        }
        
        $arr_input_fields = [
            'staff_id'              => 'required',
            'plan_description'      => 'required',
            'plan_date'             => 'required',
            'plan_time'             => 'required',
        ];
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                 
                $plan_schedule->admin_id              = $admin_id;
                $plan_schedule->update_by             = $loginInfo['admin_id'];
                $plan_schedule->staff_id              = Input::get('staff_id');
                $plan_schedule->plan_description      = Input::get('plan_description');
                $plan_schedule->current_session_id    = $session['session_id'];
                $plan_schedule->plan_date             = Input::get('plan_date');
                $plan_schedule->plan_time             = Input::get('plan_time');
                $plan_schedule->save();

                $notification = $this->send_push_notification_staff($plan_schedule->plan_schedule_id,$plan_schedule->staff_id,'plan_scheduled',$admin_id,$plan_description,$title);
                   
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/staff/plan-schedule/view-plan-schedule')->withSuccess($success_msg);   
    }

    /**
     *  Destroy Plan Schedule data
     *  @Sandeep on 14 March 2019 
    **/
    public function destroy($id)
    {
        $plan_schedule_id = get_decrypted_value($id, true);
        $plan_schedule    = PlanSchedule::find($plan_schedule_id);
        
        $success_msg = $error_message =  "";
        if ($plan_schedule)
        {
            DB::beginTransaction();
            try
            {
                $title = "Plan Deleted";
                $notification = $this->send_push_notification_staff($plan_schedule->plan_schedule_id,$plan_schedule->staff_id,'plan_scheduled',$plan_schedule->admin_id,$plan_schedule->plan_description,$title);

                $plan_schedule->delete();
                $success_msg = "Plan Schedule deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/staff/plan-schedule/view-plan-schedule')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/staff/plan-schedule/view-plan-schedule')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Plan Schedule not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change Plan Schedule's status
     *  @Sandeep on 14 March 2019
    **/
    public function changeStatus($status,$id)
    {
        $plan_schedule_id = get_decrypted_value($id, true);
        $plan_schedule    = PlanSchedule::find($plan_schedule_id);
        if ($plan_schedule)
        {
            $plan_schedule->plan_status  = $status;
            $plan_schedule->save();
            $success_msg = "Plan Schedule status updated successfully!";
            return redirect('admin-panel/staff/plan-schedule/view-plan-schedule')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Plan Schedule not found!";
            return redirect('admin-panel/staff/plan-schedule/view-plan-schedule')->withErrors($error_message);
        }
    }


    // Notification function
    public function send_push_notification_staff($module_id,$staff_ids,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($staff_ids) 
        {
            $query->whereIn('staff_id',explode(",",$staff_ids));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $staff['get_staff_admin_info']['admin_id'],
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    /**
     *  View page for staff Plan Schedule
     *  @Sandeep on 15 March 2019
    **/
   
    public function staff_plan_schedule()
    {
        $loginInfo                      = get_loggedin_user_data();
        $arr_staff                      = get_all_staffs();
        $listData                       = [];
        $listData['arr_staffs']         = add_blank_option($arr_staff, 'Select Staff');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/plan-schedule/view-plan-schedule'),
            'page_title'    => trans('language.view_plan_schedule'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.plan_schedule.staff_plan_schedule')->with($data);
    }

     /**
     *  Get Data for view staff  Plan Schedule page(Datatables)
     *  @Sandeep on 15 March 2019
    **/
    public function staff_plan_schedule_data(Request $request)
    {
        $loginInfo      = get_loggedin_user_data();
        $session        = get_current_session();
        $plan_schedule  = PlanSchedule::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->has('staff_id')) && $request->get('staff_id') != null && $request->get('staff_id') != "Select Staff")
            {
                $query->where('staff_id', "=", $request->get('staff_id'));
            }
            if (!empty($request) && !empty($request->get('plan_date')))
            {
                $query->where('plan_date', "=", $request->get('plan_date'));
            }
            $query->where('current_session_id', $session['session_id']); 
        })
        ->with('getStaff')
        ->orderBy('plan_schedule_id','DESC')
        ->get();
        return Datatables::of($plan_schedule)
            ->addColumn('staff_name', function ($plan_schedule)
            {
                $arr_staff   = get_all_staffs();
                $staff_name  = $arr_staff[$plan_schedule->staff_id];
                return $staff_name;
            })

            ->addColumn('plan_date', function ($plan_schedule)
            {
                return date('d M Y',strtotime($plan_schedule['plan_date']));
            })

            ->addColumn('plan_time', function ($plan_schedule)
            {
                return date("g:i a", strtotime($plan_schedule['plan_time']));
            })
            
            ->addColumn('action', function ($plan_schedule)
            {
                $encrypted_plan_schedule_id = get_encrypted_value($plan_schedule->plan_schedule_id, true);
                if($plan_schedule->plan_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="plan-schedule-status/'.$status.'/' . $encrypted_plan_schedule_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-plan-schedule/' . $encrypted_plan_schedule_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-plan-schedule/' . $encrypted_plan_schedule_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    

}
