<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Classes\Classes; // Model 
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\StudentSubjectManage\StudentSubjectManage; // Model
use Yajra\Datatables\Datatables;

class StudentSubjectManageController extends Controller
{
    /**
     *  View page for manage student subject
     *  @shree on 29 Oct 2018
    **/
    public function index()
    {
        $loginInfo  = get_loggedin_user_data();
        $map = $arr_section =  [];
        $arr_class  = get_all_classes_mediums();
        $map['arr_class']    = add_blank_option($arr_class, "Select Class");
        $map['arr_section']  = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.manage_student_subject'),
            'redirect_url'  => url('admin-panel/student-subject/manage'),
            'login_info'    => $loginInfo,
            'map'    => $map,
        );
        return view('admin-panel.manage-student-subject.index')->with($data);
    }

    /**
     *  Get student list for manage student subject
     *  @Shree on 29 Oct 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $request->request->add(['student_type', 'rte_apply_status']);
            $request->merge(['student_type' => '0', 'rte_apply_status'=>'0']);
           
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('manage', function ($student)
            {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                return '<div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li><a target="_blank" href="manage-subject/' . $encrypted_student_id . '" ">Manage Subject</a></li>
                    </ul>
                </div>';
               
                
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'manage'=> 'manage'])->addIndexColumn()->make(true);
    }

    /**
     *  View page for Manage student's subject 
     *  @Shree on 29 Oct 2018
    **/
    public function get_student_subjects($id)
    {
        $student_id = get_decrypted_value($id,true);
        $loginInfo = get_loggedin_user_data();
        $studentInfo = get_student_signle_data($student_id); 
        if(empty($studentInfo)){
            return redirect('admin-panel/student-subject/manage')->withError('Student not found!');
        }
        $studentInfo = isset($studentInfo[0]) ? $studentInfo[0] : [];
        // p($studentInfo);
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/student-subject/manage'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/student-subject/manage-save'),
            'student_id'		=> $student_id,
            'studentInfo'    => $studentInfo
        );
        return view('admin-panel.manage-student-subject.map_subject')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $student_id = $request->get('student_id');
        
        $class    = Classes::find($class_id);
        if ($class)
        {
            $subject 		= SubjectSectionMapping::where('class_id',$class_id)->where('section_id',$section_id)->join('subjects', function($join) use ($section_id){
                $join->on('subjects.subject_id', '=', 'subject_section_map.subject_id');
            })->get();
            
            $subject_ids 		= StudentSubjectManage::select('subject_id')->where('class_id',$class_id)->where('section_id',$section_id)->where('student_id',$student_id)->get();
            $subjectArr 		= array(); 
            $subject_ids_Arr 	= array();
            
            foreach($subject_ids as $subject_id){
                $subject_ids_Arr[] = $subject_id->subject_id;
            }
            
            foreach($subject as $key => $value){
                $subjectArr[$key] = $value;
                $subjectArr[$key]['subject_arr'] = $subject_ids_Arr;
            }
            return Datatables::of($subjectArr)
        		->addColumn('checkbox', function ($subjectArr)
                {
                    $check = '';
                    $exist = 0;
                	if(!empty($subjectArr['subject_arr'])){

                		if(in_array($subjectArr->subject_id, $subjectArr['subject_arr'])){
                            $check = 'checked';
                            $exist = 1;
                		}
                	}
                    return '
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist]" value="'.$exist.'" >
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist_id]" value="'.$subjectArr->subject_id.'" >
                    <div class="checkbox" id="customid">
                    <input type="checkbox" id="subject'.$subjectArr->subject_id.'" name="subjects['.$subjectArr->subject_id.'][subject_id]" class="check" value="'.$subjectArr->subject_id.'" '.$check.'>
                    <label  class="from_one1" style="margin-bottom: 4px !important;"  for="subject'.$subjectArr->subject_id.'"></label>
                    </div>
                  ';
                    // return '<div class="checkbox"><input name="check[]" type="checkbox" class="check" value="'.$subjectArr->subject_id.'" '.$check.'><label for="checkbox10"></label></div>';
                    
                })
                ->addColumn('subjects', function ($subjectArr)
                {
        			return $subjectArr->subject_code.' - '.$subjectArr->subject_name;
                })->rawColumns(['checkbox' => 'checkbox','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
        }
        else
        {
            $error_message = "Class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add and update student's subject data
     *  @Shree on 29 Oct 2018.
    **/
    public function save(Request $request)
    {
        //p($request->all());
        $loginInfo      = get_loggedin_user_data();
        $student_id		= $request->get('student_id');
        $class_id		= $request->get('class_id');
        $section_id		= $request->get('section_id');
        $session_id		= $request->get('session_id');
        $encrypted_student_id = get_encrypted_value($student_id, true);
        $admin_id = $loginInfo['admin_id'];
		if(!empty($request->get('student_id'))){
            //p($request->all());
                $success_msg = 'Subjects are not selected.';
				try
	            {

	            	foreach (Input::get('subjects') as $value){
                        
                        if($value['exist'] == 0 && isset($value['subject_id'])){
                            // Add case
                            $manage = new StudentSubjectManage;		
                            $manage->admin_id       = $admin_id;
                            $manage->update_by      = $loginInfo['admin_id'];
                            $manage->session_id 	 	 = $session_id;
                            $manage->class_id 	 	 = $class_id;
                            $manage->section_id 	 = $section_id;
                            $manage->subject_id 	 = $value['subject_id'];
                            $manage->student_id 	 = $student_id;
                            $manage->save();
                            $success_msg = 'Subjects successfully excluded.';
                        }
                        if($value['exist'] == 1 && !isset($value['subject_id'])){
                            // Delete case case
                            $map_delete = StudentSubjectManage::where('class_id', '=', $class_id)->where('section_id', '=', $section_id)->where('student_id', '=', $student_id)->where('subject_id', '=', $value['exist_id'])->first();
                            
                            $map_delete->delete();
                        }
	            		
	            	}
	                
	            }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
	                $error_message = $e->getMessage();
	                return redirect()->back()->withErrors($error_message);
	            }
	            DB::commit();
				return redirect('admin-panel/student-subject/manage-subject/'.$encrypted_student_id)->withSuccess($success_msg);
          
    	}else{
    		return redirect()->back();
    	}
	}
}
