<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Transport\Route;
use App\Model\Transport\RouteLocation;
use App\Model\School\School;
use App\Model\Transport\AssignDriverConductorRoute;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;

class RouteController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('13',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for route
     *  @Sandeep on 1 feb 2019
    **/

    public function add(Request $request, $id = NULL)
    {
        $data     = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_route_id = get_decrypted_value($id, true);
            $route              = Route::where('route_id','=',$decrypted_route_id)->with('get_route_locations')->get();
            $route              = isset($route[0]) ? $route[0] : [];
            
            if (!$route)
            {
                return redirect('admin-panel/transport/route/add-route')->withError('Route not found!');
            }
            
            if($route['route_location'] != '') {
                $route['route_location'] = explode(',',$route['route_location']);
            }
            
            $document_list = [];
            foreach ($route['get_route_locations'] as $route_locations)
            {
                $route_location_data['route_location_id']       = $route_locations['route_location_id'];
                $route_location_data['location_sequence']       = $route_locations['location_sequence'];
                $route_location_data['location_name']           = $route_locations['location_name'];
                $route_location_data['location_latitude']       = $route_locations['location_latitude'];
                $route_location_data['location_logitude']       = $route_locations['location_logitude'];
                $route_location_data['location_pickup_time']    = $route_locations['location_pickup_time'];
                $route_location_data['location_drop_time']      = $route_locations['location_drop_time'];
                $document_list[] = $route_location_data;
            }
            $route->route_locations = $document_list;
            $encrypted_route_id = get_encrypted_value($route->route_id, true);
            $page_title         = trans('language.edit_route');
            $save_url           = url('admin-panel/transport/route/save/' . $id);
            $submit_button      = 'Update';
        
        } else {
            $page_title    = trans('language.add_route');
            $save_url      = url('admin-panel/transport/route/save');
            $submit_button = 'Save';
        }

        $arr_route                      = \Config::get('custom.route_type');
        $route['arr_route']             = $arr_route;

        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'route'       => $route,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/route/view-route'),
        );
        return view('admin-panel.route.add')->with($data);
    }


    /**
     *  Save Route Data
     *  @Sandeep on 1 Feb 2019
    **/

    public function save(Request $request, $id = NULL)
    {
      //  p($request->all());
        $route_id           = null;
        $decrypted_route_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_route_id = get_decrypted_value($id, true);
            $route              = Route::find($decrypted_route_id);
            $routeAdmin         = Admin::Find($route->reference_admin_id);
            $admin_id = $route['admin_id'];
            if (!$route)
            {
                return redirect('/admin-panel/transport/route/add-route')->withError('Route not found!');
            }
            $success_msg = 'Route updated successfully!';
        }
        else
        {
            $route            = New Route;
            $routeAdmin       = new Admin();
            $success_msg        = 'Route saved successfully!';
        }

        $arr_input_fields = [
            'route_name'                   => 'required',
            'route_location'               => 'required',
            'route_description'            => 'required',
        ];
        
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                $route->admin_id                        = $admin_id;
                $route->update_by                       = $loginInfo['admin_id'];
                $route->route_name                      = Input::get('route_name');
                $route->route_location                  = implode(',',Input::get('route_location'));
                $route->route_description               = Input::get('route_description');
                $route->save();
                
                //p($request->all());
                
                $routelocationKey = 0;
                if(!empty($request->get('routes'))) {
                    foreach($request->get('routes') as $route_locations){ 

                        $school       = School::where('school_id' ,$loginInfo['school_id'])->first();

                        $origin       = $school->school_latitude.",".$school->school_longitude;
                        $destination  = $route_locations['location_latitude'].",".$route_locations['location_longitude'];
                        

                        $urlApi       = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origin."&destinations=".$destination."&language=en-EN&key=AIzaSyAz6rxJ-fZSj3K16LwmUtSCDWG_snDHna0";
            
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_URL,$urlApi);
                        $result=curl_exec($ch);
                        curl_close($ch);
                        $data =  json_decode($result, true);

                        $route_locations['location_km_distance'] = $data[rows][0][elements][0][distance][text];

                        if(isset($route_locations['location_name']) && !empty($route_locations['location_name'])) {
                            if(isset($route_locations['route_location_id']) ) {
                                $route_location_update     = RouteLocation::where('route_location_id', $route_locations['route_location_id'])->first();
                                $route_location_update->admin_id                 = $loginInfo['admin_id'];
                                $route_location_update->update_by                = $loginInfo['admin_id'];
                                $route_location_update->route_id                 = $route->route_id;
                                $route_location_update->location_name            = $route_locations['location_name'];
                                $route_location_update->location_sequence        = $route_locations['location_sequence'];
                                $route_location_update->location_latitude        = $route_locations['location_latitude'];
                                $route_location_update->location_logitude        = $route_locations['location_longitude'];
                                $route_location_update->location_pickup_time     = $route_locations['eta_pickiup'];
                                $route_location_update->location_drop_time       = $route_locations['eta_drop'];
                                $route_location_update->location_km_distance     = $route_locations['location_km_distance'];
                                $route_location_update->save();
                            } else {
                                $route_location   = new RouteLocation();
                                $route_location->admin_id                 = $admin_id;
                                $route_location->update_by                = $loginInfo['admin_id'];
                                $route_location->route_id                 = $route->route_id;
                                $route_location->location_name            = $route_locations['location_name'];
                                $route_location->location_sequence        = $route_locations['location_sequence'];
                                $route_location->location_latitude        = $route_locations['location_latitude'];
                                $route_location->location_logitude        = $route_locations['location_longitude'];
                                $route_location->location_pickup_time     = $route_locations['eta_pickiup'];
                                $route_location->location_drop_time       = $route_locations['eta_drop'];
                                $route_location->location_km_distance     = $route_locations['location_km_distance'];
                                $route_location->save();
                            }
                        }
                        $routelocationKey++;
                    }
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }   
        return redirect('admin-panel/transport/route/view-route')->withSuccess($success_msg);   
    }


    /**
     *  View page for route
     *  @Sandeep on 1 Feb 2019
    **/
   
    public function index()
    {
        $loginInfo                            = get_loggedin_user_data();
        $arr_designation                      = get_all_designations();
        $listData                             = [];
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/route/view-route'),
            'page_title'    => trans('language.view_route'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.route.index')->with($data);
    }


    /**
     *  Get Data for view route page(Datatables)
     *  @Sandeep on 1 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $route  = Route::where(function($query) use ($request) 
        {
            
            if (!empty($request) && !empty($request->get('route_name')))
            {
                $query->where('route_name', "like", "%{$request->get('route_name')}%");
            }

        })->with('get_route_locations')->orderBy('route_id', 'DESC')->get();

        return Datatables::of($route)

            ->addColumn('total_stops', function ($route)
            {
                $total_stops = '<button class="btn btn-raised btn-primary route" route_location_id='.$route->route_id.'>'.count($route->get_route_locations).' Stops </button>';
                return $total_stops;
            })
            
            ->addColumn('map_views', function ($route)
            {   

                foreach ($route['get_route_locations'] as $key => $value){

                    $get_route_locations[] = $value;

                    $wayptsnew[] = array(
                        "location" =>
                        array(
                            'lat' => str_replace('"', '', $value->location_latitude),
                            'lng' => str_replace('"', '', $value->location_logitude)
                        ),
                        'stopover'  => true
                    );
                }

                $start = "{lat: ".$get_route_locations[0]->location_latitude.", lng: ".$get_route_locations[0]->location_logitude."}";
                $end   = array_values(array_slice($get_route_locations, -1))[0];
                $end_new   = "{lat: ".$end->location_latitude.", lng: ".$end->location_logitude."}";

                $newarray = array_slice($wayptsnew, 1, -1);

                $wayptsnew = json_encode($newarray);
                $wayptsnews = str_replace('"','',$wayptsnew);

                if($start == ""){ $start = []; }
                if($end == ""){ $end = []; }
                if($end_new == ""){ $end_new = []; }

                $map_views = '<button class="btn btn-raised btn-primary map_views" onclick="calculateAndDisplayRoute('.$start.','.$end_new.','.$wayptsnews.')" route_location_id='.$route->route_id.'> Map View </button>';
                return $map_views;
            })

            ->addColumn('action', function ($route)
            {
                $encrypted_route_id = get_encrypted_value($route->route_id, true);
                if($route->route_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="route-status/'.$status.'/' . $encrypted_route_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-route/' . $encrypted_route_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-route/' . $encrypted_route_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','total_stops'=> 'total_stops','map_views'=> 'map_views'])->addIndexColumn()->make(true);
    }

    

    /**
     *  Change route's status
     *  @Sandeep on 1 Feb 2019 
    **/
    public function changeStatus($status,$id)
    {
        $route_id = get_decrypted_value($id, true);
        $route    = Route::find($route_id);
        if ($route)
        {
            $route->route_status  = $status;
            $route->save();
            $success_msg = "Route status updated successfully!";
            return redirect('admin-panel/transport/route/view-route')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Route not found!";
            return redirect('admin-panel/transport/route/view-route')->withErrors($error_message);
        }
    }

    /**
     *  Destroy route data
     *  @Sandeep on 1 Feb 2019   
    **/
    public function destroy($id)
    {
        $route_id = get_decrypted_value($id, true);
        $route    = Route::find($route_id);
        
        $success_msg = $error_message =  "";
        if ($route)
        {
            DB::beginTransaction();
            try
            {
                $route->delete();
                $success_msg = "Route deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/transport/route/view-route')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/transport/route/view-route')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Route not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Get Data for view route page(Datatables)
     *  @Sandeep on 1 Feb 2019 
    **/
    public function route_locations(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $route  = RouteLocation::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('vehicle_document_id'))) {
                $query->where('route_id', "=", $request->get('vehicle_document_id'));
            }
        })->with('getDocumentCategory')->orderBy('vehicle_document_id', 'DESC')->get();

        return Datatables::of($route)

            ->addColumn('document_category_name', function ($route)
            {

                $document_category_name = $route['getDocumentCategory']['document_category_name'];
                
                return $document_category_name;
            })

            ->addColumn('vehicle_document_file', function ($vehicle)
            {
                if (!empty($vehicle->vehicle_document_file)) {
                    $document_file = check_file_exist($vehicle->vehicle_document_file, 'vehicle_document_file');
                    if (!empty($document_file))
                    {
                        $vehicle_document_file = '<a href='.url($document_file).'  target="_blank" >View Document</a>';
                    } 
                } else {
                    $vehicle_document_file = '<a href="#" target="_blank" >No Document Found</a>';
                }

                return $vehicle_document_file;
            })


            ->addColumn('action', function ($vehicle)
            {
                $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_document_id, true);
                
                return '
                    <div class="btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete" style="margin-top:-8px;"><a href="delete-vehicle-document/' . $encrypted_vehicle_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','vehicle_document_file'=> 'vehicle_document_file'])->addIndexColumn()->make(true);
    }


    /**
     *  View page for Route mapping - Map Driver conductor route
     *  @Sandeep on 3 Feb 2019
    **/
    public function manage_route()
    {
        $loginInfo       = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_route'),
            'login_info'    => $loginInfo,
            'save_url'      => url('admin-panel/transport/manage-route/save'),
        );
        return view('admin-panel.route.manage_route')->with($data);
    }


    /**
     *  Get Driver conductor router mapping's Data for view page(Datatables)
     *  @Sandeep on 3 Feb 2019
    **/
    public function map_route_data(Request $request)
    {
        $loginInfo       = get_loggedin_user_data();
        $get_vehicles    = get_all_vehicles();
        $get_drivers     = get_all_driver_conductors('14');
        $get_conductors  = get_all_driver_conductors('15');
        $get_routes      = get_all_routes();

        $listData           = [];
        $table = 
        "<table class='table m-b-0 c_list' style='width:100%'>
        <thead>
        <tr>
            <th>S No.</th>
            <th>Vehicle Name</th>
            <th>Route</th>
        </tr>
        </thead>
        <tbody>";

        $i=0;
        foreach ($get_vehicles as $key => $value){
        $i++;
            $driver_id_arr = $conductor_id_arr = $route_id_arr = array();
            $assign_data = AssignDriverConductorRoute::where('vehicle_id',$value->vehicle_id)->get()->toArray();
            $conductor_driver_route_map_id = NULL;
            $assign_id_arr = "";
            foreach ($assign_data as $assignkey){  
                $driver_id_arr[]    = $assignkey['driver_id'];
                $assign_id_arr      = $assignkey['assign_id'];
                $conductor_id_arr[] = $assignkey['conductor_id'];
                $route_id_arr[]     = $assignkey['route_id'];
                $conductor_driver_route_map_id = $assignkey['conductor_driver_route_map_id'];
            }
            $table .= "<tr>";
            $table .= "<td>".$i."</td>";
            $table .= "<td>".$value->vehicle_name."</td>
                       <input type='hidden' name=routeMap[$key][assign_id]' value='".$assign_id_arr."' />
                       <input type='hidden' name=routeMap[$key][vehicle_id]' value='".$value->vehicle_id."' />
            ";
            // $table .= "<td>".$value->vehicle_name."</td>
            //             <td>";
            
            // $table  .= "
            // <input type='hidden' name=routeMap[$key][assign_id]' value='".$assign_id_arr."' />
            // <input type='hidden' name=routeMap[$key][vehicle_id]' value='".$value->vehicle_id."' />
            // <label class='field select' style='width:200px;'><select class='form-control show-tick select_form3 select2' name='routeMap[$key][driver_id]'  id='teacher_ids_".$value->subject_id."'>";
            // $table .= "<option value='' >Select Driver</option>";
            // foreach($get_drivers as $key2 => $value2){ // For Teacher's details
            //     $disable = '';

            //     if($key2 == ''){
            //         $disable = 'disabled="disabled"';
            //     }
            //     $default_select = '';
            //     if(in_array($key2, $driver_id_arr)){
            //         $default_select = 'selected';
            //     }
            //     $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
            // } 
            // $table .= "</select></label></td>";

            // $table  .= "<td>
            // <label class='field select' style='width:200px;'><select class='form-control show-tick select_form3 select2' name='routeMap[$key][conductor_id]'  id='teacher_ids_".$value->subject_id."'>";
            // $table .= "<option value='' >Select Conductor</option>";
            // foreach($get_conductors as $key2 => $value2){ // For Teacher's details
            //     $disable = '';

            //     if($key2 == ''){
            //         $disable = 'disabled="disabled"';
            //     }
            //     $default_select = '';
            //     if(in_array($key2, $conductor_id_arr)){
            //         $default_select = 'selected';
            //     }
            //     $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
            // } 
            // $table .= "</select></label></td>";

            $table  .= "<td>
            <label class='field select' style='width:200px;'><select class='form-control show-tick select_form3 select2' name='routeMap[$key][route_id]'  id='teacher_ids_".$value->subject_id."'>";
            $table .= "<option value='' >Select Route</option>";
            foreach($get_routes as $key2 => $value2){ // For Teacher's details
                $disable = '';

                if($key2 == ''){
                    $disable = 'disabled="disabled"';
                }
                $default_select = '';
                if(in_array($key2, $route_id_arr)){
                    $default_select = 'selected';
                }
                $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
            } 
            $table .= "</select></label></td>";
            $table .= "</tr>";                          
        }

        $table .= "</tbody></table>";
        return response()->json(['status'=>1001,'data'=>$table]);
    }


    /**
     *  Add and update driver conductor route mapping's data
     *  @Sandeep on 1 Feb 2019.
    **/
    public function save_manage_route(Request $request)
    {
        
        $loginInfo          = get_loggedin_user_data();
        $admin_id           = $loginInfo['admin_id'];

//        p($request->all());

        foreach ($request->get('routeMap') as $key => $map) {

            if(($map['assign_id'] != '')){

                // $arr_input_fields = [
                //     'driver_id'               => 'required|unique:assign_driver_conductor_routes,driver_id,' . $map['assign_id'] . ',assign_id',
                //     'conductor_id'            => 'required|unique:assign_driver_conductor_routes,conductor_id,' . $map['conductor_id'] . ',conductor_id',
                //     'route_id'                => 'required|unique:assign_driver_conductor_routes,route_id,' . $map['route_id'] . ',route_id',
                // ];

                // $validatior = Validator::make($map, $arr_input_fields);

                // if ($validatior->fails()) {
                //     return redirect()->back()->withInput()->withErrors($validatior);
                // } else {
                //     DB::beginTransaction(); //Start transaction!
                //     try
                // {

                    // edit case
                    $mapping                = AssignDriverConductorRoute::find($map['assign_id']);      
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->vehicle_id    = $map['vehicle_id'];
                    // $mapping->driver_id     = $map['driver_id'];
                    // $mapping->conductor_id  = $map['conductor_id'];
                    $mapping->route_id      = $map['route_id'];
                    $mapping->save();
                
                // }
                // catch (\Exception $e)
                // {
                //     //failed logic here
                //     DB::rollback();
                //     $error_message = $e->getMessage();
                //     return redirect()->back()->withErrors($error_message);
                // } }

            } else if($map['vehicle_id'] != ''){

                // $arr_input_fields = [
                //     'driver_id'               => 'required|unique:assign_driver_conductor_routes,driver_id',
                //     'conductor_id'            => 'required|unique:assign_driver_conductor_routes,conductor_id',
                //     'route_id'                => 'required|unique:assign_driver_conductor_routes,route_id',
                // ];

                // $validatior = Validator::make($map, $arr_input_fields);

                // if ($validatior->fails()) {
                //     return redirect()->back()->withInput()->withErrors($validatior);
                // } else {
                //     DB::beginTransaction(); //Start transaction!
                //     try
                // {

                    // add case
                    $mapping                = new AssignDriverConductorRoute;        
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->vehicle_id    = $map['vehicle_id'];
                    // $mapping->driver_id     = $map['driver_id'];
                    // $mapping->conductor_id  = $map['conductor_id'];
                    $mapping->route_id      = $map['route_id'];
                    $mapping->save();

                // }
                // catch (\Exception $e)
                // {
                //     //failed logic here
                //     DB::rollback();
                //     $error_message = $e->getMessage();
                //     return redirect()->back()->withErrors($error_message);
                // } }
            }

        }
            // $arr_input_fields = [
            //     'driver_id'               => 'required|unique:assign_driver_conductor_routes,driver_id',
            //     'conductor_id'            => 'required|unique:assign_driver_conductor_routes,conductor_id',
            //     'route_id'                => 'required|unique:assign_driver_conductor_routes,route_id',
            // ];

            // $validatior = Validator::make($map, $arr_input_fields);


            // if ($validatior->fails())
            // {
            //     return redirect()->back()->withInput()->withErrors($validatior);
            // } else {
            //     DB::beginTransaction(); //Start transaction!
            //     try
            //     {
                    

                // }
                // catch (\Exception $e)
                // {
                //     //failed logic here
                //     DB::rollback();
                //     $error_message = $e->getMessage();
                //     return redirect()->back()->withErrors($error_message);
                // }
        DB::commit();
        $success_msg = "Map Route successfully";
        return redirect()->back()->withSuccess($success_msg);
        //     }

        // }

    }

        
            
    /**
     *  Destroy route location data
     *  @Sandeep on 4 Feb 2019   
    **/
    public function destroy_route_location(Request $request)
    {
        //p($request->all());
        $route    = RouteLocation::find(Input::get('route_location_id'));
        $route->delete();
    }            
                

    /**
     *  Get Data for view route location page(Datatables)
     *  @Sandeep on 4 Feb 2019
    **/
    public function get_route_location(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();

        $route  = RouteLocation::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('route_location_id')))
            {
                $query->where('route_id', "=", $request->get('route_location_id') );
            }

            if (!empty($request) && !empty($request->get('route_location_name')))
            {
                $query->where('location_name', "like", "%{$request->get('route_location_name')}%");
            }

        })->orderBy('route_location_id', 'DESC')->get();

        return Datatables::of($route)
            ->addColumn('latitude_logitude', function ($route)
            {
                $latitude_logitude = $route->location_latitude.','.$route->location_logitude;
                return $latitude_logitude;
            })

            ->addColumn('pick_drop', function ($route)
            {
                $pick_drop = $route->location_pickup_time.' - '.$route->location_drop_time;
                return $pick_drop;
            })
            
            ->addColumn('action', function ($route)
            {
                $encrypted_route_id = get_encrypted_value($route->route_location_id, true);
                return '
                    <div class="btn-neutral btn-icon-mini" data-toggle="tooltip" id="route_location_id_'.$route->route_location_id.'" title="Delete" style="margin-top:-10px;"><a  href="#" onclick="delete_route_location('.$route->route_location_id.')" ><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','total_stops'=> 'total_stops'])->addIndexColumn()->make(true);
    }
                

    public function traking_api(){


        $fields = array (
            'deviceid'    => '123456789'
        );

        $username = 'letstrack';
        $password = 'Client@India';

        $header = array(
            'Authorization: Basic '. base64_encode("$username:$password"),
            'Content-Type: application/x-www-form-urlencoded',
            // 'Authentication Type: Basic Auth',
            // 'Username: letstrack',
            // 'Password: Client@India',
            // 'Accept: application/json'
        );

        $ch = curl_init();
        $remote_url = "http://clients.letstrack.in/demo/api/GetDeviceLocation";
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password); 
        curl_setopt($ch, CURLOPT_URL, $remote_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch, CURLOPT_POSTFIELDS, "deviceid=123456789");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch); 
        p($server_output);        
        // curl_close( $ch );


    }










    


}
