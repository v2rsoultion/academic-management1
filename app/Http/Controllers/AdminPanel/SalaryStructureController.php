<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\SalaryHead; // Model
use App\Model\Payroll\SalaryStructure; // Model
use App\Model\Payroll\SalaryStructureMap; // Model
use App\Model\Payroll\SalaryStructureMapStaff; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class SalaryStructureController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Salary Structure
     * @Khushbu on 21 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $sal_structure  = $sal_head = $arr_sal_struct_map = $arr_sal_head = []; 
        $loginInfo   = get_loggedin_user_data();
        $sal_head    = SalaryHead::where([['pay_salary_head_id', '<>', 1],['pay_salary_head_status',1]])->get();
        if(!empty($id)) {
	 		$decrypted_sal_struct_id 	= get_decrypted_value($id, true);
            $sal_structure      	    = SalaryStructure::Find($decrypted_sal_struct_id);
            $sal_structure_map          = SalaryStructureMap::select('pay_salary_head_id','salary_head_pf','salary_head_esi')->where('salary_structure_id',$sal_structure->salary_structure_id)->get();
            // p($sal_structure_map);
            foreach($sal_structure_map as $key => $value) {
                $arr_sal_struct_map[$key]= array(
                    'pay_salary_head_id' => $value['pay_salary_head_id'],
                    'salary_head_pf' => $value['salary_head_pf'],
                    'salary_head_esi' => $value['salary_head_esi']
                );
            }
            $page_title                 = trans('language.edit_sal_structure');
        	$save_url    			    = url('admin-panel/payroll/manage-salary-structure-save/'. $id);
        	$submit_button  		    = 'Update';
	 	} else {
            $page_title             = trans('language.add_sal_structure');
	 		$save_url    			= url('admin-panel/payroll/manage-salary-structure-save');
	 		$submit_button  		= 'Save';
	 	}
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'sal_structure'   => $sal_structure,
            'sal_head'        => $sal_head,
            'arr_sal_struct_map' => $arr_sal_struct_map,
        );
        return view('admin-panel.payroll-salary-structure.add')->with($data);
    }

    /**
     *	Add & Update of Payroll Salary Structure
     *  @Khushbu on 21 Feb 2019
    **/
    public function save(Request $request, $id = NULL) {
        // p($request->all());
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_sal_struct_id	= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id)) {
            $sal_structure        = SalaryStructure::Find($decrypted_sal_struct_id);
            if(!$sal_structure) {
                return redirect('admin-panel/payroll/manage-salary-structure-save')->withErrors('Salary Structure not found!');
            }
            $admin_id    = $sal_structure->admin_id;
            $success_msg = 'Salary Structure updated successfully!';
        } else {
            $sal_structure    	 = New SalaryStructure;
            $success_msg         = 'Salary Structure saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'sal_structure_name'    => 'required|unique:salary_structure,sal_structure_name,' . $decrypted_sal_struct_id . ',salary_structure_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            if(!empty($request->get('heads_mapping')) && count($request->get('heads_mapping')) != 0){

            DB::beginTransaction();
            try
            {
                $sal_structure->admin_id           = $admin_id;
                $sal_structure->update_by          = $loginInfo['admin_id'];
                $sal_structure->sal_structure_name = Input::get('sal_structure_name');
                $sal_structure->round_off          = Input::get('round_off');
                $sal_structure->save();

                foreach ($request->get('heads_mapping') as $value){
                    if($value['exist'] == 1)
                    {
                        if(!isset($value['sal_head_id']) && $value['exist_id'] != '') {
                            // Delete Case 
                            $success_msg = "Record Updated successfully";
                            $sal_structure_map  = SalaryStructureMap::where([['salary_structure_id',$request->get('sal_structure_id')],['pay_salary_head_id',$value['exist_id']]])->first();
                            $sal_structure_map->delete();
                        }
                        if($value['sal_head_id'] != '' && $value['exist_id'] != '') {
                            $success_msg = "Record Updated successfully";
                            $sal_structure_map  = SalaryStructureMap::where([['salary_structure_id',$request->get('sal_structure_id')],['pay_salary_head_id',$value['exist_id']]])->first();
                            $sal_structure_map->salary_head_pf   	= $value['pf_volume'];
                            $sal_structure_map->salary_head_esi   	= $value['esi_volume'];
                            $sal_structure_map->save();
                        }
                    }
                    if($value['exist'] == 0 && isset($value['sal_head_id']) )
                    {
                        //Add Case
                        $sal_structure_map                      = New SalaryStructureMap;
                        $success_msg                            = 'Salary Heads Mapped successfully.';
                        $sal_structure_map->admin_id            = $admin_id;
                        $sal_structure_map->update_by           = $loginInfo['admin_id'];
                        $sal_structure_map->salary_structure_id = $sal_structure->salary_structure_id;
                        $sal_structure_map->pay_salary_head_id 	= $value['sal_head_id'];
                        $sal_structure_map->salary_head_pf   	= $value['pf_volume'];
                        $sal_structure_map->salary_head_esi   	= $value['esi_volume'];
                        $sal_structure_map->save();
                    }    
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            }
        }
        return redirect('admin-panel/payroll/manage-salary-structure')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Salary Structure's Data fo view page
     *  @Khushbu on 21 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$sal_structure 		= SalaryStructure::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_sal_structure') !=  NULL)
            {
                $query->where('sal_structure_name', 'Like', $request->get('s_sal_structure').'%');
            }
        })->orderBy('salary_structure_id','DESC')->with('SalaryStructureMap')->get();
        return Datatables::of($sal_structure)
        ->addColumn('sal_head_info', function($sal_structure) {
            $encrypted_sal_struct_id  = get_encrypted_value($sal_structure->salary_structure_id, true);
            $sal_head_info = '<a href="" class="btn btn-raised btn-primary sal-head-info" data-toggle="modal" data-target="#viewSalaryHeadModel" sal_structure_id='.$sal_structure->salary_structure_id.'>View Details</a>';
            return $sal_head_info;
        })
        ->addColumn('round_off', function($sal_structure) {
            if($sal_structure['round_off'] == 0) {
                $round_off = 'Nearest Rupee';
            } else {
                $round_off = 'Highest Rupee';
            }
            return $round_off;
        })
    	->addColumn('action', function($sal_structure) use($request) {
            $encrypted_sal_struct_id  = get_encrypted_value($sal_structure->salary_structure_id, true);
              return '<div class="text-center">
                    
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-salary-structure/'.$encrypted_sal_struct_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-salary-structure/' . $encrypted_sal_struct_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['sal_head_info' => 'sal_head_info', 'round_off' => 'round_off', 'action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-salary-structure');
    } 

    /** 
     *  Get Salary Head Data
     *  @Khushbu on 21 Feb 2019.
    **/
    public function getSalaryHeadData(Request $request) {
        $sal_structure_map        = SalaryStructureMap::where('salary_structure_id',$request->get('sal_structure_id'))->with('SalarysHead')->get();
        return Datatables::of($sal_structure_map)
        ->addColumn('salary_head_name', function($sal_structure_map) {
            $salary_head_name = $sal_structure_map['SalarysHead']['sal_head_name'];
            return $salary_head_name;
        })
        ->addColumn('salary_head_pf', function($sal_structure_map) {
            if(empty($sal_structure_map['salary_head_pf']))
            {   
                $salary_head_pf = '---';    
            } else {
                $salary_head_pf = $sal_structure_map['salary_head_pf'].'%';
            }
            return $salary_head_pf;
        }) 
        ->addColumn('salary_head_esi', function($sal_structure_map) {
            if(empty($sal_structure_map['salary_head_esi']))
            {   
                $salary_head_esi = '---';    
            } else {
                $salary_head_esi = $sal_structure_map['salary_head_esi'].'%';
            }
            return $salary_head_esi;
        })->rawColumns(['salary_head_name' => 'salary_head_name'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-salary-structure');
    } 

    /**
	 *	Destroy Data of Payroll Salary Structure
     *  @Khushbu on 23 Feb 2019
	**/
	public function destroy($id) {
        $sal_structure_id 	= get_decrypted_value($id, true);
        $sal_structure_map  = SalaryStructureMap::where('salary_structure_id',$sal_structure_id)->get();
        $sal_structure 	    = SalaryStructure::find($sal_structure_id);
        if ($sal_structure)
        {
            DB::beginTransaction();
            try
            {
                foreach ($sal_structure_map as $key => $value) {
                    $sal_structure_map[$key]->delete();
                }
                $sal_structure->delete();
                $success_msg = "Salary Structure deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Salary Structure not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
	 *	Get Staff Data
     *  @Khushbu on 23 Feb 2019
	**/
    public function getStaffMap(Request $request) {
        $loginInfo   = get_loggedin_user_data();
        
        $page_title         = trans('language.add_sal_structure');
	 	$save_url    		= url('admin-panel/payroll/salary-structure-map-staff-save');
	 	$submit_button  	= 'Save';
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
        );
        return view('admin-panel.payroll-salary-structure.map-employees')->with($data);
    }

    /**
     *	Get Staff Data With Salary Structure
     *  @Khushbu on 23 Feb 2019
     */
    public function anyDataMapEmployees(Request $request) {  
        $arr_sal_structure = [];
        $staff       = Staff::select('staff_id', 'staff_name','staff_profile_img')->with('SalStructureMapStaff')->get();
        // p($staff);
        $sal_structure_map_staff  = SalaryStructureMapStaff::get();
        $staff_ids_arr 	    = array();
        foreach($sal_structure_map_staff as $key => $value){
            $staff_ids_arr[] = $value['staff_id'];
        }
        // p($staff_ids_arr);
        
        return Datatables::of($staff,$staff_ids_arr)
        ->addColumn('employee_profile', function($staff) use($staff_ids_arr) {
            $profile = '';
            if($staff->staff_profile_img != '') {
                $staff_profile_path = check_file_exist($staff->staff_profile_img, 'staff_profile');
                $profile = "<img src=".url($staff_profile_path)." height='30' />";
            }
            $staff_name = $staff->staff_name;
            $exist = 0;
            if(!empty($staff_ids_arr)){
                if(in_array($staff->staff_id, $staff_ids_arr)){
                    $exist = 1;
                }
            }
            $employee_profile = $profile." ".$staff_name. '
            <input type="hidden", name="sal_struct_map['.$staff->staff_id.'][staff_id]" value="'.$staff->staff_id.'">
            <input type="hidden" name="sal_struct_map['.$staff->staff_id.'][exist]" value="'.$exist.'" >
            <input type="hidden" name="sal_struct_map['.$staff->staff_id.'][exist_id]" value="'.$staff->staff_id.'" >
            ';
            return $employee_profile;
        })
        ->addColumn('basic_pay', function($staff) {
            $basic_pay = '<input type="number" name="sal_struct_map['.$staff->staff_id.'][basic_pay]" value="'.$staff['SalStructureMapStaff']['basic_pay_amt'].'" class="form-control" id = "basic_pay" palceholder="" style="width:200px;">';
            return $basic_pay;
        })
        ->addColumn('salary_structure', function($staff) {
            $arr_sal_structure  = get_all_salary_structure($staff['SalStructureMapStaff']['salary_structure_id']);
            $select = '<label class=" field select" style="width:60%"><select class="form-control show-tick select_form1 select2 " id="salary_structure"  name="sal_struct_map['.$staff->staff_id.'][salary_structure]" >'.$arr_sal_structure.'</select></label>';
            return $select;
        })->rawColumns(['employee_profile' => 'employee_profile', 'basic_pay' => 'basic_pay', 'salary_structure' => 'salary_structure'])->addIndexColumn()
        ->make(true); 
        return redirect('/payroll/salary-structure-map-staff');
    }

    /**
     *	Add & Update of Payroll Salary Structure
     *  @Khushbu on 23 Feb 2019
    **/
    public function saveStaffMap(Request $request) {
        // p($request->get('sal_struct_map'));
        $loginInfo      = get_loggedin_user_data();
        $admin_id       = $loginInfo['admin_id'];
        $session        = get_current_session();
        if (!$request->get('sal_struct_map'))
        {
            return redirect('/admin-panel/payroll/salary-structure-map-staff')->withError('Salary Structure mapped with staff not found!');
        }
        $success_msg = 'Salary Structure mapped with staff updated successfully!';
        
        DB::beginTransaction();
        try
        { 
            foreach($request->get('sal_struct_map') as $value) {
               
                if($value['exist'] == 1)
                    {
                        if($value['basic_pay'] == '' && $value['salary_structure'] == '') {
                        // Delete Case 
                        $success_msg = "Record Updated successfully";
                        $sal_structure_map_staff  = SalaryStructureMapStaff::where('staff_id',$value['staff_id'])->first();
                        $sal_structure_map_staff->delete();
                        }
                        if($value['basic_pay'] != '' || $value['salary_structure'] != '') {
                        $success_msg = "Record Updated successfully";
                        $sal_structure_map_staff  = SalaryStructureMapStaff::where('staff_id',$value['staff_id'])->first();
                        $sal_structure_map_staff->basic_pay_amt   	  = $value['basic_pay'];
                        $sal_structure_map_staff->salary_structure_id = $value['salary_structure'];
                        $sal_structure_map_staff->save();
                        }
                    }
                if($value['exist'] == 0 && (isset($value['basic_pay']) || isset($value['salary_structure'])))
                {
                    //Add Case
                    $sal_structure_map_staff                = New SalaryStructureMapStaff;
                    $success_msg                            = 'Salary Structure Mapped with staff successfully.';
                    $sal_structure_map_staff->admin_id            = $admin_id;
                    $sal_structure_map_staff->update_by           = $loginInfo['admin_id'];
                    $sal_structure_map_staff->session_id          = $session['session_id'];
                    $sal_structure_map_staff->staff_id 	          = $value['staff_id'];
                    $sal_structure_map_staff->basic_pay_amt   	  = $value['basic_pay'];
                    $sal_structure_map_staff->salary_structure_id = $value['salary_structure'];
                    $sal_structure_map_staff->save();
                }   
            }
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            // p($error_message);
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/payroll/salary-structure-map-staff')->withSuccess($success_msg);
    }
}
