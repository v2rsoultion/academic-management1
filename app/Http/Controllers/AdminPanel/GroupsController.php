<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\StudentGroups;
use App\Model\Student\StudentGroupMap;
use Yajra\Datatables\Datatables;

class GroupsController extends Controller
{
    /**
     *  View page for Student group
     *  @Shree on 31 Oct 2018
    **/
    public function student_group()
    {
        $group = [];
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_student_groups'),
            'redirect_url'  => url('admin-panel/groups/view-student-groups'),
            'login_info'    => $loginInfo,
            'group'         => $group
        );
        return view('admin-panel.groups.student_group')->with($data);
    }

    /**
     *  View page for parent group
     *  @Shree on 31 Oct 2018
    **/
    public function parent_group()
    {
        $group = [];
        $loginInfo              = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_parent_groups'),
            'redirect_url'  => url('admin-panel/groups/view-parent-groups'),
            'login_info'    => $loginInfo,
            'group'         => $group
        );
        return view('admin-panel.groups.parent_group')->with($data);
    }

    /**
     *  Add page for Group
     *  @Shree on 31 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   = [];
        $group  = [];
        $loginInfo = get_loggedin_user_data();
        $arr_group_type  = \Config::get('custom.group_type');
        if (!empty($id))
        {
            $decrypted_st_parent_group_id   = get_decrypted_value($id, true);
            $group                          = StudentGroups::Find($decrypted_st_parent_group_id);
            if (!$group)
            {
                return redirect('admin-panel/groups/add-group')->withError('Group not found!');
            }
            $page_title                     = trans('language.edit_group');
            $decrypted_st_parent_group_id   = get_encrypted_value($group->st_parent_group_id, true);
            $save_url                       = url('admin-panel/groups/save/' . $decrypted_st_parent_group_id);
            $submit_button                  = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_group');
            $save_url      = url('admin-panel/groups/save');
            $submit_button = 'Save';
        }
        $group['group_type']    = $arr_group_type;
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'group'         => $group,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/groups/view-student-groups'),
        );
        return view('admin-panel.groups.add')->with($data);
    }

    /**
     *  Add and update group's data
     *  @Shree on 31 Oct 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_st_parent_group_id = get_decrypted_value($id, true);
        $admin_id   = $loginInfo['admin_id'];
        $flag       = null;
        if (!empty($id))
        {
            $group = StudentGroups::find($decrypted_st_parent_group_id);
            $admin_id       = $group['admin_id'];
            if (!$group)
            {
                return redirect('/admin-panel/groups/add-group/')->withError('Group not found!');
            }
            $success_msg = 'Group updated successfully!';
        }
        else
        {
            $group     = New StudentGroups;
            $success_msg = 'Group saved successfully!';
        }
        if ($request->has('flag'))
        {
            $flag = Input::get('flag');
        }
        $validatior = Validator::make($request->all(), [
                'group_name'        => 'required|unique:st_parent_groups,group_name,' . $decrypted_st_parent_group_id . ',st_parent_group_id,flag,' . $flag,
                'flag'      => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $group->admin_id        = $admin_id;
                $group->update_by       = $loginInfo['admin_id'];
                $group->group_name      = Input::get('group_name');
                $group->flag            = Input::get('flag');
                $group->description     = Input::get('description');
                $group->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        if($flag == 0){
            return redirect('admin-panel/groups/view-student-groups')->withSuccess($success_msg);
        } else {
            return redirect('admin-panel/groups/view-parent-groups')->withSuccess($success_msg);
        }
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 31 Oct 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $group = StudentGroups::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')) && $request->get('name') != null)
            {
                $query->where('group_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('flag')) && $request->get('flag') != null)
            {
                $query->where('flag', "=", $request->get('flag'));
            }
        })->orderBy('st_parent_group_id', 'DESC')->get();
        
        return Datatables::of($group)
        
        ->addColumn('action', function ($group)
        {
            $encrypted_st_parent_group_id = get_encrypted_value($group->st_parent_group_id, true);
            if($group->status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="group-status/'.$status.'/' . $encrypted_st_parent_group_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-group/' . $encrypted_st_parent_group_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-group/' . $encrypted_st_parent_group_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy group's data
     *  @Shree on 31 Oct 2018
    **/
    public function destroy($id)
    {
        $st_parent_group_id = get_decrypted_value($id, true);
        $group    = StudentGroups::find($st_parent_group_id);
       
        $success_msg = $error_message =  "";
        if ($group)
        {
            DB::beginTransaction();
            try
            {
                $group->delete();
                $success_msg = "Group deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change class's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $st_parent_group_id = get_decrypted_value($id, true);
        $group    = StudentGroups::find($st_parent_group_id);
        if ($group)
        {
            $group->status  = $status;
            $group->save();
            $success_msg = "Group status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Group not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
