<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\RTECheques; // Model
use App\Model\FeesCollection\STCheques; // Model
use Yajra\Datatables\Datatables;


class RTEChequeController extends Controller
{
    /**
     *  View page for RTE cheque
     *  @shree on 2 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $cheque = [];
        $arr_bank               = get_all_bank();
        $cheque['arr_bank']     = add_blank_option($arr_bank, 'Select Bank');
        $data = array(
            'page_title'    => trans('language.view_rte_cheque'),
            'redirect_url'  => url('admin-panel/rte-cheque/view-rte-cheque'),
            'login_info'    => $loginInfo,
            'cheque'        => $cheque
        );
        return view('admin-panel.rte-cheque.index')->with($data);
    }

    /**
     *  Add page for RTE cheque
     *  @Shree on 2 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    	= [];
        $cheque      	= [];
        $loginInfo 	= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_rte_cheque_id 	= get_decrypted_value($id, true);
            $cheque      			    = RTECheques::Find($decrypted_rte_cheque_id);
            if (!$cheque)
            {
                return redirect('admin-panel/rte-cheque/add-rte-cheque')->withError('Bank not found!');
            }
            $page_title         = trans('language.edit_rte_cheque');
            $encrypted_rte_cheque_id  = get_encrypted_value($cheque->rte_cheque_id, true);
            $save_url           = url('admin-panel/rte-cheque/save/' . $encrypted_rte_cheque_id);
            $submit_button      = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_rte_cheque');
            $save_url      = url('admin-panel/rte-cheque/save');
            $submit_button = 'Save';
        }
        $arr_bank               = get_all_bank();
        $cheque['arr_bank']     = add_blank_option($arr_bank, 'Select Bank');
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'cheque' 		    => $cheque,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/rte-cheque/view-rte-cheque'),
        );
        return view('admin-panel.rte-cheque.add')->with($data);
    }

    /**
     *  Add and update cheque's data
     *  @Shree on 2 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      	      = get_loggedin_user_data();
        $decrypted_rte_cheque_id  = get_decrypted_value($id, true);
        $admin_id  = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $cheque       = RTECheques::find($decrypted_rte_cheque_id);
            if (!$cheque)
            {
                return redirect('/admin-panel/rte-cheque/add-rte-cheque/')->withError('Cheque not found!');
            }
            $success_msg = 'Cheque updated successfully!';
            $admin_id  = $cheque['admin_id'];
        }
        else
        {
            $cheque     = New RTECheques;
            $success_msg = 'Cheque saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'bank_id'           => 'required',
            'rte_cheque_no'     => 'required|unique:rte_cheques,rte_cheque_no,' . $decrypted_rte_cheque_id . ',rte_cheque_id',
            'rte_cheque_date'   => 'required',
            'rte_cheque_amt'    => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $cheque->admin_id           = $admin_id;
                $cheque->update_by          = $loginInfo['admin_id'];
                $cheque->bank_id            = Input::get('bank_id');
                $cheque->rte_cheque_no      = Input::get('rte_cheque_no');
                $cheque->rte_cheque_date    = Input::get('rte_cheque_date');
                $cheque->rte_cheque_amt     = Input::get('rte_cheque_amt');
                $cheque->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/rte-cheque/view-rte-cheque')->withSuccess($success_msg);
    }

    /**
     *  Get Cheque's Data for view page(Datatables)
     *  @Shree on 2 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $cheque  	= RTECheques::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('rte_cheque_no')) && $request->get('rte_cheque_no') != null)
            {
                $query->where('rte_cheque_no', "like", "%{$request->get('rte_cheque_no')}%");
            }
            if (!empty($request) && !empty($request->has('bank_id')) && $request->get('bank_id') != null)
            {
                $query->where('bank_id', "=",$request->get('bank_id'));
            }
        })->orderBy('rte_cheque_id', 'DESC')->with('getBankInfo')->get();
        // p($cheque);
        return Datatables::of($cheque)
        ->addColumn('bank_name', function ($cheque)
        {
            $bankName = $cheque['getBankInfo']['bank_name'];
            return $bankName;
        })
        ->addColumn('rte_cheque_amt', function ($cheque)
        {
            return "Rs ".$cheque->rte_cheque_amt;
        })
        ->addColumn('rte_cheque_date', function ($cheque)
        {
            $date = date("d M Y", strtotime($cheque->rte_cheque_date));
            return $date;
        })
        ->addColumn('status', function ($cheque)
        {
            if($cheque->rte_cheque_status == 0) {
                $status = "Pending";
            } else if($cheque->rte_cheque_status == 1) {
                $status = "Cancelled";
            } else if($cheque->rte_cheque_status == 2) {
                $status = "Cleared";
            } else if($cheque->rte_cheque_status == 3) {
                $status = "Bounce";
            }
            return $status;
        })
        ->addColumn('action', function ($cheque)
        {
            $encrypted_rte_cheque_id = get_encrypted_value($cheque->rte_cheque_id, true);
            if($cheque->rte_cheque_status != 2){
                return '
                <div class="dropdown">
                    <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="zmdi zmdi-label"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="rte-cheque-status/0/' . $encrypted_rte_cheque_id . '">Pending</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="rte-cheque-status/1/' . $encrypted_rte_cheque_id . '">Cancelled</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="rte-cheque-status/2/' . $encrypted_rte_cheque_id . '">Cleared</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="rte-cheque-status/3/' . $encrypted_rte_cheque_id . '">Bounce</a>
                        </li>
                    </ul>
                </div>';
            } else { return 'No action'; }
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Cheque's data
     *  @Shree on 2 Oct 2018.
    **/
    public function destroy($id)
    {
        $cheque_id 		= get_decrypted_value($id, true);
        $cheque 		= RTECheques::find($cheque_id);
        
        $success_msg = $error_message =  "";
        if ($cheque)
        {
            DB::beginTransaction();
            try
            {
                $cheque->delete();
                $success_msg = "Cheque deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/rte-cheque/view-rte-cheque')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/rte-cheque/view-rte-cheque')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Cheque not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Cheque's status
     *  @Shree on 2 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $cheque_id       = get_decrypted_value($id, true);
        $cheque          = RTECheques::find($cheque_id);
        if ($cheque)
        {
            $cheque->rte_cheque_status  = $status;
            $cheque->save();
            $success_msg = "Cheque status update successfully!";
            return redirect('admin-panel/rte-cheque/view-rte-cheque')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Cheque not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for student cheque
     *  @shree on 2 Oct 2018
    **/
    public function receive_cheques()
    {
        $loginInfo = get_loggedin_user_data();
        $cheque = [];
        $arr_bank               = get_all_bank();
        $cheque['arr_bank']     = add_blank_option($arr_bank, 'Select Bank');
        $data = array(
            'page_title'    => trans('language.cheque_details'),
            'redirect_url'  => url('admin-panel/receive-cheque/view-receive-cheque'),
            'login_info'    => $loginInfo,
            'cheque'        => $cheque
        );
        return view('admin-panel.rte-cheque.receive_cheques')->with($data);
    }

     /**
     *  Get Receive Cheque's Data for view page(Datatables)
     *  @Shree on 2 Oct 2018.
    **/
    public function receiveChequeData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $cheque  	= STCheques::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('student_cheque_no')) && $request->get('student_cheque_no') != null)
            {
                $query->where('student_cheque_no', "like", "%{$request->get('student_cheque_no')}%");
            }
            if (!empty($request) && !empty($request->has('bank_id')) && $request->get('bank_id') != null)
            {
                $query->where('bank_id', "=",$request->get('bank_id'));
            }
        })->orderBy('student_cheque_id', 'DESC')->with('getBankInfo')->with('getStudentInfo')->get();
        
        return Datatables::of($cheque)
        ->addColumn('student_name', function ($cheque)
        {
            $student_name = "----";
            if(!empty($cheque['getStudentInfo']['student_name'])) {
                $student_name = $cheque['getStudentInfo']['student_name'];
            }
            return $student_name;
        })
        
        ->addColumn('bank_name', function ($cheque)
        {
            $bankName = $cheque['getBankInfo']['bank_name'];
            return $bankName;
        })
        ->addColumn('student_cheque_amt', function ($cheque)
        {
            return "Rs ".$cheque->student_cheque_amt;
        })
        ->addColumn('student_cheque_date', function ($cheque)
        {
            $date = date("d M Y", strtotime($cheque->student_cheque_date));
            return $date;
        })
        ->addColumn('status', function ($cheque)
        {
            if($cheque->student_cheque_status == 0) {
                $status = "Pending";
            } else if($cheque->student_cheque_status == 1) {
                $status = "Cancelled";
            } else if($cheque->student_cheque_status == 2) {
                $status = "Cleared";
            } else if($cheque->student_cheque_status == 3) {
                $status = "Bounce";
            }
            return $status;
        })
        ->addColumn('action', function ($cheque)
        {
            $encrypted_student_cheque_id = get_encrypted_value($cheque->student_cheque_id, true);
            if($cheque->student_cheque_status != 2) {
                return '
                <div class="dropdown">
                    <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="zmdi zmdi-label"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="receive-cheque-status/0/' . $encrypted_student_cheque_id . '">Pending</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="receive-cheque-status/1/' . $encrypted_student_cheque_id . '">Cancelled</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="receive-cheque-status/2/' . $encrypted_student_cheque_id . '">Cleared</a>
                        </li>
                        <li>
                            <a onclick="return confirm('."'Are you sure?'".')" href="receive-cheque-status/3/' . $encrypted_student_cheque_id . '">Bounce</a>
                        </li>
                    </ul>
                </div>';
        } else { return 'No action'; }
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    
    /**
     *  Change Cheque's status
     *  @Shree on 2 Oct 2018.
    **/
    public function changeRChequeStatus($status,$id)
    {
        $cheque_id       = get_decrypted_value($id, true);
        $cheque          = STCheques::find($cheque_id);
        if ($cheque)
        {
            $cheque->student_cheque_status  = $status;
            $cheque->save();
            $success_msg = "Cheque status update successfully!";
            return redirect('admin-panel/receive-cheque/view-receive-cheque')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Cheque not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
