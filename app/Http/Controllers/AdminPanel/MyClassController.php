<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Staff\Staff; // Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model
use App\Model\StudentLeaveApplication\StudentLeaveApplication; // Model
use App\Model\Student\StudentAttendance; // Model
use App\Model\Student\StudentAttendanceDetails; // Model
use App\Model\Student\Student; // Model\
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model
use App\Model\HomeworkGroup\HomeworkGroup; // Model
use App\Model\HomeworkGroup\HomeworkConv; // Model
use App\Model\Remarks\Remarks; // Model
use App\Model\Section\Section; // Model
use Yajra\Datatables\Datatables;

class MyClassController extends Controller
{
    public function get_staff_class_info(){
        $loginInfo  = get_loggedin_user_data();
        $staff_id = $loginInfo['staff_id'];
        $class_info = ClassTeacherAllocation::where('staff_id', '=', $staff_id)->get()->toArray();
        return $class_info;
    }

    public function view_students(){
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $data = array(
            'page_title'    => trans('language.view_students'),
            'redirect_url'  => url('admin-panel/my-class/view-students'),
            'login_info'    => $loginInfo,
            'staff_class_info'    => $staff_class_info,
        );
        return view('admin-panel.my-class.view_students')->with($data);
    }

    /**
     *  Get student list
     *  @Shree on 2 Nov 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('profile
            ', function ($student)
            {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                
                return ' 
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="../student/student-profile/' . $encrypted_student_id . '" "">View Profile</a>
                    </li>
                 </ul>
             </div>
            ';
            })
            ->rawColumns(['student_profile'=>'student_profile', 'profile'=> 'profile'])->addIndexColumn()->make(true);
    }

    public function view_student_leaves(){
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $data = array(
            'page_title'        => trans('language.view_student_leaves'),
            'redirect_url'      => url('admin-panel/my-class/view-leaves'),
            'login_info'        => $loginInfo,
            'staff_class_info'  => $staff_class_info,
        );
        return view('admin-panel.my-class.view_student_leaves')->with($data);
    }

    /**
     *  Get student leave list
     *  @Shree on 2 Nov 2018
    **/
    public function get_student_leaves(Request $request)
    {
        $leaves = [];
        $session    = get_current_session();
        $leaves     = StudentLeaveApplication::where(function($query) use ($request,$session) 
        {
            // $query->where('session_id', "=", $session['session_id']);
            
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'student_leaves.student_id');
            $join->where('current_session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            
        })->orderBy('student_leave_id', 'DESC')
        ->with('getStudent')->get()->toArray();
        // p($leaves);
        return Datatables::of($leaves)
            ->addColumn('student_profile', function ($leaves)
            {
                $profileImg = '';
                $profile = check_file_exist($leaves['get_student']['student_image'], 'student_image');
                if (!empty($profile))
                {
                    $profileImg = $profile;
                }
                if($profileImg != ''){
                    $profile = "<img src=".$profileImg." height='30' />";
                }   
                $name = $leaves['get_student']['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('student_enroll_number', function ($leaves)
            {
                $student_enroll_number = "";
                $student_enroll_number = $leaves['get_student']['student_enroll_number'];
                return $student_enroll_number;
            })
            ->addColumn('date_range', function ($leaves)
            {
                $date_range = "";
                $date_range = date("d F Y", strtotime($leaves['student_leave_from_date'])).' - '.date("d F Y", strtotime($leaves['student_leave_from_date']));
                return $date_range;
            })
            ->addColumn('leave_status', function ($leaves)
            {
                if($leaves['student_leave_status'] == 0){
                    $status = '<span class="badge badge-primary">Disapproved</span>';
                } else {
                    $status = '<span class="badge badge-success">Approved</span>';
                }
                
                return $status;
            })
            ->addColumn('attachment', function ($leaves)
            {
                $attachmentLink = '----';
                $attechmentUrl = check_file_exist($leaves['student_leave_attachment'], 'student_leave_document_file');
                if (!empty($attechmentUrl))
                {
                    $attachmentLink = '<a href="../' . $attechmentUrl . '" "">Attachment</a>';
                }
                return $attachmentLink;
            })
            ->addColumn('action', function ($leaves)
            {
                $encrypted_student_leave_id = get_encrypted_value($leaves['student_leave_id'], true);
                if($leaves['student_leave_status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return ' 
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                        <a href="'.url('admin-panel/student-leave-application/student-leave-application-status/1/'.$encrypted_student_leave_id.' ').'">Approved</a>
                        </li>
                        <li>
                        <a href="'.url('admin-panel/student-leave-application/student-leave-application-status/0/'.$encrypted_student_leave_id.' ').'">Disapproved</a>
                    </li>
                    </ul>
                </div>
                ';
            })
            ->rawColumns(['student_profile'=>'student_profile', 'action'=> 'action', 'attachment'=> 'attachment', 'date_range'=> 'date_range', 'student_enroll_number'=> 'student_enroll_number', 'leave_status'=> 'leave_status'])->addIndexColumn()->make(true);
    }

    public function add_remark(Request $request, $id = NULL){
        
        $data = $remark = [];
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $arr_students = get_student_list_class_section($staff_class_info['class_id'],$staff_class_info['section_id']);
        $arr_subjects = get_subject_list_class_section($staff_class_info['class_id'],$staff_class_info['section_id']);
        if (!empty($id))
        {
            $decrypted_remark_id = get_decrypted_value($id, true);
            $remark 			 = Remarks::Find($decrypted_remark_id);
            if (!$remark)
            {
                return redirect()->back()->withErrors('Remark not found!');
            }
            $page_title          = trans('language.edit_remark');
            $encrypted_remark_id = get_encrypted_value($remark->remark_id, true);
            $save_url            = url('admin-panel/my-class/remarks/save-remark/' . $encrypted_remark_id);
            $submit_button       = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_remark');
            $save_url      = url('admin-panel/my-class/remarks/save-remark');
            $submit_button = 'Save';
        }
        $remark['arr_students']   = add_blank_option($arr_students, 'Select Student');
        $remark['arr_subjects']   = add_blank_option($arr_subjects, 'Select Subject');
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'remark' 	        => $remark,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/my-class/remarks/view-remarks'),
        );

        return view('admin-panel.remarks.add')->with($data);
    }

    /**
     *  Add and update Remark's data
     *  @Shree on 6 Nov 2018.
    **/
    public function save_remark(Request $request, $id = NULL)
    {
        $loginInfo      		= get_loggedin_user_data();
        $decrypted_remark_id	= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        $session    = get_current_session();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        if (!empty($id))
        {
            $remark = Remarks::find($decrypted_remark_id);
            $admin_id = $remark['admin_id'];
            if (!$remark)
            {
                redirect()->back()->withInput()->withErrors('Remark not found!');
            }
            $success_msg = 'Remark updated successfully!';
        }
        else
        {
            $remark     	= New Remarks;
            $success_msg 	= 'Remark saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'student_id'   => 'required',
                'subject_id'   => 'required',
                'remark_text'   => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $remark->admin_id       = $admin_id;
                $remark->update_by      = $loginInfo['admin_id'];
                $remark->session_id 	= $session['session_id'];
                $remark->class_id 	    = $staff_class_info['class_id'];
                $remark->section_id 	= $staff_class_info['section_id'];
                $remark->staff_id 	    = $staff_class_info['staff_id'];
                $remark->student_id 	= Input::get('student_id');
                $remark->subject_id 	= Input::get('subject_id');
                $remark->remark_text 	= Input::get('remark_text');
                $remark->remark_date 	= date('Y-m-d');
                $remark->save();

                $subject_name = get_subject_name($request->get('subject_id'));
                $send_notification  = $this->send_push_notification($remark->remark_id,$staff_class_info['staff_id'],$request->get('student_id'),$subject_name,"","",'student_dairy_remark_by_staff',"","");
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/my-class/remarks/view-remarks')->withSuccess($success_msg);
    }

    /**
     *  View page for remarks
     *  @Shree on 6 Nov 2018
    **/
    public function view_remarks(Request $request)
    {
        $remark = [];
        $loginInfo              = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $arr_students = get_student_list_class_section($staff_class_info['class_id'],$staff_class_info['section_id']);
        $arr_subjects = get_subject_list_class_section($staff_class_info['class_id'],$staff_class_info['section_id']);

        $remark['arr_students']   = add_blank_option($arr_students, 'Select Student');
        $remark['arr_subjects']   = add_blank_option($arr_subjects, 'Select Subject');
        $data = array(
            'page_title'   => trans('language.view_remarks'),
            'redirect_url' => url('admin-panel/my-class/remarks/view-remarks'),
            'login_info'    => $loginInfo,
            'remark'       => $remark
        );
        return view('admin-panel.remarks.index')->with($data);
    }

    /**
     *  Get Remarks's Data for view page(Datatables)
     *  @Shree on 6 Nov 2018.
    **/
    public function reviewData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $remark   = Remarks::where(function($query) use ($request,$staff_class_info) 
        {
            if (!empty($staff_class_info) && !empty($staff_class_info['class_id']) && $staff_class_info['class_id'] != null)
            {
                $query->where('class_id', "=", $staff_class_info['class_id']);
            }
            if (!empty($staff_class_info) && !empty($staff_class_info['section_id']) && $staff_class_info['section_id'] != null)
            {
                $query->where('section_id', "=", $staff_class_info['section_id']);
            }
            if (!empty($request) && !empty($request->has('student_id')) && $request->get('student_id') != null)
            {
                $query->where('student_id', "=", $request->get('student_id'));
            }
            if (!empty($request) && !empty($request->has('subject_id')) && $request->get('subject_id') != null)
            {
                $query->where('subject_id', "=", $request->get('subject_id'));
            }
        })->with('getStudent')->with('getSubject')->orderBy('remark_id', 'ASC')->get();
       
        return Datatables::of($remark)
        ->addColumn('subject_name', function ($remark)
            {
                $subjectName = $remark->getSubject['subject_name'].' - '.$remark->getSubject['subject_code'];
                return $subjectName;
                
            })
            ->addColumn('student_name', function ($remark)
            {
                $studentName = $remark->getStudent['student_name'];
                return $studentName;
                
            })
            ->addColumn('remark_date', function ($remark)
            {
                $remark_date = date("d F Y", strtotime($remark->remark_date));
                return $remark_date;
                
            })
        ->addColumn('action', function ($remark)
        {
            $encrypted_remark_id = get_encrypted_value($remark->remark_id, true);
            
            return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit">
                    <a href="'.url('admin-panel/my-class/remarks/add-remark/'.$encrypted_remark_id.' ').'"><i class="zmdi zmdi-edit"></i></a>
                </div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete">
                <a href="'.url('admin-panel/my-class/remarks/delete-remark/'.$encrypted_remark_id.' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a>
                </div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy remark's data
     *  @Shree on 6 Nov 2018.
    **/
    public function destroy_remark($id)
    {
        $remark_id  = get_decrypted_value($id, true);
        $remark     = Remarks::find($remark_id);
        
        $success_msg = $error_message =  "";
        if ($remark)
        {
            DB::beginTransaction();
            try
            {
                $remark->delete();
                $success_msg = "Remark deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Remark not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function add_attendance(Request $request, $id = NULL){
        
        $data = $attendance = [];
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $holiday_list = get_school_all_holidays(1);
      
        if (!empty($id))
        {
            $decrypted_attendance_id = get_decrypted_value($id, true);
            $attendance 			 = StudentAttendance::Find($decrypted_attendance_id)->toArray();
            if (!$attendance)
            {
                return redirect()->back()->withErrors('Attendance not found!');
            }
            $page_title          = trans('language.edit_attendance');
            $encrypted_attendance_id = get_encrypted_value($attendance['student_attendance_id'], true);
            $save_url            = url('admin-panel/my-class/attendance/save-attendance/' . $encrypted_attendance_id);
            $submit_button       = 'Update';
            $currentDate = $attendance['student_attendance_date'];
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;
        }
        else
        {
            $page_title    = trans('language.add_attendance');
            $save_url      = url('admin-panel/my-class/attendance/save-attendance');
            $submit_button = 'Save';
            $currentDate = date('Y-m-d');
            $day = date('l', strtotime($currentDate));
            $attendance['currentDate'] = $currentDate;
            $attendance['day'] = $day;
            $attendance['holiday'] = 0;
        }
        if(in_array($currentDate,$holiday_list)){
            $attendance['holiday'] = 1;
        }
        
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'attendance' 	    => $attendance,
            'login_info'    	=> $loginInfo,
            'staff_class_info'  => $staff_class_info,
            'redirect_url'  	=> url('admin-panel/my-class/attendance/view-attendance'),
        );

        return view('admin-panel.student-attendance.add')->with($data);
    }

    public function attendance_data(Request $request){
        $student  = $arr_attendance   = [];
        //p($request->all());
        $attendance_mark_status = $this->check_attendance_exist($request);
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            if($request->get('holiday_status') != null && $request->get('holiday_status') == 0){
                $arr_students = get_all_student_for_attendance($request);
            } else {
                $arr_students = [];
            }
        } else {
            $arr_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        // p($student);
        return Datatables::of($student) 
            ->addColumn('student_profile', function ($student)
            {
                $profile = '';
                if($student->profile != ''){
                    $profile = "<img src=".url($student->profile)." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('student_enroll_number', function ($student)
            {
                $student_enroll_number = "";
                $student_enroll_number = $student->student_enroll_number;
                return $student_enroll_number;
            })
            ->addColumn('attendance', function ($student) use ($attendance_mark_status)
            {
                if($attendance_mark_status == 1 && $student->student_attend_d_id == NULL){
                    return "Not Available";
                }
                $present = $absent = '';
                $present = "checked";
                if($student->student_attendance_type == 1){
                    $present = "checked";
                } else if($student->student_attendance_type == '0' ){
                    $absent = "checked";
                } else if($student->student_attendance_type == ''){
                    $present = "checked";
                }
                $attendance = '
                    <input type="hidden" name="attendance['.$student->student_id.'][student_attend_d_id]"  value="'.$student->student_attend_d_id.'" >

                    <input type="hidden" name="attendance['.$student->student_id.'][student_id]"  value="'.$student->student_id.'" >

                    <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status1'.$student->student_id.'" name="attendance['.$student->student_id.'][attendance]" '.$present.' type="radio" value="1" >
                        <label for="attendance_status1'.$student->student_id.'" class="document_staff">Present</label>
                    </div>
                    <div class="red float-left" style="margin-top:6px !important;">
                        <input id="attendance_status2'.$student->student_id.'" name="attendance['.$student->student_id.'][attendance]" '.$absent.' type="radio" value="0" />
                        <label for="attendance_status2'.$student->student_id.'" class="document_staff">Absent</label> 
                    </div>
                    ';
                return $attendance;
            })
            
            ->rawColumns(['student_profile'=>'student_profile', 'attendance'=> 'attendance', 'student_enroll_number'=> 'student_enroll_number'])->addIndexColumn()->make(true);
    }
    public function check_attendance_exist(Request $request){
        $attendance_mark_status = 0;
        $check_status = StudentAttendance::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $query->where('student_attendance_date',$request->get('attendance_date'));
                }
                if (!empty($request) && !empty($request->get('student_attendance_id')) && $request->get('student_attendance_id') != null){
                    $query->where('student_attendance_id',$request->get('student_attendance_id'));
                }
            })
            ->get()->toArray();
        if(!empty($check_status)){
            $attendance_mark_status = 1;
        }
        return $attendance_mark_status;
    }
    public function attendance_save(Request $request, $id = NULL){
        // p($request->all());
        $total_present_students = $total_absent_students = 0;

        $loginInfo      		            = get_loggedin_user_data();
        $decrypted_student_attendance_id	= get_decrypted_value($id, true);
        $admin_id                           = $loginInfo['admin_id'];
        $session    = get_current_session();
        if (!empty($id)) {
            $attendance = StudentAttendance::find($decrypted_student_attendance_id);
            $admin_id   = $attendance['admin_id'];
            if (!$attendance)
            {
                return redirect('/admin-panel/my-class/attendance/view-attendance/')->withError('Attendance not found!');
            }
            $success_msg = 'Attendance updated successfully!';
        }
        else {
            $attendance     = New StudentAttendance;
            $success_msg = 'Attendance saved successfully!';
        }
        DB::beginTransaction();
        try
        {
            $attendance = StudentAttendance::where('student_attendance_date',Input::get('attendance_date'))->first();
            if(empty($attendance)){
                $attendance     = New StudentAttendance;
            }
            $attendance->admin_id       = $admin_id;
            $attendance->update_by      = $loginInfo['admin_id'];
            $attendance->session_id 	= $session['session_id'];
            $attendance->class_id 	    = Input::get('class_id');
            $attendance->section_id 	= Input::get('section_id');
            $attendance->student_attendance_date 	= Input::get('attendance_date');
            $attendance->save();
            
            foreach($request->get('attendance') as $details){ 
                if($details['attendance'] == 0){
                    $total_absent_students++;
                }
                if($details['attendance'] == 1){
                    $total_present_students++;
                }
                if(isset($details['student_attend_d_id']) ) {
                    
                    $student_attend = StudentAttendanceDetails::where('student_attend_d_id', $details['student_attend_d_id'])->first();
                    $student_attend->admin_id               = $admin_id;
                    $student_attend->update_by              = $loginInfo['admin_id'];
                    $student_attend->student_attendance_id  = $attendance->student_attendance_id;
                    $student_attend->session_id             = $session['session_id'];
                    $student_attend->class_id 	            = Input::get('class_id');
                    $student_attend->section_id 	        = Input::get('section_id');
                    $student_attend->student_id             = $details['student_id'];
                    $student_attend->student_attendance_date    = Input::get('attendance_date');
                    $student_attend->student_attendance_type    = $details['attendance'];
                    $student_attend->save();
                } else {
                    $student_attend = New StudentAttendanceDetails();
                    $student_attend->admin_id               = $admin_id;
                    $student_attend->update_by              = $loginInfo['admin_id'];
                    $student_attend->student_attendance_id  = $attendance->student_attendance_id;
                    $student_attend->session_id             = $session['session_id'];
                    $student_attend->class_id 	            = Input::get('class_id');
                    $student_attend->section_id 	        = Input::get('section_id');
                    $student_attend->student_id             = $details['student_id'];
                    $student_attend->student_attendance_date    = Input::get('attendance_date');
                    $student_attend->student_attendance_type    = $details['attendance'];
                    $student_attend->save();
                    if($details['attendance'] == 0){
                        $type = "Absent";
                    } else {
                        $type = "Present";
                    }
                    $send_notification  = $this->send_push_notification($student_attend->student_attend_d_id,"",$details['student_id'],"","","",'student_attendance',$type,$student_attend->student_attendance_date);
                    
                }
            }
            $attendance->total_present_student 	= $total_present_students;
            $attendance->total_absent_student 	= $total_absent_students;
            $attendance->save();
            
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/my-class/attendance/view-attendance')->withSuccess($success_msg);
    }

    /**
     *  View page for remarks
     *  @Shree on 6 Nov 2018
    **/
    public function view_attendance(Request $request)
    {
        $attendance = [];
        $loginInfo              = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
       
        $data = array(
            'page_title'   => trans('language.view_attendance'),
            'redirect_url' => url('admin-panel/my-class/attendance/view-attendance'),
            'login_info'    => $loginInfo,
            'attendance'       => $attendance
        );
        return view('admin-panel.student-attendance.index')->with($data);
    }

    public function attendance_records(Request $request){
        $arr_attendance   = [];
        $session    = get_current_session();
        $loginInfo = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        $arr_attendance   = StudentAttendance::where(function($query) use ($request,$staff_class_info,$session) 
        {
            if (!empty($staff_class_info) && !empty($staff_class_info['class_id']) && $staff_class_info['class_id'] != null)
            {
                $query->where('class_id', "=", $staff_class_info['class_id']);
            }
            if (!empty($staff_class_info) && !empty($staff_class_info['section_id']) && $staff_class_info['section_id'] != null)
            {
                $query->where('section_id', "=", $staff_class_info['section_id']);
            }
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->has('student_attendance')) && $request->get('student_attendance') != null)
            {
                $query->where('student_attendance_date', "=", $request->get('student_attendance'));
            }
        })->orderBy('student_attendance_date', 'DESC')->get();
       
        
        return Datatables::of($arr_attendance) 
            ->addColumn('student_attendance', function ($arr_attendance)
            {
                return date('d M Y', strtotime($arr_attendance->student_attendance_date));
            })
            
            ->addColumn('action', function ($arr_attendance)
            {
                $encrypted_student_attendance_id = get_encrypted_value($arr_attendance->student_attendance_id, true);
                
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit">
                        <a href="'.url('admin-panel/my-class/attendance/add-attendance/'.$encrypted_student_attendance_id.' ').'"><i class="zmdi zmdi-edit"></i></a>
                    </div>
                    ';
            })
            
            ->rawColumns(['student_attendance'=>'student_attendance',  'action'=> 'action'])->addIndexColumn()->make(true);
    }

    public function show_class_subjects(){
        $subjects = $arr_section =  [];
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];
        
        $subjects['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $subjects['arr_section'] = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.menu_my_subject'),
            'redirect_url'  => url('admin-panel/my-subjects'),
            'login_info'    => $loginInfo,
            'subjects'      => $subjects,
        );
        return view('admin-panel.my-subjects.index')->with($data);
    }

    public function send_push_notification($module_id,$staff_id,$student_id,$subject_name,$class_id,$section_id,$notification_type,$attendance_type,$attendance_date){
        $device_ids = [];
        $session       = get_current_session();
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            if (!empty($student_id) && !empty($student_id) && $student_id != null){
                $query->where('students.student_id', $student_id);
            }
            $query->where('students.student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($class_id,$section_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('students.student_id','students.student_name','students.student_parent_id','students.reference_admin_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];

        if($student_info['get_admin_info']['fcm_device_id'] != ""  && $student_info['get_admin_info']['notification_status'] == 1){
            if($notification_type != 'student_attendance'){
                $student_admin_id = $student_info['get_admin_info']['admin_id'];
                $device_ids[] = $student_info['get_admin_info']['fcm_device_id'];
            }
        }
        if($student_info['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student_info['get_parent']['get_parent_admin_info']['notification_status'] == 1){
            $device_ids[] = $student_info['get_parent']['get_parent_admin_info']['fcm_device_id'];
            $parent_admin_id = $student_info['get_parent']['get_parent_admin_info']['admin_id'];
        }
        if($notification_type == 'student_dairy_remark_by_staff'){
            $message = $student_info['student_name']." received remark in ".$subject_name;
        }  else if($notification_type == 'student_attendance'){
            $message = $student_info['student_name']." is ".$attendance_type." in school on ".date("d F Y", strtotime($attendance_date)).".";
        }
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => $student_admin_id,
            'parent_admin_id' => $parent_admin_id,
            'staff_admin_id' => Null,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }

    public function  homework(){
        $loginInfo = get_loggedin_user_data();
        $session                = get_current_session();
        $decrypted_homework_group_id = get_decrypted_value($id, true);
        $decrypted_subject_id = get_decrypted_value($subject, true);
        $subject = get_all_section_subjects($loginInfo['section_id']);
        $homework['subject'] = add_blank_option($subject, 'Select Subject');
        $conversations = HomeworkGroup::where([['class_id', $loginInfo['class_id']],['section_id', $loginInfo['section_id']]])->with('getClass')->with('getSection')->with(['getMessages' => function($query) use ($decrypted_subject_id) {
            $query->where('subject_id', $decrypted_subject_id);
            $query->orderBy('h_conversation_id', 'ASC');
        }])->first();
        $all_hw = HomeworkConv::where('h_session_id', "=", $session['session_id'])->where('homework_group_id', "=", $conversations['homework_group_id'])->with('getStaff')->with('getSubject')->get()->toArray();
        
        foreach($all_hw as $hw) {
            $staff_img = "public/images/default.png"; 
            if (!empty($hw['get_staff']['staff_profile_img']))
            {
                $profile = check_file_exist($hw['get_staff']['staff_profile_img'], 'staff_profile');
                if (!empty($profile))
                {
                    $staff_img = $profile;
                }
            }
            $homeworkData[] = array(
                'h_conversation_id' => $hw['h_conversation_id'],
                'homework_group_id' => $hw['homework_group_id'],
                'staff_id' => $hw['staff_id'],
                'subject_id' => $hw['subject_id'],
                'h_conversation_text' => $hw['h_conversation_text'],
                'time' => time_elapsed_string($hw['created_at']),
                'staff_name' => $hw['get_staff']['staff_name'],
                'staff_profile_img' => $staff_img,
                'attechment' => $hw['h_conversation_attechment'],
                'subject_name' => $hw['get_subject']['subject_name'].' - '.$hw['get_subject']['subject_code'],
            );
            
        }

        $data = array(
            'page_title'        => trans('language.homework_group'),
            'redirect_url'      => url('admin-panel/my-subjects/homework-group/'.$id),
            'login_info'        => $loginInfo,
            'conversations'     => $conversations,
            'homework_data'     => $homeworkData,
            'homework'          => $homework,
        );
        return view('admin-panel.staff-class-homework.index')->with($data);
    }

    public function  send_homework(Request $request){
        
        $loginInfo = get_loggedin_user_data();
        $staff_id  = $loginInfo['staff_id'];
        $message_text       = Input::get('message');
        $subject_id         = Input::get('subject_id');
        $homework_group_id  = Input::get('homework_group_id');
        $subject_name = get_subject_name($subject_id);
        $staff_info = get_staff_signle_data($staff_id);
        $staff_info = isset($staff_info[0]) ? $staff_info[0] : [];
        $session    = get_current_session();
        $group = HomeworkGroup::where('homework_group_id', "=", $homework_group_id)->with('getClass')->with('getSection')->first();

        $message_date = '';
        if ( !empty($message_text) && !empty($homework_group_id) ){

            $homework = New HomeworkConv();
            DB::beginTransaction();
            try
            {
                $homework->admin_id       = $loginInfo['admin_id'];
                $homework->update_by      = $loginInfo['admin_id'];
                $homework->h_session_id   = $session['session_id'];
                $homework->h_conversation_text  = $message_text;
                $homework->staff_id             = $staff_id;
                $homework->subject_id           = $subject_id;
                $homework->homework_group_id    = $homework_group_id;
                $homework->save();
                if ($request->hasFile('h_conversation_attechment'))
		        {
		            $file                          = $request->File('h_conversation_attechment');
		            $config_document_upload_path   = \Config::get('custom.conversation_attechment');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
                    $name                          = substr($file->getClientOriginalName(),0,-4);
                    if($ext == 'docx'){
                        $ext = ".docx";
                    }
                    if($ext == 'jpeg'){
                        $ext = ".jpeg";
                    }
                    if($ext == 'xlsx'){
                        $ext = ".xlsx";
                    }
		            $filename                      = $homework->h_conversation_id.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
                    $homework->h_conversation_attechment = $filename;
                    $homework->save();
                }
                $subject_name = get_subject_name($subject_id);
                $send_notification  = $this->send_push_notification_class($homework_group_id,$group->class_id,$group->section_id,'student_homework_by_staff');
                $message_date = $homework->created_at;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return response()->json(['status'=>false, 'error_message'=> $error_message]);
            }
            DB::commit();

            if(!empty($staff_info["staff_profile_img"])){
                $staff_img = '<img src="'.url($staff_info["staff_profile_img"]).'" class="float-right" title="preview" alt="preview">';
            } else {
                $default_img = "public/images/default.png"; 
                $staff_img = '<img src="'.url($default_img).'" class="float-right" title="preview" alt="preview">';
            }
            $user_message = '<li class="clearfix message-animation"> <div class="message my-message float-right"> Subject: '.$subject_name.' <br /><br /> '.$message_text.' <span class="chat-staff-name">'.$staff_info["staff_name"].'</span> </div><div class="clearfix"></div> '.$staff_img.' <div class="message-data float-right"> <span class="message-data-time"> '.time_elapsed_string($message_date).' </span> &nbsp; </div><div class="clearfix"></div></li>';
            
            return response()->json(['status'=>true, 'user_message'=> $user_message ]);

        }
    }

    public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();
        $message = "Today's homework is updated.";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name'].":".$section['section_class']['class_id']."-".$section['section_id'];

            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name'].":".$section['section_class']['class_id']."-".$section['section_id'];

            $students = pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],1);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['section_class']['class_id'],$section['section_id'],2);
        }
        
    }
}
