<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\TimeTable\TimeTable; // Model 
use App\Model\TimeTable\TimeTableMap; // Model 
use Yajra\Datatables\Datatables;

class TimeTableController extends Controller
{
    
    /**
     *  View page for time table
     *  @Khushbu on 06 Sept 2018
    **/
    public function index()
    {
        $time_table               = [];
        $loginInfo                = get_loggedin_user_data();
        $arr_class                = get_all_classes_mediums();
        $time_table['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $data                     = array(
            'page_title'          => trans('language.view_time_table'),
            'redirect_url'        => url('admin-panel/time-table/view-time-table'),
            'login_info'          => $loginInfo,
            'time_table'          => $time_table
        );
        return view('admin-panel.time-table.index')->with($data);
    }

    /**
     *  View page for time table
     *  @Khushbu on 06 Sept 2018
    **/
    public function set_time_table($id)
    {
        $time_table = $arr_staff = [];
        $loginInfo               = get_loggedin_user_data();
        $arr_class               = get_all_classes_mediums();
        $decrypted_time_table_id = get_decrypted_value($id, true);
        $time_table              = TimeTable::find($decrypted_time_table_id);
        $time_map                = TimeTableMap::where('time_table_id', '=', $time_table->time_table_id)->with('getStaff')->with('getSubject')->get()->toArray(); 
        $time_table->time_table_week_days = explode(',',$time_table->time_table_week_days);
        $arr_subject              = get_subject_list_class_section($time_table->class_id,$time_table->section_id);
        $time_table['arr_subject']  = add_blank_option($arr_subject, 'Select Subject');
        $time_table['arr_staff']  = add_blank_option($arr_staff, 'Select Staff');
        $data                     = array(
            'page_title'          => trans('language.set_time_table'),
            'redirect_url'        => url('admin-panel/time-table/set-time-table/'.$id),
            'login_info'          => $loginInfo,
            'time_table'          => $time_table,
            'time_map'            => $time_map,
        );
        return view('admin-panel.time-table.set-time-table')->with($data);
    }

    /**
     *  Add page for time table
     *  @Khushbu on 06 Sept 2018
    **/
    public function add(Request $request, $id = null) 
    {
    	$data    		= [];
        $time_table	 = $arr_section  = [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_time_table_id 	= get_decrypted_value($id, true);
            $time_table                 = $this->getTimeTableData($decrypted_time_table_id,"");
            $time_table                 = isset($time_table[0]) ? $time_table[0] : [];
            if (!$time_table)
            {
                return redirect('admin-panel/time-table/add-time-table')->withError('Time table not found!');
            }
            $page_title               = trans('language.edit_time_table');
            $encrypted_time_table_id  = get_encrypted_value($time_table['time_table_id'], true);
            $save_url                 = url('admin-panel/time-table/save/' . $encrypted_time_table_id);
            $submit_button            = 'Update';
            $arr_section              = get_class_section($time_table['class_id']);
            $week_days                = [];
            $week                     = [];
            if(!empty($time_table['week_days'])){
                $time_table['week_days'] = explode(',',$time_table['week_days']);
            }
            $week_key                 = $time_table['week_days'];
        }
        else
        {
            $page_title                = trans('language.add_time_table');
            $save_url                  = url('admin-panel/time-table/save');
            $submit_button             = 'Save';
        }
        $arr_class                      = get_all_classes_mediums();
        $time_table['arr_class']        = add_blank_option($arr_class, 'Select Class');
        $time_table['arr_section']      = add_blank_option($arr_section, 'Select Section');
        $arr_week_days                  = \Config::get('custom.week_days');
        $time_table['arr_week_days']    = $arr_week_days;
        // $time_table['arr_week_days']   = add_blank_option($arr_week_days, 'Nothing selected');
        
        $data                          = array(
            'page_title'    	       => $page_title,
            'save_url'      	       => $save_url,
            'submit_button' 	       => $submit_button,
            'time_table'               => $time_table,
            'login_info'    	       => $loginInfo,
            'redirect_url'  	       => url('admin-panel/time-table/view-time-table'),
        );
        return view('admin-panel.time-table.add')->with($data);
    }
    /**
     *  Add and update time-table's data
     *  @Khushbu on 06 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo     = get_loggedin_user_data();
        $session       = get_current_session();
        $admin_id      = $loginInfo['admin_id'];
        $decrypted_time_table_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $time_table = TimeTable::find($decrypted_time_table_id);
            $admin_id   = $time_table['admin_id'];
            if (!$time_table)
            {
                return redirect('admin-panel/time-table/view-time-table/')->withError('Time table not found!');
            }
            $success_msg = 'Time table updated successfully!';
        }
        else
        { 
            $time_table      =  New TimeTable;
            $success_msg     = 'Time table saved successfully!';
        }
        $class_id  =    $section_id    = null;
        $status     = 1;
        if ($request->has('class_id'))
        {
            $class_id   = Input::get('class_id');
        }
        if ($request->has('section_id'))
        {
            $section_id = Input::get('section_id');
        }
        $validatior   = Validator::make($request->all(), [
                'name'        => 'required|unique:time_tables,time_table_name,' . $decrypted_time_table_id . ',time_table_id,class_id,' . $class_id . ',section_id,' . $section_id,
                'class_id'    => 'required|unique:time_tables,class_id,' . $decrypted_time_table_id . ',time_table_id,class_id,' . $class_id . ',section_id,' . $section_id. ',time_table_status,' . $status,
                'section_id'  => 'required',
                'week_days'   => 'required',
                'lecture_no'  => 'required',
        ], [
            'class_id.unique' => 'Class already exist with this section.'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $time_table->admin_id            = $admin_id;
                $time_table->update_by           = $loginInfo['admin_id'];
                $time_table->session_id          = $session['session_id'];
                $time_table->time_table_name     = Input::get('name');
                $time_table->class_id            = Input::get('class_id');
                $time_table->section_id          = Input::get('section_id');
                $time_table->lecture_no          = Input::get('lecture_no');
                $week_days1                      =  Input::get('week_days');
                $week_day                        = implode(",", $week_days1);
                $time_table->time_table_week_days= $week_day;
                $time_table->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 07 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $time_table     = [];
        $arr_time_table = $this->getTimeTableData("",$request);
        foreach ($arr_time_table as $key => $time_table_data)
        {
            $time_table[$key] = (object) $time_table_data;
        }
        return Datatables::of($time_table)  
            ->addColumn('action', function ($time_table)
            {
                $encrypted_time_table_id = get_encrypted_value($time_table->time_table_id, true);
                if($time_table->time_table_status == 0) {
                    $status    = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status    = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '

                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                            <a title="Set Time Table" href="'.url('admin-panel/time-table/set-time-table/' . $encrypted_time_table_id).'" "">Set Time Table</a>
                        </li>
                    
                    </ul>
                </div>
                <div class="pull-left"><a href="time-table-status/'.$status.'/' . $encrypted_time_table_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-time-table/' . $encrypted_time_table_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-time-table/' . $encrypted_time_table_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Get Time Table data
     *  @Khushbu on 07 Sept 2018
    **/
    public function getTimeTableData($time_table_id = array(),$request)
    {
        $loginInfo                 = get_loggedin_user_data();
        $time_table_return         = [];
        $time_table                = [];
        $arr_class                 = [];
        $arr_medium                = get_all_mediums();
        $arr_week_days             = \Config::get('custom.week_days');
        $time_table['arr_class']   = $arr_class;
        $arr_time_table_data       = TimeTable::where(function($query) use ($time_table_id,$loginInfo,$request) 
        {
            if (!empty($time_table_id))
            {
                $query->where('time_table_id', $time_table_id);
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('time_table_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
        })->with('timeTableClass', 'timeTableSection')->get();

        if (!empty($arr_time_table_data))
        {
            foreach ($arr_time_table_data as $key => $time_table_data)
            {       
                $week= explode(",", $time_table_data['time_table_week_days']);
                // p($week);
                $week_days= '';
                foreach($week as $key) 
                {
                    $week_days .= $arr_week_days[$key].',';
                }
                $time_table = array(
                    'time_table_id'     => $time_table_data['time_table_id'],
                    'time_table_name'   => $time_table_data['time_table_name'],
                    'class_id'          => $time_table_data['class_id'],
                    'section_id'        => $time_table_data['section_id'],
                    'lecture_no'        => $time_table_data['lecture_no'],
                    'week_days'        =>  $time_table_data['time_table_week_days'],
                    'week_days1'        =>  rtrim($week_days, ','),
                    'time_table_status' => $time_table_data['time_table_status'],       
                );
                if (isset($time_table_data['timeTableClass']['class_name']))
                {
                    $time_table['class_name'] = $time_table_data['timeTableClass']['class_name'].' - '.$arr_medium[$time_table_data['timeTableClass']['medium_type']];
                }
                if (isset($time_table_data['timeTableSection']['section_name']))
                {
                    $time_table['section_name'] = $time_table_data['timeTableSection']['section_name'];
                }
                $time_table_return[] = $time_table;
            }
        }
        return $time_table_return;
    }
    /**
     *  Destroy time-table data
     *  @Khushbu on 07 Sept 2018
    **/
    public function destroy($id)
    {
        $time_table_id = get_decrypted_value($id, true);
        $time_table    = TimeTable::find($time_table_id);
        
        $success_msg = $error_message =  "";
        if ($time_table)
        {
            DB::beginTransaction();
            try
            {
                $time_table->delete();
                $success_msg = "Time table deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/time-table/view-time-table')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Time table not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy time-table  period data
     *  @Shree on 30 Nov 2018
    **/
    public function destroyPeriod($id)
    {
        $time_table_map_id = $id;
        $time_table_map    = TimeTableMap::find($time_table_map_id);
        
        $success_msg = $error_message =  "";
        if ($time_table_map)
        {
            DB::beginTransaction();
            try
            {
                $time_table_map->delete();
                $success_msg = "Period deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Time table not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Time-table's status
     *  @Khushbu on 07 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $time_table_id = get_decrypted_value($id, true);
        $time_table    = TimeTable::find($time_table_id);
        if ($time_table)
        {
            $time_table->time_table_status  = $status;
            $time_table->save();
            DB::table('time_table_map') 
            ->where('time_table_id',$time_table_id)
            ->update([ 'expire_status' => $status ]);
            $success_msg = "Time table status update successfully!";
            return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Time table not found!";
            return redirect('admin-panel/time-table/view-time-table')->withErrors($error_message);
        }
    }

    public function get_available_staff(Request $request){
        $get_all_staff = get_subject_staff($request);
        $staffIds = [];
        foreach($get_all_staff as $staff_id){
            $exist_staff_id = NULL;
            if($request->get('exist_staff_id') != ''){
                $exist_staff_id = $request->get('exist_staff_id');
            }
            if($exist_staff_id != $staff_id){
                $all_staff_map = check_staff_availability($request,$staff_id);
            } 
            if(!empty($all_staff_map)){
                foreach($all_staff_map as $map){
                    
                    /*
                     ( (strtotime($request->get('start_time')) < strtotime($map['start_time'])) && (strtotime($request->get('end_time')) > strtotime($map['start_time'])) &&  (strtotime($request->get('end_time')) > strtotime($map['end_time'])) &&  (strtotime($request->get('end_time')) == strtotime($map['start_time'])) )  
                    */
                    if( ( ( (strtotime($request->get('start_time')) > strtotime($map['start_time'])) && (strtotime($request->get('start_time')) >= strtotime($map['end_time']))  ) || ( (strtotime($request->get('start_time')) < strtotime($map['start_time'])) &&  (strtotime($request->get('end_time')) >= strtotime($map['start_time'])) ) ) || ( (strtotime($request->get('start_time')) < strtotime($map['start_time'])) && (strtotime($request->get('end_time')) < strtotime($map['start_time']))  )  ||  ( (strtotime($request->get('start_time')) < strtotime($map['start_time'])) && (strtotime($request->get('end_time')) > strtotime($map['start_time'])) && ( (strtotime($request->get('end_time')) > strtotime($map['end_time'])) &&  (strtotime($request->get('end_time')) == strtotime($map['start_time'])) )  )  ){

                        if( ( strtotime($request->get('start_time')) <  strtotime($map['start_time']))  && ( strtotime($request->get('end_time')) > strtotime($map['start_time'])) && ( strtotime($request->get('start_time')) <  strtotime($map['end_time'])) &&  ( strtotime($request->get('end_time')) <=  strtotime($map['end_time']))){
                            /// 
                        } else {
                            $staffIds[] = $map['staff_id'];
                        }
                    } else {
                        // Faill case
                        $staffIds[] = '';
                    }
                }
            } else {
                $staffIds[] = $staff_id;
            }
        }
        $staff = get_multiple_staff($staffIds);
        $data = view('admin-panel.staff.ajax-staff-select',compact('staff'))->render();
        return response()->json(['options'=>$data]);
        
    }

    public function save_time_table_period(Request $request){
        // p($request->all());
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        if($request->get('start_time') != ""){
            try
	            {
                    // Add/Edit case
                    if(!empty($request->get('time_table_map_id')) && $request->get('time_table_map_id') != ''){
                        $mapping = TimeTableMap::find($request->get('time_table_map_id'));
                    } else {
                        $mapping = new TimeTableMap;
                    }
                    $lunch_break = $request->get('lunch_break');
                    if($request->get('lunch_break') == ''){
                        $lunch_break = 0;
                    }
                    $subject_id = Input::get('subject_id');
                    $staff_id = Input::get('staff_id');

                    if($request->get('lunch_break') == 1){
                        $subject_id = NULL;
                        $staff_id = NULL;
                    }
                    $mapping->admin_id       = $loginInfo['admin_id'];
                    $mapping->update_by      = $loginInfo['admin_id'];
                    $mapping->start_time     = Input::get('start_time');
                    $mapping->end_time 	     = Input::get('end_time');
                    $mapping->lunch_break 	 = $lunch_break;
                    $mapping->subject_id 	 = $subject_id;
                    $mapping->staff_id 	     = $staff_id;
                    $mapping->lecture_no 	 = Input::get('lecture_no');
                    $mapping->day 	         = Input::get('day');
                    $mapping->session_id 	 = $session['session_id'];
                    $mapping->time_table_id  = Input::get('time_table_id');
                    $mapping->save();
                    $success_msg = 'Period successfully added.';
                }
	            catch (\Exception $e)
	            {
	                DB::rollback();
                    $error_message = $e->getMessage();
                    return $error_message;
	            }
	            DB::commit();
				return "success";
        }
    }

    public function get_period_info(Request $request){
        $time_table_map_id =  $request->get('time_table_map_id');
        $period_info = TimeTableMap::where('time_table_map_id',$time_table_map_id)->with('getTimeTable')->get();
        
        $period_info = isset($period_info[0]) ? $period_info[0] : [];
        $request->merge(["subject_id"=> $period_info->subject_id]);
        $request->merge(["class_id"=> $period_info['getTimeTable']->class_id]);
        $request->merge(["section_id"=> $period_info['getTimeTable']->section_id]);
        
        $get_all_staff = get_subject_staff($request);
        
        $staffIds = [];
        foreach($get_all_staff as $staff_id){
            if($staff_id == $period_info->staff_id) {
                $staffIds[] = $staff_id;
            } else { 
                $all_staff_map = check_staff_availability($request,$staff_id);
                if(!empty($all_staff_map)){

                } else {
                    $staffIds[] = $staff_id;
                }
            }
        }
        $staff = get_multiple_staff($staffIds);

        $subjects = get_subject_list_class_section($period_info['getTimeTable']->class_id,$period_info['getTimeTable']->section_id);
        $subjectData = view('admin-panel.subject.ajax-subject-select',compact('subjects'))->render();
        $staffData = view('admin-panel.staff.ajax-staff-select',compact('staff'))->render();
        return response()->json(['lunch_break'=> $period_info->lunch_break,'start_time'=> $period_info->start_time,'end_time'=> $period_info->end_time,'subject_id'=> $period_info->subject_id,'staff_id'=> $period_info->staff_id,'time_table_map_id'=> $period_info->time_table_map_id,'time_table_id'=> $period_info->time_table_id,'staff_data'=>$staffData,'subject_data'=>$subjectData]);
    }


    /**
     *  View time table for student
     *  @Sandeep on 09 Jan 2019
    **/
    public function student_time_table($id = 1)
    {
        $time_table = $arr_staff = [];
        $loginInfo               = get_loggedin_user_data();
        $arr_class               = get_all_classes_mediums();
    //    $decrypted_time_table_id = get_decrypted_value($id, true);
        $time_table              = TimeTable::where([ 'class_id' => $loginInfo['class_id'] , 'section_id' => $loginInfo['section_id'] ])->first();
        $time_map                = TimeTableMap::where('time_table_id', '=', $time_table->time_table_id)->with('getStaff')->with('getSubject')->get()->toArray(); 
        $time_table->time_table_week_days = explode(',',$time_table->time_table_week_days);
        $arr_subject              = get_subject_list_class_section($time_table->class_id,$time_table->section_id);
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

        // $time_table['arr_subject']  = add_blank_option($arr_subject, 'Select Subject');
        // $time_table['arr_staff']  = add_blank_option($arr_staff, 'Select Staff');
        $data                     = array(
            'page_title'          => trans('language.time_table'),
            'redirect_url'        => url('admin-panel/time-table/set-time-table/'.$id),
            'login_info'          => $loginInfo,
            'time_table'          => $time_table,
            'time_map'            => $time_map,
            'parent_student'      => $parent_student,
        );

        //p($time_table->time_table_week_days[0]);

        return view('admin-panel.time-table.student_time_table')->with($data);
    }

    public function check_time_availability(Request $request){
        $count = 0;
        $time_table = $arr_staff = [];
        $loginInfo      = get_loggedin_user_data();
        $all_time_map   = TimeTableMap::where('time_table_id', '=', $request->get('time_table_id'))->where('day', '=', $request->get('day'))->where('time_table_map_id', '!=', $request->get('time_table_map_id'))->get()->toArray(); 
        // p($request->all());

        if(( strtotime($request->get('start_time')) == strtotime($request->get('end_time')) )){
            $count++;
        }
        if(!empty($all_time_map)){
            foreach($all_time_map as $map){
                if( ( strtotime($request->get('start_time')) == strtotime($map['start_time']))  && ( strtotime($request->get('end_time')) == strtotime($map['end_time']))){
                    $count++;
                    break;
                }else{
                    if( ( strtotime($request->get('start_time')) >  strtotime($map['start_time']))  && ( strtotime($request->get('start_time')) < strtotime($map['end_time'])) || ( strtotime($request->get('end_time')) > strtotime($map['start_time'])) &&  ( strtotime($request->get('end_time')) <
                     strtotime($map['end_time']))){
                        $count++;
                        break;
                    }else{
                        if( ( strtotime($request->get('start_time')) < strtotime('07:00'))  && ( strtotime($request->get('end_time')) > strtotime('16:00'))){
                            $count++;
                            break;
                        }else{
                            if( ( strtotime($request->get('start_time')) <  strtotime($map['start_time']))  && ( strtotime($request->get('end_time')) > strtotime($map['start_time'])) || ( strtotime($request->get('start_time')) < strtotime($map['end_time'])) &&  ( strtotime($request->get('end_time')) >
                            strtotime($map['end_time']))){ 
                                $count++;
                                break;
                            }else{

                            }
                        }
                    }
                }

            }
        }
        return $count;
    }

}
