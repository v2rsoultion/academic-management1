<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Transport\FeesHead;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;

class FeesHeadController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('13',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for fees
     *  @Sandeep on 3 feb 2019
    **/

    public function add(Request $request, $id = NULL)
    {
        $data     = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_fees_head_id = get_decrypted_value($id, true);
            $fees_head              = FeesHead::where('fees_head_id','=',$decrypted_fees_head_id)->get();
            $fees_head              = isset($fees_head[0]) ? $fees_head[0] : [];
            
            if (!$fees_head)
            {
                return redirect('admin-panel/transport/fees_head/add-fees-head')->withError('Fees Head not found!');
            }
            
            $encrypted_fees_head_id = get_encrypted_value($fees_head->fees_head_id, true);
            $page_title         = trans('language.edit_fees_head');
            $save_url           = url('admin-panel/transport/fees-head/save/' . $id);
            $submit_button      = 'Update';
        
        } else {
            $page_title    = trans('language.add_fees_head');
            $save_url      = url('admin-panel/transport/fees-head/save');
            $submit_button = 'Save';
        }

        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'fees_head'       => $fees_head,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/fees_head/view-fees-head'),
        );
        return view('admin-panel.fees_head.add')->with($data);
    }


    /**
     *  Save Fees Head Data
     *  @Sandeep on 3 Feb 2019
    **/

    public function save(Request $request, $id = NULL)
    {
     //   p($request->all());
        $fees_head_id           = null;
        $decrypted_fees_head_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];

        $check_fees_head = FeesHead::where( 'fees_head_km_from' ,'>', Input::get('fees_head_km_from') )->count();

        if($check_fees_head != 0){
            return redirect()->back()->withInput()->withErrors("Please choose different KM range as its already exist.");
        }
        if (!empty($id))
        {
            $decrypted_fees_head_id = get_decrypted_value($id, true);
            $fees_head              = FeesHead::find($decrypted_fees_head_id);
            $fees_headAdmin         = Admin::Find($fees_head->reference_admin_id);
            $admin_id = $fees_head['admin_id'];
            if (!$fees_head)
            {
                return redirect('/admin-panel/transport/fees-head/add-fees-head')->withError('Fees Head not found!');
            }
            $success_msg = 'Fees Head updated successfully!';
        }
        else
        {
            $fees_head            = New FeesHead;
            $fees_headAdmin       = new Admin();
            $success_msg        = 'Fees Head saved successfully!';
        }

        $arr_input_fields = [
            'fees_head_name'             => 'required|unique:transport_fees_head,fees_head_name,' . $decrypted_fees_head_id . ',fees_head_id',
            'fees_head_km_from'          => 'required',
            'fees_head_km_to'            => 'required',
            'fees_head_amount'           => 'required',
        ];
        
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction(); //Start transaction!
            try
            {
                $fees_head->admin_id                        = $admin_id;
                $fees_head->update_by                       = $loginInfo['admin_id'];
                $fees_head->fees_head_name                  = Input::get('fees_head_name');
                $fees_head->fees_head_km_from               = Input::get('fees_head_km_from');
                $fees_head->fees_head_km_to                 = Input::get('fees_head_km_to');
                $fees_head->fees_head_amount                = Input::get('fees_head_amount');
                $fees_head->save();

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }   
        return redirect('admin-panel/transport/fees-head/view-fees-head')->withSuccess($success_msg);   
    }


    /**
     *  View page for Fees Head
     *  @Sandeep on 3 Feb 2019
    **/
   
    public function index()
    {
        $loginInfo                            = get_loggedin_user_data();
        $arr_designation                      = get_all_designations();
        $listData                             = [];
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/fees-head/view-fees-head'),
            'page_title'    => trans('language.view_fees_head'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.fees_head.index')->with($data);
    }


    /**
     *  Get Data for view Fees Head page(Datatables)
     *  @Sandeep on 3 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $fees_head  = FeesHead::where(function($query) use ($request) 
        {
            
            if (!empty($request) && !empty($request->get('fees_head_name')))
            {
                $query->where('fees_head_name', "like", "%{$request->get('fees_head_name')}%");
            }

        })->orderBy('fees_head_id', 'DESC')->get();

        return Datatables::of($fees_head)
            
            ->addColumn('action', function ($fees_head)
            {
                $encrypted_fees_head_id = get_encrypted_value($fees_head->fees_head_id, true);
                if($fees_head->fees_head_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="fees-head-status/'.$status.'/' . $encrypted_fees_head_id . '">'.$statusVal.'</a></div>

                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-fees-head/' . $encrypted_fees_head_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-fees-head/' . $encrypted_fees_head_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
                ';
            })->rawColumns(['action' => 'action','total_stops'=> 'total_stops'])->addIndexColumn()->make(true);
    }

    

    /**
     *  Change fees_head's status
     *  @Sandeep on 3 Feb 2019 
    **/
    public function changeStatus($status,$id)
    {
        $fees_head_id = get_decrypted_value($id, true);
        $fees_head    = FeesHead::find($fees_head_id);
        if ($fees_head)
        {
            $fees_head->fees_head_status  = $status;
            $fees_head->save();
            $success_msg = "Fees Head status updated successfully!";
            return redirect('admin-panel/transport/fees-head/view-fees-head')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Fees Head not found!";
            return redirect('admin-panel/transport/fees-head/view-fees-head')->withErrors($error_message);
        }
    }

    /**
     *  Destroy Fees Head data
     *  @Sandeep on 3 Feb 2019   
    **/
    public function destroy($id)
    {
        $fees_head_id = get_decrypted_value($id, true);
        $fees_head    = FeesHead::find($fees_head_id);
        
        $success_msg = $error_message =  "";
        if ($fees_head)
        {
            DB::beginTransaction();
            try
            {
                $fees_head->delete();
                $success_msg = "Fees Head deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/transport/fees-head/view-fees-head')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/transport/fees-head/view-fees-head')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Fees Head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


}
