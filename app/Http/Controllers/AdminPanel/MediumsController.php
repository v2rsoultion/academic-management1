<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Mediums\Mediums; // Model
use Yajra\Datatables\Datatables;

class MediumsController extends Controller
{
    /**
     *  View page for Mediums
     *  @Shree on 30 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_mediums'),
            'redirect_url'  => url('admin-panel/mediums/view-mediums'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.mediums.index')->with($data);
    }

    /**
     *  Add page for Mediums
     *  @Shree on 30 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $medium 			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_medium_type_id 	= get_decrypted_value($id, true);
            $medium      			    = Mediums::Find($decrypted_medium_type_id);
            if (!$medium)
            {
                return redirect('admin-panel/mediums/add-medium')->withError('Medium not found!');
            }
            $page_title             	= trans('language.edit_medium');
            $encrypted_medium_type_id   		= get_encrypted_value($medium->medium_type_id, true);
            $save_url               	= url('admin-panel/mediums/save/' . $encrypted_medium_type_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_medium');
            $save_url      = url('admin-panel/mediums/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'medium' 			=> $medium,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/mediums/view-mediums'),
        );
        return view('admin-panel.mediums.add')->with($data);
    }

    /**
     *  Add and update Mediums's data
     *  @Shree on 30 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_medium_type_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $medium = Mediums::find($decrypted_medium_type_id);
            if (!$medium)
            {
                return redirect('/admin-panel/mediums/add-medium/')->withError('Medium not found!');
            }
            $success_msg = 'Medium updated successfully!';
        }
        else
        {
            $medium     	= New Mediums;
            $success_msg 	= 'Medium saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'medium_type'   => 'required|unique:medium_type,medium_type,' . $decrypted_medium_type_id . ',medium_type_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $medium->medium_type 	= Input::get('medium_type');
                $medium->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/mediums/view-mediums')->withSuccess($success_msg);
    }

    /**
     *  Get Medium's Data for view page(Datatables)
     *  @Shree on 30 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $medium  			= Mediums::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('medium_type', "like", "%{$request->get('name')}%");
            }
        })->orderBy('medium_type_id', 'DESC')->get();

        return Datatables::of($medium)
        ->addColumn('action', function ($medium)
        {
            $encrypted_medium_type_id = get_encrypted_value($medium->medium_type_id, true);
            if($medium->medium_type_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="medium-status/'.$status.'/' . $encrypted_medium_type_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-medium/' . $encrypted_medium_type_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-medium/' . $encrypted_medium_type_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Medium's data
     *  @Shree on 30 Oct 2018.
    **/
    public function destroy($id)
    {
        $medium_type_id = get_decrypted_value($id, true);
        $medium 	    = Mediums::find($medium_type_id);
        
        $success_msg = $error_message =  "";
        if ($medium)
        {
            DB::beginTransaction();
            try
            {
                $medium->delete();
                $success_msg = "Medium deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/mediums/view-mediums')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/mediums/view-mediums')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Medium not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Medium's status
     *  @Shree on 30 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $medium_type_id 	= get_decrypted_value($id, true);
        $medium 		  	    = Mediums::find($medium_type_id);
        if ($medium)
        {
            $medium->medium_type_status  = $status;
            $medium->save();
            $success_msg = "Medium status update successfully!";
            return redirect('admin-panel/mediums/view-mediums')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Medium not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
