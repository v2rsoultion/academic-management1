<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Admin; // Model
use Carbon\Carbon;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/admin-panel/dashboard';

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest');
        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $admin_type = request()->input('admin_type');
        if($admin_type == 1 || $admin_type == 2 || $admin_type == 3) 
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
        if($admin_type == 4)
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'enroll_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm1(Request $request, $token = null, $username)
    { 
        
        $username = urldecode($username);
        $data = array(
            'username' => $username
        );
        $pass_data = DB::select('select * from admin_password_resets where email = :email', ['email' => $username]);

        $current_date = Carbon::now();
        if( !empty($pass_data) ){
            foreach ( $pass_data as $res ){

                if($res->created_at <= $current_date AND  $current_date >= date("Y-m-d H:i:s", strtotime("+24 hours",strtotime($res->created_at))))
                {
                   return redirect('/admin-panel')->withErrors('Your Reset Password Link is Expired!');

                } else {
                    if(Hash::check($token, $res->token)){
                        return view('admin-panel.auth.passwords.reset')->with(
                        ['token' => $token, 'email' => $username, 'mobile_no' => $username, 'enroll_no' => $username])->with($data);
                    } else {
                        return redirect('/admin-panel')->withErrors('Your Reset Password Link is Expired!');
                    }
                }  
            }
        } else {
            return redirect('/admin-panel')->withErrors('Your Reset Password Link is Expired!');
        }      
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        $pass_data  = DB::table('admin_password_resets')->where('email',$request->username)->orWhere('mobile_no',$request->username)->orWhere('enroll_no',$request->username)->first();

        if(!empty($pass_data) && $request->password == $request->password_confirmation) {
            if(!empty($pass_data->email)) {
            DB::table('admins')->where('email', $request->username)->update(['password' => Hash::make($request->password)]);
            }
            if(!empty($pass_data->mobile_no)) {
            DB::table('admins')->where('mobile_no', $request->username)->update(['password' => Hash::make($request->password)]);
            }
            if(!empty($pass_data->enroll_no)) {
            DB::table('admins')->where('enroll_no', $request->username)->update(['password' => Hash::make($request->password)]);
            }
            DB::table('admin_password_resets')->where('email', $request->username)->orWhere('mobile_no',$request->username)->orWhere('enroll_no',$request->username)->delete();
             return redirect('admin-panel/')->withSuccess('Your Password has been changed.');   
        }
        else {
            return redirect('admin-panel/')->withErrors('Reset Link Expired! Please try again.');
        }
    }
    // public function showResetForm(Request $request, $token = null)
    // { 
    //     $hash_token = Hash::make($token);
    //     $pass_data = DB::select('select * from admin_password_resets where token = "'.$hash_token.'" ');

    //     return view('admin-panel.auth.passwords.reset')->with(
    //         ['token' => $token]
    //     );
    // }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email', 'enroll_no', 'mobile_no', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
                    ->withInput($request->only('username'))
                    ->withErrors(['username' => trans($response)]);
    }

     /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }
}
