<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Recruitment\Job;
use App\Model\Recruitment\JobForm;
use App\Model\Recruitment\JobFields;
use App\Model\Recruitment\RecruitmentForm;
use App\Model\Staff\Staff;
use App\Model\School\School;
use Yajra\Datatables\Datatables;

class JobFormController extends Controller
{
    /**
     *  Job Recruitment Form
     *  @Sandeep on 13 Feb 2019
    **/

    public function recruitment_form(Request $request, $job_id , $job_form_id )
    {
        $listData = [];
        $decrypted_job_id        = get_decrypted_value($job_id, true);
        $decrypted_job_form_id   = get_decrypted_value($job_form_id, true);
        
        $job  = Job::Find($decrypted_job_id);
        if (!$job) {
            return redirect('/');
        }

        $job_form   = JobForm::Find($decrypted_job_form_id);
        if (!$job_form) {
            return redirect('/');
        }

        $job_form_fields             = JobFields::where('job_form_id' , $job_form['job_form_id'])->first();
        $job_form_fields             = explode(',', $job_form_fields->form_keys);
        $staff                       =  $arr_state_p = $arr_city_p = $arr_state_t = $arr_city_t = [];
        $school_data                 = School::with('getCountry')->with('getState')->with('getCity')->get()->toArray();
        $school_data                 = isset($school_data[0]) ? $school_data[0] : [];
        $school_data['medium_name']  = get_medium_info($school_data['school_medium']);
        $school_data['country_name'] = $school_data['get_country']['country_name'];
        $school_data['state_name']   = $school_data['get_state']['state_name'];
        $school_data['city_name']    = $school_data['get_city']['city_name'];
        $school_data['boards']       = get_boards_info($school_data['school_board_of_exams']);
        $staff_gender                = \Config::get('custom.staff_gender');
        $arr_marital                 = \Config::get('custom.staff_marital');
        $staff['arr_gender']         = $staff_gender;
        $staff['arr_marital']        = $arr_marital;
        $arr_title                   = get_titles();
        $arr_caste                   = get_caste();
        $arr_religion                = get_religion();
        $arr_natioanlity             = get_nationality();
        $arr_designations            = get_all_designations();
        $arr_country                 = get_all_country();
        $staff['arr_title']          = add_blank_option($arr_title, 'Select Title');
        $staff['arr_caste']          = add_blank_option($arr_caste, 'Select caste');
        $staff['arr_religion']       = add_blank_option($arr_religion, 'Select religion');
        $staff['arr_designations']   = add_blank_option($arr_designations, 'Select designation');
        $staff['arr_natioanlity']    = add_blank_option($arr_natioanlity, 'Select nationality');
        $staff['arr_country']        = add_blank_option($arr_country, 'Select Country');
        $staff['arr_state_p']        = add_blank_option($arr_state_p, 'Select State');
        $staff['arr_city_p']         = add_blank_option($arr_city_p, 'Select City');
        $staff['arr_state_t']        = add_blank_option($arr_state_t, 'Select State');
        $staff['arr_city_t']         = add_blank_option($arr_city_t, 'Select City');

        $page_title    = trans('language.recruitment_form');
        $save_url      = url('recruitment-form/save');
        $submit_button = 'Submit';        
       
        $data    = array(
            'page_title'        => $page_title,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'job_form'          => $job_form,
            'job'               => $job,
            'school_data'       => $school_data,
            'staff'             => $staff,
            'job_form_fields'   => $job_form_fields,
            'redirect_url'      => url('recruitment-form/'.$job_id.'/'.$job_form_id),
        );
        return view('recruitment.recruitment_form')->with($data);
    }


    /**
     *  Add recruitment form's data
     *  @Sandeep on 13 Feb 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        
        if(Input::get('staff_gender') == ""){
            $request->staff_gender = 0;
        }
        if(Input::get('staff_marital_status') == ""){
            $request->staff_marital_status = 0;
        }

        if($request->get('staff_email') != ''){
            $arr_input_fields = [
                'staff_email'   => 'required|unique:staff,staff_email',
                'staff_mobile_number'   => 'required|unique:staff,staff_mobile_number',
            ];
        } else {
            $arr_input_fields = [
                'staff_mobile_number'  => 'required|unique:staff,staff_mobile_number',
            ];
        }
       
        $validatior = Validator::make($request->all(), $arr_input_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }

        $recruitment                                    = New RecruitmentForm;
        $recruitment->job_id                            = Input::get('job_id');
        $recruitment->job_form_id                       = Input::get('job_form_id');
        $recruitment->form_number                       = Input::get('form_number');
        $recruitment->staff_name                        = Input::get('staff_name');
        $recruitment->staff_profile_img                 = Input::get('staff_profile_img');
        $recruitment->staff_aadhar                      = Input::get('staff_aadhar');
        $recruitment->staff_UAN                         = Input::get('staff_UAN');
        $recruitment->staff_ESIN                        = Input::get('staff_ESIN');
        $recruitment->staff_PAN                         = Input::get('staff_PAN');
        $recruitment->designation_id                    = Input::get('designation_id');
        $recruitment->staff_email                       = Input::get('staff_email');
        $recruitment->staff_mobile_number               = Input::get('staff_mobile_number');
        $recruitment->staff_dob                         = Input::get('staff_dob');
        $recruitment->staff_gender                      = $request->staff_gender;
        $recruitment->staff_father_name_husband_name    = Input::get('staff_father_name_husband_name');
        $recruitment->staff_father_husband_mobile_no    = Input::get('staff_father_husband_mobile_no');
        $recruitment->staff_mother_name                 = Input::get('staff_mother_name');
        $recruitment->staff_blood_group                 = Input::get('staff_blood_group');
        $recruitment->staff_marital_status              = $request->staff_marital_status;
        $recruitment->title_id                          = Input::get('title_id');
        $recruitment->caste_id                          = Input::get('caste_id');
        $recruitment->nationality_id                    = Input::get('nationality_id');
        $recruitment->religion_id                       = Input::get('religion_id');
        $recruitment->staff_qualification               = Input::get('staff_qualification');
        $recruitment->staff_specialization              = Input::get('staff_specialization');
        $recruitment->staff_reference                   = Input::get('staff_reference');
        $recruitment->staff_temporary_address           = Input::get('staff_temporary_address');
        $recruitment->staff_temporary_city              = Input::get('staff_temporary_city');
        $recruitment->staff_temporary_state             = Input::get('staff_temporary_state');
        $recruitment->staff_temporary_county            = Input::get('staff_temporary_county');
        $recruitment->staff_temporary_pincode           = Input::get('staff_temporary_pincode');
        $recruitment->staff_permanent_address           = Input::get('staff_permanent_address');
        $recruitment->staff_permanent_city              = Input::get('staff_permanent_city');
        $recruitment->staff_permanent_state             = Input::get('staff_permanent_state');
        $recruitment->staff_permanent_county            = Input::get('staff_permanent_county');
        $recruitment->staff_permanent_pincode           = Input::get('staff_permanent_pincode');

        if ($request->hasFile('staff_profile_img'))
        {
            if (!empty($id)){
                $profile = check_file_exist($recruitment->staff_profile_img, 'recruitment_form');
                if (!empty($profile))
                {
                    unlink($profile);
                } 
            }
            $file                          = $request->file('staff_profile_img');
            $config_upload_path            = \Config::get('custom.recruitment_form');
            $destinationPath               = public_path() . $config_upload_path['upload_path'];
            $ext                           = substr($file->getClientOriginalName(),-4);
            $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
            $filename                      = $name.mt_rand(0,100000).time().$ext;
            $file->move($destinationPath, $filename);
            $recruitment->staff_profile_img       = $filename;
        }

        if ($request->hasFile('staff_resume'))
        {
            if (!empty($id)){
                $profile = check_file_exist($recruitment->staff_resume, 'recruitment_form');
                if (!empty($profile))
                {
                    unlink($profile);
                } 
            }
            $file                          = $request->file('staff_resume');
            $config_upload_path            = \Config::get('custom.recruitment_form');
            $destinationPath               = public_path() . $config_upload_path['upload_path'];
            $ext                           = substr($file->getClientOriginalName(),-4);
            $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
            $filename                      = $name.mt_rand(0,100000).time().$ext;
            $file->move($destinationPath, $filename);
            $recruitment->staff_resume     = $filename;
        }

        $recruitment->save();
        $job_id = Input::get('job_id');
        $job  = Job::Find($job_id);
        $send_notification  = $this->send_push_notification($recruitment->applied_cadidate_id,$job->job_name,'job_apply');
        $success_msg = "You have successfully applied for the recruitment";
        return redirect()->back()->withSuccess($success_msg);
    }

    public function send_push_notification($module_id,$job_name,$notification_type){
        $device_ids = [];
        $session       = get_current_session();
        $message = "New Applicant applied for ".$job_name." .";
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->orWhereRaw('FIND_IN_SET(10,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            $staff_admin_id = $staff['getStaffAdminInfo']->admin_id;
            if($staff['getStaffAdminInfo']->fcm_device_id != "" && $staff['getStaffAdminInfo']->notification_status == 1){
                $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
                
            }
            
            $info = array(
                'admin_id' => $staff_admin_id,
                'update_by' => $staff_admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $staff_admin_id,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = save_notification($info);
            
        }
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
        
    }

}
