<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Response;

class SchoolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        $login = Auth::guard('admin')->user();
        if (!empty($login) && $login->admin_type != 1)
        {
            return redirect('/admin-panel/unauthorized');
        }
        return $next($request);
    }
}
