<?php
use App\Admin;
use App\Model\Address\Country;
use App\Model\Address\State;
use App\Model\Address\City;
use App\Model\School\School;
use App\Model\School\SchoolBoard;
use App\Model\Session\Session;
use App\Model\Classes\Classes;
use App\Model\CoScholastic\CoScholastic;
use App\Model\Section\Section;
use App\Model\Caste\Caste;
use App\Model\Title\Title;
use App\Model\Holiday\Holiday;
use App\Model\Religion\Religion;
use App\Model\Nationality\Nationality;
use App\Model\SchoolGroup\SchoolGroup; 
use App\Model\Student\Student;
use App\Model\Student\StudentParent;
use App\Model\Student\StudentAcademic;
use App\Model\Student\StudentHealth;
use App\Model\Designation\Designation;
use App\Model\Staff\Staff;
use App\Model\StaffRoles\StaffRoles;
use App\Model\Subject\Subject;
use App\Model\DocumentCategory\DocumentCategory; 
use App\Model\Term\Term; // Model @Pratyush on 11  Aug 2018
use App\Model\BookCupboard\BookCupboard; // Model @Pratyush on 13 Aug 2018
use App\Model\BookCategory\BookCategory; // Model @Pratyush on 14 Aug 2018
use App\Model\BookVendor\BookVendor;     // Model @Pratyush on 14 Aug 2018
use App\Model\BookCupboardshelf\BookCupboardshelf;     // Model @Pratyush on 16 Aug 2018
use App\Model\SubjectClassMapping\SubjectClassmapping;     // Model @Ashish on 22 Aug 2018
use App\Model\SubjectSectionMapping\SubjectSectionMapping; 
use App\Model\StudentSubjectManage\StudentSubjectManage; 
use App\Model\Exam\Exam;     // Model @Ashish on 22 Aug 2018
use App\Model\Stream\Stream; // Model
use App\Model\Competition\Competition; // Model 
use App\Model\Competition\CompetitionMapping; // Model 
use App\Model\Brochure\Brochure; // Model 
use App\Model\Admission\AdmissionForm;
use App\Model\Admission\AdmissionFields;
use App\Model\Admission\AdmissionData;
use App\Model\Shift\Shift;   
use App\Model\FeesCollection\Bank;
use App\Model\FeesCollection\OneTime; // Model
use App\Model\FeesCollection\RecurringHead; // Model
use App\Model\FeesCollection\RTEHead; // Model
use App\Model\ApiToken\ApiToken; // Model @Pratyush 11 Oct 2018
use App\Model\FeesCollection\Fine; // Model
use App\Model\School\ImprestAcConfig; // Model
use App\Model\Student\ImprestAc; // Model
use App\Model\Student\WalletAc; // Model
use App\Model\FeesCollection\ConcessionMap; // Model
use App\Model\FeesCollection\FeesStMap; // Model
use App\Model\FeesCollection\Receipt;
use App\Model\FeesCollection\ReceiptDetail;
use App\Model\FeesCollection\Reason;
use App\Model\Examination\ExamMap; // Model
use App\Model\Examination\MarksCriteria; // Model
use App\Model\Examination\GradeScheme; // Model
use App\Model\LibraryMember\LibraryMember; // Model
use App\Model\Examination\Grades; // Model
use App\Model\Mediums\Mediums; // Model
use App\Model\Student\StudentAttendance; // Model
use App\Model\Staff\StaffAttendance; // Model
use App\Model\Staff\StaffAttendanceDetails; // Model
use App\Model\Hostel\Hostel; // Model
use App\Model\Hostel\HostelBlock; // Model
use App\Model\Hostel\HostelRoomCategory; // Model
use App\Model\Hostel\HostelRooms; // Model
use App\Model\Hostel\HostelFloor; // Model
use App\Model\Hostel\HostelStudentMap; // Model
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model
use App\Model\HomeworkGroup\HomeworkGroup; // Model
use App\Model\TimeTable\TimeTable; // Model
use App\Model\TimeTable\TimeTableMap; // Model
use App\Model\Exam\ExamScheduleMap; // Model
use App\Model\RoomNo\RoomNo; // Model
use Illuminate\Support\Facades\Crypt;
use App\Model\InventoryCategory\Category; // Model
use App\Model\InventoryUnit\Unit; // Model
use App\Model\InventoryVendor\Vendor; // Model
use App\Model\InventoryItem\Items; // Model
use App\Model\Transport\Vehicle; // Model
use App\Model\Transport\Route; // Model
use App\Model\Notification\Notification; // Model
use App\Model\Transport\MapStudentStaffVehicle;
use Session as LSession;
use Carbon\Carbon;

    /*  Get Complete fees by class id
    *   @Shree 10 Oct 2018.
    */ 
    function api_get_complete_fees($student_id,$class_id){
        
        $session = get_current_session();
        $arr_heads   = [];
        $currentDate = date('Y-m-d');
        $arr_onetime_head_list   = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')
        ->where(array('ot_status' => 1,'deduct_imprest_status' => 0))
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'one_time_heads.ot_head_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','ONETIME');
        })
        ->orderBy('ot_head_id', 'ASC')->get();
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->ot_head_id;
                $headData['head_name'] = $arr_head_value->ot_particular_name;
                $headData['head_amt'] = $arr_head_value->ot_amount;
                $headData['head_type'] = "ONETIME";
                $headData['head_date'] = $arr_head_value->ot_date;
                $headData['head_expire'] = $feesExpire;
                $headData['head_inst_id'] = null;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
                $headData['student_id'] = $student_id;
    
            $arr_heads[] = $headData;
            }
        }
        $arr_recurring_head_list   = RecurringHead::whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->where(array('rc_status' => 1))
        ->join('recurring_inst', function($join) {
            $join->on('recurring_inst.rc_head_id', '=', 'recurring_heads.rc_head_id');
        })
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('head_type', '=','RECURRING');
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
        })
        ->orderBy('rc_particular_name', 'ASC')->get();
        
        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire1 = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire1 = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->rc_inst_id;
                $headData['head_name'] = $arr_head_value->rc_inst_particular_name;
                $headData['head_amt'] = $arr_head_value->rc_inst_amount;
                $headData['head_type'] = "RECURRING";
                $headData['head_date'] = $arr_head_value->rc_inst_date;
                $headData['head_expire'] = $feesExpire1;
                $headData['head_inst_id'] = $arr_head_value->rc_inst_id;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
                $headData['student_id'] = $student_id;
    
            $arr_heads[] = $headData;
            }
        }


        $checkForTransport   = MapStudentStaffVehicle::where(function($query) use ($student_id,$session) 
        {
            $query->where('student_staff_id', $student_id);
            $query->where('student_staff_type', 1);
            $query->where('student_staff_status', 1);
            $query->where('session_id', $session['session_id']);
        })
        ->with('get_location')
        ->get();

        if(!empty($checkForTransport[0])){
            
            $checkForTransport = isset($checkForTransport[0]) ? $checkForTransport[0] : [];
            $transportAssignDdate = date("Y-m-d", strtotime($checkForTransport->created_at));

            $kms = explode(" ", $checkForTransport['get_location']->location_km_distance);
            
            $start    = (new DateTime($transportAssignDdate));
            $end      = (new DateTime($session['end_date']));
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            $headInfo = DB::select("select * from `transport_fees_head` where '".$kms[0]."' BETWEEN fees_head_km_from and fees_head_km_to
            ");
            $map_transport_receipt = ReceiptDetail::
            where('head_type', '=','TRANSPORT')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->where('map_student_staff_id', '=',$checkForTransport->map_student_staff_id)
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','TRANSPORT');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
            
            $map_student_staff_id = $checkForTransport->map_student_staff_id;
            
            foreach ($period as $dt) {
                if(!empty($map_transport_receipt)){
                    $head_paid_amt = $head_pay_amt = 0;
                    foreach ($map_transport_receipt as $receiptValues) {
                        if($map_student_staff_id == $receiptValues['map_student_staff_id'] && $dt->format("Y-m") == $receiptValues['head_month_year']){
                            if($receiptValues['inst_paid_amt'] != ""){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];
                            }
                        }
                        
                    }
                }
                if(!in_array($dt->format("Y-m"), array_column($map_transport_receipt[0], 'head_month_year'))){
                    $counter = COUNT($arr_heads);
                    $headData = [];
                    $feesExpire1 = 0;
                    $total_delay_days = 0;
                    $fineinfo = [];
                    // p($headInfo);
                    $head_pay_amt =  $headInfo[0]->fees_head_amount;
                    // p($headInfo[0]->fees_head_amount);
                    if($head_pay_amt != $head_paid_amt){
                        $head_pay_amt = $head_pay_amt - $head_paid_amt;
                    }
                    
                    if($head_pay_amt != 0){
                        $headData['counter'] = $counter;
                        $headData['head_id'] = $headInfo[0]->fees_head_id;
                        $headData['head_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_amt'] = $headInfo[0]->fees_head_amount;
                        $headData['head_paid_amt'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['head_type'] = "TRANSPORT";
                        $headData['head_month_year'] = $dt->format("Y-m");
                        $headData['head_date'] = $dt->format("Y-m-d");
                        $headData['head_expire'] = $feesExpire1;
                        $headData['head_inst_id'] = NULL;
                        $headData['concession_map_id'] = NULL;
                        $headData['concession_amt'] = NULL;
                        $headData['concession_status'] = NULL;
                        $headData['class_id'] = $class_id;
                        $headData['student_id'] = $student_id;
                        $headData['total_delay_days'] = $total_delay_days;
                        $headData['fine_info'] = $fineinfo;
                        $headData['map_student_staff_id'] = $map_student_staff_id;
                        $headData['editable'] = 1;
            
                        $arr_heads[] = $headData;
                    }
                }
            }
            
        }
        // p($arr_heads);
        return $arr_heads;
        
    }

    /*  Get Complete fees by student id and class id
    *   @Shree 16 Oct 2018.
    */ 
    function api_get_complete_fees_student($student_id,$class_id){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $arr_heads  = [];
        $map_onetime_head_for_student = $map_recurring_head_for_student = $map_onetime_receipt = $map_recurring_receipt = [];
        $currentDate = date('Y-m-d');
        $arr_onetime_head_list   = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')
        ->where(array('ot_status' => 1,'deduct_imprest_status' => 0))
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'one_time_heads.ot_head_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('concession_map.head_type', '=','ONETIME');
        })
        ->leftJoin('fee_receipt_details', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt_details.head_id', '=', 'one_time_heads.ot_head_id');
            $join->where('fee_receipt_details.head_type', '=','ONETIME');
            $join->where('inst_status', '=',0);
            $join->where('cancel_status', '=',0);
        })
        ->orderBy('ot_head_id', 'ASC')->groupBy('ot_head_id')->get();

        $map_onetime_head_for_student = FeesStMap::whereRaw('FIND_IN_SET('.$student_id.',student_ids)')
        ->where('session_id', '=',$session['session_id'])
        ->where('head_type', '=','ONETIME')
        ->orderBy('fees_st_map_id', 'DESC')->get()->toArray();
        
        $map_onetime_receipt = ReceiptDetail::
        where('head_type', '=','ONETIME')
        ->where('inst_status', '=','1')
        ->where('session_id', '=',$session['session_id'])
        ->where('receipt_status', '=','1')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','ONETIME');
            $join->where('receipt_status', '=',1);
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                if(!in_array($arr_head_value->ot_head_id, array_column($map_onetime_head_for_student, 'head_id'))){
                    if(!in_array($arr_head_value->ot_head_id, array_column($map_onetime_receipt, 'head_id'))){
                        $counter = COUNT($arr_heads);
                        $headData = [];
                        $feesExpire = 0;
                        $total_delay_days = 0;
                        $fineinfo = [];
                        if($currentDate > $arr_head_value->ot_date){
                            $feesExpire = 1;
                            $datediff = strtotime($currentDate) - strtotime($arr_head_value->ot_date);
                            $total_delay_days =  round($datediff / (60 * 60 * 24));
                            $fineinfo = get_fine_info($total_delay_days,$arr_head_value->ot_head_id,'ONETIME');
                        }
                        $head_paid_amt = $head_pay_amt = 0;
                        $head_pay_amt = $arr_head_value->ot_amount;
                        if($arr_head_value->inst_paid_amt != ""){
                            $head_paid_amt = $arr_head_value->inst_paid_amt;
                            $head_pay_amt = $head_pay_amt - $head_paid_amt;
                        }
                        $headData['counter'] = $counter;
                        $headData['fees_name'] = $arr_head_value->ot_particular_name;
                        $headData['head_id'] = $arr_head_value->ot_head_id;
                        $headData['head_name'] = $arr_head_value->ot_particular_name;
                        $headData['head_amt'] = $arr_head_value->ot_amount;
                        $headData['head_paid_amt'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['head_type'] = "ONETIME";
                        $headData['head_date'] = $arr_head_value->ot_date;
                        $headData['head_expire'] = $feesExpire;
                        $headData['head_inst_id'] = null;
                        $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                        $headData['concession_amt'] = $arr_head_value->concession_amt;
                        $headData['concession_status'] = $arr_head_value->concession_status;
                        $headData['class_id'] = $class_id;
                        $headData['student_id'] = $student_id;
                        $headData['total_delay_days'] = $total_delay_days;
                        $headData['fine_info'] = $fineinfo;
                        $headData['editable'] = 1;
            
                        $arr_heads[] = $headData;
                    }
                }
            }
        }
        //p($arr_onetime_head_list);
        
        $arr_recurring_head_list   = RecurringHead::select()->whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->where(array('rc_status' => 1))
        ->join('recurring_inst', function($join) {
            $join->on('recurring_inst.rc_head_id', '=', 'recurring_heads.rc_head_id');
        })
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('concession_map.session_id', '=',$session['session_id']);
            $join->where('concession_map.head_type', '=','RECURRING');
            $join->where('concession_map.class_id', '=',$class_id);
            $join->where('concession_map.student_id', '=',$student_id);
        })
        ->leftJoin('fee_receipt_details', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt_details.head_inst_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('fee_receipt_details.head_type', '=','RECURRING');
            $join->where('inst_status', '=',0);
            $join->where('cancel_status', '=','0');
        })
        ->orderBy('rc_particular_name', 'ASC')->get();

        // p($arr_recurring_head_list);
        $map_recurring_head_for_student = FeesStMap::whereRaw('FIND_IN_SET('.$student_id.',student_ids)')
        ->where('session_id', '=',$session['session_id'])
        ->where('head_type', '=','RECURRING')
        ->orderBy('fees_st_map_id', 'DESC')->get()->toArray();

        $map_recurring_receipt = ReceiptDetail::
            where('head_type', '=','RECURRING')
            ->where('inst_status', '=','1')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','RECURRING');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();

        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                if(!in_array($arr_head_value->rc_head_id, array_column($map_recurring_head_for_student, 'head_id'))){
                    if(!in_array($arr_head_value->rc_inst_id, array_column($map_recurring_receipt, 'head_inst_id'))){
                        $counter = COUNT($arr_heads);
                        $headData = [];
                        $feesExpire1 = 0;
                        $total_delay_days = 0;
                        $fineinfo = [];
                        if($currentDate > $arr_head_value->rc_inst_date){
                            $feesExpire = 1;
                            $datediff = strtotime($currentDate) - strtotime($arr_head_value->rc_inst_date);
                            $total_delay_days =  round($datediff / (60 * 60 * 24));
                            $fineinfo = get_fine_info($total_delay_days,$arr_head_value->rc_head_id,'RECURRING');
                        }
                        $head_paid_amt = $head_pay_amt = 0;
                        $head_pay_amt =  $arr_head_value->rc_inst_amount;
                        if($arr_head_value->inst_paid_amt != ""){
                            $head_paid_amt = $arr_head_value->inst_paid_amt;
                            // echo $head_paid_amt;
                            $$head_pay_amt = $head_pay_amt - $head_paid_amt;
                        }
                        $headData['counter'] = $counter;
                        $headData['fees_name'] = $arr_head_value->rc_particular_name;
                        $headData['head_id'] = $arr_head_value->rc_inst_id;
                        $headData['head_name'] = $arr_head_value->rc_inst_particular_name;
                        $headData['head_amt'] = $arr_head_value->rc_inst_amount;
                        $headData['head_paid_amt'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['head_type'] = "RECURRING";
                        $headData['head_date'] = $arr_head_value->rc_inst_date;
                        $headData['head_expire'] = $feesExpire1;
                        $headData['head_inst_id'] = $arr_head_value->rc_inst_id;
                        $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                        $headData['concession_amt'] = $arr_head_value->concession_amt;
                        $headData['concession_status'] = $arr_head_value->concession_status;
                        $headData['class_id'] = $class_id;
                        $headData['student_id'] = $student_id;
                        $headData['total_delay_days'] = $total_delay_days;
                        $headData['fine_info'] = $fineinfo;
                        $headData['editable'] = 1;
            
                        $arr_heads[] = $headData;
                    }
                }
            }
        }

        $checkForTransport   = MapStudentStaffVehicle::where(function($query) use ($student_id,$session) 
        {
            $query->where('student_staff_id', $student_id);
            $query->where('student_staff_type', 1);
            $query->where('student_staff_status', 1);
            $query->where('session_id', $session['session_id']);
        })
        ->with('get_location')
        ->get();

        if(!empty($checkForTransport[0])){
            
            $checkForTransport = isset($checkForTransport[0]) ? $checkForTransport[0] : [];
            $transportAssignDdate = date("Y-m-d", strtotime($checkForTransport->created_at));

            $kms = explode(" ", $checkForTransport['get_location']->location_km_distance);
            
            $start    = (new DateTime($transportAssignDdate));
            $end      = (new DateTime($session['end_date']));
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            $headInfo = DB::select("select * from `transport_fees_head` where '".$kms[0]."' BETWEEN fees_head_km_from and fees_head_km_to
            ");
            $map_transport_receipt = ReceiptDetail::
            where('head_type', '=','TRANSPORT')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->where('map_student_staff_id', '=',$checkForTransport->map_student_staff_id)
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','TRANSPORT');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
            
            $map_student_staff_id = $checkForTransport->map_student_staff_id;
            
            foreach ($period as $dt) {
                if(!empty($map_transport_receipt)){
                    $head_paid_amt = $head_pay_amt = 0;
                    foreach ($map_transport_receipt as $receiptValues) {
                        if($map_student_staff_id == $receiptValues['map_student_staff_id'] && $dt->format("Y-m") == $receiptValues['head_month_year']){
                            if($receiptValues['inst_paid_amt'] != ""){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];
                            }
                        }
                        
                    }
                }
                $year_month = $dt->format("Y-m");
                if(!in_array($year_month, $map_transport_receipt[0])){
                    
                    $counter = COUNT($arr_heads);
                    $headData = [];
                    $feesExpire1 = 0;
                    $total_delay_days = 0;
                    $fineinfo = [];
                    // p($headInfo);
                    $head_pay_amt =  $headInfo[0]->fees_head_amount;
                    // p($headInfo[0]->fees_head_amount);
                    if($head_pay_amt != $head_paid_amt){
                        $head_pay_amt = $head_pay_amt - $head_paid_amt;
                    }
                    
                    if($head_pay_amt != 0){
                        $headData['counter'] = $counter;
                        $headData['head_id'] = $headInfo[0]->fees_head_id;
                        $headData['fees_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_amt'] = $headInfo[0]->fees_head_amount;
                        $headData['head_paid_amt'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['head_type'] = "TRANSPORT";
                        $headData['head_month_year'] = $dt->format("Y-m");
                        $headData['head_date'] = $dt->format("Y-m-d");
                        $headData['head_expire'] = $feesExpire1;
                        $headData['head_inst_id'] = NULL;
                        $headData['concession_map_id'] = NULL;
                        $headData['concession_amt'] = NULL;
                        $headData['concession_status'] = NULL;
                        $headData['class_id'] = $class_id;
                        $headData['student_id'] = $student_id;
                        $headData['total_delay_days'] = $total_delay_days;
                        $headData['fine_info'] = $fineinfo;
                        $headData['map_student_staff_id'] = $map_student_staff_id;
                        $headData['editable'] = 1;
            
                        $arr_heads[] = $headData;
                    }
                }
            }
            
        }

        if($loginInfo['imprest_ac_status'] == 1) {
            $imprest_fees = DB::select("SELECT ac_entry_id,IMPST.class_id,IMPST.section_id,ac_entry_name,ac_entry_date,ac_entry_amt FROM `imprest_ac_entries` as IMPST
            WHERE `ac_entry_id` NOT IN (SELECT DISTINCT ac_entry_id FROM `ac_entry_map` where `session_id` = '".$session['session_id']."' AND `class_id` = '".$class_id."'  AND `student_id` = '".$student_id."' AND `ac_entry_map_amt_status` = 1)
            AND  `session_id` = '".$session['session_id']."'
            ");
            
            if(!empty($imprest_fees)){
                foreach($imprest_fees as $fees){
                    $fineinfo = [];
                    $counter = COUNT($arr_heads);
                    $headData['counter'] = $counter;
                    $headData['head_id'] = $fees->ac_entry_id;
                    $headData['head_name'] = $fees->ac_entry_name;
                    $headData['head_amt'] = $fees->ac_entry_amt;
                    $headData['head_paid_amt'] = 0;
                    $headData['head_pay_amt'] =  $fees->ac_entry_amt;
                    $headData['head_type'] = "IMPREST";
                    $headData['head_date'] = $fees->ac_entry_date;
                    $headData['head_expire'] = 0;
                    $headData['head_inst_id'] = '';
                    $headData['concession_map_id'] = '';
                    $headData['concession_amt'] = '';
                    $headData['concession_status'] = '';
                    $headData['class_id'] = $class_id;
                    $headData['student_id'] = $student_id;
                    $headData['total_delay_days'] = '';
                    $headData['fine_info'] = $fineinfo;
                    $headData['editable'] = 0;
        
                    $arr_heads[] = $headData;
                }
            }
        }
       // p($arr_heads);
        return $arr_heads;
    } 

    /*  Get Paid fees by student id
    *   @Sumit 07 Feb 2019.
    */
    function api_get_complete_paid_fees_student($student_id,$class_id){
        $session    = get_current_session();

        $paid_arr = [];

        $map_onetime_receipt = ReceiptDetail::
        where('head_type', '=','ONETIME')
        //->where('inst_status', '=','1')
        ->where('session_id', '=',$session['session_id'])
        ->where('fee_receipt.receipt_status', '=','1')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','ONETIME');
            $join->where('receipt_status', '=',1);
        })->leftJoin('one_time_heads', function($join) use($session,$class_id,$student_id) {
            $join->on('one_time_heads.ot_head_id', '=', 'fee_receipt_details.head_id');
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
        
        $map_recurring_receipt = ReceiptDetail::
        where('head_type', '=','RECURRING')
        //->where('inst_status', '=','1')
        ->where('session_id', '=',$session['session_id'])
        ->where('cancel_status', '=','0')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','RECURRING');
            $join->where('receipt_status', '=',1);
        })->leftJoin('recurring_heads', function($join) use($session,$class_id,$student_id) {
            $join->on('recurring_heads.rc_head_id', '=', 'fee_receipt_details.head_id');
        })->leftJoin('recurring_inst', function($join) use($session,$class_id,$student_id) {
            $join->on('recurring_inst.rc_inst_id', '=', 'fee_receipt_details.head_inst_id');
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();

        foreach($map_onetime_receipt as $map_onetime_receipt_res){
            $paid_arr[] = array(
                'receipt_id'                    => $map_onetime_receipt_res['receipt_id'],
                'fees_name'                     => $map_onetime_receipt_res['ot_particular_name'],
                'head_id'                       => $map_onetime_receipt_res['head_id'],
                'head_name'                     => $map_onetime_receipt_res['ot_particular_name'],
                'head_type'                     => $map_onetime_receipt_res['head_type'],
                'total_amount'                  => $map_onetime_receipt_res['inst_paid_amt'],
                'total_calculate_fine_amount'   => $map_onetime_receipt_res['total_calculate_fine_amount'],
                'receipt_date'                  => $map_onetime_receipt_res['receipt_date'],
            );
        }

        foreach($map_recurring_receipt as $map_recurring_receipt_res){
            $paid_arr[] = array(
                'receipt_id'                    => $map_recurring_receipt_res['receipt_id'],
                'fees_name'                     => $map_recurring_receipt_res['rc_particular_name'],
                'head_id'                       => $map_recurring_receipt_res['head_id'],
                'head_name'                     => $map_recurring_receipt_res['rc_inst_particular_name'],
                'head_type'                     => $map_recurring_receipt_res['head_type'],
                'head_inst_id'                  => $map_recurring_receipt_res['head_inst_id'],
                'total_amount'                  => $map_recurring_receipt_res['inst_paid_amt'],
                'total_calculate_fine_amount'   => $map_recurring_receipt_res['total_calculate_fine_amount'],
                'receipt_date'                  => $map_recurring_receipt_res['receipt_date'],
            );
        }


        $checkForTransport   = MapStudentStaffVehicle::where(function($query) use ($student_id,$session) 
        {
            $query->where('student_staff_id', $student_id);
            $query->where('student_staff_type', 1);
            $query->where('student_staff_status', 1);
            $query->where('session_id', $session['session_id']);
        })
        ->with('get_location')
        ->get();

        if(!empty($checkForTransport[0])){
            
            $checkForTransport = isset($checkForTransport[0]) ? $checkForTransport[0] : [];
            $transportAssignDdate = date("Y-m-d", strtotime($checkForTransport->created_at));

            $kms = explode(" ", $checkForTransport['get_location']->location_km_distance);
            
            $start    = (new DateTime($transportAssignDdate));
            $end      = (new DateTime($session['end_date']));
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            $headInfo = DB::select("select * from `transport_fees_head` where '".$kms[0]."' BETWEEN fees_head_km_from and fees_head_km_to
            ");
            $map_transport_receipt = ReceiptDetail::
            where('head_type', '=','TRANSPORT')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->where('map_student_staff_id', '=',$checkForTransport->map_student_staff_id)
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','TRANSPORT');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();

            $map_student_staff_id = $checkForTransport->map_student_staff_id;

            foreach ($period as $dt) {
                if(!empty($map_transport_receipt)){
                    $head_paid_amt = $head_pay_amt = 0;
                    $total_calculate_fine_amount = "";
                    $receipt_date = "";
                    foreach ($map_transport_receipt as $receiptValues) {

                        if($map_student_staff_id == $receiptValues['map_student_staff_id'] && $dt->format("Y-m") == $receiptValues['head_month_year']){
                            if($receiptValues['inst_paid_amt'] != ""){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];

                                $total_calculate_fine_amount = $receiptValues['total_calculate_fine_amount'];
                                $receipt_date = $receiptValues['receipt_date'];
                            }
                        }
                        $receipt_id = $receiptValues['receipt_id'];
                        
                    }
                }
                if(!in_array($dt->format("Y-m"), array_column($map_transport_receipt[0], 'head_month_year'))){
                    $counter = COUNT($arr_heads);
                    $headData = [];
                    $feesExpire1 = 0;
                    $total_delay_days = 0;
                    $fineinfo = [];
                    // p($headInfo);
                    $head_pay_amt =  $headInfo[0]->fees_head_amount;
                    // p($headInfo[0]->fees_head_amount);
                    if($head_pay_amt != $head_paid_amt){
                        $head_pay_amt = $head_pay_amt - $head_paid_amt;
                    }
                    
                    if($head_paid_amt != 0){
                        $headData['receipt_id']  = $receipt_id;
                        $headData['fees_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_id'] = $headInfo[0]->fees_head_id;
                        $headData['head_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_type'] = "TRANSPORT";
                        $headData['total_amount'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['total_calculate_fine_amount']   = $total_calculate_fine_amount;
                        $headData['receipt_date']   = $receipt_date;
    
                        $paid_arr[] = $headData;
                    }
                }
            }
            
        }
       //p($paid_arr);
        return $paid_arr;
    }

    /*  Get Due fees by student id
    *   @Sumit 07 Feb 2019.
    */
    function api_get_complete_due_fees_student($student_id,$class_id){
        $session    = get_current_session();

        $due_arr = [];

        $map_onetime_receipt = ReceiptDetail::
        where('head_type', '=','ONETIME')
        ->where('inst_status', '=','0')
        ->where('session_id', '=',$session['session_id'])
        ->where('fee_receipt.receipt_status', '=','1')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','ONETIME');
            $join->where('receipt_status', '=',1);
        })->leftJoin('one_time_heads', function($join) use($session,$class_id,$student_id) {
            $join->on('one_time_heads.ot_head_id', '=', 'fee_receipt_details.head_id');
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
        
        $map_recurring_receipt = ReceiptDetail::
        where('head_type', '=','RECURRING')
        ->where('inst_status', '=','0')
        ->where('session_id', '=',$session['session_id'])
        ->where('cancel_status', '=','0')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','RECURRING');
            $join->where('receipt_status', '=',1);
        })->leftJoin('recurring_heads', function($join) use($session,$class_id,$student_id) {
            $join->on('recurring_heads.rc_head_id', '=', 'fee_receipt_details.head_id');
        })->leftJoin('recurring_inst', function($join) use($session,$class_id,$student_id) {
            $join->on('recurring_inst.rc_inst_id', '=', 'fee_receipt_details.head_inst_id');
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();

        foreach($map_onetime_receipt as $map_onetime_receipt_res){
            $due_arr[] = array(
                'fees_name'                     => $map_onetime_receipt_res['ot_particular_name'],
                'head_id'                       => $map_onetime_receipt_res['head_id'],
                'head_name'                     => $map_onetime_receipt_res['ot_particular_name'],
                'head_type'                     => $map_onetime_receipt_res['head_type'],
                'pay_later_amount'              => $map_onetime_receipt_res['pay_later_amount'],
                'receipt_date'                  => $map_onetime_receipt_res['receipt_date'],
            );
        }

        foreach($map_recurring_receipt as $map_recurring_receipt_res){
            $due_arr[] = array(
                'fees_name'                     => $map_recurring_receipt_res['rc_particular_name'],
                'head_id'                       => $map_recurring_receipt_res['head_id'],
                'head_name'                     => $map_recurring_receipt_res['rc_inst_particular_name'],
                'head_type'                     => $map_recurring_receipt_res['head_type'],
                'head_inst_id'                  => $map_recurring_receipt_res['head_inst_id'],
                'pay_later_amount'              => $map_recurring_receipt_res['pay_later_amount'],
                'receipt_date'                  => $map_recurring_receipt_res['receipt_date'],
            );
        }


        $checkForTransport   = MapStudentStaffVehicle::where(function($query) use ($student_id,$session) 
        {
            $query->where('student_staff_id', $student_id);
            $query->where('student_staff_type', 1);
            $query->where('student_staff_status', 1);
            $query->where('session_id', $session['session_id']);
        })
        ->with('get_location')
        ->get();

        if(!empty($checkForTransport[0])){
            
            $checkForTransport = isset($checkForTransport[0]) ? $checkForTransport[0] : [];
            $transportAssignDdate = date("Y-m-d", strtotime($checkForTransport->created_at));

            $kms = explode(" ", $checkForTransport['get_location']->location_km_distance);
            
            $start    = (new DateTime($transportAssignDdate));
            $end      = (new DateTime($session['end_date']));
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            $headInfo = DB::select("select * from `transport_fees_head` where '".$kms[0]."' BETWEEN fees_head_km_from and fees_head_km_to
            ");

            $map_transport_receipt = ReceiptDetail::
            where('head_type', '=','TRANSPORT')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->where('inst_status', '=','0')
            ->where('map_student_staff_id', '=',$checkForTransport->map_student_staff_id)
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','TRANSPORT');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();

            $map_student_staff_id = $checkForTransport->map_student_staff_id;

            foreach ($period as $dt) {
                if(!empty($map_transport_receipt)){
                    $head_paid_amt = $head_pay_amt = 0;
                    $total_calculate_fine_amount = "";
                    $receipt_date = "";
                    foreach ($map_transport_receipt as $receiptValues) {

                        //p($receiptValues);

                        if($map_student_staff_id == $receiptValues['map_student_staff_id'] && $dt->format("Y-m") == $receiptValues['head_month_year']){
                            if($receiptValues['inst_paid_amt'] != ""){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];

                                $total_calculate_fine_amount = $receiptValues['total_calculate_fine_amount'];
                                $receipt_date = $receiptValues['receipt_date'];
                            }
                        }
                        
                    }
                }
                if(!in_array($dt->format("Y-m"), array_column($map_transport_receipt[0], 'head_month_year'))){
                    $counter = COUNT($arr_heads);
                    $headData = [];
                    $feesExpire1 = 0;
                    $total_delay_days = 0;
                    $fineinfo = [];
                    //p($headInfo);
                    $head_pay_amt =  $headInfo[0]->fees_head_amount;
                    //p($headInfo[0]->fees_head_amount);
                    if($head_pay_amt != $head_paid_amt){
                       // $head_pay_amt = $head_pay_amt - $head_paid_amt;
                    }
                    
                    if($head_paid_amt != ""){
                        $headData['fees_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_id'] = $headInfo[0]->fees_head_id;
                        $headData['head_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_type'] = "TRANSPORT";
                        $headData['pay_later_amount'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['total_calculate_fine_amount']   = $total_calculate_fine_amount;
                        $headData['receipt_date']   = $receipt_date;
    
                        $due_arr[] = $headData;
                    }
                }
            }
            
        }

      //p($due_arr);
       
        return $due_arr;
    }

    /*  Get Total fees by student id
    *   @Sumit 07 Feb 2019.
    */
    function api_get_fees_total_student($student_id,$class_id){

        $session = get_current_session();

        $fees_data          = api_get_complete_fees($student_id,$class_id);
        $paid_fees_data     = api_get_complete_paid_fees_student($student_id,$class_id);
        $due_fees_data      = api_get_complete_due_fees_student($student_id,$class_id);

        $concession_amt = ConcessionMap::where([ ['student_id',$student_id], ['class_id',$class_id], ['session_id',$session['session_id']] ])->where('concession_status' ,1)->sum('concession_amt');

        $imprest_amt = ImprestAc::where([ ['student_id',$student_id], ['class_id',$class_id], ['session_id',$session['session_id']] ])->select('imprest_ac_amt')->first();

        $wallet_amt = WalletAc::where([ ['student_id',$student_id], ['class_id',$class_id], ['session_id',$session['session_id']] ])->select('wallet_ac_amt')->first();

        $total_fees = 0;
        foreach ($fees_data as $fees_data_res) {
            $total_fees += $fees_data_res['head_amt'];
        }

        $paid_fees = 0;
        $fine_fees = 0;
        foreach ($paid_fees_data as $paid_fees_data_res) {
            $paid_fees += $paid_fees_data_res['total_amount'];
            $fine_fees += $paid_fees_data_res['total_calculate_fine_amount'];
        }

        $due_fees = 0;
        foreach ($due_fees_data as $due_fees_data_res) {
            $due_fees += $due_fees_data_res['pay_later_amount'];
        }

        $fees_summary = array(
            'total_fees'        => $total_fees,
            'paid_fees'         => $paid_fees,
            'due_fees'          => $due_fees,
            'concession_amt'    => $concession_amt,
            'fine_fees'         => $fine_fees,
            'imprest_amt'       => $imprest_amt['imprest_ac_amt'],
            'wallet_amt'        => $wallet_amt['wallet_ac_amt'],
        );
        return $fees_summary;
    }

    /*  Push Notification
    *   @Sumit 13 Feb 2019.
    */
    function api_pushnotification($module_id=null,$notification_type=null,$staff_id=null,$parent_id=null,$student_id=null,$device_ids,$message,$title=null){

        if($title != ""){
            $title = $title;
        } else {
            $title = $message;
        }
        
        $data = json_encode(array (
            'title'=>$title,
            'body'=> $message,
            'module_id' => $module_id,
            'staff_id'  => $staff_id,
            'class_id'  => "",
            'section_id'  => "",
            'parent_id' => $parent_id,
            'student_id' => $student_id,
            'notification_type' => $notification_type,
        ),true);
        $res = PushNotification::setService('fcm')
        ->setMessage([
            'notification' => [
                'title'=>$message,
                'body'=> $message,
                'module_id' => $module_id,
                'staff_id'  => $staff_id,
                'class_id'  => "",
                'section_id'  => "",
                'parent_id' => $parent_id,
                'student_id' => $student_id,
                'notification_type' => $notification_type,
            ],
            'data' => [
                'data' => $data,
            ]
        ])
        ->setApiKey('AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf')
        ->setDevicesToken($device_ids)
        ->send()
        ->getFeedback();
        return $res;
    }

    /*  Push Notification By Topic
    *   @Sumit 13 Feb 2019.
    */
    function api_pushnotification_by_topic($module_id=null,$notification_type=null,$staff_id=null,$parent_id=null,$student_id=null,$topic,$message,$class_id,$section_id,$admin_id,$title=null){
        
        $session = get_current_session();

        if($title != ""){
            $title = $title;
        } else {
            $title = $message;
        }

        $data = json_encode(array (
            'title'=>$title,
            'body'=> $message,
            'module_id' => $module_id,
            'staff_id'  => $staff_id,
            'parent_id' => $parent_id,
            'student_id' => $student_id,
            'notification_type' => $notification_type,
            'class_id'  => "'".$class_id."'",
            'section_id'  => "'".$section_id."'",
        ),true);
        
        $res = PushNotification::setService('fcm')
        ->setMessage([
            'notification' => [
                'title'=>$title,
                'body'=> $message,
                'module_id' => $module_id,
                'staff_id'  => $staff_id,
                'parent_id' => $parent_id,
                'student_id' => $student_id,
                'notification_type' => $notification_type,
                'class_id'  => "'".$class_id."'",
                'section_id'  => "'".$section_id."'",
            ],
            'data' => [
                'data' => $data,
            ]
        ])
        ->setApiKey('AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf')
        ->setConfig(['dry_run' => false])
        ->sendByTopic($topic)
        ->getFeedback();
       //  p($res);
       if(!empty($res)){
           
            $notification = New Notification();
            $topic_type = 1;
            if($topic == 'active_staff'){
                $topic_type = 3;
            } else if($topic == 'active_student'){
                $topic_type = 2;
            } else if($topic == 'active_parent'){
                $topic_type = 2;
            } else {
                $notification->class_id = $class_id;
                $notification->section_id = $section_id;
            }
            
            $notification->admin_id = $admin_id;
            $notification->update_by = $admin_id;
            $notification->notification_via = $topic_type;
            $notification->notification_type = $notification_type;
            $notification->session_id = $session['session_id'];
            $notification->module_id = $module_id;
            $notification->notification_text = $message;
            $notification->save();
       }
        return $res;
    }

    /*  Save Notification
    *   @Sumit 13 Feb 2019.
    */
    function api_save_notification($info){
        $notification = New Notification();
        $notification->admin_id = $info['admin_id'];
        $notification->update_by = $info['admin_id'];
        $notification->notification_via = $info['notification_via'];
        $notification->notification_type = $info['notification_type'];
        $notification->session_id = $info['session_id'];
        $notification->class_id = $info['class_id'];
        $notification->section_id = $info['section_id'];
        $notification->module_id = $info['module_id'];
        $notification->student_admin_id = $info['student_admin_id'];
        $notification->parent_admin_id = $info['parent_admin_id'];
        $notification->staff_admin_id = $info['staff_admin_id'];
        $notification->school_admin_id = $info['school_admin_id'];
        $notification->notification_text = $info['notification_text'];
        $notification->save();
    }