<?php

namespace App\Model\Term;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table      = 'term_exams';
    protected $primaryKey = 'term_exam_id';
}
