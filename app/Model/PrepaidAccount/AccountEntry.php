<?php

namespace App\Model\PrepaidAccount;

use Illuminate\Database\Eloquent\Model;

class AccountEntry extends Model
{
    protected $table = "account_entires";
    protected $primaryKey = "account_entiry_id";

    public function getAccountInfo()
    {
    	return $this->hasMany('App\Model\PrepaidAccount\PrepaidAccount','prepaid_account_id');
    }

    public function getStudentInfo()
    {
    	return $this->hasMany('App\Model\Student\Student','student_id');
    }
}
