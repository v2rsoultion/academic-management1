<?php

namespace App\Model\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccOpenBalance extends Model
{
    protected $table      = 'acc_open_balance';
    protected $primaryKey = 'acc_open_balance_id';

    public function getSubHead() {
        return $this->belongsTo('App\Model\Accounts\AccSubHead', 'acc_sub_head_id');
    }

    public function getAccGroupHead() {
        return $this->belongsTo('App\Model\Accounts\AccGroupHead', 'acc_group_head_id');
    }
}
