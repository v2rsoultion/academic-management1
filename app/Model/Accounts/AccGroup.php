<?php

namespace App\Model\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccGroup extends Model
{
    protected $table      = 'acc_group';
    protected $primaryKey = 'acc_group_id';

    public function getSubHead() {
        return $this->belongsTo('App\Model\Accounts\AccSubHead', 'acc_sub_head_id');
    }

    public function getGroupHeads() {
        return $this->hasMany('App\Model\Accounts\AccGroupHead', 'acc_group_id');
    }

}
