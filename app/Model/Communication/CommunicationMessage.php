<?php

namespace App\Model\Communication;

use Illuminate\Database\Eloquent\Model;

class CommunicationMessage extends Model
{
    protected $table      = 'communication_message';
    protected $primaryKey = 'comm_message_id';

    // For Student Parent
    public function getParent()
    {
        return $this->belongsTo('App\Model\Student\StudentParent','student_parent_id');
    }


    // For Staff
    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
