<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffLeaveHistoryInfo extends Model
{
    protected $table      = 'staff_leave_history_info';
    protected $primaryKey = 'staff_leave_history_info_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function getStaffLeave()
    {
        return $this->belongsTo('App\Model\Staff\StaffLeaveApplication', 'staff_leave_id');
    }

    public function getLeaveScheme()
    {
        return $this->belongsTo('App\Model\Payroll\LeaveScheme', 'pay_leave_scheme_id');
    }
}
