<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffLeaveApplication extends Model
{
    protected $table      = 'staff_leaves';
    protected $primaryKey = 'staff_leave_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function getLeaveScheme()
    {
        return $this->belongsTo('App\Model\Payroll\LeaveScheme', 'pay_leave_scheme_id');
    }

    public function getSubstitutions()
    {
        return $this->hasMany('App\Model\Staff\StaffSubstitution', 'ref_leave_id');
    }

}
