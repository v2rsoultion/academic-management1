<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffSubstitution extends Model
{
    protected $table      = 'staff_substitute';
    protected $primaryKey = 'substitute_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }
    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
    public function getLeaveInfo()
    {
        return $this->belongsTo('App\Model\Staff\StaffLeaveApplication', 'ref_leave_id');
    }
    public function getLeaveStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'leave_staff_id');
    }

}
