<?php

namespace App\Model\Country;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table      = 'country';
    protected $primaryKey = 'country_id';

    public function getCountry()
    {
        return $this->hasMany('App\Model\State\State', 'country_id');
    }
}
