<?php

namespace App\Model\TimeTable;

use Illuminate\Database\Eloquent\Model;

class TimeTableMap extends Model
{
    protected $table      = 'time_table_map';
    protected $primaryKey = 'time_table_map_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }

    public function getTimeTable()
    {
        return $this->belongsTo('App\Model\TimeTable\TimeTable', 'time_table_id');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
}
