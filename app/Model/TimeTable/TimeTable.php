<?php

namespace App\Model\TimeTable;

use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{
    protected $table      = 'time_tables';
    protected $primaryKey = 'time_table_id';

    public function timeTableClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function timeTableSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
}
