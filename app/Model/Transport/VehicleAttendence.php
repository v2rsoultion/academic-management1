<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class VehicleAttendence extends Model
{
    protected $table      = 'vehicle_attendence';
    protected $primaryKey = 'vehicle_attendence_id';

    public function attendence_details()
    {
        return $this->hasMany('App\Model\Transport\VehicleAttendDetails', 'vehicle_attendence_id');
    }

    public function GetVehicle()
    {
        return $this->belongsTo('App\Model\Transport\Vehicle', 'vehicle_id');
    }
 	
}
