<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class MapStudentStaffVehicle extends Model
{
    protected $table      = 'map_student_staff_vehicle';
    protected $primaryKey = 'map_student_staff_id';

    public function get_student()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

    public function get_staff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function get_location()
    {
        return $this->belongsTo('App\Model\Transport\RouteLocation', 'route_location_id');
    }


}
