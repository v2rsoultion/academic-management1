<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class RouteLocation extends Model
{
    protected $table      = 'route_locations';
    protected $primaryKey = 'route_location_id';
}
