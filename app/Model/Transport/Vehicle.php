<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table      = 'vehicles';
    protected $primaryKey = 'vehicle_id';

    public function get_vehicle_documents()
    {
        return $this->hasMany('App\Model\Transport\VehicleDocument', 'vehicle_id');
    }

    public function get_driver_conductor()
    {
        return $this->belongsTo('App\Model\Transport\AssignDriverConductorRoute', 'vehicle_id');
    }

    public function getDriver()
    {
        return $this->belongsTo('App\Model\Staff\Staff','driver_id');
    }

    public function getConductor()
    {
        return $this->belongsTo('App\Model\Staff\Staff','conductor_id');
    }

    public function getRoute()
    {
        return $this->belongsTo('App\Model\Transport\Route','route_id');
    }


    public function get_student()
    {
        return $this->hasMany('App\Model\Transport\MapStudentStaffVehicle', 'vehicle_id');
    }

    public function get_staff()
    {
        return $this->hasMany('App\Model\Transport\MapStudentStaffVehicle', 'vehicle_id');
    }

 	
}
