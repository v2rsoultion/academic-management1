<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class FeesHead extends Model
{
    protected $table      = 'transport_fees_head';
    protected $primaryKey = 'fees_head_id';
}
