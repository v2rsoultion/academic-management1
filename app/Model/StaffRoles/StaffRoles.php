<?php

namespace App\Model\StaffRoles;

use Illuminate\Database\Eloquent\Model;

class StaffRoles extends Model
{
    protected $table      = 'staff_roles';
    protected $primaryKey = 'staff_role_id';
}
