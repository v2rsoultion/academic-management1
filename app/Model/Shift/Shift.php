<?php

namespace App\Model\Shift;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $table      = 'shifts';
    protected $primaryKey = 'shift_id';
}
