<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelBlock extends Model
{
    protected $table      = 'hostel_blocks';
    protected $primaryKey = 'h_block_id';

    public function getFloors()
    {
        return $this->hasMany('App\Model\Hostel\HostelFloor', 'h_block_id');
    }

    public function getHostel()
    {
        return $this->belongsTo('App\Model\Hostel\Hostel', 'hostel_id');
    }
}
