<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelStudentMap extends Model
{
    protected $table      = 'hostel_student_map';
    protected $primaryKey = 'h_student_map_id';

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
    public function getHostel()
    {
        return $this->belongsTo('App\Model\Hostel\Hostel', 'hostel_id');
    }

    public function getBlock()
    {
        return $this->belongsTo('App\Model\Hostel\HostelBlock', 'h_block_id');
    }

    public function getFloor()
    {
        return $this->belongsTo('App\Model\Hostel\HostelFloor', 'h_floor_id');
    }

    public function getRoom()
    {
        return $this->belongsTo('App\Model\Hostel\HostelRooms', 'h_room_id');
    }

    public function getRoomCate()
    {
        return $this->belongsTo('App\Model\Hostel\HostelRoomCategory', 'h_room_cate_id');
    }
}
