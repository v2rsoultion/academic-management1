<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelFloor extends Model
{
    protected $table      = 'hostel_floors';
    protected $primaryKey = 'h_floor_id';

    public function getRooms()
    {
        return $this->hasMany('App\Model\Hostel\HostelRooms', 'h_floor_id');
    }
}
