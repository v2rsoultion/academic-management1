<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelFeeReceipt extends Model
{
    protected $table      = 'hostel_fees';
    protected $primaryKey = 'hostel_fee_id';

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'h_fee_class_id');
    }

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'h_fee_student_id');
    }

    public function getStudentMapping()
    {
        return $this->belongsTo('App\Model\Hostel\HostelStudentMap', 'h_fee_student_map_id');
    }

    public function getBank()
    {
        return $this->belongsTo('App\Model\FeesCollection\Bank', 'bank_id');
    }
    // For student Academic Info
    public function getStudentAcademic()
    {
        return $this->belongsTo('App\Model\Student\StudentAcademic','h_fee_student_id');
    }

    // For Installments
    public function ReceiptDetail()
    {
        return $this->belongsTo('App\Model\Hostel\HostelFeeReceiptDetail','hostel_fee_id');
    }
}
