<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelFeeReceiptDetail extends Model
{
    protected $table      = 'hostel_fee_details';
    protected $primaryKey = 'hostel_fee_detail_id';
}
