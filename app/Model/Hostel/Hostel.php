<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
    protected $table      = 'hostels';
    protected $primaryKey = 'hostel_id';
}
