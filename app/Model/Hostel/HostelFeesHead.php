<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelFeesHead extends Model
{
    protected $table      = 'hostel_fees_head';
    protected $primaryKey = 'h_fees_head_id';
}
