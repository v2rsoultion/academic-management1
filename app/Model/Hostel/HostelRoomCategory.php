<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelRoomCategory extends Model
{
    protected $table      = 'hostel_room_cate';
    protected $primaryKey = 'h_room_cate_id';
}
