<?php

namespace App\Model\MarksheetTemplate;

use Illuminate\Database\Eloquent\Model;

class MarksheetTemplate extends Model
{
    protected $table      = 'marksheet_templates';
    protected $primaryKey = 'marksheet_template_id';
}
