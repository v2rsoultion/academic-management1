<?php

namespace App\Model\BookVendor;

use Illuminate\Database\Eloquent\Model;

class BookVendor extends Model
{
    protected $table      = 'book_vendors';
    protected $primaryKey = 'book_vendor_id';
}
