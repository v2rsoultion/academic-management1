<?php

namespace App\Model\Brochure;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model
{
    protected $table      = 'brochures';
    protected $primaryKey = 'brochure_id';

    public function getSessionInfo()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }
}
