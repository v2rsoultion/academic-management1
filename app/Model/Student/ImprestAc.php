<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class ImprestAc extends Model
{
    protected $table      = 'imprest_ac';
    protected $primaryKey = 'imprest_ac_id';

    // For Student info 
    public function getStudentInfo()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
}
