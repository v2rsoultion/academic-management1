<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentParent extends Model
{
    protected $table      = 'student_parents';
    protected $primaryKey = 'student_parent_id';

    public function getStudent()
    {
        return $this->hasMany('App\Model\Student\Student','student_parent_id');
    }

    public function getSchool()
    {
        return $this->belongsTo('App\Model\School\School','school_id');
    }
    
    public function getParentStudent()
    {
        return $this->hasOne('App\Model\Student\Student','student_parent_id');
    }

    public function getStudents()
    {
        return $this->hasMany('App\Model\Student\Student','student_parent_id');
    }

    public function getParentAdminInfo()
    {
        return $this->belongsTo('App\Admin','reference_admin_id');
    }


}
