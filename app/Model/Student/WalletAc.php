<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class WalletAc extends Model
{
    protected $table      = 'wallet_ac';
    protected $primaryKey = 'wallet_ac_id';

    // For Student info 
    public function getStudentInfo()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
}
