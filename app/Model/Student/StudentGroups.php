<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentGroups extends Model
{
    protected $table      = 'st_parent_groups';
    protected $primaryKey = 'st_parent_group_id';
}
