<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;
use DB;
class Student extends Model
{
    protected $table      = 'students';
    protected $primaryKey = 'student_id';


    // For Student Parent
    public function getParent()
    {
        return $this->belongsTo('App\Model\Student\StudentParent','student_parent_id');
    }

    // For student Academic Info
    public function getStudentAcademic()
    {
        return $this->hasOne('App\Model\Student\StudentAcademic','student_id');
    }

    // For Student Health
    public function getStudentHealth()
    {
        return $this->hasOne('App\Model\Student\StudentHealth','student_id');
    }

    // For Caste
    public function getStudentCaste()
    {
        return $this->belongsTo('App\Model\Caste\Caste','caste_id');
    }

    // For Religion
    public function getStudentReligion()
    {
        return $this->belongsTo('App\Model\Religion\Religion','religion_id');
    }

    // For Nationality
    public function getStudentNationality()
    {
        return $this->belongsTo('App\Model\Nationality\Nationality','nationality_id');
    }

    // For Sibling Class 
    public function getSiblingClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'student_sibling_class_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Model\Student\StudentDocument', 'student_id');
    }

    public function getRTEInfo()
    {
        return $this->hasMany('App\Model\FeesCollection\RTEHeadMap', 'student_id');
    }

    // For Imprest Ac 
    public function getImprestAc()
    {
        return $this->hasMany('App\Model\Student\ImprestAc', 'student_id');
    }

    // For Wallet Ac
    public function getWalletAc()
    {
        return $this->hasMany('App\Model\Student\WalletAc', 'student_id');
    }

    // For Temporary City
    public function getTemporaryCity()
    {
        return $this->belongsTo('App\Model\City\City', 'student_temporary_city');
    }

    // For Temporary State
    public function getTemporaryState()
    {
        return $this->belongsTo('App\Model\State\State', 'student_temporary_state');
    }

    // For Temporary Country
    public function getTemporaryCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'student_temporary_county');
    }

    // For Permanent City
    public function getPermanentCity()
    {
        return $this->belongsTo('App\Model\City\City', 'student_permanent_city');
    }

    // For Permanent State
    public function getPermanentState()
    {
        return $this->belongsTo('App\Model\State\State', 'student_permanent_state');
    }

    // For Permanent City
    public function getPermanentCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'student_permanent_county');
    }
    // For student hostel Info
    public function getStudentHostelMap()
    {
        return $this->hasOne('App\Model\Hostel\HostelStudentMap','student_id');
    }

    // For competition Info
    public function getCompetitionMap()
    {
        return $this->belongsTo('App\Model\Competition\CompetitionMapping','student_id');
    }

    // For student Academic History Info
    public function getStudentAcademicHistory()
    {
        return $this->hasMany('App\Model\Student\StudentAcademicHistory','student_id');
    }

    // For student Medium Type
    public function getStudentMedium()
    {
        return $this->belongsTo('App\Model\Mediums\Mediums','medium_type');
    }

    // For Title
    public function getTitle()
    {
        return $this->belongsTo('App\Model\Title\Title','title_id');
    }

    public function leaves()
    {
        return $this->hasMany('App\Model\StudentLeaveApplication\StudentLeaveApplication', 'student_id');
    }

    public function get_subject_with_marks()
    {
        return $this->hasMany('App\Model\Examination\ExamMarks', 'student_id');
    }


    // For student Attendence
    public function getStudentAttendence()
    {
        return $this->hasMany('App\Model\Student\StudentAttendanceDetails','student_id');
    }

    // For student Remaarks
    public function remarks()
    {
        return $this->hasMany('App\Model\Remarks\Remarks','student_id');
    }

    // For student Examination schedule
    public function ExamSchedule()
    {
        return $this->hasMany('App\Model\Exam\ExamScheduleMap','student_id');
    }

    public function getAdminInfo()
    {
        return $this->belongsTo('App\Admin','reference_admin_id');
    }
    public function getTc()
    {
        return $this->hasOne('App\Model\Student\StudentTC','student_id');
    }
    public function getCc()
    {
        return $this->hasOne('App\Model\Student\StudentCC','student_id');
    }

    public function getSections()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

    public function getMarksheet()
    {
        return $this->belongsTo('App\Model\Examination\StMarksheet', 'student_id');
    }

    public function getStudentMember()
    {
        return $this->belongsTo('App\Model\IssueBook\IssueBook', 'member_id');
    }
    
    public function marksheetRecords()
    {
        return $this->hasMany('App\Model\Examination\StMarksheet','student_id');
    }

    public function getStudentReceipts()
    {
        return $this->hasMany('App\Model\FeesCollection\Receipt','student_id');
    }

    public function getStudentHFeesReceipts()
    {
        return $this->hasMany('App\Model\Hostel\HostelFeeReceipt','h_fee_student_id');
    }

}
