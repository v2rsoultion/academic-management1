<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentAcademicHistory extends Model
{
    protected $table      = 'student_academic_history_info';
    protected $primaryKey = 'student_academic_history_info_id';

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }

    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session', 'session_id');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
}
