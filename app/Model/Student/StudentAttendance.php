<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    protected $table      = 'student_attendance';
    protected $primaryKey = 'student_attendance_id';
}
