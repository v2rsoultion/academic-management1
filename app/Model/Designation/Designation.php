<?php

namespace App\Model\Designation;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table      = 'designations';
    protected $primaryKey = 'designation_id';
}
