<?php

namespace App\Model\LibraryFine;

use Illuminate\Database\Eloquent\Model;

class LibraryFine extends Model
{
    protected $table      = 'library_fine';
    protected $primaryKey = 'library_fine_id';
}
