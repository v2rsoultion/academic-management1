<?php

namespace App\Model\Mediums;

use Illuminate\Database\Eloquent\Model;

class Mediums extends Model
{
    protected $table      = 'medium_type';
    protected $primaryKey = 'medium_type_id';
}
