<?php

namespace App\Model\VirtualClass;

use Illuminate\Database\Eloquent\Model;

class VirtualClass extends Model
{
    protected $table      = 'virtual_classes';
    protected $primaryKey = 'virtual_class_id';
}
