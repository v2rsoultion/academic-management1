<?php

namespace App\Model\IssueBook;

use Illuminate\Database\Eloquent\Model;

class IssueBook extends Model
{
    protected $table      = 'issued_books';
    protected $primaryKey = 'issued_book_id';

    // For CupBoard
    public function getBook()
    {
        return $this->belongsTo('App\Model\Book\Book','book_id');
    }

    public function getBookInfo()
    {
        return $this->hasOne('App\Model\Book\BookCopiesInfo','book_info_id');
    }
    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
