<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class PfSetting extends Model
{
    protected $table      = 'pf_setting';
    protected $primaryKey = 'pf_setting_id';
}
