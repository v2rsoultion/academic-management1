<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table      = 'pay_dept';
    protected $primaryKey = 'pay_dept_id';

    public function departmentMap() {
        return $this->hasMany('App\Model\Payroll\PayrollMapping','pay_dept_id');
    }
}
