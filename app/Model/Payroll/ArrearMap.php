<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class ArrearMap extends Model
{
    protected $table      = 'pay_arrear_map';
    protected $primaryKey = 'pay_arrear_map_id';

    public function getArrearMap() {
        return $this->belongsTo('App\Model\Payroll\Arrear','pay_arrear_id');
    }

    public function ArrearStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
