<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table      = 'pay_grade';
    protected $primaryKey = 'pay_grade_id';

    public function gradeMap() {
        return $this->hasMany('App\Model\Payroll\PayrollMapping','pay_grade_id');
    }
}
