<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Advance extends Model
{
    protected $table = 'pay_advance';
    protected $primaryKey = 'pay_advance_id';

    public function AdvanceStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
