<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class BonusMap extends Model
{
    protected $table      = 'pay_bonus_map';
    protected $primaryKey = 'pay_bonus_map_id';

    public function getBonusMap() {
        return $this->belongsTo('App\Model\Payroll\Bonus','pay_bonus_id');
    }

    public function BonusStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
