<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class EsiSetting extends Model
{
    protected $table = 'esi_setting';
    protected $primaryKey = 'esi_setting_id';
}
