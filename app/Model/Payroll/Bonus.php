<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table      = 'pay_bonus';
    protected $primaryKey = 'pay_bonus_id';

    public function BonusMap() {
        return $this->hasMany('App\Model\Payroll\BonusMap','pay_bonus_id');
    }
}
