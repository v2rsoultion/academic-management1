<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class LeaveScheme extends Model
{
    protected $table      = 'pay_leave_scheme';
    protected $primaryKey = 'pay_leave_scheme_id';

    public function LeaveSchemeMap() {
        return $this->hasMany('App\Model\Payroll\LeaveSchemeMap','pay_leave_scheme_map_id');
    }
}
