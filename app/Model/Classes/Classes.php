<?php

namespace App\Model\Classes;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table      = 'classes';
    protected $primaryKey = 'class_id';

    public function getSections()
    {
        return $this->hasMany('App\Model\Section\Section', 'class_id');
    }

    public function getTimeTable()
    {
        return $this->hasMany('App\Model\TimeTable\TimeTable', 'class_id');
    }

    public function getMedium()
    {
        return $this->belongsTo('App\Model\Mediums\Mediums','medium_type');
    }
    public function getBoard()
    {
        return $this->belongsTo('App\Model\School\SchoolBoard','board_id');
    }

    public function getClassReceipts()
    {
        return $this->hasMany('App\Model\FeesCollection\Receipt','class_id');
    }

}
