<?php

namespace App\Model\Admission;

use Illuminate\Database\Eloquent\Model;

class AdmissionData extends Model
{
    protected $table      = 'admission_data';
    protected $primaryKey = 'admission_data_id';

    public function getSessionInfo()
    {
        return $this->belongsTo('App\Model\Session\Session','current_session_id');
    }

    public function getClassInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes','current_class_id');
    }
    public function getFormInfo()
    {
        return $this->belongsTo('App\Model\Admission\AdmissionForm','admission_form_id');
    }
    public function getFormFieldsInfo()
    {
        return $this->belongsTo('App\Model\Admission\AdmissionFields','admission_form_id');
    }

    // For Caste
    public function getStudentCaste()
    {
        return $this->belongsTo('App\Model\Caste\Caste','caste_id');
    }

    // For Religion
    public function getStudentReligion()
    {
        return $this->belongsTo('App\Model\Religion\Religion','religion_id');
    }

    // For Nationality
    public function getStudentNationality()
    {
        return $this->belongsTo('App\Model\Nationality\Nationality','nationality_id');
    }

    // For Sibling Class 
    public function getSiblingClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'student_sibling_class_id');
    }

    // For Temporary City
    public function getTemporaryCity()
    {
        return $this->belongsTo('App\Model\City\City', 'student_temporary_city');
    }

    // For Temporary State
    public function getTemporaryState()
    {
        return $this->belongsTo('App\Model\State\State', 'student_temporary_state');
    }

    // For Temporary Country
    public function getTemporaryCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'student_temporary_county');
    }

    // For Permanent City
    public function getPermanentCity()
    {
        return $this->belongsTo('App\Model\City\City', 'student_permanent_city');
    }

    // For Permanent State
    public function getPermanentState()
    {
        return $this->belongsTo('App\Model\State\State', 'student_permanent_state');
    }

    // For Permanent City
    public function getPermanentCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'student_permanent_county');
    }

    public function getAdmissionDataInfo()
    {
        return $this->belongsTo('App\Model\Admission\AdmissionForm','admission_form_id');
    }
}
