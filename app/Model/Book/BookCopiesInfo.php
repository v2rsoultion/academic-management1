<?php

namespace App\Model\Book;

use Illuminate\Database\Eloquent\Model;

class BookCopiesInfo extends Model
{
    protected $table      = 'book_copies_info';
    protected $primaryKey = 'book_info_id';
}
