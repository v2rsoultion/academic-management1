<?php

namespace App\Model\Book;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table      = 'books';
    protected $primaryKey = 'book_id';

    // For CupBoard
    public function getCupBoard()
    {
        return $this->belongsTo('App\Model\BookCupboard\BookCupboard','book_cupboard_id');
    }

    // For CupBoard
    public function getCupBoardShelf()
    {
        return $this->belongsTo('App\Model\BookCupboardshelf\BookCupboardshelf','book_cupboardshelf_id');
    }

    // For Book Category
    public function getCategory()
    {
        return $this->belongsTo('App\Model\BookCategory\BookCategory','book_category_id');
    }

    // For Book Copies
    public function getBookCopies()
    {
        return $this->hasMany('App\Model\Book\BookCopiesInfo','book_id');
    }

    // For Book Vendor
    public function getBookVendor()
    {
        return $this->belongsTo('App\Model\BookVendor\BookVendor','book_vendor_id');
    }
}
