<?php

namespace App\Model\Address;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table      = 'city';
    protected $primaryKey = 'city_id';
    
    public function stateInfo()
    {
        return $this->belongsTo('App\Model\Address\State','state_id');
    }
}
