<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;

class ExamScheduleRoomMap extends Model
{
    protected $table      = 'schedule_room_map';
    protected $primaryKey = 'schedule_room_map_id';

    public function getRoom()
    {
        return $this->belongsTo('App\Model\RoomNo\RoomNo', 'room_no_id');
    }
}
