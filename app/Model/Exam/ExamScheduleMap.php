<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;

class ExamScheduleMap extends Model
{
    protected $table      = 'schedule_map';
    protected $primaryKey = 'schedule_map_id';

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }

    public function getSubjectData()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }

    public function getRoomMap()
    {
        return $this->belongsTo('App\Model\Exam\ExamScheduleRoomMap', 'exam_schedule_id');
    }

    public function getExam()
    {
        return $this->belongsTo('App\Model\Exam\Exam', 'exam_id');
    }

    public function getSchedule()
    {
        return $this->belongsTo('App\Model\Exam\ExamSchedules', 'exam_schedule_id');
    }
}
