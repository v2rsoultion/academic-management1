<?php

namespace App\Model\HomeworkGroup;

use Illuminate\Database\Eloquent\Model;

class HomeworkConv extends Model
{
    protected $table      = 'homework_conversations';
    protected $primaryKey = 'h_conversation_id';
    protected $dates        = ['created_at'];
    
    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }

    public function getGroup()
    {
        return $this->belongsTo('App\Model\HomeworkGroup\HomeworkGroup','homework_group_id');
    }

}
