<?php

namespace App\Model\HomeworkGroup;

use Illuminate\Database\Eloquent\Model;

class HomeworkGroup extends Model
{
    protected $table      = 'homework_groups';
    protected $primaryKey = 'homework_group_id';

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }
    public function getMessages()
    {
        return $this->hasMany('App\Model\HomeworkGroup\HomeworkConv','homework_group_id');
    }
    // Use for join
    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }
}
