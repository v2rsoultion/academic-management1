<?php

namespace App\Model\SchoolGroup;

use Illuminate\Database\Eloquent\Model;

class SchoolGroup extends Model
{
    protected $table      = 'groups';
    protected $primaryKey = 'group_id';
}
