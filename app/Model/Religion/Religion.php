<?php

namespace App\Model\Religion;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $table      = 'religions';
    protected $primaryKey = 'religion_id';
}
