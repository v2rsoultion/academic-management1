<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class ExamMarks extends Model
{
    protected $table      = 'exam_marks';
    protected $primaryKey = 'exam_mark_id';

    public function getMarksCriteria()
    {
        return $this->belongsTo('App\Model\Examination\MarksCriteria', 'grade_scheme_id');
    }

    public function marksCriteriaInfo()
    {
        return $this->belongsTo('App\Model\Examination\MarksCriteria', 'marks_criteria_id');
    }
    public function subjectInfo()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }
    public function getStudentDetails()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
    public function GradeSchemeInfo()
    {
        return $this->belongsTo('App\Model\Examination\GradeScheme', 'grade_scheme_id');
    }
    public function getMap()
    {
        return $this->belongsTo('App\Model\Examination\ExamMap', 'exam_id');
    }
   
}
