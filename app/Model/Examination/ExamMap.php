<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class ExamMap extends Model
{
    protected $table      = 'exam_map';
    protected $primaryKey = 'exam_map_id';

    public function classInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
    public function sectionInfo()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
    public function examInfo()
    {
        return $this->belongsTo('App\Model\Exam\Exam', 'exam_id');
    }
    public function subjectInfo()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }
    public function marksCriteriaInfo()
    {
        return $this->belongsTo('App\Model\Examination\MarksCriteria', 'marks_criteria_id');
    }
    public function gradeSchemeInfo()
    {
        return $this->belongsTo('App\Model\Examination\GradeScheme', 'grade_scheme_id');
    }
    public function getClasses()
    {
        return $this->hasMany('App\Model\Classes\Classes', 'class_id');
    }
}
