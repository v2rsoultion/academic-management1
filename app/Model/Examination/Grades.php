<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $table      = 'grades';
    protected $primaryKey = 'grade_id';

    public function getGrades()
    {
        return $this->belongsTo('App\Model\Examination\GradeScheme', 'grade_scheme_id');
    }
}
