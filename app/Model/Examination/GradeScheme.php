<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class GradeScheme extends Model
{
    protected $table      = 'grade_schemes';
    protected $primaryKey = 'grade_scheme_id';

    public function getGrades()
    {
        return $this->hasMany('App\Model\Examination\Grades', 'grade_scheme_id');
    }
}
