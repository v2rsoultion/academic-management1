<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class StudentExamResult extends Model
{
    protected $table      = 'st_exam_result';
    protected $primaryKey = 'st_exam_result_id';

    public function classInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
    public function examInfo()
    {
        return $this->belongsTo('App\Model\Exam\Exam', 'exam_id');
    }
    
}
