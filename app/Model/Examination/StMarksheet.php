<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class StMarksheet extends Model
{
    protected $table      = 'student_marksheet';
    protected $primaryKey = 'student_marksheet_id';

    public function classInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
    public function sectionInfo()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
    public function examInfo()
    {
        return $this->belongsTo('App\Model\Exam\Exam', 'exam_id');
    }
    public function studentInfo()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
    
}
