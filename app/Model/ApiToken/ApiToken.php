<?php

namespace App\Model\ApiToken;

use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{
    protected $table      = 'api_tokens';
    protected $primaryKey = 'token_id';
}
