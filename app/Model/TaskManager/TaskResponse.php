<?php

namespace App\Model\TaskManager;

use Illuminate\Database\Eloquent\Model;

class TaskResponse extends Model
{
    protected $table      = 'task_response';
    protected $primaryKey = 'task_response_id';

    public function GetTask()
    {
        return $this->belongsTo('App\Model\TaskManager\Task', 'task_id');
    }
}
