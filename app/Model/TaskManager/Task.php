<?php

namespace App\Model\TaskManager;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table      = 'tasks';
    protected $primaryKey = 'task_id';

    public function TaskResponse()
    {
        return $this->hasMany('App\Model\TaskManager\TaskResponse', 'task_id');
    }

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function getAdmin()
    {
        return $this->belongsTo('App\Model\Admin\Admin', 'admin_id');
    }

     // For Sibling Class 
    public function getSiblingClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSections()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

}
