<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;

class PurchaseItems extends Model
{
    protected $table      = 'purchase_items';
    protected $primaryKey = 'purchase_items_id';

    public function getPurchaseItems()
    {
        return $this->belongsTo('App\Model\Purchase\Purchase','purchase_id');
    }
    public function purchase_items()
    {
        return $this->belongsTo('App\Model\InventoryItem\Items', 'item_id');
    }
    public function items()
    {
        return $this->belongsTo('App\Model\InventoryItem\Items', 'item_id');
    }

    public function units()
    {
        return $this->belongsTo('App\Model\InventoryUnit\Unit', 'unit_id');
    }

    public function category() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'category_id');
    }

    public function subCategory() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'sub_category_id');
    }

}
