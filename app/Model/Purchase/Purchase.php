<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table      = 'purchases';
    protected $primaryKey = 'purchase_id';

    // public function items()
    // {
    //     return $this->hasMany('App\Model\Purchase\PurchaseItems', 'purchase_id');
    // }
    public function purchaseItems() {
        return $this->hasMany('App\Model\Purchase\PurchaseItems','purchase_id');
    }
    public function category() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'category_id');
    }

    public function subCategory() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'subcategory_id');
    }

    public function vendor() {
        return $this->belongsTo('App\Model\InventoryVendor\Vendor', 'vendor_id');
    }

    public function bank() {
        return $this->belongsTo('App\Model\FeesCollection\Bank', 'bank_id');
    }

}
