<?php

namespace App\Model\StudentLeaveApplication;

use Illuminate\Database\Eloquent\Model;

class StudentLeaveApplication extends Model
{
    protected $table      = 'student_leaves';
    protected $primaryKey = 'student_leave_id';

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

}
