<?php

namespace App\Model\Permissions;

use Illuminate\Database\Eloquent\Model;

class MasterModule extends Model
{
    protected $table      = 'master_modules';
    protected $primaryKey = 'master_module_id';
}
