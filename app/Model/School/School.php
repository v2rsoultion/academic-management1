<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table      = 'school';
    protected $primaryKey = 'school_id';

    public function last()
    {
        return $this->hasOne('App\Model\School\School','school_id');
    }

    public function getCountry()
    {
        return $this->belongsTo('App\Model\Address\Country','country_id');
    }

    public function getState()
    {
        return $this->belongsTo('App\Model\Address\State','state_id');
    }

    public function getCity()
    {
        return $this->belongsTo('App\Model\Address\City','city_id');
    }
}
