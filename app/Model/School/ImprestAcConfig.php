<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class ImprestAcConfig extends Model
{
    protected $table      = 'imprest_ac_confi';
    protected $primaryKey = 'imprest_ac_confi_id';
}
