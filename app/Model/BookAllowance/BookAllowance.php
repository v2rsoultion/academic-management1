<?php

namespace App\Model\BookAllowance;

use Illuminate\Database\Eloquent\Model;

class BookAllowance extends Model
{
    protected $table      = 'book_allowance';
    protected $primaryKey = 'book_allowance_id';
}
