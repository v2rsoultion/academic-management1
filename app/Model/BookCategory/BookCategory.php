<?php

namespace App\Model\BookCategory;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    protected $table      = 'book_categories';
    protected $primaryKey = 'book_category_id';
}
