<?php

namespace App\Model\Recruitment;

use Illuminate\Database\Eloquent\Model;

class RecruitmentForm extends Model
{
    protected $table      = 'job_applied_cadidates';
    protected $primaryKey = 'applied_cadidate_id';

    public function GetJob()
    {
        return $this->belongsTo('App\Model\Recruitment\Job','job_id');
    }

    public function getDesignation()
    {
        return $this->belongsTo('App\Model\Designation\Designation', 'designation_id');
    }

    public function getCaste()
    {
        return $this->belongsTo('App\Model\Caste\Caste', 'caste_id');
    }

    public function getNationality()
    {
        return $this->belongsTo('App\Model\Nationality\Nationality', 'nationality_id');
    }

    public function getReligion()
    {
        return $this->belongsTo('App\Model\Religion\Religion', 'religion_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Model\Staff\StaffDocument', 'staff_id');
    }

    public function getTemporaryCity()
    {
        return $this->belongsTo('App\Model\City\City', 'staff_temporary_city');
    }

    public function getTemporaryState()
    {
        return $this->belongsTo('App\Model\State\State', 'staff_temporary_state');
    }

    public function getTemporaryCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'staff_temporary_county');
    }

    public function getPermanentCity()
    {
        return $this->belongsTo('App\Model\City\City', 'staff_permanent_city');
    }

    public function getPermanentState()
    {
        return $this->belongsTo('App\Model\State\State', 'staff_permanent_state');
    }

    public function getPermanentCountry()
    {
        return $this->belongsTo('App\Model\Country\Country', 'staff_permanent_county');
    }

    public function getTitle()
    {
        return $this->belongsTo('App\Model\Title\Title', 'title_id');
    }

    public function leaves()
    {
        return $this->hasMany('App\Model\Staff\StaffLeaveApplication', 'staff_id');
    }

    public function getStaffAdminInfo()
    {
        return $this->belongsTo('App\Admin','reference_admin_id');
    }

}
