<?php

namespace App\Model\Recruitment;

use Illuminate\Database\Eloquent\Model;

class JobFields extends Model
{
    protected $table      = 'job_form_fields';
    protected $primaryKey = 'job_form_field_id';

    public function GetJobForms()
    {
        return $this->belongsTo('App\Model\Recruitment\JobForm','job_form_id');
    }
}
