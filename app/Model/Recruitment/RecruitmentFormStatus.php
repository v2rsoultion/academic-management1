<?php

namespace App\Model\Recruitment;

use Illuminate\Database\Eloquent\Model;

class RecruitmentFormStatus extends Model
{
    protected $table      = 'job_applied_cadidate_status';
    protected $primaryKey = 'applied_cadidate_status_id';

    public function GetJob()
    {
        return $this->belongsTo('App\Model\Recruitment\Job','job_id');
    }

    public function GetAppliedCandidateDetails()
    {
        return $this->hasMany('App\Model\Recruitment\RecruitmentForm','applied_cadidate_id');
    }

    public function GetForm()
    {
        return $this->belongsTo('App\Model\Recruitment\JobForm','job_form_id');
    }
}
