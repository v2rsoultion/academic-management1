<?php

namespace App\Model\Visitor;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table      = 'visitors';
    protected $primaryKey = 'visitor_id';
}
