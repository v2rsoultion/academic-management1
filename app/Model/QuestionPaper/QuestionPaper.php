<?php

namespace App\Model\QuestionPaper;

use Illuminate\Database\Eloquent\Model;

class QuestionPaper extends Model
{
    protected $table      = 'question_papers';
    protected $primaryKey = 'question_paper_id';

    // For Class
    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    // For Section
    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }

    // For Subject
    public function getSubjects()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }

    // For Exam Type
    public function getExamType()
    {
        return $this->belongsTo('App\Model\Exam\Exam','exam_id');
    }

    // For Subject Class Mapping
    public function getSubjectClassMapping()
    {
        return $this->belongsTo('App\Model\SubjectClassMapping\SubjectClassMapping', 'subject_class_map_id');
    }
}
