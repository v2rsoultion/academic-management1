<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class OneTime extends Model
{
    protected $table      = 'one_time_heads';
    protected $primaryKey = 'ot_head_id';
}
