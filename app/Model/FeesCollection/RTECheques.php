<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class RTECheques extends Model
{
    protected $table      = 'rte_cheques';
    protected $primaryKey = 'rte_cheque_id';

    public function getBankInfo()
    {
        return $this->belongsTo('App\Model\FeesCollection\Bank','bank_id');
    }
}
