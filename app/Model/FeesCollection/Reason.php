<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    protected $table      = 'reasons';
    protected $primaryKey = 'reason_id';
}
