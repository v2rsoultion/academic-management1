<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class STCheques extends Model
{
    protected $table      = 'student_cheques';
    protected $primaryKey = 'student_cheque_id';

    public function getBankInfo()
    {
        return $this->belongsTo('App\Model\FeesCollection\Bank','bank_id');
    }

    public function getStudentInfo()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }

    public function getClassInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes','cheques_class_id');
    }
}
