<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class ReceiptDetail extends Model
{
    protected $table      = 'fee_receipt_details';
    protected $primaryKey = 'fee_receipt_detail_id';

    public function getInstallmentInfo()
    {
        return $this->belongsTo('App\Model\FeesCollection\RecurringInst', 'head_inst_id');
    }
    public function getReceiptInfo()
    {
        return $this->belongsTo('App\Model\FeesCollection\Receipt', 'receipt_id');
    }
    public function getHeadNameInfo()
    {
        return $this->belongsTo('App\Model\FeesCollection\OneTime', 'head_id');
    }
}
