<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class RecurringHead extends Model
{
    protected $table      = 'recurring_heads';
    protected $primaryKey = 'rc_head_id';

    // For Installments
    public function getInstallments()
    {
        return $this->hasMany('App\Model\FeesCollection\RecurringInst','rc_head_id');
    }
}
