<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class ConcessionMap extends Model
{
    protected $table      = 'concession_map';
    protected $primaryKey = 'concession_map_id';


    public function getHeadName()
    {
        return $this->belongsTo('App\Model\FeesCollection\OneTime', 'head_id');
    }

    public function getStudentConcession()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

    public function getClassInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }
}
