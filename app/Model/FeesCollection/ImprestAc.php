<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class ImprestAc extends Model
{
    protected $table      = 'imprest_ac';
    protected $primaryKey = 'imprest_ac_id';
}
