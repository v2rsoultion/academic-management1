<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class Fine extends Model
{
    protected $table      = 'fine';
    protected $primaryKey = 'fine_id';

    // For Fine Details
    public function getFineDetails()
    {
        return $this->hasMany('App\Model\FeesCollection\FineDetails','fine_id');
    }
}
