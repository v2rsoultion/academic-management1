<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $table      = 'fee_receipt';
    protected $primaryKey = 'receipt_id';

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

    public function getBank()
    {
        return $this->belongsTo('App\Model\FeesCollection\Bank', 'bank_id');
    }

    // For Installments
    public function getInstallments()
    {
        return $this->hasMany('App\Model\FeesCollection\ReceiptDetail','receipt_id');
    }
    // For student Academic Info
    public function getStudentAcademic()
    {
        return $this->belongsTo('App\Model\Student\StudentAcademic','student_id');
    }

    // For Installments
    public function ReceiptDetail()
    {
        return $this->belongsTo('App\Model\FeesCollection\ReceiptDetail','receipt_id');
    }

    public function ReceiptDetailAll()
    {
        return $this->hasMany('App\Model\FeesCollection\ReceiptDetail','receipt_id');
    }

    
}
