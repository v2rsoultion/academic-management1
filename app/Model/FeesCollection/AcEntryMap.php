<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class AcEntryMap extends Model
{
    protected $table      = 'ac_entry_map';
    protected $primaryKey = 'ac_entry_map_id';

    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }
}
