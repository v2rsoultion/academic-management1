<?php

namespace App\Model\LibraryMember;

use Illuminate\Database\Eloquent\Model;

class LibraryMember extends Model
{
    protected $table      = 'library_members';
    protected $primaryKey = 'library_member_id';
}
