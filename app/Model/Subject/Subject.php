<?php

namespace App\Model\Subject;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table      = 'subjects';
    protected $primaryKey = 'subject_id';
    protected $fillable = ['co_scholastic_type_id'];
    public function scholasticSubjects()
    {
        return $this->belongsTo('App\Model\CoScholastic\CoScholastic', 'co_scholastic_type_id');
    }

}
