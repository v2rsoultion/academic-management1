<?php

namespace App\Model\InventoryItem;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
	protected $table      = 'inv_items';
    protected $primaryKey = 'item_id';

    public function category() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'category_id');
    }

    public function getSubCategory() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'sub_category_id');
    }

    public function unit() {
        return $this->belongsTo('App\Model\InventoryUnit\Unit', 'unit_id');
    }

    public function itemsdetails() {
         return $this->hasOne('App\Model\Purchase\PurchaseItems','item_id');
    }
}
