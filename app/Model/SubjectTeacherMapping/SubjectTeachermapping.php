<?php

namespace App\Model\SubjectTeacherMapping;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacherMapping extends Model
{
    protected $table      = 'class_subject_staff_map';
    protected $primaryKey = 'class_subject_staff_map_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_ids');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }
}
