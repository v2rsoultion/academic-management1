<?php

namespace App\Model\InventoryCategory;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table      = 'inv_category';
    protected $primaryKey = 'category_id';

    public function categorydetails() {
         return $this->hasOne('App\Model\InventoryItem\Items','category_id');
    }
    public function purchasecategorydetails() {
         return $this->hasOne('App\Model\Purchase\Purchase','category_id');
    }
    
}
