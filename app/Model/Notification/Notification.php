<?php

namespace App\Model\Notification;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table      = 'notifications';
    protected $primaryKey = 'notification_id';
}
