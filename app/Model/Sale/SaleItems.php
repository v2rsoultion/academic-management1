<?php

namespace App\Model\Sale;

use Illuminate\Database\Eloquent\Model;

class SaleItems extends Model
{
    protected $table      = 'sale_items';
    protected $primaryKey = 'sale_items_id';

    public function getSaleItems()
    {
        return $this->belongsTo('App\Model\Sale\Sale','sale_id');
    }
    public function items()
    {
        return $this->belongsTo('App\Model\InventoryItem\Items', 'item_id');
    }

    public function units()
    {
        return $this->belongsTo('App\Model\InventoryUnit\Unit', 'unit_id');
    }

    public function category() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'category_id');
    }

    public function subCategory() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'sub_category_id');
    }

}
