<?php

    return [
        "authorization_error_msg"       => "You don\"t have authorization !",
        "email_sender"                  => "Academic Management Team",
        "project_title"                 => "Academic Management",
        "reset_password"                => "Reset Password",
        "dashboard"                     => "Dashboard",
        "profile"                       => "Profile",
        "medium_type"                   => "Medium Type", 

        "menu_configuration"            => "Configuration",
        "menu_academic"                 => "Academic",
        "menu_admission"                => "Admission",
        "menu_student"                  => "Student",
        "menu_recruitment"              => "Recruitment",
        "menu_examination"              => "Examination",
        "menu_fee_collection"           => "Fee Collection",
        "menu_staff"                    => "Staff",
        "menu_hostel"                   => "Hostel",
        "menu_library"                  => "Library",
        "menu_transport"                => "Transport",
        "menu_visitor"                  => "Visitors",
        "menu_notice_board"             => "Notice Board",
        "menu_online_content"           => "Online Content",
        "menu_task_manager"             => "Task Manager",
        "menu_leave_management"         => "Leave Management",
        "menu_my_class"                 => "My Class",
        "menu_my_subject"               => "My Subjects",
        "menu_certificates"             => "Certificates",
        "menu_inventory"                => "Inventory",

        "add_school"                    => "Add School", // School segment start here
        "school_detail"                 => "School Detail",
        "school_name"                   => "School Name",
        "school_registration_no"        => "School Reg/Affiliation No",
        "school_sno_numbers"            => "Sr No(Multiple)",
        "school_description"            => "Description",
        "school_address"                => "Address",
        "school_district"               => "District",
        "school_city"                   => "City",
        "school_state"                  => "State",
        "school_pincode"                => "Pincode",
        "school_board_of_exams"         => "Board of Education",
        "school_medium"                 => "Medium (Lang)",
        "school_class_from"             => "Class From",
        "school_class_to"               => "Class To",
        "school_total_students"         => "No of Students",
        "school_total_staff"            => "No of Staff",
        "school_fee_range_from"         => "Fees Range(From)",
        "school_fee_range_to"           => "Fees Range(To)",
        "school_facilities"             => "Facilities",
        "school_url"                    => "Branch URL",
        "school_logo"                   => "Branch Logo",
        "school_email"                  => "Email",
        "school_mobile_number"          => "Mobile",
        "school_telephone"              => "Telephone",
        "school_fax_number"             => "Fax",
        "school_image"                  => "Image",
        "imprest_ac_status"             => "Imprest Account",
        "imprest_ac_confi_amt"          => "Imprest Account Amount",
        "imprest_ac_setting"            => "Imprest Account Setting",

        "session"                       => "Academic Year", // For Academic Year
        "add_session"                   => "Add Academic Year",
        "view_session"                  => "View Academic Years",
        "edit_session"                  => "Edit Academic Year",
        "session_name"                  => "Academic Year Name",
        "session_start_date"            => "Start Date",
        "session_end_date"              => "End Date",

        "holidays"                      => "Holidays", // For Holiday
        "add_holiday"                   => "Add Holiday",
        "view_holiday"                  => "View Holidays",
        "edit_holiday"                  => "Edit Holiday",
        "holiday_name"                  => "Holiday Name",
        "holiday_start_date"            => "Start Date",
        "holiday_end_date"              => "End Date",
        "holiday_date_range"            => "Date Range",
        "holiday_for"                   => "Holiday For",

        "shifts"                        => "Shifts", // For Shifts
        "shift"                         => "Shifts",
        "add_shift"                     => "Add Shift",
        "view_shift"                    => "View Shifts",
        "edit_shift"                    => "Edit Shift",
        "shift_name"                    => "Shift Name",
        "shift_start_time"              => "Start Time",
        "shift_end_time"                => "End Time",


        "class"                         => "Class", // For Classes
        "add_class"                     => "Add Class",
        "view_class"                    => "View Classes",
        "edit_class"                    => "Edit Class",
        "class_name"                    => "Class Name",
        "class_board"                   => "Board",
        "class_stream"                  => "Stream",
        "class_order"                   => "Class Order",
        "class_students"                => "No of students",
        
        "section"                       => "Section", // For Section
        "add_section"                   => "Add Section",
        "view_section"                  => "View Sections",
        "edit_section"                  => "Edit Section",
        "section_class_id"              => "Classes",
        "section_name"                  => "Section",
        "section_class"                 => "Class",
        "section_shift"                 => "Shift",
        "section_stream"                => "Stream",
        "section_order"                 => "Order",
        "section_intake"                => "Intake/Capacity",
        
        "co_scholastic"                     => "Co-Scholastic", // For Type of Co-Scholastic
        "add_co_scholastic"                 => "Add Co-Scholastic",
        "view_co_scholastic"                => "View Co-Scholastic",
        "edit_co_scholastic"                => "Edit Co-Scholastic",
        "co_scholastic_type_name"           => "Co Scholastic",
        "co_scholastic_type_description"    => "Description",
   

        "facilities"                    => "Facilities", // For Facilities by Pratyush on 19 July 2018
        "add_facility"                  => "Add Facility",
        "view_facility"                 => "View Facilities",
        "edit_facility"                 => "Edit Facility",
        "facility_name"                 => "Facility Name",
        "is_fees_applied"               => "Is Fees Applied",
        "fees_applied_yes"              => "Yes",
        "fees_applied_no"               => "No",
        "facility_fees_amount"          => "Fees Amount",

        "document_category"             => "Category", // For Document categories by Pratyush on 19 July 2018
        "add_document_category"         => "Add Document Category",
        "view_document_category"        => "View Document Categories",
        "edit_document_category"        => "Edit Document Category",
        "name_of_doc"                   => "Name",
        "document_category_for"         => "Documents For",
        "document_category_student"     => "Student",
        "document_category_staff"       => "Staff",
        "document_category_both"        => "Both",

        "designation"                   => "Designation", // For Document categories by Pratyush on 19 July 2018
        "add_designation"               => "Add Designation",
        "view_designation"              => "View Designation",
        "edit_designation"              => "Edit Designation",
        "designation_name"              => "Designation Name",
        "teaching_status"              => "Teaching Status",

        "title"                         => "Title", // For Title by Pratyush on 20 July 2018
        "add_title"                     => "Add Title",
        "view_title"                    => "View Titles",
        "edit_title"                    => "Edit Title",
        "title_name"                    => "Title",

        "school_board"                  => "School Board", // For School Board by shree on 29 sept 2018
        "add_school_board"              => "Add School Board",
        "view_school_board"             => "View School Board",
        "edit_school_board"             => "Edit School Board",
        "school_board_name"             => "Board Name",

        "caste"                         => "Caste", // For Caste by Pratyush on 20 July 2018
        "add_caste"                     => "Add Caste",
        "view_caste"                    => "View Caste",
        "edit_caste"                    => "Edit Caste",
        "caste_name"                    => "Caste Name",

        "religion"                      => "Religion", // For Religion by Pratyush on 20 July 2018
        "add_religion"                  => "Add Religion",
        "view_religion"                 => "View Religions",
        "edit_religion"                 => "Edit Religion",
        "religion_name"                 => "Religion Name",

        "nationality"                   => "Nationality", // For Nationality by Pratyush on 20 July 2018
        "add_nationality"               => "Add Nationality",
        "view_nationality"              => "View Nationality",
        "edit_nationality"              => "Edit Nationality",
        "nationality_name"              => "Nationality",

        "school_group"                  => "School Group", // For School House by Pratyush on 20 July 2018
        "add_school_group"              => "Add School Group",
        "view_school_group"             => "View School Groups",
        "edit_school_group"             => "Edit School Group",
        "school_group_name"             => "School Group Name",

        "virtual_class"                 => "Virtual Class", // For Virtual Class by Pratyush on 21 July 2018
        "add_virtual_class"             => "Add Virtual Class",
        "manage_virtual_classes"        => "View Virtual Classes",
        "edit_virtual_class"            => "Edit Virtual Class",
        "virtual_class_name"            => "Name",
        "virtual_class_description"     => "Description",
        "virtual_class_sr_no"           => "Sr. No.",
        "virtual_class_stu_list"        => "View Student List", // For Student list (Virtual class add student)
        "virtual_class_stu_name"        => "Name",
        "virtual_class_stu_roll_no"     => "Roll No",
        "virtual_class_stu_profile"     => "Student Profile",
        "virtual_class_stu_enroll_no"   => "Enroll No",
        "virtual_class_stu_f_name"      => "Father Name",
        "virtual_class_stu_class"       => "Class",
        "virtual_class_stu_session"     => "Session",
        "virtual_class_no_of_student"   => "No Of Students",

        "competitions"                  => "Competition", // For Competitions by Pratyush on 21 July 2018
        "add_competitions"              => "Add Competition",
        "manage_competitions"           => "View Competitions",
        "edit_competitions"             => "Edit Competition",
        "competitions_name"             => "Name",
        "competitions_description"      => "Description",
        "competitions_date"             => "Date",
        "competitions_level"            => "Level",
        "competitions_level_school"     => "school",
        "competitions_level_class"      => "Class",
        "Issue_certificate"             => "Issue Participation Certificate",
        "Issue_certificate_yes"         => "Yes",
        "Issue_certificate_no"          => "No",
        "map_students"                  => "Map Students",
        "competitions_no_winner"        => "No Of Winner",
        "issue_certificates"            => "Issue Certificates",
        "competitions_level_classes"    => "Classes", //for add page dropdown menu

        "select_winner"                 => "Select Winner", //for Select Winner page (Competition)
        "comp_list_of_winner"           => "List of winners",
        "comp_list_of_winner_rank"      => "Rank",
        "comp_list_of_winner_name"      => "Name",
        "comp_list_of_winner_class"     => "Class",
        "comp_list_of_winner_section"   => "Section",

        "subject"                        => "Subject", // For Type of Co-Scholastic
        "add_subject"                    => "Add Subject",
        "view_subject"                   => "View Subjects",
        "edit_subject"                   => "Edit Subject",
        "subject_name"                   => "Subject Name",
        "subject_code"                   => "Subject Code",
        "subject_class_ids"              => "Classes",
        "co_scholastic_type_id"          => "Type of Co scholastic",
        "subject_is_coscholastic"        => "Co-Scholastic",

        "student"                        => "Student", // Student
        "students"                       => "Students",
        "add_student"                    => "Add Student",
        "view_student_list"              => "View List",
        "view_students"                  => "View students",
        "student_profile"                => "Student Profile",
        "edit_student"                   => "Edit Student",
        "student_enroll_number"          => "Enroll No",
        "student_roll_no"                => "Roll No",
        "student_email"                  => "Email Address",
        "student_reg_date"               => "Date of Reg",
        "student_name"                   => "Name",
        "student_type"                   => "Student Type",
        "student_class_section"          => "Class Section",
        "student_image"                  => "Photo of student",
        "student_gender"                 => "Gender",
        "student_dob"                    => "DOB",
        "student_have"                   => "Student Have",
        "student_adhar_card_number"      => "Aadhaar Card Number",
        "student_category"               => "Category",
        "caste_id"                       => "Caste",
        "religion_id"                    => "Religion",
        "nationality_id"                 => "Nationality",
        "title_id"                       => "Title",
        "student_sibling_name"           => "Sibling Name",
        "student_sibling_class_id"       => "Sibling Class",
        "student_privious_school"        => "School Name",
        "student_privious_class"         => "Class",
        "student_privious_tc_no"         => "TC No.",
        "student_privious_tc_date"       => "TC Date",
        "student_privious_result"        => "Result",
        "student_status"                 => "Status",

        "student_temporary_address"      => "Address", // Student temporary address information
        "student_temporary_city"         => "City",
        "student_temporary_state"        => "State",
        "student_temporary_county"       => "Country",
        "student_temporary_pincode"      => "Pincode",

        "student_permanent_address"      => "Address", // Student permanent address information
        "student_permanent_city"         => "City",
        "student_permanent_state"        => "State",
        "student_permanent_county"       => "Country",
        "student_permanent_pincode"      => "Pincode",

        "student_login_name"             => "Name",
        "student_login_email"            => "Email",
        "student_login_contact_no"       => "Contact No",
        "student_father_name"            => "Father Name",  // Student parent information
        "student_father_mobile_number"   => "Father’s Mobile number",
        "student_father_email"           => "Father’s Email Address",
        "student_father_occupation"      => "Father’s Occupation",
        "student_father_annual_salary"   => "Father’s Annual Salary",
        "student_mother_name"            => "Mother Name",
        "student_mother_mobile_number"   => "Mother’s Mobile number",
        "student_mother_email"           => "Mother’s Email Address",
        "student_mother_occupation"      => "Mother’s Occupation",
        "student_mother_annual_salary"   => "Mother’s Annual Salary",
        "student_mother_tongue"          => "Mother Tongue",
        "student_guardian_name"          => "Guardian Name",
        "student_guardian_email"         => "Email Address",
        "student_guardian_mobile_number" => "Mobile Number",
        "student_guardian_relation"      => "Relation with guardian",

        "admission_session_id"           => "Admit Session",  // Student academic information
        "admission_class_id"             => "Admit Class",
        "admission_section_id"           => "Admit Section",
        "current_session_id"             => "Current Session",
        "current_class_id"               => "Current Class",
        "current_section_id"             => "Current Section",
        "student_unique_id"              => "Attendance Unique ID",
        "group_id"                       => "House/Group",

        "ac_session_id"                  => "Session",  // Student academic history information
        "ac_class_id"                    => "Class",
        "ac_section_id"                  => "Section",
        "ac_percentage"                  => "Percentage",
        "ac_rank"                        => "Rank",
        "ac_activity_winner"             => "Any CC activity winner",

        "student_height"                  => "Height",  // Student health information
        "student_weight"                  => "Weight",
        "student_blood_group"             => "Blood Group",
        "student_vision_left"             => "Vision Right",
        "student_vision_right"            => "Vision Left",
        "medical_issues"                  => "Medical Issue(You can add multiple issue by press Enter)",
        "medical_issues_p"                => "Medical Issue",
        "total_students"                  => "No of students",
        "search_by_roll_no"               => "Roll No",
        "search_by_enroll_no"             => "Enroll No",
        "search_by_student_name"          => "Student Name",
        "search_by_father_name"           => "Father Name",
        "change_parent"           => "Change Parent",

        "student_document_category"       => "Document category", // Student document information
        "student_document_file"           => "Document File",
        "student_document_details"        => "Document Details",
        "student_edit_document"           => "Edit Document", // @Pratyush 10 Aug 2018

        "room_no"                         => "Room No.", // For Room No by Pratyush on 28 July 2018
        "add_room_no"                     => "Add Room No.",
        "view_room_no"                    => "View Room No.",
        "edit_room_no"                    => "Edit Room No.",
        "room_no_add_page"                => "Room No",

        "s_no"                            => "S. No.",
        "parent_information"              => "Parent Information",
        "student_contact"                 => "Contact No",
        "view_parent_detail"              => "View Parent Detail",

        "staff_role"                      => "Staff Roles", // For Holiday
        "add_staff_role"                  => "Add Staff Role",
        "view_staff_role"                 => "View Staff Roles",
        "edit_staff_role"                 => "Edit Staff Role",
        "staff_role_name"                 => "Staff Role Name",
        "manage_staff_role"               => "Manage Staff Roles",

        "staff"                           => "Staff", // For Staff
        "add_staff"                       => "Add Staff",
        "view_staff"                      => "View Staff",
        "staff_profile"                      => "Staff Profile",
        "edit_staff"                      => "Edit Staff",
        "staff_name"                      => "Name",
        "staff_aadhar"                    => "Aadhar Number",
        "staff_UAN"                       => "Universal Account Number",
        "staff_ESIN"                      => "Employee State Ins. Number",
        "staff_PAN"                       => "Permanent Account Number",
        "staff_designation_id"            => "Designation",
        "staff_designation_name"          => "Designation",
        'staff_blood_group'               => "Blood Group",
        "staff_role_id"                   => "Roles",
        "staff_email"                     => "Email",
        "staff_mobile_number"             => "Mobile Number",
        "staff_dob"                       => "DOB",
        "staff_gender"                    => "Gender",
        "staff_profile_img"               => "Profile Image",

        "staff_specialization"            => "Specialization",
        "staff_qualification"             => "Qualification",
        "staff_reference"                 => "Reference",
        "staff_experience"                => "Experience ",
        "staff_attendance_unique_id"     => "Attendance Unique ID",

        "staff_father_name_husband_name"  => "Father Name/Husband Name",
        "staff_father_husband_mobile_no"  => "Contact Number",
        "staff_mother_name"               => "Mother Name",
        "staff_marital_status"            => "Marital Status",

        "staff_document_category"       => "Document category", // Staff document information
        "staff_document_file"           => "Document File",
        "staff_document_details"        => "Document Details",

        "staff_temporary_address"         => "Address", // staff temporary address information
        "staff_temporary_city"            => "City",
        "staff_temporary_state"           => "State",
        "staff_temporary_county"          => "Country",
        "staff_temporary_pincode"         => "Pincode",

        "staff_permanent_address"         => "Address", // staff permanent address information
        "staff_permanent_city"            => "City",
        "staff_permanent_state"           => "State",
        "staff_permanent_county"          => "Country",
        "staff_permanent_pincode"         => "Pincode",

        "notes"                           => "Notes", // For Notes by Ashish on 31 July 2018
        "add_notes"                       => "Add Notes",
        "view_notes"                      => "View Notes",
        "edit_notes"                      => "Edit Notes",
        "online_content"                  => "Online Content",
        "t_name"                          => "Name",
        "class_section"                   => "Class - Section",
        "notes_class"                     => "Class",
        "notes_section"                   => "Section",
        "notes_subject"                   => "Subject",
        "notes_unit"                      => "Unit",
        "notes_topic"                     => "Topic",
        "teacher_name"                    => "Teacher Name",
        "upload_date"                     => "Upload Date",
        "file_url"                        => "Note File",

        
        "class_teacher_allocation"        => "Class-Teacher Allocation", // For Class Teacher Allocation by Pratyush on 06 Aug 2018
        "allot_teacher"                   => "Allocate Class",
        "view_allocation"                 => "View Allocations",
        "edit_allocation"                 => "Edit Allocation",
        "allot_class_name"                => "Class Name",
        "allot_section_name"              => "Section Name",
        "allot_teacher_name"              => "Teacher Name",

        "map_subject_to_class"            => "Map Subject-Class", // For Map Subject to class by Pratyush on 07 Aug 2018
        "map_subject"                     => "Map Subject",
        "view_map_subject"                => "View Subject Class Mapping",
        "ms_class_name"                   => "Class Name",
        "ms_section_name"                 => "Sections",
        "ms_no_subject_applied"           => "No Of Subject Applied",
        "ms_subject_code"                 => "Subject Code",
        "ms_subject_name"                 => "Subject Name",
        "ms_subject"                      => "Subject",

        "map_subject_to_section"          => "Map Subject-Section", // For Map Subject to class by Pratyush on 07 Aug 2018
        "map_subject"                     => "Map Subject",
        "view_map_subject_section"        => "View Subject Section Mapping",
        "ms_class_name"                   => "Class Name",
        "ms_section_name"                 => "Sections",
        "ms_no_subject_applied"           => "No Of Subject Applied",
        "ms_subject_code"                 => "Subject Code",
        "ms_subject_name"                 => "Subject Name",
        "ms_subject"                      => "Subject",

        "map_subject_to_teacher"          => "Map Subject To Teacher", // For Map Subject to teacher by Pratyush on 07 Aug 2018
        "map_subject_teacher"             => "Map Subject",
        "view_map_subject_teacher"        => "View Subject Teacher Mapping",
        "ms_class_section"                => "Class Name - Section",
        "mst_no_subjects"                 => "No Of Subjects",
        "mst_no_of_map_subject"           => "No of Mapped Subject", 
        "mst_remaining_subjects"          => "Remaining Subject",
        "ms_subject_code"                 => "Subject Code - Subject Name",
        "ms_teacher_code"                 => "Teacher Id/Code",
        "ms_teacher_name"                 => "Teacher Name",
        "mst_view_report"                 => "View Report",
        "mst_subject_code_name"           => "Subject Code - Subject Name",

        "leave_application"       => "Leave Application", // For Leave Application 
        "add_leave_application"   => "Add Leave Application",
        "view_leave_application"  => "View Leave Application",
        "edit_leave_application"  => "Edit Leave Application",
        "manage_leave_application"  => "Manage Leave Application",
        "leave_app_reason"           => "Reason",
        "leave_app_date_to"          => "Date To",
        "leave_app_date_from"        => "Date From",
        "leave_app_attachment"       => "Attachment Link",
        "leave_app_status"           => "Status",
        "leave_date_range"           => "Date Range",
        "leave_days"                 => "Days",

        "stream"                          => "Stream", // For Stream by Pratyush on 11 Aug 2018
        "add_stream"                      => "Add Stream",
        "view_stream"                     => "View Streams",
        "edit_stream"                     => "Edit Stream",
        "stream_name"                     => "Stream",

        "examination"                    => "Examination",
        "terms"                          => "Manage Term", // For terms by Pratyush on 11 Aug 2018
        "add_terms"                      => "Manage Term",
        // "view_terms"                     => "View Terms",
        "edit_terms"                     => "Edit Term",
        "terms_name"                     => "Term Name",
        "terms_caption"                  => "Caption",

        "exam"                           => "Manage Exam", // For Exam by Pratyush on 11 Aug 2018
        "add_exam"                       => "Manage Exam",
        "view_exam"                      => "View Exam",
        "edit_exam"                      => "Edit Exam",
        "exam_name"                      => "Exam",
        "term_select"                    => "Select Term",
        "exam_term"                      => "Term",

        "book_category"                  => "Book Category", // For Book Category by Pratyush on 13 Aug 2018
        "add_book_category"              => "Add Book Category",
        "view_book_category"             => "View Book Categories",
        "edit_book_category"             => "Edit Book Category",
        "book_category_name"             => "Name",

        "book_allowance"                 => "Book Allowance", // For Book Allowance by Pratyush on 13 Aug 2018
        "edit_book_allowance"            => "Edit Book Allowance",
        "edit_book_staff"                => "For staff",
        "edit_book_student"              => "For student",

        "book_vendor"                    => "Book Vendor", // For Book Allowance by Pratyush on 13 Aug 2018
        "add_book_vendor"                => "Add Book Vendor",
        "view_book_vendor"               => "View Book Vendors",
        "edit_book_vendor"               => "Edit Book Vendor",
        "book_vendor_name"               => "Name",
        "book_vendor_no"                 => "Contact No",
        "book_vendor_email"              => "Email Address",
        "book_vendor_address"            => "Address",

        "cupboard"                       => "CupBoard", // For CupBoard by Pratyush on 13 Aug 2018
        "add_cupboard"                   => "Add CupBoard",
        "view_cupboard"                  => "View CupBoard",
        "edit_cupboard"                  => "Edit CupBoard",
        "cupboard_name"                  => "CupBoard Name",

        "cupboard_shelf"                 => "CupBoard Shelf", // For CupBoard Shelf by Pratyush on 13 Aug 2018
        "add_cupboard_shelf"             => "Add CupBoard Shelf",
        "view_cupboard_shelf"            => "View CupBoard Shelf",
        "edit_cupboard_shelf"            => "Edit CupBoard Shelf",
        "cupboard_shelf_name"            => "CupBoard Shelf Name",
        "cupboard_shelf_cup"             => "CupBoard",
        "cupboard_shelf_capacity"        => "Capacity",
        "cupboard_shelf_detail"          => "Details",

        "bbook"                          => "Book", // For Book by Pratyush on 14 Aug 2018
        "add_bbook"                      => "Add Book",
        "view_bbook"                     => "View Books",
        "edit_bbook"                     => "Edit Book",
        "bbook_category"                 => "Book Category",
        "bbook_name"                     => "Book Name",
        "bbook_type"                     => "Book Type",
        "bbook_subtitle"                 => "Subtitle",
        "bbook_isbn_no"                  => "ISBN No",
        "bbook_author"                   => "Author",
        "bbook_publisher"                => "Publisher",
        "bbook_edition"                  => "Edition",
        "bbook_vendor"                   => "Book Vendor",
        "bbook_price"                    => "Price",
        "bbook_copy"                     => "Copy",
        "bbook_unique_no"                => "Book Unique No",
        "bbook_exclusive_staff"          => "Exclusive for staff",
        "bbook_remark"                   => "Remark",
        "bbook_cupboard"                 => "CupBoard",
        "bbook_cupboard_shelf"           => "CupBoard Shelf",
        "bbook_total_copies"             => "Total Copies",
        "bbook_issue_copies"             => "Copies Issued",
        "bbook_cupboard_plus_shelf"      => "cupboard + shelf",
        "bbook_issue_available"          => "Issued/Available",
        "bbook_info"                     => "Copies",
        "exclusive_for_staff"            => "Exclusive For Staff",


        "library_member"                 => "Members",
        "member_student"                 => "Register Student",
        "member_student_view"            => "View Students",
        'member_staff'                   => "Register Staff",
        'member_staff_view'              => "View Staff",
        'member_staff_emp_id'            => "Emp ID",
        'member_staff_photo'             => "Photo",
        'member_staff_name'              => "Name",
        'member_staff_designation'       => "Designation",
        'member_student_roll_no'         => "Roll No",
        'member_student_name'            => "Name",
        'member_student_class_section'   => "Class",
        'member_no_of_book_issued'       => "No of Issued Book",
        'member_issued_books'            => "Issued Books",
        'member_issued_from'             => "Issue From",
        'member_issued_to'               => "Issue To",
        'member_issued_book'             => "Book",

        'book_issued'                    => "Manage Book",
        'book_issued_name'               => "Name",
        'book_issued_father_name'        => "Father Name",
        'book_issued_profile'            => "Profile",
        'book_issued_book_left'          => "No of Book Left",
        'book_issued_available_copies'   => "Available Copies",
        'book_issued_from'               => 'From',   
        'book_issued_to'                 => 'To',
        'book_issued_start'              => 'Start Date',   
        'book_issued_end'                => 'End Date',
        'book_issued_unique_no'          => 'Book Unique No',
        'book_issued_book_name'          => 'Book Name',
        'book_issued_duration'           => 'Duration',
        'book_issued_member_name'        => 'Member Name',
        'book_issued_isbn_no'            => 'ISBN No',


        "question_paper"                  => "Question Paper", // For Question Paper by Ashish on 21 July 2018
        "add_question_paper"              => "Add Question Paper",
        "view_question_paper"             => "View Question Paper",
        "edit_question_paper"             => "Edit Question Paper",
        "online_content"                  => "Online Content",
        "question_paper_name"             => "Name",
        "question_paper_file_upload"      => "File Upload",
        "question_paper_exam_type_id"     => "Exam Type",
        "question_paper_class_section"    => "Class-section",
        "question_paper_class_id"         => "Class",
        "question_paper_section_id"       => "Section",
        "question_paper_subject_id"       => "Subject",

        'return_renew_book'             => 'Return Book',

        "library_fine"                  => "Library Fine",
        "library_fine_member_type"      => "Member Type",
        "library_fine_member"           => "Member",
        "library_fine_amount"           => "Amount",
        "library_fine_status"           => "Status",
        "library_fine_due"              => "Due",
        "library_fine_paid"             => "Paid",
        "library_fine_remark"           => "Remark",
        "library_fine_date"             => "Date",
        "library_fine_name"             => "Name",

 
        "time_table"                    => "Time Table",  //For time table by Khushbu on 06 Sept 2018
        "add_time_table"                => "Add Time Table",
        "edit_time_table"               => "Edit Time Table",
        "view_time_table"               => "View Time Table",   
        "time_table_class_name"         => "Name",   
        "time_table_section"            => "Section",    
        "time_table_week_day"           => "Week Days", 
        "lecture_no"                    => "Lecture No",
        "lunch_break"                   => "Lunch Break",
        "set_time_table"                => "Set Time Table",
        "academic"                      => "Academic",
        "monday"                        => "Monday",
        "tuesday"                       => "Tuesday",
        "wednesday"                     => "Wednesday",
        "thursday"                      => "Thursday",
        "friday"                        => "Friday",
        "saturday"                      => "Saturday",
        "start_time"                    => "Start Time",
        "end_time"                      => "End Time",

        "country"                       => "Country",  //For Country by Khushbu on 08 Sept 2018
        "add_country"                   => "Add Country",
        "view_country"                  => "View Country",
        "edit_country"                  => "Edit Country",
        "country_name"                  => "Country Name",    

        "state"                         => "State",   //For State by Khushbu on 08 Sept 2018
        "add_state"                     => "Add State",
        "view_state"                    => "View State",
        "edit_state"                    => "Edit State",
        "state_name"                    => "State Name",    

        "city"                          => "City",   //For City by Khushbu on 08 Sept 2018
        "add_city"                      => "Add City",
        "view_city"                     => "View City",
        "edit_city"                     => "Edit City",
        "city_name"                     => "City Name",


        "brochure"                      => "Brochure", // For Brochure module
        "add_brochure"                  => "Add Brochure",
        "view_brochure"                 => "View Brochures",
        "edit_brochure"                 => "Edit Brochure",
        "brochure_name"                 => "Brochure Name", 
        "brochure_upload_date"          => "Upload Date",
        "brochure_file"                 => "Brochure File",

        "admission_form"                => "Admission Form", // For Admission module
        "create_form"                   => "Create Form",
        "view_forms"                    => "View Forms",
        "add_admission_form"            => "Add Admission Form",
        "view_admission_form"           => "View Admission Forms",
        "edit_admission_form"           => "Edit Admission Form",
        "manage_form"                   => "Manage Form",
        "form_name"                     => "Form Name", 
        "no_of_intake"                  => "No of Intake",
        "form_prefix"                   => "Form Prefix",
        "form_instruction"              => "Instruction",
        "form_information"              => "Information",
        "form_type"                     => "Form Type",
        "form_last_date"                => "Application Last Date",
        "form_fee_amount"               => "Fees Amount",
        "form_pay_mode"                 => "Payment Mode",
        "form_email_receipt"            => "Email Receipt", 
        "form_success_message"          => "Success Message",
        "form_failed_message"           => "Failure Message",
        "form_message_via"              => "Message Via",

        "enquiry_form"                => "Enquiry Form", // For Enquiry module
        "manage_enquiries"            => "Manage Enquiries",
        "manage_admissions"           => "Manage Admissions",
        "fees_status"                 => "Fees Status",
        "mode"                        => "Mode",
        "form_number"                 => "Form No.",
        "student_name"                => "Student Name",
        "class_session"               => "Class-Session",
        "father_name"                 => "Father Name",
        "mobile"                      => "Mobile",

        
        "selected_students"          => "Selected Students",
        "no_of_intake"               => "No of Intake",
        "no_of_intake_remaining"     => "No of Intake Remaining",
        "enquiry_number"             => "Enquiry No",

        "one_time"                 => "One Time Head", // For One time Head
        "add_one_time"             => "Add One Time Head",
        "view_one_time"            => "View One Time Head",
        "edit_one_time"            => "Edit One Time Head",
        "particular_name"          => "Particular Name",
        "alias"                    => "Alias",
        "student_for"              => "For Students",
        "deduct_imprest_status"    => "Deduct from Imprest A/C",
        "receipt_status"           => "On Receipt",
        "fee_classes"              => "Classes",
        "fee_amount"               => "Amount",
        "fee_date"                 => "Effective Date",
        "fee_refundable"           => "Refundable",

        "recurring_h"               => "Recurring Head", // For Recurring Head
        "add_recurring_h"           => "Add Recurring Head",
        "view_recurring_h"          => "View Recurring Head",
        "edit_recurring_h"          => "Edit Recurring Head",
        "recurring_fee"             => "Total Fees",
        "no_of_inst"                => "Total Installments",
        "inst_name"                 => "Installment Name",

        "bank"                      => "Bank", // For Bank Module
        "add_bank"                  => "Add Bank",
        "view_bank"                 => "View Banks",
        "edit_bank"                 => "Edit Bank",
        "bank_name"                 => "Bank Name", 

        "rte_cheque"                => "RTE Cheque", // For RTE Cheque Module
        "add_rte_cheque"            => "Add RTE Cheque",
        "view_rte_cheque"           => "View RTE Cheques",
        "edit_rte_cheque"           => "Edit RTE Cheque",
        "rte_cheque_no"             => "Cheque Number", 
        "rte_cheque_date"           => "Cheque Date", 
        "rte_cheque_amt"            => "Cheque Amount", 
        "apply_rte_fees"            => "Apply fees on head",

        "cheque_details"            => "Cheque Details", 
        "manage_rte_h"              => "RTE Fees Head",  // For RTE Head
        "view_rte_h"                => "View RTE Head", 
        "rte_h_name"                => "RTE Head Name", 
        "rte_h_amt"                 => "Amount", 

        "visitor"                       => "Visitor", // For Visitor
        "add_visitor"                   => "Add Visitor",
        "view_visitor"                  => "View Visitor",
        "edit_visitor"                  => "Edit Visitor",
        "name"                          => "Name",
        "purpose"                       => "Purpose",
        "contact"                       => "Contact",
        "email"                         => "Email",
        "visit_date"                    => "Visit Date",
        "check_in_time"                 => "Check In Time",
        "check_out_time"                => "Check Out Time",

        "free_by_rte"                   => "Free by RTE",
        "rte_status"                    => "Status",

        "grade_scheme"                  => "Grade Scheme", // For Grade scheme
        "add_grade_scheme"              => "Add Grade Scheme",
        "view_grade_scheme"             => "View Grade Scheme",
        "edit_grade_scheme"             => "Edit Grade Scheme",
        "scheme_name"                   => "Scheme Name", 
        "grades"                        => "Grades", 
        "grade_type"                    => "Grade Type",
        "grade_min_max"                     => "Min - Max",  
        "grade_min"                     => "Min", 
        "grade_max"                     => "Max", 
        "grade_name"                    => "Grade Name", 
        "grade_point"                   => "Grade Point", 
        "grade_remark"                  => "Remark", 

        "fine"                          => "Fine", // For Fine
        "add_fine"                      => "Add Fine",
        "view_fine"                     => "View Fine",
        "edit_fine"                     => "Edit Fine",
        "fine_name"                     => "Fine Name",
        "fine_for"                      => "Fine For",
        "fees_head"                     => "Fees Head",
        "fine_detail_days"              => "Fine Days",
        "fine_detail_amt"               => "Fine Amount",
        "details"                       => "Details",

        "add_marks_criteria"            => "Manage Marks Criteria",// For marks criteria
        "edit_marks_criteria"           => "Edit Marks Criteria",
        "criteria_name"                 => "Criteria Name",
        "max_marks"                     => "Maximum Marks",
        "passing_marks"                 => "Passing Marks",

        "manage_concession"             => "Manage Concession",// For manage concession
        "fees_name"                     => "Fees Name",
        "fees_amt"                      => "Fees Amount",
        "concession_amt"                => "Concession Amount",
        "paid_amt"                      => "Paid Amount",

        "wallet_ac"                     => "Wallet Account", // For Manage Accounts
        "wallet_amt"                    => "Wallet Amount",
        "imprest_ac"                    => "Imprest Account",
        "imprest_amt"                   => "Imprest Amount",

        "map_heads"                     => "Map Heads", // For manage heads
        "map_student"                   => "Map Student",

        "fees_counter"                  => "Fees Counter", // For Fees Counter
        
        "reason"                        => "Reasons", // For Stream by Pratyush on 11 Aug 2018
        "add_reason"                    => "Add Reason",
        "view_reason"                   => "View Reasons",
        "edit_reason"                   => "Edit Reason",
        "reason_text"                   => "Reason",

        "map_exam_class_subject"        => "Map exam to class subjects",
        "map_classes"                   => "Map Classes",
        "add_classes"                   => "Add Classes",
        "exam_class_subject_report"     => "Exam to class subject report",
        "manage_grade"                  => "Manage Grade",
        "manage_criteria"               => "Map marks criteria to subjects",
        "subjects"                      => "Subjects",
        "manage_grade_scheme"           => "Manage Grade Scheme",
        "marks_criteria"                => "Marks criteria",
        "manage_student_subject"        => "Manage Student's Subjects",
        "exclude_subject"               => "Exclude Subjects",
        "gradescheme_map_report"        => "Grade Scheme Map Report",
        "markscriteria_map_report"      => "Marks Criteria Map Report",
        "grade_scheme_name"             => "Grade Scheme Name",
        "grade_scheme_type"             => "Grade Scheme Type",
        
        "view_student_leaves"           => "View student leaves",

        "mediums"           => "Mediums", // For Medium types
        "add_medium"        => "Add Medium",
        "view_mediums"      => "View Mediums",
        "edit_medium"       => "Edit Medium",
        "medium_type"       => "Medium Type",

        "groups"                => "Groups", // For Student groups
        "add_group"             => "Add Group",
        "view_student_groups"   => "View Student Groups",
        "view_parent_groups"    => "View Parent Groups",
        "edit_group"            => "Edit Group",
        "group_name"            => "Group Name",
        "flag"                  => "Group Type",
        "description"           => "Description",

        "remarks"               => "Dairy Remarks", // For Daily remarks
        "remark_date"           => "Remark Date",
        "add_remark"            => "Add Remark",
        "edit_remark"           => "Edit Remark",
        "view_remarks"          => "View Remarks",
        "remark_text"           => "Remark Text",
        "view_student_groups"   => "View Student Groups",
        "view_parent_groups"    => "View Parent Groups",
        "edit_group"            => "Edit Group",
        "group_name"            => "Group Name",
        "flag"                  => "Group Type",
        "description"           => "Description",

        "add_attendance"        => "Add Attendance",
        "edit_attendance"       => "Edit Attendance",
        "view_attendance"       => "View Attendance",
        "student_attendance"    => "Attendance Date",
        "total_present_student" => "Present Students",
        "total_absent_student"  => "Absent Students",

        "staff_attendance"      => "Attendance Date",
        "total_present_staff"   => "Total Present Staff",
        "total_absent_staff"    => "Total Absent Staff",
        "total_halfday_staff"    => "Total Half-Day Staff",

        "manage_hostel"     => "Manage Hostel",
        "edit_hostel"       => "Edit Hostel",
        "hostel_name"       => "Hostel Name",
        "hostel_type"       => "Hostel Type",

        "manage_room_cate"  => "Manage Room Category",
        "edit_room_cate"    => "Edit Room Category",
        "h_room_cate_name"  => "Category Name",
        "h_room_cate_fees"  => "Fees",
        "h_room_cate_des"   => "Description",

        "manage_hostel_block"  => "Manage Hostel Block",
        "edit_hostel_block"    => "Edit Hostel Block",
        "h_block_name"      => "Block Name",
        "h_floor_no"        => "Floor Number",
        "h_floor_rooms"     => "Floor Rooms",
        "h_floor_description"   => "Description",
        "floors"            => "Floors",
        "manage_room"       => "Manage Room",
        "h_room_no"         => "Room No",
        "h_room_alias"      => "Room Alias",
        "h_room_capacity"   => "Room Capacity",
        "floors"            => "Floors",
        "block_name"        => "Block Name",
        "floor_name"        => "Floor name",
        "room_category"     => "Room category",
        "edit_room"         => "Edit Room",
        "manage_registration"   => "Manage Registration",
        "room_allocation"   => "Room Allocation",
        "student_transfer"  => "Student Transfer",
        "room_leave"        => "Room Leave",
        "collect_fees"      => "Collect Fees",
        "status"            => "Status",
        "hostel_block"      => 'Hostel-Block',
        "floor_room_no"     => "Floor-Room No",
        "join_date"         => "Join Date",
        "leave_date"        => "Leave Date",
        "room_category"     => "Room Category",
        "room_available"    => "Room Available",

        "registered_student_report" => "Registered Student Report",
        "leave_student_report"      => "Leave Student Report",
        "allocation_report"         => "Allocation Report",
        "free_space_room_report"    => "Free Space Room Report",

        "homework_group"        => "Homework Group",
        "view_homework_group"   => "View Homework Group",
        "homework"              => "Homework",

        "notice"            => "Notice", // For Notice Board
        "add_notice"        => "Add Notice",
        "view_notice"       => "View Notice",
        "edit_notice"       => "Edit Notice",
        "notice_name"       => "Notice Name",
        "notice_text"       => "Notice",
        "class_ids"         => "Class(Multiple)",
        "staff_ids"         => "Staff(Multiple)",
        "for_class"         => "For Class",
        "for_staff"         => "For Staff",
        "publish_date"      => "Publish Date",
        
        "submit_date"      => "Submit Date",

        "competition_certificates"  => "Competition Certificates", // For Certificates
        "character_certificates"  => "Character Certificates",
        "character_template1"  => "Character Template 1",
        "transfer_certificates"  => "Transfer Certificates",
        "edit_certificate"          => "Edit Certificate",
        "template_status"           => "Status",
        "template_name"             => "Template Name",
        "template_for"              => "Template For",
        "book_no"                   => "Book No(Range)",
        "serial_no"                 => "Serial No(Range)",
        "book_no_start"             => "Book No(Start)",
        "book_no_end"               => "Book No(End)",
        "serial_no_start"           => "Serial No(Start)",
        "serial_no_end"             => "Serial No(End)",
        "template_link"             => "Template",

        "schedules"                 => "Schedules", // For exam schedules
        "add_schedule"              => "Add Schedule",
        "edit_schedule"             => "Edit Schedule",
        "schedule_name"             => "Schedule Name",
        "classes"                   => "Classes",
        "manage_classes"            => "Manage Classes",
        "exam_date"                 => "Exam Date",
        "exam_time"                 => "Time Range",
        "staffs"                    => "Teachers",
        "view_exam_schedules"       => "View Exam Schedule",
        "schedule_date_range"       => "Date Range",
        "teachers"                  => "Teachers",
        "schedule_time_range"       => "Time Range",
        "roll_no_from"              => "Roll No(From)",
        "roll_no_to"                => "Roll No(To)",

        "view_fees_receipt"         => "View Fees Receipt", // Fees Receipt
        "receipt_number"            => "Receipt No",
        "receipt_date"              => "Receipt Date",
        "net_amt"                   => "Net Amount",
        "pay_amt"                   => "Pay Amount",
        "wallet_amt"                => "Wallet Use",
        "concession_amt"            => "Conce. Amount",
        "fine_amt"                  => "Fine Amount",
        "imprest_amt"               => "Imprest Amt Use",
        "pay_later_amt"             => "Pay-later Amount",
        "pay_mode"                  => "Payment Mode",

        "manage_marks"              => "Manage Marks", // Manage Marks
        
        "manage_ac_entry"           => "Manage Entry", // Imprest A/C entry
        "ac_entry_name"             => "Entry Name",
        "ac_entry_date"             => "Entry Date",
        "ac_entry_amt"              => "Entry Amount",

         "inventory"                     => "Inventory",     // For Vendor of Inventory Module
        "manage_vendor"                 => "Manage Vendor",
        "edit_vendor"                   => "Edit Vendor",
        "add_vendor"                    => "Add Vendor",
        "inventory_s_no"                => "S No",
        "inventory_name"                => "Name",
        "inventory_gstin"               => "GSTIN",
        "inventory_contact_no"          => "Contact No",
        "inventory_email"               => "Email",
        "inventory_address"             => "Address",  
        "inventory_vendor_name"         => "Vendor Name",
        "inventory_form_gstin"          => "GSTIN",
        "inventory_form_contact_no"     => "Contact No",
        "inventory_form_email"          => "Email",
        "inventory_form_address"        => "Address", 

       
        "manage_unit"                   => "Manage Unit",   // For Unit of Inventory Module
        "edit_unit"                     => "Edit Unit",
        "add_unit"                      => "Add Unit",
        "unit_s_no"                     => "S No",
        "unit_name"                     => "Unit Name",    


        "manage_category"                   => "Manage Category", // For Category of Inventory Module
        "edit_category"                     => "Edit Category",
        "add_category"                      => "Add Category",
        "category_s_no"                     => "S No",
        "category_level"                    => "Category Level",
        "category_name"                     => "Category Name",   
        "s_category_name"                   => "Category Name",


        "manage_item"                       => "Manage Item", // For Items of Inventory Module
        "edit_item"                         => "Edit Item",
        "add_item"                          => "Add Item",
        "item_s_no"                         => "S No",
        "item_name"                         => "Item Name", 
        "item_category_name"                => "Category", 
        "item_sub_category_name"            => "Sub Category",
        "item_measuring_unit"               => "Measuring Unit", 
        "s_item_name"                       => "Item Name", 

        // For Task Manager 
        "add_task"                          => "Add Task",
        "edit_task"                         => "Edit Task",
        "view_task"                         => "View Task",
        "view_responses"                    => "View Responses",
        "mapping"                           => "Mapping",
        "task_name"                         => "Task Name",
        "task_priority"                     => "Priority",
        "task_date"                         => "Task Date",
        "task_description"                  => "Task Description", 
        "task_s_no"                         => "S No",
        "task_status"                       => "Status",

        // For Admin Type
        'admin_type'                        => 'User Type',

        "student_remark"                     => "Remarks", // For Items of Student Remarks
        "student_teacher"                    => "Teacher",
        "remark_description"                 => "Remark Description",

        "student_homework"                   => "Homework", // For Items of Student Homework
        "issue_transfer_certificate"         => "Issue Transfer Certificate",
        "date"                               => "Date",
        "homework_description"               => "HomeWork Description",

        "my_attendance"                      => "Attendance", // For Items of Student Attendence
        "attendance"                         => "Attendance",

        "my_timetable"                       => "Time Table", // For Items of Student TimeTable 

        "edit_fee_head"                      => "Edit Hostel Fees Head", // For Hostel Fees Head
        "manage_h_fees_head"                 => "Manage Hostel Fees Head",
        "h_fees_head_name"                   => "Fees Head Name",
        "h_fees_head_price"                  => "Price",
        "hostel_fees_head"                   => "Hostel Fees Head",
        "fees_head"                          => "Fees Head",

        "my_examination"                     => "Exams",
        "my_marks"                           => "Marks",

        // For Vehicle Module

        "vehicle"                            => "Vehicle", 
        "add_vehicle"                        => "Add Vehicle",
        "view_vehicle"                       => "View Vehicle",
        "edit_vehicle"                       => "Edit Vehicle",
        "vehicle_name"                       => "Vehicle Name",
        "vehicle_registration_number"        => "Vehicle Registration Number",
        "vehicle_ownership"                  => "Ownership",
        "vehicle_capacity"                   => "Capacity",
        "contact_person"                     => "Contact Person",
        "contact_number"                     => "Contact Number",
        "vehicle_document_category"          => "Document Category",
        "vehicle_document_file"              => "Document File",
        "vehicle_document_details"           => "Document Details",
        "person_info"                        => "Person Info",
        "total_documents"                    => "Total Documents",

        "edit_invoice_config"                => " Edit Invoice Configuration", // For Inventory Configuration 
        "add_invoice_config"                 => "Add Invoice Configuration",
        "manage_invoice_config"              => "Manage Invoice Configuration",  
        "invoice_prefix"                     => "Prefix",
        "invoice_date"                       => "Date",
        "invoice_type"                       => "Invoice Type",
        "invoice_s_no"                       => "S No",
        "invoice_format"                     => "Invoice Format",

        "purchase"                           => "Purchase", // For Purchase
        "add_purchase"                       => "Add Purchase", 
        "view_purchase"                      => "View Purchase",
        "edit_purchase"                      => "Edit Purchase",
        "system_invoice_no"                  => "System Invoice No",
        "ref_invoice_no"                     => "Reference Invoice No",
        "invoice_date"                       => "Date",   
        "invoice_vendor"                     => "Vendor",
        "invoice_category"                   => "Category",
        "invoice_subcategory"                => "Sub-Category",
        "payment_mode"                       => "Payment Mode",
        "bank_name"                          => "Bank Name",
        "cheque_no"                          => "Cheque No",
        "cheque_date"                        => "Cheque Date",
        "cheque_amount"                      => "Cheque Amount", 
        "rate"                               => "Rate", 
        "quantity"                           => "Quantity", 
        "unit"                               => "Unit",   
        "view_item_details"                  => "View Item Details",
        "s_item_name"                        => "Item Name",
        "amount"                             => "Amount",  
        "view_details"                       => "View Details",
        "calculation_amount"                 => "Calcuation Amount",

        "payroll"                            => "Payroll",   // For Payroll Module     
        "manage_department"                  => "Department",  
        "manage_grade"                       => "Grade", 
        "dept_name"                          => "Department Name",
        "dept_desc"                          => "Description",
        "dept_s_no"                          => "S No",
        "no_staff_mapped"                    => "Staff Mapped",
        "not_mapped"                         => "Not Mapped",
        "dept_status"                        => "Status",     
        "mapped"                             => "Mapped", 
        "add_dept"                           => "Add Department",
        "edit_dept"                          => "Edit Department",   
        "map_employees"                      => "Map Employees",     
        "emp_name"                           => "Employee Name",                        

        "import_students"                   => "Import Students",
        "section_id"                        => "Section ID",
        "ID"                                => "ID",
        "annual_salary"                     => "Annual Salary",
        "natioanlity"                       => "Natioanlity",
        "group"                             => "House/Group",
        "catse"                             => "Catse",
        "category"                          => "Category",
        "import_file"                       => "Import File",
        "import_staff"                      => "Import Staff",

        "grade_name"                        => "Grade Name",  // For Payroll grade Module 
        "grade_desc"                        => "Description",
        "grade_s_no"                        => "S No",
        "add_grade"                         => "Add Grade",
        "edit_grade"                        => "Edit Grade",      
        // For Route Module

        "route"                              => "Route", 
        "add_route"                          => "Add Route",
        "view_route"                         => "View Route",
        "edit_route"                         => "Edit Route",
        "route_name"                         => "Route Name",
        "route_description"                  => "Route Description",
        "route_type"                         => "Route Type",
        "location_name"                      => "Location Name",
        "location_sequence"                  => "Sequence",
        "location_latitude"                  => "Location Latitude",
        "location_longitude"                 => "Location Longitude",
        "eta_pickiup"                        => "ETA Pickup",
        "eta_drop"                           => "ETA Drop",
        "route_stops"                        => "Stops",
        "manage_route"                       => "Manage Route",
        "vehicle_driver"                     => "Driver",
        "vehicle_conductor"                  => "Conductor",
        "latitude_logitude"                  => "Lat Long",
        "pick_drop"                          => "Pick Drop Time",
        "km_distance"                        => "Km Distance",
        "map_views"                          => "Map View",

        // For Fees Head Module

        "fees_head"                          => "Fees Head", 
        "add_fees_head"                      => "Add Fees Head",
        "view_fees_head"                     => "View Fees Head",
        "edit_fees_head"                     => "Edit Fees Head",
        "fees_head_name"                     => "Name",
        "fees_head_km_from"                  => "Km Range (Min)",
        "fees_head_km_to"                    => "Km Range (Max)",
        "fees_head_amount"                   => "Amount (Per Month)",

        "pf_setting"                         => "PF Setting", // For Payroll pf/esi Module
        "pf_month"                           => "Month",
        "epf_a"                              => "EPF (A) (%)",
        "pension_fund_b"                     => "Pension Fund (B)",
        "epf_ab"                             => "EPF(A-B)",
        "pf_cut_off"                         => "Cut Off",
        "acc_no_02"                          => "Acc. No. 02 (%)",
        "acc_no_21"                          => "Acc. No. 21 (%)", 
        "acc_no_22"                          => "Acc. No. 22 (%)",     
        
        "configuration"                      => "Configuration",  // For Payroll Configuration Module
        "pf_config_name"                     => "Setting Name",
        "pf_enable_disable"                  => "Enable/Disable",

        "add_pf"                             => "Add PF",
        "edit_pf"                            => "Edit PF",
        "pf_year"                            => "Year",  
        "pf_amount"                          => "Amount", 
        "month_year"                         => "Month-Year",   

        "add_tds"                            => "Add TDS",      // For Payroll TDS Module
        "edit_tds"                           => "Edit TDS",
        "salary_range_min"                   => "Salary Range(Min)",
        "salary_range_max"                   => "Salary Range(Max)",
        "sal_deduction"                      => "Deduction(%)",   
        "tds_setting"                        => "TDS Setting",
        "salary_range"                       => "Salary Range(Min-Max)",

        "add_esi"                            => "Add ESI",   // For Payroll ESI Module
        "edit_esi"                           => "Edit ESI",
        "esi_setting"                        => "ESI Setting",
        "employee"                           => "Employee (%)",
        "employer"                           => "Employer (%)",
        "esi_cut_off"                        => "Cut Off",  

        "add_pt"                             => "Add Pt",
        "edit_pt"                            => "Edit Pt",
        "pt_setting"                         => "PT Setting",
        "view_amount"                        => "View Amount",
        "amount"                             => "Amount",

        // For Manage student staff Module

        "manage_student_staff"               => "Manage Student Staff Vehicle",
        "driver_conductor_info"              => "Driver / Conductor Info",
        "no_of_student"                      => "No. of Students",
        "no_of_staff"                        => "No. of Staff",
        "route_info"                         => "Route Info",
        "manage_staff"                       => "Manage Staff",
        "manage_student"                     => "Manage Student",
        "father_contact"                     => "Father Contact Number",
        "staff_list"                         => "View All Staff",
        "student_list"                       => "View All Student",

        "vehicle_attendence"                 => "Vehicle Attendance",
        "add_attendence"                     => "Add Attendance",
        "view_attendence"                    => "View Attendance",
        "student_staff_name"                 => "Student / Staff Name",
        "student_staff_type"                 => "Student / Staff Type",
        "total_present_staff"                => "Total Present Staff",
        "total_present_student"              => "Total Present Student",
        "total_strength"                     => "Total Strength",
        "vehicle_attendence_details"         => "Vehicle Attendance Details",
        "attendence_report"                  => "Attendance Report",

        // For Transport Job Module
        "job"                                => "Job",
        "add_job"                            => "Add Job",
        "view_job"                           => "View Job",
        "edit_job"                           => "Edit Job",  
        "job_name"                           => "Name",  
        "job_type"                           => "Type",
        "job_no_of_vacancy"                  => "No of Vacancy",
        "job_no_of_remaining_vacancy"        => "Remaining Vacancy",
        "job_recruitment"                    => "Recruitment",
        "job_description"                    => "Description",
        "job_durations"                      => "Durations",
        "job_info"                           => "Info",

        // For Transport form module
        "add_form"                           => "Add Form",
        "job_name_new"                       => "Job Name",
        "edit_form"                          => "Edit Form",
        "recruitment_form"                   => "Recruitment Form",

        //  For Payroll Arrear Module
        "add_arrear"                        => "Add Arrear",
        "edit_arrear"                       => "Edit Arrear",    
        "manage_arrear"                     => "Manage Arrear",
        "arrear_name"                       => "Arrear Name",
        "arrear_from_date"                  => "From Date",
        "arrear_to_date"                    => "To Date",    
        "arrear_amount"                     => "Amount (Per Month)",
        "arrear_total_amt"                  => "Total Amount",
        "arrear_round_off"                  => "Round Off",
        "arrear_s_no"                       => "S No",
        "date_range"                        => "Date Range",

        //  For Payroll Bonus Module
        "add_bonus"                        => "Add Bonus",
        "edit_bonus"                       => "Edit Bonus",    
        "manage_bonus"                     => "Manage Bonus",
        "bonus_name"                       => "Bonus Name", 
        "bonus_amount"                     => "Bonus Amount",   
        "bonus_s_no"                       => "S No",   
        "applied_candidates"                 => "Applied Candidates",
        "form_number"                        => "Form Number",
        "selected_candidates"                => "Selected Candidates",
        "recruitment_report"                 => "Recruitment Report",
        "total_applied"                      => "Total Applied",
        "total_approved"                     => "Total Approved",
        "total_disapproved"                  => "Total Disapproved",
        "resume"                             => "Resume",
        "candidate_details"                  => "Candidate Details",

        // For report card generation
        "manage_report_card"                => "Manage Report Card",
        "total_pass_students"                => "Total Pass Students",
        "total_fail_students"                => "Total Fail Students",
        "generate_marksheet"                => "Generate Marksheet",

        // For Payroll Salary Head
        "add_sal_head"                      => "Add Salary Head",
        "edit_sal_head"                     => "Edit Salary Head",
        "manage_sal_head"                   => "Manage Salary Head",
        "sal_head_name"                     => "Head Name",
        "head_type"                         => "Type",
        "sal_head_s_no"                     => "S No",

        // For Payroll Loan
        "add_loan"                          => "Add Loan",
        "edit_loan"                         => "Edit Loan",
        "manage_loan"                       => "Manage Loan",
        "loan_employee"                     => "Employee",
        "loan_principal"                    => "Loan Principal",
        "loan_from_date"                    => "From Date",
        "loan_to_date"                      => "To Date",
        "loan_interest_rate"                => "Interest Rate (Per Year)",
        "loan_deduct_amount"                => "Deduct Amount (Per Month)",
        "loan_total_amt"                    => "Total Amount (Amount to be Paid)",
        "loan_date_range"                   => "Tenure Date",

        // For Payroll Advance
        "add_advance"                       => "Add Advance",
        "edit_advance"                      => "Edit Advance",
        "manage_advance"                    => "Manage Advance",
        "advance_amount"                    => "Advance Amount",
        "suggested_amount"                  => "Suggested Amount (Per Month)",
        "suggested_amount1"                 => "Suggested Amount",
        "paid_amount"                       => "Paid Amount",
        "remaining_amount"                  => "Remaining Amount",

        // For Payroll Salary Structure
        "add_sal_structure"                 => "Add Salary Structure",
        "edit_sal_structure"                => "Edit Salary Structure",
        "manage_sal_structure"              => "Manage Salary Structure",
        "sal_structure_name"                => "Salary Structure Name",
        "sal_head_info"                     => "Salary Heads Info",
        // For Task manager
        "task_type"                          => "Task Type",
        "my_task"                            => "My Task",
        "task_file"                          => "Task File",
        "task_response"                      => "Task Response",

        // For report card generation
        "total_exam_marks"                  => "Total Marks",
        "total_obtain_marks"                => "Obtain Marks",
        "student_percentage"                => "Percentage",
        "class_rank"                        => "Class Rank",
        "section_rank"                      => "Section Rank",
        "result_date"                       => "Result Date",

        // Generate TC
        "tc_date"                           => "TC Date",
        "tc_remark"                         => "TC Remark",
        "tc_reason"                         => "TC Reason",
        "view_tc"                           => "View Transfer Certificate",
        'tc_issue_date'                     => "Date of Issue",
        "tc_no"                             => "TC No.",
        "view_tc"                           => "View Issue Transfer Certificate",
        'tc_issue_date'                     => "Date of Issue",
        "tc_no"                             => "TC No.",

        // Generate CC
        "issue_character_certificate"   => "Issue Character Certificate",
        "cc_date"                       => "CC Date",
        "cc_reason"                     => "CC Reason",
        "view_cc"                       => "View Issue Character Certificate",
        'cc_issue_date'                 => "Date of Issue",
        "cc_no"                         => "CC No.",

        // Communication
        "communication"                 => "Communication",
        "staff_type"                    => "Staff Type",
        "staff_communication"           => "Staff Communication",
        "view_parents"                  => "View Parents",
        "parent_communication"          => "Parent Communication",

        // For Marksheet Templates
        'menu_marksheet_templates'  => "Marksheet Templates",
        'examwise_templates'        => "Exam Wise Templates",
        'termwise_templates'        => "Term Wise Templates",
        'annual_templates'          => "Annual Templates",
        'pass_fail_status'          => "Pass/Fail",
        'view_marksheet'            => "View Marksheet",
        'tabsheet'                  => "Tabsheet",
        // Inventory
        "manage_purchase_return"    => "Manage Purchase Return",

        // For Examination Reports
        "menu_examination_report" => "Examination Report",
        "examwise_mark"           => "Exam Wise Marks Report",
        "subjectwise_mark"        => "Subject Wise Marks Report",
        "exam_roll_no"            => "Enroll No",
        "exam_student_name"       => "Student Name",
        "exam_total_mark"         => "Total Marks",
        "exam_obtain_mark"        => "Obtain Marks",   
        "percentage_report"       => "Percentage Report",  
        "exam_percentage"         => "Percentage",
        "exam_report"             => "Exam Report",
        "school_latitude"         => "Latitude",
        "school_longitude"        => "Longitude",
        "school_lat_long"         => "Latitude & Longitude",
        "student_rank"            => "Student Rank Report",
        "topper_rank_list"        => "Topper Rank List Report",  

        // For Admission Reports
        "menu_admission_report"   => "Admission Report",
        "classwise_admission"     => "Classwise Admission Report",
        "board_of_examination"    => "Board of Education",
        "no_of_applicant"         => "No of Applicant",
        "classwise_enquiry"       => "Classwise Enquiry Report",
        "enquiry_date"            => "Enquiry Date",
        "contact_no"              => "Contact No",
        "formwise_fees"           => "Form Wise Fees Report",    
        "class_medium"            => "Class-Medium",  
        "fees_collected"          => "Fees Collected",

        // Inventory
        "purchase_type"             => "Purchase Type",
        "sale"                      => "Sale", // For Sale
        "add_sale"                  => "Add Sale", 
        "view_sale"                 => "View Sale",
        "edit_sale"                 => "Edit Sale",
        "sale_type"                 => "Sale Type",

        // For Student Reports
        "menu_student_report"        => "Student Report",
        "student_attendance_report"  => "Student Attendance Report",
        "total_working_days"         => "Total Working Day<br>(Till Now)",
        "total_present"              => "Total Present",
        "menu_student_report"        => "Student Report",
        "menu_staff_report"          => "Staff Report",   
        "staff_attendance_report"    => "Staff Attendance Report",
        "staff_unique_id"            => "Unique ID",
        "total_paid_leaves"          => "Total Paid Leaves",
        "total_loss_pay"             => "Total Loss of Pay",   

        "sale"                      => "Sales", // For Sale
        "add_sale"                  => "Add Sales", 
        "view_sale"                 => "View Sales",
        "edit_sale"                 => "Edit Sales",
        "sale_type"                 => "Sales Type",
        "manage_sale_return"        => "Manage Sales Return",


        // For Fees Collection Reports
        "menu_fees_collection_report" => "Fees Collection Report",
        "rte_cheque"                  => "RTE Cheque Report",
        "imprest_entries"             => "Imprest Entries Report",

        "manage_stock_register"     => "Manage Stock Register",

        // Library Reports
        "menu_library_report"       => "Library Report",
        "registered_student"        => "Registered Student Report",
        "books_report"              => "Books Report",
        "book_name"                 => "Book Name",
        "book_isbn_no"              => "Book ISBN No",
        "book_author_name"          => "Author Name",
        "book_price"                => "Price",
        "issued_books_report"       => "Issued Book Report",
        "member_type"               => "Member Type",
        "dues_report"               => "Dues Report",
        "expired_date"              => "Expired Date",

        // For Payroll Salary Structure Map Staff
        "sal_basic_pay"                     => "Basic Pay",
        "sal_structure"                     => "Salary Structure",     

        // Fees Collection Reports
        "class_monthly"             => "Class Monthly Sheet",
        "student_monthly"           => "Student Monthly Sheet",
        "student_headwise"          => "Student Headwise Report",
        "class_headwise"            => "Class Headwise Report",
        "student_fees_applied"      => "Fees Applied",
        "student_paid_fees"         => "Paid Fees",
        "student_concession_applied"=> "Concession Applied",
        "student_remaining_fees"    => "Remaining Fees",
        "student_fine_paid"         => "Fine Paid",
        "student_cheque"            => "Student Cheque",
        "student_fine"              => "Student Fine Report",
        "receipt_no"                => "Receipt No",
        "concession_report"         => "Concession Report",
        "head_concession_amt"       => "Head - Concession Amount",
        "total"                     => "Total",


        // Plan Schedule
        "plan_schedule"                    => "Plan Schedule", // For Plan Schedule
        "add_plan_schedule"                => "Add Plan Schedule",
        "view_plan_schedule"               => "View Plan Schedule",
        "edit_plan_schedule"               => "Edit Plan Schedule",
        "plan_date"                        => "Date",
        "plan_time"                        => "Time",
        "plan_description"                 => "Description",
        "my_plan_schedule"                 => "My Plan Schedule",

        // Fees Information
        "total_fees"                        => "Total Fees",
        "due_amount"                        => "Due Amount",
        "consession_amount"                 => "Consession Amount",
        "wallet_amount"                     => "Wallet Amount",
        "imprest_amount"                    => "Imprest Amount",
        "installment"                       => "Installment",

        // Leave Scheme 
        "edit_lscheme"                      => "Edit Leave Scheme",
        "add_lscheme"                       => "Add Leave Scheme",
        "manage_lscheme"                    => "Manage Leave Scheme",
        "lscheme_name"                      => "Leave Scheme Name",   
        "lscheme_period"                    => "Leave Scheme Period", 
        "no_of_leaves"                      => "No of Leaves",
        "special_instruction"               => "Special Instruction",
        "carry_forward_limit"               => "Carry Forward Limit",
        "leave_scheme"                      => "Leave Scheme",    

        // Receipt
        "receipts"                          => "Receipts",
        "received_fees"                     => "Received Fees",
        "student_registeration_date"        => "Registeration Date",
        "start_date"                        => "Start Date",
        "end_date"                          => "End Date",
        "registered_students"               => "Registered Student",
        "fees_due_report"                   => "Fees Due Report",
        "loss_of_pay"                       => "Loss of Pay",
        "remaining_leaves"                  => "Remaining Leaves",
        "manage_leave"                      => "Manage Leave",

        // Salary Generation
        "manage_salary_generation"                 => "Manage Salary Generation",
        "action"                            => "Action",
        "permissions"                       => "Permissions",

        "unauthorized"                      => "Unauthorized Page",
        "menu_substitute_management"        => "Substitute Management",
        "manage_substitution"               => "Manage Substitution",

        // System Configuration 
        "add_system_config"                 => "System Configuration",
        "edit_system_config"                => "Edit System Configuration",
        "config_email"                      => "Email",
        "config_password"                   => "Password",
        "sms_gateway_url"                   => "SMS Gateway URL",
        "sms_username"                      => "Username",
        "sms_password"                      => "Password",
        "sms_sender"                        => "Sender",
        "sms_priority"                      => "Priority",
        "sms_stype"                         => "Stype",

        // Accounts Module
        "menu_account"                      => "Accounts",
        "account_group"                     => "Accounts Group",
        "main_head"                         => "Main Heads",
        "sub_head"                          => "Sub Heads",
        "acc_group"                     => "Account Group",
        "edit_acc_group"                    => "Edit Account Group",


        // For substitute management
        "allocate"      => "Allocate",
        "date_from"     => "Date(From)",
        "date_to"       => "Date(To)",
        "as_class_teacher"  => "Appoint as Class Teacher",
        "pay_extra"     => "Pay Extra",
        "view_allocations"  => "View Allocations",
        "class_lecture"     => "Class - Lecture",
        "edit_allocate"     => "Edit Allocate",

        "substitute_report" => "Substitute Report",
        "substitution" => "Substitution",
        "menu_report" => "Report",
        "missed_substitute_report" => "Missed Substitute Report",
        "class_wise_status" => "Class Wise Status",

        // Salary Generation
        "salary_generate"       => "Salary Generation",
        "account_heads"         => "Account Heads",
        "acc_group_head_name"   => "Group Head",
        "account_heads"         => "Account Heads",
        "account_heads"         => "Account Heads",
        "account_heads"         => "Account Heads",
        "acc_group_head"        => "Group Head",
        "edit_acc_group_h"      => "Edit Account Group Head",
        "head_name"             => "Head Name",

        // Opening Balance
        "open_balance"          => "Opening Balance",
        "edit_open_balance"     => "Edit Opening Balance",
        "balance_amount"        => "Amount",
        "balance_date"          => "Date"

    ];