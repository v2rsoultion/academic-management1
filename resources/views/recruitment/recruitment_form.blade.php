<!DOCTYPE html>
<html>
<head>
	<title>Admission Form</title>
	
	{!! Html::style('public/web/css/style.css') !!}
	{!! Html::style('public/admin/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('public/admin/assets/css/main.css') !!}
    {!! Html::style('public/admin/assets/css/style1.css') !!}
    {!! Html::script('public/admin/assets/js/jquery.min.js') !!}
    {!! Html::script('public/admin/assets/js/bootstrap-tagsinput.min.js') !!}
    {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!}
    {!! Html::style('public/admin/assets/css/select2.min.css') !!}

</head>

<style type="text/css">
	.btn-primary {
	    background-color: #1f2f60 !important;
	}
</style>
<body>
	<div class="header">
		{!! Form::open(['files'=>TRUE,'id' => 'recruitment-form' , 'class'=>'','url' =>$save_url]) !!}
		<table style="width: 100%;">
			<tr>
				<td style="width: 4%;">
					<img src="{!! URL::to('public/uploads/school-logo/'.$school_data['school_logo']) !!}" alt="" width="120px">
				</td>
				<td style="width: 25%;" class="align">
					<h2 style="margin:0px;"> {{ $school_data['school_name'] }}</h2>	
					<p class="less" style="font-size: 12px;margin-top: 0px;margin-bottom: 2px;">
					<u style="font-size: 13px;">
						{{ $school_data['school_address'] }} 
						@if( $school_data['city_name'] != "" ) , {{ $school_data['city_name'] }} @endif
					</u><br> 
						@if( $school_data['school_telephone'] != "" ) Phone: {{ $school_data['school_telephone'] }} @endif
						@if( $school_data['school_fax_number'] != "" ) Fax: {{ $school_data['school_fax_number'] }} @endif
					&nbsp;&nbsp; 
					<br>E-mail : {{ $school_data['school_email'] }} </p>	
				</td>
				<td style="border:1px solid #000;width:4%;height: 130px;">
					<div class="avatar-upload">
				        <div class="avatar-edit">
				            <input type='file' id="imageUpload" name="staff_profile_img" accept=".png, .jpg, .jpeg" />
				            <label for="imageUpload"> <i class="zmdi zmdi-edit" style="margin-left: 10px;"></i> </label>
				        </div>
				        <div class="avatar-preview">
				            <div id="imagePreview"></div>
				        </div>
				    </div>
				</td>
			</tr>
		</table>

		<hr>
		<div class="row">
        <div class="col-lg-12">
        	<div class="panel panel-default" style=" background-color:#ffffff">
        		<div class="panel-body">
        			
					<div class="row col-lg-12 p-0 m-0">
						<div class="row col-lg-6 p-0">
							<div class="col-lg-2" style="margin-top: 7px;">
								<lable class="from_one1">Form No:</lable> 
							</div>
							<div class="col-lg-3 p-0">
				    			<input type="text" name="form_number" id="" class="form-control" value="{{ $job_form['form_prefix'] }} - {{ $job_form['job_form_id'] }}" readonly="true" style="padding-top:10px;">

				    			<input type="hidden" name="job_form_id" value="{{ $job_form['job_form_id'] }}">
				    			<input type="hidden" name="job_id" value="{{ $job['job_id'] }}">
				    		</div>	
				    	</div>
				    	<div class="clearfix"> </div>
				    	<div class="row col-lg-6 p-0 m-0">
				    		<div class="col-lg-10 text-right" style="padding: 0px 15px 0px 0px;margin-top: 7px;">
								<lable class="from_one1">Date:</lable> 
							</div>
							<div class="col-lg-2 p-0 m-0">
				    			<input type="text" name="name" id="" class="form-control" value="{{ date('d-m-Y') }}" readonly="true" style="width: 125px;padding-top: 10px;">
				    		</div>	
				    	</div>	
					</div>

					<div style="margin-top: 20px;">
					@if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    </div>


					@if(in_array("staff_name", $job_form_fields) || in_array("designation_id", $job_form_fields) || in_array("staff_email", $job_form_fields) || in_array("staff_mobile_number", $job_form_fields) || in_array("staff_dob", $job_form_fields) || in_array("staff_gender", $job_form_fields) )
					<div class="row col-lg-12">
						<h5 class="heading-style">Basic Information: </h5>
					</div>
					@endif
					<div class="row col-lg-12 p-0 m-0">

						@if(in_array("staff_name", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_name') !!} <span class="red-text">*</span> :</lable>
								{!! Form::text('staff_name', old('staff_name',isset($staff['staff_name']) ? $staff['staff_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_name'), 'id' => 'staff_name']) !!}

							</div>
						</div>
						@endif

						@if(in_array("designation_id", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_designation_id') !!} <span class="red-text">*</span> :</lable> 
								{!!Form::select('designation_id', $staff['arr_designations'],isset($staff['designation_id']) ? $staff['designation_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'designation_id'])!!}
							</div>
						</div>
						@endif

						@if(in_array("staff_email", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_email') !!}  :</lable> 
								{!! Form::text('staff_email', old('staff_email',isset($staff['staff_email']) ? $staff['staff_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_email'), 'id' => 'staff_email']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_mobile_number", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_mobile_number') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::text('staff_mobile_number', old('staff_mobile_number',isset($staff['staff_mobile_number']) ? $staff['staff_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mobile_number'), 'id' => 'staff_mobile_number','minlength'=> '10', 'maxlength'=> '10']) !!}
							</div>
						</div>
						@endif
					</div>
					<div class="row col-lg-12 p-0 m-0">
						@if(in_array("staff_dob", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_dob') !!} <span class="red-text">*</span> :</lable>
								{!! Form::date('staff_dob', old('staff_dob',isset($staff['staff_dob']) ? $staff['staff_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_dob'), 'id' => 'staff_dob']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_gender", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_gender') !!} <span class="red-text">*</span> :</lable> 
								{!!Form::select('staff_gender', $staff['arr_gender'],isset($staff['staff_gender']) ? $staff['staff_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_gender'])!!}
							</div>
						</div>
						@endif

					</div>

					@if(in_array("staff_aadhar", $job_form_fields) || in_array("staff_UAN", $job_form_fields) || in_array("staff_ESIN", $job_form_fields) || in_array("staff_PAN", $job_form_fields) )
					<div class="row col-lg-12">
						<h5 class="heading-style">Official Information: </h5>
					</div>
					@endif

					<div class="row col-lg-12 p-0 m-0">

						@if(in_array("staff_aadhar", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_aadhar') !!} <span class="red-text">*</span> :</lable>
								{!! Form::text('staff_aadhar', old('staff_aadhar',isset($staff['staff_aadhar']) ? $staff['staff_aadhar']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_aadhar'), 'minlength'=> '12','maxlength'=> '12','id' => 'staff_aadhar']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_UAN", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_UAN') !!} :</lable>
								{!! Form::text('staff_UAN', old('staff_UAN',isset($staff['staff_UAN']) ? $staff['staff_UAN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_UAN'), 'id' => 'staff_UAN','minlength'=> '12','maxlength'=> '12']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_ESIN", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_ESIN') !!} :</lable> 
								{!! Form::text('staff_ESIN', old('staff_ESIN',isset($staff['staff_ESIN']) ? $staff['staff_ESIN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_ESIN'), 'id' => 'staff_ESIN']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_PAN", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_PAN') !!} :</lable> 
								{!! Form::text('staff_PAN', old('staff_PAN',isset($staff['staff_PAN']) ? $staff['staff_PAN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_PAN'), 'id' => 'staff_PAN']) !!}
							</div>
						</div>
						@endif
					</div>

					@if(in_array("staff_father_name_husband_name", $job_form_fields) || in_array("staff_father_husband_mobile_no", $job_form_fields) || in_array("staff_mother_name", $job_form_fields) || in_array("staff_blood_group", $job_form_fields) || in_array("caste_id", $job_form_fields) || in_array("religion_id", $job_form_fields) || in_array("nationality_id", $job_form_fields) || in_array("staff_marital_status", $job_form_fields) || in_array("title_id", $job_form_fields) )  

					<div class="row col-lg-12">
						<h5 class="heading-style">Other Information: </h5>
					</div>
					@endif

					<div class="row col-lg-12 p-0 m-0">

						@if(in_array("staff_father_name_husband_name", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_father_name_husband_name') !!} <span class="red-text">*</span> :</lable>
								{!! Form::text('staff_father_name_husband_name', old('staff_father_name_husband_name',isset($staff['staff_father_name_husband_name']) ? $staff['staff_father_name_husband_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_name_husband_name'), 'id' => 'staff_father_name_husband_name']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_father_husband_mobile_no", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_father_husband_mobile_no') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::text('staff_father_husband_mobile_no', old('staff_father_husband_mobile_no',isset($staff['staff_father_husband_mobile_no']) ? $staff['staff_father_husband_mobile_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_husband_mobile_no'), 'id' => 'staff_father_husband_mobile_no', 'maxlength' => '10']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_mother_name", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_mother_name') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::text('staff_mother_name', old('staff_mother_name',isset($staff['staff_mother_name']) ? $staff['staff_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mother_name'), 'id' => 'staff_mother_name']) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_blood_group", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_blood_group') !!} :</lable> 
								{!! Form::text('staff_blood_group', old('staff_blood_group',isset($staff['staff_blood_group']) ? $staff['staff_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_blood_group'), 'id' => 'staff_blood_group']) !!}
							</div>
						</div>
						@endif
					</div>

					<div class="row col-lg-12 p-0 m-0">

						@if(in_array("caste_id", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.caste_id') !!} <span class="red-text">*</span> :</lable>
								{!!Form::select('caste_id', $staff['arr_caste'],isset($staff['caste_id']) ? $staff['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
							</div>
						</div>
						@endif

						@if(in_array("religion_id", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.religion_id') !!} <span class="red-text">*</span> :</lable>
								{!!Form::select('religion_id', $staff['arr_religion'],isset($staff['religion_id']) ? $staff['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
							</div>
						</div>
						@endif

						@if(in_array("nationality_id", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.nationality_id') !!} <span class="red-text">*</span> :</lable>
								{!!Form::select('nationality_id', $staff['arr_natioanlity'],isset($staff['nationality_id']) ? $staff['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
							</div>
						</div>
						@endif

						@if(in_array("staff_marital_status", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_marital_status') !!} <span class="red-text">*</span> :</lable>
								{!!Form::select('staff_marital_status', $staff['arr_marital'],isset($staff['staff_marital_status']) ? $staff['staff_marital_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_marital_status'])!!}
							</div>
						</div>
						@endif
					</div>
					<div class="row col-lg-12 p-0 m-0">
						@if(in_array("title_id", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.title') !!} <span class="red-text">*</span> :</lable>
								{!!Form::select('title_id', $staff['arr_title'],isset($staff['title_id']) ? $staff['title_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'title_id'])!!}
							</div>
						</div>
						@endif
					</div>

					@if(in_array("staff_specialization", $job_form_fields) || in_array("staff_qualification", $job_form_fields) || in_array("staff_reference", $job_form_fields) || in_array("staff_experience", $job_form_fields) )  
					<div class="row col-lg-12">
						<h5 class="heading-style">Qualification and Experience Information: </h5>
					</div>
					@endif

					<div class="row col-lg-12 p-0 m-0">

						@if(in_array("staff_specialization", $job_form_fields))
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_specialization') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::textarea('staff_specialization', old('staff_specialization',isset($staff['staff_specialization']) ? $staff['staff_specialization']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_specialization'), 'id' => 'staff_specialization', 'rows'=> 3]) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_qualification", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_qualification') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::textarea('staff_qualification', old('staff_qualification',isset($staff['staff_qualification']) ? $staff['staff_qualification']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_qualification'), 'id' => 'staff_qualification', 'rows'=> 3]) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_reference", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_reference') !!} :</lable> 
								{!! Form::textarea('staff_reference', old('staff_reference',isset($staff['staff_reference']) ? $staff['staff_reference']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_reference'), 'id' => 'staff_reference', 'rows'=> 3]) !!}
							</div>
						</div>
						@endif

						@if(in_array("staff_experience", $job_form_fields))
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_experience') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::textarea('staff_experience', old('staff_experience',isset($staff['staff_experience']) ? $staff['staff_experience']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_experience'),  'rows'=> 3, 'required']) !!}
							</div>
						</div>
						@endif
					</div>
					
					@if(in_array("staff_temporary_address", $job_form_fields) || in_array("staff_temporary_county", $job_form_fields) || in_array("staff_temporary_state", $job_form_fields) || in_array("staff_temporary_city", $job_form_fields) || in_array("staff_temporary_pincode", $job_form_fields) )  
					<div class="row col-lg-12">
						<h5 class="heading-style">Temporary Address: </h5>
					</div>
					@endif

					<div id="permanent_address_details">

						@if(in_array("staff_temporary_address", $job_form_fields))
						<div class="row col-lg-12 p-0 m-0">
							<div class="col-lg-12 p-0">
								<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_temporary_address') !!} :</lable>
								{!! Form::textarea('staff_temporary_address', old('staff_temporary_address',isset($staff['staff_temporary_address']) ? $staff['staff_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_address'), 'id' => 'staff_temporary_address', 'rows'=> 2]) !!}
								</div>
							</div>
						</div>
						@endif
						<div class="row col-lg-12 p-0 m-0">
							
							@if(in_array("staff_temporary_county", $job_form_fields))
							<div class="col-lg-3 p-0">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_temporary_county') !!} :</lable> 
									{!!Form::select('staff_temporary_county', $staff['arr_country'],isset($staff['staff_temporary_county']) ? $staff['staff_temporary_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_county' , 'onChange'=> 'showStates(this.value,"staff_temporary_state")'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_temporary_state", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_temporary_state') !!} :</lable> 
									{!!Form::select('staff_temporary_state', $staff['arr_state_t'],isset($staff['staff_temporary_state']) ? $staff['staff_temporary_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_state', 'onChange'=> 'showCity(this.value,"staff_temporary_city")'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_temporary_city", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_temporary_city') !!} :</lable>
									{!!Form::select('staff_temporary_city', $staff['arr_city_t'],isset($staff['staff_temporary_city']) ? $staff['staff_temporary_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_city'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_temporary_pincode", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_temporary_pincode') !!} :</lable>
									{!! Form::text('staff_temporary_pincode', old('staff_temporary_pincode',isset($staff['staff_temporary_pincode']) ? $staff['staff_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_pincode'), 'id' => 'staff_temporary_pincode', 'maxlength'=> '6']) !!}
								</div>
							</div>
							@endif
						</div>
					</div>

					@if(in_array("staff_permanent_address", $job_form_fields) || in_array("staff_permanent_county", $job_form_fields) || in_array("staff_permanent_state", $job_form_fields) || in_array("staff_permanent_city", $job_form_fields) || in_array("staff_permanent_pincode", $job_form_fields) )
					<div class="row col-lg-12">
						<h5 class="heading-style">Permanent Address: </h5>
					</div>
					@endif

					<div id="permanent_address_details">
						@if(in_array("staff_permanent_address", $job_form_fields))
						<div class="row col-lg-12 p-0 m-0">
							<div class="col-lg-12 p-0">
								<div class="form-group">
								<lable class="from_one1">{!! trans('language.staff_permanent_address') !!} <span class="red-text">*</span> :</lable> 
								{!! Form::textarea('staff_permanent_address', old('staff_permanent_address',isset($staff['staff_permanent_address']) ? $staff['staff_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_address'), 'id' => 'staff_permanent_address', 'rows'=> 2]) !!}
								</div>
							</div>
						</div>
						@endif
						<div class="row col-lg-12 p-0 m-0">

							@if(in_array("staff_permanent_county", $job_form_fields))
							<div class="col-lg-3 p-0">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_permanent_county') !!}  <span class="red-text">*</span>  :</lable>
									{!!Form::select('staff_permanent_county', $staff['arr_country'],isset($staff['staff_permanent_county']) ? $staff['staff_permanent_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_county' , 'onChange'=> 'showStates(this.value,"staff_permanent_state")'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_permanent_state", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_permanent_state') !!} <span class="red-text">*</span>  :</lable> 
									{!!Form::select('staff_permanent_state', $staff['arr_state_p'],isset($staff['staff_permanent_state']) ? $staff['staff_permanent_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_state', 'onChange'=> 'showCity(this.value,"staff_permanent_city")'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_permanent_city", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_permanent_city') !!} <span class="red-text">*</span>  :</lable>
									{!!Form::select('staff_permanent_city', $staff['arr_city_p'],isset($staff['staff_permanent_city']) ? $staff['staff_permanent_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_city'])!!}
                					<i class="arrow double"></i>
								</div>
							</div>
							@endif

							@if(in_array("staff_permanent_pincode", $job_form_fields))
							<div class="col-lg-3" style="padding-right: 0px;">
								<div class="form-group">
									<lable class="from_one1">{!! trans('language.staff_permanent_pincode') !!} <span class="red-text">*</span> :</lable> 
									{!! Form::text('staff_permanent_pincode', old('staff_permanent_pincode',isset($staff['staff_permanent_pincode']) ? $staff['staff_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_pincode'), 'id' => 'staff_permanent_pincode', 'maxlength'=> '6']) !!}
								</div>
							</div>
							@endif
						</div>
					</div>
					<!-- <div class="row">
						<div class="col-lg-12">
							<div class="checkbox" id="">
	                    		<input id="checkbox4" type="checkbox" name="declare" required="required" class="form-control">
	                    		<label for="checkbox4">I have entered all the Details are Correct</label>
	                		</div>
						</div>
					</div> -->

					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<lable class="from_one1">Resume <span class="red-text">*</span> : </lable> 
								<input type='file' id="staff_resume" name="staff_resume"/>
							</div>
						</div>
					</div>

					@if($job_form->form_information != "" )
						<div class="row">
							<div class="col-lg-12">
								<h5 class="heading-style">Information: </h5>
							</div>
							<div class="col-lg-12">
								<div class="checkbox" id="">
		                    		{{ $job_form->form_information }}
		                		</div>
							</div>
						</div>
					@endif

					@if($job_form->form_instruction != "" )
						<div class="row">
							<div class="col-lg-12">
								<h5 class="heading-style">Instruction: </h5>
							</div>
							<div class="col-lg-12">
								<div class="checkbox" id="">
		                    		{{ $job_form->form_instruction }}
		                		</div>
							</div>
						</div>
					@endif

					
					<div class="row">
						<div class="col-lg-12 text-center">
							<input type="submit" class="btn btn-raised btn-primary" title="Submit" value="Submit">
							<input type="reset" class="btn btn-raised" title="reset" value="Reset">
						</div>
					</div>
				
				</div>
			</div>
		</div>
		</div>
		{!! Form::close() !!}
	</div>





{!! Html::script('public/admin/assets/plugins/jquery-validation/jquery.validate.js') !!}
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
{!! Html::script('public/admin/assets/js/pages/forms/form-validation.js') !!}
{!! Html::script('public/admin/assets/js/select2.full.js') !!}


</body>
</html>

<!--  <script type="text/javascript">
	 $('#dob').bootstrapMaterialDatePicker({
      weekStart: 0, format: 'DD/MM/YYYY',time : false
      });
	 $('#tc_date').bootstrapMaterialDatePicker({
      weekStart: 0, format: 'DD/MM/YYYY',time : false
      });
	 $('input[name="sameAddress"]').click(function () {
	 	if ($(this).is(':checked')) 
	 	{
	 		$('#permanent_address_details').hide();
	 	}
	 	else
	 	{
	 		$('#permanent_address_details').show();
	 	}
	 });
</script> -->

<script type="text/javascript">

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function(e) {
	            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
	            $('#imagePreview').hide();
	            $('#imagePreview').fadeIn(650);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#imageUpload").change(function() {
	    readURL(this);
	});


    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#recruitment-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_profile_img: {
                	required: true,
                    extension: 'jpg,png,jpeg,gif',
                },
                staff_resume:{
                	required: true,
                	extension: 'pdf,doc',
                },
                staff_aadhar: {
                    required: true,
                    minlength: 12,
                },
                staff_UAN: {
                    minlength: 12,
                },
                designation_id: {
                    required: true
                },
                staff_role_id: {
                    required: true
                },
                declare: {
                    required: true
                },
                staff_email: {
                    // required: true,
                    email: true,
                    emailvalid: true
                },
                staff_mobile_number: {
                    required: true,
                    digits: true
                },
                staff_dob: {
                    required: true,
                    date: true,
                    minDate: true,
                },
                staff_father_name_husband_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_father_husband_mobile_no: {
                    required: true,
                    digits: true
                },
                staff_mother_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_marital_status: {
                    required: true
                },
                caste_id: {
                    required: true
                },
                nationality_id: {
                    required: true
                },
                religion_id: {
                    required: true
                },
                staff_qualification: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_specialization: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_permanent_address: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_permanent_county: {
                    required: true
                },
                staff_permanent_state: {
                    required: true
                },
                staff_permanent_city: {
                    required: true
                },
                staff_permanent_pincode: {
                    digits: true,
                    required: true
                },
                staff_temporary_pincode: {
                    digits: true
                },
                staff_gender: {
                    required: true
                },
                title_id: {
                    required: true
                },
                shift_ids: {
                    // required: true
                },
                teaching_status: {
                    required: true
                }
                
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

        // d = new Date();
        // d.setFullYear(d.getFullYear() - 10);
        // $('#staff_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false,maxDate : d }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        
    });


    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b);
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} :</lable><div class="form-group "><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1 select2" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]">@foreach ($staff["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.staff_document_file")}}</span><label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][staff_document_file]" id="file'+arr_key+'" ><input type="hidden" class="gui-input" id="staff_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.staff_document_details") !!} :</lable><div class="form-group"><textarea name="documents['+arr_key+'][staff_document_details]" class="form-control" placeholder="{{trans("language.staff_document_details")}}" id="staff_document_details'+arr_key+'" rows="2" ></textarea></div> </div></div></div>');
        $("#document_category_id"+arr_key).select2();
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function addExpBlock() {
        var a = parseInt(document.getElementById('exp_hide').value);
        document.getElementById('exp_hide').value = a+1;
        var b = document.getElementById('exp_hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#exp-body").append('<div id="exp_block'+arr_key+'" ><div class="row clearfix"> <div class="col-lg-10 col-md-10 text_area_desc"><lable class="from_one1">{!! trans('language.staff_experience') !!} <span class="red-text">*</span> :</lable><div class="form-group"> {!! Form::textarea('staff_experience[]', '', ['class' => 'form-control','placeholder'=>trans('language.staff_experience'),  'rows'=> 1, 'required']) !!}</div> </div><div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_exp_block('+arr_key+')" style="margin-top: 30px !important;">Remove</button></div></div></div>');
        
    }
    function remove_exp_block(id){
        $("#exp_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>