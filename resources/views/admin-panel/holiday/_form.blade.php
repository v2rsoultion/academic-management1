{!! Form::hidden('holiday_id',old('holiday_id',isset($holiday['holiday_id']) ? $holiday['holiday_id'] : ''),['class' => 'gui-input', 'id' => 'holiday_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3"">
        <lable class="from_one1">{!! trans('language.holiday_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('holiday_name', old('holiday_name',isset($holiday['holiday_name']) ? $holiday['holiday_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.holiday_name'), 'id' => 'holiday_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.holiday_for') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('holiday_for', $holiday['arr_holiday_for'],isset($holiday['holiday_for']) ? $holiday['holiday_for'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'holiday_for'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.holiday_start_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('holiday_start_date', old('holiday_start_date',isset($holiday['holiday_start_date']) ? $holiday['holiday_start_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.holiday_start_date'), 'id' => 'holiday_start_date']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.holiday_end_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('holiday_end_date', old('holiday_end_date',isset($holiday['holiday_end_date']) ? $holiday['holiday_end_date']: ''), ['class' => 'form-control ','placeholder'=>trans('language.holiday_end_date'), 'id' => 'holiday_end_date']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        // return this.optional(element) || /^[0-9a-zA-Z_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) >= Number($(params).val()) ); 
        },'End Date must be greater than start date or equal to start .');

        $.validator.addMethod("minDate", function(value, element) {
            var curDate = new Date();
            var inputDate = new Date(value);
            if (inputDate >= curDate)
                return true;
            return false;
        }, "Please select future dates");

        $("#holiday-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                holiday_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                holiday_for: {
                    required: true
                },
                holiday_start_date: {
                    required: true,
                    date: true,
                    minDate: true,
                    maxlength:10
                },
                holiday_end_date: {
                    required: true,
                    date: true,
                    maxlength:10,
                    greaterThan: "#holiday_start_date"
                },
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        // $('#holiday_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false,minDate : new Date() }).on('change', function(e, date){ $(this).valid(); });
        // $('#holiday_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false,minDate : new Date()}).on('change', function(e, date) {  
        //     $('#holiday_end_date').val(''); 
        //     $(this).valid();  
        //     $('#holiday_end_date').bootstrapMaterialDatePicker('setMinDate', date);  
        // });
    });
</script>