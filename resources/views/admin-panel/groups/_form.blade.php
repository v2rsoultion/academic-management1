@if(isset($group['st_parent_group_id']) && !empty($group['st_parent_group_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('st_parent_group_id',old('st_parent_group_id',isset($group['st_parent_group_id']) ? $group['st_parent_group_id'] : ''),['class' => 'gui-input', 'id' => 'st_parent_group_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.group_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('group_name', old('group_name',isset($group['group_name']) ? $group['group_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.group_name'), 'id' => 'group_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.flag') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('flag', $group['group_type'],isset($group['flag']) ? $group['flag'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'flag'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('description',old('description',isset($group['description']) ? $group['description']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.description'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/student') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#group-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                group_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                flag: {
                    required: true,
                },
                description: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>