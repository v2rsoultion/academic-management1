{!! Form::hidden('shift_id',old('shift_id',isset($shift['shift_id']) ? $shift['shift_id'] : ''),['class' => 'gui-input', 'id' => 'shift_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.shift_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('shift_name', old('shift_name',isset($shift['shift_name']) ? $shift['shift_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.shift_name'), 'id' => 'shift_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.shift_start_time') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::time('shift_start_time', old('shift_start_time',isset($shift['shift_start_time']) ? $shift['shift_start_time']: ''), ['class' => 'form-control time','placeholder'=>trans('language.shift_start_time'), 'id' => 'shift_start_time']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.shift_end_time') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::time('shift_end_time', old('shift_end_time',isset($shift['shift_end_time']) ? $shift['shift_end_time']: ''), ['class' => 'form-control time','placeholder'=>trans('language.shift_end_time'), 'id' => 'shift_end_time']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        
        jQuery.validator.addMethod("greaterThanTime",  function(value, element) {
            
            var start_time = $("#shift_start_time").val(); //start time
            var end_time = $("#shift_end_time").val();  //end time

            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt || stt  ==  endt) {
                return false;
            }
            return true;
        },'End time must be greater than start time.');

        jQuery.validator.addMethod("lessThanTime",  function(value, element) {
            
            var start_time = $("#shift_start_time").val(); //start time
            var end_time = $("#shift_end_time").val();  //end time
            console.log(end_time);
            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt && end_time != '' ) {
                return false;
            }
            return true;
        },'Start time must be less than end time.');

        $("#shift-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules   ------------------------------------------ */
            rules: {
                shift_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                shift_start_time: {
                    required: true,
                    time: true,
                    lessThanTime: true
                },
                shift_end_time: {
                    required: true,
                    time: true,
                    greaterThanTime: true
                },
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
       
        //Added later by Pratyush on 18 July 2018
        // $('#shift_start_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        // $('#shift_end_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        
    });

    

</script>