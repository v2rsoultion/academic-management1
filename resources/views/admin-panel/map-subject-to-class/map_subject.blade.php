@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.map_subject') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/class-subject-mapping/view-subject-class-mapping') !!}">{!! trans('language.map_subject_to_class') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.map_subject') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                                <span class="classname_padding">Class Name : <strong>{{$class_name}}</strong></span>
                                {!! Form::open(['files'=>TRUE,'id' => 'class-teacher-allocation-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                {!! Form::hidden('class_id',$class_id,array('readonly' => 'true')) !!}
                                
                                @if ($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }}</br >
                                    @endforeach
                                </div>
                                @endif
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="subject-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>
                                            <div class="checkbox" id="customid">
                                                <input type="checkbox" id="check_all" class="check" >
                                                <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
                                            </div> Select Subjects
                                            </th>
                                            <th>{{trans('language.ms_subject_name')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                                </div>
                                <div class="container-fluid">
                                    <div class="row ">
                                            {!! Form::submit('Apply', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button3','name'=>'save']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#subject-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            pageLength: '100',
            ajax: {
                url: "{{url('admin-panel/class-subject-mapping/map-subject-data')}}",
                data: function (d) {
                    d.class_id = $('input[name=class_id]').val();
                }
            },
            // ajax: '{{url('admin-panel/class-subject-mapping/map-subject-data')}}',
            
            columns: [
                {data: 'checkbox', name: 'checkbox' },
                {data: 'subjects', name: 'subjects'},
            ],
             columnDefs: [
                {
                "targets": 0,
                "orderable": false
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            console.log(flag);
            $("#check_all").prop("checked", flag);
        });
        
    });


</script>
@endsection