<option value="">Select Group</option>
@if(!empty($groups))
  @foreach($groups as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif