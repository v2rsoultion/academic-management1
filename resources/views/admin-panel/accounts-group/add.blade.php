@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
 <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.account_group') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account') !!}">{!! trans('language.menu_account') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account/manage-accounts-group') !!}">{!! trans('language.account_group') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_acc_group' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.accounts-group._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row" >
          <div class="col-lg-3 col-md-3">
            <div class="form-group m-bottom-0">
              <label class=" field select size">
                {!!Form::select('s_acc_sub_head_id', $acc_group['arr_heads'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_acc_sub_head_id'])!!}
                <i class="arrow double"></i>
              </label>
            </div>
          </div>
          
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_acc_group','',['class' => 'form-control','placeholder'=> trans('language.group_name'), 'id' => 's_acc_group']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="acc-group-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.s_no') !!}</th>
              <th>{!! trans('language.main_head') !!}</th>
              <th>{!! trans('language.group_name') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#acc-group-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/account/manage-accounts-group-view')}}",
              data: function (f) {
                  f.s_acc_sub_head_id = $('#s_acc_sub_head_id').val();
                  f.s_acc_group = $('#s_acc_group').val();
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'head_name', name: 'main_head'},
            {data: 'acc_group', name: 'group_name'},
            {data: 'action', name: 'action'}
        ],
        columnDefs: [
          {
            "targets": 0, // your case first column
            "width": "5%"
          },
          {
            "targets": 1, // your case first column
            "width": "10%"
          },
          {
            "targets": 2, // your case first column
            "width": "12%"
          },
          {
            "targets": 3, // your case first column
            "width": "15%"
          }
        ]  
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });

   
</script>
@endsection