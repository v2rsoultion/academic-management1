<option value="">Select Group</option>
@if(!empty($groups))
  @foreach($groups as $key => $value)
    @foreach($value as $keyD => $valueD)
    <option value="{{ $keyD }}">{{ $valueD }}</option>
    @endforeach
  @endforeach
@endif