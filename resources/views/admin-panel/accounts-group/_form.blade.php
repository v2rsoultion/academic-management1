@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row">

  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.main_head') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('acc_sub_head_id', $acc_group['arr_heads'], isset($acc_group['acc_sub_head_id']) ? $acc_group['acc_sub_head_id']: '', ['class' => 'form-control show-tick select_form1 select2','id'=>'acc_sub_head_id'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.group_name') !!}</lable>
      {!! Form::text('acc_group',isset($acc_group->acc_group) ? $acc_group->acc_group : '',['class' => 'form-control','placeholder'=> trans('language.group_name'), 'id' => 'acc_group']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/account/manage-accounts-group') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_acc_group").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                acc_sub_head_id: {
                    required:true
                },
                acc_group: {
                    required:true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

  

</script>