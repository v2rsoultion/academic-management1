@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.esi_setting') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-esi-setting') !!}">{!! trans('language.esi_setting') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_esi_setting' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-esi-setting._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
    <!--  DataTable for view Records  -->
  <div class="container-fluid">
    <div class="card">
      <div class="body form-gap">
        @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <div class="table-responsive">
          <table class="table m-b-0 c_list" id="esi_setting_table" style="width:100%">
            {{ csrf_field() }}
            <thead>
              <tr>
                <th>{!! trans('language.s_no') !!}</th>
                <th>{!! trans('language.employee') !!}</th>
                <th>{!! trans('language.employer') !!}</th>
                <th>{!! trans('language.esi_cut_off') !!}</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody> </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#esi_setting_table').DataTable({
      serverSide: true,
      searching: false, 
      paging: false, 
      info: false,
      ajax: {
          url:"{{ url('admin-panel/payroll/manage-esi-setting-view')}}"
          // data: function (f) {
          //     f.s_dept_name = $('#s_dept_name').val();
          // }
      },
      columns: [

        {data: 'DT_RowIndex', name: 'DT_RowIndex' },
        {data: 'employee', name: 'employee'},
        {data: 'employer', name: 'employer'},
        {data: 'esi_cut_off', name: 'esi_cut_off'},
        {data: 'action', name: 'action'}
    ],
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "5%"
            },
            {
                "targets": 1, // your case first column
                "width": "14%"
            },
            {
                "targets": 2, // your case first column
                "width": "17%"
            },
            {
                "targets": 3, // your case first column
                "width": "12%"
            },
            {
                "targets": 4, // your case first column
                "width": "15%"
            },
            {
                targets: [ 0, 1, 2, 3, 4],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $('#clearBtn').click(function(){
        location.reload();
    });
  });
</script>
@endsection