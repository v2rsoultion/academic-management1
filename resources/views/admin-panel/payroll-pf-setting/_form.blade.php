@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.pf_month') !!}</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select size">
            {!!Form::select('pf_month',$pf_setting['month'],isset($pf_setting['pf_month']) ? $pf_setting['pf_month'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'pf_month'])!!}
            <i class="arrow double"></i>
        </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.pf_year') !!}</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select size">
            {!!Form::select('pf_year',$pf_setting['year'],isset($pf_setting['pf_year']) ? $pf_setting['pf_year'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'pf_year'])!!}
            <i class="arrow double"></i>
        </label>
        </div>
    </div>
</div>
<hr class="row col-lg-12 padding-0" style="margin-bottom: -1px;width: 100%;margin-left: 0px;">
<div class="row" style="border-left: 1px solid #e6e6e6; margin-left: 0px;">
    <div class="row col-lg-12" style="border-right: 1px solid #e6e6e6;">
        <div class="row col-lg-12">
            <div class="headingcommon  col-lg-12">Provident Fund</div>
            <div class="row col-lg-6">
                <div class="col-lg-4 m_top_gap">
                    <lable class="from_one1">{!! trans('language.epf_a') !!}</lable>
                </div>
                <div class="col-lg-6 padding-0">
                    <div class="form-group">
                        {!! Form::number('epf_a',isset($pf_setting['epf_a']) ? $pf_setting['epf_a']: '',['class' => 'form-control','placeholder' => trans('language.pf_amount'), 'id' => 'epf_a', 'min' => 0, 'max' => 100 ,'onkeyup' => 'calculate()']) !!}
                    </div>
                </div>  
            </div>
            <div class="row col-lg-6">
                <div class="col-lg-6 text-right m_top_gap">
                    <lable class="from_one1">{!! trans('language.pf_cut_off') !!}</lable>
                </div>
                <div class="col-lg-6 padding-0">
                    <div class="form-group">
                        {!! Form::number('pf_cut_off',old('pf_cuff_off',isset($pf_setting['pf_cuff_off']) ? $pf_setting['pf_cuff_off']: ''),['class' => 'form-control','placeholder' => trans('language.pf_amount'), 'id' => 'pf_cut_off', 'min' => 0]) !!}
                    </div>
                </div>  
            </div>
        </div>
        <hr class="col-lg-12" style="padding-right: 0px; margin-bottom: -1px;">
        <div class="row col-lg-6" style="border-right: 1px solid #e6e6e6;">
            <div class=" col-lg-12">
                <div style="font-weight: bold; font-size: 14px; margin: 10px 0px;">Employer's Share (%)</div>
            </div>
            <div class="row col-lg-12">
                <div class="col-lg-7 m_top_gap">
                    <lable class="from_one1">{!! trans('language.pension_fund_b') !!}</lable>
                </div>
                <div class="col-lg-5 padding-0">
                    <div class="form-group">
                        {!! Form::number('pension_fund_b',isset($pf_setting['pension_fund_b']) ? $pf_setting['pension_fund_b']: '',['class' => 'form-control', 'id' => 'pension_fund_b', 'min' => 0, 'onkeyup' => 'calculate()']) !!}
                    </div>
                </div> 
            </div>
            <div class="row col-lg-12">
                <div class="col-lg-7 m_top_gap">
                    <lable class="from_one1">{!! trans('language.epf_ab') !!}</lable>
                </div>
                <div class="col-lg-5 padding-0">
                    <div class="form-group">
                        <input type="hidden" name="epf_fund" class="calculate_fund">
                        {!! Form::number('epf_ab',old('epf_a_b',isset($pf_setting['epf_a_b']) ? $pf_setting['epf_a_b'] : ''),['class' => 'form-control', 'id' => 'epf_ab', 'disabled']) !!}
                    </div>
                </div> 
            </div>
        </div>
        <div class="col-lg-6" style="margin-top: 10px;">
            <div class="row col-lg-12">
                <div class="col-lg-6 m_top_gap">
                    <lable class="from_one1">{!! trans('language.acc_no_02') !!}</lable>
                </div>
                <div class="col-lg-6 padding-0">
                    <div class="form-group">
                        {!! Form::number('acc_no_02',old('acc_no_02',isset($pf_setting['acc_no_02']) ? $pf_setting['acc_no_02'] : ''),['class' => 'form-control', 'id' => 'acc_no_02', 'min' => 0, 'max' => 100]) !!}
                    </div>
                </div> 
            </div>
            <div class="row col-lg-12">
                <div class="col-lg-6 m_top_gap">
                    <lable class="from_one1">{!! trans('language.acc_no_21') !!}</lable>
                </div>
                <div class="col-lg-6 padding-0">
                    <div class="form-group">
                        {!! Form::number('acc_no_21',old('acc_no_21',isset($pf_setting['acc_no_21']) ? $pf_setting['acc_no_21'] : ''),['class' => 'form-control', 'id' => 'acc_no_21', 'min' => 0, 'max' => 100]) !!}
                    </div>
                </div> 
            </div>
            <div class="row col-lg-12">
                <div class="col-lg-6 m_top_gap">
                    <lable class="from_one1">{!! trans('language.acc_no_22') !!}</lable>
                </div>
                <div class="col-lg-6 padding-0">
                    <div class="form-group">
                        {!! Form::number('acc_no_22',old('acc_no_22',isset($pf_setting['acc_no_22']) ? $pf_setting['acc_no_22'] : ''),['class' => 'form-control', 'id' => 'acc_no_22', 'min' => 0, 'max' => 100]) !!}
                    </div>
                </div> 
            </div>
        </div>
        <hr class="col-lg-12" style="padding-right: 0px; margin-top: -1px;margin-bottom: -1px;">
        <div class="row col-lg-4" style="border-right: 1px solid #e6e6e6;">
            <div class=" col-lg-12">
                <div style="font-size: 14px; margin: 10px 0px;">Round Off</div>
            </div>
            @php
            $check1 = $check0 = '';
            if($pf_setting['round_off'] == 1)
            {
                $check1='checked';
            } else 
            {
                $check0='checked';
            }
            @endphp
            <div class="row col-lg-12">
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="radio" style="margin-top:6px !important;">
                            <input id="radio32" name="round_off" type="radio" value="0" <?php echo $check0; ?>>
                            <label for="radio32" class="document_staff">Nearest Rupee</label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="radio" style="margin-top:6px !important;">
                            <input id="radio33" name="round_off" type="radio" value="1" <?php echo $check1; ?>>
                            <label for="radio33" class="document_staff">Higher Rupee</label>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="row col-lg-12 padding-0" style="margin-top: -1px; width: 100%;margin-left: 0px;">
<div class="row col-lg-12 padding-0">
    <div class="col-lg-1">
        <button class="btn btn-raised btn-primary">Save</button>
    </div>
    <div class="col-lg-1 text-right">
    <a href="{{ url('admin-panel/payroll/manage-pf-setting') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
        $('#manage_pf_setting').trigger("reset");
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("numbersonly", function(value, element) {
          return this.optional(element) || /^[0-9]+[.]?[0-9]+$/i.test(value);
        }, "Please use only number values");

        $("#manage_pf_setting").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                pf_month: {
                    required: true
                },
                pf_year: {
                    required: true
                },
                epf_a: {
                    required: true
                },
                pf_cut_off: {
                    required: true
                },
                pension_fund_b: {
                    required: true
                },
                epf_ab: {
                    required: true
                },
                acc_no_02: {
                    required: true
                },
                acc_no_21: {
                    required: true
                },
                acc_no_22: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });

    function calculate() {
        var $a = $('#epf_a').val();
        var $b = $('#pension_fund_b').val();
        var $c = ($a - $b).toFixed(2);
        $('#epf_ab').val($c);
        $('.calculate_fund').val($c);
    }
</script>