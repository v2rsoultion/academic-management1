@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.search_button4{
    margin-left: 20px !important;
}
</style>

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h2>{!! trans('language.manage_student') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport/vehicle/manage-student-staff') !!}">{!! trans('language.manage_student_staff') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_student') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Vehicle</strong> Information 
                                </h2>

                                <div class="std1" style="margin-bottom: -30px;">
                                    <div class="profile_detail_3">
                                        <label> <span>Vehicle Name : </span> {{ $vehicle->vehicle_name }}</label>
                                    </div>
                                    <div class="profile_detail_3">

                                        <label> <span>Driver - Conductor : </span>
                                            @if($vehicle['getDriver']->staff_name != "")
                                                {{ $vehicle['getDriver']->staff_name }}
                                            @else 
                                                Not Available
                                            @endif

                                            -

                                            @if($vehicle['getConductor']->staff_name != "")
                                                {{ $vehicle['getConductor']->staff_name }}
                                            @else 
                                                Not Available
                                            @endif
                                        </label>
                                    </div>
                                </div> 

                            </div>
                            <div class="clearfix"></div>
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        {!! Form::hidden('vehicle_id',old('vehicle_id',isset($vehicle['vehicle_id']) ? $vehicle['vehicle_id'] : ''),['class' => 'gui-input', 'id' => 'vehicle_id', 'readonly' => 'true']) !!}
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id' ,'onChange' => 'checkSearch()'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                                <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
                                <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
                                <div id="student-grid" style="display: none">

                                <!-- <div class="table-responsive">
                                    <table class="table m-b-0 c_list " id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.student_name')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{{trans('language.student_father_name')}}</th>
                                                <th>{{trans('language.father_contact')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div> -->
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
    </div>
    
</section>


<div class="modal fade" id="manage_student" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"> Register Student </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        
            <div class="" style="margin-left: 10px;">

                {!! Form::open(['files'=>TRUE,'id' => 'manage-student-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}

                <div class="table m-b-0 c_list" id="student-data">
                    {{ csrf_field() }}
                </div>
            
                <div class="container-fluid">
                    <div class="row ">
                            {!! Form::submit('Apply', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            
            </div>  
        </div>
    </div>
</div>



<script>

    function main_route(route_id){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/transport/vehicle/get-manage-route-location')}}",
            type: 'POST',
            data: {
                'route_id': route_id,
            },
            success: function (data) {
                $('#route_location_id').html(data.data);
            }
        });
    }


    $(document).on('click','.register_student',function(e){
        var student_id = $(this).attr('student_id');
        var vehicle_id = $('#vehicle_id').val(); 
        var type = 1;

        $('#manage_student').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/transport/vehicle/get-manage-route')}}",
            type: 'POST',
            data: {
                'student_id': student_id,
                'vehicle_id': vehicle_id,
                'type': type,
            },
            success: function (data) {

                $('#student-data').html(data.data);
                $('.select2').select2();
            }
        });

    });


    $(document).ready(function() {
        $('.select2').select2();

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $('#search-form').on('submit', function(e) {
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        var vehicle_id = $("#vehicle_id").val();

        $(".mycustloading").show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin-panel/transport/vehicle/student-list-data')}}',
            type: 'GET',
            data: {
                'class_id': class_id,
                'section_id' : section_id,
                'vehicle_id' : vehicle_id
            },
            success: function (data) {
                $("#student-grid").css("display", "block");
                $("#student-grid").html(data);
                $(".mycustloading").hide();
                
            }
        });
        e.preventDefault();
    });

    // $('#search-form').on('submit', function(e) {
    //     var table = $('#student-table').DataTable({
    //         //dom: 'Blfrtip',
    //         pageLength: 20,
    //         processing: true,
    //         serverSide: true,
    //         bLengthChange: false,
    //         ajax: {
    //             url: '{{url('admin-panel/transport/vehicle/student-list-data')}}',
    //             data: function (d) {
    //                 d.class_id = $('select[name="class_id"]').val();
    //                 d.section_id = $('select[name="section_id"]').val();
    //             }
    //         },
    //         columns: [
    //             {data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //             {data: 'student_name', name: 'student_name'},
    //             {data: 'student_enroll_number', name: 'student_enroll_number'},
    //             {data: 'student_father_name', name: 'student_father_name'},
    //             {data: 'student_father_mobile_number', name: 'student_father_mobile_number'},
    //             {data: 'action', name: 'action'},
    //         ],
    //         //  columnDefs: [
    //         //     {
    //         //         "targets": 0, // your case first column
    //         //         "width": "15%"
    //         //     },
    //         //     {
    //         //         "targets": 3, // your case first column
    //         //         "width": "20%"
    //         //     },
    //         //     {
    //         //         targets: [ 0, 1, 2, 3 ],
    //         //         className: 'mdl-data-table__cell--non-numeric'
    //         //     }
    //         // ]
    //     });
    //     $('#search-form').on('submit', function(e) {
    //         table.draw();
    //         e.preventDefault();
    //     });
    //     $('#clearBtn').click(function(){
    //         document.getElementById('search-form').reset();
    //         table.draw();
    //         e.preventDefault();
    //     })
    // });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(class_id == "" || section_id == ""){
            $(':input[name="Search"]').prop('disabled', true);
            $("#student-grid").css("display", "none");
        } else {
            $("#section_id").val(section_id);
            $("#class_id").val(class_id);
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }


    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }
    

</script>
@endsection



