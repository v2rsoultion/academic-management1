@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_vehicle') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/transport/vehicle/add-vehicle') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_vehicle') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_name', old('vehicle_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.vehicle_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_registration_no', old('vehicle_registration_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.vehicle_registration_number'), 'id' => 'vehicle_registration_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('vehicle_ownership', $listData['vehicle_ownership'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'vehicle_ownership'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('contact_person', old('contact_person', ''), ['class' => 'form-control ','placeholder'=>trans('language.contact_person'), 'id' => 'contact_person']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="vehicle-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.vehicle_name')}}</th>
                                                <th>{{trans('language.vehicle_registration_number')}}</th>
                                                <th>{{trans('language.vehicle_ownership')}}</th>
                                                <th>{{trans('language.vehicle_capacity')}}</th>
                                                <th>{{trans('language.person_info')}}</th>
                                                <th>{{trans('language.total_documents')}}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="infoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Document Information </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
        <div class="table-responsive" style="margin-left: 10px;">
            <table class="table m-b-0 c_list" id="vehicle-document-table" style="width:97%">
            {{ csrf_field() }}
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{{trans('language.vehicle_document_category')}}</th>
                        <th>{{trans('language.vehicle_document_file')}}</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>  
    </div>
  </div>
</div>


<div class="modal fade" id="mapping" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"> Assign Driver / Conductor </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
        <div class="" style="margin-left: 10px;">

            {!! Form::open(['files'=>TRUE,'id' => 'mapping-route-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}

                <div class="table m-b-0 c_list" id="map-data">
                    {{ csrf_field() }}
                </div>
            
                <div class="container-fluid">
                    <div class="row ">
                            {!! Form::submit('Apply', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            
        </div>  
    </div>
  </div>
</div>


<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
        var table = $('#vehicle-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/data')}}',
                data: function (d) {
                    d.vehicle_name = $('input[name="vehicle_name"]').val();
                    d.vehicle_registration_no = $('input[name="vehicle_registration_no"]').val();
                    d.vehicle_ownership = $('select[name="vehicle_ownership"]').val();
                    d.contact_person = $('input[name="contact_person"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'vehicle_name', name: 'vehicle_name'},
                {data: 'vehicle_registration_no', name: 'vehicle_registration_no'},
                {data: 'vehicle_ownership', name: 'vehicle_ownership'},
                {data: 'vehicle_capacity', name: 'vehicle_capacity'},
                {data: 'person_info', name: 'person_info'},
                {data: 'total_documents', name: 'total_documents'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "20%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "24%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    
    $(document).on('click','.vehicle',function(e){
        var vehicle_document_id = $(this).attr('vehicle_document_id');
        $('#infoModel').modal('show');
        var table1 = $('#vehicle-document-table').DataTable({
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: "{{url('admin-panel/transport/vehicle/vehicle-documents')}}",
                data: function (d) {
                    d.vehicle_document_id = vehicle_document_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'document_category_name', name: 'document_category_name'},
                {data: 'vehicle_document_file', name: 'vehicle_document_file'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    });


    $(document).on('click','.vehicle_mapping',function(e){
        var vehicle_mapping_id = $(this).attr('vehicle_mapping_id');
        $('#mapping').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/transport/vehicle/manage-vehicle-data')}}",
            type: 'POST',
            data: {
                'vehicle_mapping_id': vehicle_mapping_id
            },
            success: function (data) {

                $('#map-data').html(data.data);
                $('.select2').select2();
            }
        });

    });


    jQuery(document).ready(function () {

        $("#mapping-route-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                route_description: {
                    required: true
                },
                route_location: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });


</script>
@endsection




