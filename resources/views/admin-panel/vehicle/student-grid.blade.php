
<div class="table-responsive">
    <table class="table m-b-0 c_list " id="student-table" style="width:100%">
    {{ csrf_field() }}
        <thead>
            <tr>
                <th>{{trans('language.s_no')}}</th>
                <th>{{trans('language.student_name')}}</th>
                <th>{{trans('language.student_enroll_number')}}</th>
                <th>{{trans('language.student_father_name')}}</th>
                <th>{{trans('language.father_contact')}}</th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($arr_student))
                
                @php $i=0; @endphp
                @foreach($arr_student as $arr_students)
                @php $i++; @endphp
                    <tr>
                        <th> {{ $i }} </th>
                        <th> {{ $arr_students['student_name'] }} </th>
                        <th> {{ $arr_students['student_enroll_number'] }} </th>
                        <th> {{ $arr_students['student_father_name'] }} </th>
                        <th> {{ $arr_students['student_father_mobile_number'] }} </th>
                        <th> 
                            <div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="zmdi zmdi-label"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                    <li style="padding-left:20px;" class="register_student" student_id={{ $arr_students['student_id'] }} > Register Student </li>
                                </ul>
                            </div> 
                        </th>
                    </tr>
                @endforeach
                
            @else
                <tr>
                <th colspan="6" style="text-align:center;"> No Record Found !! </th>
            </tr>
            @endif

        </tbody>
    </table>
</div>
