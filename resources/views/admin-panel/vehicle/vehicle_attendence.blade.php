@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.search_button4{
    margin-left: 20px !important;
}
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.vehicle_attendence') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.vehicle_attendence') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2><strong>Vehicle</strong> Details  </h2>
                            </div>
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_name', old('vehicle_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.vehicle_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}


                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="vehicle-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.vehicle_name')}}</th>
                                                <th>{{trans('language.driver_conductor_info')}}</th>
                                                <th>{{trans('language.route_info')}}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="map_views" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Map View </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding-top: 0px !important;">
                <div id="map" style="width: 100%; height: 520px;"></div>
            </div>
        </div>
    </div>
</div>


<script>
    
    $(document).ready(function () {
        var table = $('#vehicle-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/vehicle-attendance-data')}}',
                data: function (d) {
                    d.vehicle_name = $('input[name="vehicle_name"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'vehicle_name', name: 'vehicle_name'},
                {data: 'driver_coductor_info', name: 'driver_coductor_info'},
                {data: 'map_views', name: 'map_views'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "30%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });

    $('.modal').on('hidden.bs.modal', function () {
        location.reload();
    })

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH1iJIF-DtiFNs29Fogl3oSnX0YWYdhvE&libraries=places&language=en"></script>
<script type="text/javascript">

    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;

    function calculateAndDisplayRoute(start,end,wayptsnew) {

        $('#map_views').modal('show');
        
        var mapOptions = {
            center: new google.maps.LatLng(26.2703357, 72.9603313),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        directionsDisplay = new google.maps.DirectionsRenderer();
        
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
       
        directionsDisplay.setMap(map);
        
        var start = start;
        var end = end ;
        var request = {
          origin:start, 
          destination:end,
          waypoints: wayptsnew,
          travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var myRoute = response.routes[0];
            var txtDir = '';
            for (var i=0; i<myRoute.legs[0].steps.length; i++) {
              txtDir += myRoute.legs[0].steps[i].instructions+"<br />";
            }
            // document.getElementById('directions').innerHTML = txtDir;
          }
        });
    }

</script>
@endsection




