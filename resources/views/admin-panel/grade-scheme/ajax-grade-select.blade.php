<option value="">Select Grade Scheme</option>
@if(!empty($arr_schemes))
  @foreach($arr_schemes as $key => $value)
    <option @if($grade_scheme_id == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif