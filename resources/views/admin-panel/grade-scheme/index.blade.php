@extends('admin-panel.layout.header')
@section('content')

<style type="text/css">
  
</style>
<!--  Main content here -->
<section class="content contact">

  <div class="block-header">
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
              <h2>{!! trans('language.view_grade_scheme') !!}</h2>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <a href="{!! url('admin-panel/grade-scheme/add-grade-scheme') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
              <li class="breadcrumb-item">{!! trans('language.view_grade_scheme') !!}</li>
            </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                  <div class="alert alert-success" role="alert">
                                      {{ session()->get('success') }}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                @if($errors->any())
                                  <div class="alert alert-danger" role="alert">
                                      {{$errors->first()}}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                  <div class="row clearfix">
                                      
                                      <div class="col-lg-3 col-md-3">
                                          <div class="input-group ">
                                              {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.particular_name'), 'id' => 'name']) !!}
                                              <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                          </div>
                                      </div>
                                      <div class="col-lg-1 col-md-1">
                                          {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                      </div>
                                      <div class="col-lg-1 col-md-1">
                                          {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                      </div>
                                      
                                  </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="scheme-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{!! trans('language.scheme_name') !!}</th>
                                                <th>{!! trans('language.grade_type') !!}</th>
                                                <th>{!! trans('language.grades') !!}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  
</section>

<div class="modal fade" id="gradeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Grades </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="green" id="grade_success"></p>
      <table class="table m-b-0 c_list" id="grade-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.grade_name') !!}</th>
                    <th>{!! trans('language.grade_min_max') !!}</th>
                    <th>{!! trans('language.grade_point') !!}</th>
                    <th>{!! trans('language.grade_remark') !!}</th>
                    <th>Action</th>
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#scheme-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/grade-scheme/data')}}',
                data: function (d) {
                    d.name = $('input[name=name]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'scheme_name', name: 'scheme_name'},
                {data: 'grade_type', name: 'grade_type'},
                {data: 'grades', name: 'grades'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $(document).on('click','.grades',function(e){
        var grade_id = $(this).attr('grade-id');
        $('#gradeModel').modal('show');
        var table1 = $('#grade-table').DataTable({
                //dom: 'Blfrtip',
                destroy: true,
                pageLength: 10,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                ajax: {
                    url: '{{url('admin-panel/grade-scheme/grades')}}',
                    data: function (d) {
                        d.id = grade_id;
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'grade_name', name: 'grade_name'},
                    {data: 'grade_min_max', name: 'grade_min_max'},
                    {data: 'grade_point', name: 'grade_point'},
                    {data: 'grade_remark', name: 'grade_remark'},
                    {data: 'action', name: 'action'},
                ],
                columnDefs: [
                    {
                        "targets": 4, // your case first column
                        "width": "30%"
                    },
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });
    })
    $(document).on('click','.grade-records',function(e){
        var grade_id = $(this).attr('grade-record');
        var grade_scheme_id = $(this).attr('scheme-record');
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/grade-scheme/delete-grade/')}}/"+grade_id,
                success: function (data) {
                    $("#grade_success").html(data);
                    $(".mycustloading").hide();
                    var table1 = $('#grade-table').DataTable({
                        //dom: 'Blfrtip',
                        destroy: true,
                        pageLength: 10,
                        processing: true,
                        serverSide: true,
                        bLengthChange: false,
                        ajax: {
                            url: '{{url('admin-panel/grade-scheme/grades')}}',
                            data: function (d) {
                                d.id = grade_scheme_id;
                            }
                        },
                        columns: [
                            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                            {data: 'grade_name', name: 'grade_name'},
                            {data: 'grade_min_max', name: 'grade_min_max'},
                            {data: 'grade_point', name: 'grade_point'},
                            {data: 'grade_remark', name: 'grade_remark'},
                            {data: 'action', name: 'action'},
                        ],
                        columnDefs: [
                            {
                                "targets": 4, // your case first column
                                "width": "30%"
                            },
                            {
                                targets: [ 0, 1, 2 ],
                                className: 'mdl-data-table__cell--non-numeric'
                            }
                        ]
                    });
                }
            });
        }
    });
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
   
    function loadInst(inst){
        document.getElementById("inst_id").value = inst;
        
    }
</script>
@endsection