@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.receipts') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Fees</strong> Receipt  </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <div class="input-group ">
                                    {!! Form::text('receipt_number', old('receipt_number', ''), ['class' => 'form-control ','placeholder'=>trans('language.receipt_number'), 'id' => 'receipt_number']) !!}
                                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                </div>
                            </div>
                            
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="list-block" >
                            <hr>
                            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
                            <div class="table-responsive" >
                                <table class="table m-b-0 c_list " id="receipt-table" style="width:100%;">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.receipt_number')}}</th>
                                            <th>{{trans('language.student_name')}}</th>
                                            <th>{{trans('language.class_name')}}</th>
                                            <th>{{trans('language.receipt_date')}}</th>
                                            <th>{{trans('language.net_amt')}}</th>
                                            <th>{{trans('language.pay_amt')}}</th>
                                            <th>{{trans('language.pay_later_amt')}}</th>
                                            <th>{{trans('language.pay_mode')}}</th>
                                            <th>{{trans('language.concession_amt')}}</th>
                                            <th>{{trans('language.fine_amt')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
      var table = $('#receipt-table').DataTable({
          pageLength: 20,
          processing: true,
          serverSide: true,
          bLengthChange: false,
          bPaginate: true,
          bInfo : false,
          bFilter: false,
          ajax: {
              url: "{{url('admin-panel/hostel/fees-receipt-data')}}",
              data: function (d) {
                  d.receipt_number = $('input[name=receipt_number]').val();
                  d.receipt_status = 0;
              }
          },
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex' },
              {data: 'receipt_number', name: 'receipt_number'},
              {data: 'student_name', name: 'student_name'},
              {data: 'class_name', name: 'class_name'},
              {data: 'receipt_date', name: 'receipt_date'},
              {data: 'net_amt', name: 'net_amt'},
              {data: 'pay_amt', name: 'pay_amt'},
              {data: 'pay_later_amt', name: 'pay_later_amt'},
              {data: 'pay_mode', name: 'pay_mode'},
              {data: 'concession_amt', name: 'concession_amt'},
              {data: 'fine_amt', name: 'fine_amt'},
              {data: 'action', name: 'action'},
          ],
          columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": ""
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": ""
                },
                {
                    "targets": 3, // your case first column
                    "width": ""
                },
                {
                    "targets": 11, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11],
                    className: 'mdl-data-table__cell--non-numeric'
                }
          ]
      });
      $('#search-form').on('submit', function(e) {
            table.draw();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $("#error-block").hide();
            $("#success-block").hide();
          if(class_id == "" && section_id == ""){
              $("#list-block").hide();
          } else {
              $("#list-block").show();
          }
          e.preventDefault();
      });
      
    });
    
    
</script>
@endsection