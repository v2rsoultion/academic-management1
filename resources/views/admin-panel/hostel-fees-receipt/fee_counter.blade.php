@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
    }
    .theme-blush .nav-tabs .nav-link.active{
    color: #fff !important;
    border-radius: 0px !important;
    background: #6572b9 !important;
    width: 190px;
    }
    #fees_counter{
        width: 100% !important;
    }
    /* li {
    border-right: 1px solid #fff;
    }*/
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Fee Counter</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">Fee Counter</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/fee-counter/add-fee-counter') !!}">Add Fee Counter</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12" id="bodypadd">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="header">
                                
                            </div>
                            <div class="body form-gap">
                                <ul class="nav nav-tabs tabing">
                                    @php $tabCounter = 0; @endphp
                                    @foreach($students as $studentData)
                                    <li class="nav-item"><a class="nav-link @if($tabCounter == 0) active @endif" data-toggle="tab" href="#student{{$tabCounter}}"><i class="zmdi zmdi-account"></i>
                                        {!! $studentData['student_name'] !!} - {!! $studentData['current_class'].' '.$studentData['current_section'] !!}</a>
                                    </li>
                                    @php $tabCounter++; @endphp
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    @php $contentCounter = $total_fine_amt = $total_chargable_fee = $total_concession_amt = 0; @endphp
                                    @foreach($students as $studentData)
                                    <div class="tab-pane @if($contentCounter == 0) active @endif" id="student{{$contentCounter}}">
                                        <div class="row feecounterclass">
                                            <div class="col-lg-3  float-md-left">
                                                <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                                            </div>
                                            <div class="col-lg-8 feecountergap">
                                                <label><b>Enroll</b> : {!! $studentData['student_enroll_number'] !!} </label>
                                                <label><b>Name</b> : {!! $studentData['student_name'] !!}</label>
                                                <label><b>Father Name</b> : {!! $studentData['student_father_name'] !!}</label>
                                                <label><b>Class</b> : {!! $studentData['current_class'].' - '.$studentData['current_section'] !!}</label>
                                            </div>
                                        </div>
                                        <div class="row tablefee">
                                            
                                            {!! Form::open(['files'=>TRUE,'id' => 'fees_counter' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                            @if(session()->has('success'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session()->get('success') }}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if($errors->any())
                                                <div class="alert alert-danger" role="alert">
                                                    {{$errors->first()}}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                                <p class="red" id="error-block" style="display: none"></p>
                                                <p class="green" id="success-block" style="display: none"></p>
                                                <div id="table-wrapper">
                                                    <div id="table-scroll">
                                                        <table  class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Effective Date</th>
                                                                    <th>Particular</th>
                                                                    <th>Amount</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(!empty($studentData['complete_fees']))
                                                                <input type="hidden" value="{!! COUNT($studentData['complete_fees']) !!}" id="headcounter" >
                                                                <input type="hidden" value="{!! $studentData['student_id'] !!}" id="student_id" >
                                                                @foreach($studentData['complete_fees'] as $feesData)
                                                                @php
                                                                    $total_chargable_fee = $total_chargable_fee + $feesData['head_pay_amt'];
                                                                    $total_concession_amt = $total_concession_amt + $studentData['total_concession'];
                                                                @endphp
                                                                @if(!empty($feesData['fine_info']))
                                                                    @php
                                                                       $total_fine_amt = $total_fine_amt + $feesData['fine_info']['fine_detail_amt'];
                                                                    @endphp
                                                                @endif
                                                                @php 
                                                                    // echo "<pre>";
                                                                    // print_r($feesData);
                                                                    // echo "</pre>";
                                                                @endphp
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox" id="customid">
                                                                        <input id="checkbox{{$studentData['student_id']}}{{$feesData['counter']}}" class="checkboxes headchecks" counter-id="{{$studentData['student_id']}}{{$feesData['counter']}}" type="checkbox" name="pay[{{$feesData['counter']}}][head_id]" value="{{$feesData['head_id']}}" >
                                                                            <label  class="from_one1" style="margin-bottom: 4px !important;"  for="checkbox{{$studentData['student_id']}}{{$feesData['counter']}}"></label>
                                                                        </div>
                                                                    </td>
                                                                    {!! Form::hidden('pay['.$feesData['counter'].'][head_type]', $feesData['head_type'], ['id' => 'head_type'.$studentData['student_id'].$feesData['counter'].'']) !!}

                                                                    {!! Form::hidden('pay['.$feesData['counter'].'][head_inst_id]', $feesData['head_inst_id'], ['id' => 'head_inst_id'.$studentData['student_id'].$feesData['counter'].'']) !!}
        
                                                                    <td>{!! date('d M y', strtotime($feesData['head_date'])) !!}</td>
                                                                    <td>{!! $feesData['head_name'] !!}</td>
                                                                    <td>{!! $feesData['head_pay_amt'] !!}</td>
                                                                    <td class="text-center">
                                                                        <input type="number"  name="pay[{{$feesData['counter']}}][pay_amt]" class="form-control heads-amt" counter-id="{{$studentData['student_id']}}{{$feesData['counter']}}" placeholder="" value="{!! $feesData['head_pay_amt'] !!}" min="0" max="{!! $feesData['head_amt'] !!}" id="pay_amt{{$studentData['student_id']}}{{$feesData['counter']}}" disabled >

                                                                        <input type="hidden" name="pay[{{$feesData['counter']}}][head_pay_amt]" class="form-control"  placeholder="" value="{!! $feesData['head_pay_amt'] !!}" min="0" max="{!! $feesData['head_pay_amt'] !!}" id="head_pay_amt{{$studentData['student_id']}}{{$feesData['counter']}}" >


                                                                        <input type="hidden" name="pay[{{$feesData['counter']}}][pay_amt_actual]" class="form-control"  placeholder="" value="{!! $feesData['head_amt'] !!}" min="0" max="{!! $feesData['head_amt'] !!}" id="pay_amt_actual{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                        <input type="hidden" name="pay[{{$feesData['counter']}}][concession_map_id]" value="{!! $feesData['concession_map_id'] !!}"  id="concession_map_id{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                        <input type="hidden" name="pay[{{$feesData['counter']}}][concession_amt]" value="{!! $feesData['concession_amt'] !!}" id="concession_amt{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                        @if(!empty($feesData['fine_info']))
                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_status]" value="1"  id="fine_status{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_id]" value="{!! $feesData['fine_info']['fine_id'] !!}"  id="fine_id{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_detail_id]" value="{!! $feesData['fine_info']['fine_detail_id'] !!}"  id="fine_detail_id{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_detail_days]" value="{!! $feesData['fine_info']['fine_detail_days'] !!}"  id="fine_detail_days{{$studentData['student_id']}}{{$feesData['counter']}}" >

                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_detail_amt]" value="{!! $feesData['fine_info']['fine_detail_amt'] !!}"  id="fine_detail_amt{{$studentData['student_id']}}{{$feesData['counter']}}" >
                                                                        @else
                                                                            <input type="hidden" name="pay[{{$feesData['counter']}}][fine_status]" value="0"  id="fine_status{{$studentData['student_id']}}{{$feesData['counter']}}" >    
                                                                        @endif
                                                                        <p class="red" id="error_pay{{$studentData['student_id']}}{{$feesData['counter']}}" style="display: none; font-size: 11px;" >Amount should not be greater than {{$feesData['head_amt']}}</p>
                                                                    </td>
                                                                </tr>
                                                                
                                                                @endforeach
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <table class="table-borderd" width="100%" id="tableCutm">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td ><b>Total Chargeable Fee</b></td>
                                                        <td class="text-center">{!! $total_chargable_fee !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td ><b>Calculate Amount</b></td>
                                                            <td class="text-center" id="showPayAmt">0</td>
                                                            <input type="hidden" name="pay_final_calculate_amt" value="0"  id="pay_final_calculate_amt">
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td ><b>Paying Amount</b></td>
                                                            <td ><input type="text" name="pay_final_amt"  class="form-control final-pay-amt"  value="0"  id="pay_final_amt"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 200px;">
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxWallet" class="checkboxWallet checkboxes" type="checkbox" name="wallet_use" value='1'>
                                                                    <label for="checkboxWallet">Wallet - {!! $studentData['wallet_ac_amt'] !!}</label>
                                                                </div>
                                                            </td>
                                                            <td class="form-group">
                                                                <input type="hidden" id="wallet_actual_amt" name="wallet_actual_amt" class="form-control" value="" min="0" >
                                                                <input type="text" id="wallet_calculate_amt" name="wallet_calculate_amt" class="form-control" value="{!! $studentData['wallet_ac_amt'] !!}" min="0" max="{!! $studentData['wallet_ac_amt'] !!}" >
                                                            </td>
                                                            <td style="width: 200px;">
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxConcession" class="checkboxConcession checkboxes" type="checkbox" name="concession_use" value='1'>
                                                                    <label for="checkboxConcession">Concession - <span id="showConcessionAmt">0</span></label>
                                                                </div>
                                                            </td>
                                                            <td class="form-group">
                                                                <input type="text" id="concession_head_amt" name="concession_head_amt" class="form-control" value="0" min="0" disabled>
                                                                <input type="hidden" id="concession_final_amt" name="concession_final_amt" class="form-control" value="0" min="0" >
                                                            </td>

                                                            <td style="width: 200px;">
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxFine" name="fine_use" class="checkboxFine checkboxes" type="checkbox" value='1'>
                                                                    <label for="checkboxFine">Fine - <span id="showFineAmt">0</span></label>
                                                                </div>
                                                            </td>

                                                            <td class="form-group">
                                                                <input type="text" id="fine_head_amt" name="fine_head_amt" class="form-control final-fine-amt" value="0" min="0" >
                                                                <input type="hidden" id="fine_actual_amt" name="fine_actual_amt" class="form-control" value="0" min="0" >
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td >
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxPayLator" type="checkbox" name="pay_lator_use" value='1'>
                                                                    <label for="checkboxPayLator">Pay-later</label>
                                                                </div>
                                                            </td>
                                                            <td class="form-group">
                                                                <input type="text" id="pay_lator_amt" name="pay_lator_amt" class="form-control" value="0" disabled>
                                                                <input type="hidden" id="pay_lator_amt_hide" name="pay_lator_amt_hide" class="form-control" value="0" >
                                                            </td>
                                                            <td >
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxImprest" name="imprest_use" class="checkboxImprest" type="checkbox" value='1'>
                                                                    <label for="checkboxImprest"> Add Excessive in Wallet</label>
                                                                </div>
                                                            </td>
                                                            <td class="form-group">
                                                                <input type="text" id="imprest_amt" name="imprest_amt" class="form-control" value="0" disabled>
                                                                <input type="hidden" id="imprest_amt_hide" name="imprest_amt_hide" class="form-control" value="0" >
                                                            </td>
                                                            <td > Payment Mode</td>
                                                            <td class="form-group">
                                                                <select class="form-control show-tick select2 select_form1" name="pay_mode" id="pay_mode" onChange="checkType(this.value)">
                                                                    <option value=""> Select</option>
                                                                    <option value="online"> Online</option>
                                                                    <option value="offline">Offline</option>
                                                                    <option value="cheque">Cheque</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="showbank" style="display:none;">
                                                            <div >
                                                                <td colspan="2"></td>
                                                                <td  colspan="1">
                                                                <label class=" field select" style="width: 100%">
                                                                    {!!Form::select('bank_id', $studentData['arr_bank'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'bank_id'])!!}
                                                                <i class="arrow double"></i>
                                                                </label>
                                                                </td>
                                                                <td class="form-group">
                                                                    <input type="text" name="cheque_date" class="form-control" placeholder=" Cheque Date" id="cheque_date" required>
                                                                </td>
                                                                <td class="form-group">
                                                                    <input type="text" name="cheque_number" id="cheque_number" class="form-control" placeholder="Cheque No" maxlength="5" required>
                                                                </td>
                                                                <td class="form-group">
                                                                    <input type="text" name="cheque_amt" id="cheque_amt" class="form-control" placeholder="Cheque Amount" required>
                                                                    <p class="red" id="cheque-amt-block" style="display: none"></p>
                                                                </td>
                                                            </div>
                                                        </tr>
                                                        <tr id="showTrans" style="display:none;">
                                                            <div >
                                                                <td colspan="4"></td>
                                                                <td class="form-group">
                                                                    <input type="text" name="transaction_id" id="transaction_id" class="form-control" placeholder="Transaction ID" required>
                                                                </td>
                                                                <td class="form-group">
                                                                    <input type="text" name="transaction_date" class="form-control" placeholder=" Transaction Date" id="transaction_date" required> 
                                                                </td>
                                                                
                                                            </div>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td >
                                                                Select Date
                                                            </td>
                                                            <td class="form-group">
                                                                <input type="text" name="select_date" class="form-control" id="date_start" placeholder="Date">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td >
                                                                Add Description
                                                            </td>
                                                            <td class="form-group">
                                                                <textarea class="form-control" name="description" id="description "placeholder="Description"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"></td>
                                                            <td >
                                                            </td>
                                                            <td class="text-center">
                                                                <button type="submit" class="btn btn-raised btn-primary" title="Charge Fees">Charge Fees
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    @php $contentCounter++; @endphp
                                    @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    function checkType(type){
        if(type == 'cheque'){
            $('#showbank').show();
            $('#showTrans').hide();
        } else if(type == 'online'){
            $('#showbank').hide();
            $('#showTrans').show();
        } else {
            $('#showTrans').hide();
            $('#showbank').hide();
        }
    }

    $(document).on('click','.checkboxes',function(e){
        if ($(this).is(':checked')) {
            var counterVal = $(this).attr('counter-id');
            $('#pay_amt'+counterVal).prop('disabled', false);

            var headcounter = $('#headcounter').val();
            var student_id = $('#student_id').val();
            var total_pay_amt = total_fine_amt = total_concession_amt = wallet_amt = concession_amt = fine_amt =  0;
            for(i=0; i<headcounter; i++){
                if ($('#checkbox'+student_id+i).is(':checked')) {
                    
                    var fine_status = $("#fine_status"+student_id+i).val();
                    if(fine_status == 1){
                        var fine_amt = $("#fine_detail_amt"+student_id+i).val();
                        if(fine_amt != '') {
                            total_fine_amt = parseFloat(total_fine_amt) + parseFloat(fine_amt);
                        }
                    }
                    var concession_amt = $("#concession_amt"+student_id+i).val();
                    var pay_amt = $("#pay_amt"+student_id+i).val();
                    total_pay_amt = parseFloat(total_pay_amt) + parseFloat(pay_amt);
                    if(concession_amt != '') {
                        total_concession_amt = parseFloat(total_concession_amt) + parseFloat(concession_amt);
                    }
                }
            }
           
            $('#concession_head_amt').val(total_concession_amt);
            $('#concession_final_amt').val(total_concession_amt);
            $('#showConcessionAmt').html(total_concession_amt);
            $('#fine_head_amt').val(total_fine_amt);
            $('#fine_actual_amt').val(total_fine_amt);
            $('#showFineAmt').html(total_fine_amt);
            
            if ($('#checkboxWallet').is(':checked')) {
                wallet_amt = $("#wallet_calculate_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(wallet_amt);
            }

            if ($('#checkboxConcession').is(':checked')) {
                concession_amt = $("#concession_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(concession_amt);
            }

            if ($('#checkboxFine').is(':checked')) {
                fine_amt = $("#fine_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) + parseFloat(fine_amt);
            }

            $('#pay_final_calculate_amt').val(total_pay_amt);
            $('#showPayAmt').html(total_pay_amt);
        } else {
            var counterVal = $(this).attr('counter-id');
            $('#pay_amt'+counterVal).prop('disabled', true);

            var headcounter = $('#headcounter').val();
            var student_id = $('#student_id').val();
            var total_pay_amt = total_fine_amt = total_concession_amt = 0;
            for(i=0; i<headcounter; i++){
                if ($('#checkbox'+student_id+i).is(':checked')) {

                    var fine_status = $("#fine_status"+student_id+i).val();
                    if(fine_status == 1){
                        var fine_amt = $("#fine_detail_amt"+student_id+i).val();
                        if(fine_amt != '') {
                            total_fine_amt = parseFloat(total_fine_amt) + parseFloat(fine_amt);
                        }
                    }

                    var concession_amt = $("#concession_amt"+student_id+i).val();
                    var pay_amt = $("#pay_amt"+student_id+i).val();
                    total_pay_amt = parseFloat(total_pay_amt) + parseFloat(pay_amt);

                    if(concession_amt != '') {
                        total_concession_amt = parseFloat(total_concession_amt) + parseFloat(concession_amt);
                    }
                }
            }
            $('#concession_head_amt').val(total_concession_amt);
            $('#concession_final_amt').val(total_concession_amt);
            $('#showConcessionAmt').html(total_concession_amt);
            $('#fine_head_amt').val(total_fine_amt);
            $('#fine_actual_amt').val(total_fine_amt);
            $('#showFineAmt').html(total_fine_amt);

            if ($('#checkboxWallet').is(':checked')) {
                wallet_amt = $("#wallet_calculate_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(wallet_amt);
            }

            if ($('#checkboxConcession').is(':checked')) {
                concession_amt = $("#concession_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(concession_amt);
            }

            if ($('#checkboxFine').is(':checked')) {
                fine_amt = $("#fine_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) + parseFloat(fine_amt);
            }

            $('#pay_final_calculate_amt').val(total_pay_amt);
            $('#showPayAmt').html(total_pay_amt);
            
            
        }
    })

  
    $(document).on('keyup','.final-fine-amt',function(e){
        var fineAmt = $("#fine_head_amt").val();
        var finalPayAmt  = $("#pay_final_amt").val();
        console.log('fineAmt',fineAmt);
        console.log('finalPayAmt',finalPayAmt);
    });
    $(document).on('change','.heads-amt',function(e){
        var counterVal = $(this).attr('counter-id');
        var getPayAmt = $("#pay_amt"+counterVal).val();
        var getPayAmtActual = $('#head_pay_amt'+counterVal).val();
        if(parseFloat(getPayAmtActual) >= parseFloat(getPayAmt)) {
            $('#error_pay'+counterVal).hide();
            var headcounter = $('#headcounter').val();
            var student_id = $('#student_id').val();
            var total_pay_amt = wallet_amt = concession_amt = fine_amt = 0;
            for(i=0; i<headcounter; i++){
                if ($('#checkbox'+student_id+i).is(':checked')) {
                    var pay_amt = $("#pay_amt"+student_id+i).val();
                    total_pay_amt = parseFloat(total_pay_amt) + parseFloat(pay_amt);
                }
            }

            if ($('#checkboxWallet').is(':checked')) {
                wallet_amt = $("#wallet_calculate_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(wallet_amt);
            }

            if ($('#checkboxConcession').is(':checked')) {
                concession_amt = $("#concession_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(concession_amt);
            }

            if ($('#checkboxFine').is(':checked')) {
                fine_amt = $("#fine_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) + parseFloat(fine_amt);
            }

            $('#pay_final_calculate_amt').val(total_pay_amt);
            $('#showPayAmt').html(total_pay_amt);

        } else {
            $('#error_pay'+counterVal).show();
        }
        
        $("#pay_lator_amt").val(0);
        $("#pay_lator_amt_hide").val(0);
        $("#pay_final_amt").val(0);
        
    })
    $(document).on('keyup','.heads-amt',function(e){
        var counterVal = $(this).attr('counter-id');
        var getPayAmt = $("#pay_amt"+counterVal).val();
        var getPayAmtActual = $('#head_pay_amt'+counterVal).val();
        if(parseFloat(getPayAmtActual) >= parseFloat(getPayAmt)) {
            $('#error_pay'+counterVal).hide();
            var headcounter = $('#headcounter').val();
            var student_id = $('#student_id').val();
            var total_pay_amt = wallet_amt = concession_amt = fine_amt = 0;
            for(i=0; i<headcounter; i++){
                if ($('#checkbox'+student_id+i).is(':checked')) {
                    var pay_amt = $("#pay_amt"+student_id+i).val();
                    total_pay_amt = parseFloat(total_pay_amt) + parseFloat(pay_amt);
                }
            }

            if ($('#checkboxWallet').is(':checked')) {
                wallet_amt = $("#wallet_calculate_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(wallet_amt);
            }

            if ($('#checkboxConcession').is(':checked')) {
                concession_amt = $("#concession_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) - parseFloat(concession_amt);
            }

            if ($('#checkboxFine').is(':checked')) {
                fine_amt = $("#fine_head_amt").val();
                total_pay_amt = parseFloat(total_pay_amt) + parseFloat(fine_amt);
            }

            $('#pay_final_calculate_amt').val(total_pay_amt);
            $('#showPayAmt').html(total_pay_amt);

        } else {
            $('#error_pay'+counterVal).show();
        }
        $("#pay_lator_amt").val(0);
        $("#pay_lator_amt_hide").val(0);
        $("#pay_final_amt").val(0);
    })

    $(document).on('keyup','.final-pay-amt',function(e){
        var finalPayAmt = calculatePayAmt = remainingAmt = 0;
        finalPayAmt  = $("#pay_final_amt").val();
        calculatePayAmt  = $("#pay_final_calculate_amt").val();

        console.log(finalPayAmt);
        console.log(calculatePayAmt);
        if(parseFloat(finalPayAmt) > parseFloat(calculatePayAmt) ){
            console.log('Add wallet case');
            remainingAmt = parseFloat(finalPayAmt) - parseFloat(calculatePayAmt);
            $("#imprest_amt").val(remainingAmt);
            $("#imprest_amt_hide").val(remainingAmt);
            $("#pay_lator_amt").val('0');
            $("#pay_lator_amt_hide").val('0');
            $('#checkboxImprest').prop('checked', true);
            $('#checkboxPayLator').prop('checked', false);
        } else if(parseFloat(finalPayAmt) < parseFloat(calculatePayAmt)){
            remainingAmt = parseFloat(calculatePayAmt) - parseFloat(finalPayAmt);
            console.log('Pay-later case');
            $("#pay_lator_amt").val(remainingAmt);
            $("#pay_lator_amt_hide").val(remainingAmt);
            $("#imprest_amt").val('0');
            $("#imprest_amt_hide").val('0');
            $('#checkboxPayLator').prop('checked', true);
            $('#checkboxImprest').prop('checked', false);
        } else if(parseFloat(finalPayAmt) == parseFloat(calculatePayAmt)){
            console.log('Pay-later case 0');
            $("#pay_lator_amt").val(0);
            $("#pay_lator_amt_hide").val(0);
        }
    });
    
    jQuery(document).ready(function () {
        $('#date_start').bootstrapMaterialDatePicker({ weekStart : 0,time: false}).on('change', function(e, date){ $(this).valid(); });
        $('#cheque_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}).on('change', function(e, date){ $(this).valid(); });
        $('#transaction_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}).on('change', function(e, date){ $(this).valid(); });
    });

    $(document).ready(function () {
        $('#fees_counter').on('submit', function(e) {
            if ($('.headchecks').filter(':checked').length < 1){
                $("#error-block").html("Please select atleast one Fees Head");
                $("#error-block").show();
                $("#success-block").hide();
                return false;
            } else {
                $("#error-block").hide();
            }
            var pay_mode = $('#pay_mode').val();
            var cheque_amt = $('#cheque_amt').val();
            var total_pay_amt = $('#pay_final_calculate_amt').val();
            var diffAmt = 0;
            if(pay_mode == 'cheque'){
                if(parseFloat(cheque_amt) < parseFloat(total_pay_amt)){
                    // For pay later amt
                    diffAmt = parseFloat(total_pay_amt) - parseFloat(cheque_amt);
                    $('#pay_lator_amt').val(diffAmt);
                }
            }
      });
      
    });

    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#fees_counter").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                pay_mode: {
                    required: true,
                },
                select_date: {
                    required: true,
                },
                description: {
                    required: true,
                    lettersonly:true
                },
                cheque_date: {
                    required: true,
                },
                cheque_number: {
                    required: true,
                    digits: true
                },
                cheque_amt: {
                    required: true,
                    number: true
                },
                wallet_calculate_amt: {
                    number: true
                },
                fine_head_amt: {
                    number: true
                },
                imprest_amt: {
                    number: true
                },
                pay_lator_amt: {
                    number: true
                }

            },

            // message: {
            //     required: 'This field is required';
            // }

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                   // element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });
</script>
@endsection
