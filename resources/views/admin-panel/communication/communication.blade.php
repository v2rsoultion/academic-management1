@extends('admin-panel.layout.header')
@section('content')

<style type="text/css">
  .chat .chat-history{
    height: 400px !important;
  }
</style>
<!--  Main content here -->
<section class="content contact">

  <div class="block-header">
      <div class="row">
          <div class="col-lg-8 col-md-6 col-sm-12">
              <h2>{!! $page_title !!}</h2>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item">{!! $page_title !!}</li>
            </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                  <div class="alert alert-success" role="alert">
                                      {{ session()->get('success') }}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                @if($errors->any())
                                  <div class="alert alert-danger" role="alert">
                                      {{$errors->first()}}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                
                                <div class="row clearfix">
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="chat">
                                            <!-- end chat-header -->
                                            <div class="chat-history">
                                                <ul id="chat-box">
                                                    @if( !empty($communication_data) )
                                                        @foreach ($communication_data as $cm)
                                                          <li class="clearfix message-animation">
                                                              <div class="message my-message @if($sender_type == $cm['sender_type'] ) float-right @else float-left @endif">
                                                                  <br />
                                                                  {!! $cm['message']; !!} 
                                                              
                                                                  <span class="chat-staff-name">{!! $cm['name']; !!}</span>
                                                              
                                                              </div>
                                                              <div class="clearfix"></div>

                                                              <div class="@if($sender_type == $cm['sender_type']) float-right @else float-left @endif" >
                                                              {{ Html::image($cm['profile_img'], 'preview',  array('class' =>  ' rounded-circle', 'title'=> $cm['name'])) }}
                                                              </div>

                                                              <div class="message-data @if($sender_type == $cm['sender_type']) float-right @else float-left @endif">
                                                                  <span class="message-data-time">{!! $cm['time']; !!}</span> &nbsp; 
                                                              </div>
                                                              <div class="clearfix"></div>
                                                          </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                                <div class="scroll_all"></div>
                                            </div>
                                            <!-- end chat-history -->
                                            <div class="chat-message clearfix">
                                              {!! Form::open(['id' => 'send_message_supplier_form','url' => '','class' => '']) !!}
                                                <div class="row clearfix">
                                                    <div class="col-md-10 col-lg-10">
                                                        {!! Form::textarea('message', '', ['placeholder' => 'Enter your response', 'id' => 'message-to-send', 'autocomplete' => 'off']) !!}
                                                    </div>
                                                    <div class="col-md-2 col-lg-2">
                                                        <button type="button" onclick="return sendMessage()" id="send-msg-button" class="btn btn-raised btn-primary" title="Send">Send</button>
                                                    </div>
                                                  </div>
                                                  <small class="help-block" id="messge_error" style="display:none;">Please enter your message</small>
                                                </div>
                                                {!! Form::hidden('student_parent_id', $student_parent_id, ['id' => 'student_parent_id']) !!}
                                                {!! Form::hidden('staff_id', $staff_id, ['id' => 'staff_id']) !!}
                                                {!! Form::hidden('sender_id', $sender_id, ['id' => 'sender_id']) !!}
                                                {!! Form::hidden('sender_type', $sender_type, ['id' => 'sender_type']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                            
                                            <!-- end chat-message -->
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  
</section>


<script>
function sendMessage() {

    if( $('#message-to-send').val() != '' ){
        var form = $('#send_message_supplier_form');
        $('#send-msg-button').html('<i class="fa fa-spinner fa-spin"></i>');

        var formData = new FormData($("form")[0]);
        console.log($("form")[0]);
        $.ajax({
            type: 'POST',
            url: "{{url('admin-panel/send-communication-response')}}",
            data: formData,
            type: 'post',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function(result) {
                
                $("#messge_error").hide();
                $('#message-to-send').val('');
                $('#task_file').val('');
                $('#send-msg-button').html('Send');

                $('.no-chat').hide();
                
                if (result.status == true){
                    $('#chat-box').append(result.user_message);
                     // $('#chat-box').animate({
                     //     scrollTop: $("#chat-box").offset().top
                     // }, 2500);
                    $('#chat-box').animate({
                      scrollTop: $("#chat-box").height()}, 1000);
                    
                //   $('.scroll_all').animate({
                //      scrollTop: $(".scroll_all").offset().top
                // }, 2500);
                // $(".chat-history").animate({ scrollTop: $('.scroll_all').prop("scrollHeight")}, 1000);
                // $(".scroll_all").animate({ scrollTop: $('.scroll_all').prop("scrollHeight")}, 1000);
                // $('.chat-history').animate({
                //     scrollTop: $(".scroll_alls").offset().top
                // }, 2000);
                }
            }
        });
    }else{
        $("#messge_error").show();
        $('#message-to-send').focus();
    }
}

$(document).ready(function() {
     $('.chat-history').animate({
         scrollTop: $(".scroll_all").offset().top
     }, 2500);
 
});
</script>

@endsection