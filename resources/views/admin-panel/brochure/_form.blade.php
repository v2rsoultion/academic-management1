@if(isset($brochure['brochure_id']) && !empty($brochure['brochure_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('brochure_id',old('brochure_id',isset($brochure['brochure_id']) ? $brochure['brochure_id'] : ''),['class' => 'gui-input', 'id' => 'brochure_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('session_id', $brochure['arr_session'],old('session_id',isset($brochure['session_id']) ? $brochure['session_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.brochure_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('brochure_name', old('brochure_name',isset($brochure['brochure_name']) ? $brochure['brochure_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.brochure_name'), 'id' => 'brochure_name']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.brochure_file') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="brochure_file" id="brochure_file" >
                <input type="hidden" class="gui-input" id="brochure_file" placeholder="Upload File" readonly>
            </label>
        </div>
        {!! Form::hidden('brochure_file1',old('brochure_file1',isset($brochure['brochure_file1']) ? $brochure['brochure_file1'] : ''),['class' => 'gui-input', 'id' => 'brochure_file1', 'readonly' => 'true']) !!}
        @if(isset($brochure['brochure_file1']))
            <a href="../../../{{$brochure['brochure_file1']}}" target="_blank">View Brochure File</a>
        @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/admission') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#brochure-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                session_id: {
                    required: true
                },
                brochure_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                brochure_file: {
                    required: true,
                    extension: 'jpg,png,jpeg,pdf,doc,docx'
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
        
    });

    

</script>