@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
    .table-responsive {
    overflow-x: visible !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>My Class</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-menu/my-class') !!}">My Class</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/ViewList.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Students</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/my-class/view-students') }}" class="cusa" title="View" style="width: 60%; margin: 16px auto;">
                                            <i class="fas fa-eye"></i>View Students
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/Attendence.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Attendence</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/my-class/attendance/add-attendance') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/my-class/attendance/view-attendance') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                            <div class="row clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/LeaveManagement.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Leave Management</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/my-class/view-leaves') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/remarks.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Daily Remarks</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/my-class/remarks/add-remark') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('admin-panel/my-class/remarks/view-remarks') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/communication.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Communication</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/student-class/view-parents') }}" class="" title="Communication" style="width: 58%; margin: 15px auto;">
                                            <i class="fas fa-eye"></i> Communication 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/homework.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Home Work</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/my-class/homework-group') }}" class="cusa" title="View" style="width: 70%; margin: 16px auto;">
                                            <i class="fas fa-eye"></i>Manage Homework
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Marks</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection