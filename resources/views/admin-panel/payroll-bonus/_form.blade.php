@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.bonus_name') !!}</lable>
            {!! Form::text('bonus_name',old('bonus_name',isset($bonus['bonus_name']) ? $bonus['bonus_name']: ''),['class' => 'form-control','placeholder' => trans('language.bonus_name'), 'id' => 'bonus_name']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.bonus_amount') !!}</lable>
            {!! Form::number('bonus_amount',old('bonus_amount',isset($bonus['bonus_amount']) ? $bonus['bonus_amount']: ''),['class' => 'form-control','placeholder' => trans('language.bonus_amount'), 'id' => 'bonus_amount']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
<div class="headingcommon row col-lg-12" style="margin-left:0px;">Staffs: 
    <div class="checkbox" id="customid" style="margin-left:10px;">
        <input type="checkbox" id="check_all" class="checkBoxClass">
        <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
         Select All</div> 
        </div>
    @foreach($staff as $key => $fields)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="checkbox">
        @php
        $profile = '';
        if($fields->staff_profile_img != '') {
            $staff_profile_path = check_file_exist($fields->staff_profile_img, 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $fields->staff_name;
        $employee_profile = $profile." ".$staff_name;
        $check = '';
        $exist = 0;
        if(!empty($staff_ids_arr)){
            if(in_array($fields->staff_id, $staff_ids_arr)){
                $check = 'checked';
                $exist = 1;
            }
        }
        
        @endphp
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist]" value="{{$exist}}" >
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist_id]" value="{{$fields->staff_id}}" >
            <input id="staff{{$fields->staff_id}}" type="checkbox" name="staffs[{{$fields->staff_id}}][staff_id]" class="checkBoxClass check" value="{{$fields->staff_id}}"  @if(!empty($staff_ids_arr)) @if(in_array($fields->staff_id, $staff_ids_arr)) checked @endif @endif>
            <label for="staff{{$fields->staff_id}}" class="checkbox_1">@php echo $employee_profile; @endphp</label>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-bonus') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_bonus").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                bonus_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                bonus_amount: {
                    required: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });
    
    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });
</script>