@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.salary_range_min') !!}</lable>
      {!! Form::number('salary_range_min',old('salary_range_min',isset($tds_setting['salary_min']) ? $tds_setting['salary_min']: ''),['class' => 'form-control range','placeholder' => trans('language.salary_range_min'), 'id' => 'salary_range_min', 'min' => 1]) !!}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.salary_range_max') !!}</lable>
      {!! Form::number('salary_range_max',old('salary_range_max',isset($tds_setting['salary_max']) ? $tds_setting['salary_max']: ''),['class' => 'form-control range','placeholder' => trans('language.salary_range_max'), 'id' => 'salary_range_max', 'min' => 1]) !!}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.sal_deduction') !!}</lable>
      {!! Form::number('sal_deduction',old('sal_deduction',isset($tds_setting['salary_deduction']) ? $tds_setting['salary_deduction']: ''),['class' => 'form-control','placeholder' => trans('language.sal_deduction'), 'id' => 'sal_deduction', 'step' => 0.1, 'min' => 0, 'max' => 100]) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-tds-setting') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    $(".range").keyup(function(){
            $("#salary_range_min").valid();
            $("#salary_range_max").valid();
        });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("numbersonly", function(value, element) {
          return this.optional(element) || /^[0-9]+[.]?[0-9]+$/i.test(value);
        }, "Please use only number values");

        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) > parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) < parseFloat($otherElement.val(), 10);
        },"The value  must be less");

        $("#manage_tds_setting").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                salary_range_min: {
                    required: true,
                    number: true,
                    lessThan: '#salary_range_max'
                },
                salary_range_max: {
                    required: true,
                    number: true,
                    greaterThan: '#salary_range_min'
                },
                sal_deduction: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });

    function calculate() {
        var $a = $('#epf_a').val();
        var $b = $('#pension_fund_b').val();
        var $c = $a - $b;
        $('#epf_ab').val($c);
        $('.calculate_fund').val($c);
    }
</script>