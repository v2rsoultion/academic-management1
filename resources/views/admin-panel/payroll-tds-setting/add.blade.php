@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.tds_setting') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-tds-setting') !!}">{!! trans('language.tds_setting') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_tds_setting' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-tds-setting._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
      <div class="row">
        <div class="col-lg-3">
          <div class="form-group">
            {!! Form::text('s_deduction','',['class' => 'form-control','placeholder' => trans('language.sal_deduction'), 'id' => 's_deduction']) !!}
          </div>
        </div>
        
        <div class="col-lg-1">
          {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
        </div>
          <div class="col-lg-1">
            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
        </div>
      </div>
      {!! Form::close() !!}
      <div class="clearfix"></div>
    <!--  DataTable for view Records  -->
          <div class="table-responsive">
            <table class="table m-b-0 c_list" id="tds_setting_table" style="width:100%">
              {{ csrf_field() }}
              <thead>
                <tr>
                  <th>{!! trans('language.s_no') !!}</th>
                  <th>{!! trans('language.salary_range') !!}</th>
                  <th>{!! trans('language.sal_deduction') !!}</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody> </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#tds_setting_table').DataTable({
      serverSide: true,
      searching: false, 
      paging: false, 
      info: false,
      ajax: {
          url:"{{ url('admin-panel/payroll/manage-tds-setting-view')}}",
          data: function (f) {
              f.s_deduction = $('#s_deduction').val();
          }
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex' },
        {data: 'salary_range', name: 'salary_range'},
        {data: 'salary_deduction', name: 'salary_deduction'},
        {data: 'action', name: 'action'}
    ],
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "5%"
            },
            {
                "targets": 1, // your case first column
                "width": "15%"
            },
            {
                "targets": 2, // your case first column
                "width": "10%"
            },
            {
                "targets": 3, // your case first column
                "width": "10%"
            },
            {
                targets: [ 0, 1, 2, 3],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $('#clearBtn').click(function(){
        location.reload();
    });
  });
</script>
@endsection