

<div class="table-responsive" style="margin-top: 30px;" >
    @if(!empty($all_students))
    <table class="table m-b-0 c_list table-bordered" id="student-table" style="width:100%;">
        {{ csrf_field() }}

        <thead>
            <tr>
                <th style="width: 30px !important;">{{trans('language.s_no')}}</th>
                <th  style="width: 60px !important;">{{trans('language.student_enroll_number')}}</th>
                <th>{{trans('language.student_name')}}</th>
                @if(!empty($common_exam_list))
                    @foreach($common_exam_list as $exam_list)
                        @php 
                            $all_subject_count += $exam_list['all_subject_count'];
                        @endphp
                    @endforeach
                    @if($exam_type != '')
                        <th class="text-center" colspan="{!! $all_subject_count !!}">Consolidated</th>
                    @else
                        @foreach($common_exam_list as $exam_list)
                        @php 
                            $subjectCount = COUNT($exam_list['exam_subjects']);
                        @endphp
                        <th class="text-center" colspan="{!! $subjectCount !!}">{!! $exam_list['exam_name'] !!} </th>
                        @endforeach
                    @endif
                @endif
            </tr>
            @if($exam_type != '')
            <tr>
                <td style="width: 30px !important;">&nbsp;</td>
                <td  style="width: 60px !important;">&nbsp;</td>
                <td>&nbsp;</td>
                @if(!empty($common_exam_list))
                @foreach($common_exam_list as $exam_list)
                @php 
                    $subjectCount = COUNT($exam_list['exam_subjects']);
                @endphp
                <th class="text-center" colspan="{!! $subjectCount !!}">{!! $exam_list['exam_name'] !!} </th>
               
                @endforeach
                @endif
            </tr>
            @endif   
            <tr>
                <td style="width: 30px !important;">&nbsp;</td>
                <td  style="width: 60px !important;">&nbsp;</td>
                <td>&nbsp;</td>
                @if(!empty($common_exam_list))
                @foreach($common_exam_list as $exam_list)
                @php 
                    $subjectCount = COUNT($exam_list['exam_subjects']);
                @endphp
                
                @foreach($exam_list['exam_subjects'] as $subject_list)
                <td>{!! $subject_list['subject_name'] !!}</td>
                @endforeach
                @endforeach
                @endif
            </tr>
            
        </thead>
        <tbody>
           
            @if(!empty($all_students))
                @foreach($all_students as $key => $students)
                @php $key++; $total_subjects = $total_marks_enter = $total_submit_marks =  0 @endphp
            <tr>
                <td style="width: 30px !important;">{!! $key !!}</td>
                <td  style="width: 60px !important;">{!! $students['student_enroll_number'] !!}</td>
                <td>@if($students['profile'] != '') <img src="{!! $students['profile'] !!}" width="30px;" /> @endif {!! $students['student_name'] !!}</td>
                @if(!empty($students['student_exam_list']))
                @foreach($students['student_exam_list'] as $exam_list)
                @php 
                    $subjectCount = COUNT($exam_list['exam_subjects']);
                @endphp
                
                @foreach($exam_list['exam_subjects'] as $subject_list)
                
                <td>{!! $subject_list['marks'] !!}</td>
                @endforeach
                @endforeach
                @endif

                
                
            </tr>
            @endforeach
            @endif
            
        </tbody>
    </table>
    @else
        <div class="alert alert-danger" role="alert">
            No record found or Schedule still not publish.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
       
    @endif
    
</div>
@if($total_subjects === $total_marks_enter)
    @php $disabled = ""; @endphp
@else 
    @php $disabled = "disabled";  @endphp
@endif
@if($total_subjects === $total_submit_marks)
    @php $disabled = "disabled"; @endphp
@endif
@if(!empty($all_subjects))
<div class="row clearfix">
    <div class="col-lg-1 col-md-1">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Save','id'=> 'save']) !!}
    </div>
   
    <div class="col-lg-1 col-md-1">
        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary clearBtn1','name'=>'Clear', 'id' => "clearBtn1"]) !!}
    </div>

    <div class="col-lg-2 col-md-2">
        {!! Form::button('Final Submission', ['class' => 'btn btn-raised btn-round btn-primary final_submission','name'=>'final_submission','id'=> 'final_submission', 'style' => 'background: #F8A31B !important', $disabled]) !!}
    </div>
</div>

@endif