@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/manage-report-card') !!}">{!! trans('language.manage_report_card') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <h2><strong>Generate</strong> Marksheet 
                            <small>Exam Name: {!! $scheduleInfo['getExam']->exam_name !!}</small>
                            <small>Class: {!! $scheduleInfo['getClass']->class_name !!} </small> 
                            <small>Total Students: {!! $total_students !!}</small> 
                        </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="row">
                            {!! Form::open(['files'=>TRUE,'id' => 'msform' , 'class'=>'form-horizontal']) !!}
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active">Calculate Percentage & Pass/Fail Report</li>
                                    <li>Rank Generation </li>
                                    <li>Result Declare</li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset>
                                    <h2><strong>Calculate Percentage & Pass/Fail</strong> Report
                                    </h2>

                                    @php $i=0; @endphp
                                    @if(!empty($arr_pending_marks_list))
                                    <h2 class="fs-title">Missing Marks</h2>
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="missing-marks-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{!! trans('language.student_name') !!}</th>
                                                    <th>{!! trans('language.student_enroll_number') !!}</th>
                                                    <th>{!! trans('language.subjects') !!}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($arr_pending_marks_list as $pending_list)
                                                @php $i++; @endphp
                                                <tr>
                                                    <td>{!! $i; !!}</td>
                                                    <td>{!! $pending_list['student_name']; !!}</td>
                                                    <td>{!! $pending_list['student_enroll_number']; !!}</td>
                                                    <td>
                                                        @php $subject_names = [];  @endphp
                                                        @foreach($pending_list['student_subjects'] as $key => $subjects)

                                                        @if($subjects['marks'] === '')
                                                            @php $subject_names[] = $subjects['subject_name'] @endphp
                                                        @endif
                                                        @endforeach
                                                        @php echo implode(',', $subject_names)  @endphp
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @endif
                                    @if(empty($arr_pending_marks_list))
                                    
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{!! trans('language.student_name') !!}</th>
                                                    <th>{!! trans('language.student_enroll_number') !!}</th>
                                                    <th>{!! trans('language.total_exam_marks') !!}</th>
                                                    <th>{!! trans('language.total_obtain_marks') !!}</th>
                                                    <th>{!! trans('language.student_percentage') !!}(%)</th>
                                                    @if(!empty($all_subjects))
                                                    @foreach($all_subjects as $subject)
                                                    <th>{!! $subject['subject_name'] !!}<br />({!! $subject['max_marks'] !!})</th>
                                                    @endforeach
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($all_students as $student_list)
                                                @php $i++; @endphp
                                                <tr>
                                                    <td>{!! $i; !!}</td>
                                                    <td>@if($student_list['profile'] != '') <img src="{!! $student_list['profile'] !!}" width="30px;" /> @endif {!! $student_list['student_name']; !!}</td>
                                                    <td>{!! $student_list['student_enroll_number']; !!}</td>
                                                    <td>{!! $student_list['total_exam_marks']; !!}</td>
                                                    <td>{!! $student_list['total_obtain_marks']; !!}</td>
                                                    <td>{!! $student_list['student_percentage']; !!} %</td>
                                                    @foreach($student_list['student_subjects'] as $subject)

                                                    @php 
                                                        $current_date = date('d M Y'); 
                                                    @endphp
                                                    @if($subject['subject_status'] == 'Fail' && $subject['exist_subject_for_student'] == 1 )
                                                        @php $style = "style = 'width: 100%; background: #fee2e1'"; @endphp
                                                    @else
                                                        @php $style = "style = 'width: 100%; background: #fff'"; @endphp
                                                    @endif
                                                    <td>
                                                    @if($subject['exclude'] == 0)
                                                        @php 
                                                            $total_subjects++; 
                                                            $marks = 0;
                                                            $marks = $subject['marks'];  
                                                        @endphp 
                                                        @if($subject['marks'] !== '')
                                                            @php $total_marks_enter++; @endphp 
                                                        @endif
                                                        @if($subject['attend_flag'] == 0)
                                                            @php 
                                                                $marks = "A";  
                                                            @endphp 
                                                        @endif
                                                        @php 
                                                            $marks_disabled = "";
                                                        @endphp
                                                        @if($subject['marks_status'] === 1)
                                                            @php  $total_submit_marks++;   $marks_disabled = "disabled";  @endphp
                                                        @endif
                                                        <label class="form-group field select" {!! $style !!}> 
                                                            @if($subject['exist_subject_for_student'] == 1)
                                                            Marks: {!! $marks !!} <br />
                                                            Status: {!! $subject['subject_status'] !!} <br />
                                                            Grade: {!! $subject['grade_name'] !!}
                                                            @else 
                                                                -----
                                                            @endif
                                                        </label>
                                                    @else 
                                                        ------
                                                    @endif
                                                    </td>
                                                    @endforeach
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="button" name="next" class="next action-button submit-result" value="Next" />
                                    @endif
                                </fieldset>
                                <fieldset>
                                    <h2><strong>Rank</strong> Generation
                                    </h2>
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="rank-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{!! trans('language.student_name') !!}</th>
                                                    <th>{!! trans('language.student_enroll_number') !!}</th>
                                                    <th>{!! trans('language.student_percentage') !!}(%)</th>
                                                    <th>{!! trans('language.class_rank') !!}</th>
                                                    <th>{!! trans('language.section_rank') !!}</th>
                                                    <th>{!! trans('language.pass_fail_status') !!}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($all_ranks as $rank)
                                                @php $i++; @endphp
                                                <tr>
                                                    <td>{!! $i; !!}</td>
                                                    <td>{!! $rank['student_name']; !!}</td>
                                                    <td>{!! $rank['student_enroll_number']; !!}</td>
                                                    <td>{!! $rank['percentage']; !!}%</td>
                                                    <td>{!! $rank['class_rank']; !!}</td>
                                                    <td>{!! $rank['section_rank']; !!}</td>
                                                    <td>@if($rank['pass_fail'] == 1) Pass @else Fail @endif</td>
                                                    
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                                    <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <h2><strong>Result</strong> Declare
                                    </h2>
                                    <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
                                    <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
                                    {!! Form::hidden('exam_id',$scheduleInfo['exam_id'],['id' => 'exam_id']) !!}
                                    {!! Form::hidden('class_id',$scheduleInfo['class_id'],['id' => 'class_id']) !!}
                                    <lable class="from_one1">{!! trans('language.result_date') !!} <span class="red-text">*</span> :</lable>
                                    <div class="clearfix"></div>
                                    {!! Form::date('result_date', old('result_date',''), ['class' => 'form-control','placeholder'=>trans('language.result_date'), 'id' => 'result_date','style'=> 'width: 200px !important; float: left; margin-top: 10px;margin-right: 10px;','required']) !!}

                                    {!! Form::time('result_time', old('result_time',''), ['class' => 'form-control','placeholder'=>trans('language.result_time'), 'id' => 'result_time','style'=> 'width: 200px !important; float: left; margin-top: 10px;margin-right: 10px;','required']) !!}
                                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                                    
                                        {!! Form::submit('Submit', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Submit']) !!}
                                   
                                  
                                </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
        $('#msform').on('submit', function(e) {
            var r = confirm("Are You Sure To Declare Result?");
            if (r == true) {
                var exam_id = $("#exam_id").val();
                var class_id = $("#class_id").val();
                var result_date = $('#result_date').val();
                if(result_date != ""){
                    $(".mycustloading").show();
                    var formData = $(this).serialize();
                    console.log(formData);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type     : "POST",
                        url      : "{{url('admin-panel/examination/save-result')}}",
                        data     : formData,
                        cache    : false,
                        success  : function(data) {
                            var trimmedResponse = $.trim(data);
                            if(trimmedResponse == "success"){
                                $("#success-block").html("Result will declare on given date & time.");
                                $("#success-block").show();
                                $("#error-block").html("");
                                $("#error-block").hide();
                                $(".mycustloading").hide();
                                $(':input[name="Submit"]').prop('disabled', true);
                            } else {
                                $("#success-block").html("");
                                $("#success-block").hide();
                                $("#error-block").html("Something goes wrong.");
                                $("#error-block").show();
                                $(".mycustloading").hide();
                            }
                        }
                    })
                } else {

                }
            } else {
                return false;
            }
            
           e.preventDefault();
       });
    });
    
    
    $(document).ready(function () {
        var rankTable = $('#rank-table').DataTable({
            dom: 'Blfrtip',
            processing: true,
            searching: false,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            buttons: [
                'pdf', 'print'
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%",
                    "class": "text-left"
                },
                {
                    "targets": 1, // your case first column
                    "width": "20%",
                    "class": "text-left"
                },
                {
                    "targets": 2, // your case first column
                    "width": "20%",
                    "class": "text-left"
                },
                {
                    "targets": 3, // your case first column
                    "width": "50%",
                    "class": "text-left"
                },
                {
                    targets: [ 0, 1,2,3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
            
        });
        var table = $('#missing-marks-table').DataTable({
            dom: 'Blfrtip',
            processing: true,
            searching: false,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            buttons: [
                'pdf', 'print'
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%",
                    "class": "text-left"
                },
                {
                    "targets": 1, // your case first column
                    "width": "20%",
                    "class": "text-left"
                },
                {
                    "targets": 2, // your case first column
                    "width": "20%",
                    "class": "text-left"
                },
                {
                    "targets": 3, // your case first column
                    "width": "50%",
                    "class": "text-left"
                },
                {
                    targets: [ 0, 1,2,3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
            
        });
        var table1 = $('#student-table').DataTable({
            dom: 'Blfrtip',
            processing: true,
            searching: false,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            buttons: [
                'pdf', 'print'
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%",
                    "class": "text-left"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%",
                    "class": "text-left"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%",
                    "class": "text-left"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%",
                    "class": "text-left"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%",
                    "class": "text-left"
                },
                {
                    targets: [ 0, 1,2,3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
            
        });
        
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            
            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                'transform': 'scale('+scale+')',
                'position': 'absolute'
            });
                    next_fs.css({'left': left, 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            
            //show the previous fieldset
            previous_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity,'position': 'relative'});
                }, 
                duration: 1000, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".submit").click(function(){
            return false;
        })
    });

   

</script>
@endsection