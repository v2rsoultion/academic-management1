@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Tabsheet</strong> </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('type', $manage['arr_type'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'type','onChange' => 'checkSearch(),showBlock(this.value)'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="exam-block"  style="display: none">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('exam_id', $manage['arr_exams'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="class-block" style="display: none">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $manage['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="grid-block" style="display: none">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        
        $('#search-form').on('submit', function(e) {
            var exam_id = $("#exam_id").val();
            var class_id = $("#class_id").val();
            var formData = $(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{url('admin-panel/examination/get-tabsheet-grid')}}",
                data     : formData,
                cache    : false,
                success  : function(data) {
                    var trimmedResponse = $.trim(data);
                    $("#grid-block").css("display", "block");
                    $("#grid-block").html(trimmedResponse);
                    $(".mycustloading").hide();
                   
                }
            })
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    function showBlock(type){
        if(type == 2){
            $("#exam-block").hide();
            $("#class-block").show();
            $("#exam_id").val('');
        } else if(type == 1){
            $("#exam-block").show();
            $("#class-block").show();
        } else {
            $("#exam-block").hide();
            $("#class-block").hide();
            $("#exam_id").val('');
        }
    }
    function checkSearch(){
        var exam_id = $("#exam_id").val();
        var term_id = $("#term_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(exam_id == "" && term_id == ''){
            $(':input[name="Search"]').prop('disabled', true);
            $("#marks-grid").css("display", "none");
        } else {
            
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }


</script>
@endsection