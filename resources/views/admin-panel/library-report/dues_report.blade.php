@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.dues_report') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library-report') !!}">{!! trans('language.menu_library_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.dues_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('book_id', $map['arr_book'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'book_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('member_type', $map['member_type'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'member_type','onChange' => 'showBlock(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3"  id="student-block"  style="display: none">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('student_id', $map['arr_student'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3"  id="staff-block"  style="display: none">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('staff_id', $map['arr_staff'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search','disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                                <div class="table-responsive" style="margin-top:10px;">
                                    <table class="table m-b-0 c_list" id="library-report" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.book_name')}}</th>
                                                <th>{{trans('language.bbook_unique_no')}}</th>
                                                <th>{{trans('language.book_category')}}</th>
                                                <th>{!! trans('language.book_isbn_no') !!}</th>
                                                <th>{!! trans('language.book_author_name') !!}</th>
                                                <th>{!! trans('language.member_type') !!}</th>
                                                <th>{!! trans('language.book_issued_member_name') !!}</th>
                                                <th>{!! trans('language.expired_date') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#library-report').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                'csv', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/library-report/dues-report-data')}}',
                data: function (d) {
                    d.book_id = $('select[name=book_id]').val();
                    d.member_type = $('select[name=member_type]').val();
                    d.student_id = $('select[name=student_id]').val();
                    d.staff_id = $('select[name=staff_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_name', name: 'book_name' },
                {data: 'book_unique_id', name: 'book_unique_id' },
                {data: 'book_category', name: 'book_category'},
                {data: 'book_isbn_no', name: 'book_isbn_no'},
                {data: 'author_name', name: 'author_name'},
                {data: 'member_type', name: 'member_type'},
                {data: 'member_name', name: 'member_name'},
                {data: 'expired_date', name: 'expired_date'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "9%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 8, // your case first column
                    "width": "19%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });
    });

    function showBlock(type){
        if(type == 2){
            $("#student-block").hide();
            $("#staff-block").show();
            $("#student-block").val('');
        } else if(type == 1){
            $("#student-block").show();
            $("#staff-block").hide();
            $("#staff-block").val('');
        } else {
            $("#student-block").hide();
            $("#staff-block").hide();
            $("#student-block").val('');
            $("#staff-block").val('');
        }
    }

    function checkSearch(){
        var book_id = $("#book_id").val();
        var member_type = $("#member_type").val();
        if(book_id != '' || member_type != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
    
</script>
@endsection