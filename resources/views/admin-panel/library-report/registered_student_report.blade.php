@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.registered_student') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library-report') !!}">{!! trans('language.menu_library_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.registered_student') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $map['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'getSubject(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search','disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                                <div class="table-responsive" style="margin-top:10px;">
                                    <table class="table m-b-0 c_list" id="library-report" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{!! trans('language.student_name') !!}</th>
                                                <th>{!! trans('language.class_section') !!}</th>
                                                <th>{!! trans('language.student_father_name') !!}</th>
                                                <th>{!! trans('language.contact_no') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#library-report').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                'csv', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/library-report/registered-student-report-data')}}',
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                    d.section_id = $('select[name=section_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_enroll_number', name: 'student_enroll_number' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'class_section', name: 'class_section'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'student_father_mobile_number', name: 'student_father_mobile_number'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });
    });

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var class_id = $("#class_id").val();
        if(class_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
    
</script>
@endsection