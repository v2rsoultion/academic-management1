@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.lscheme_name') !!}</lable>
            {!! Form::text('lscheme_name',old('lscheme_name',isset($lscheme['lscheme_name']) ? $lscheme['lscheme_name']: ''),['class' => 'form-control','placeholder' => trans('language.lscheme_name'), 'id' => 'lscheme_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.lscheme_period') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('leave_period', $leave_period['lscheme_period'],isset($lscheme['lscheme_period']) ? $lscheme['lscheme_period'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'leave_period'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @php
    $check1 = $check0 = '';
    if($lscheme['carry_forward_limit'] == '')
    {
        $check0='checked';
    } else 
    {
        $check1='checked';
    }
    @endphp
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">Carry Forward</lable>
        <div class="radio" style="margin-top:6px !important;">
            <input id="radio32" name="inventory_invoice" class="abc" type="radio" value="1" <?php echo $check1; ?>>
            <label for="radio32" class="document_staff"> Yes</label>
            <input id="radio31" name="inventory_invoice" class="abc" type="radio" value="0" <?php echo $check0; ?>>
            <label for="radio31" class="document_staff"> No</label>
        </div>
        </div>
    </div>
    @if(!empty($lscheme['carry_forward_limit']))
    <div class="col-lg-3" id="CarryForwardLimit" style="display:block;">
        <div class="form-group">
            <lable class="from_one1">Carry Forward Limit</lable>
            {!! Form::text('carry_forward_limit',old('carry_forward_limit',isset($lscheme['carry_forward_limit']) ? $lscheme['carry_forward_limit']: ''),['class' => 'form-control','placeholder' => trans('language.carry_forward_limit'), 'id' => 'carry_forward_limit', 'onkeyup' => 'leaveCalculate()']) !!}
        </div>
        <div class="help-block" id="carry-forward-error"> </div>
    </div>
    @else
    <div class="col-lg-3" id="CarryForwardLimit" style="display:none;">
        <div class="form-group">
            <lable class="from_one1">Carry Forward Limit</lable>
            {!! Form::text('carry_forward_limit',old('carry_forward_limit',isset($lscheme['carry_forward_limit']) ? $lscheme['carry_forward_limit']: ''),['class' => 'form-control','placeholder' => trans('language.carry_forward_limit'), 'id' => 'carry_forward_limit', 'onkeyup' => 'leaveCalculate()']) !!}
        </div>
        <div class="help-block" id="carry-forward-error"> </div>
    </div>
    @endif
    </div>
    <div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.no_of_leaves') !!}</lable>
            {!! Form::number('no_of_leaves',old('no_of_leaves',isset($lscheme['no_of_leaves']) ? $lscheme['no_of_leaves']: ''),['class' => 'form-control','placeholder' => trans('language.no_of_leaves'), 'id' => 'no_of_leaves', 'onkeyup' => 'leaveCalculate()']) !!}
        </div>
    </div>
    <div class="col-lg-9">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.special_instruction') !!}</lable>
      {!! Form::textarea('special_instruction',old('special_instruction',isset($lscheme['special_instruction']) ? $lscheme['special_instruction']: ''),['class' => 'form-control','placeholder'=>trans('language.special_instruction'), 'id' => 'special_instruction', 'rows' => '2']) !!}
    </div>
    </div>
    
</div>
<div class="row clearfix">
<div class="manage_lschemeeadingcommon row col-lg-12" style="margin-left:0px;">Staffs: 
    <div clamanage_lschemes="checkbox" id="customid" style="margin-left:10px;">
        <input type="checkbox" id="check_all" class="checkBoxClass">
        <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
         Select All</div> 
        </div>
    @foreach($staff as $key => $fields)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="checkbox">
        @php
        $profile = '';
        if($fields->staff_profile_img != '') {
            $staff_profile_path = check_file_exist($fields->staff_profile_img, 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $fields->staff_name;
        $employee_profile = $profile." ".$staff_name;
        $check = '';
        $exist = 0;
        if(!empty($staff_ids_arr)){
            if(in_array($fields->staff_id, $staff_ids_arr)){
                $check = 'checked';
                $exist = 1;
            }
        }
        
        @endphp
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist]" value="{{$exist}}" >
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist_id]" value="{{$fields->staff_id}}" >
            <input id="staff{{$fields->staff_id}}" type="checkbox" name="staffs[{{$fields->staff_id}}][staff_id]" class="checkBoxClass check" value="{{$fields->staff_id}}"  @if(!empty($staff_ids_arr)) @if(in_array($fields->staff_id, $staff_ids_arr)) checked @endif @endif>
            <label for="staff{{$fields->staff_id}}" class="checkbox_1">@php echo $employee_profile; @endphp</label>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
  <div class="col-lg-1">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1">
    <a href="{{ url('admin-panel/payroll/manage-leave-scheme') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

         jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');

        $("#manage_lscheme").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                lscheme_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                leave_period: {
                    required: true
                },
                carry_forward_limit: {
                    required: true  
                },
                no_of_leaves: {
                    required: true
                },
                special_instruction: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });
    $(document).ready(function() {
        $(".abc").click(function(){ 
        if($(this).val() == 1)
            $("#CarryForwardLimit").show();
        else
            $("#CarryForwardLimit").hide();
        });
    });

    function leaveCalculate() {
        var carry_forward_limit = $('#carry_forward_limit').val(); 
        var no_of_leaves = $('#no_of_leaves').val(); 
        if(carry_forward_limit <= no_of_leaves) {
            $('#save').removeAttr("disabled");
            $('#carry-forward-error').html('');
        } else {
            $errors = 'please enter less than no of leaves!!';
            $('#carry-forward-error').html($errors);
            $('#save').attr("disabled", "disabled");
        }
    } 
</script>