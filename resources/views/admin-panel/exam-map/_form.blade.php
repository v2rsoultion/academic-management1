
{!! Form::hidden('decrypted_exam_id',isset($decrypted_exam_id) ? $decrypted_exam_id : '',['class' => 'gui-input', 'id' => 'exam_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

{!! Form::hidden('hide', '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<!-- Basic Info section -->
    <div id="sectiontable-body">
        
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select('addClasses[0][class_id]', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id0','onChange'=>'getSection(0,this.value)', 'required'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.section_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select('addClasses[0][section_id]', $listData['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id0','onChange'=>'showSubject(0,this.value)', 'required'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 text-left"><button class="btn btn-primary custom_btn" type="button" onclick="addClassBlock()" >Add Class</button></div>
        </div>
        <div id="subject_block0" class="row clearfix" style="margin-top: 15px;">
           
        </div>
    </div>
    


<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination/map-exam-class-subjects') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9][\s0-9a-zA-Z]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#class-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                class_id: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });
    function getSection(counterVal,class_id)
    {
        var exam_id = $("#exam_id").val();
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/get-sections')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'exam_id': exam_id
                },
                success: function (data) {
                    console.log(data);
                    $("#section_id"+counterVal).html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function showSubject(countVal,section_id){
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/show-section-subjects')}}",
                type: 'GET',
                data: {
                    'countVal': countVal,
                    'section_id': section_id
                },
                success: function (data) {
                    $("#subject_block"+countVal).html(data.res);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#subject_block"+countVal).html('11');
            $(".mycustloading").hide();
        }
    }

    function addClassBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body").append('<div id="class_block'+arr_key+'" ><hr /><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable><div class="form-group m-bottom-0"><label class=" field select" style="width: 100%"><select name="addClasses['+arr_key+'][class_id]" id="class_id'+arr_key+'" required class="form-control show-tick select_form1 select2" onChange="getSection('+arr_key+',this.value)"> @foreach($listData['arr_class'] as $key => $value)<option value="{{$key}}">{{$value}}</option> @endforeach</select><i class="arrow double"></i></label></div> </div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.section_name') !!} <span class="red-text">*</span> :</lable><div class="form-group m-bottom-0"><label class=" field select" style="width: 100%"><select name="addClasses['+arr_key+'][section_id]" id="section_id'+arr_key+'" required class="form-control show-tick select_form1 select2" onChange="showSubject('+arr_key+',this.value)"> @foreach($listData['arr_section'] as $key => $value)<option value="{{$key}}">{{$value}}</option> @endforeach</select><i class="arrow double"></i> </label> </div> </div><div class="col-lg-3 col-md-3 text-left"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div> <div id="subject_block'+arr_key+'" class="row clearfix" style="margin-top: 15px;"></div></div>');
        $('#class_id'+arr_key+'').select2().on('change', function(e, data){ $(this).valid(); });
        $('#section_id'+arr_key+'').select2().on('change', function(e, data){ $(this).valid(); });
    }
    function remove_block(id){
        $("#class_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    

</script>