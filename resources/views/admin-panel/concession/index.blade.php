@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                            <div class="row clearfix">
                               
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $concession['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)', 'required'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $concession['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: none">
                            <hr>
                           
                            <div class="table-responsive" >    
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.name')}}</th>
                                            <th>{{trans('language.student_enroll_number')}}</th>
                                            <th>{{trans('language.student_father_name')}}</th>
                                            <th>Manage</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="modal fade" id="concessionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Concession </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
            <table class="table m-b-0 c_list" id="concession-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.fees_name') !!}</th>
                        <th>{!! trans('language.fees_amt') !!}</th>
                        <th>{!! trans('language.concession_amt') !!}</th>
                        <th>{!! trans('language.reason') !!}</th>
                        <th>{!! trans('language.paid_amt') !!}</th>
                    </tr>
                </thead>
            </table>
            <div class="row clearfix">
                <div class="col-lg-2 col-md-2">
                    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit']) !!}
                </div>
                <div class="col-lg-3 col-md-3">
                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
        var table = $('#student-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            ajax: {
                url: "{{url('admin-panel/concession/get-student-list')}}",
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                    d.section_id = $('select[name=section_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'concession', name: 'concession'},
            ],
             columnDefs: [
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            if(class_id == "" && section_id == ""){
                $("#list-block").hide();
            } else {
                $("#list-block").show();
            }
            e.preventDefault();
        });
       
    });
    
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    $(document).on('click','.students',function(e){
        $("#error-block").hide();
        $("#success-block").hide();
        var student_id = $(this).attr('student-id');
        var class_id = $('#class_id').val();
        $('#concessionModel').modal('show');
        var table1 = $('#concession-table').DataTable({
                //dom: 'Blfrtip',
                destroy: true,
                pageLength: 30,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                searching: false,
                paging: false,
                info: false,
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ],
                ajax: {
                    url: '{{url('admin-panel/concession/get-student-fees')}}',
                    data: function (d) {
                        d.student_id = student_id;
                        d.class_id = class_id;
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'head_name', name: 'head_name'},
                    {data: 'head_amt', name: 'head_amt'},
                    {data: 'concession_amt_data', name: 'concession_amt_data'},
                    {data: 'reason', name: 'reason'},
                    {data: 'paid_amt', name: 'paid_amt'},
                ],
                columnDefs: [
                   
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });

            $('.select2').select2();
    })

    $('#list-form').on('submit', function(e) { 
        var formData = $(this).serialize();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/concession/concession-map')}}",
            data     : formData,
            cache    : false,

            success  : function(data) {
                data = $.trim(data);
                
                if(data == "success"){
                    $("#success-block").html("Concession amount successfully stored !!");
                    $("#success-block").show();
                    $("#error-block").hide();

                } else {
                    $("#error-block").html("No record found !!");
                    $("#error-block").show();
                    $("#success-block").hide();
                }
            }
        })
        e.preventDefault();
    });
   
    function calculateAmt(counter){
        var originalAmt = document.getElementById('head_amt'+counter).value;
        var concessionAmt = document.getElementById('concession_amt'+counter).value;
        
        if(concessionAmt == ''){
            concessionAmt = 0;
        }
        if(parseFloat(originalAmt) < parseFloat(concessionAmt)){
            document.getElementById('error_msg'+counter).innerHTML = "Concession amount should be less than Fees amount.";
            $(':input[type="submit"]').prop('disabled', true);
            return false;
        } else {
            var paidAmt = parseFloat(originalAmt) - parseFloat(concessionAmt);
            document.getElementById('error_msg'+counter).innerHTML = "";
            document.getElementById('paid_fees_id'+counter).innerHTML = paidAmt;
            $(':input[type="submit"]').prop('disabled', false);
            return true;
        }
    }

</script>
@endsection

