@if(isset($book['book_id']) && !empty($book['book_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('book_id',old('book_id',isset($book['book_id']) ? $book['book_id'] : ''),['class' => 'gui-input', 'id' => 'book_id', 'readonly' => 'true']) !!}
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_name', old('book_name',isset($book['book_name']) ? $book['book_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_name'), 'id' => 'book_name']) !!}
        </div>
        @if ($errors->has('book_name')) <p class="help-block">{{ $errors->first('book_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.bbook_category') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('book_category_id',$listData['arr_category'],isset($book['book_category_id']) ? $book['book_category_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'book_category_id','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if($errors->has('book_category_id')) <p class="help-block">{{ $errors->first('book_category_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.bbook_type') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('book_type',$listData['book_type'],isset($book['book_type']) ? $book['book_type'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'book_type','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if($errors->has('book_type')) <p class="help-block">{{ $errors->first('book_type') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_subtitle') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_subtitle', old('book_subtitle',isset($book['book_subtitle']) ? $book['book_subtitle']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_subtitle'), 'id' => 'book_subtitle']) !!}
        </div>
        @if ($errors->has('book_subtitle')) <p class="help-block">{{ $errors->first('book_subtitle') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_isbn_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_isbn_no', old('book_isbn_no',isset($book['book_isbn_no']) ? $book['book_isbn_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_isbn_no'), 'id' => 'book_isbn_no']) !!}
        </div>
        @if ($errors->has('book_isbn_no')) <p class="help-block">{{ $errors->first('book_isbn_no') }}</p> @endif
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_author') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('author_name', old('author_name',isset($book['author_name']) ? $book['author_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_author'), 'id' => 'author_name']) !!}
        </div>
        @if ($errors->has('author_name')) <p class="help-block">{{ $errors->first('author_name') }}</p> @endif
    </div>
</div>
<!-- Other Information -->
<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_publisher') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('publisher_name', old('publisher_name',isset($book['publisher_name']) ? $book['publisher_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_publisher'), 'id' => 'publisher_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_edition') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('edition', old('edition',isset($book['edition']) ? $book['edition']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_edition'), 'id' => 'edition']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.bbook_vendor') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('book_vendor_id',$listData['arr_vendor'],isset($book['book_vendor_id']) ? $book['book_vendor_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'book_vendor_id','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bbook_price') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_price', old('title_name',isset($book['book_price']) ? $book['book_price']: ''), ['class' => 'form-control','placeholder'=>trans('language.bbook_price'), 'id' => 'book_price']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.bbook_cupboard') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('book_cupboard_id',$listData['arr_cubboard'],isset($book['book_cupboard_id']) ? $book['book_cupboard_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'book_cupboard_id','onChange' => 'getCupboardShelf(this.value)','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.bbook_cupboard_shelf') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('book_cupboardshelf_id',$listData['cupboard_shelf'],isset($book['book_cupboardshelf_id']) ? $book['book_cupboardshelf_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'book_cupboardshelf_id','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.bbook_remark') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('book_remark',old('book_remark',isset($book['book_remark']) ? $book['book_remark']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.bbook_remark'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
    

</div>    

<!-- Other Information -->
<div class="header">
    <h2><strong>Additional</strong> Copy</h2>
</div>

{!! Form::hidden('addition_copy_counter',isset($book['addition_copy']) ? COUNT($book['addition_copy']) : '1',['class' => 'gui-input', 'id' => 'addition_copy_counter']) !!}
<div id="additional-copy">
    @if(isset($book['addition_copy']) && !empty($book['addition_copy']))
        @php  $key = 0; @endphp
        @foreach($book['addition_copy'] as $copyKey => $copy)
        @php  
        $book_info_id = "addition_copy[".$key."][book_info_id]"; 
    @endphp
    <div id="copy_block{{$key}}">
        {!! Form::hidden($book_info_id,$copy['book_info_id'],['class' => 'gui-input']) !!}
        <p class="green" id="document_success"></p>
        <div class="row clearfix">                            
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.bbook_unique_no') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::text('addition_copy['.$key.'][unique_no]',$copy['book_unique_id'], ['class' => 'form-control','placeholder'=>trans('language.bbook_unique_no'), 'id' => 'bbook_unique_no'.$key.'','required']) !!}
                </div>
                </div>
            <div class="col-lg-4 col-md-4 custchechboxpad">
                <div class="">
                    @php
                        $check ="";
                        if($copy['exclusive_for_staff'] == 1){
                            $check ="checked";
                        }
                    @endphp
                    <input type="checkbox" name="addition_copy[{{$key}}][staff]" {{$check}}>
                    <label for="">
                    Exclusive for Staff
                    </label>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 custchechboxpad">
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addcopy()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="removecopy({{$key}}),remove_record({{$copy['book_info_id']}})" >Remove</button>
                @endif
            </div>
            
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">                            
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.bbook_unique_no') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('addition_copy[0][unique_no]','', ['class' => 'form-control unique_no','placeholder'=>trans('language.bbook_unique_no'), 'id' => 'bbook_unique_no','required']) !!}
            </div>
            </div>
        <div class="col-lg-4 col-md-4 custchechboxpad">
            <div class="">
                <input type="checkbox" name='addition_copy[0][staff]'>
                <label for="">
                Exclusive for Staff
                </label>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 custchechboxpad">
            <button class="btn btn-primary custom_btn" type="button" onclick="return addcopy();">Add</button>
        </div>
    </div>
    @endif
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/library') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#book-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                book_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                book_category_id: {
                    required: true,
                },
                book_type: {
                    required: true,
                },
                book_subtitle: {
                    required: true,
                },
                book_isbn_no: {
                    required: true,
                },
                author_name: {
                    required: true,
                },
                publisher_name: {
                    required: true,
                },
                edition: {
                    required: true,
                },
                book_vendor_id: {
                    required: true,
                },
                book_price: {
                    required: true,
                    number: true
                },
                book_cupboard_id: {
                    required: true,
                },
                book_cupboardshelf_id: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });


    });

    function getCupboardShelf(cupboard_id)
    {
        if(cupboard_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/book/get-cupboard-shelf-data')}}",
                type: 'GET',
                data: {
                    'cupboard_id': cupboard_id
                },
                success: function (data) {
                    $("select[name='book_cupboardshelf_id'").html(data.options);
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='book_cupboardshelf_id'").html('');
        }    
    }

    function addcopy(){
        var counter_a = parseInt(document.getElementById('addition_copy_counter').value);
        document.getElementById('addition_copy_counter').value = counter_a+1;
        var counter_b = document.getElementById('addition_copy_counter').value;
        var arr_key = parseInt(counter_b) - 1;
         
        $('#additional-copy').append('<div id="copy_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"> <lable class="from_one1">{!! trans("language.bbook_unique_no") !!} :</lable><div class="form-group"> <input type="text" name="addition_copy['+arr_key+'][unique_no]" class="form-control" placeholder="{!! trans("language.bbook_unique_no") !!}" id="addition_copy" required></div></div><div class="col-lg-4 col-md-4 custchechboxpad"><div class=""> <input id="" type="checkbox" name="addition_copy['+arr_key+'][staff]"> <label for=""> Exclusive for Staff </label></div></div><div class="col-lg-4 col-md-4 custchechboxpad"> <button class="btn btn-danger custom_btn" type="button" onclick="return removecopy('+arr_key+');">Remove</button></div></div></div></div>');
        }
    
    function removecopy(id){
        $("#copy_block"+id).remove();
    }

    function remove_record(book_info_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/book/delete-single-book/')}}/"+book_info_id,
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }

</script>