@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_bbook') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/book/add-book') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_bbook') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name','', ['class' => 'form-control ','placeholder'=>trans('language.bbook_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('publisher','', ['class' => 'form-control ','placeholder'=>trans('language.bbook_publisher'), 'id' => 'publisher']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('subtitle','', ['class' => 'form-control ','placeholder'=>trans('language.bbook_subtitle'), 'id' => 'subtitle']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}                                

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="book-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.bbook_name')}}</th>
                                            <th>{{trans('language.bbook_category')}}</th>
                                            <th>{{trans('language.bbook_publisher')}}</th>
                                            <th>{{trans('language.bbook_cupboard_plus_shelf')}}</th>
                                            <th>{{trans('language.bbook_issue_copies')}}</th>
                                            <th>{{trans('language.bbook_total_copies')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<div class="modal fade" id="copiesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Book Copies - <span id="book_name"></span> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding-top: 0px">
      <p class="green" id="copy_success"></p>
      <table class="table m-b-0 c_list" id="copy-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.bbook_unique_no') !!}</th>
                    <th>{!! trans('language.exclusive_for_staff') !!}</th>
                    <th>Availability</th>
                    <th>Action</th>
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#book-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            // ajax: "{{url('admin-panel/book/data')}}",

            ajax: {
                url: "{{url('admin-panel/book/data')}}",
                data: function (d) {
                    d.book_name = $('input[name=name]').val();
                    d.publisher = $('input[name=publisher]').val();
                    d.subtitle  = $('input[name=subtitle]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_name', name: 'book_name'},
                {data: 'category', name: 'category'},
                {data: 'publisher_name', name: 'publisher_name'},
                {data: 'cupboard_shelf', name: 'cupboard_shelf'},
                {data: 'issued_copies', name: 'issued_copies'},
                {data: 'info', name: 'info'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "6%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });
    $(document).on('click','.copies',function(e){
        var book_id = $(this).attr('book-id');
        var book_name = $(this).attr('book-name');
        $("#book_name").html(book_name);
        $('#copiesModel').modal('show');
        var table1 = $('#copy-table').DataTable({
                //dom: 'Blfrtip',
                destroy: true,
                pageLength: 10,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ],
                ajax: {
                    url: '{{url('admin-panel/book/copies')}}',
                    data: function (d) {
                        d.id = book_id;
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'book_unique_id', name: 'book_unique_id'},
                    {data: 'exclusive_for_staff', name: 'exclusive_for_staff'},
                    {data: 'book_copy_status', name: 'book_copy_status'},
                    {data: 'action', name: 'action'},
                ],
                columnDefs: [
                   
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });
    })
    $(document).on('click','.book-records',function(e){
        var copy_id = $(this).attr('copy-record');
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/book/delete-single-book/')}}/"+copy_id,
                success: function (data) {
                    $("#copy_success").html(data);
                    $(".mycustloading").hide();
                    var book_id = $(this).attr('book-id');
                    // $('#copiesModel').modal('show');
                    var table1 = $('#copy-table').DataTable({
                            //dom: 'Blfrtip',
                            destroy: true,
                            pageLength: 10,
                            processing: true,
                            serverSide: true,
                            bLengthChange: false,
                            // buttons: [
                            //     'copy', 'csv', 'excel', 'pdf', 'print'
                            // ],
                            ajax: {
                                url: '{{url('admin-panel/book/copies')}}',
                                data: function (d) {
                                    d.id = book_id;
                                }
                            },
                            columns: [
                                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                                {data: 'book_unique_id', name: 'book_unique_id'},
                                {data: 'exclusive_for_staff', name: 'exclusive_for_staff'},
                                {data: 'book_copy_status', name: 'book_copy_status'},
                                {data: 'action', name: 'action'},
                            ],
                            columnDefs: [
                            
                                {
                                    targets: [ 0, 1, 2 ],
                                    className: 'mdl-data-table__cell--non-numeric'
                                }
                            ]
                        });
                }
            });
        }
    });
    

</script>
@endsection




