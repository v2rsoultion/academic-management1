<option value="">Select CupBoard Shelf </option>
@if(!empty($cupboard_shelf))
  @foreach($cupboard_shelf as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif