@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_name') !!}</lable>
            {!! Form::text('arrear_name',old('arrear_name',isset($arrear['arrear_name']) ? $arrear['arrear_name']: ''),['class' => 'form-control','placeholder' => trans('language.arrear_name'), 'id' => 'arrear_name']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_from_date') !!}</lable>
            {!! Form::Date('arrear_from_date',old('arrear_from_date',isset($arrear['arrear_from_date']) ? $arrear['arrear_from_date']: ''),['class' => 'form-control','placeholder' => trans('language.arrear_from_date'), 'id' => 'arrear_from_date']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_to_date') !!}</lable>
            {!! Form::Date('arrear_to_date',old('arrear_to_date',isset($arrear['arrear_to_date']) ? $arrear['arrear_to_date']: ''),['class' => 'form-control','placeholder' => trans('language.arrear_to_date'), 'id' => 'arrear_to_date']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_amount') !!}</lable>
            {!! Form::number('arrear_amount',old('arrear_amount',isset($arrear['arrear_amount']) ? $arrear['arrear_amount']: ''),['class' => 'form-control','placeholder' => trans('language.arrear_amount'), 'id' => 'arrear_amount', 'onkeyup' => 'calculateAmount()']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_total_amt') !!}</lable>
            {!! Form::number('arrear_total_amt',old('arrear_total_amt',isset($arrear['arrear_total_amt']) ? $arrear['arrear_total_amt']: ''),['class' => 'form-control','placeholder' => trans('language.arrear_total_amt'), 'id' => 'arrear_total_amt', 'readonly']) !!}
        </div>
    </div>
    @php
    $check1 = $check0 = '';
    if($arrear['round_off'] == 0)
    {
        $check0='checked';
    } else 
    {
        $check1='checked';
    }
    @endphp
    <div class="col-lg-4">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_round_off') !!}</lable>
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio33" class="round_off" name="round_off" type="radio" value=1 <?php echo $check1; ?>>
                <label for="radio33" class="document_staff"> Highest Rupee</label>
                <input id="radio34" class="round_off" name="round_off" type="radio" value=0 <?php echo $check0; ?>>
                <label for="radio34" class="document_staff"> Nearest Rupee</label>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
<div class="headingcommon row col-lg-12" style="margin-left:0px;">Staffs: 
    <div class="checkbox" id="customid" style="margin-left:10px;">
        <input type="checkbox" id="check_all" class="checkBoxClass">
        <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
         Select All</div> 
        </div>
    @foreach($staff as $key => $fields)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="checkbox">
        @php
        $profile = '';
        if($fields->staff_profile_img != '') {
            $staff_profile_path = check_file_exist($fields->staff_profile_img, 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $fields->staff_name;
        $employee_profile = $profile." ".$staff_name;
        $check = '';
        $exist = 0;
        if(!empty($staff_ids_arr)){
            if(in_array($fields->staff_id, $staff_ids_arr)){
                $check = 'checked';
                $exist = 1;
            }
        }
        
        @endphp
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist]" value="{{$exist}}" >
            <input type="hidden" name="staffs[{{$fields->staff_id}}][exist_id]" value="{{$fields->staff_id}}" >
            <input id="staff{{$fields->staff_id}}" type="checkbox" name="staffs[{{$fields->staff_id}}][staff_id]" class="checkBoxClass check" value="{{$fields->staff_id}}"  @if(!empty($staff_ids_arr)) @if(in_array($fields->staff_id, $staff_ids_arr)) checked @endif @endif>
            <label for="staff{{$fields->staff_id}}" class="checkbox_1">@php echo $employee_profile; @endphp</label>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
  <div class="col-lg-1">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1">
    <a href="{{ url('admin-panel/payroll/manage-arrear') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

         jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');

        $("#manage_arrear").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                arrear_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                arrear_from_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
                arrear_to_date: {
                    required: true,
                    date: true,
                    maxlength:10,
                    greaterThan: "#arrear_from_date"  
                },
                arrear_amount: {
                    required: true
                },
                arrear_total_amt: {
                    required: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });
    
    function calculateAmount() {
        var $amount = $('#arrear_amount').val();
        var $total_amount = ($amount*12).toFixed(2);
        var $round_off_amount = Math.ceil($total_amount);
        $('#arrear_total_amt').val($round_off_amount);
    }
    $(document).on('click', '.round_off', function(e) {
        var $round_val = $(this).val();
       
        if($round_val == 0) {
            var $amount = $('#arrear_amount').val();
            var $total_amount = ($amount*12).toFixed(2);
            var $round_off_amount = Math.floor($total_amount);
            $('#arrear_total_amt').val($round_off_amount);
        } else {
            var $amount = $('#arrear_amount').val();
            var $total_amount = ($amount*12).toFixed(2);
            var $round_off_amount = Math.ceil($total_amount);
            $('#arrear_total_amt').val($round_off_amount);
        }
    }); 
</script>