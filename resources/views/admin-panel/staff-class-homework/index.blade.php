@extends('admin-panel.layout.header')
@section('content')

<!-- <style type="text/css">
.chat-staff-name1{
    position: absolute;
    left:10px;
    bottom:10px;
}
</style> -->
<!--  Main content here -->
<section class="content contact">

  <div class="block-header">
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
              <h2>{!! $page_title !!}</h2>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/my-class') !!}">{!! trans('language.menu_my_class') !!}</a></li>
              <li class="breadcrumb-item">{!! $page_title !!}</li>
            </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                  <div class="alert alert-success" role="alert">
                                      {{ session()->get('success') }}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                @if($errors->any())
                                  <div class="alert alert-danger" role="alert">
                                      {{$errors->first()}}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="chat">
                                            <!-- end chat-header -->
                                            <div class="chat-history">
                                                <ul id="chat-box">
                                                    @if (!empty($homework_data))
                                                        @foreach ($homework_data as $hw)
                                                            <li class="clearfix message-animation">
                                                            @if($login_info['staff_id'] == $hw['staff_id'])
                                                                <div class="message my-message float-right message-right">
                                                                    Subject: {!! $hw['subject_name']; !!} <br /><br />
                                                                    {!! $hw['h_conversation_text']; !!}
                                                                    @if($hw['attechment'] != "")
                                                                        <span class="chat-staff-attechment"><a href="{!! url('admin-panel/download-homework-attechment/'.$hw['h_conversation_id']) !!}" > Download Attechment</a></span>
                                                                    @endif
                                                                    <span class="chat-staff-name ">{!! $hw['staff_name']; !!}</span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                {{ Html::image($hw['staff_profile_img'], 'preview',  array('class' => 'float-right rounded-circle', 'title'=> $hw['staff_name'])) }}
                                                                <div class="message-data float-right">
                                                                    <span class="message-data-time">{!! $hw['time']; !!}</span> &nbsp; 
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            @endif
                                                            @if($login_info['staff_id'] != $hw['staff_id'])
                                                                <div class="message my-message float-left">
                                                                    Subject: {!! $hw['subject_name']; !!} <br /><br />
                                                                    {!! $hw['h_conversation_text']; !!}
                                                                    @if($hw['attechment'] != "")
                                                                        <span class="chat-staff-attechment"><a href="{!! url('admin-panel/download-homework-attechment/'.$hw['h_conversation_id']) !!}" > Download Attechment</a></span>
                                                                    @endif
                                                                    <span class="chat-staff-name">{!! $hw['staff_name']; !!}</span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                {{ Html::image($hw['staff_profile_img'], 'preview',  array('class' => 'float-left rounded-circle', 'title'=> $hw['staff_name'])) }}
                                                                <div class="message-data float-left">
                                                                    <span class="message-data-time">{!! $hw['time']; !!}</span> &nbsp; 
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            @endif
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                            <!-- end chat-history -->
                                            <div class="chat-message clearfix">
                                                {!! Form::open(['id' => 'send_message_supplier_form','url' => '','class' => '','enctype'=>"multipart/form-data"]) !!}
                                                    <div class="row clearfix">
                                                        <div class="col-md-2 col-lg-2">
                                                        <div class="form-group ">
                                                            <label class=" field select" style="width: 100%">
                                                                {!!Form::select('subject_id',$homework['subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                                                                <i class="arrow double"></i>
                                                                <small class="help-block" id="messge_error1" style="display:none;">Please Select Subject</small>
                                                            </label>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-2 col-lg-2">
                                                            <input type="file" class="gui-file" name="h_conversation_attechment" id="h_conversation_attechment" >
                                                        </div>
                                                        <div class="col-md-7 col-lg-7">
                                                            {!! Form::textarea('message', '', ['placeholder' => 'Enter your homework', 'id' => 'message-to-send', 'autocomplete' => 'off']) !!}
                                                            <small class="help-block" id="messge_error2" style="display:none;">Please enter homework</small>
                                                        </div>
                                                        <div class="col-md-1 col-lg-1">
                                                            <button type="submit" id="send-msg-button" class="btn btn-raised btn-primary" title="Send">Send</button>
                                                        </div>
                                                    </div>
                                                    {!! Form::hidden('homework_group_id', $conversations['homework_group_id'], ['id' => 'homework_group_id']) !!}
                                                    <!-- {!! Form::hidden('subject_id', $subject_id, ['id' => 'subject_id']) !!} -->
                                                {!! Form::close() !!}
                                            </div>
                                            <!-- end chat-message -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  
</section>

<script>

$('#send_message_supplier_form').submit(function(event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{url('admin-panel/my-class/send-homework')}}",
        type: 'POST',              
        data: formData,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function(result)
        {
          location.reload();
        },
        error: function(data)
        {
            console.log(data);
        }
    });

});
$(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
function sendMessage() {
    if( $('#message-to-send').val() != '' && $('#subject_id').val() != ''){
        var form = $('#send_message_supplier_form');
        $('#send-msg-button').html('<i class="fa fa-spinner fa-spin"></i>');
        $.ajax({
            type: 'POST',
            url: "{{url('admin-panel/my-class/send-homework')}}",
            data: $(form).serialize(),
            success: function(result) {
                
                $("#messge_error2").hide();
                $('#message-to-send').val('');
                $("#messge_error1").hide();
                $('#subject_id').val('');
                $('#send-msg-button').html('Send');

                $('.no-chat').hide();
                $('.chat-history').animate({
                    scrollTop: $(".chat-message").offset().top},
                    'slow');
                if (result.status == true){
                    $('#chat-box').append(result.user_message);
                }
            }
        });
    }else{
        $("#messge_error2").show();
        $('#message-to-send').focus();
        $("#messge_error1").show();
        $('#subject_id').focus();
    }
}
</script>

@endsection