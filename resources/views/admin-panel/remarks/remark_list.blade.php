@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body form-gap">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                            <div class="row clearfix">
                               
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $remark['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $remark['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('subject_id', $remark['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('staff_id', $remark['arr_staff'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <div class="input-group ">
                                        {!! Form::text('student_enroll_number', old('student_enroll_number', ''), ['class' => 'form-control ','placeholder'=>trans('language.student_enroll_number'), 'id' => 'student_enroll_number']) !!}
                                        <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <div class="input-group ">
                                        {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.student_name'), 'id' => 'student_name']) !!}
                                        <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <div class="input-group ">
                                        {!! Form::text('remark_date', old('remark_date', ''), ['class' => 'form-control ','placeholder'=>trans('language.remark_date'), 'id' => 'remark_date']) !!}
                                        <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                    </div>
                                </div>
                                
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <div class="table-responsive" >    
                            <table class="table m-b-0 c_list " id="remark-table" style="width:100%;">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>{{trans('language.student_enroll_number')}}</th>
                                        <th>{{trans('language.student_name')}}</th>
                                        <th>{{trans('language.class_section')}}</th>
                                        <th>{{trans('language.subject_name')}}</th>
                                        <th>{{trans('language.staff_name')}}</th>
                                        <th>{{trans('language.remark_date')}}</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="modal fade" id="remarkModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Remark Info </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
            <div id="remark_block" style="width:100%;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
        var table = $('#remark-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            ajax: {
                url: "{{url('admin-panel/daily-remarks/remarks-data')}}",
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                    d.section_id = $('select[name=section_id]').val();
                    d.subject_id = $('select[name=subject_id]').val();
                    d.staff_id = $('select[name=staff_id]').val();
                    d.enroll_no = $('input[name=student_enroll_number]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.remark_date = $('input[name=remark_date]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'student_profile', name: 'student_profile'},
                {data: 'class_section', name: 'class_section'},
                {data: 'subject', name: 'subject'},
                {data: 'staff', name: 'staff'},
                {data: 'remark_date', name: 'remark_date'},
                {data: 'remark', name: 'remark'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        })
        $('#remark_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false});
       
    });
    
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    $(document).on('click','.remarks',function(e){
        var remark_id = $(this).attr('remark-id');
        if(remark_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/daily-remarks/remark-single-data')}}",
                type: 'GET',
                data: {
                    'remark_id': remark_id
                },
                success: function (data) {
                    $("#remark_block").html(data);
                    $(".mycustloading").hide();
                    $("#remarkModel").modal('show');
                }
            });
        }
        
    })

</script>
@endsection

