@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>{!! trans('language.student_remark') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/view-remarks') !!}">{!! trans('language.student_remark') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('staff_id', $listData['arr_teacher'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('subject_id', $listData['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="remark-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.remark_date')}}</th> 
                                                <th>{{trans('language.subject')}}</th>
                                                <th>{{trans('language.student_teacher')}}</th>
                                                <th>{{trans('language.remark_description')}}</th> 
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="remarkModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Remark Info </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
            <div id="remark_block" style="width:100%;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;"></div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#remark-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/student/remarks/remark-student-data')}}',
                data: function (d) {
                    d.staff_id = $('select[name="staff_id"]').val();
                    d.subject_id = $('select[name="subject_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'remark_date', name: 'remark_date'},
                {data: 'subject_name', name: 'subject_name'},
                {data: 'teacher_name', name: 'teacher_name'},
                {data: 'remark_description', name: 'remark_description'},
            ],
            //  columnDefs: [
            //     {
            //         "targets": 0, // your case first column
            //         "width": "8%"
            //     },
            //     {
            //         "targets": 1, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         "targets": 2, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         "targets": 3, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         "targets": 5, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         targets: [ 0, 1, 2, 3, 4 ],
            //         className: 'mdl-data-table__cell--non-numeric'
            //     }
            // ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    $(document).on('click','.remarks',function(e){
        var remark_id = $(this).attr('remark-id');
        if(remark_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/daily-remarks/remark-student-single-data')}}",
                type: 'GET',
                data: {
                    'remark_id': remark_id
                },
                success: function (data) {
                    $("#remark_block").html(data);
                    $(".mycustloading").hide();
                    $("#remarkModel").modal('show');
                  
                }
            });
        }
        
    })


</script>
@endsection




