@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! trans('language.member_issued_books') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.library_member') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{{ URL::to('admin-panel/member/view-students') }}">{!! trans('language.member_student_view') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.member_issued_books') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                                {!! Form::hidden('student_id',$student_id)!!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list " id="member-student-issuedbooks-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('language.member_issued_book')}}</th>
                                                <th>{{trans('language.member_issued_from')}}</th>
                                                <th>{{trans('language.member_issued_to')}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#member-student-issuedbooks-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            //ajax: '{{url('admin-panel/class/data')}}',

            ajax: {
                url: "{{url('admin-panel/member/view-students/issued-books-data')}}",
                data: function (d) {
                    d.student_id = $('input[name=student_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_name', name: 'book_name'},
                {data: 'issue_from', name: 'issue_from'},
                {data: 'issue_to', name: 'issue_to'},
            ],
             columnDefs: [
                {
                    "targets": 0,
                    "orderable": false
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });
  
</script>
@endsection