{!! Form::hidden('task_id',old('task_id',isset($task['task_id']) ? $task['task_id'] : ''),['class' => 'gui-input', 'id' => 'task_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.task_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('task_name', isset($task['task_name']) ? $task['task_name']: '', ['class' => 'form-control','placeholder' => trans('language.task_name'), 'id' => 'task_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.task_priority') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('task_priority', $task['arr_task_priority'], isset($task['task_priority']) ? $task['task_priority'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'task_priority']) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.task_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::Date('task_date', isset($task['task_date']) ? $task['task_date']: '', ['class' => 'form-control','placeholder' => trans('language.task_date'), 'id' => 'task_date']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.task_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('task_type', $task['arr_task_type'], isset($task['task_type']) ? $task['task_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'task_type', 'onChange' => 'tasktype(this.value)' ]) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    @if($task['task_type'] == 2 && $task['task_type'] != "") 
        <div class="col-lg-3 col-md-3" id="show_staff">
    @else 
        <div class="col-lg-3 col-md-3" style="display: none;" id="show_staff">
    @endif

    
        <lable class="from_one1">{!! trans('language.staff') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_id', $task['arr_staff'], isset($task['staff_id']) ? $task['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id']) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>
@if($task['task_type'] == 1 && $task['task_type'] != "") 
    <div class="row clearfix" id="show_student">    
@else 
    <div class="row clearfix" style="display: none;" id="show_student">
@endif
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $task['arr_class'], isset($task['class_id']) ? $task['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)' ]) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('section_id', $task['arr_section'], isset($task['section_id']) ? $task['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'getStudent(this.value)' ]) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_id', $task['arr_student'], isset($task['student_id']) ? $task['student_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id']) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<div class="row">  
  <div class="col-lg-12 text_area_desc">
    <lable class="from_one1">{!! trans('language.task_description') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::textarea('task_description', isset($task['task_description']) ? $task['task_description']: '',['class' => 'form-control','placeholder'=>trans('language.task_description'), 'id' => 'task_description', 'rows' => '2']) !!}
    </div>
  </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.task_file') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="task_file" id="task_file" >
                <input type="hidden" class="gui-input" id="task_file" placeholder="Upload File" readonly>
            </label>
        </div>
        {!! Form::hidden('task_file1',old('task_file1',isset($brochure['task_file1']) ? $brochure['task_file1'] : ''),['class' => 'gui-input', 'id' => 'task_file1', 'readonly' => 'true']) !!}
        @if(isset($task['task_file']))
            <a href="{{ url($task['arr_task_file']['display_path'].''.$task['task_file'])}}" target="_blank">View Task File</a>
        @endif
    </div>
</div>
<br>

<div class="row" >
  <div class="col-lg-1">
    <button type="submit" class="btn btn-raised btn-primary" title="Submit">Save
    </button>
  </div>
  <div class="col-lg-1">
    <a href="{{ url('admin-panel/task-manager/add-task') }}" class="btn btn-raised btn-primary" title="Cancel">Cancel
    </a>
  </div>
</div>

<style type="text/css">
    .select2{
        width: 100% !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 
        $("#task-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                task_name: {
                    required: true
                },
                task_priority: {
                    required: true
                },
                task_date: {
                    required: true,
                    date:true,
                    maxlength:10
                },
                task_description: {
                    required: true
                },
                task_type: {
                    required: true
                },
                section_id: {
                    required: true
                },
                student_id: {
                    required: true
                },
                staff_id: {
                    required: true
                },
                class_id: {
                    required: true
                },
                task_file: {
                    extension: 'jpg,png,jpeg,pdf,doc,docx'
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });


    function tasktype(tasktype)
    {
        if(tasktype == 1){
            $("#show_student").show();
        } else {
            $("#show_student").hide();
        }

        if(tasktype == 2){
            $("#show_staff").show();
        } else {
            $("#show_staff").hide();
        }
        
    }

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }


    function getStudent(section_id)
    {
        var class_id = $("#class_id").val();
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/task-manager/get-student-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id': section_id
                },
                success: function (data) {
                    $("select[name='student_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='student_id'").html('<option value="">Select Student</option>');
            $(".mycustloading").hide();
        }
    }

    

</script>