@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .tabless table tr td .opendiv ul li{
    padding: 0px 0px !important;
  }
  .footable .dropdown-menu>li>a{
    padding: 10px 15px !important;
  }
  .footable .dropdown-menu{
    padding: 0px 0px !important; 
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.view_task') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <a href="{{ url('admin-panel/task-manager/add-task') }}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/task-manager') }}">{!! trans('language.menu_task_manager') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/task-manager/view-task') }}">{!! trans('language.view_task') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
              @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              @endif
              @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    {{$errors->first()}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              @endif
              {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                <div class="row">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Task</lable> -->
                      {!!Form::text('s_task_name', '', ['class' => 'form-control','placeholder'=>trans('language.task_name'), 'id' => 's_task_name']) !!}
                    </div>
                  </div>
                  
                  <div class="col-lg-3">
                    <div class="form-group">
                      {!!Form::select('task_type', $arr_task_type, '', ['class' => 'form-control show-tick select_form1 select2','id'=>'task_type' ]) !!}
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Task Date</lable> -->
                      {!!Form::Date('s_task_date', '', ['class' => 'form-control','placeholder'=>trans('language.task_date'), 'id' => 's_task_date']) !!}
                    </div>
                  </div>
                  
                  <div class="col-lg-1 col-md-1">
                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                  </div>
                  <div class="col-lg-1 col-md-1">
                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                  </div>
                </div>
              {!!Form::close()!!}
              <!--  DataTable for view Records  -->
                <table class="table m-b-0 c_list" id="task-table" style="width:100%">
                {{ csrf_field() }}
                    <thead>
                        <tr>
                            <th>{{trans('language.task_s_no')}}</th>
                            <th>{{trans('language.task_name')}}</th>
                            <th>{{trans('language.task_priority')}}</th>
                            <th>{{trans('language.task_date')}}</th>
                            <th>{{trans('language.task_description')}}</th>
                            <th>{{trans('language.task_type')}}</th>
                            <th>{{trans('language.task_status')}}</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                </table> 
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {

    $('.select2').select2().on('change', function(e, data){ $(this).valid(); });

        var table = $('#task-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/task-manager/data')}}",
              data: function (f) {
                  f.s_task_name     = $('#s_task_name').val();
                  f.s_task_date     = $('#s_task_date').val();
                  f.task_type       = $('#task_type').val();
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'task_name', name: 'task_name'},
            {data: 'task_priority', name: 'task_priority'},
            {data: 'task_date', name: 'task_date'},
            {data: 'task_description', name: 'task_description'},
            {data: 'task_type_detail', name: 'task_type_detail'},
            {data: 'task_status', name: 'task_status'},
            {data: 'action', name: 'action'}
        ],
        columnDefs: [
          {
            "targets": 0, // your case first column
            "width": "1%"
          },
          {
            "targets": 4, // your case first column
            "width": "30%"
          },
          {
            "targets": 5, // your case first column
            "width": "15%"
          },
          {
            "targets": 6, // your case first column
            "width": "10%"
          },
          {
            "targets": 7, // your case first column
            "width": "22%"
          },
          // {
          //     targets: [ 0, 1, 2, 3, 4],
          //     className: 'mdl-data-table__cell--non-numeric'
          // }
        ] 
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
</script>
@endsection
