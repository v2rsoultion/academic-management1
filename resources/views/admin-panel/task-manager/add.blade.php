@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.add_task') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <a href="{{ url('admin-panel/task-manager/view-task') }}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/task-manager') }}">{!! trans('language.menu_task_manager') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/task-manager/add-task') }}">{!! trans('language.add_task') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap">
                {!! Form::open(['files'=>TRUE,'id' => 'task-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                @include('admin-panel.task-manager._form',['submit_button' => $submit_button])
                {!! Form::close() !!}
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
@endsection

