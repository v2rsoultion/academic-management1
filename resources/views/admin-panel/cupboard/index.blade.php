@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_cupboard') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/book-cupboard/add-book-cupboard') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_cupboard') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                        <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                         
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.cupboard_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="cupboard-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.cupboard_name')}}</th>
                                            <th>{{trans('language.cupboard_shelf')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<div class="modal fade" id="cupboardshelfModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cupboard Shelf </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="green" id="cupboardshelf_success"></p>
      <table class="table m-b-0 c_list" id="cupboardshelf-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.cupboard_shelf_name') !!}</th>
                    <th>{!! trans('language.cupboard_shelf_capacity') !!}</th>
                    <th>{!! trans('language.cupboard_shelf_detail') !!}</th>
                    <th>Action</th>
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#cupboard-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/book-cupboard/data')}}',
                data: function (d) {
                    d.name  = $('input[name="name"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_cupboard_name', name: 'book_cupboard_name'},
                {data: 'cupboard_shelf', name: 'cupboard_shelf'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $(document).on('click','.cupboardshelfs',function(e){
        var cupboardshelfs_id = $(this).attr('cupboardshelf-id');
        $('#cupboardshelfModel').modal('show');
        var table1 = $('#cupboardshelf-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 10,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/cupboard/cupboard-shelf')}}',
                data: function (d) {
                    d.id = cupboardshelfs_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_cupboardshelf_name', name: 'book_cupboardshelf_name'},
                {data: 'book_cupboard_capacity', name: 'book_cupboard_capacity'},
                {data: 'book_cupboard_shelf_detail', name: 'book_cupboard_shelf_detail'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                {
                    "targets": 4, // your case first column
                    "width": "30%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    })

    $(document).on('click','.cupboardshelf-records',function(e){
        var cupboardshelfs_id = $(this).attr('cupboardshelf-record');
        var book_cupboard_id = $(this).attr('cupboard-record');
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/cupboard/delete-cupboard-shelf/')}}/"+cupboardshelfs_id,
                success: function (data) {
                    $("#cupboardshelf_success").html(data);
                    $(".mycustloading").hide();
                    var table1 = $('#cupboardshelf-table').DataTable({
                        //dom: 'Blfrtip',
                        destroy: true,
                        pageLength: 10,
                        processing: true,
                        serverSide: true,
                        bLengthChange: false,
                        ajax: {
                            url: '{{url('admin-panel/cupboard/cupboard-shelf')}}',
                            data: function (d) {
                                d.id = book_cupboard_id;
                            }
                        },
                        columns: [
                            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                            {data: 'book_cupboardshelf_name', name: 'book_cupboardshelf_name'},
                            {data: 'book_cupboard_capacity', name: 'book_cupboard_capacity'},
                            {data: 'book_cupboard_shelf_detail', name: 'book_cupboard_shelf_detail'},
                            {data: 'action', name: 'action'},
                        ],
                        columnDefs: [
                            {
                                "targets": 4, // your case first column
                                "width": "30%"
                            },
                            {
                                targets: [ 0, 1, 2, 3],
                                className: 'mdl-data-table__cell--non-numeric'
                            }
                        ]
                    });
                }
            });
        }
    });
</script>
@endsection




