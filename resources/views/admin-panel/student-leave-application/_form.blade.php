@if(isset($leave_application['student_leave_id']) && !empty($leave_application['student_leave_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('leave_application',old('student_leave_id',isset($leave_application['student_leave_id']) ? $leave_application['student_leave_id'] : ''),['class' => 'gui-input', 'id' => 'student_leave_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.leave_app_date_to') !!} :</lable>
        <div class="form-group">
            {!! Form::date('student_leave_from_date', old('student_leave_from_date',isset($leave_application['student_leave_from_date']) ? $leave_application['student_leave_from_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_app_date_to'), 'id' => 'student_leave_from_date']) !!}
        </div>
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.leave_app_date_from') !!} :</lable>
        <div class="form-group">
            {!! Form::date('student_leave_to_date', old('student_leave_to_date',isset($leave_application['student_leave_to_date']) ? $leave_application['student_leave_to_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_app_date_from'), 'id' => 'student_leave_to_date'] ) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.leave_app_attachment') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="student_document_file" >
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
            </label>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.leave_app_reason') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('student_leave_reason',old('student_leave_reason',isset($leave_application['student_leave_reason']) ? $leave_application['student_leave_reason']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.leave_app_reason'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {


        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Only alphabetical characters");

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) >= Number($(params).val())); 
        },'End Date must be greater than start date.');

        $.validator.addMethod("minDate", function(value, element) {
            var curDate = new Date();
            var inputDate = new Date(value);
            if (inputDate >= curDate)
                return true;
            return false;
        }, "Please select future dates");

        jQuery.validator.addMethod("extension", function(a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif|pdf", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only pdf,jpg,png format allowed."));

        $("#student-leave-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_leave_reason: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_leave_from_date: {
                    required: true,  
                    date: true,
                    minDate: true
                },
                student_leave_to_date: {
                    required: true,  
                    date: true,
                    greaterThan: "#student_leave_from_date"
                },
                documents:{
                    extension: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    //error.insertAfter(element.parent());
                    element.closest('.form-group').after(error);
                }
            }
        });
        
        // $('#student_leave_to_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#student_leave_from_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false ,minDate : new Date() }).on('change', function(e, date)
        // {
        //     $('#student_leave_to_date').bootstrapMaterialDatePicker('setMinDate', date);
        //     $('#student_leave_to_date').val('');
        //     $(this).valid();  
        // });

        
    });

</script>