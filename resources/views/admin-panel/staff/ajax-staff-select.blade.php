<option value="">Select Staff</option>

@if(!empty($staff))
  @foreach($staff as $key => $value)
    <option value="{{ $key }}" @if(isset($staffIdsData) && $staffIdsData != '') @if(in_array($key,explode(',',$staffIdsData)))  @php echo "selected" @endphp
    @endif @endif>{{ $value }}</option>
  @endforeach
@endif