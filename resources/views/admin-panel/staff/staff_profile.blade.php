@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nav_item7
    {
    min-height: 150px !important;
    clear: both;
    }
    .imgclass:hover .active123 .tab_images123{
    left: 44% !important;
    }
    .imgclass:hover .tab_images123{
    left: 50% !important;
    }
    .nav_item3 {
    min-height: 255px !important;
    }
    .tab1{
        padding-left: 5px !important;
    }
    .tab2{
        padding-right: 3px !important;
    }

    .fc-bg tr td:nth-child(5) {
    background: white !important;
    color: white !important;
    }
    .fc-bg tr td:nth-child(3) {
        background: white !important;
        color: white !important;
    }
    .fc-bg tr td:nth-child(1) {
        background: white !important;
        color: white !important;
    }
    .fc-row.fc-week.fc-widget-content.fc-rigid{
        height: 90px !important;
    }
    .fc-scroller.fc-day-grid-container{
        height: 540px !important;
    }
    .fc-unthemed td.fc-today {
        background: white;
    }
    .attendence_green{
        background-color: green !important; 
        color:white !important;
        width: 80% !important;
    }
    .attendence_red{
        background-color: red !important; 
        color:white !important;
        width: 80% !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Profile</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    @if($login_info['admin_type'] != 2)
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff') !!}">{!! trans('language.staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff/view-staff') !!}">{!! trans('language.view_staff') !!}</a></li>
                    @endif
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/view-profile') !!}">{!! trans('language.profile') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card profile_card1">
                    <div class="body text-center pro_body_center1 form-gap ">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 text-left">
                                <div class="profile-image"> 
                                    @if($staff['staff_profile_img'] != "") 
                                    <img src="{!! URL::to($staff['staff_profile_img']) !!}" alt="{!! $staff['staff_name'] !!}" class="profile_image"> 
                                    @else
                                    <img src="{!! URL::to('public/images/default.png') !!}" alt="{!! $staff['staff_name'] !!}" class="profile_image"> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std1">
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.staff_name') !!}: </span> {!! $staff['title_name'] !!} {!! $staff['staff_name'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        
                                        <label> <span>{!! trans('language.designation') !!} : </span> {!! $staff['designation_name'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.staff_role') !!} : </span> {!! $staff['staff_role'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span> {!! trans('language.staff_email') !!} : </span> {!! $staff['staff_email'] !!}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std2">
                                    <div class="profile_detail_3">
                                        <label> <span> {!! trans('language.staff_dob') !!} : </span> {!! $staff['staff_dob'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.staff_gender') !!} : </span> {!! $staff['gender'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.staff_mobile_number') !!} : </span> {!! $staff['staff_mobile_number'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.shifts') !!} : </span> {!! $staff['staff_shifts'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.teaching_status') !!} : </span> {!! $staff['teaching_status'] !!}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="pull-left" style="width: 24%; margin-left: 2%">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card right_scroll" style="height: 400px;overflow: scroll;overflow-x: hidden;">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#personal" onClick="toggleBlock('personal');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/personal-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Personal Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#address_info" onClick="toggleBlock('address_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/address-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Address Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#documents" onClick="toggleBlock('documents');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/documents.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Documents </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#attendance"  onClick="toggleBlock('attendance');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/attendence.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Attendance </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#leave"  onClick="toggleBlock('leave');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/leave-management.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Leave Application </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#shift" onClick="toggleBlock('shift');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/shift.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Shift </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#schedule" onClick="toggleBlock('schedule');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/schedule.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Schedule </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass" id="">
                                    <a class="nav-link active123" data-toggle="tab" href="#salary" onClick="toggleBlock('salary');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/salary.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Salary </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass" id="">
                                    <a class="nav-link active123" data-toggle="tab" href="#edithistory" onClick="toggleBlock('edithistory');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/edit-history.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Edit History </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right" style="width: 72%;">
                <div class="col-lg-12 col-md-12 col-sm-12 tab2 padding-0">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="personal" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Basic Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['title_name'] !!} {!! $staff['staff_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_attendance_unique_id') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_attendance_unique_id'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.designation') !!} :</b> <span class="profile_detail_span_6">{!! $staff['designation_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_role') !!}  :</b> <span class="profile_detail_span_6">{!! $staff['staff_role'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_email') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_dob') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_dob'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_gender') !!} :</b> 
                                                                                        <span class="profile_detail_span_6">
                                                                                        {!! $staff['gender'] !!}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.shifts') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_shifts'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.teaching_status') !!} :</b> <span class="profile_detail_span_6">{!! $staff['teaching_status'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Professional Info </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_aadhar') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_aadhar'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_UAN') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_UAN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_ESIN') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_ESIN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_PAN') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_PAN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_qualification') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_qualification'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_specialization') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_specialization'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_reference') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_reference'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_experience') !!} :</b> <span class="profile_detail_span_6">
                                                                                        @if($staff['staff_experience'] != '')
                                                                                        @foreach($staff['staff_experience'] as $exp)
                                                                                            <li>{!! $exp !!}</li>
                                                                                        @endforeach
                                                                                        
                                                                                        @endif
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Other Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_father_name_husband_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_father_name_husband_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_mother_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_mother_name'] !!} </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_father_husband_mobile_no') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_father_husband_mobile_no'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_blood_group') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_blood_group'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.caste_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['caste_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.religion_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['religion_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.nationality_name') !!} :</b> <span class="profile_detail_span_6">{!! $staff['nationality_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_marital_status') !!} :</b> <span class="profile_detail_span_6">{!! $staff['marital'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane body" id="address_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="">
                                                    <div class="card border_info_tabs_1 card_top_border1">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="card">
                                                                    <div class="body">
                                                                        <div class="nav_item4">
                                                                            <div class="nav_item1">
                                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Address Information </a>
                                                                            </div>
                                                                            <div class="nav_item2">
                                                                                <div role="tabpanel" class="tab-pane" id="address_with_icon_title">
                                                                                    <div class="parent_address_info_1">
                                                                                        <div class="parent_address_temparay_info_1">
                                                                                          
                                                                                            
                                                                                        <div style="" class="pi_1">
                                                                                            <h6>Permanent Address :</h6>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_permanent_address') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_permanent_address'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_permanent_city') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_permanent_city_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_permanent_state') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_permanent_state_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_permanent_county') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_permanent_county_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_permanent_pincode') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_permanent_pincode'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="clearfix"></div>
                                                                                        </div>

                                                                                        <h6>Temporary Address :</h6>
                                                                                            
                                                                                            <div class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_temporary_address') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_temporary_address'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_temporary_city') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_temporary_city_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_temporary_state') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_temporary_state_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                           
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_temporary_county') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_temporary_county_name'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div  class="profile_detail_2">
                                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                                    <b class="profile_detail_bold_2">{!! trans('language.staff_temporary_pincode') !!} :</b> <span class="profile_detail_span_6">{!! $staff['staff_temporary_pincode'] !!}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="documents" style="margin-left: 20px !important;">
                            <!-- <div class=""> -->
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                    <div class="tab-content" style="background-color: #fff;">
                                        <div class="tab-pane active" id="classlist">
                                            <div class="">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item4">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Document Info </a>
                                                            </div>
                                                            <div class="nav_item2">
                                                                <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                <div class="table-responsive">
                                                                    <table class="table m-b-0 c_list" id="document-table" style="width:100%">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 8%">{{trans('language.s_no')}}</th>
                                                                                <th style="width: 20%">{{trans('language.staff_document_category')}}</th>
                                                                                <th>{{trans('language.staff_document_details')}}</th>
                                                                                <th style="width: 15%">{{trans('language.staff_document_file')}}</th>
                                                                                <th style="width: 15%">{{trans('language.submit_date')}}</th>
                                                                                <th style="width: 8%">Status </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @if($staff['documents'] != '')
                                                                                @php $documentKey = 0; @endphp
                                                                                @foreach($staff['documents'] as $documents )
                                                                                @php $documentKey++; @endphp
                                                                                <tr>
                                                                                    <td>{!! $documentKey !!}</td>
                                                                                    <td>{!! $documents['document_category_name'] !!}</td>
                                                                                    <td>{!! $documents['staff_document_details'] !!}</td>
                                                                                    @php 
                                                                                    $staff_document_file = '';
                                                                                    $status = 'Pending';
                                                                                    @endphp
                                                                                    @if($documents['staff_document_file'] != '')
                                                                                        @php
                                                                                            $staff_document_file = $documents['staff_document_file'];
                                                                                            $status = 'Uploaded';
                                                                                        @endphp
                                                                                    @endif
                                                                                    <td>
                                                                                        @if($staff_document_file != '')
                                                                                        <a target="_blank" href="{!! URL::to($staff_document_file) !!}">Attachment</a>
                                                                                        @else 
                                                                                            ------
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>{!! $documents['document_submit_date'] !!}</td>
                                                                                    <td>{!! $status !!}</td>
                                                                                </tr>
                                                                                @endforeach
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                    
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="shift" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Shift Info </a>
                                                            </div>
                                                            <div class="nav_item2">
                                                                <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                    <div class="table-responsive">
                                                                        <table class="table m-b-0 c_list" id="document-table" style="width:100%">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S No</th>
                                                                                    <th>Shifts</th>
                                                                                    <th>Start Time</th>
                                                                                    <th>End Time</th>
                                                                                    <!-- <th>Status</th>
                                                                                        <th>Action</th> -->
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>Morning</td>
                                                                                    <td>7:00 AM</td>
                                                                                    <td>12:00 PM</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>Evening</td>
                                                                                    <td>12:00 PM</td>
                                                                                    <td>05:00 PM</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="schedule" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item8">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Schedule Info </a>
                                                            </div>
                                                            <div class="nav_item2">
                                                                <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                    <div class="table-responsive">
                                                                        <table class="table m-b-0 c_list" id="document-table" style="width:100%">
                                                           
                                                                            <thead>
                                                                                <tr>
                                                                                    <th width="30">Period no.</th>
                                                                                    <th>Monday</th>
                                                                                    <th>Tuesday</th>
                                                                                    <th>Wenesday</th>
                                                                                    <th>Thursday</th>
                                                                                    <th>Friday</th>
                                                                                    <th>Saturday</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Period-1</td>
                                                                                    <td class="data_table1">
                                                                                        07:30 AM - 08:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> I-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        07:30 AM - 08:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> I-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        07:30 AM - 08:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> I-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        07:30 AM - 08:00 AM <br>
                                                                                        <strong>Sub. :</strong> Economic <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-2</td>
                                                                                    <td class="data_table1">
                                                                                        08:00 AM - 08:30 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> II-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        08:00 AM - 08:30 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> II-A 
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        08:00 AM - 08:30 AM<br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> II-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        08:00 AM - 08:30 AM <br>
                                                                                        <strong>Sub. :</strong> Business <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-3</td>
                                                                                    <td class="data_table1">
                                                                                        08:30 AM - 09:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> III-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        08:30 AM - 09:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> III-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        08:30 AM - 09:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        08:30 AM - 09:00 AM  <br>
                                                                                        <strong>Sub. :</strong> Computer <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        08:30 AM - 09:00 AM <br>
                                                                                        <strong>Sub. :</strong> English <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-4</td>
                                                                                    <td class="data_table1">
                                                                                        09:00 AM - 09:30 AM <br>
                                                                                        <strong>Sub. :</strong> Economic <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        09:00 AM - 09:30 AM  <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        09:00 AM - 09:30 AM <br>
                                                                                        <strong>Sub. :</strong> Economic <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        09:00 AM - 09:30 AM <br>
                                                                                        <strong>Sub. :</strong> Account <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        09:00 AM - 09:30 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-5</td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        10:00 AM - 10:30 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> X-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        10:00 AM - 10:30 AM <br>
                                                                                        <strong>Sub. :</strong> Account <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        10:00 AM - 10:30 AM <br>
                                                                                        <strong>Sub. :</strong> Business <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        10:00 AM - 10:30 AM <br>
                                                                                        <strong>Sub. :</strong> Account <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-6</td>
                                                                                    <td class="data_table1">
                                                                                        10:30 AM - 11:00 AM <br>
                                                                                        <strong>Sub. :</strong> Business <br>
                                                                                        <strong>Class :</strong> XI-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        10:30 AM - 11:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> X-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        10:30 AM - 11:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-7</td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        11:00 AM - 11:30 AM <br>
                                                                                        <strong>Sub. :</strong> Business <br>
                                                                                        <strong>Class :</strong> XI-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        11:00 AM - 11:30 AM <br>
                                                                                        <strong>Sub. :</strong> Economic <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td>----</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Period-8</td>
                                                                                    <td class="data_table1">
                                                                                        11:30 AM - 12:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td class="data_table1">
                                                                                        11:30 AM - 12:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td class="data_table1">
                                                                                        11:30 AM - 12:00 AM <br>
                                                                                        <strong>Sub. :</strong> Hindi <br>
                                                                                        <strong>Class :</strong> XII-A
                                                                                    </td>
                                                                                    <td>----</td>
                                                                                    <td>----</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="attendance" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                                            </div>
                                                            <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                                                            <div id="staff_attendence_calendar" class="m-t-15"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="salary" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Salary Info </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="edithistory" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  History Info </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="leave" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Leave Application </a>
                                                            </div>
                                                            <div class="nav_item2">
                                                                <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                <div class="table-responsive">
                                                                    <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 8%">{{trans('language.s_no')}}</th>
                                                                                <th style="width: 20%">{{trans('language.leave_app_reason')}}</th>
                                                                                <th>{{trans('language.leave_date_range')}}</th>
                                                                                <th style="width: 15%">{{trans('language.leave_app_attachment')}}</th>
                                                                                <th style="width: 15%">{{trans('language.leave_days')}}</th>
                                                                                <th style="width: 8%">Status </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @if($staff['leaves'] != '')
                                                                                @php $leaveKey = 0; @endphp
                                                                                @foreach($staff['leaves'] as $leaves )
                                                                                @php $leaveKey++; @endphp
                                                                                <tr>
                                                                                    <td>{!! $leaveKey !!}</td>
                                                                                    <td>{!! $leaves['staff_leave_reason'] !!}</td>
                                                                                    <td>{!! $leaves['staff_leave_from_date'] !!} - {!! $leaves['staff_leave_to_date'] !!}</td>

                                                                                    @php 
                                                                                    $staff_leave_attachment = '';
                                                                                    $status = 'Pending';
                                                                                    @endphp
                                                                                    @if($leaves['staff_leave_attachment'] != '')
                                                                                        @php
                                                                                            $staff_leave_attachment = $leaves['staff_leave_attachment'];
                                                                                            $status = 'Uploaded';
                                                                                        @endphp
                                                                                    @endif
                                                                                    <td>
                                                                                        @if($staff_leave_attachment != '')
                                                                                        <a target="_blank" href="{!! URL::to($staff_leave_attachment) !!}">Attachment</a>
                                                                                        @else 
                                                                                            ------
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>{!! $leaves['staff_leave_days'] !!}</td>

                                                                                    
                                                                                    <td>{!! $leaves['leave_status'] !!}</td>
                                                                                </tr>
                                                                                @endforeach
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                    
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->


<script>

    $(document).ready(function() {
        $('#staff_attendence_calendar').fullCalendar({
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
        //    defaultDate: '2018-01-12',
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            eventLimit: true, // allow "more" link when too many events
            events: [
                @foreach($staff['attendance'] as $arr_staff_lists)
                    {
                        title: "{{$arr_staff_lists['staff_attendance_type']}}",
                        start: "{{$arr_staff_lists['staff_attendance_date']}}",
                        className: "{{$arr_staff_lists['attendence_class']}}"
                    },
                @endforeach()    
            ]
        });
        // Change to month view
        $('#change-view-month').on('click',function(){
            $('#staff_attendence_calendar').fullCalendar('changeView', 'month');

            // safari fix
            $('#content .main').fadeOut(0, function() {
                setTimeout( function() {
                    $('#content .main').css({'display':'table'});
                }, 0);
            });
        });

        // Change to week view
        $('#change-view-week').on('click',function(){
            $('#staff_attendence_calendar').fullCalendar( 'changeView', 'agendaWeek');

            // safari fix
            $('#content .main').fadeOut(0, function() {
                setTimeout( function() {
                    $('#content .main').css({'display':'table'});
                }, 0);
            });
        });

        // Change to day view
        $('#change-view-day').on('click',function(){
            $('#staff_attendence_calendar').fullCalendar( 'changeView','agendaDay');

            // safari fix
            $('#content .main').fadeOut(0, function() {
                setTimeout( function() {
                    $('#content .main').css({'display':'table'});
                }, 0);
            });
        });

        // Change to today view
        $('#change-view-today').on('click',function(){
            $('#staff_attendence_calendar').fullCalendar('today');
        });
    });
</script>
@endsection

<script>
    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }
</script>