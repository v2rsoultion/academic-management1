@if(isset($book_vendor['book_vendor_id']) && !empty($book_vendor['book_vendor_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('book_vendor_id',old('book_vendor_id',isset($book_vendor['book_vendor_id']) ? $book_vendor['book_vendor_id'] : ''),['class' => 'gui-input', 'id' => 'book_vendor_id', 'readonly' => 'true']) !!}
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_name', old('book_vendor_name',isset($book_vendor['book_vendor_name']) ? $book_vendor['book_vendor_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_name'), 'id' => 'book_vendor_name']) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_number', old('book_vendor_number',isset($book_vendor['book_vendor_number']) ? $book_vendor['book_vendor_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_no'), 'id' => 'book_vendor_number', 'maxlength'=>'10']) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_email') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_email', old('book_vendor_email',isset($book_vendor['book_vendor_email']) ? $book_vendor['book_vendor_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_email'), 'id' => 'book_vendor_email']) !!}
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.book_vendor_address') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('book_vendor_address',old('book_vendor_address',isset($book_vendor['book_vendor_address']) ? $book_vendor['book_vendor_address']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.book_vendor_address'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/library') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        jQuery.validator.addMethod("phoneNo", function(value, element) {
        return this.optional(element) || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/i.test(value);
        }, "Please enter a valid contact number"); 

        $("#book-vendor-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                book_vendor_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                book_vendor_number: {
                    required: true,
                    digits: true,
                    phoneNo:true
                },
                book_vendor_email: {
                    required: true,
                    email: true,
                    emailvalid: true
                },
                book_vendor_address: {
                    required: true,
                }                
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>