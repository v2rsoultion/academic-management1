
{!! Form::hidden('module_permission_id',1,['class' => 'gui-input', 'id' => 'module_permission_id', 'readonly' => 'true']) !!}

@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@php 
    $permissionData = explode(",",$all_permissions->permissions);
@endphp 
<!-- Basic Info section -->
<div class="row clearfix">
    @foreach($all_modules as $key => $modules)
    @if($modules->mandatory == 1)
        @php $moduleDisable = 'disabled';
        $checked = 'checked'
        @endphp 
    @else 
    @php $moduleDisable = '';
    $checked = '';
    @endphp 
    @endif
    @if(isset($permissionData)) @if(in_array($modules->master_module_id, $permissionData)) @php $checked = 'checked' @endphp @endif @endif
     <div class="col-lg-3 col-md-4 col-sm-4">
         <div class="checkbox">
             <input {!! $moduleDisable !!} id="check{{$modules->master_module_id}}" {!! $checked !!} type="checkbox"  name="permissions[]" class="checkBoxClass" value="{{$modules->master_module_id}}">
             <label for="check{{$modules->master_module_id}}" class="checkbox_1">{{$modules->module_name}}</label>
         </div>
     </div>
     @endforeach
    

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/notice-board') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#group-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });
    

</script>