@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
    }
    .theme-blush .nav-tabs .nav-link.active{
    color: #fff !important;
    border-radius: 0px !important;
    background: #6572b9 !important;
    width: 220px;
    }
    #fees_counter{
        width: 100% !important;
    }
    /* li {
    border-right: 1px solid #fff;
    }*/
    .width90 {
        width: 90% !important;
    }
    .width100 {
        width: 100% !important;
    }
    .select2{
        width: 90% !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Fee Counter</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">Fee Counter</a></li>
                    <li class="breadcrumb-item"><a href="">Add Fee Counter</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12" id="bodypadd">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="header">
                                
                            </div>
                            <div class="body form-gap">
                                <ul class="nav nav-tabs tabing">
                                    @php $tabCounter = 0; @endphp
                                    @foreach($students as $studentData)
                                    <li class="nav-item"><a class="nav-link @if($tabCounter == 0) active @endif" data-toggle="tab" href="#student{{$studentData['student_id']}}"><i class="zmdi zmdi-account"></i>
                                        {!! $studentData['student_name'] !!} - {!! $studentData['current_class'].' '.$studentData['current_section'] !!}</a>
                                    </li>
                                    @php $tabCounter++; @endphp
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    
                                    @foreach($students as $studentData)
                                    <div class="tab-pane @if($contentCounter == 0) active @endif" id="student{{$studentData['student_id']}}">
                                        <div class="row feecounterclass">
                                            <div class="col-lg-3  float-md-left">
                                                <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                                            </div>
                                            <div class="col-lg-8 feecountergap">
                                                <label><b>Enroll</b> : {!! $studentData['student_enroll_number'] !!} </label>
                                                <label><b>Name</b> : {!! $studentData['student_name'] !!}</label>
                                                <label><b>Father Name</b> : {!! $studentData['student_father_name'] !!}</label>
                                                <label><b>Class</b> : {!! $studentData['current_class'].' - '.$studentData['current_section'] !!}</label>
                                            </div>
                                        </div>
                                        @php $contentCounter = $total_fine_amt = $total_chargable_fee = $total_concession_amt = 0; @endphp
                                        <div class="row tablefee">
                                            
                                            {!! Form::open(['files'=>TRUE,'id' => 'fees_counter' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                            @if(session()->has('success'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session()->get('success') }}
                                                </div>
                                            @endif
                                            @if($errors->any())
                                                <div class="alert alert-danger" role="alert">
                                                    {{$errors->first()}}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                                <p class="red" id="error-block" style="display: none"></p>
                                                <p class="green" id="success-block" style="display: none"></p>
                                                @if(!empty($studentData['complete_fees']))
                                                <div id="table-wrapper">
                                                    <div id="table-scroll">
                                                        <table  class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Effective Date</th>
                                                                    <th>Month</th>
                                                                    <th>Particular</th>
                                                                    <th>Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(!empty($studentData['complete_fees']))
                                                                <input type="hidden" value="{!! COUNT($studentData['complete_fees']) !!}" id="headcounter" >
                                                                <input type="hidden" name="student_id" value="{!! $studentData['student_id'] !!}" id="student_id" >
                                                                <input type="hidden" name="h_student_map_id" value="{!! $studentData['h_student_map_id'] !!}" id="h_student_map_id" >
                                                                @foreach($studentData['complete_fees'] as $feesData)
                                                                @php
                                                                    $total_chargable_fee = $total_chargable_fee + $feesData['fee_pay_amount'];
                                                                @endphp
                                                                
                                                                @php 
                                                                    // echo "<pre>";
                                                                    // print_r($feesData);
                                                                    // echo "</pre>";
                                                                @endphp
                                                                <tr>
                                                                    <td style="width: 50px !important">
                                                                        <div class="checkbox" id="customid">
                                                                        <input id="checkbox{{$studentData['student_id']}}{{$feesData['counter']}}" class="checkboxes headchecks" counter-id="{{$studentData['student_id']}}{{$feesData['counter']}}" type="checkbox" name="pay[{{$feesData['counter']}}][month_year]" value="{{$feesData['month_year']}}" >
                                                                            <label  class="from_one1" style="margin-bottom: 4px !important;"  for="checkbox{{$studentData['student_id']}}{{$feesData['counter']}}"></label>
                                                                        </div>
                                                                    </td>
                                                                   
        
                                                                    <td>{!! date('d M y', strtotime($feesData['month_date'])) !!}</td>
                                                                    <td>{!! $feesData['month_year_string'] !!}</td>
                                                                    <td>{!! $feesData['head_name'] !!}</td>
                                                                    <td>{!! $feesData['fee_pay_amount'] !!}
                                                                    
                                                                    <input type="hidden"  name="pay[{{$feesData['counter']}}][pay_amt]" class="form-control heads-amt" counter-id="{{$studentData['student_id']}}{{$feesData['counter']}}" placeholder="" value="{!! $feesData['fee_pay_amount'] !!}" min="0" max="{!! $feesData['fee_pay_amount'] !!}" id="pay_amt{{$studentData['student_id']}}{{$feesData['counter']}}"  >

                                                                    </td>
                                                                    
                                                                </tr>
                                                                
                                                                @endforeach
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="row clearfix">
                                                    <div class="col-md-5"></div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-3"><b>Total Chargeable Fee</b></div>
                                                            <div class="col-md-2">{!! $total_chargable_fee !!}</div>
                                                            <div class="col-md-3"><b>Calculate Amount</b></div>
                                                            <div class="col-md-4">
                                                                <div id="showPayAmt{{$studentData['student_id']}}">0</div>
                                                                <input type="hidden" name="pay_final_calculate_amt" value="0"  id="pay_final_calculate_amt{{$studentData['student_id']}}">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-3"> 
                                                                <div class="checkbox" id="customid">
                                                                    <input id="checkboxPayLator" type="checkbox" name="pay_lator_use" value='1'>
                                                                    <label for="checkboxPayLator">Pay-later</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <input type="text" id="pay_lator_amt{{$studentData['student_id']}}" name="pay_lator_amt" class="form-control" value="0" disabled>
                                                                <input type="hidden" id="pay_lator_amt_hide{{$studentData['student_id']}}" name="pay_lator_amt_hide" class="form-control" value="0" >
                                                            </div>
                                                            <div class="col-md-3"><b>Paying Amount</b></div>
                                                            <div class="col-md-4">
                                                                <input type="number" name="pay_final_amt"  class="form-control final-pay-amt width90 numbers"  value="0" min=0  id="pay_final_amt{{$studentData['student_id']}}">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-3"> <b>Fine Amount</b></div>
                                                            <div class="col-md-2">
                                                                <input type="number" name="fine_amt"  class="form-control fine-amt width90 numbers"  value="0" min=0  id="fine_amt{{$studentData['student_id']}}">
                                                            </div>
                                                            <div class="col-md-3"><b>Concession Amount</b></div>
                                                            <div class="col-md-4">
                                                                <input type="number" name="conc_amt"  class="form-control conc-amt width90 numbers"  value="0" min=0  id="conc_amt{{$studentData['student_id']}}">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-3"> 
                                                                &nbsp;
                                                            </div>
                                                            <div class="col-md-2">
                                                                &nbsp;
                                                            </div>
                                                            <div class="col-md-3"><b>Payment Mode</b></div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select class="form-control show-tick select2 select_form1 width90" name="pay_mode" id="pay_mode" onChange="checkType(this.value)">
                                                                        <option value=""> Select</option>
                                                                        <option value="online"> Online</option>
                                                                        <option value="offline">Offline</option>
                                                                        <option value="cheque">Cheque</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row" id="showTrans" style="display:none;">
                                                            <div class="col-md-2"> 
                                                                <b>Transaction ID</b>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" name="transaction_id" id="transaction_id" class="form-control width100" placeholder="Transaction ID" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3"><b>Transaction Date</b></div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="date" name="transaction_date" class="form-control width90" placeholder=" Transaction Date" id="transaction_date" required> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="showBank" style="display: none">
                                                            <br />
                                                            <div class="row" >
                                                                <div class="col-md-2"> 
                                                                    <b>Bank</b>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class=" field select " style="width: 100%">
                                                                        {!!Form::select('bank_id', $studentData['arr_bank'],'', ['class' => 'form-control show-tick select_form1 select2 width90','id'=>'bank_id'])!!}
                                                                    <i class="arrow double"></i>
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-3"><b>Cheque Date</b></div>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="cheque_date" class="form-control width90" placeholder=" Cheque Date" id="cheque_date" required>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <div class="row" >
                                                                <div class="col-md-2"> 
                                                                    <b>Cheque Number</b>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <input type="text" name="cheque_number" id="cheque_number" class="form-control width90" placeholder="Cheque No" maxlength="5" required>
                                                                </div>
                                                                <div class="col-md-3"><b>Cheque Amount</b></div>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="cheque_amt" id="cheque_amt" class="form-control width90 numbers cheque-amt" placeholder="Cheque Amount" required>
                                                                    <p class="red" id="cheque-amt-block" style="display: none"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" >
                                                            <div class="col-md-2"> &nbsp;</div>
                                                            <div class="col-md-3">&nbsp;</div>
                                                            <div class="col-md-3"><b>Select Date</b></div>
                                                            <div class="col-md-4">
                                                                <input type="date" name="select_date" class="form-control width90" id="select_date" placeholder="Date">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row" >
                                                            <div class="col-md-2">&nbsp;</div>
                                                            <div class="col-md-3">&nbsp;</div>
                                                            <div class="col-md-3"><b>Description</b></div>
                                                            <div class="col-md-4">
                                                                <textarea class="form-control width90" name="description" id="description "placeholder="Description"></textarea>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row" >
                                                            <div class="col-md-2">&nbsp;</div>
                                                            <div class="col-md-3">&nbsp;</div>
                                                            <div class="col-md-3">&nbsp;</div>
                                                            <div class="col-md-4">
                                                                <button type="submit" class="btn btn-raised btn-primary width90" title="Charge Fees" id="submit">Charge Fees
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @else 
                                                <p class="green" id="">No Fees Available</p>
                                                @endif
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    @php $contentCounter++; @endphp
                                    @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    function checkType(type){
        if(type == 'cheque'){
            $('#showBank').show();
            $('#showTrans').hide();
        } else if(type == 'online'){
            $('#showBank').hide();
            $('#showTrans').show();
        } else {
            $('#showTrans').hide();
            $('#showBank').hide();
        }
    }

    function calculatePayAmount(){
        var pay_amt = total_pay_amt = total_fine_amt = concession_amt = fine_amt = finalPayAmt = calculatePayAmt = remainingAmt =  0;
        var total_check_fee = $('input:checkbox.checkboxes:checked').length;
        var student_id = $('#student_id').val();
        $("input:checkbox.checkboxes:checked").each(function(){
            var counterVal = $(this).attr('counter-id');
            var headcounter = $('#headcounter').val();
            total_pay_amt += parseInt($("#pay_amt"+counterVal).val());
        });

        concession_amt = $('#conc_amt'+student_id).val();
        fine_amt = $('#fine_amt'+student_id).val();

        total_pay_amt = parseFloat(total_pay_amt) - parseFloat(concession_amt);
        total_pay_amt = parseFloat(total_pay_amt) + parseFloat(fine_amt);

        $('#pay_final_calculate_amt'+student_id).val(total_pay_amt);
        $('#showPayAmt'+student_id).html(total_pay_amt);

        finalPayAmt  = $("#pay_final_amt"+student_id).val();
        calculatePayAmt  = $("#pay_final_calculate_amt"+student_id).val();

        if(parseFloat(finalPayAmt) > parseFloat(calculatePayAmt) ){
            console.log('Extra Pay Case');
            $("#pay_final_amt"+student_id).val(calculatePayAmt);
            $("#pay_lator_amt"+student_id).val(0);
            $("#pay_lator_amt_hide"+student_id).val(0);
            $('#checkboxPayLator').prop('checked', false);
        } else if(parseFloat(finalPayAmt) < parseFloat(calculatePayAmt)){
            remainingAmt = parseFloat(calculatePayAmt) - parseFloat(finalPayAmt);
            console.log('Pay-later case',remainingAmt);
            $("#pay_lator_amt"+student_id).val(remainingAmt);
            $("#pay_lator_amt_hide"+student_id).val(remainingAmt);
            $('#checkboxPayLator').prop('checked', true);
        } else if(parseFloat(finalPayAmt) == parseFloat(calculatePayAmt)){
            console.log('Pay-later case 0');
            $("#pay_lator_amt"+student_id).val(0);
            $("#pay_lator_amt_hide"+student_id).val(0);
            $('#checkboxPayLator').prop('checked', false);
        }
    }
    $(document).on('click','.checkboxes',function(e){
        calculatePayAmount();
    })

    $(document).on('keyup','.final-pay-amt',function(e){
        calculatePayAmount();
    });

    $(document).on('keyup','.conc-amt',function(e){
        calculatePayAmount();
    });

    $(document).on('keyup','.fine-amt',function(e){
        calculatePayAmount();
    });

    $(document).on('keyup','.cheque-amt',function(e){
        var numVal = $(this).val();
        if(parseFloat(numVal) <= 0){
            $(this).val(0);
        }
        calculatePayAmount();
        var student_id = $('#student_id').val();
        var cheque_amt = $("#cheque_amt").val();
        finalPayAmt  = $("#pay_final_amt"+student_id).val();
        if(cheque_amt != finalPayAmt){
            $("#cheque-amt-block").html("Cheque amount should be equal to paying amount");
            $("#cheque-amt-block").show();
            $('#submit').prop('disabled', true);
        } else {
            $("#cheque-amt-block").html("");
            $("#cheque-amt-block").hide();
            $('#submit').prop('disabled', false);
        }
    });

    $(document).ready(function () {
        $('#fees_counter').on('submit', function(e) {
            if ($('.headchecks').filter(':checked').length < 1){
                $("#error-block").html("Please select atleast one Fees Head");
                $("#error-block").show();
                $("#success-block").hide();
                return false;
            } else {
                $("#error-block").hide();
            }
        });
      
    });

    $(document).ready(function () {
        $(document).on('keyup','.numbers',function(e){
            var numVal = $(this).val();
            if(parseFloat(numVal) <= 0){
                $(this).val(0);
            }
        });
    });

    jQuery(document).ready(function () {

        $("#fees_counter").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                pay_mode: {
                    required: true,
                },
                select_date: {
                    required: true,
                    date: true
                },
                description: {
                    required: true
                },
                cheque_date: {
                    required: true,
                    date: true
                }

            },

            // message: {
            //     required: 'This field is required';
            // }

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                   // element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });
</script>
@endsection
