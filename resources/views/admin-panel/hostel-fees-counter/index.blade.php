@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                   
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Fees</strong> Counter </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('section_id', $listData['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: none">
                            <hr>
                            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
                            <div class="table-responsive" >
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.student_enroll_number')}}</th>
                                            <th>{{trans('language.name')}}</th>
                                            <th>{{trans('language.student_father_name')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
      var table = $('#student-table').DataTable({
          pageLength: 20,
          processing: true,
          serverSide: true,
          bLengthChange: false,
          bPaginate: false,
          bInfo : false,
          bFilter: false,
          ajax: {
              url: "{{url('admin-panel/hostel/fees-counter/get-student-list')}}",
              data: function (d) {
                  d.leave_status = 0;
                  d.class_id = $('select[name=class_id]').val();
                  d.section_id = $('select[name=section_id]').val();
              }
          },
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex' },
              {data: 'student_enroll_number', name: 'student_enroll_number'},
              {data: 'student_profile', name: 'student_profile'},
              {data: 'student_father_name', name: 'student_father_name'},
              {data: 'action', name: 'action'},
          ],
           columnDefs: [
              {
                  "targets": 3, // your case first column
                  "width": "15%"
              },
              {
                  targets: [ 0, 1, 2, 3],
                  className: 'mdl-data-table__cell--non-numeric'
              }
          ]
      });
      $('#search-form').on('submit', function(e) {
            table.draw();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $("#error-block").hide();
            $("#success-block").hide();
          if(class_id == "" && section_id == ""){
              $("#list-block").hide();
          } else {
              $("#list-block").show();
          }
          e.preventDefault();
      });
      
    });
    
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }
    
    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(class_id == "" || section_id == ""){
            $(':input[name="Search"]').prop('disabled', true);
        } else {
            $("#st_section_id").val(section_id);
            $("#st_class_id").val(class_id);
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }
</script>
@endsection