@extends('admin-panel.layout.header')
@section('content')
{!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
{!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!}
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item "><a href="{!! URL::to('admin-panel/competition/view-competitions') !!}">{!! trans('language.manage_competitions') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        {!! Form::hidden('competition_id',$competition_id) !!}
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'],isset($listData['section_id']) ? $listData['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'checkSearch()'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search', 'disabled']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1"> 
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div id="list-block" style="display: none">
                                <hr>
                                <div class="alert alert-danger" role="alert" id="error-block" style="display: none">
                                </div>
                                <div class="alert alert-success" role="alert" id="success-block" style="display: none">
                                </div>
                                {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
                                {!! Form::hidden('competition_id',$competition_id,['class' => 'gui-input', 'id' => 'competition_id']) !!}
                                <div class="row clearfix">
                                <!-- <div class="checkbox pull-left col-md-4" id="customid"> -->
                                    <!-- <input type="checkbox" id="check_all"class="check" >
                                    <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"> Select All</label> -->
                                <!-- </div> -->
                                <div class="col-md-4">
                                    <div style="margin-left:-13px"><b class="headingcommon">Competition Name:-</b>{{$competition->competition_name}}</div>
                                </div>
                                <div class="col-md-6">&nbsp;</div>
                                    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary pull-right','name'=>'submit']) !!}
                                </div>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>Select</th>
                                                <th>Position/Rank</th>
                                                <th>{{trans('language.profile')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{{trans('language.class_section')}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                {!! Form::close() !!}

                             
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<!-- Model code ends here -->
<script>
    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });
    
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        // For Pop-up
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/competition/get-student-list')}}',
                data: function (d) {
                    d.competition_id = $('input[name="competition_id"]').val();
                    d.class_id      = $('select[name="class_id"]').val();
                    d.section_id    = $('select[name="section_id"]').val();
                    d.type    = 1;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'checkbox', name: 'checkbox'},
                {data: 'winner_rank', name: 'winner_rank'},
                {data: 'profile', name: 'profile'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'class_section' , name: 'class_section'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "8%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $("#error-block").hide();
            $("#success-block").hide();
          if(class_id == "" && section_id == ""){
              $("#list-block").hide();
          } else {
              $("#list-block").show();
          }
          e.preventDefault();
        });

        $('#clearBtn').click(function(){
            window.location.reload(true);
        })
        
    });
    
    $('#list-form').on('submit', function(e) {
            
        var formData = $(this).serialize();
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/competition/save-map-students')}}",
            data     : formData,
            cache    : false,

            success  : function(data) {
                data = $.trim(data);
                if(data == "success"){
                    $("#success-block").html("Student successfully mapped.");
                    $("#success-block").show();
                    $("#error-block").html("");
                    $("#error-block").hide();
                    table.draw();

                } else {
                    $("#error-block").html(data);
                    $("#error-block").show();
                    $("#success-block").html("");
                    $("#success-block").hide();
                }
            }
        })
        e.preventDefault();
      });
    function uncheck() {
        
    }
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');

        }
    }
    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(class_id != '' && section_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#form-block").hide();
            $("#Search").prop('disabled', true);
        }
    }

    $(document).on('click','.students',function(e){
        var student_id = $(this).attr('student-id');
        if($('#student'+student_id).prop("checked") == true) {
            $("#position_rank"+student_id).val('');
            $("#position_rank"+student_id).prop("readonly",true);
        } else {
            $("#position_rank"+student_id).prop("readonly",false);
        }
    });
</script>
@endsection