@if(isset($notes['online_note_id']) && !empty($notes['online_note_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

@if(!isset($notes['online_note_id']) && empty($notes['online_note_id']))
<?php $admit_section_readonly = false; $admit_section_disabled=''; 
?>
@else
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@endif


{!! Form::hidden('online_note_id',old('online_note_id',isset($notes['online_note_id']) ? $notes['online_note_id'] : ''),['class' => 'gui-input', 'id' => 'online_note_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info notes -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.t_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('t_name', old('t_name',isset($notes['online_note_name']) ? $notes['online_note_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.t_name'), 'id' => 't_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.notes_class') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('notes_class_id', $notes['arr_class'],isset($notes['class_id']) ? $notes['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'notes_class_id','onChange' => 'getSection(this.value);getSubject(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.notes_section') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('notes_section_id', $notes['arr_section'],isset($notes['section_id']) ? $notes['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'notes_section_id',$admit_section_disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>

<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.notes_subject') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('notes_subject_id', $notes['arr_subject'],isset($notes['subject_id']) ? $notes['subject_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'notes_subject_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.notes_unit') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('unit', old('unit',isset($notes['online_note_unit']) ? $notes['online_note_unit']: ''), ['class' => 'form-control','placeholder'=>trans('language.notes_unit'), 'id' => 'unit']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.file_url')}} </span> <br />
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="online_note_url" id="file1" onChange="document.getElementById('online_note_url').value = this.value;">
                <input type="hidden" class="gui-input" id="online_note_url" placeholder="Upload Photo" readonly>
            </label>
            @if(isset($notes['online_note_url']))<br /><a href="{{url($notes['online_note_url'])}}" target="_blank" >View Image</a>@endif
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.notes_topic') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('topic', old('unit',isset($notes['online_note_topic']) ? $notes['online_note_topic']: ''), ['class' => 'form-control','rows'=>'5','placeholder'=>trans('language.notes_topic'), 'id' => 'topic']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/online-content') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#notes-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                t_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                notes_class_id: {
                    required: true
                },
                notes_section_id: {
                    required: true
                },
                notes_subject_id: {
                    required: true
                },
                unit: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                topic: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>

<script type="text/javascript">

function getSection(class_id)
    {
        if(class_id != "") {
            $('.mycustloading').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/notes/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                        $("select[name='notes_section_id'").html(data.options);
                        $('.mycustloading').hide();
                }
            });
        } else {
            $("select[name='notes_section_id'").html('<option value="">Select Section</option>');
        }
    }

function getSubject(class_id)
    {
        if(class_id != "") {
            var medium_type = $('#medium_type').val();
            $('.mycustloading').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/subject/get-class-subject-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'medium_type': medium_type
                },
                success: function (data) {
                        $("select[name='notes_subject_id'").html(data.options);
                        $('.mycustloading').hide();
                }
            });
        } else {
            
            $("select[name='notes_subject_id'").html('<option value="">Select Subject</option>');
        }
    }


</script>