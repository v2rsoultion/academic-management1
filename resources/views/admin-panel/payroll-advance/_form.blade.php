@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.loan_employee') !!}</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select size">
            {!!Form::select('staff_id',$advance['staff'],isset($advance['staff_id']) ? $advance['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
            <i class="arrow double"></i>
        </label>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.advance_amount') !!}</lable>
        {!! Form::number('advance_amount',old('advance_amount',isset($advance['advance_amount']) ? $advance['advance_amount']: ''),['class' => 'form-control','placeholder' => trans('language.advance_amount'), 'id' => 'advance_amount']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.suggested_amount') !!}</lable>
        {!! Form::number('suggested_amount',old('suggested_amount',isset($advance['suggested_amount']) ? $advance['suggested_amount']: ''),['class' => 'form-control','placeholder' => trans('language.suggested_amount'), 'id' => 'suggested_amount']) !!}
        </div>
    </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-advance') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }); 
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');

        $("#manage_advance").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_id: {
                    required: true
                },
                advance_amount: {
                    required: true
                },
                suggested_amount: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });
</script>