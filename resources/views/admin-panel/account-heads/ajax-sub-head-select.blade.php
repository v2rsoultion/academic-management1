<option value="">Select Sub Head</option>
@if(!empty($subhead))
  @foreach($subhead as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif