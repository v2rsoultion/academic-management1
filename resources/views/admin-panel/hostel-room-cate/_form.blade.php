@if(isset($room_cate['h_room_cate_id']) && !empty($room_cate['h_room_cate_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('h_room_cate_id',old('h_room_cate_id',isset($room_cate['h_room_cate_id']) ? $room_cate['h_room_cate_id'] : ''),['class' => 'gui-input', 'id' => 'h_room_cate_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-2 col-md-2">
        <lable class="from_one1">{!! trans('language.h_room_cate_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_room_cate_name', old('h_room_cate_name',isset($room_cate['h_room_cate_name']) ? $room_cate['h_room_cate_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_room_cate_name'), 'id' => 'h_room_cate_name']) !!}
        </div>
    </div>
    <div class="col-lg-2 col-md-2">
        <lable class="from_one1">{!! trans('language.h_room_cate_fees') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('h_room_cate_fees', old('h_room_cate_fees',isset($room_cate['h_room_cate_fees']) ? $room_cate['h_room_cate_fees']: '1'), ['class' => 'form-control','placeholder'=>trans('language.h_room_cate_fees'), 'id' => 'h_room_cate_fees', 'min'=> '1']) !!}
        </div>
    </div>
    <div class="col-lg-5 col-md-5">
        <lable class="from_one1">{!! trans('language.h_room_cate_des') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('h_room_cate_des', old('h_room_cate_des',isset($room_cate['h_room_cate_des']) ? $room_cate['h_room_cate_des']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_room_cate_des'), 'id' => 'h_room_cate_des', 'rows'=> '2']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/hostel') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#room-category-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                h_room_cate_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                h_room_cate_fees: {
                    required: true,
                    number: true
                },
                h_room_cate_des: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>