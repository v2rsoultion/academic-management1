<option value="">Select Room Category</option>
@if(!empty($room_cate))
  @foreach($room_cate as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif