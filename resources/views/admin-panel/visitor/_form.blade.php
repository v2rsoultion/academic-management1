@if(isset($visitor['visitor_id']) && !empty($visitor['visitor_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('visitor_id',old('visitor_id',isset($visitor['visitor_id']) ? $visitor['visitor_id'] : ''),['class' => 'gui-input', 'id' => 'visitor_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('name', old('name',isset($visitor['name']) ? $visitor['name']: ''), ['class' => 'form-control','placeholder'=>trans('language.name'), 'id' => 'name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.contact') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('contact', old('contact',isset($visitor['contact']) ? $visitor['contact']: ''), ['class' => 'form-control','placeholder'=>trans('language.contact'), 'id' => 'contact', 'maxlength'=>'10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('email', old('email',isset($visitor['email']) ? $visitor['email']: ''), ['class' => 'form-control','placeholder'=>trans('language.email'), 'id' => 'email']) !!}
        </div>
    </div>
</div>
<div class="row clearfix"> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.check_in_time') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::time('check_in_time', old('check_in_time',isset($visitor['check_in_time']) ? $visitor['check_in_time']: ''), ['class' => 'form-control','placeholder'=>trans('language.check_in_time'), 'id' => 'check_in_time']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.check_out_time') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::time('check_out_time', old('check_out_time',isset($visitor['check_out_time']) ? $visitor['check_out_time']: ''), ['class' => 'form-control ','placeholder'=>trans('language.check_out_time'), 'id' => 'check_out_time']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.purpose') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('purpose',old('purpose',isset($visitor['purpose']) ? $visitor['purpose']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.purpose'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/visitor') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        jQuery.validator.addMethod("phoneNo", function(value, element) {
        return this.optional(element) || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/i.test(value);
        }, "Please enter a valid contact number"); 

        jQuery.validator.addMethod("greaterThanTime",  function(value, element) {
            
            var start_time = $("#check_in_time").val(); //start time
            var end_time = $("#check_out_time").val();  //end time

            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt) {
                return false;
            }
            return true;
        },'Check-Out time must be greater than Check-In time.');

        jQuery.validator.addMethod("lessThanTime",  function(value, element) {
            
            var start_time = $("#check_in_time").val(); //start time
            var end_time = $("#check_out_time").val();  //end time
            console.log(end_time);
            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt && end_time != '') {
                return false;
            }
            return true;
        },'Check-In time must be less than Check-Out time.');


        $("#visitor-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules   ------------------------------------------ */
            rules: {
                name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                contact: {
                    required: true,
                    digits:true,
                    phoneNo:true
                },
                email: {
                    email:true,
                    emailvalid: true
                },
                purpose: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                check_in_time: {
                    required: true,
                    time: true,
                    lessThanTime: true
                },
                check_out_time: {
                    required: true,
                    time: true,
                    greaterThanTime: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        //Added later by Pratyush on 18 July 2018
        // $('#check_in_time').bootstrapMaterialDatePicker({ date: false,shortTime: false,format: 'HH:mm'}).on('change', function(e, date){ alert(date); $(this).valid();  $('#check_out_time').bootstrapMaterialDatePicker('setMinDate', date); });
        // $('#check_out_time').bootstrapMaterialDatePicker({ date: false,shortTime: false,format: 'HH:mm' }).on('change', function(e, date){ $(this).valid(); });
        
    });

    

</script>