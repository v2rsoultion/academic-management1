@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! trans('language.virtual_class_stu_list') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/virtual-class/view-virtual-classes') !!}">{!! trans('language.manage_virtual_classes') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.virtual_class_stu_list') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','disabled'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('roll_no', old('roll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_roll_no'), 'id' => 'roll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div> -->
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('virtual_class_id',$virtual_class_id)!!}
                                        {!! Form::hidden('operation_type','1')!!}
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('father_name', old('father_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_father_name'), 'id' => 'father_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.virtual_class_sr_no')}}</th>
                                                <th>{{trans('language.virtual_class_stu_profile')}}</th>
                                                <th>{{trans('language.virtual_class_stu_enroll_no')}}</th>
                                                <th>{{trans('language.virtual_class_stu_roll_no')}}</th>
                                                <th>{{trans('language.virtual_class_stu_f_name')}}</th>
                                                <th>{{trans('language.virtual_class_stu_class')}}</th>
                                                <!-- <th>{{trans('language.virtual_class_stu_session')}}</th> -->
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: "{{url('admin-panel/virtual-class/student-list-data')}}",
                data: function (d) {
                    d.roll_no = $('input[name=roll_no]').val();
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.father_name = $('input[name=father_name]').val();
                    d.class_id          = $('select[name="class_id"]').val();
                    if($('select[name=section_id]').find(':selected').val() != 'Select section'){
                        d.section_id        = $('select[name=section_id]').find(':selected').val();
                    }else{
                        d.section_id        = '';
                    }
                    d.virtual_class_id = $('input[name=virtual_class_id]').val();
                    d.operation_type = $('input[name=operation_type]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'profile', name: 'profile'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'student_roll_no', name: 'student_roll_no'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'class_section' , name: 'class_section'},
                // {data: 'student_session', name: 'student_session'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
        }
    }

</script>
@endsection