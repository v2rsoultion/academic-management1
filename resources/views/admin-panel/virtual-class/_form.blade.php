@if(isset($virtual_class['virtual_class_id']) && !empty($virtual_class['virtual_class_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('virtual_class_id',old('virtual_class_id',isset($virtual_class['virtual_class_id']) ? $virtual_class['virtual_class_id'] : ''),['class' => 'gui-input', 'id' => 'virtual_class_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.virtual_class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('virtual_class_name', old('virtual_class_name',isset($virtual_class['virtual_class_name']) ? $virtual_class['virtual_class_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.virtual_class_name'), 'id' => 'virtual_class_name']) !!}
        </div>
    </div>

    <div class="col-lg-8 col-md-8">
        <lable class="from_one1">{!! trans('language.virtual_class_description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('virtual_class_description',old('virtual_class_description',isset($virtual_class['virtual_class_description']) ? $virtual_class['virtual_class_description']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.virtual_class_description'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Only alphanumerical characters");

        $("#virtual-class-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                virtual_class_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                virtual_class_description:{
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>