@if(isset($section['section_id']) && !empty($section['section_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('section_id',old('section_id',isset($section['section_id']) ? $section['section_id'] : ''),['class' => 'gui-input', 'id' => 'section_id', 'readonly' => 'true']) !!}


{!! Form::hidden('stream_status',old('stream_status',isset($section['stream_id']) ? '1' : '0'),['class' => 'gui-input', 'id' => 'stream_status', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_class_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('class_id', $section['arr_class'],isset($section['class_id']) ? $section['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getStreamStatus(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('section_name', old('section_name',isset($section['section_name']) ? $section['section_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.section_name'), 'id' => 'section_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_shift') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('shift_id', $section['arr_shift'],isset($section['shift_id']) ? $section['shift_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'shift_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" id="stream_block" @if(isset($section['stream_id'])) @if($section['stream_id'] != "")  style="display: block" @endif @else style="display: none" @endif>
        <lable class="from_one1">{!! trans('language.section_stream') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('stream_id', $section['arr_stream'],isset($section['stream_id']) ? $section['stream_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_intake') !!} :</lable>
        <div class="form-group">
            {!! Form::number('section_intake', old('section_intake',isset($section['section_intake']) ? $section['section_intake']: 0), ['class' => 'form-control ','placeholder'=>trans('language.section_intake'), 'id' => 'section_order', 'min' => 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_order') !!} :</lable>
        <div class="form-group">
            {!! Form::number('section_order', old('section_order',isset($section['section_order']) ? $section['section_order']: 0), ['class' => 'form-control ','placeholder'=>trans('language.section_order'), 'id' => 'section_order', 'min' => 0]) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#section-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                medium_type: {
                    required: true,
                },
                class_id: {
                    required: true,
                },
                shift_id: {
                    required: true,
                },
                stream_id: {
                    required: true,
                },
                section_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                section_intake: {
                    required: true,
                    digits: true
                },
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }
    function getStreamStatus(classId)
    {
        if(classId != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-stream-status')}}",
                type: 'GET',
                data: {
                    'class_id': classId
                },
                success: function (data) {
                    if(data == 'yes'){
                        $('#stream_status').val('1');
                        $("#stream_block").css("display", "block");
                        $(".mycustloading").hide();
                    } else {
                        $('#stream_status').val('0');
                        $("#stream_block").css("display", "none");
                        $(".mycustloading").hide();
                    }
                }
            });
        } else {
            $('#stream_status').val('0');
            $("#stream_block").css("display", "none");
            $(".mycustloading").hide();
        }
    }

    

</script>