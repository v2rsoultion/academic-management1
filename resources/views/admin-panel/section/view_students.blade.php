@extends('admin-panel.layout.header')
@section('content')

<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-12">
                <h2>{!! trans('language.view_students') !!}</h2>
            </div>
          
            <div class="col-lg-8 col-md-9 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/section/view-sections') !!}">{!! trans('language.view_section') !!}</a></li>
                    <li class="breadcrumb-item"><a href="#"> {!! $studentListData['arr_class_section_info']['class_name'] !!}, {!! $studentListData['arr_class_section_info']['section_name'] !!} </a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_students') !!}</li>
                </ul>
            </div>
            
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('roll_no', old('roll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_roll_no'), 'id' => 'roll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('father_name', old('father_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_father_name'), 'id' => 'father_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('section_id', old('section_id', $studentListData['section_id']), ['id' => 'section_id']) !!}
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list " id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.profile')}}</th>
                                                <th>{{trans('language.student_roll_no')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{{trans('language.student_father_name')}}</th>
                                                <!-- <th>{{trans('language.medium_type')}}</th> -->
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/student/student-list-data')}}',
                data: function (d) {
                    d.roll_no = $('input[name=roll_no]').val();
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.father_name = $('input[name=father_name]').val();
                    d.section_id = $('input[name=section_id]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'student_roll_no', name: 'student_roll_no'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'student_father_name', name: 'student_father_name'},
                // {data: 'medium_type', name: 'medium_type'},
               
            ],
             columnDefs: [
                {
                    "targets": 1, // your case first column
                    "className": "",
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    

</script>
@endsection




