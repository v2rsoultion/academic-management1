@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.category_level') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('category', $getCategoryData,isset($category->category_level) ? $category->category_level : '', ['class' => 'form-control show-tick select_form1 select2','id'=>''])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.category_name') !!}</lable>
      {!! Form::text('category_name',isset($category->category_name) ? $category->category_name : '',['class' => 'form-control','placeholder'=>trans('language.category_name'), 'id' => 'category_name']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/inventory/manage-category') }}" class="btn btn-raised btn-primary" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_category").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                category: {
                  required: true
                },
                category_name: {
                    required: true
                    }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>