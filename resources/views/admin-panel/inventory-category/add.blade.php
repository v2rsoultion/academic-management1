@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_category') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-category') !!}">{!! trans('language.manage_category') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_category' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.inventory-category._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row" >
          <div class="col-lg-3 col-md-3">
            <div class="form-group m-bottom-0">
              <label class=" field select size">
                {!!Form::select('search_category', $getCategoryData11, '', ['class' => 'form-control show-tick select_form1 select2','id'=>'search_category'])!!}
                <i class="arrow double"></i>
              </label>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_category_name','',['class' => 'form-control','placeholder'=> trans('language.s_category_name'), 'id' => 's_category_name']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="category-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.category_s_no') !!}</th>
              <th>{!! trans('language.category_level') !!}</th>
              <th>{!! trans('language.category_name') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#category-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/inventory/manage-category-view')}}",
              data: function (f) {
                  f.search_category = $('#search_category').val();
                  f.s_category_name = $('#s_category_name').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'category_level', name: 'category_level'},
            {data: 'category_name', name: 'category_name'},
            {data: 'action', name: 'action'}
        ] 
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
</script>
@endsection