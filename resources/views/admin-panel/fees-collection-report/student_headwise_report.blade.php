@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.student_headwise') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection-report') !!}">{!! trans('language.menu_fees_collection_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.student_headwise') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal', 'url' =>$save_url]) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <!-- <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('student_id', $map['arr_student'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div> -->
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search', 'disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        <a href="student-headwise-report" class="btn btn-raised btn-round btn-primary" name="Clear">Clear</a>
                                        
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div id="grid-block"> 
                                <div class="table-responsive" style="margin-top: 30px;" >
                                    <table class="table m-b-0 c_list" id="student-headwise-table" style="width:100%;">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{{trans('language.student')}}</th>
                                                <th>{{trans('language.student_fees_applied')}} (₹)</th>
                                                <th>{{trans('language.student_paid_fees')}} (₹)</th>
                                                <th>{{trans('language.student_concession_applied')}} (₹)</th>
                                                <th>{{trans('language.student_remaining_fees')}} (₹)</th>
                                                <th>{{trans('language.student_fine_paid')}} (₹)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

$(document).ready(function () {
        $("#student-headwise-table").append(
            $('<tfoot/>').append( $("#student-headwise-table thead tr").clone() )
        );
        var table = $('#student-headwise-table').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                { extend: 'csv', footer: true },
                { extend: 'print', footer: true }
            ],
            ajax: {
                url: '{{url('admin-panel/fees-collection/student-headwise-report-data')}}',
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_enroll', name: 'student_enroll'},
                {data: 'student_profile', name: 'student_profile'},
                {data: 'total_fees_applied', name: 'total_fees_applied'},
                {data: 'total_paid_fees', name: 'total_paid_fees' },
                {data: 'total_concession_amount', name: 'total_concession_amount' },
                {data: 'total_remaining_fees', name: 'total_remaining_fees' },
                {data: 'total_fine_paid', name: 'total_fine_paid' },
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "13%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "13%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "12%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "8%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6, 7],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ],
            drawCallback: function (row, data, start, end, display) {
                var api = this.api();

                $(api.column(0).footer()).html('');
                $(api.column(1).footer()).html('');
                $(api.column(2).footer()).html('');
                $( api.column(3).footer() ).html(
                    round(api.column( 3, {page:'current'} ).data().sum(),2)
                );
                $( api.column(4).footer() ).html(
                    round(api.column( 4, {page:'current'} ).data().sum(),2)
                );
                $( api.column(5).footer() ).html(
                    round(api.column( 5, {page:'current'} ).data().sum(),2)
                );
                $( api.column(6).footer() ).html(
                    round(api.column( 6, {page:'current'} ).data().sum(),2)
                );
                $( api.column(7).footer() ).html(
                    round(api.column( 7, {page:'current'} ).data().sum(),2)
                );
            },
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });
    });

    function round(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }
    
    function checkSearch(){
        var class_id = $("#class_id").val();
        if(class_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
    function getStudent(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-student-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='student_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='student_id'").html('');
            $(".mycustloading").hide();
        }
    }

</script>
@endsection