@if(session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="header">
    <h2><strong>Email</strong> Credentials</h2>
</div>

<!-- Email Credentials section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.config_email') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('config_email', old('config_email',isset($system_config['email']) ? $system_config['email']: ''), ['class' => 'form-control','placeholder'=>trans('language.config_email'), 'id' => 'config_email']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.config_password') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('config_password', old('config_password',isset($system_config['password']) ? $system_config['password']: ''), ['class' => 'form-control','placeholder'=>trans('language.config_password'), 'id' => 'config_password']) !!}
        </div>
    </div>
</div>

<div class="header">
    <h2><strong>sms</strong> Credentials</h2>
</div>

<!-- SMS Credentials section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_gateway_url') !!} :</lable>
        <div class="form-group">
            {!! Form::text('sms_gateway_url', old('sms_gateway_url',isset($system_config['sms_gateway_url']) ? $system_config['sms_gateway_url']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_gateway_url'), 'id' => 'sms_gateway_url']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_username') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('sms_username', old('sms_username',isset($system_config['sms_username']) ? $system_config['sms_username']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_username'), 'id' => 'sms_username']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_password') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('sms_password', old('sms_password',isset($system_config['sms_password']) ? $system_config['sms_password']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_password'), 'id' => 'sms_password']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_sender') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('sms_sender', old('sms_sender',isset($system_config['sms_sender']) ? $system_config['sms_sender']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_sender'), 'id' => 'sms_sender']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_priority') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('sms_priority', old('sms_priority',isset($system_config['sms_priority']) ? $system_config['sms_priority']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_priority'), 'id' => 'sms_priority']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.sms_stype') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('sms_stype', old('sms_stype',isset($system_config['sms_stype']) ? $system_config['sms_stype']: ''), ['class' => 'form-control','placeholder'=>trans('language.sms_stype'), 'id' => 'sms_stype']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        
       
        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) >= parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) <= parseFloat($otherElement.val(), 10);
        },"The value  must be less");
       
        $("#system-configuration-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                config_email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                config_password: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_gateway_url: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_username: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_password: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_sender: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_priority: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                sms_stype: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                }
            }
        });
    });

</script>