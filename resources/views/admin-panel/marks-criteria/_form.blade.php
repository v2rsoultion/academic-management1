@if(isset($markscriteria['marks_criteria_id']) && !empty($markscriteria['marks_criteria_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
{!! Form::hidden('marks_criteria_id',old('marks_criteria_id',isset($markscriteria['marks_criteria_id']) ? $markscriteria['marks_criteria_id'] : ''),['class' => 'gui-input', 'id' => 'marks_criteria_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.criteria_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('criteria_name', old('criteria_name',isset($markscriteria['criteria_name']) ? $markscriteria['criteria_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.criteria_name'), 'id' => 'criteria_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.max_marks') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('max_marks', old('max_marks',isset($markscriteria['max_marks']) ? $markscriteria['max_marks']: '0'), ['class' => 'form-control','placeholder'=>trans('language.max_marks'), 'id' => 'max_marks', 'min'=>'0']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.passing_marks') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('passing_marks', old('passing_marks',isset($markscriteria['passing_marks']) ? $markscriteria['passing_marks']: '0'), ['class' => 'form-control','placeholder'=>trans('language.passing_marks'), 'id' => 'passing_marks', 'min'=>'0']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
    
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) > parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) < parseFloat($otherElement.val(), 10);
        },"The value  must be less");
        $("#marks-criteria-form").validate({
    
            /* @validation states + elements 
             ------------------------------------------- */
    
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
    
            /* @validation rules 
             ------------------------------------------ */
    
            rules: {
                
                criteria_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                max_marks: {
                    required: true,
                    digits:true,
                    greaterThan: '#passing_marks'
                },
                passing_marks: {
                    required: true,
                    number:true,
                    lessThan: '#max_marks'
                }
            },
    
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
    
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    
    });
    
    
    
</script>