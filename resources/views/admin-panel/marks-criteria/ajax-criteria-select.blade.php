<option value="">Select Criteria</option>
@if(!empty($arr_criteria))
  @foreach($arr_criteria as $key => $value)
    <option @if($marks_criteria_id == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif