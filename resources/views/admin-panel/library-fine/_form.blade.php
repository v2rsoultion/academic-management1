<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.library_fine_member_type') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('member_type', $listData['member_type'],'',['class' => 'form-control show-tick select_form1 select2','id'=>'member_type','onchange' => 'return getMember(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.library_fine_member') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('member_id',$listData['member'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'member_id','disabled'=>'disabled'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.library_fine_amount') !!} :</lable>
        <div class="form-group">
            {!! Form::number('amount','',['class' => 'form-control','placeholder'=>trans('language.library_fine_amount'), 'id' => 'amount','min'=>'1']) !!}
        </div>
        
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.library_fine_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('date','',['class' => 'form-control','placeholder'=>trans('language.library_fine_date'), 'id' => 'date']) !!}
        </div>
        
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <label class="from_one1">{!! trans('language.library_fine_status') !!} :</label>
        <div class="form-group">
            <div class="radio">
                {!! Form::radio('status','1',true,['class' => 'form-control','id'=>'status_due']) !!}
                <label for="status_due">{!! trans('language.library_fine_due') !!}</label>
                {!! Form::radio('status','2',false,['class' => 'form-control','id'=>'status_paid']) !!}
                <label for="status_paid">{!! trans('language.library_fine_paid') !!}</label>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <lable class="from_one1">{!! trans('language.library_fine_remark') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('remark','',array('class'=>'form-control no-resize','placeholder'=>trans('language.library_fine_remark'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/library-fine') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
        <div class="col-sm-12">
            <hr />
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        $("#library-fine-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                member_type: {
                    required: true,
                },
                member_id: {
                    required: true,
                },
                amount: {
                    required: true,
                    number: true
                },
                remark: {
                    required: true,
                },
                date: {
                    required: true,
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        $('#date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,minDate : new Date() }).on('change', function(ev) {
            $(this).valid();  
        });

    });

    function getMember(member_id)
    {
        
        if(member_id != ''){
            //$('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/library-fine/get-member')}}",
                type: 'GET',
                data: {
                    'member_id': member_id
                },
                success: function (data) {
                    $("select[name='member_id'").html('');
                    $("select[name='member_id'").html(data.options);
                    $("select[name='member_id'").removeAttr("disabled");
                    $("select[name='member_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        } else {
            $("select[name='member_id'").html('');
            $("select[name='member_id'").selectpicker('refresh');
        }
    }
    

</script>