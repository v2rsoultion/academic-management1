@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body form-gap">
                    <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">Add Fine</button>
                      <div id="demo" class="collapse">
                      <br />
                    {!! Form::open(['files'=>TRUE,'id' => 'library-fine-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    @include('admin-panel.library-fine._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                      </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('search_member_type',$listData['member_type'],'1', ['class' => 'form-control show-tick select_form1 select2','id'=>'member_type'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('member_name', old('member_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.library_fine_name'), 'id' => 'member_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('search_date', old('search_date', ''), ['class' => 'form-control ','placeholder'=>trans('language.library_fine_date'), 'id' => 'search_date']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="library-fine-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.library_fine_name')}}</th>
                                            <th>{{trans('language.library_fine_amount')}}</th>
                                            <th>{{trans('language.library_fine_date')}}</th>
                                            <th>{{trans('language.library_fine_remark')}}</th>
                                            <th>{{trans('language.library_fine_status')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#library-fine-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            // ajax: "{{url('admin-panel/examination/data-exam')}}",
            ajax: {
                url: "{{url('admin-panel/library-fine/data')}}",
                data: function (d) {
                    d.member_type   = $('select[name="search_member_type"]').val();
                    d.member_name   = $('input[name=member_name]').val();
                    d.search_date   = $('input[name=search_date]').val();
                    d.search_amount = $('input[name=search_amount]').val();
                    }
            },
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'name', name: 'name'},
                {data: 'amount', name: 'amount'},
                {data: 'date', name: 'date'},
                {data: 'remark', name: 'remark'},
                {data: 'fine_status', name: 'fine_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // Last Column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });

         $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='member_type'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })
    });


</script>
@endsection

