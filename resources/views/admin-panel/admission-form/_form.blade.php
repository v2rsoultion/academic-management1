@if(isset($admission['admission_form_id']) && !empty($admission['admission_form_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('admission_form_id',old('admission_form_id',isset($admission['admission_form_id']) ? $admission['admission_form_id'] : ''),['class' => 'gui-input', 'id' => 'admission_form_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<!-- Basic Info section -->

<div class="alert alert-danger" role="alert" id="error-block" style="display: none">
    
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('form_type', $admission['arr_form_type'],old('form_type',isset($admission['form_type']) ? $admission['form_type'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_type', 'onChange'=>'showOptions(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('form_name', old('form_name',isset($admission['form_name']) ? $admission['form_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_name'), 'id' => 'form_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $admission['arr_medium'],old('medium_type',isset($admission['medium_type']) ? $admission['medium_type'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=> 'getClass(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('session_id', $admission['arr_session'],old('session_id',isset($admission['session_id']) ? $admission['session_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id', 'onChange'=> 'getBrochure(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
            <!-- 'onChange'=>'getIntake(this.value)' -->
                {!!Form::select('class_id[]', $admission['arr_class'],old('class_id',isset($admission['class_ids']) ? explode(",",$admission['class_ids']) : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','multiple' ])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.no_of_intake') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::hidden('no_of_intake', old('no_of_intake',isset($admission['no_of_intake']) ? $admission['no_of_intake']: 0), ['class' => 'form-control','placeholder'=>trans('language.no_of_intake'), 'id' => 'no_of_intake', 'min' => 0]) !!}
            {!! Form::text('no_of_intake1', old('no_of_intake1',isset($admission['no_of_intake']) ? $admission['no_of_intake']: 0), ['class' => 'form-control','placeholder'=>trans('language.no_of_intake'), 'id' => 'no_of_intake1', 'min' => 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_prefix') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('form_prefix', old('form_prefix',isset($admission['form_prefix']) ? $admission['form_prefix']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_prefix'), 'id' => 'form_prefix']) !!}
        </div>
    </div>

    

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_message_via') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('form_message_via', $admission['arr_message_via'],old('form_message_via',isset($admission['form_message_via']) ? $admission['form_message_via'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_message_via'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>
<div id="admission_fields" @if($admission['form_type'] === 0) style="display: block"  @else   style="display: none" @endif>
    
    <!-- Admission Information -->
    <div class="header">
        <h2><strong>Admission </strong> Information</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.brochure_name') !!} :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('brochure_id', $admission['arr_brochure'],old('brochure_id',isset($admission['brochure_id']) ? $admission['brochure_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'brochure_id'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.form_last_date') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::date('form_last_date', old('form_last_date',isset($admission['form_last_date']) ? $admission['form_last_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_last_date'), 'id' => 'form_last_date','required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.form_fee_amount') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('form_fee_amount', old('form_fee_amount',isset($admission['form_fee_amount']) ? $admission['form_fee_amount']: 0), ['class' => 'form-control','placeholder'=>trans('language.form_fee_amount'), 'id' => 'form_fee_amount', 'min'=>0,'required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.form_pay_mode') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('form_pay_mode[]', $admission['arr_payment_mode'],old('form_pay_mode',isset($admission['form_pay_mode']) ? explode(",",$admission['form_pay_mode']) : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_pay_mode','multiple','required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.form_email_receipt') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('form_email_receipt', $admission['arr_email_receipt'],old('form_email_receipt',isset($admission['form_email_receipt']) ? $admission['form_email_receipt'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_email_receipt','required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
    </div>
</div>

<!-- Instruction Information -->
<div class="header">
    <h2><strong>Instruction  </strong> Information</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_instruction') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('form_instruction', old('form_instruction',isset($admission['form_instruction']) ? $admission['form_instruction']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_instruction'), 'id' => 'form_instruction', 'rows' => '3']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_information') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('form_information', old('form_information',isset($admission['form_information']) ? $admission['form_information']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_information'), 'id' => 'form_information', 'rows' => '3']) !!}
        </div>
    </div>
</div>

<!-- Message Information -->
<div class="header">
    <h2><strong>Message  </strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_success_message') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('form_success_message', old('form_success_message',isset($admission['form_success_message']) ? $admission['form_success_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_success_message'), 'id' => 'form_success_message', 'rows' => '3']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_failed_message') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('form_failed_message', old('form_failed_message',isset($admission['form_failed_message']) ? $admission['form_failed_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_failed_message'), 'id' => 'form_failed_message', 'rows' => '3']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/admission') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $.validator.addMethod("maxDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate < inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select future date");

        $("#admission-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                form_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                session_id: {
                    required: true,
                },
                class_id: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                no_of_intake: {
                    required: true
                },
                form_prefix: {
                    required: true
                },
                form_type: {
                    required: true
                },
                form_last_date: {
                    required: true,
                    date: true,
                    maxDate: true,
                },
                form_success_message: {
                    required: true
                },
                form_failed_message: {
                    required: true
                },
                form_message_via: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        // $('#form_last_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,minDate : new Date()});
        
    });

    function showOptions(type){
        $(".mycustloading").show();
        if(type == 0){
            $('#admission_fields').show();
            $(".mycustloading").hide();
        } else {
            $('#admission_fields').hide();
            $(".mycustloading").hide();
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("#class_id").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#class_id").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getBrochure(session_id)
    {
        if(session_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/brochure/get-brochure-data')}}",
                type: 'GET',
                data: {
                    'session_id': session_id
                },
                success: function (data) {
                    $("select[name='brochure_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='brochure_id'").html('<option value="">Select Brochure</option>');
            $(".mycustloading").hide();
        }
    }
    function getIntake(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-total-intake-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $(':input[name="no_of_intake1"]').prop('disabled', false);
                    $("#no_of_intake").val(data);
                    $("#no_of_intake1").val(data);
                    $(':input[name="no_of_intake1"]').prop('disabled', true);
                    $(".mycustloading").hide();
                    if(data == 0){
                        $('#error-block').html("Sorry, We have already exceed class's student capacity.");
                        $('#error-block').show();
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $(':input[type="submit"]').prop('disabled', false);
                        $('#error-block').html('');
                        $('#error-block').hide();
                    }
                }
            });
        } else {
            
            $(':input[name="no_of_intake1"]').prop('disabled', false);
            $("#no_of_intake").vl('0');
            $("#no_of_intake1").vl('0');
            $(':input[name="no_of_intake1"]').prop('disabled', true);
            $(".mycustloading").hide();
        }
    }

</script>