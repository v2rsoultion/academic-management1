@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.manage_enquiries') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/admission') !!}">{!! trans('language.menu_admission') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_enquiries') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('session_id', $formData['arr_session'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('class_id', $formData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('form_number', old('form_number', ''), ['class' => 'form-control ','placeholder'=>trans('language.form_number'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.student_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="enquiry-form" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.enquiry_number')}}</th>
                                            <th>{{trans('language.student_name')}}</th>
                                            <th>{{trans('language.class_session')}}</th>
                                            <th>{{trans('language.father_name')}}</th>
                                            <th>{{trans('language.mobile')}}</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
     $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#enquiry-form').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/admission-form/enquiry-data')}}',
                data: function (d) {
                    d.session_id = $('select[name=session_id]').val();
                    d.medium_type = $('select[name=medium_type]').val();
                    d.class_id = $('select[name=class_id]').val();
                    d.form_number = $('input[name=form_number]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.form_type = 1;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'form_number', name: 'form_number'},
                {data: 'student_name', name: 'student_name'},
                {data: 'class_session', name: 'class_session'},
                {data: 'father_name', name: 'father_name'},
                {data: 'mobile', name: 'mobile'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 7, // your case first column
                    "width": "12%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6, 7 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }


</script>
@endsection




