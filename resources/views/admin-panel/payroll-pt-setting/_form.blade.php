@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.salary_range_min') !!}</lable>
      {!! Form::number('salary_range_min',old('salary_range_min',isset($pt_setting['salary_min']) ? $pt_setting['salary_min']: ''),['class' => 'form-control range','placeholder' => trans('language.salary_range_min'), 'id' => 'salary_range_min', 'min' => 1]) !!}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.salary_range_max') !!}</lable>
      {!! Form::number('salary_range_max',old('salary_range_max',isset($pt_setting['salary_max']) ? $pt_setting['salary_max']: ''),['class' => 'form-control range','placeholder' => trans('language.salary_range_max'), 'id' => 'salary_range_max', 'min' => 1]) !!}
    </div>
  </div>
</div>
@php
    $obj_month = json_decode($pt_setting['monthly_amount']);
    $month = (array) $obj_month;
@endphp
<div class="row">
    <div class="headingcommon  col-lg-12" style="margin-left: -1px">Amount For Monthly :-</div>
    <div class="col-lg-6" style="border-right: 1px solid #e6e6e6;">
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">January</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[jan]',old('month[jan]',isset($month['jan']) ? $month['jan']: ''),['class' => 'form-control', 'id' => 'january', 'placeholder' => trans('language.amount'), 'onkeyup' => 'autoFillMonthAmount()'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">Febuary</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[feb]',old('month[feb]',isset($month['feb']) ? $month['feb']: ''),['class' => 'form-control', 'id' => 'febuary', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">March</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[mar]',old('month[mar]',isset($month['mar']) ? $month['mar']: ''),['class' => 'form-control', 'id' => 'march', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">April</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[apr]',old('month[apr]',isset($month['apr']) ? $month['apr']: ''),['class' => 'form-control', 'id' => 'april', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">May</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[may]',old('month[may]',isset($month['may']) ? $month['may']: ''),['class' => 'form-control', 'id' => 'may', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">June</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[jun]',old('month[jun]',isset($month['jun']) ? $month['jun']: ''),['class' => 'form-control', 'id' => 'june', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
    </div>
    <div class="col-lg-6"> 
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">July</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[jul]',old('month[jul]',isset($month['jul']) ? $month['jul']: ''),['class' => 'form-control', 'id' => 'july', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">August</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[aug]',old('month[aug]',isset($month['aug']) ? $month['aug']: ''),['class' => 'form-control', 'id' => 'august', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">September</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[sep]',old('month[sep]',isset($month['sep']) ? $month['sep']: ''),['class' => 'form-control', 'id' => 'september', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">October</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[oct]',old('month[oct]',isset($month['oct']) ? $month['oct']: ''),['class' => 'form-control', 'id' => 'october', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">November</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[nov]',old('month[nov]',isset($month['nov']) ? $month['nov']: ''),['class' => 'form-control', 'id' => 'november', 'placeholder' => trans('language.amount')])!!}
                </div>
            </div>  
        </div>
        <div class="row col-lg-12">
            <div class="col-lg-4">
                <lable class="from_one1">December</lable>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    {!! Form::number('month[dec]',old('month[dec]',isset($month['dec']) ? $month['dec']: ''),['class' => 'form-control', 'id' => 'december', 'placeholder' => trans('language.amount'), 'readonly'])!!}
                </div>
            </div>  
        </div>
    </div>
</div>
<div class="row">
  <div class="col-lg-1">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1">
    <a href="{{ url('admin-panel/payroll/manage-pt-setting') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    $(".range").keyup(function(){
            $("#salary_range_min").valid();
            $("#salary_range_max").valid();
        });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("numbersonly", function(value, element) {
          return this.optional(element) || /^[0-9]+[.]?[0-9]+$/i.test(value);
        }, "Please use only number values");

        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) > parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) < parseFloat($otherElement.val(), 10);
        },"The value  must be less");

        $("#manage_pt_setting").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                salary_range_min: {
                    required: true,
                    number: true,
                    lessThan: '#salary_range_max'
                },
                salary_range_max: {
                    required: true,
                    number: true,
                    greaterThan: '#salary_range_min'
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });
    
    function autoFillMonthAmount() {
        var $amount = $('#january').val();
        $('#febuary').val($amount);
        $('#march').val($amount);
        $('#april').val($amount);
        $('#may').val($amount);
        $('#june').val($amount);
        $('#july').val($amount);
        $('#august').val($amount);
        $('#september').val($amount);
        $('#october').val($amount);
        $('#december').val($amount);
    }
</script>