@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.pt_setting') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-pt-setting') !!}">{!! trans('language.pt_setting') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_pt_setting' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-pt-setting._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      <div class="clearfix"></div>
    <!--  DataTable for view Records  -->
          <div class="table-responsive">
            <table class="table m-b-0 c_list" id="pt_setting_table" style="width:100%">
              {{ csrf_field() }}
              <thead>
                <tr>
                  <th>{!! trans('language.s_no') !!}</th>
                  <th>{!! trans('language.salary_range') !!}</th>
                  <th>{!! trans('language.view_amount') !!}</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody> </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#pt_setting_table').DataTable({
      serverSide: true,
      searching: false, 
      paging: false, 
      info: false,
      ajax: {
          url:"{{ url('admin-panel/payroll/manage-pt-setting-view')}}",
          data: function (f) {
              f.s_deduction = $('#s_deduction').val();
          }
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex' },
        {data: 'salary_range', name: 'salary_range'},
        {data: 'monthly_amount', name: 'monthly_amount'},
        {data: 'action', name: 'action'}
    ],
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "5%"
            },
            {
                "targets": 1, // your case first column
                "width": "25%"
            },
            {
                "targets": 2, // your case first column
                "width": "8%"
            },
            {
                "targets": 3, // your case first column
                "width": "15%"
            },
            {
                targets: [ 0, 1, 2, 3],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $('#clearBtn').click(function(){
        location.reload();
    });

    $(document).on('click', '.monthly', function(e) {
      var pt_setting_id = $(this).attr('pt_setting_id'); 
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{url('admin-panel/payroll/pt-setting-model-data')}}",
        type: 'GET',
        data: {
            'pt_setting_id': pt_setting_id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            $("#salary_range").html(obj.salary_min + "-" + obj.salary_max);
            var amount = JSON.parse(obj.monthly_amount);
            $("#jan").text(amount.jan);
            $("#feb").html(amount.feb);
            $("#mar").html(amount.mar);
            $("#apr").html(amount.apr);
            $("#mey").html(amount.may);
            $("#jun").html(amount.jun);
            $("#jul").html(amount.jul);
            $("#aug").html(amount.aug);
            $("#sep").html(amount.sep);
            $("#oct").html(amount.oct);
            $("#nov").html(amount.nov);
            $("#dec").html(amount.dec);
            $(".mycustloading").hide();
        }
    });
    });
  });
</script>
<!-- Action Model  -->
<div class="modal fade" id="MonthlyAmountModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Monthly Amount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div style="width:452px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-bottom: 10px;">
          <div><b>Salary Range:</b> <span id="salary_range"> </span></div>
        </div>
        <div style="width:452px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
          <div class="row col-lg-12">
            <div class="col-lg-6">
              <div><b>January: </b><span id="jan"> </span></div>
              <div><b>Febuary:</b> <span id="feb"> </span></div>
              <div><b>March:</b> <span id="mar"> </span></div>
              <div><b>April:</b> <span id="apr"> </span></div>
              <div><b>May:</b> <span id="mey"> </span></div>
              <div><b>June:</b> <span id="jun"> </span></div>
            </div>
            <div class="col-lg-6">
              <div><b>July:</b> <span id="jul"> </span></div>
              <div><b>August:</b> <span id="aug"> </span></div>
              <div><b>September:</b> <span id="sep"> </span></div>
              <div><b>October:</b> <span id="oct"> </span></div>
              <div><b>November:</b> <span id="nov"> </span></div>
              <div><b>December:</b> <span id="dec"> </span></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection