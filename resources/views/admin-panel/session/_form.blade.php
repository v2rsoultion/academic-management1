{!! Form::hidden('session_id',old('session_id',isset($session['session_id']) ? $session['session_id'] : ''),['class' => 'gui-input', 'id' => 'session_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.session_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('session_name', old('session_name',isset($session['session_name']) ? $session['session_name']: ''), ['class' => 'form-control session_name','placeholder'=>trans('language.session_name'), 'id' => 'session_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_start_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('session_start_date', old('session_start_date',isset($session['session_start_date']) ? $session['session_start_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.session_start_date'), 'id' => 'session_start_date']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_end_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('session_end_date', old('session_end_date',isset($session['session_end_date']) ? $session['session_end_date']: ''), ['class' => 'form-control ','placeholder'=>trans('language.session_end_date'), 'id' => 'session_end_date']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    
    jQuery(document).ready(function () {
       
        jQuery.validator.addMethod("sessionNameCheck", function(value, element) {
            // return this.optional(element) || /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/i.test(value);
            return this.optional(element) || /(((19|20|30)\d\d)\-((19|20|30)\d\d))$/i.test(value);
        }, "Please enter session name like 2017-2018");

        jQuery.validator.addMethod("checkSessionYear", function(value, element) {
            var name = value.split("-");
            if(name[0] == name[1] || parseInt(name[0]) > parseInt(name[1])){
                return false;
            } else {
                console.log('Hello1');
                return true;
            }
        }, "Both years should not be same or second year should be grater then first year");

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');
        
        $("#session-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                session_name: {
                    required: true,
                    sessionNameCheck:true,
                    checkSessionYear: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                session_start_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
                session_end_date: {
                    required: true,
                    date: true,
                    maxlength:10,
                    greaterThan: "#session_start_date"
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        // $('#session_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#session_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        // {
        // $('#session_end_date').bootstrapMaterialDatePicker('setMinDate', date);
        // $('#session_end_date').val('');
        // $(this).valid();  
        // });
        
    });

    

</script>