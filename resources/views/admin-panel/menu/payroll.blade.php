@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
    background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-8 col-md-6 col-sm-12">
        <h2>Payroll</h2>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Payroll Configuration.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Configuration</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/configuration') }}" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage 
                        </a>
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Department.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Department</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-department') }}" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage 
                        </a>
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Grades.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Grades</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-grade') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage 
                        </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Leave Scheme.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Leaves Scheme</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-leave-scheme') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div> 
                  @foreach($pay_config as $value)
                    @if($value['pay_config_name'] == 'Provident Fund' && $value['pay_config_status'] == 1)
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Payroll/PF Setting.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">PF Settings</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-pf-setting') }}" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage 
                        </a>
                        <div class="clearfix"></div>  
                      </div>
                    </div>
                    @endif
                    @if($value['pay_config_name'] == 'Employee State Insurance' && $value['pay_config_status'] == 1)
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Payroll/ESI Setting.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">ESI Settings</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-esi-setting') }}" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    @endif
                    @if($value['pay_config_name'] == 'Tax Deducted at Source' && $value['pay_config_status'] == 1)
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Payroll/TDS Setting.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">TDS Settings</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-tds-setting') }}" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    @endif
                    @if($value['pay_config_name'] == 'Professional Tax' && $value['pay_config_status'] == 1)
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Payroll/PT Setting.svg') !!}" alt="Payroll">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">PT Settings</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/payroll/manage-pt-setting') }}" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                          <i class="fas fa-eye"></i>Manage
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    @endif
                  @endforeach 
                  
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Salary Heads.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Salary Heads</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-salary-head') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage 
                      </a>
                      <div class="clearfix"></div> 
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Salary Structure.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Salary Structure</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-salary-structure') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage 
                      </a>
                      <div class="clearfix"></div>
                      <ul class="header-dropdown opendiv">
                        <li class="dropdown">
                          <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right slideUp">
                            <li>
                              <a href="{{ url('admin-panel/payroll/salary-structure-map-staff') }}" title="">Map Staff</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Arrears.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Arrears</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-arrear') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Bonus.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Bonus</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-bonus') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage 
                      </a>
                      <div class="clearfix"></div> 
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Loan & Advance.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Loan & Advance</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-loan') }}" class="cusa" title="manage" style="width: 60%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage Loan
                      </a>
                      <div class="clearfix"></div>
                      <ul class="header-dropdown opendiv">
                        <li class="dropdown">
                          <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right slideUp">
                            <li>
                              <a href="{{ url('admin-panel/payroll/manage-advance') }}" title="">Manage Advance</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/salary Generation.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Salary Generation</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-salary-generation') }}" class="cusa" title="manage" style="width: 48%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Manage
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <!-- <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Payroll/Report.svg') !!}" alt="Payroll">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Reports</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('admin-panel/payroll/manage-loan') }}" class="cusa" title="manage" style="width: 60%; margin: 15px auto;">
                        <i class="fas fa-eye"></i>Report
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div> -->
                </div>
                                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
