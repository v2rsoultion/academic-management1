@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_library_report') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_library_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="tab-content">
                <div class="tab-pane active" id="classlist">
                    <div class="card">
                        <div class="body">
                            <!--  All Content here for any pages -->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Admission/Exam-wise mark-sheet template_new.svg') !!}" alt="Registered Student Report">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Registered Student Report </div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ url('/admin-panel/library-report/registered-student-report') }}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Admission/Subject-wise.svg') !!}" alt="Books Report">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Books Report </div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ url('/admin-panel/library-report/books-report') }}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Admission/Exam-wise mark-sheet template_new.svg') !!}" alt="Issued Books Report">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Issued Books Report</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{!! URL::to('admin-panel/library-report/issued-books-report') !!}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Admission/Exam-wise mark-sheet template_new.svg') !!}" alt="Dues Report">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Dues Report</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{!! URL::to('admin-panel/library-report/dues-report') !!}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection