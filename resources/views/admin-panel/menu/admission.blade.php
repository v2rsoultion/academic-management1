@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_admission') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item">{!! trans('language.menu_admission') !!}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/brochure.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Brochure</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/brochure/add-brochure') }}" class="float-left" title="Add ">
                        <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="{{ url('/admin-panel/brochure/view-brochures') }}" class="float-right" title="View ">
                        <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                      
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/admission/admission_form.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Create Forms</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission/create-form') }}" class="float-left" title="Create ">
                          <i class="fas fa-plus"></i> Create 
                        </a>
                        <a href="{{ url('/admin-panel/admission/view-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                       
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/manage_enquires.svg') !!}" alt="Admission Forms">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Admission Forms</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/admission-form/view-forms') }}" class="cusa" title="Admission Forms ">
                        <i class="fas fa-plus"></i> Form 
                      </a>
                   
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/manage_enquires.svg') !!}" alt="Enquiry Forms">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Enquiry Forms</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/admission-form/view-enquiry-forms') }}" class="cusa" title="Enquiry Forms ">
                        <i class="fas fa-plus"></i> Form  
                      </a>
                   
                      <div class="clearfix"></div>
                    </div>
                  </div>

                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/manage_admissions.svg') !!}" alt="Manage Admissions">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Manage Admissions</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/admission-form/manage-admissions') }}" class="cusa" title="Manage Admissions ">
                        <i class="fas fa-plus"></i> Manage 
                      </a>
                      
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/manage_enquires.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Manage Enquiries</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/admission-form/manage-enquiries') }}" class="cusa" title="Manage Enquires ">
                        <i class="fas fa-plus"></i> Manage 
                      </a>
                   
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                      <img src="{!! URL::to('public/assets/images/admission/student.svg') !!}" alt="Selected Students">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Selected Students</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/admission-form/selected-students') }}" class="cusa" title="Manage Admissions ">
                        <i class="fas fa-plus"></i> Students 
                      </a>
                      
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Report</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{!! URL::to('admin-panel/admission-report') !!}" class="cusa" title="View ">
                        <i class="fas fa-eye"></i>Manage 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection

