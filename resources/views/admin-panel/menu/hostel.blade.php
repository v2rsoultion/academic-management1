@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_hostel') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Configuration.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Hostel Fees Head</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/hostel/manage-hostel-fees-head') !!}" class="cusa" style="width: 48%; margin: 15px auto;" title="Hostel Fees Head">
                                            <i class="fas fa-eye"></i> Manage  
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Configuration.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Configuration</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/hostel/configuration/manage-hostel') !!}" class="float-left" title="Hostels">
                                            <i class="fas fa-plus"></i> Hostels 
                                            </a>
                                            <a href="{!! URL::to('admin-panel/hostel/configuration/manage-hostel-block') !!}" class="float-right" title="Blocks">
                                            <i class="fas fa-plus"></i>Blocks
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Room No.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Rooms</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/hostel/rooms/manage-room-category') !!}" class="float-left" title="Room Category">
                                            <i class="fas fa-plus"></i> Category 
                                            </a>
                                            <a href="{!! URL::to('admin-panel/hostel/rooms/manage-room') !!}" class="float-right" title="Rooms">
                                            <i class="fas fa-plus"></i>Rooms
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="dashboard_div" >
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Register Students.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Register Students</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            
                                            <a href="{!! URL::to('admin-panel/hostel/manage-registration') !!}" class="cusa" title="Manage Registration" style="width: 48%; margin: 15px auto;">
                                            <i class="fas fa-plus"></i>Manage 
                                            </a>
											<div class="clearfix"></div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Allocate Room.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Room Allocation</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <!-- <a href="" class="float-left" title="Add ">
                                                <i class="fas fa-plus"></i> Add 
                                                </a> -->
                                            <a href="{!! URL::to('admin-panel/hostel/room-allocation') !!}" class="cusa" title="Room Allocation" style="width: 48%; margin: 15px auto;">
                                            <i class="fas fa-plus"></i>Allocation 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Transferred Student.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Transferred Student</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <!-- <a href="" class="float-left" title="Add ">
                                                <i class="fas fa-plus"></i> Add 
                                                </a> -->
                                            <a href="{!! URL::to('admin-panel/hostel/student-transfer') !!}" class="cusa" title="Transfer Student" style="width: 68%; margin: 15px auto;">
                                            <i class="fas fa-plus"></i>Transfer Student 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Leave Room.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Leave Room</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <!--  <a href="" class="float-left" title="Add ">
                                                <i class="fas fa-plus"></i> Add 
                                                </a> -->
                                            <a href="{!! URL::to('admin-panel/hostel/room-leave') !!}" class="cusa" title="Room Leave" style="width: 58%; margin: 15px auto;">
                                            <i class="fas fa-eye"></i>Leave Room
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Fee Counter.svg') !!}" alt="Fees Counter">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Fees Counter</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/hostel/fees-counter') }}" class="cusa" title="Add ">
                                            <i class="fas fa-plus"></i> Fee Counter 
                                            </a>
                                            <!-- <a href="" class="float-right" title="View ">
                                                <i class="fas fa-eye"></i>View 
                                                </a> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Receipts.svg') !!}" alt="Receipts">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Receipts</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/hostel/view-fees-receipt') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                            <i class="fas fa-plus"></i>View
                                            </a>
                                            <!--  <a href="cancel_fee_receipt.php" class="float-right" title="Cancel"> -->
                                            <!-- <a href="#" class="float-right" title="Cancel">
                                                <i class="fas fa-eye"></i>Cancel
                                                </a> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <?php /* <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Collect hostal fees.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Collect hostal fees</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <!-- <a href="" class="float-left" title="Add ">
                                                <i class="fas fa-plus"></i> Add 
                                                </a> -->
                                            <a href="{!! URL::to('admin-panel/hostel/collect-fees') !!}" class="cusa" title="Collect Fees" style="width: 50%; margin: 15px auto;">
                                            <i class="fas fa-eye"></i>Collect Fees 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div> -->
                                    */ ?>
                                
                                    <div class="col-md-4">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Hostel/Report.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Reports</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/hostel/registered-student-report') !!}" class="float-left" title="Registered student report" style="width: 48%; margin: 15px auto;"><i class="fas fa-eye"></i>Registered<br> Student Report</a>
                                            <a href="{!! URL::to('admin-panel/hostel/leave-student-report') !!}" class="float-right" title="" style="width: 48%; margin: 15px auto;"><i class="fas fa-eye"></i>Leave Student Report
                                                            </a>
                                            <!-- <a href="{!! URL::to('admin-panel/hostel/free-space-room-report') !!}" class="float-right" title="" style="width: 48%; margin: 15px auto;"><i class="fas fa-eye"></i>Free Space Room Report</a> -->
                                            <div class="clearfix"></div>
                                            <ul class="header-dropdown opendiv">
                                                <li class="dropdown">
                                                    <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                                        <!-- <li><a href="{!! URL::to('admin-panel/hostel/leave-student-report') !!}" title="">Leave Student Report
                                                            </a> 
                                                        </li> -->
                                                        <li> <a href="{!! URL::to('admin-panel/hostel/allocation-report') !!}" title="Allocation Report">Allocation Report</a></li>
                                                        <li><a href="{!! URL::to('admin-panel/hostel/fees-due-report') !!}" title="Fees Due Report">Fees Due Report</a> </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection