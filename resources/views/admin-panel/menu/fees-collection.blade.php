@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_fee_collection') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/FeesStructure.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Fees Heads</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/one-time/add-one-time') }}" class="float-left" title="One Time ">
                                            <i class="fas fa-plus"></i>One Time 
                                            </a>
                                            <a href="{{ url('/admin-panel/recurring-head/add-recurring-head') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>Recurring Heads 
                                            </a>
                                            <div class="clearfix"></div>
                                           
                                            <ul class="header-dropdown opendiv">
                                                <li class="dropdown">
                                                    <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                                        <li>
                                                            <a href="{{ url('/admin-panel/one-time/view-one-time') }}" title="">View One Time</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('/admin-panel/recurring-head/view-recurring-heads') }}" title="">View Recurring Heads</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="dashboard_div" style="min-height: 207px;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/MapStudent.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Map Free Student</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/map-student/free-by-rte') }}" class="cusa" title="Free By Rte " style="width: 48%; margin: 20px auto;">
                                            <i class="fas fa-plus"></i> By Rte 
                                            </a>
                                            <!-- <a href="{{ url('/admin-panel/fees-collection/view-free-by-management') }}" class="float-right" title="Free By Management">
                                                <i class="fas fa-eye" style="margin-right: 0px;"></i>Management 
                                                </a> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="dashboard_div" style="min-height: 207px;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Map Heads to Student.svg') !!}" alt="Map Heads to Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Map Heads to Student</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/map-head/heads') }}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Map Heads  
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/bank.svg') !!}" alt="Bank">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Banks</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/bank/add-bank') }}" class="float-left" title="Add">
                                            <i class="fas fa-plus"></i>Add
                                            </a>
                                            <a href="{{ url('/admin-panel/bank/view-bank') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Reason.svg') !!}" alt="Reason">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Reasons</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/reason/add-reason') }}" class="float-left" title="Add">
                                            <i class="fas fa-plus"></i>Add
                                            </a>
                                            <a href="{{ url('/admin-panel/reason/view-reasons') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div" style="height: auto;">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Fine.svg') !!}" alt="Fine">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Fine</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/fine/add-fine') }}" class="float-left" title="Add">
                                            <i class="fas fa-plus"></i>Add
                                            </a>
                                            <a href="{{ url('/admin-panel/fine/view-fine') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/FacilitiesFees.svg') !!}" alt="Concession">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Concession </div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <!--    <a href="" class="float-left" title="Add ">
                                                <i class="fas fa-plus"></i> Add 
                                                </a> -->
                                            <a href="{{ url('/admin-panel/concession/manage-concession') }}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage  
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Fee Counter.svg') !!}" alt="Fees Counter">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Fees Counter</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/fees-counter') }}" class="cusa" title="Add ">
                                            <i class="fas fa-plus"></i> Fees Counter 
                                            </a>
                                            <!-- <a href="" class="float-right" title="View ">
                                                <i class="fas fa-eye"></i>View 
                                                </a> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Cheque Details.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Cheques Details</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/receive-cheque/view-receive-cheque') }}" class="cusa" title="Add " style="width: 68%; margin: 10px auto;">
                                            <i class="fas fa-plus"></i> List of Cheques  
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/RTE Collection.svg') !!}" alt="RTE Collection">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">RTE Collection</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/rte-head/rte-fees-head') }}" class="float-left" title="RTE Fees Head  ">
                                            <i class="fas fa-plus"></i> RTE Fees Head 
                                            </a>
                                            <a href="{{ url('/admin-panel/rte-head/apply-fees-on-head') }}" class="float-right" title="Apply Fees on Head">
                                            <i class="fas fa-eye"></i>Apply Fees on Head 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/RTE Received Cheque.svg') !!}" alt="RTE Received Cheque">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">RTE Received Cheque</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/rte-cheque/add-rte-cheque') }}" class="float-left" title="Add">
                                            <i class="fas fa-plus"></i>Add
                                            </a>
                                            <a href="{{ url('/admin-panel/rte-cheque/view-rte-cheque') }}" class="float-right" title="View">
                                            <i class="fas fa-eye"></i>View
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Receipts.svg') !!}" alt="Receipts">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Receipts</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/fees-collection/view-fees-receipt') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                            <i class="fas fa-plus"></i>View
                                            </a>
                                            <!--  <a href="cancel_fee_receipt.php" class="float-right" title="Cancel"> -->
                                            <!-- <a href="#" class="float-right" title="Cancel">
                                                <i class="fas fa-eye"></i>Cancel
                                                </a> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/PrepaidAccount.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Manage Account</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/manage-account/wallet-account') }}" class="float-left" title="Wallet A/C">
                                            <i class="fas fa-eye"></i>Wallet A/C
                                            </a>
                                            <a href="{{ url('/admin-panel/manage-account/imprest-account') }}" class="float-right" title="Imprest A/C">
                                            <i class="fas fa-eye"></i>Imprest A/C
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/PrepaidAccount.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Imprest A/C Entries</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/fees-collection/manage-entry') }}" class="cusa" title="Manage">
                                            <i class="fas fa-plus"></i>Manage
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/FeesCollection/Fee Counter.svg') !!}" alt="Fees Counter">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Imprest A/C </div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/fees-collection/imprest-ac-counter') }}" class="cusa" title="Imprest A/C Counter  ">
                                            <i class="fas fa-plus"></i> Counter 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Report</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/fees-collection-report') !!}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    </div>
</section>
@endsection