@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_configuration') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_configuration') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/School Detail.svg') !!}" alt="School Detail">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">School Detail</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            @if($login_info['encrypted_school_id'] == "") 
                                            <a href="{{ url('admin-panel/school/add-school') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            @else
                                            <a href="{{ url('admin-panel/school/add-school/'.$login_info['encrypted_school_id']) }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Edit 
                                            </a>
                                            @endif
                                            @if($login_info['encrypted_school_id'] == "") 
                                            <a href="{{ url('admin-panel/school/view-school-detail') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            @else
                                            <a href="{{ url('admin-panel/school/view-school-detail/'.$login_info['encrypted_school_id']) }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i> View 
                                            </a>
                                            @endif
                                            <div class="clearfix"></div>
                                            <!-- <ul class="header-dropdown opendiv">
                                                <li class="dropdown">
                                                   <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                   <i class="fas fa-ellipsis-v"></i>
                                                   </button>
                                                   <ul class="dropdown-menu dropdown-menu-right slideUp">
                                                      <li>
                                                         <a href="#" title="">Option 1</a>
                                                      </li>
                                                      <li>
                                                         <a href="#" title="">Option 2</a>
                                                      </li>
                                                      <li>
                                                         <a href="#" title="">Option 3</a>
                                                      </li>
                                                   </ul>
                                                </li>
                                                </ul> -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!-- @if($login_info['imprest_ac_status'] == 1)
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student.svg') !!}" alt="Student" >
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Imprest Account Setting</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/school/setting/'.$login_info['encrypted_school_id']) }}" class="cusa" title="Add ">
                                            <i class="fas fa-plus"></i> Setting 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    @endif -->
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Academic Year.svg') !!}" alt="Academic Year" >
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Academic Year</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/academic-year/add-academic-year') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/academic-year/view-academic-years') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Holiday.svg') !!}" alt="Holiday">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Holiday</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/holidays/add-holiday') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/holidays/view-holidays') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Shift.svg') !!}" alt="Shift">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Shift</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/shift/add-shift') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/shift/view-shifts') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                
                                    <!-- <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Facility.svg') !!}" alt="Facility">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Facility</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/facilities/add-facility') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/facilities/view-facilities') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Document Categories.svg') !!}" alt="Document Category">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Document Category</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/document-category/add-document-category') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/document-category/view-document-categories') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Designation_Category.svg') !!}" alt="Designations">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Designations</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/designation/add-designation') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/designation/view-designations') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Title.svg') !!}" alt="Title">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Title</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/title/add-title') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/title/view-titles') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                               
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Caste.svg') !!}" alt="Caste">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Caste</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/caste/add-caste') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/caste/view-caste') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Religion.svg') !!}" alt="Religion">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Religion</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/religion/add-religion') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/religion/view-religions') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Nationality.svg') !!}" alt="Nationality">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Nationality</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/nationality/add-nationality') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/nationality/view-nationality') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/School Group_House.svg') !!}" alt="School Group / House">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">School Group / House</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/schoolgroup/add-group') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/schoolgroup/view-groups') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                               
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Room No.svg') !!}" alt="Room No">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Room No</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/room-no/add-room-no') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/room-no/view-room-no') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Stream.svg') !!}" alt="Stream">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Stream</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/stream/add-stream') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/stream/view-streams') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/school_board.svg') !!}" alt="School Boards">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">School Boards</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/school-board/add-school-board') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/school-board/view-school-boards') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/school_board.svg') !!}" alt="System Configuration">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">System Configuration</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/system-configuration/add-system-configuration') }}" class="cusa" title="Edit">
                                            <i class="fas fa-plus"></i> Edit 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 commclass">
                                        <h4>Other Details :- </h4>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/country.svg') !!}" alt="Country">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Country</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/country/add-country') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/country/view-country') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/state.svg') !!}" alt="State">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">State</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/state/add-state') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/state/view-state') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Configuration/city.svg') !!}" alt="City">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">City</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/city/add-city') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/city/view-city') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection