@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_academic') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_academic') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/medium.svg') !!}" alt="Mediums">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Mediums</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/mediums/add-medium') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/mediums/view-mediums') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/class.svg') !!}" alt="Class">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Class</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/class/add-class') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/class/view-classes') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/section.svg') !!}" alt="Section" >
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Section</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/section/add-section') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/section/view-sections') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/class-teacher-allocation.svg') !!}" alt="Class Teacher Allocation">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Class Teacher Allocation</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/class-teacher-allocation/allocate-class') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/class-teacher-allocation/view-allocations') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/co-scholastics.svg') !!}" alt="Co-Scholastics">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Co-Scholastics</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/subject/add-co-scholastic') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/subject/view-co-scholastic') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/subject.svg') !!}" alt="Subject">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Subject</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/subject/add-subject') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/subject/view-subjects') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/map-subject-to-class.svg') !!}" alt="Map Subject To Class">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Map Subject To Class</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/class-subject-mapping/view-subject-class-mapping') !!}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/map-subject-to-section.svg') !!}" alt="Map Subject To Section">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Map Subject To Section</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/section-subject-mapping/view-subject-section-mapping') !!}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/map-subject-to-student.svg') !!}" alt="Manage student's subject">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Manage student's subject</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/student-subject/manage') !!}" class="cusa" title="Manage ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/map-subject-to-teacher.svg') !!}" alt="Map Subject To Teacher">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Map Subject To Teacher</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping') !!}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/virtual-class.svg') !!}" alt="Virtual Class">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Virtual Class</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/virtual-class/add-virtual-class') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/virtual-class/view-virtual-classes') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/competition.svg') !!}" alt="Competition">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Competition</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/competition/add-competition') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/competition/view-competitions') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/time-table.svg') !!}" alt="Time Table">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Time Table</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/time-table/add-time-table') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/time-table/view-time-table') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/academic/certificates.svg') !!}" alt="Certificates">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Certificates</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{!! URL::to('admin-panel/certificates') !!}" class="cusa" title="View ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection