

<div class="table-responsive" >
    @if(!empty($all_subjects))
    <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
        {{ csrf_field() }}

        <thead>
            <tr>
                <th style="width: 30px !important;">{{trans('language.s_no')}}</th>
                <th  style="width: 60px !important;">{{trans('language.student_enroll_number')}}</th>
                <th>{{trans('language.student_name')}}</th>
                @if(!empty($all_subjects))
                @foreach($all_subjects as $subject)
                <th>{!! $subject['subject_name'] !!} <br /> ({!! $subject['exam_date'] !!}) <br />({!! $subject['exam_time'] !!})  <br />({!! $subject['max_marks'] !!})</th>
                @endforeach
                @endif
            </tr>
            
        </thead>
        <tbody>
           
            @if(!empty($all_students))
                @foreach($all_students as $key => $students)
                @php $key++; $total_subjects = $total_marks_enter = $total_submit_marks =  0 @endphp
            <tr>
                <td style="width: 30px !important;">{!! $key !!}</td>
                <td  style="width: 60px !important;">{!! $students['student_enroll_number'] !!}</td>
                <td>@if($students['profile'] != '') <img src="{!! $students['profile'] !!}" width="30px;" /> @endif {!! $students['student_name'] !!}</td>
                @foreach($students['student_subjects'] as $subject)

                @php 
                    $current_date = date('d M Y'); 
                @endphp
                @if($subject['marks'] === '' )
                    @php $class = "has-error"; @endphp
                @else
                    @php $class = "has-success"; @endphp
                @endif
                <td>
                @if($subject['exclude'] == 0)
                    @php 
                        $total_subjects++; 
                        $marks = 0;
                        $marks = $subject['marks'];  
                    @endphp 
                    @if($subject['marks'] !== '')
                        @php $total_marks_enter++; @endphp 
                    @endif
                    @if($subject['attend_flag'] == 0)
                        @php 
                            $marks = "A";  
                        @endphp 
                    @endif
                    @php 
                        $marks_disabled = "";
                    @endphp
                    @if($subject['marks_status'] === 1)
                        @php  $total_submit_marks++;   $marks_disabled = "disabled";  @endphp
                    @endif
                    <label class="form-group field select {{$class}}" style="width: 100%"> 
                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][student_id]', $students['student_id'], ['class' => 'form-control', 'id' => 'student_id'.$key.'']) !!}
                        
                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][subject_id]', $subject['subject_id'], ['class' => 'form-control', 'id' => 'subject_id'.$key.'']) !!}
                        
                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][class_id]', $students['class_id'], ['class' => 'form-control', 'id' => 'class_id'.$key.'']) !!}
                        
                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][section_id]', $students['section_id'], ['class' => 'form-control', 'id' => 'section_id'.$key.'']) !!}

                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][exam_id]', $students['exam_id'], ['class' => 'form-control', 'id' => 'exam_id'.$key.'']) !!}

                        {!! Form::hidden('marks['.$key.$subject['subject_id'].'][exam_mark_id]', $subject['exam_mark_id'], ['class' => 'form-control', 'id' => 'exam_mark_id'.$key.'']) !!}

                        @if(strtotime($current_date) > strtotime($subject['exam_date']))
                            {!! Form::text('marks['.$key.$subject['subject_id'].'][marks]', $marks, ['class' => 'form-control marks', 'id' => 'marks'.$key.$subject['subject_id'].'',  'max'=> $subject['max_marks'], 'style' => 'width: 100px', 'max-marks'=>$subject['max_marks'], $marks_disabled]) !!}
                        @else 
                            Exam Pending
                        @endif
                    </label>
                @else 
                    ------
                @endif
                </td>
                @endforeach
                
            </tr>
            @endforeach
            @endif
            
        </tbody>
    </table>
    @else
        <div class="alert alert-danger" role="alert">
            No record found or Schedule still not publish.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
       
    @endif
    
</div>
@if($total_subjects === $total_marks_enter)
    @php $disabled = ""; @endphp
@else 
    @php $disabled = "disabled";  @endphp
@endif
@if($total_subjects === $total_submit_marks)
    @php $disabled = "disabled"; @endphp
@endif
@if(!empty($all_subjects))
<div class="row clearfix">
    <div class="col-lg-1 col-md-1">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Save','id'=> 'save']) !!}
    </div>
   
    <div class="col-lg-1 col-md-1">
        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary clearBtn1','name'=>'Clear', 'id' => "clearBtn1"]) !!}
    </div>

    <div class="col-lg-2 col-md-2">
        {!! Form::button('Final Submission', ['class' => 'btn btn-raised btn-round btn-primary final_submission','name'=>'final_submission','id'=> 'final_submission', 'style' => 'background: #F8A31B !important', $disabled]) !!}
    </div>
</div>

@endif