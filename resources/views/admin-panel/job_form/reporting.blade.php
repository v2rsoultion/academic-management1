@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.recruitment_report') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/recruitment') !!}">{!! trans('language.menu_recruitment') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.recruitment_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('medium_type', $listData['arr_medium'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'arr_medium'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('job_name', old('job_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.job_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="job-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.medium_type')}}</th>
                                                <th>{{trans('language.job_name')}}</th>
                                                <th>{{trans('language.job_type')}}</th>
                                                <th>{{trans('language.job_no_of_vacancy')}}</th>
                                                <th>{{trans('language.total_approved')}}</th>
                                                <th>{{trans('language.total_disapproved')}}</th>
                                                <th>{{trans('language.total_applied')}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
        var table = $('#job-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            buttons: [
                'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/recruitment/reporting_data')}}',
                data: function (d) {
                    d.job_name = $('input[name="job_name"]').val();
                    d.medium_type = $('select[name="medium_type"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'medium_type', name: 'medium_type'},
                {data: 'job_name', name: 'job_name'},
                {data: 'job_type', name: 'job_type'},
                {data: 'job_no_of_vacancy', name: 'job_no_of_vacancy'},
                {data: 'total_approved', name: 'total_approved'},
                {data: 'total_disapproved', name: 'total_disapproved'},
                {data: 'total_applied', name: 'total_applied'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "24%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            // document.getElementById('search-form').reset();
            // table.draw();
            // e.preventDefault();
            location.reload();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    
    $(document).on('click','.recruitment',function(e){
        var job_id = $(this).attr('job-id');
        if(job_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recruitment/job/single-recruitment-description')}}",
                type: 'GET',
                data: {
                    'job_id': job_id,
                    'type': 1
                },
                success: function (data) {
                    $("#recruitment_block").html(data);
                    $(".mycustloading").hide();
                    $("#recruitment_details").modal('show');
                }
            });
        }
        
    })


    $(document).on('click','.description',function(e){
        var job_id = $(this).attr('job-id');
        if(job_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recruitment/job/single-recruitment-description')}}",
                type: 'GET',
                data: {
                    'job_id': job_id,
                    'type': 2
                },
                success: function (data) {
                    $("#description_block").html(data);
                    $(".mycustloading").hide();
                    $("#description").modal('show');
                }
            });
        }
        
    })
    


</script>
@endsection




