@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.selected_candidates') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/recruitment') !!}">{!! trans('language.menu_recruitment') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.selected_candidates') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('job_id', $listData['arr_jobs'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'job_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('email', old('email', ''), ['class' => 'form-control ','placeholder'=>trans('language.email'), 'id' => 'email']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('contact_number', old('contact_number', ''), ['class' => 'form-control ','placeholder'=>trans('language.contact_number'), 'id' => 'contact_number']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="job-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.form_number')}}</th>
                                                <th>{{trans('language.job_name_new')}}</th>
                                                <th>{{trans('language.name')}}</th>
                                                <th>{{trans('language.email')}}</th>
                                                <th>{{trans('language.contact')}}</th>
                                                <th>{{trans('language.resume')}}</th>
                                                <th>{{trans('language.status')}}</th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>    
</section>


<div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"> Approved </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
        {!! Form::open(['files'=>TRUE,'id' => 'applied-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
        <div class="modal-body" >
            <div id="approve_block"></div>
            <div class="container-fluid">
                <div class="row ">
                        {!! Form::submit('Submit', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!} 

    </div>
  </div>
</div>

<div class="modal fade" id="disapprove" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"> Disapproved </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
        {!! Form::open(['files'=>TRUE,'id' => 'applied-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
        <div class="modal-body" >
            <div id="disapprove_block"></div>
            <div class="container-fluid">
                <div class="row ">
                        {!! Form::submit('Submit', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!} 
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
        var table = $('#job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/recruitment/selected-candidates-data')}}',
                data: function (d) {
                    d.job_id = $('select[name="job_id"]').val();
                    d.name = $('input[name="name"]').val();
                    d.email = $('input[name="email"]').val();
                    d.contact_number = $('input[name="contact_number"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'form_number', name: 'form_number'},
                {data: 'job_name', name: 'job_name'},
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_email', name: 'staff_email'},
                {data: 'staff_mobile_number', name: 'staff_mobile_number'},
                {data: 'resume', name: 'resume'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            // document.getElementById('search-form').reset();
            // table.draw();
            // e.preventDefault();
            location.reload();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }


    $(document).on('click','.approve',function(e){
        var job_id               = $(this).attr('job-id');
        var job_form_id         = $(this).attr('job-form-id');
        var applied_candidate_id = $(this).attr('applied-candidates-id');
        $('#approve').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/recruitment/schedule-approved-disaaproved')}}",
            type: 'POST',
            data: {
                'job_id': job_id,
                'job_form_id': job_form_id,
                'applied_candidate_id': applied_candidate_id,
                'type': 2,
            },
            success: function (data) {
                $('#approve_block').html(data.data);
                $('.select2').select2();
            }
        });

    });




    $(document).on('click','.disapprove',function(e){
        var job_id               = $(this).attr('job-id');
        var job_form_id         = $(this).attr('job-form-id');
        var applied_candidate_id = $(this).attr('applied-candidates-id');
        $('#disapprove').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/recruitment/schedule-approved-disaaproved')}}",
            type: 'POST',
            data: {
                'job_id': job_id,
                'job_form_id': job_form_id,
                'applied_candidate_id': applied_candidate_id,
                'type': 3,
            },
            success: function (data) {
                $('#disapprove_block').html(data.data);
                $('.select2').select2();
            }
        });

    });

</script>
@endsection




