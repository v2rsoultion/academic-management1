{!! Form::hidden('job_form_id',old('job_form_id',isset($job_form['job_form_id']) ? $job_form['job_form_id'] : ''),['class' => 'gui-input', 'id' => 'job_form_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<!-- Basic Info section -->

<div class="alert alert-danger" role="alert" id="error-block" style="display: none">
    
</div>

<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('form_name', old('form_name',isset($job_form['form_name']) ? $job_form['form_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_name'), 'id' => 'form_name']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_prefix') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('form_prefix', old('form_prefix',isset($job_form['form_prefix']) ? $job_form['form_prefix']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_prefix'), 'id' => 'form_prefix']) !!}
        </div>
    </div>

    <!-- <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_message_via') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('form_message_via', $job_form['arr_message_via'],old('form_message_via',isset($job_form['form_message_via']) ? $job_form['form_message_via'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_message_via'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div> -->

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.job') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('job_id', $job_form['arr_jobs'],old('job_id',isset($job_form['job_id']) ? $job_form['job_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'job_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>
<!-- Instruction Information -->
<div class="header">
    <h2><strong>Instruction  </strong> Information</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_instruction') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('form_instruction', old('form_instruction',isset($job_form['form_instruction']) ? $job_form['form_instruction']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_instruction'), 'id' => 'form_instruction', 'rows' => '3']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_information') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('form_information', old('form_information',isset($job_form['form_information']) ? $job_form['form_information']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_information'), 'id' => 'form_information', 'rows' => '3']) !!}
        </div>
    </div>
</div>

<!-- Message Information -->
<!-- <div class="header">
    <h2><strong>Message  </strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_success_message') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('form_success_message', old('form_success_message',isset($job_form['form_success_message']) ? $job_form['form_success_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_success_message'), 'id' => 'form_success_message', 'rows' => '3']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.form_failed_message') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('form_failed_message', old('form_failed_message',isset($job_form['form_failed_message']) ? $job_form['form_failed_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_failed_message'), 'id' => 'form_failed_message', 'rows' => '3']) !!}
        </div>
    </div>
</div> -->

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/recruitment') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $.validator.addMethod("maxDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate < inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select future date");

        $("#job-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                form_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                form_prefix: {
                    required: true
                },
                job_id: {
                    required: true
                },
                form_success_message: {
                    required: true
                },
                form_failed_message: {
                    required: true
                },
                form_message_via: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        // $('#form_last_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,minDate : new Date()});
        
    });

    function showOptions(type){
        $(".mycustloading").show();
        if(type == 0){
            $('#admission_fields').show();
            $(".mycustloading").hide();
        } else {
            $('#admission_fields').hide();
            $(".mycustloading").hide();
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("#class_id").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#class_id").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getBrochure(session_id)
    {
        if(session_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/brochure/get-brochure-data')}}",
                type: 'GET',
                data: {
                    'session_id': session_id
                },
                success: function (data) {
                    $("select[name='brochure_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='brochure_id'").html('<option value="">Select Brochure</option>');
            $(".mycustloading").hide();
        }
    }
    function getIntake(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-total-intake-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $(':input[name="no_of_intake1"]').prop('disabled', false);
                    $("#no_of_intake").val(data);
                    $("#no_of_intake1").val(data);
                    $(':input[name="no_of_intake1"]').prop('disabled', true);
                    $(".mycustloading").hide();
                    if(data == 0){
                        $('#error-block').html("Sorry, We have already exceed class's student capacity.");
                        $('#error-block').show();
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $(':input[type="submit"]').prop('disabled', false);
                        $('#error-block').html('');
                        $('#error-block').hide();
                    }
                }
            });
        } else {
            
            $(':input[name="no_of_intake1"]').prop('disabled', false);
            $("#no_of_intake").vl('0');
            $("#no_of_intake1").vl('0');
            $(':input[name="no_of_intake1"]').prop('disabled', true);
            $(".mycustloading").hide();
        }
    }

</script>