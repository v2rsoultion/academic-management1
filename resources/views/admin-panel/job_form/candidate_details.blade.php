@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nav_item7
    {
    min-height: 150px !important;
    clear: both;
    }
    .imgclass:hover .active123 .tab_images123{
    left: 44% !important;
    }
    .imgclass:hover .tab_images123{
    left: 50% !important;
    }
    .nav_item3 {
    min-height: 255px !important;
    }
    .tab1{
        padding-left: 5px !important;
    }
    .tab2{
        padding-right: 3px !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.candidate_details') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/recruitment') !!}">{!! trans('language.job_recruitment') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="#">{!! trans('language.candidate_details') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card profile_card1">
                    <div class="body text-center pro_body_center1 form-gap ">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 text-left">
                                <div class="profile-image"> 
                                    @if($applied_candidate['staff_profile_img'] != "") 
                                    <img src="{!! URL::to($config_upload_path.'/'.$applied_candidate['staff_profile_img']) !!}" alt="{!! $applied_candidate['staff_name'] !!}" class="profile_image"> 
                                    @else
                                    <img src="{!! URL::to('public/images/dummy.jpg') !!}" alt="{!! $applied_candidate['staff_name'] !!}" class="profile_image"> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std1">
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.form_number') !!}: </span> {!! $applied_candidate['form_number'] !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        
                                        <label> <span>{!! trans('language.job_name_new') !!} : </span> {!! $applied_candidate['job_name'] !!}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std2">
                                    <div class="profile_detail_3">
                                        <label> <span> {!! trans('language.resume') !!} : </span> 
                                            
                                            @if(!empty($applied_candidate['staff_resume']))
                                                
                                                <a href="{!! URL::to($config_upload_path.'/'.$applied_candidate['staff_resume']) !!}"  target="_blank" >View Resume</a>
                                            @else
                                                No Document Found
                                            @endif

                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="pull-right">
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="personal">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Basic Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['title_name'] !!} {!! $applied_candidate['staff_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.designation') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['designation_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_email') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_dob') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_dob'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_gender') !!} :</b> 
                                                                                        <span class="profile_detail_span_6">
                                                                                        {!! $applied_candidate['gender'] !!}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Professional Info </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_aadhar') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_aadhar'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_UAN') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_UAN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_ESIN') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_ESIN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_PAN') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_PAN'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_qualification') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_qualification'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.staff_specialization') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_specialization'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_reference') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_reference'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.staff_experience') !!} :</b> <span class="profile_detail_span_6">
                                                                                        @if($applied_candidate['staff_experience'] != '')
                                                                                        @foreach($applied_candidate['staff_experience'] as $exp)
                                                                                            <li>{!! $exp !!}</li>
                                                                                        @endforeach
                                                                                        
                                                                                        @endif
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Other Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_father_name_husband_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_father_name_husband_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_mother_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_mother_name'] !!} </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_father_husband_mobile_no') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_father_husband_mobile_no'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_blood_group') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['staff_blood_group'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.caste_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['caste_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.religion_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['religion_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.nationality_name') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['nationality_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b  class="profile_detail_bold_2">{!! trans('language.staff_marital_status') !!} :</b> <span class="profile_detail_span_6">{!! $applied_candidate['marital'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
@endsection

<script>
    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }
</script>