<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="School management.">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{!!trans('language.reset_password')!!} | {!!trans('language.project_title') !!}</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Styles -->
        {!! Html::style('public/admin/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('public/assets/css/main.css') !!}
        {!! Html::style('public/admin/assets/css/authentication.css') !!}
        {!! Html::style('public/admin/assets/css/color_skins.css') !!}
        {!! Html::style('public/assets/css/stylesheet.css') !!}
        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body class="theme-blush authentication sidebar-collapse">
    <!-- Main Content -->
        <div class="container-fluid banner-img">
            <div class="row">
                <div class="col-lg-7 padding-0">
                    <div class="page-header-image">
                        <div class="text-center m-auto part1">
                            <div class="logo-container">
                                <img src="{!! URL::to('public/assets/images/logo_new1.png') !!}" alt="" width="130px;">
                            </div>
                            <h4 class="text-center">Welcome to<br> Academic Eye</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 padding-0">
                    <div class="card-plain">
                        {!! Form::open(array('url' => '/admin-panel/password/reset', 'files' => true)) !!}
                            <div class="header">
                                <div class="logo-container">
                                    <img src="{!! URL::to('public/assets/images/logo_new1.png') !!}" alt="">
                                </div>
                                <h5 class="text-center">Reset Password</h5>
                            </div>
                            @if ($errors->any())
                                <div class="alert alert-danger s-f-size" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close m-top" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <input type="hidden" name="token" id="token" class="form-control space" value="{!! Request::segment(4) !!}" />
                            <input type="hidden" name="username" id="username" class="form-control space" value="{!! urldecode(Request::segment(5)) !!}">
                            <div class="content"> 
                                <div class="input-group input-lg">
                                    <input type="password" name="password" id="password" placeholder="New Password" class="form-control space {{ $errors->has('password') ? ' has-error' : '' }}" />
                                    <span class="input-group-addon space">
                                        <i class="zmdi zmdi-lock"></i>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                                <div class="input-group input-lg">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control space {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" placeholder="Confirm Password">
                                <span class="input-group-addon space">
                                    <i class="zmdi zmdi-lock"></i>
                                </span>
                                 </div> 
                                @if($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                            </div>
                            <div class="footer text-center space">
                                <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Reset Password</button>
                                <h5><a href="{{ url('/admin-panel') }}" class="link">Login?</a></h5>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    <!-- Jquery Core Js -->
    {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!}
    {!! Html::script('public/admin/assets/bundles/vendorscripts.bundle.js') !!} <!-- Lib Scripts Plugin Js -->

    <script>
        $(".navbar-toggler").on('click',function() {
            $("html").toggleClass("nav-open");
        });
        //=============================================================================
        $('.form-control').on("focus", function() {
            $(this).parent('.input-group').addClass("input-group-focus");
        }).on("blur", function() {
            $(this).parent(".input-group").removeClass("input-group-focus");
        });
    </script>
    </body>
</html>
