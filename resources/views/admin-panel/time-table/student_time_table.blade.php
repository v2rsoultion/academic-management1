@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! trans('language.my_timetable') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="#">{!! trans('language.my_timetable') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active">
                        <div class="card">
                            <div class="body form-gap">
                               
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                   <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>      
                                @endif
                                <div class="">
                                    @if($time_table->time_table_week_days[0] != "")
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Period No</th>
                                                @if(in_array(1,$time_table->time_table_week_days))<th class="text-center">Monday</th>@endif
                                                @if(in_array(2,$time_table->time_table_week_days))<th class="text-center">Tuesday</th>@endif
                                                @if(in_array(3,$time_table->time_table_week_days))<th class="text-center">Wenesday</th>@endif
                                                @if(in_array(4,$time_table->time_table_week_days))<th class="text-center">Thursday</th>@endif
                                                @if(in_array(5,$time_table->time_table_week_days))<th class="text-center">Friday</th>@endif
                                                @if(in_array(6,$time_table->time_table_week_days))<th class="text-center">Saturday</th> @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($lecture = 1; $lecture <= $time_table->lecture_no; $lecture++)
                                            <tr>
                                                <td class="text-center">Period-{!! $lecture; !!}</td>
                                                @if(in_array(1,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 1)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(2,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 2)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(3,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 3)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(4,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 4)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(5,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 5)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(6,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 6)
                                                        @php $exist = 1; @endphp
                                                        {!! $map['start_time'] !!} - {!! $map['end_time'] !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            -----
                                                        @endif
                                                    @else 
                                                        -----
                                                    @endif
                                                </td>
                                                @endif
                                            </tr>
                                            @endfor
                                           
                                                                                      
                                        </tbody>
                                    </table>

                                    @else
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Time Table not prepared !!</th>
                                                </tr>
                                            </thead>
                                        </table>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

@endsection




