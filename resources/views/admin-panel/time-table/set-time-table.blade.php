@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <h2>{!! trans('language.set_time_table') !!}</h2>
            </div>
            <div class="col-lg-9 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/time-table/view-time-table') !!}">{!! trans('language.time_table') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/time-table/view-time-table') !!}">{!! trans('language.view_time_table') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="#">{!! trans('language.set_time_table') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                               
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                   <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>      
                                @endif
                                <div class="">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="v_notice1 text-center">S No</th>
                                                @if(in_array(1,$time_table->time_table_week_days))<th class="text-center">Monday</th>@endif
                                                @if(in_array(2,$time_table->time_table_week_days))<th class="text-center">Tuesday</th>@endif
                                                @if(in_array(3,$time_table->time_table_week_days))<th class="text-center">Wenesday</th>@endif
                                                @if(in_array(4,$time_table->time_table_week_days))<th class="text-center">Thursday</th>@endif
                                                @if(in_array(5,$time_table->time_table_week_days))<th class="text-center">Friday</th>@endif
                                                @if(in_array(6,$time_table->time_table_week_days))<th class="text-center">Saturday</th> @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for($lecture = 1; $lecture <= $time_table->lecture_no; $lecture++)
                                            <tr>
                                                <td class="text-center">{!! $lecture; !!}</td>
                                                @if(in_array(1,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 1)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:i A", strtotime($map['start_time'])); !!} - {!! date("H:i A", strtotime($map['end_time'])); !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif
                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add" map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}" day="1" time-table-id="{!! $time_table->time_table_id !!}" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="1" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="1" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(2,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 2)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:i A", strtotime($map['start_time'])); !!} - {!! date("H:i A", strtotime($map['end_time'])); !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add " map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}" day="2"  data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="2" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="2" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(3,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 3)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:i A", strtotime($map['start_time'])); !!} - {!! date("H:i A", strtotime($map['end_time'])); !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add" map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}"  day="3" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="3" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="3" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(4,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 4)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:i A", strtotime($map['start_time'])); !!} - {!! date("H:i A", strtotime($map['end_time'])); !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add" map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}"   day="4"  data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="4" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="4" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(5,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 5)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:i A", strtotime($map['start_time'])) !!} - {!! date("H:i A", strtotime($map['end_time'])) !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add " map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}"  day="5"  data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="5" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="5" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                                @if(in_array(6,$time_table->time_table_week_days))
                                                <td class="text-left">
                                                    @php $exist = 0; @endphp
                                                    @if(!empty($time_map))
                                                    @foreach($time_map as $map)
                                                        @if($map['lecture_no'] == $lecture && $map['day'] == 6)
                                                        @php $exist = 1; @endphp
                                                        {!! date("H:M A", strtotime($map['start_time'])); !!} - {!! date("H:M A", strtotime($map['end_time'])); !!} <br>
                                                        @if($map['lunch_break'] == 1)
                                                        <strong>Lunch Break</strong>
                                                        @else 
                                                        <strong>Sub. :</strong> {!! $map['get_subject']['subject_name'] !!} - {!! $map['get_subject']['subject_code'] !!} <br>
                                                        <strong>Teacher :</strong> {!! $map['get_staff']['staff_name'] !!} <br>
                                                        @endif

                                                        <a class="pointer float-right" data-toggle="tooltip" title="Delete" href="{!! URL::to('admin-panel/time-table/delete-time-table-period/'.$map['time_table_map_id']) !!}" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete zmdidelete1" ></i></a>

                                                        <div class="pointer float-right add" map-id='{!! $map['time_table_map_id'] !!}' lecture_no = "{!! $lecture; !!}"  day="6"  data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit zmdidelete1"></i></div>
                                                        @endif
                                                        @endforeach
                                                        @if($exist == 0)
                                                            <button map-id='' lecture_no = "{!! $lecture; !!}" day="6" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                        @endif
                                                    @else 
                                                        <button map-id='' lecture_no = "{!! $lecture; !!}" day="6" time-table-id="{!! $time_table->time_table_id !!}" class="btn btn-raised btn-round btn-primary add" >Add Period</button>
                                                    @endif
                                                </td>
                                                @endif
                                            </tr>
                                            @endfor
                                           
                                                                                      
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="timetableModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Period </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
        {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
            
            <div class="row clearfix" style="margin-top: 15px;">
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.start_time') !!} <span class="red-text">*</span> :</lable>
                    <div class="form-group">
                        {!! Form::time('start_time', old('start_time',''), ['class' => 'form-control times','placeholder'=>trans('language.start_time'), 'id' => 'start_time','onChange'=> 'CheckValid()']) !!}
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.end_time') !!} <span class="red-text">*</span> :</lable>
                    <div class="form-group">
                        {!! Form::time('end_time', old('end_time', ''), ['class' => 'form-control times','placeholder'=>trans('language.end_time'), 'id' => 'end_time','onChange'=> 'CheckValid()']) !!}
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.lunch_break') !!} :</lable>
                    <div class="checkbox" id="customid">
                        <input type="checkbox" id="lunch_break" name="lunch_break" class="check" value="1" >
                        <label  class="from_one1" style="margin-bottom: 4px !important;"  for="lunch_break">Lunch Break</label>
                    </div>
                </div>
            </div>
            <div id="subject-staff-block">
                <div class="row clearfix" >
                    <div class="col-lg-3 col-md-3">
                        <lable class="from_one1">{!! trans('language.subject') !!} <span class="red-text">*</span> :</lable>
                        <label class=" field select" style="width: 100%">
                            {!!Form::select('subject_id', $time_table['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id','onChange' => 'CheckValid()'])!!}
                            <i class="arrow double"></i>
                        </label>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <lable class="from_one1">{!! trans('language.staff') !!} <span class="red-text">*</span> :</lable>
                        <label class=" field select" style="width: 100%">
                            {!!Form::select('staff_id', $time_table['arr_staff'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id','disabled'])!!}
                            <i class="arrow double"></i>
                        </label>
                    </div>
                </div>
            </div>
            {!! Form::hidden('class_id', $time_table->class_id, ['class' => 'form-control', 'id' => 'class_id']) !!}
            {!! Form::hidden('section_id', $time_table->section_id, ['class' => 'form-control', 'id' => 'section_id']) !!}
            {!! Form::hidden('time_table_id', '', ['class' => 'form-control', 'id' => 'time_table_id']) !!}
            {!! Form::hidden('lecture_no', old('lecture_no',''), ['class' => 'form-control', 'id' => 'lecture_no']) !!}
            {!! Form::hidden('day', old('day',''), ['class' => 'form-control', 'id' => 'day']) !!}
            {!! Form::hidden('time_table_map_id', old('time_table_map_id',''), ['class' => 'form-control', 'id' => 'time_table_map_id']) !!}
            {!! Form::hidden('exist_staff_id', old('exist_staff_id',''), ['class' => 'form-control', 'id' => 'exist_staff_id']) !!}
            <div class="row clearfix">
                
                <div class="col-lg-2 col-md-2">
                    <lable class="from_one1">&nbsp;</lable> <br />
                    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit', 'id'=>'list-submit']) !!}
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">&nbsp;</lable> <br />
                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
        // $('#start_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  CheckValid(); });
        // $('#end_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  CheckValid();  });
        $('#timetableModel').on('hidden.bs.modal', function () {
        location.reload();
        })
    });
    $(function() {
        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            if(flag == true){
                $("#subject-staff-block").css("display", "none");
            } else {
                $("#subject-staff-block").css("display", "block");
            }
        });
    });
    $(document).on('click','.add',function(e){
        var time_table_id = $(this).attr('time-table-id');
        var lecture_no = $(this).attr('lecture_no');
        var day = $(this).attr('day');
        var map_id = $(this).attr('map-id');
        //alert(map_id);
        $("#lecture_no").val(lecture_no);
        $("#day").val(day);
        $("#time_table_id").val(time_table_id);
        $("#time_table_map_id").val(map_id);
        
        if(map_id != ''){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/time-table/get-period-info')}}",
                type: 'GET',
                data: {
                    'time_table_map_id': map_id
                },
                success: function (data) {
                   
                    $("#staff_id").prop('disabled', false);
                    $("select[name='staff_id'").html(data.staff_data);
                    $("select[name='subject_id'").html(data.subject_data);
                    $("#start_time").val(data.start_time);
                    $("#end_time").val(data.end_time);
                    $("#subject_id").val(data.subject_id);
                    $("#staff_id").val(data.staff_id);
                    $("#time_table_map_id").val(data.time_table_map_id);
                    $("#time_table_id").val(data.time_table_id);
                    $("#exist_staff_id").val(data.staff_id);
                    if(data.lunch_break == 1){
                        $('#lunch_break').prop('checked', true);
                        $("#subject-staff-block").css("display", "none");
                    }
                    // $('#submit_tc').attr('disabled',true);
                    // $(".mycustloading").hide();
                }
            });
        }
        $('#timetableModel').modal('show');
        
    })

    function CheckValid(){
        var start_time = $('#start_time').val();
        var end_time = $('#end_time').val();
        var subject_id = $('#subject_id').val();
        var exist_staff_id = $('#exist_staff_id').val();
        var day = $('#day').val();
        if(subject_id != "" && end_time != "" && start_time != ""){
            $("#staff_id").prop('disabled', false);
            var class_id = $('#class_id').val();
            var section_id = $('#section_id').val();
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/time-table/get-available-staff')}}",
                type: 'GET',
                data: {
                    'start_time': start_time,
                    'end_time' : end_time,
                    'subject_id': subject_id,
                    'class_id': class_id,
                    'section_id': section_id,
                    'exist_staff_id': exist_staff_id,
                    'day': day
                },
                success: function (data) {
                    $("select[name='staff_id'").html(data.options);
                   $(".mycustloading").hide();
                }
            });
        } else {
            $("#staff_id").prop('disabled', true);
        }
    }
    $(document).ready(function() {

        jQuery.validator.addMethod("greaterThanTime",  function(value, element) {
            var start_time = $("#start_time").val(); //start time
            var end_time = $("#end_time").val();  //end time

            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt) {
                return false;
            }
            return true;
        },'End time must be greater than start time.');

        jQuery.validator.addMethod("lessThanTime",  function(value, element) {
            var start_time = $("#start_time").val(); //start time
            var end_time = $("#end_time").val();  //end time
           
            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt && end_time != '') {
                return false;
            }
            return true;
        },'Start time must be less than end time.');
        $("#list-form").validate({

        /* @validation states + elements 
        ------------------------------------------- */

        errorClass: "state-error",
        validClass: "state-success",
        errorElement: "em",
        // errorLabelContainer: '.errorTxt',

        /* @validation rules 
        ------------------------------------------ */

        rules: {
            start_time: {
                required: true,
                time: true,
                lessThanTime: true
            },
            end_time: {
                required: true,
                time: true,
                greaterThanTime: true
            },
            staff_id: {
                required: true
            },
            subject_id: {
                required: true
            },
        },
        /* @validation highlighting + error placement  
        ---------------------------------------------------- */
        highlight: function (element, errorClass, validClass) {
            $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },

        errorPlacement: function (error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                element.closest('.option-group').after(error);
            } else {
                element.closest('.form-group').after(error);
            // error.insertAfter(element.parent());
            }
        }
        });
    });
    
    $('#list-form').on('submit', function(e) {
        var start_time = $('#start_time').val();
        var end_time = $('#end_time').val();
        var subject_id = $('#subject_id').val();
        var staff_id = $('#staff_id').val();
        // $(".mycustloading").show();
        if(start_time != '' && end_time != '' || (subject_id != '' && staff_id != '') || ($(".check").checked ==  true) ) {
            var formData = $(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{url('admin-panel/time-table/save-time-table-map')}}",
                data     : formData,
                cache    : false,

                success  : function(data) {
                    data = $.trim(data);
                    console.log(data);
                    if(data == "success"){
                        $("#success-block").html("Period successfully added.");
                        $("#success-block").show();
                        $("#error-block").html("");
                        $("#error-block").hide();
                        $(".mycustloading").hide();
                        $('#list-submit').attr('disabled',true);

                    } else {
                        $("#success-block").html("");
                        $("#success-block").hide();
                        $("#error-block").html("Something goes wrong.");
                        $("#error-block").show();
                        $(".mycustloading").hide();
                    }
                }
            })
        } else {
            $("#error-block").html("Required fields missing");
            $("#error-block").show();
        }
        e.preventDefault();
    });

    $(document).ready(function() {
        $(document).on("keyup", ".times", function () {
            console.log('click trigger');
            $(this).valid();  CheckValid();
            var start_time = $('#start_time').val(); //start time
            var end_time = $('#end_time').val();  //end time
            var time_table_id = $("#time_table_id").val();
            var day = $("#day").val();
            var time_table_map_id = $("#time_table_map_id").val();
            //convert both time into timestamp
            if(start_time != '' && end_time != ''){
                var stt = new Date("November 30, 2018 " + start_time);
                stt = stt.getTime();
                var endt = new Date("November 30, 2018 " + end_time);
                endt = endt.getTime();
                if(stt < endt && end_time != '') {
                    console.log('Check time availability');
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{url('admin-panel/time-table/check-time-availability')}}",
                        type: 'GET',
                        data: {
                            'start_time': start_time,
                            'end_time': end_time,
                            'time_table_id': time_table_id,
                            'day': day,
                            'time_table_map_id': time_table_map_id,
                        },
                        success: function (data) {
                            if(data == 0){
                                $("#error-block").html('');
                                $("#error-block").hide();
                                $('#list-submit').attr('disabled',false);
                            } else {
                                $("#error-block").html('Time slot already exist');
                                $("#error-block").show();
                                $('#list-submit').attr('disabled',true);
                            }
                            $(".mycustloading").hide();
                        }
                    });
                } else  if(stt == endt && end_time != '') {
                    $("#error-block").html('Time slot should be different');
                    $("#error-block").show();
                    $('#list-submit').attr('disabled',true);
                }
            }
        });
    });
        
</script>
@endsection




