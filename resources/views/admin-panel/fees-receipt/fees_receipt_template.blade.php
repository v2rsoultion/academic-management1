<!DOCTYPE html>
<html>
<head>
<title>{!! $page_title !!}</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
	body {
		/*width: 1020px;*/
		width: 720px;
		margin: auto;
		/*margin-top: 20px;*/
	}
	.content_size {
		font-size: 17px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		/*border-radius: 10px;*/
		border: 1px solid #1f2f60;
		padding:10px 40px;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 10px;
	}
	.rl {
		padding: 0px;
	}
	.bold {
		font-weight: bold;
	}
	.position1{
		position: relative;
		left: -115px;
	}
	/*table{
		height: 40px;

	}*/
	tr {
		height: 23px;
	}
	#border11 tr {
		height: 30px;
	}
	.border_bottom {
		border-bottom:1px solid #000;
	}
	.border_top {
		border-top:1px solid #000;
	}
	.border {
		border-right: 1px solid #000;
	}
	</style>

</head>
<body>
	<div class="header">
		<table style="width: 100%;">
			<tr>
				<td colspan="3" style="width: 15%;" class="align">
					@if(isset($certificate_info) && $certificate_info['logo'] != '')
						<img src="{!! URL::to($certificate_info['logo']) !!}" alt="{!! $certificate_info['school_name'] !!}" width="60px"> 
					@else 
						<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px">
					@endif
				</td>
				<td style="width: 55%;"> </td>
				<td><h2 style="margin-bottom:10px;">{!! $page_title !!}</h2>	
					<p class="align_left" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> @if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else  APEX SENIOR SECONDARY SCHOOL @endif <br> Ph. @if(isset($certificate_info)) {!! $certificate_info['school_mobile_number'] !!} @else  APEX SENIOR SECONDARY SCHOOL @endif &nbsp;&nbsp; <br>
					E-mail : @if(isset($certificate_info)) {!! $certificate_info['school_email'] !!} @else  demo@demo.com @endif</p>	
				</td>
			</tr>
		</table>
		
		<hr style="margin:4px 0px;">
		<table style="width: 100%">
			<tr>
				<td style="width: 20%" class="bold content_size">Receipt No. </td>
				<td style="width: 20%" class="content_size bold"> @if(isset($certificate_info)) {!! $certificate_info['receipt_number'] !!} @else FC-108 @endif</td>
				<td style="width: 20%"> </td>
				<td style="width: 19%" class="bold content_size">Date</td>
				<td style="width: 13%" class="content_size position"> @if(isset($certificate_info)) {!! $certificate_info['receipt_date'] !!} @else 28/11/2011 @endif</td>
			</tr>
			<tr>
				<td class="bold content_size">Name</td>
				<td class="content_size"> @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else Sarthak Pundir @endif</td>
				<td> </td>
				<td class="bold content_size">Class</td>
				<td class="content_size position">@if(isset($certificate_info)) {!! $certificate_info['class'] !!} @else  XI A @endif</td>
			</tr>
			<tr>
				<td class="bold content_size">Enroll No</td>
				<td class="content_size">@if(isset($certificate_info)) {!! $certificate_info['student_enroll_number'] !!} @else 2381 @endif</td>
				<td> </td>
				<td class="bold content_size">Payment Mode</td>
				<td class="content_size position">@if(isset($certificate_info)) {!! ucwords($certificate_info['pay_mode']); !!} @else Cash @endif</td>
			</tr>
			
		</table>
		
		<table style="width: 100%;border: 1px solid #000;" id="border11" cellspacing="0px">
			<tr class="align">
				<th class="border border_bottom">S No.</th>
				<th class="border border_bottom">Particulars</th>
				<th class=" border_bottom">Amount</th>
			</tr>
			<tbody>
				@if(isset($certificate_info) && !empty($certificate_info['all_installments'])) 
					@foreach($certificate_info['all_installments'] as $installment)
					@if($installment['head_type'] == 'ONETIME')
						@php $head_name = $installment['head_name']; @endphp
					@else 
						@php $head_name = $installment['head_name'].'('.$installment['get_installment_info']['rc_inst_particular_name'].')';  @endphp
					@endif
					<tr style="height: 23px;">
						<td class="align border">1</td>
						<td class="ll border">{!! $head_name !!}</td>
						<td class="align ">&nbsp;&nbsp;&nbsp;{!! $installment['inst_paid_amt'] !!}</td>
					</tr>
					@endforeach
				@else 
				
				<tr style="height: 23px;">
					<td class="align border">2</td>
					<td class="ll border">Installment(Tuition) Fees</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border">3</td>
					<td class="ll border">Exam Fees</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;200.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border">4</td>
					<td class="ll border">Concession</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;400.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border ">5</td>
					<td class="ll border">Fine</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				@endif
			</tbody>
		</table>
		<table cellspacing="0px" id="border11" style="width: 100%">

		@if(isset($certificate_info) && $certificate_info['wallet_amount'] != 0)
		<tr>
			<td style="width: 78%;" class="align_right">Wallet Use:</td>
			<td style="width: 22%;" class="align">{!! $certificate_info['wallet_amount']; !!}</td>
		</tr>
		@endif;

		@if(isset($certificate_info) && $certificate_info['imprest_amount'] != 0)
		<tr>
			<td style="width: 78%;" class="align_right">Imprest Use:</td>
			<td style="width: 22%;" class="align">{!! $certificate_info['imprest_amount']; !!}</td>
		</tr>
		@endif
		@if(isset($certificate_info) && $certificate_info['total_calculate_fine_amount'] != 0)
		<tr>
			<td style="width: 78%;" class="align_right">Fine Amount:</td>
			<td style="width: 22%;" class="align">{!! $certificate_info['total_calculate_fine_amount']; !!}</td>
		</tr>
		@endif
		@if(isset($certificate_info) && $certificate_info['total_concession_amount'] != 0)
		<tr>
			<td style="width: 78%;" class="align_right">Concession Amount:</td>
			<td style="width: 22%;" class="align"> {!! $certificate_info['total_concession_amount']; !!} </td>
		</tr>
		@endif

		@if(isset($certificate_info) && $certificate_info['net_amount'] != 0)
		<tr>
			<td style="width: 78%;" class="align_right">Net Amount:</td>
			<td style="width: 22%;" class="align"> {!! $certificate_info['net_amount']; !!} </td>
		</tr>
		@endif
		<tr>
			<td style="width: 78%;" class="align_right">Total Pay Amount:</td>
			<td style="width: 22%;" class="align">@if(isset($certificate_info)) {!! $certificate_info['total_amount']; !!} @else 100.00 @endif</td>
		</tr>

		
		</table>
		<!-- <p>Rupees :	<b>Ten thousands two hundred fifty only</b></p> -->
		
		
	</div>
</body>
</html>



<script type="text/javascript">
	window.print();
	window.location.href = "{{url('admin-panel/fees-collection/view-fees-receipt')}}"; 
</script>