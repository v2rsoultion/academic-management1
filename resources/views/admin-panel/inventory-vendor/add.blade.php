@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_vendor') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-vendor') !!}">{!! trans('language.manage_vendor') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_vendor' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.inventory-vendor._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_vendor_name','',['class' => 'form-control','placeholder'=>trans('language.inventory_vendor_name'), 'id' => 's_vendor_name']) !!}
            </div>
          </div>
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_vendor_email','',['class' => 'form-control','placeholder'=>trans('language.inventory_form_email'), 'id' => 's_vendor_email']) !!}
            </div>
          </div>
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_vendor_contact_no','',['class' => 'form-control','placeholder'=>trans('language.inventory_form_contact_no'), 'id' => 's_vendor_contact_no']) !!}
            </div>
          </div>

          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="vendor-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.inventory_s_no') !!}</th>
              <th>{!! trans('language.inventory_name') !!}</th>
              <th>{!! trans('language.inventory_gstin') !!}</th>
              <th>{!! trans('language.inventory_contact_no') !!}</th>
              <th>{!! trans('language.inventory_email') !!}</th>
              <th>{!! trans('language.inventory_address') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#vendor-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/inventory/manage-vendor-view')}}",
              data: function (f) {
                  f.s_vendor_name        = $('#s_vendor_name').val();
                  f.s_vendor_email       = $('#s_vendor_email').val();
                  f.s_vendor_contact_no  = $('#s_vendor_contact_no').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'vendor_name', name: 'vendor_name'},
            {data: 'vendor_gstin', name: 'vendor_gstin'},
            {data: 'vendor_contact_no', name: 'vendor_contact_no'},
            {data: 'vendor_email', name: 'vendor_email'},
            {data: 'vendor_address', name: 'vendor_address'},
            {data: 'action', name: 'action'}
        ],
        columnDefs: [
          {
            "targets": 6, // your case first column
            "width": "19%"
          }
        ] 
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
</script>
@endsection