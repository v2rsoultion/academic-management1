<!DOCTYPE html>
<html>
<head>
	<title>@if(isset($page_title)) {!! $page_title !!} @else Marksheet @endif</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 14px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border: 1px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 14px;
		/*font-style: italic;*/
	}
	.dotted {
		/*border-bottom: 2px dotted #000;*/
		font-size: 14px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	.sub_heading{
		margin-top:0px;
		margin-bottom: 1px;
		font-size: 20px;
		color:#1f2f60;
		font-weight: bold;
		font-style: italic;
	}
	.affiliate {
		font-size:14px; 
		width: 230px;
		text-align: center; 
		font-weight: bold;
	}
	.bold {
		font-weight: bold;
	}
	tr{
		height: 25px;

	}
	</style>
</head>
<body>
	
	<div class="header">
		<table>
			<tr>
				<td style="width: 150px;" class="align">
				@if(isset($certificate_info) && $certificate_info['logo'] != '')
					<img src="{!! URL::to($certificate_info['logo']) !!}" alt="{!! $certificate_info['school_name'] !!}" width="80px" style="margin-top: 15px;"> 
				@else 
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" width="80px" style="margin-top: 15px;">
				@endif
				</td>
				<td style="width: 800px">
					<h3 class="align header_gap" style="margin-top:10px;margin-bottom: 0px;">@if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else Golden Valley School @endif</h3>
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;">  Affiliation No.&nbsp;@if(isset($certificate_info)) {!! $certificate_info['school_sno_numbers'] !!} @else 124568 @endif</p>	
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;">@if(isset($certificate_info)) {!! $certificate_info['school_address'] !!} @else Hanuman Gali, Pushkar, Rajasthan @endif <br> Ph. @if(isset($certificate_info)) {!! $certificate_info['school_mobile_number'] !!} @else +91 999 999 9999 @endif, &nbsp;&nbsp; E-mail : @if(isset($certificate_info)) {!! $certificate_info['school_email'] !!} @else demo@demomail.com @endif</p>	
				</td>
  			</tr>
		</table>
		<h6 class="align" style="margin-top:20px;margin-bottom: 0px;">Report card</h6>
		<p style="margin-top: 5px;margin-bottom: 5px;" class="align bold content_size">Class : @if(isset($certificate_info)) {!! $certificate_info['class'] !!} @else 1st A @endif</p>
		<p style="margin-top: 5px;" class="align bold content_size">Academic Session: @if(isset($certificate_info)) {!! $certificate_info['session_name'] !!} @else 2017-2018 @endif @if(isset($certificate_info)) ( {!! $certificate_info['term_name'] !!} ) @else (Periodic Test of Term 1) @endif</p>

		<table style="margin: 15px 60px;">
			<tr>
				<td style="width: 110px;" class="content_size">Student's Name</td>
				<td style="width: 300px;" class="text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else Amit Kumar @endif</td>
				<td style="width: 200px;"></td>
				<td class="content_size">Roll No. </td>
				<td class="dotted content_size">@if(isset($certificate_info)) {!! $certificate_info['student_roll_no'] !!} @else 1 @endif</td>
			</tr>
			<tr>
				<td class="content_size">Father's Name</td>
				<td class="text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_father_name'] !!} @else Naimesh Mehta @endif</td>
			</tr>
			<tr>
				<td class="content_size">Mother's Name</td>
				<td class="text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_mother_name'] !!} @else Sheetal Rani @endif</td>
				<td style="width: 200px;"></td>
				<td style="width: 120px;" class="content_size">Enroll No.</td>
				<td style="width: 90px;" class="align text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_enroll_number'] !!} @else 25445 @endif</td>
			</tr>
			<tr>
				<td class="content_size">Date of Birth</td>
				<td class="text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_dob'] !!} @else 11/11/2004 @endif</td>
			</tr>
			<tr>
				<td class="content_size">Address</td>
				<td class="text content_size">&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_address'] !!} @else #44, Model Town, Hisar @endif</td>
				<td class="text"> </td>	
				<td class="text"> </td>
				<td class="text"> </td>	
			</tr>
		</table>

		<table style="margin: 0px 60px;width: 88%; " class="align content_size">
			<tr style="background-color: #f8e503de;">
				<td>SCHOLASTIC AREA</td>
				<td>@if(isset($certificate_info)) {!! $certificate_info['exam_name'] !!} @if($certificate_info['term_name']) ( {!! $certificate_info['term_name'] !!} ) @endif @else Period Test (Term 1) @endif</td>
			</tr>
			<tr style="background-color: #75e5db;">
				<td>Subjects</td>
				<td>Marks</td>
			</tr>
			@if(isset($certificate_info))
				@php 
					$total = 0;
				@endphp
				@foreach($certificate_info['all_subjects'] as $subjects)
				@php 
				$total += $subjects['optain_marks'];
				@endphp
				<tr>
					<td>{!! $subjects['subject_name'] !!} (Max Marks-{!! $subjects['max_marks'] !!})</td>
					<td>{!! $subjects['optain_marks'] !!}</td>
				</tr>
				@endforeach
				<tr class="bold">
					<td>Total</td>
					<td>{!! $total !!}</td>
				</tr>
			@else 
			<tr>
				<td>English</td>
				<td>10.00</td>
			</tr>
			<tr>
				<td>Hindi</td>
				<td>9.00</td>
			</tr>
			<tr>
				<td>Maths</td>
				<td>7.00</td>
			</tr>
			<tr>
				<td>Science</td>
				<td>8.00</td>
			</tr>
			<tr>
				<td>Social Studies</td>
				<td>6.00</td>
			</tr>
			<tr>
				<td>Computer Sc.</td>
				<td>9.00</td>
			</tr>
			<tr class="bold">
				<td>Total</td>
				<td>49.00</td>
			</tr>
			@endif
		</table>
		<br><br>
	<table style="margin: 0px 60px;width: 88%; ">
		<tr>
			<td style="width: 115px" class="align_right content_size sign">Date : @php echo  date('d/m/Y') @endphp</td>
			<td style="width: 712px" class="align content_size sign">Class Teacher</td>
			<td style="width: 145px" class="align content_size sign">Principal</td>
		</tr>
	</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>