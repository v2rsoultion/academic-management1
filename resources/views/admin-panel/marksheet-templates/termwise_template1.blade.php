<!DOCTYPE html>
<html>
<head>
	<title>Report-Card-03 Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 14px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border: 1px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 14px;
		/*font-style: italic;*/
	}
	.dotted {
		/*border-bottom: 2px dotted #000;*/
		font-size: 14px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	.sub_heading{
		margin-top:0px;
		margin-bottom: 1px;
		font-size: 20px;
		color:#1f2f60;
		font-weight: bold;
		font-style: italic;
	}
	.affiliate {
		font-size:14px; 
		width: 230px;
		text-align: center; 
		font-weight: bold;
	}
	.bold {
		font-weight: bold;
	}
	tr{
		height: 24px;

	}
	#border tr {
		border: 1px solid #000;
		height: 27px;
	} 
	#border td {
		border: 1px solid #000;
	}
	.gap {
		padding:6px 0px;
	}
	.gap_left {
		padding-left: 5px;
	}
	</style>
</head>
<body>
	
	<div class="header">
		<table>
			<tr>
				<td style="width: 150px;" class="align"><img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px" style="margin-top: 15px;"></td>
				<td style="width: 800px;padding-right: 145px;">
					<h3 class="align header_gap" style="margin-top:10px;margin-bottom: 0px;">Apex Senior Secondary School</h3>
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Affiliated to CBSE,  Affiliation No.&nbsp; 124568</p>	
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Hanuman Gali, Pushkar, Rajasthan <br> Ph. +91 999 999 9999, &nbsp;&nbsp; E-mail : demo@demomail.com</p>	
				</td>
  			</tr>
		</table>
		<h6 class="align" style="margin-top:8px;margin-bottom: 0px;">Report card</h6>
		<p style="margin-top: 5px;margin-bottom: 5px;" class="align bold content_size">Class : 1st A</p>
		<p style="margin-top: 5px;" class="align bold content_size">Academic Session: 2017-2018 (Pre Term 1)</p>

		<table style="margin: 15px 60px;" cellspacing="0px">
			<tr>
				<td style="width: 110px;" class="content_size">Student's Name</td>
				<td style="width: 300px;" class="text content_size">&nbsp; Amit Kumar</td>
				<td style="width: 200px;"></td>
				<td class="align bold content_size gap" style="border:1px solid #000;width: 10px;background-color: #f8e503de;color:#7c0707;">Roll No. </td>
				<td class="align content_size" style="border:1px solid #000;width: 10px;">1</td>
			</tr>
			<tr>
				<td class="content_size">Father's Name</td>
				<td class="text content_size">&nbsp; Naimesh Mehta</td>
			</tr>
			<tr>
				<td class="content_size">Mother's Name</td>
				<td class="text content_size">&nbsp; Sheetal Rani</td>
				<td style="width: 304px;"></td>
				<td style="width: 100px;" class="content_size">Admission No.</td>
				<td style="width: 70px;" class="align text content_size">&nbsp; 25445</td>
			</tr>
			<tr>
				<td class="content_size">Date of Birth</td>
				<td class="text content_size">&nbsp; 11/11/2004</td>
			</tr>
			<tr>
				<td class="content_size">Address</td>
				<td class="text content_size">&nbsp; #44, Model Town, Hisar</td>
				<td class="text"> </td>	
				<td class="text"> </td>
				<td class="text"> </td>	
			</tr>
		</table>

		<table style="margin: 0px 60px;width: 88%;" class="content_size" id="border" cellspacing="0px">
			<tr style="background-color: #f8e503de;" class="align bold">
				<td class="gap" style="width: 150px;">SCHOLASTIC AREA</td>
				<td class="gap" colspan="3">Pre Term 1</td>
			</tr>
			<tr style="background-color: #75e5db;"  class="align bold">
				<td rowspan="2">Subjects</td>
				<td class="gap align">Per. Test</td>
				<td class="gap align">Notebook</td>
				<td class="gap align">SEA</td>
			</tr>
			<tr style="background-color: #75e5db;"  class="align bold">
				<td class="gap align">10</td>
				<td class="gap align">5</td>
				<td class="gap align">5</td>
			</tr>
			<tr>
				<td class="gap_left">English</td>
				<td class="align">10.00</td>
				<td class="align">4.00</td>
				<td class="align">5.00</td>
			</tr>
			<tr>
				<td class="gap_left">Hindi</td>
				<td class="align">9.00</td>
				<td class="align">5.00</td>
				<td class="align">4.00</td>
			</tr>
			<tr>
				<td class="gap_left">Maths</td>
				<td class="align">7.00</td>
				<td class="align">4.00</td>
				<td class="align">3.00</td>
			</tr>
			<tr>
				<td class="gap_left">Science</td>
				<td class="align">8.00</td>
				<td class="align">3.00</td>
				<td class="align">4.00</td>
			</tr>
			<tr>
				<td class="gap_left">Social Studies</td>
				<td class="align">6.00</td>
				<td class="align">5.00</td>
				<td class="align">5.00</td>
			</tr>
			<tr>
				<td class="gap_left">Computer Sc.</td>
				<td class="align">9.00</td>
				<td class="align">5.00</td>
				<td class="align">5.00</td>
			</tr>
			<tr class="bold align">
				<td>Total</td>
				<td>49.00</td>
				<td>26.00</td>
				<td>26.00</td>
			</tr>
		</table>
		<br><br>
	<table style="margin: 0px 60px;width: 88%; ">
		<tr>
			<td style="width: 115px" class="align_right content_size sign">Date : 18/12/2017</td>
			<td style="width: 712px" class="align content_size sign">Class Teacher</td>
			<td style="width: 145px" class="align content_size sign">Principal</td>
		</tr>
	</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>