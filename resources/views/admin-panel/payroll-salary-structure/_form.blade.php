@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.sal_structure_name') !!}</lable>
      {!! Form::text('sal_structure_name',old('sal_structure_name',isset($sal_structure['sal_structure_name']) ? $sal_structure['sal_structure_name']: ''),['class' => 'form-control','placeholder' => trans('language.sal_structure_name'), 'id' => 'sal_structure_name']) !!}
    </div>
  </div>
  @php
    $check1 = 'checked';
    $check0 = '';
    if(isset($sal_structure['round_off'])) {
        if($sal_structure['round_off'] == 0)
        {
            $check0='checked';
            $check1='';
        } 
        if($sal_structure['round_off'] == 1)
        {
            $check0='';
            $check1='checked';
        }
    }
    @endphp
    <div class="col-lg-4">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.arrear_round_off') !!}</lable>
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio33" class="round_off" name="round_off" type="radio" value=1 <?php echo $check1; ?>>
                <label for="radio33" class="document_staff"> Highest Rupee</label>
                <input id="radio34" class="round_off" name="round_off" type="radio" value=0 <?php echo $check0; ?>>
                <label for="radio34" class="document_staff"> Nearest Rupee</label>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid padding-0">
    <!-- <div class="table-responsive"> -->
    <input type="hidden" name="sal_structure_id" value="{{$sal_structure['salary_structure_id']}}" >
        <table class="table m-b-0 c_list" style="width:100%">
        {{ csrf_field() }}
            <thead>
            <tr>
                <th style="width:200px;">Included Heads</th>
                <th style="width:200px;">Salary Head</th>
                <th style="width:60px;">PF</th>
                <th style="width:300px;">Volume (%)</th>
                <th style="width:60px;">ESI</th>
                <th style="width:300px;">Volume (%)</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($sal_head))
            @foreach($sal_head as $key => $value)
            @php 
                $check = $pfcheck = $esicheck = $headChecked =  '';
                $exist = $pfexist = $esiexist = 0;
                $pf = $esi = '';
                $pfstyle = $esistyle = 'display:none';
            @endphp 
                @if(!empty($arr_sal_struct_map))
                    @foreach($arr_sal_struct_map as $key => $record)
                        @if($record['pay_salary_head_id'] == $value['pay_salary_head_id'])
                        @php
                            $headChecked = 'checked';
                            $pf = $record['salary_head_pf'];
                            $esi = $record['salary_head_esi'];
                            $exist = 1;
                            if($pf != '') {
                                $pfcheck = 'checked';
                                $pfexist = 1;
                                $pfstyle = 'display:block';
                            }
                            else {
                                $pfcheck = '';
                                $pfexist = 0;
                                $pfstyle = 'display:none';
                            }
                            if($esi != '') {
                                $esicheck = 'checked';
                                $esiexist = 1;
                                $esistyle = 'display:block';
                            }
                            else {
                                $esicheck = '';
                                $esiexist = 0;
                                $esistyle = 'display:none';
                            }
                        @endphp
                        @endif
                    @endforeach
                    <tr>
                        <td>
                            <div class="checkbox">
                                <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][exist]" value="{{$exist}}" >
                                @if(!empty($headChecked))
                                <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][exist_id]" value="{{$value->pay_salary_head_id}}" >
                                @endif

                                <input id="salHead{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][sal_head_id]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}"  @if(!empty($headChecked)) checked @endif>
                                <label for="salHead{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td>{{$value->sal_head_name}}</td>
                        <td>
                            <div class="checkbox">
                            <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][exist]" value="{{$exist}}" >
                          
                            <input id="salHeadPf{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][pf_volume_c]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}" onclick = "showPfVolume('{{$value->pay_salary_head_id}}')" @if(!empty($pf)) checked @endif>
                            <label for="salHeadPf{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td style="padding:0px 10px !important;">
                            <div id="pf_volume_{{$value->pay_salary_head_id}}" style="{{$pfstyle}}">
                            <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][pfexist]" value="{{$pfexist}}" >
                            {!! Form::number('heads_mapping['.$value->pay_salary_head_id.'][pf_volume]',$pf,['class' => 'form-control', 'id' => 'pf_volume'.$value->pay_salary_head_id.'', 'min' => 0, 'max' => 100, 'step' => 0.1, 'style' => 'width:70px;'])!!}
                            </div>
                        </td>
                        <td>
                        <div class="checkbox">
                            <input id="salHeadEsi{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][esi_volume]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}" onclick = "showEsiVolume('{{$value->pay_salary_head_id}}')"  @if(!empty($esi)) checked @endif>
                            <label for="salHeadEsi{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td style="padding:0px 10px !important;">
                            <div id="esi_volume_{{$value->pay_salary_head_id}}" style="{{$esistyle}}">
                            <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][esiexist]" value="{{$esiexist}}" >
                                {!! Form::number('heads_mapping['.$value->pay_salary_head_id.'][esi_volume]',$esi,['class' => 'form-control', 'id' => 'esi_volume'.$value->pay_salary_head_id.'', 'min' => 0, 'max' => 100, 'step' => 0.1, 'style' => 'width:70px;'])!!}
                            </div>
                        </td>
                    </tr>
                @else 
                    <tr>
                        <td>
                            <div class="checkbox">
                            <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][exist]" value="{{$exist}}" >

                            <input id="salHead{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][sal_head_id]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}"  >
                            <label for="salHead{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td>{{$value->sal_head_name}}</td>
                        <td>
                            <div class="checkbox">
                            <input type="hidden" name="heads_mapping[{{$value->pay_salary_head_id}}][exist]" value="{{$exist}}" >
                           
                            <input id="salHeadPf{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][pf_volume_c]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}" onclick = "showPfVolume('{{$value->pay_salary_head_id}}')">
                            <label for="salHeadPf{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td style="padding:0px 10px !important;">
                            <div id="pf_volume_{{$value->pay_salary_head_id}}" style="display:none;">
                            {!! Form::number('heads_mapping['.$value->pay_salary_head_id.'][pf_volume]','',['class' => 'form-control', 'id' => 'heads_mapping['.$value->pay_salary_head_id.'][pf_volume]', 'min' => 0, 'max' => 100, 'step' => 0.1, 'style' => 'width:70px;'])!!}
                            </div>
                        </td>
                        <td>
                        <div class="checkbox">
                            <input id="salHeadEsi{{$value->pay_salary_head_id}}" type="checkbox" name="heads_mapping[{{$value->pay_salary_head_id}}][esi_volume]" class="checkBoxClass check" value="{{$value->pay_salary_head_id}}" onclick = "showEsiVolume('{{$value->pay_salary_head_id}}')">
                            <label for="salHeadEsi{{$value->pay_salary_head_id}}" class="checkbox_1"></label>
                            </div>
                        </td>
                        <td style="padding:0px 10px !important;">
                            <div id="esi_volume_{{$value->pay_salary_head_id}}" style="display:none;">
                                {!! Form::number('heads_mapping['.$value->pay_salary_head_id.'][esi_volume]','',['class' => 'form-control', 'id' => 'heads_mapping['.$value->pay_salary_head_id.'][esi_volume]', 'min' => 0, 'max' => 100, 'step' => 0.1, 'style' => 'width:70px;'])!!}
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
            @endif
            </tbody>
        </table>
    <!-- </div> -->
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-salary-structure') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_sal_structure").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                sal_structure_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });

function showPfVolume(id) {
    $('#salHead'+id).prop('checked', true);
    var pf_id = $('#salHeadPf'+id).val();
    $('#pf_volume_'+pf_id).toggle();
    $('#pf_volume'+id).val('');

}

function showEsiVolume(id) {
    $('#salHead'+id).prop('checked', true);
    var esi_id = $('#salHeadEsi'+id).val();
    $('#esi_volume_'+esi_id).toggle();
    $('#esi_volume'+id).val('');
}
</script>