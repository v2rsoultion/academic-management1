@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .checkbox label, .radio label{
        line-height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>{!! trans('language.map_employees') !!}</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
        <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-structure') !!}">{!! trans('language.manage_sal_structure') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/salary-structure-map-staff') !!}">{!! trans('language.map_employees') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
              {!! Form::open(['files'=>TRUE, 'class'=>'form-horizontal','url' =>$save_url]) !!}
              <div class= "row" style="margin-top:15px;">
                <div class="headingcommon col-lg-4">Map Employees :-</div>
                <div class="col-lg-6"> </div>
                <div class=col-lg-2>
                  <button type="submit" class="float-right btn btn-raised btn-primary saveBtn" title="Save">Save</button>
                </div>
              </div>
                @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $error)
                    {{ $error }}</br >
                    @endforeach
                </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
                <div class="table-responsive">
                <table class="table m-b-0 c_list" id="map-employee-table" width="100%">
                  <thead>
                    <tr>
                      <th>{!! trans('language.emp_name') !!}</th>
                      <th>{!! trans('language.sal_basic_pay') !!}</th>
                      <th>{!! trans('language.sal_structure') !!}</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
                <div class="container-fluid">
                  <div class= "row">
                    <button type="submit" class="float-right btn btn-raised btn-primary saveBtn" title="Save">Save</button>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>     
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#map-employee-table').DataTable({
      serverSide: true,
      searching: false, 
      paging: false, 
      info: false,
      ajax: {
          url:"{{ url('admin-panel/payroll/salary-structure-map-staff-data')}}",
          type: 'GET',
      },
      columns: [
        {data: 'employee_profile', name: 'employee_profile'},
        {data: 'basic_pay', name: 'basic_pay'},
        {data: 'salary_structure', name: 'salary_structure'}
    ],
      columnDefs: [
        {
          "targets": 0, // your case first column
          "orderable":false,
          "width": "15%"
        },
        {
            "targets": 1, // your case first column
            "width": "20%"
        },
        {
            "targets": 2, // your case first column
            "width": "20%"
        },
        {
          targets: [ 0, 1, 2],
          className: 'mdl-data-table__cell--non-numeric'
        }
      ]
    });
    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $('#clearBtn').click(function(){
        location.reload();
    });
  });
</script>
@endsection