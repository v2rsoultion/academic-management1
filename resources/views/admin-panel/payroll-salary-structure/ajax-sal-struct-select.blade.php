<option value="">Select Employees</option>
@if(!empty($arr_sal_structure))
  @foreach($arr_sal_structure as $key => $value)
    <option @if($salary_structure_id == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif