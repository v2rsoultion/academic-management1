@if(isset($reason['reason_id']) && !empty($reason['reason_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('reason_id',old('reason_id',isset($reason['reason_id']) ? $reason['reason_id'] : ''),['class' => 'gui-input', 'id' => 'reason_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.reason_text') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('reason_text', old('reason_text',isset($reason['reason_text']) ? $reason['reason_text']: ''), ['class' => 'form-control','placeholder'=>trans('language.reason_text'), 'id' => 'reason_text', 'rows'=> '3']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z0-9][ a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{6,]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#reason-form").validate({
            /* @validation states + elements------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                reason_text: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }

                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>