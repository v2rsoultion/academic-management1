<option value="">Select Reason</option>
@if(!empty($arr_reason))
  @foreach($arr_reason as $key => $value)
    <option @if($reason_id == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif