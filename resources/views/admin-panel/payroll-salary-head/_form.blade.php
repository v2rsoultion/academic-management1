@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.sal_head_name') !!}</lable>
      {!! Form::text('sal_head_name',old('sal_head_name',isset($salary_head['sal_head_name']) ? $salary_head['sal_head_name']: ''),['class' => 'form-control','placeholder' => trans('language.sal_head_name'), 'id' => 'sal_head_name']) !!}
    </div>
  </div>
  @php
    $check1 = 'checked'; 
    $check0 = '';
    if(isset($salary_head['head_type'])) {
        if($salary_head['head_type'] == 1)
        {
            $check1='checked';
            $check0 = '';
        }
        if($salary_head['head_type'] == 0)
        {
            $check0='checked';
            $check1 = '';
        }
    }
    @endphp
  <div class="col-lg-4">
    <div class="form-group">
        <lable class="from_one1">Type</lable>
        <div class="radio" style="margin-top:6px !important;">
        <input id="radio31" name="head_type" type="radio" value="1" <?php echo $check1; ?>>
        <label for="radio31" class="document_staff">Allowance</label>
        <input id="radio32" name="head_type" type="radio" value="0" <?php echo $check0; ?>>
        <label for="radio32" class="document_staff">Deduction</label>
        </div>
    </div>
   </div>

</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-salary-head') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_salary_head").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                sal_head_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                inventory_invoice: {
                    required: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>