@extends('admin-panel.layout.header')
@section('content')
<style>
p{
    margin-bottom: 5px;
}
</style>
<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/student/view-issue-transfer-certificate') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group ">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('class_id', $listData['arr_class'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="form-group ">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('section_id', $listData['arr_section'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="form-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="form-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div id="list-block" style="display: none">
                                <hr>
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.profile')}}</th>
                                                    <th>{{trans('language.student_enroll_number')}}</th>
                                                    <th>{{trans('language.class_section')}}</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="tcModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Generate Tc </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body" style="min-height: 200px; padding-top: 20px;">
            {!! Form::open(['files'=>TRUE,'id' => 'info-form' , 'class'=>'form-horizontal']) !!}
            <div class="info_block">
                <div><b>Student:</b> <span id="s_student_name"></span></div>
                <div><b>Class - Section:</b>  <span id="class_section"></span> </div>
            </div>

            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>

            {!! Form::hidden('s_class_id', '',['id' => 's_class_id']) !!}
            {!! Form::hidden('s_section_id', '',['id' => 's_section_id']) !!}
            {!! Form::hidden('s_student_id', '',['id' => 's_student_id']) !!}
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4">
                    <lable class="from_one1">{!! trans('language.tc_date') !!} :</lable>
                    <div class="form-group">
                        {!! Form::date('tc_date', '', ['class' => 'form-control','placeholder'=>trans('language.tc_date'), 'id' => 'tc_date']) !!}
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <lable class="from_one1">{!! trans('language.tc_remark') !!} :</lable>
                    <div class="form-group">
                        {!! Form::textarea('tc_remark','',array('id' => 'tc_remark','class'=>'form-control no-resize','placeholder'=>trans('language.tc_remark'),'rows' => 3, 'cols' => 50)) !!}
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <lable class="from_one1">{!! trans('language.tc_reason') !!} :</lable>
                    <div class="form-group">
                        {!! Form::textarea('tc_reason','',array('id' => 'tc_reason', 'class'=>'form-control no-resize','placeholder'=>trans('language.tc_reason'),'rows' => 3, 'cols' => 50)) !!}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                    <div class="col-lg-2 col-md-2">
                    {!! Form::submit('Save', ['id'=>'submit_tc','class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit']) !!}
                </div>
                <div class="col-lg-3 col-md-3">
                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/student/get-tc-records')}}',
                data: function (d) {
                    d.class_id = $('select[name="class_id"]').val();
                    d.section_id = $('select[name="section_id"]').val();
                    d.enroll_no = $('input[name="enroll_no"]').val();
                    d.student_name = $('input[name="student_name"]').val();
                    d.student_status = 1;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'class_section', name: 'class_section'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            var student_attendance_date = $("#student_attendance_date").val();
            if(class_id != "" && section_id != "" && student_attendance_date !=""){
                $("#list-block").show();
                table.draw();
                e.preventDefault();
            } else {
                $("#list-block").hide();
            }
            e.preventDefault();
        });
          
        $('#clearBtn').click(function(){
           location.reload();
        })

        $('#info-form').on('submit', function(e) { 
            var formData = $(this).serialize();
            $(".mycustloading").show();
            // $('#submit_tc').attr('disabled',true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{url('admin-panel/student/save-tc')}}",
                data     : formData,
                cache    : false,

                success  : function(data) {
                data = $.trim(data);
                    if(data == "success"){
                        $(".mycustloading").hide();
                        $("#success-block").html("TC Successfully Genrated !!");
                        $("#success-block").show();
                        $("#error-block").hide();
                        $('#submit_tc').attr('disabled',true);
                        table.draw();

                    } else {
                        $(".mycustloading").hide();
                        $("#error-block").html(data);
                        $("#error-block").show();
                        $("#success-block").hide();
                        table.draw();
                    }
                }
            })
            e.preventDefault();
        });

        $(document).on('click','.generate-tc',function(e){
            var class_id = $(this).attr('class-id');
            var section_id = $(this).attr('section-id');
            var student_id = $(this).attr('student-id');
            var student_name = $(this).attr('student-name');
            var class_section = $(this).attr('class-section');
            $(".mycustloading").show();
            $('#tcModel').modal('show');
            $("#s_student_name").html(student_name);
            $("#class_section").html(class_section);
            $("#s_class_id").val(class_id);
            $("#s_section_id").val(section_id);
            $("#s_student_id").val(student_id);
            $(".mycustloading").hide();
        });

    });

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

    jQuery(document).ready(function () {
        
        $("#search-form").validate({
            /* @validation states + elements   ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                class_id: {
                    required: true,
                },
                section_id: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });

        $("#info-form").validate({
            /* @validation states + elements   ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                tc_date: {
                    date: true,
                    required: true,
                },
                tc_remark: {
                    required: true,
                },
                tc_reason: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
    });

</script>
@endsection




