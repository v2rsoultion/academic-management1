@if(isset($onetime['ot_head_id']) && !empty($onetime['ot_head_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('ot_head_id',old('ot_head_id',isset($onetime['ot_head_id']) ? $onetime['ot_head_id'] : ''),['class' => 'gui-input', 'id' => 'ot_head_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info onetime -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.particular_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('ot_particular_name', old('ot_particular_name',isset($onetime['ot_particular_name']) ? $onetime['ot_particular_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.particular_name'), 'id' => 'ot_particular_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.alias') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('ot_alias', old('ot_alias',isset($onetime['ot_alias']) ? $onetime['ot_alias']: ''), ['class' => 'form-control','placeholder'=>trans('language.alias'), 'id' => 'ot_alias']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fee_classes') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('ot_classes[]', $onetime['arr_class'],isset($onetime['ot_classes']) ? $onetime['ot_classes'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'ot_classes', 'multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fee_amount') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('ot_amount', old('ot_amount',isset($onetime['ot_amount']) ? $onetime['ot_amount']: ''), ['class' => 'form-control','placeholder'=>trans('language.fee_amount'), 'id' => 'ot_amount', 'min'=>'1', 'max'=>'999999']) !!}
        </div>
        @if ($errors->has('ot_amount')) <p class="help-block">{{ $errors->first('ot_amount') }}</p> @endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fee_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('ot_date', old('ot_date',isset($onetime['ot_date']) ? $onetime['ot_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.fee_date'), 'id' => 'ot_date']) !!}
        </div>
        @if ($errors->has('ot_date')) <p class="help-block">{{ $errors->first('ot_date') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fee_refundable') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('ot_refundable', $onetime['arr_refundable'],isset($onetime['ot_refundable']) ? $onetime['ot_refundable'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'ot_refundable'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('ot_refundable')) <p class="help-block">{{ $errors->first('ot_refundable') }}</p> @endif
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_for') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('for_students', $onetime['arr_student_for'],isset($onetime['student_for']) ? $onetime['student_for'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_for'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('student_for')) <p class="help-block">{{ $errors->first('student_for') }}</p> @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.deduct_imprest_status') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('deduct_imprest_status', $onetime['arr_deduct_imprest_status'],isset($onetime['deduct_imprest_status']) ? $onetime['deduct_imprest_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'deduct_imprest_status'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('deduct_imprest_status')) <p class="help-block">{{ $errors->first('deduct_imprest_status') }}</p> @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.receipt_status') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('receipt_status', $onetime['arr_receipt_status'],isset($onetime['receipt_status']) ? $onetime['receipt_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'receipt_status'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('receipt_status')) <p class="help-block">{{ $errors->first('receipt_status') }}</p> @endif
        </div>
    </div>
    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $.validator.addMethod("minDate", function(value, element) {
            var curDate = new Date();
            var inputDate = new Date(value);
            if (inputDate >= curDate)
                return true;
            return false;
        }, "Please select future dates");
        $("#onetime-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                ot_particular_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                ot_alias: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                ot_classes: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                ot_amount: {
                    required: true,
                    number: true
                },
                ot_date: {
                    required: true,
                    date: true,
                    minDate: true,
                },
                ot_refundable: {
                    required: true
                },
                for_students: {
                    required: true
                },
                deduct_imprest_status: {
                    required: true
                },
                receipt_status: {
                    required: true
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
       // $('#ot_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false  });

    });

    

</script>

