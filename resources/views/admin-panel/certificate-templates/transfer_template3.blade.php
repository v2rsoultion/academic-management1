<!DOCTYPE html>
<html>
<head>
	<title>@if(isset($page_title)) {!! $page_title !!} @else TC -3 Certificate @endif </title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 16px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		text-decoration: underline;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		padding: 10px 0px;
		/*border-radius: 10px;*/
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 16px;
		font-style: italic;
	}
	.dotted {
		/*border-bottom: 2px dotted #000;*/
		font-size: 16px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>
</head>
<body>
	
	<div class="header">
	
  		<h4 class="align header_gap" style="margin-top:10px; font-size: 25px;margin-bottom: 1px;"> @if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else New Middle East International School - Rivadh @endif <br> Application for Transfer Certificate</h4>
		<br>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 105px !important;" class="content_size">Student Name: </td>
			<td style="width: 800px" class="text align_left"> @if(isset($certificate_info)) {!! $certificate_info['class'] !!} @else Amit Gupta @endif</td>
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 125px !important;" class="content_size">Class & Section: </td>
			<td style="width: 200px" class="text align_left"> @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else A @endif</td>
			<td  style ="width: 110px !important;" class="content_size">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Enroll No.: </td>
			<td style="width: 200px" class="text align_left">@if(isset($certificate_info)) {!! $certificate_info['student_enroll_number'] !!} @else 10011 @endif</td>
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 105px !important;" class="content_size">Date of Birth: </td>
			<td style="width: 200px" class="text align_left"> @if(isset($certificate_info)) {!! $certificate_info['student_dob'] !!} @else 02-04-1994 @endif</td>
			<td  style ="width: 145px !important;" class="content_size">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Admission Date: </td>
			<td style="width: 200px" class="text align_left">  @if(isset($certificate_info)) {!! $certificate_info['admission_date'] !!} @else 02-04-1994 @endif</td>
			<td  style ="width: 105px !important;" class="content_size">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nationality: </td>
			<td style="width: 160px" class="text align_left">@if(isset($certificate_info)) {!! $certificate_info['nationality'] !!} @else Indian @endif</td>
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 105px !important;" class="content_size">Library Dues: </td>
			<td style="width: 230px" class="dotted align_left"> Yes / No</td>
			<td style ="width: 175px !important;" class="align_right content_size">Librarian's Signature: &nbsp;&nbsp;</td>
			<td style="width: 200px" class="text align_left"> </td>
			
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 145px !important;" class="content_size">Attendance up to: </td>
			<td style="width: 210px" class="dotted align_left"> @if(isset($certificate_info)) {!! $certificate_info['total_attendance'] !!} @else 311 @endif </td>
			<!-- <td style ="width: 115px !important;" class="content_size"> Fees paid up to: </td>
			<td style="width: 200px" class="text align_left"> @if(isset($certificate_info)) {!! $certificate_info['student_enroll_number'] !!} @else 111 @endif </td> -->
			
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 145px !important;" class="content_size">Reason for Leaving: </td>
			<td style="width:765px" class="text align_left">@if(isset($certificate_info)) {!! $certificate_info['tc_reason'] !!} @else @endif </td>
		</tr>
	</table>
	<!-- <table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 125px !important;" class="content_size">Place of transfer: </td>
			<td style="width: 300px" class="text align_left"> </td>
			<td style ="width: 125px !important;" class="align_right content_size"> Password No.: </td>
			<td style="width: 330px" class="text align_left"> </td>
			
		</tr>
	</table> -->
	<!-- <table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 205px !important;" class="content_size">Ministry Attestation required: </td>
			<td style="width: 230px" class="dotted align_left"> Yes / No</td>
			
			
		</tr>
	</table> -->
	<!-- <table style="margin-top: -25px;">
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 255px !important;" class="content_size">If <b style="text-decoration: underline;">YES,</b> kindly submit the following : </td>
			<td style="width: 330px;padding-top: 30px;" class="dotted align_left"> 1) Password and Iqama copies of parent and child<br>2)Copy of Exit Re-entry of Visa / Exit papers<br>3) Original report cards of all the previous years</td>
		</tr>
	</table> -->
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 105px !important;" class="content_size">Father's Name: </td>
			<td style="width: 800px" class="text align_left"> @if(isset($certificate_info)) {!! $certificate_info['student_father_name'] !!} @else Arun Gupta @endif</td>
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 95px !important;" class="content_size">Contact No.: </td>
			<td style="width: 300px" class="text align_left">@if(isset($certificate_info)) {!! $certificate_info['father_contact'] !!} @else @endif </td>
			<td style ="width: 170px !important;" class="align_right content_size"> Signature & Date: </td>
			<td style="width: 330px" class="text align_left"> </td>
			
		</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td style ="width: 150px !important;" class="content_size">Supervisor's signature: </td>
			<td style="width: 250px" class="text align_left"> </td>
			<td style ="width: 190px !important;" class="align_right content_size"> Principal's signature: </td>
			<td style="width: 300px" class="text align_left"> </td>
			
		</tr>
	</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>