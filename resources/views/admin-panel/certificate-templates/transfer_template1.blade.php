<!DOCTYPE html>
<html>
<head>
	<title>@if(isset($page_title)) {!! $page_title !!} @else TC Certificate @endif</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 16px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 16px;
		font-style: italic;
	}
	.dotted {
		border-bottom: 2px dotted #000;
		font-size: 13px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	/*table{
		height: 40px;

	}*/
	</style>
</head>
<body>
	
	<div class="header">
  		<h4 class="align header_gap" style="margin-top:10px; font-size: 25px;margin-bottom: 1px;">@if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else APEX SENIOR SECONDARY SCHOOL @endif</h4>
  		<p class="align" style="font-size: 11px;margin-top: 0px;margin-bottom: 5px;"> @if(isset($certificate_info)) {!! $certificate_info['school_address'] !!}, {!! $certificate_info['city'] !!}, {!! $certificate_info['state'] !!}, {!! $certificate_info['country'] !!} - {!! $certificate_info['school_pincode'] !!} @else No. 8-2-404/1, Road No 6, Near Times Hospital, Street Opp GVK. Banjara Hills, Hyderabad- 500034 @endif</p>
  		<h5 class="align" style="font-size:14px;margin:0px;">@if(isset($certificate_info)) {!! $certificate_info['school_description'] !!} @else Affiliated to Central Board of Secondary Education, New Delhi @endif</h5>
  		<p class="align" style="font-size: 14px;font-weight: bold;margin-bottom: 10px;margin-top: 5px;">Affiliation No. &nbsp; @if(isset($certificate_info)) {!! $certificate_info['school_registration_no'] !!} @else 130345 @endif</p>
		<!-- <div class="align" style="margin: 15px 0px;">
		<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px"></div> -->
		<div class="align" style="width: 400px;margin:auto; font-size:20px;"><b>TRANSFER CERTIFICATE</b></div>

		<table style="font-weight: bold;">
			<tr>
				<td style ="width: 65px !important;" class="align_right content_size"> Book </td> 
				<td style="width: 80px" class="dotted align"> @if(isset($certificate_info)) {!! $certificate_info['book_no'] !!} @else 2 @endif </td>
				<td style ="width: 327px !important;" class="align_right content_size"> SI. No.</td> 
				<td style="width: 80px" class="dotted align"> @if(isset($certificate_info)) {!! $certificate_info['serial_no'] !!} @else 70 @endif </td>
				
			</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 573px !important;" class="content_size">1) &nbsp;&nbsp; Name of the Student</td>
			<td style="width: 368px" class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else  Fayeza Qaisar @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">2) &nbsp;&nbsp; Father's/Guardian's Name</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_father_name'] !!} @else Qaisar Ansari @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">3) &nbsp;&nbsp; Nationality</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['nationality'] !!} @else Indian @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">4) &nbsp;&nbsp; Whether the candidate belongs to Schedule Caste or Schedule Tribe</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Yes</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">5) &nbsp;&nbsp; Date of admission in the School with class</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['admission_date'] !!} @else 04-06-2015 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">6) &nbsp;&nbsp; Date of Birth (in Chrisitan Era) according to Admission Register</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_dob'] !!} @else 03-01-2004 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">7) &nbsp;&nbsp; Class in which the student last studied</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['class'] !!} @else Std  VI @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">8) &nbsp;&nbsp; School/Board Annual examination last taken with result</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['pass_fail'] !!} @else Passed @endif</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">9) &nbsp;&nbsp; Whether failed, if so once / twice in the same class </td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">9) &nbsp;&nbsp; Subjects</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['all_subjects'] !!} @else English, Maths, Science, S. Sc @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">10) &nbsp;&nbsp; Languages</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['language'] !!} @else Hindi @endif</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">12) &nbsp;&nbsp; Whether qualified for promotion to the higher class if so, to which class</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">13) &nbsp;&nbsp; Any fee concession availed of : if so, the nature of such concession</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">11) &nbsp;&nbsp; Total No. of working days</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['total_days'] !!} @else 18 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">12) &nbsp;&nbsp; Total No. of working days present</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['total_attendance'] !!} @else 11 @endif</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">16) &nbsp;&nbsp; Games played or extra curricular activities in which the student usually took part</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Basket Ball</td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">17) &nbsp;&nbsp; General Conduct</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Good</td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">13) &nbsp;&nbsp; Date of application for transfer certificate</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_created_date'] !!} @else 28-04-2016 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">14) &nbsp;&nbsp; Date of issue of transfer certificate</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_issue_date'] !!} @else 04-05-2016 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">15) &nbsp;&nbsp; Reason for leaving the school</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_reason'] !!} @else Transfer @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">16) &nbsp;&nbsp; Any other remark</td>
			<td class="text align_left"> - &nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_remark'] !!} @else - @endif </td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<td style="width: 115px" class="align_right content_size sign">Signature of Class Teacher</td>
			<td style="width: 712px" class="align content_size sign">Checked by<br> State Full Name & Designation </td>
			<td style="width: 145px" class="align content_size sign">Principal<br>Seal</td>
		</tr>
	</table>
	<!-- <table>
		<tr>
			<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
			<td style="width: 712px" class="align content_size sign"> </td>
			<td style="width: 145px" class="align content_size sign">Signature</td>
		</tr>
	</table> -->
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>