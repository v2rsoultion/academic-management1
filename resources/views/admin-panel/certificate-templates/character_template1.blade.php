<!DOCTYPE html>
<html>
<head>
	<title>@if(isset($page_title)) {!! $page_title !!} @else Character Certificate @endif</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 17px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>

</head>
<body>
	<div class="header">
	
  		<h4 class="align header_gap">@if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else APEX SENIOR SECONDARY SCHOOL @endif</h4>
  		<h5 class="align" style="font-size:16px;margin-top:10px;">@if(isset($certificate_info)) {!! $certificate_info['school_description'] !!} @else An English medium Co-education Sr. Secondary School @endif</h5>
		<div class="align" style="margin-top: 15px;margin-bottom: 5px;">
		@if(isset($certificate_info) && $certificate_info['logo'] != '')
			<img src="{!! URL::to($certificate_info['logo']) !!}" alt="{!! $certificate_info['school_name'] !!}" width="60px"> 
		@else 
			<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" width="60px">
		@endif
		
		</div>
		<div class="align" style="width: 720px;margin-bottom:15px; font-size:13px; text-align: center;"><b>@if(isset($certificate_info)) SESSION {!! $certificate_info['session_year'] !!} @else SESSION 2018-2019 @endif</b></div>
		<div class="align" style="width: 400px;margin:auto; font-size:25px;"><b>CHARACTER CERTIFICATE</b></div>


		<p class="align content_size"></p>
		<table>
			<tr>
				<td style ="width: 325px !important;" class="align_right content_size">This is to certify that I know Shri/Smt./Ku. &nbsp;</td> 
				<td style="width: 340px" class="text"> @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else  Khushbu Vaishnav @endif</td>
				 
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 179px" class="align_right content_size"> Son/daughter of Shri &nbsp;</td>
				<td style="width:400px;" class=" text"> @if(isset($certificate_info)) {!! $certificate_info['student_father_name'] !!} @else Nemi Chand Vaishnav @endif</td>
				<td style="width: 92px" class="content_size">, resident of </td></tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width:432px;" class=" text"> @if(isset($certificate_info)) {!! $certificate_info['address'] !!} @else  Ratanada, Jodhpur @endif</td>
				<td style="width: 92px" class="content_size"> &nbsp; for the last </td>
				<td style="width: 50px;" class="align text">  @if(isset($certificate_info)) {!! $certificate_info['total_year'] !!} @else 2 @endif </td>
				<td style="width: 55px" class="align content_size"> &nbsp;&nbsp; years. </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 128px" class="align_right content_size"> Shri/Smt./Ku. &nbsp;</td>
				<td style="width:232px;" class=" text"> @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else Khushbu Vaishnav @endif</td>
				<td style="width: 320px" class="content_size"> bears good moral character and to the best</td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width: 592px" class="content_size"> of my knowledge is not involved in any criminal activity and no personal legal case is </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width: 592px" class="content_size"> pending against him/her. </td>
			</tr>
		</table>
		<br><br>
		<table>
			<tr>
				<td style="width: 145px" class="align_right content_size sign">Date: @if(isset($certificate_info)) {!! $certificate_info['cc_issue_date'] !!} @else 24-10-2018 @endif</td>
				<td style="width: 420px" class="align content_size sign"> </td>
				<td style="width: 145px" class="align content_size sign">Signature</td>
			</tr>
	
				
		</table>
	</div>
</body>
</html>

<script type="text/javascript">
	window.print();
</script>