<!DOCTYPE html>
<html>
<head>
	<title>@if(isset($page_title)) {!! $page_title !!} @else TC - 2 Certificate @endif</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 14px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border: 1px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		/*border-bottom: 2px solid #000;*/
		font-size: 14px;
		font-style: italic;
	}
	.dotted {
		/*border-bottom: 2px dotted #000;*/
		font-size: 14px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	.sub_heading{
		margin-top:0px;
		margin-bottom: 1px;
		font-size: 20px;
		color:#1f2f60;
		font-weight: bold;
		font-style: italic;
	}
	.affiliate {
		font-size:14px; 
		width: 230px;
		text-align: center; 
		font-weight: bold;
	}
	.bold {
		font-weight: bold;
	}
	/*tr{
		height: 25px;

	}*/
	</style>
</head>
<body>
	
	<div class="header">
		<table>
			<tr>
				<td style="width: 150px;" class="align">
				@if(isset($certificate_info) && $certificate_info['logo'] != '')
					<img src="{!! URL::to($certificate_info['logo']) !!}" alt="{!! $certificate_info['school_name'] !!}" width="80px" style="margin-top: 15px;"> 
				@else 
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px" style="margin-top: 15px;">
				@endif
				</td>
				<td style="width: 800px">
					<h3 class="align header_gap" style="margin-top:10px;margin-bottom: 0px;">@if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else Vidhyashram @endif</h3>
					<!-- <p class="align header_gap sub_heading">@if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else International School @endif</p> -->
  					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> @if(isset($certificate_info)) {!! $certificate_info['school_address'] !!}, {!! $certificate_info['city'] !!}, {!! $certificate_info['state'] !!}, {!! $certificate_info['country'] !!} - {!! $certificate_info['school_pincode'] !!} @else Uchiyarda Nandra Kallan, via-Saran Nagar, P.O. Jodhpur (Raj.) - 342015 @endif <br> 
					  
					  @if(isset($certificate_info)) Tel. No. : {!! $certificate_info['school_telephone'] !!} @else Tel. No. : 0291-2227976, 3206327  @endif&nbsp;&nbsp; E-mail : @if(isset($certificate_info)) {!! $certificate_info['school_email'] !!} @else info@vidhyashram.edu.in @endif<br> Website : @if(isset($certificate_info)) {!! $certificate_info['school_url'] !!} @else www.vidhyashram.edu.in @endif</p>	
  					</td>
  			</tr>
		</table>
		<table>
			<tr>
				<td>
					<p class="affiliate">Affiliated No. &nbsp; @if(isset($certificate_info)) {!! $certificate_info['school_registration_no'] !!} @else 130345 @endif</p>
  				</td>
  				<td style="width: 540px;" class="align"><h6 style="margin:0px;">TRANSFER CERTIFICATE</h6></td>
  				<td>
					<p class="affiliate">School Code - No. &nbsp; @if(isset($certificate_info)) {!! $certificate_info['school_sno_numbers'] !!} @else 16105 @endif</p>
  				</td>
			</tr>
		</table>

		<hr style="margin-top: 0px;margin-bottom: 0px;">

		<table>
			<tr>
				<td style ="width: 150px !important;" class="align content_size"> Certificate No. </td> 
				<td style="width: 80px" class="dotted align bold"> @if(isset($certificate_info)) TC{!! $certificate_info['tc_no'] !!} @else 247/17-18 @endif </td>
				
				<!-- <td style ="width: 547px !important;" class="align_right content_size"> Admission  No.</td> 
				<td style="width: 80px" class="dotted align bold"> 70  </td> -->
			</tr>
		</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td class="content_size">01. &nbsp;&nbsp; Name of the Student</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else  A. TILAK DAS @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">02. &nbsp;&nbsp; Mother's Name</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_mother_name'] !!} @else  SHEETAL DAS @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">03. &nbsp;&nbsp; Father's Name/Guardian's Name</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_father_name'] !!} @else  ARUN DAS @endif</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">04. &nbsp;&nbsp; Date of Birth (in Chrisitan Era) according to Admission & Withdrawal Register</td>
			<td class="content_size"> &nbsp;&nbsp; <b>03/05/2001</b> &nbsp;&nbsp;&nbsp;&nbsp; (in words)  <b>THREE MAY TWO THOUSAND ONE</b></td>
		</tr> -->
		<tr>
			<td> </td>
			<td class=" content_size">04. &nbsp;&nbsp; Nationality</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['nationality'] !!} @else  Indian @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">05. &nbsp;&nbsp; Whether the candidate belongs to Schedule Caste or Schedule Tribe</td>
			<td class="text align_left bold">&nbsp;&nbsp;  YES</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">07. &nbsp;&nbsp; Date of First Admission in the School with class</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else  20/06/17 @endif &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; XI</td>
		</tr>
		 -->
		<tr>
			<td> </td>
			<td class=" content_size">06. &nbsp;&nbsp; Class in which the student Last Studied(in Figures)</td>
			<td class="text align_left bold">&nbsp;&nbsp;@if(isset($certificate_info)) {!! $certificate_info['class'] !!} @else  XI C @endif &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">09. &nbsp;&nbsp; School/Board's Annual examination last taken with result</td>
			<td class="text align_left bold">&nbsp;&nbsp;  CLASS XI FINAL EXAM</td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">10. &nbsp;&nbsp; Whether failed, if so once / twice in the same class </td>
			<td class="text align_left bold">&nbsp;&nbsp;  NO </td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">07. &nbsp;&nbsp; Subject Studied</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['all_subjects'] !!} @else  ENGLISH, GEOGRAPHY, HISTORY, P.E., POL. SCIENCE @endif</td>
		</tr>
		
		<!-- <tr>
			<td> </td>
			<td class="content_size">12. &nbsp;&nbsp; Whether qualified for Promotion to the higher class if so, to which class(in fig.)</td>
			<td class="text align_left bold">&nbsp;&nbsp; XII &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (in words)&nbsp;&nbsp;  TWELVETH</td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">13. &nbsp;&nbsp; Month up to which the (student has paid) school dues</td>
			<td class="text align_left bold">&nbsp;&nbsp;  JAN TO MARCH &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NIL </td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">14. &nbsp;&nbsp; Any fee concession availed of : if so, the nature of such concession</td>
			<td class="text align_left bold">&nbsp;&nbsp;  N/A </td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">08. &nbsp;&nbsp; Total No. of Working Days in the academic session</td>
			<td class="text align_left bold">&nbsp;&nbsp;  @if(isset($certificate_info)) {!! $certificate_info['total_days'] !!} @else 190 @endif days</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">09. &nbsp;&nbsp; Total No. of working days present in the school</td>
			<td class="text align_left bold">&nbsp;&nbsp;  @if(isset($certificate_info)) {!! $certificate_info['total_attendance'] !!} @else 134 @endif days</td>
		</tr>
		<!-- <tr>
			<td> </td>
			<td class="content_size">17. &nbsp;&nbsp; Whether NCC Cadet/Boy Scout/Girl Guide (details may be given)</td>
			<td class="text align_left bold">&nbsp;&nbsp;  -------------</td>
		</tr> -->
		<!-- <tr>
			<td> </td>
			<td class="content_size">18. &nbsp;&nbsp; Games played or extra curricular activities in which the student usually took part</td>
			<td class="text align_left bold">&nbsp;&nbsp;  -------------</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">19. &nbsp;&nbsp; General Conduct</td>
			<td class="text align_left bold">&nbsp;&nbsp;  GOOD</td>
		</tr> -->
		<tr>
			<td> </td>
			<td class="content_size">10. &nbsp;&nbsp; Date of Application for Certificate</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_created_date'] !!} @else 20/04/18 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">11. &nbsp;&nbsp; Date of issue of Certificate</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_issue_date'] !!} @else 24/04/18 @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">12. &nbsp;&nbsp; Reason for Leaving the School</td>
			<td class="text align_left bold">&nbsp;&nbsp; @if(isset($certificate_info)) {!! $certificate_info['tc_reason'] !!} @else PARENTS REQUEST @endif</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">13. &nbsp;&nbsp; Any other remarks</td>
			<td class="text align_left bold">&nbsp;&nbsp;@if(isset($certificate_info)) {!! $certificate_info['tc_remark'] !!} @else NIL @endif</td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<td style="width: 115px" class="align_right content_size sign">Signature of<br> Class Teacher</td>
			<td style="width: 712px" class="align content_size sign">Checked by<br> State Full Name & Designation </td>
			<td style="width: 145px" class="align content_size sign">Principal Signature & Seal</td>
		</tr>
	</table>
	<!-- <table>
		<tr>
			<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
			<td style="width: 712px" class="align content_size sign"> </td>
			<td style="width: 145px" class="align content_size sign">Signature</td>
		</tr>
	</table> -->
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>