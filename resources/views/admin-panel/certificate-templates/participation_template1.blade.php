<!DOCTYPE html>
<html>
<head>
<title>{!! $page_title !!}</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 17px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>

	
</head>
<body>
	<div class="header">
	
	<h4 class="align header_gap"> @if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else  APEX SENIOR SECONDARY SCHOOL @endif</h4>
  		<h5 class="align" style="font-size:16px;margin-top:10px;"> @if(isset($certificate_info)) {!! $certificate_info['school_description'] !!} @else  An English medium Co-education Sr. Secondary School @endif</h5>
		
		<div class="align" style="margin: 15px 0px;">
		@if(isset($certificate_info) && $certificate_info['logo'] != '')
			<img src="{!! URL::to($certificate_info['logo']) !!}" alt="{!! $certificate_info['school_name'] !!}" width="60px"> 
		@else 
			<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px">
		@endif
		</div>
		<div class="align" style="width: 400px;margin:auto; font-size:25px;"><b> @if(isset($certificate_info)) {!! $certificate_info['competition_name'] !!} @else  Inter - School English Debate @endif</b></div>

		<p class="align content_size"></p>
		<table>
			<tr>
				<td style ="width: 180px !important;" class="align_right content_size"> This is to certify that &nbsp;</td> 
				<td style="width: 350px" class="text">  @if(isset($certificate_info)) {!! $certificate_info['student_name'] !!} @else  Khushbu Vaishnav @endif</td>
				<td style ="width: 177px !important;" class=" content_size">&nbsp; has participated in </td> 
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 28px;"> </td>
				<td style="width: 280px" class=" text">  @if(isset($certificate_info)) {!! $certificate_info['competition_name'] !!} @else Inter - School English Debate @endif</td>
				<td style ="width: 175px !important;" class="content_size">competition held under </td>
				<td style="width:200px;" class=" text">  @if(isset($certificate_info)) {!! $certificate_info['school_name'] !!} @else Apex Senior Secondary @endif</td>
				<td style="width: 33px;"> </td>
			</tr>
		</table>
		
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style ="width: 602px !important;" class=" content_size"> We appreciate your participation and wish you all the best for your bright future.</td> 
				
			</tr>
		</table>
	
		<br><br><br>
		<table>
			<tr>
				<td style="width: 145px" class="align_right content_size sign">Date: @if(isset($certificate_info)) {!! $certificate_info['issue_date'] !!} @else 24-10-2018 @endif</td>
				<td style="width: 420px" class="align content_size sign"> </td>
				<td style="width: 145px" class="align content_size sign">Signature</td>
			</tr>
	
				
		</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>