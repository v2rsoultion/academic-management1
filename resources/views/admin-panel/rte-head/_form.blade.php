@if(isset($rtehead['rte_head_id']) && !empty($rtehead['rte_head_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('rte_head_id',old('rte_head_id',isset($rtehead['rte_head_id']) ? $rtehead['rte_head_id'] : ''),['class' => 'gui-input', 'id' => 'rte_head_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.rte_h_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('rte_head', old('rte_head',isset($rtehead['rte_head']) ? $rtehead['rte_head']: ''), ['class' => 'form-control','placeholder'=>trans('language.rte_h_name'), 'id' => 'rte_head']) !!}
        </div>
        @if ($errors->has('rte_head')) <p class="help-block">{{ $errors->first('rte_head') }}</p> @endif
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.rte_h_amt') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('rte_amount', old('rte_amount',isset($rtehead['rte_amount']) ? $rtehead['rte_amount']: ''), ['class' => 'form-control','placeholder'=>trans('language.rte_h_amt'), 'id' => 'rte_amount', 'min'=> '1']) !!}
        </div>
        @if ($errors->has('rte_amount')) <p class="help-block">{{ $errors->first('rte_amount') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
    <lable class="from_one1">&nbsp;&nbsp;</lable> <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
    
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#rte-head-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                rte_head: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                rte_amount: {
                    required: true,
                    number:true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>