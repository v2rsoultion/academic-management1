@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">Imprest A/C</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Fees</strong> Counter </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('ot_head_id', $listData['arr_fees'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'ot_head_id','onChange' => 'getClass(this.value);checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('section_id', $listData['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: none">
                            <hr>
                            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
                            <div class="row clearfix">
                                <div class="col-lg-2"><b>Total Students : </b> <span id="applied_students"></span></div>
                                <div class="col-lg-2"><b>Students(Pay fees): </b> <span id="pay_students"></span></div>
                                <div class="col-lg-2"><b>Students(Pay Due): </b> <span id="unpay_students"></span></div>
                                <div class="col-lg-6 col-md-6 ">
                                    {!! Form::submit('Deduct', ['class' => 'btn btn-raised btn-round btn-primary float-right','name'=>'submit']) !!}
                                </div>
                            </div>
                            
                            <br />
                            <div class="alert alert-danger" role="alert" id="error-block" style="display: none">
                            </div>
                            <div class="alert alert-success" role="alert" id="success-block" style="display: none">
                            </div>
                            
                            <div class="table-responsive" >
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>
                                                <div class="checkbox" id="customid">
                                                    <input type="checkbox" id="check_all" class="check" >
                                                    <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
                                                </div> Select
                                            </th>
                                            <th>{{trans('language.student_enroll_number')}}</th>
                                            <th>{{trans('language.name')}}</th>
                                            <th>Available Imprest Amount</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
      var table = $('#student-table').DataTable({
          pageLength: 20,
          processing: true,
          serverSide: true,
          bLengthChange: false,
          bPaginate: false,
          bInfo : false,
          bFilter: false,
          ajax: {
              url: "{{url('admin-panel/imprest/get-student-list')}}",
              data: function (d) {
                  d.class_id = $('select[name=class_id]').val();
                  d.section_id = $('select[name=section_id]').val();
                  d.ot_head_id = $('select[name=ot_head_id]').val();
              }
          },
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex' },
              {data: 'select', name: 'select', orderable: false},
              {data: 'student_enroll_number', name: 'student_enroll_number'},
              {data: 'student_profile', name: 'student_profile'},
              {data: 'imprest_ac_amt', name: 'imprest_ac_amt'},
          ],
           columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
          ]
      });
      $('#search-form').on('submit', function(e) {
            table.draw();
            var head_id = $("#ot_head_id").val(); 
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $("#error-block").hide();
            $("#success-block").hide();
          if(class_id == "" && section_id == "" && head_id == ""){
              $("#list-block").hide();
          } else {
              $("#list-block").show();
              get_total_counts(head_id,class_id,section_id);
          }
          e.preventDefault();
      });

      $('#list-form').on('submit', function(e) {
            $("#error-block").hide();
            var head_id = $("#ot_head_id").val(); 
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            var formData = $(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{url('admin-panel/imprest/fees-deduct')}}",
                data     : formData,
                cache    : false,

                success  : function(data) {
                    var res = $.trim(data)
                    res = JSON.parse(res);
                    if(res.status == "success"){
                        $("#success-block").html(res.msg);
                        $("#success-block").show();
                        $("#error-block").hide();
                        table.draw();
                        get_total_counts(head_id,class_id,section_id);
                        $("#check_all").prop("checked",false);
                    } else if(res.status == "select"){
                        $("#error-block").html(res.msg);
                        $("#error-block").show();
                        $("#success-block").hide();
                    } else if(res.status == "error"){
                        $("#error-block").html(res.msg);
                        $("#error-block").show();
                        $("#success-block").hide();
                    }
                }
            })
            e.preventDefault();
        });
      
    });
    
    function getClass(fees_id)
    {
        if(fees_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/imprest/get-class-data')}}",
                type: 'GET',
                data: {
                    'fees_id': fees_id
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }
    
    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        var ot_head_id = $("#ot_head_id").val();
        if(class_id == "" || section_id == "" || ot_head_id == ""){
            $(':input[name="Search"]').prop('disabled', true);
        } else {
            $("#st_section_id").val(section_id);
            $("#st_class_id").val(class_id);
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            console.log(flag);
            $("#check_all").prop("checked", flag);
        });
    });

    function get_total_counts(head_id,class_id,section_id){
        if(head_id != "" &&  class_id != "" && section_id != ""){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/imprest/get-student-counts')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id' : section_id,
                    'head_id': head_id,
                    'head_type': 'ONETIME'
                },
                success: function (data) {
                    data = $.trim(data);
                    var res = JSON.parse(data);
                    console.log(res.pay_students);
                    $("#applied_students").html(res.total_students);
                    $("#pay_students").html(res.pay_students);
                    $("#unpay_students").html(res.unpay_students);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("#applied_students").html('0');
            $("#pay_students").html('0');
            $("#unpay_students").html('0');
        }
    }
    

    
</script>
@endsection