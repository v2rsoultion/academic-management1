<option value="">Select Term</option>
@if(!empty($terms))
  @foreach($terms as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif