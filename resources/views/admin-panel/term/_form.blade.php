@if(isset($term['term_exam_id']) && !empty($term['term_exam_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('term_exam_id',old('term_exam_id',isset($term['term_exam_id']) ? $term['term_exam_id'] : ''),['class' => 'gui-input', 'id' => 'term_exam_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.terms_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('term_name', old('term_name',isset($term['term_exam_name']) ? $term['term_exam_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.terms_name'), 'id' => 'term_name']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.terms_caption') !!} :</lable>
        <div class="form-group">
            {!! Form::text('term_exam_caption', old('term_exam_caption',isset($term['term_exam_caption']) ? $term['term_exam_caption']: ''), ['class' => 'form-control','placeholder'=>trans('language.terms_caption'), 'id' => 'term_exam_caption']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#term-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                term_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>