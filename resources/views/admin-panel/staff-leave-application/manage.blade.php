@extends('admin-panel.layout.header')
@section('content')
<style>
.modal-dialog {
    max-width: 730px !important;
}
.theme-blush .btn-primary {
    margin-top: 25px !important;
}
.select2-container .select2-selection--single .select2-selection__rendered {
    padding: 4px 20px;
}
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! trans('language.manage_leave_application') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-leave-management') !!}">{!! trans('language.menu_leave_management') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_leave_application') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.staff_name')}}</th>
                                            <th>{{trans('language.leave_date_range')}}</th>
                                            <th>{{trans('language.leave_days')}}</th>
                                            <th>{{trans('language.leave_app_attachment')}}</th>
                                            <th>{{trans('language.leave_app_reason')}}</th>
                                            <th>{{trans('language.leave_app_status')}}</th>
                                            <th>{{trans('language.manage_leave')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#leave-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/staff-leave-application/manage-data')}}",
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'date_range', name: 'date_range'},
                {data: 'leave_days', name: 'leave_days'},
                {data: 'staff_leave_attachment', name: 'staff_leave_attachment'},
                {data: 'staff_leave_reason', name: 'staff_leave_reason'},
                {data: 'staff_leave_status', name: 'staff_leave_status'},
                {data: 'previous_leave_details', name: 'previous_leave_details'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "18%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });
</script>
@endsection




