{!! Form::hidden('fees_head_id',old('fees_head_id',isset($fees_head['fees_head_id']) ? $fees_head['fees_head_id'] : ''),['class' => 'gui-input', 'id' => 'fees_head_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->

<div class="row clearfix"> 
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.fees_head_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('fees_head_name', old('fees_head_name',isset($fees_head['fees_head_name']) ? $fees_head['fees_head_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.fees_head_name'), 'id' => 'fees_head_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.fees_head_km_from') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('fees_head_km_from', old('fees_head_km_from',isset($fees_head['fees_head_km_from']) ? $fees_head['fees_head_km_from']: ''), ['class' => 'form-control','placeholder'=>trans('language.fees_head_km_from'), 'id' => 'fees_head_km_from']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.fees_head_km_to') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('fees_head_km_to', old('fees_head_km_to',isset($fees_head['fees_head_km_to']) ? $fees_head['fees_head_km_to']: ''), ['class' => 'form-control','placeholder'=>trans('language.fees_head_km_to'), 'id' => 'fees_head_km_to']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.fees_head_amount') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('fees_head_amount', old('fees_head_amount',isset($fees_head['fees_head_amount']) ? $fees_head['fees_head_amount']: ''), ['class' => 'form-control','placeholder'=>trans('language.fees_head_amount'), 'id' => 'fees_head_amount']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/transport') !!}" class="btn btn-raised" >Cancel</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        jQuery.validator.addMethod("greaterThan",
            function (value, element, param) {
                var $otherElement = $('#fees_head_km_from').val();
                return parseInt(value) > parseInt($otherElement);
            }, "Please enter value greater than Km Range (Min)" );

        $("#fees-head-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                fees_head_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                fees_head_km_from: {
                    required: true,
                    number: true,
                    min:1
                },
                fees_head_km_to: {
                    required: true,
                    number: true,
                    min:1,
                    greaterThan:true
                },
                fees_head_amount:{
                    required: true,
                    integer: true,
                    min:1,  
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });        
    });


</script>