@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/map-head/heads') !!}">{!! trans('language.map_heads') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                            <div class="row clearfix">
                                
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $heads['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $heads['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: none">
                            <hr>
                            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
                            <div class="row clearfix">
                                {!! Form::hidden('st_class_id','',['class' => 'gui-input', 'id' => 'st_class_id']) !!}
                                {!! Form::hidden('fees_st_map_id','',['class' => 'gui-input', 'id' => 'fees_st_map_id']) !!}
                                {!! Form::hidden('st_section_id','',['class' => 'gui-input', 'id' => 'st_section_id']) !!}
                                {!! Form::hidden('head_id',$heads['head_id'],['class' => 'gui-input', 'id' => 'head_id']) !!}
                                {!! Form::hidden('head_type',$heads['head_type'],['class' => 'gui-input', 'id' => 'head_type']) !!}
                                <div class="col-lg-3 col-md-3">
                                    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit']) !!}
                                </div>
                                <div class="col-lg-2"><b>Student Applied : </b> <span id="applied_students"></span></div>
                                <div class="col-lg-2"><b>Remaining: </b> <span id="remaining_students"></span></div>
                            </div>
                            <br />
                            <br />
                            <div class="alert alert-danger" role="alert" id="error-block" style="display: none">
                            </div>
                            <div class="alert alert-success" role="alert" id="success-block" style="display: none">
                            </div>
                            <div class="table-responsive" >    
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>Select</th>
                                            <th>{{trans('language.student_roll_no')}}</th>
                                            <th>{{trans('language.student_enroll_number')}}</th>
                                            <th>{{trans('language.name')}}</th>
                                            <th>{{trans('language.student_father_name')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
      var table = $('#student-table').DataTable({
          pageLength: 20,
          processing: true,
          serverSide: true,
          bLengthChange: false,
          bPaginate: false,
          bInfo : false,
          bFilter: false,
          ajax: {
              url: "{{url('admin-panel/map-heads/get-student-list')}}",
              data: function (d) {
                  d.class_id = $('select[name=class_id]').val();
                  d.section_id = $('select[name=section_id]').val();
                  d.head_id = $('input[name=head_id]').val();
                  d.head_type = $('input[name=head_type]').val();
              }
          },
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex' },
              {data: 'Select', name: 'Select'},
              {data: 'student_roll_no', name: 'student_roll_no'},
              {data: 'student_enroll_number', name: 'student_enroll_number'},
              {data: 'student_profile', name: 'student_profile'},
              {data: 'student_father_name', name: 'student_father_name'},
          ],
           columnDefs: [
              {
                  "targets": 3, // your case first column
                  "width": "15%"
              },
              {
                  targets: [ 0, 1, 2, 3],
                  className: 'mdl-data-table__cell--non-numeric'
              }
          ]
      });
      $('#search-form').on('submit', function(e) {
            table.draw();
            var head_id = $("#head_id").val();
            var head_type = $("#head_type").val();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $("#error-block").hide();
            $("#success-block").hide();
          if(class_id == "" && section_id == ""){
              $("#list-block").hide();
          } else {
              get_total_counts(head_id,head_type,class_id,section_id);
              $("#list-block").show();
          }
          e.preventDefault();
      });
      $('#list-form').on('submit', function(e) {
        // if ($('input:checkbox').filter(':checked').length < 1){
        //     $("#error-block").html("Please select atleast one student");
        //     $("#error-block").show();
            
        //     $("#success-block").hide();
        //     return false;
        // }
          var formData = $(this).serialize();
          console.log(formData);
          $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type     : "POST",
              url      : "{{url('admin-panel/map-heads/save-map-student')}}",
              data     : formData,
              cache    : false,

              success  : function(data) {
                data = $.trim(data);
                  console.log(data);
                  if(data == "success"){
                      $("#success-block").html("Data successfully store");
                      $("#success-block").show();

                  } else {
                      $("#error-block").html("Please select Fees Head");
                      $("#error-block").show();
                  }
              }
          })
          e.preventDefault();
      });

      $('#clearBtn').click(function(){
          location.reload();
      });
  });
    function get_total_counts(head_id,head_type,class_id,section_id){
        if(head_id != "" && head_type != "" &&  class_id != "" && section_id != ""){
           
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/map-heads/get-student-counts')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id' : section_id,
                    'head_id': head_id,
                    'head_type': head_type
                },
                success: function (data) {
                    data = $.trim(data);
                    console.log(data);
                    var res = JSON.parse(data);
                    $("#applied_students").html(res.map_students);
                    $("#fees_st_map_id").val(res.fees_st_map_id);
                    $("#remaining_students").html(res.remaining_students);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("#applied_students").html('0');
            $("#remaining_students").html('0');
        }
    }
    
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(class_id == "" || section_id == ""){
            $(':input[name="Search"]').prop('disabled', true);
        } else {
            $("#st_section_id").val(section_id);
            $("#st_class_id").val(class_id);
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }


</script>
@endsection

