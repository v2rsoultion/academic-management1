@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if(session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session()->get('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="headingcommon  col-lg-12">Payroll Configuration</div>
    <div class="row col-lg-12">
        <div class="col-lg-3">
            <lable class="from_one1">PF(Provident Fund)</lable>
        </div>
        <div class="col-lg-6">    
            <div class="form-group">
                <div class="radio">
                    <input id="radio21" name="applied_pf" type="radio" value="1">
                    <label for="radio21" class="document_staff">Yes</label>
                    <input id="radio22" name="applied_pf" type="radio" value="0" checked="true">
                    <label for="radio22" class="document_staff">No</label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <lable class="from_one1">ESI(Employee State Insurance)</lable>
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio23" name="applied_esi" type="radio" value="1">
                <label for="radio23" class="document_staff">Yes</label>
                <input id="radio24" name="applied_esi" type="radio" value="0" checked="true">
                <label for="radio24" class="document_staff">No</label>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <lable class="from_one1">TDS(Tax Deducted at Source)</lable>
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio25" name="applied_tds" type="radio" value="1">
                <label for="radio25" class="document_staff">Yes</label>
                <input id="radio26" name="applied_tds" type="radio" value="0" checked="true">
                <label for="radio26" class="document_staff">No</label>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <lable class="from_one1">PT(Professional Tax)</lable>
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio27" name="applied_pt" type="radio" value="1">
                <label for="radio27" class="document_staff">Yes</label>
                <input id="radio28" name="applied_pt" type="radio" value="0" checked="true">
                <label for="radio28" class="document_staff">No</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Update">Update
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-department') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#configuration").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                dept_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                dept_desc: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    }
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>