@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content profile-page">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.configuration') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-department') !!}">{!! trans('language.configuration') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" >
            <div class="card">
              <div class="body form-gap ">
                @if ($errors->any())
                  <div class="alert alert-danger" role="alert">
                    {{$errors->first()}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif 
                     
                {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                <div class="headingcommon col-lg-4 show" style="padding: 15px 0px;">Payroll Configuration :- </div>
                
                <div id="attendance-block">
                  <div class="float-right">
                    <button type="submit" class="btn btn-raised btn-primary" style="margin-top: -68px !important;" title="Save" >{{$submit_button}}
                    </button>
                  </div>
                  <div class="clearfix"></div>
                  @if(session()->has('success'))
                  <div class="alert alert-success" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif 
                  <div class="clearfix"></div>
                  <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="pay-config-table" style="width:100%">
                      {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>{{trans('language.s_no')}}</th>
                          <th>{{trans('language.pf_config_name')}}</th>
                          <th>{{trans('language.pf_enable_disable')}}</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
                {!! Form::close() !!}              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function () {
        var table = $('#pay-config-table').DataTable({
            //dom: 'Blfrtip',
            searching: false,
            pageLength: 200,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            ajax: {
                url: '{{url('admin-panel/payroll/configuration-data')}}'
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'pay_config_name', name: 'pay_config_name'},
                {data: 'pay_config_status', name: 'pay_config_status'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });
</script>
@endsection