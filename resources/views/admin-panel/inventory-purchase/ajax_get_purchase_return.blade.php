@if(count($purchase_return_data) != 0)
<div class="table-responsive">
  <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
    <thead>
      <tr>
        <th>No</th>
        <th>Item Names</th>
        <th>Invoice Quantity</th>
        <!-- <th>Stock Quantity</th> -->
        <th>Available</th>
        <th>Rate(Per item)</th>
        <th>Return Quantity</th>
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>
      
      <input type="hidden" name="total_amount" id="total_amount" value="0">
      @php $i = 0; @endphp
      @foreach($purchase_return_data as $purchase_return_datas)
      @php $i++; @endphp

        <input type="hidden" name="purchase_return[{{$i}}][purchase_id]" id="purchase_id_{{$i}}" value="{{ $purchase_return_datas['purchase_id'] }}">

        <input type="hidden" name="purchase_return[{{$i}}][item_id]" id="purchase_id_{{$i}}" value="{{ $purchase_return_datas['item_id'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][unit_id]" id="purchase_id_{{$i}}" value="{{ $purchase_return_datas['unit_id'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][category_id]" id="purchase_id_{{$i}}" value="{{ $purchase_return_datas['category_id'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][sub_category_id]" id="purchase_id_{{$i}}" value="{{ $purchase_return_datas['sub_category_id'] }}">

        <input type="hidden" name="purchase_return[{{$i}}][quantity]" id="quantity_{{$i}}" value="{{ $purchase_return_datas['quantity'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][available_quantity]" id="available_quantity_{{$i}}" value="{{ $purchase_return_datas['available_quantity'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][rate]" id="rate_{{$i}}" value="{{ $purchase_return_datas['rate'] }}">
        <input type="hidden" name="purchase_return[{{$i}}][amount]" id="return_amount_{{$i}}" value="">
        
        
        <tr>
          <td style="width: 1px;"> {{ $i }}</td>
          <td> {{ $purchase_return_datas['purchase_items_name'] }}</td>
          <td> {{ $purchase_return_datas['quantity'] }}</td>
          <!-- <td> {{ $purchase_return_datas['stock_quantity'] }}</td> -->
          <td> {{ $purchase_return_datas['available_quantity'] }}</td>
          <td> {{ $purchase_return_datas['rate'] }}</td>
          <td>
            <input type="text" name="purchase_return[{{$i}}][return_quantity]" id="return_quantity_{{$i}}" class="form-control" placeholder="" onchange="showAmount('{{$i}}')">
            <div style="color: red; display: none;" id="msg_rtn_qty_{{$i}}"> Please enter return Quantity </div>
            <div style="color: red; display: none;" id="msg_avbl_qty_{{$i}}"> Please enter Quantity less than available </div>
            <div style="color: red; display: none;" id="msg_inv_qty_{{$i}}"> Please enter Quantity less than invoice </div>
          </td>
          <td id="amount_{{$i}}" class="text-center"> </td>  
        </tr>

      @endforeach
    </tbody>
  </table>
</div>
<br>
<div id="manage_purchase">
  <div class="row">
    <div class="col-lg-10"></div>
    <div class="col-lg-2" style="font-size: 16px;">
      <input type="text" name="" class="form-control" id="totalAmount" readonly="true" value="Total: 0">
    </div>
  </div>
  <div class="row float-right" style="margin: 20px -14px;">
    <div class="col-lg-2">
      <button type="submit" class="btn btn-raised btn-primary" id="save_return" title="Save"> Save </button>
    </div>
  </div>
</div>

@else

<div class="table-responsive">
  <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
    <thead>
      <tr>
        <th class="text-center"> No Record Found </th>
      </tr>
    </thead>
  </table>
</div>

@endif