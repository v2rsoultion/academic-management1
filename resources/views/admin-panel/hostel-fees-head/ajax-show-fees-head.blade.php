<div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.hostel_fees_head') !!}:</lable>
</div>
<div class="row col-lg-12">
	
    @foreach($arr_fees_head_name_list as $key => $FeesHeadData)
      <div class="col-lg-2">
        <div class="checkbox" id="customid{{$key}}">
        <input id="checkbox{{$key}}" class="checkboxes headchecks" counter-id="{{$key}}" type="checkbox" value="{{$key}}" name="fee_head[]">
            <label  class="from_one1" style="margin-bottom: 0px !important;"  for="checkbox{{$key}}">{{$FeesHeadData}}</label>
        </div>
      </div>    
    @endforeach 
</div> 