@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<style type="text/css">
  th, td{
    text-align: center;
  }
  .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 10px;
    margin-top: 15px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 190px;
   }
   .heading {
    margin-top: 10px;
    padding: 10px 0px;
    background-color: #1f2f60;
    color: #fff;
  }
  .border {
    border: 1px solid #000;
}
</style>
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2> {!! trans('language.manage_stock_register') !!} </h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="#">{!! trans('language.manage_stock_register') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <ul class="nav nav-tabs tabing">
                  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#stockInwardReport">Stock Inward Report</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#stockOutwardReport">
                  Stock Outward Report</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#netStockReport">
                  Net Stock Report</a></li>
                </ul>

                <div class="tab-content">

                  <div class="tab-pane active" id="stockInwardReport">
                    <div class="row feecounterclass">  
                      <div style="width: 100%;margin-left: 15px;"><h6 class="text-center heading">Stock Inward Report</h6>
                      <table class="table border" id="" style="color: #000;">
                      {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th style="width: 125px;">Date</th>
                            <th>Product</th>
                            <th style="width: 160px;">Purchase Quantity</th>
                            <th style="width: 160px;">Return Quantity</th>
                            <th style="width: 160px;">Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($manage_stock['purchase_item'] as $purchase_items)
                          <tr>
                            <td>1</td>
                            <td> {{ $purchase_items['stock_date'] }}</td>
                            <td>{{ $purchase_items['purchase_items_name'] }}</td>
                            <td>{{ $purchase_items['purchase_quantity'] }}</td>
                            <td>{{ $purchase_items['return_quantity'] }}</td>
                            <td>{{ $purchase_items['purchase_quantity'] - $purchase_items['return_quantity'] }}</td>
                          </tr>
                          @endforeach

                        </tbody>
                      </table>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="stockOutwardReport">
                    <div class="row feecounterclass"> 
                      <div style="width: 100%;margin-left: 15px;"><h6 class="heading text-center">Stock Outward Report</h6>
                      <table class="table border" id="" style="color: #000;">
                      {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th style="width: 125px;">Date</th>
                            <th>Product</th>
                            <th style="width: 160px;">Sales Quantity</th>
                            <th style="width: 160px;">Return Quantity</th>
                            <th style="width: 160px;">Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($manage_stock['sale_item'] as $sale_items)
                          <tr>
                            <td>1</td>
                            <td> {{ $sale_items['stock_date'] }}</td>
                            <td>{{ $sale_items['sale_items_name'] }}</td>
                            <td>{{ $sale_items['sale_quantity'] }}</td>
                            <td>{{ $sale_items['return_quantity'] }}</td>
                            <td>{{ $sale_items['sale_quantity'] - $sale_items['return_quantity'] }}</td>
                          </tr>
                          @endforeach

                        </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                    
                    <div class="tab-pane fade" id="netStockReport">
                      <div class="row feecounterclass"> 
                        <div style="width: 100%;margin-left: 15px;"><h6 class="heading text-center">Net Stock Report</h6>
                        <table class="table border" id="" style="color: #000;">
                        {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th>Product</th>
                            <th style="width: 160px;">Purchase Quantity</th>
                            <th style="width: 160px;">Purchase Return</th>
                            <th style="width: 160px;">Sales Quantity</th>
                            <th style="width: 160px;">Sales Return</th>
                            <th style="width: 160px;">Available Quantity</th>

                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($manage_stock['stock_item'] as $stock_items)
                          <tr>
                            <td>1</td>
                            <td> {{ $stock_items['stock_items_name'] }}</td>
                            <td>{{ $stock_items['purchase_quantity'] }}</td>
                            <td>{{ $stock_items['return_pur_quantity'] }}</td>
                            <td>{{ $stock_items['sale_quantity'] }}</td>
                            <td>{{ $stock_items['return_sale_quantity']  }}</td>
                            <td>{{ $stock_items['available_quantity']  }}</td>
                          </tr>
                          @endforeach

                        </tbody>
                        </table>
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection