@foreach($sale as $sales)

  <div style="width:452px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
  <div><b>System Invoice No:</b> <span id="sys_invoice_no"> {{ $sales->system_invoice_no }} </span></div>
  <div><b>Invoice No:</b> <span id="ref_invoice_no"> {{ $sales->ref_invoice_no }} </span></div>
  <!-- <div><b>Category:</b> <span id="category_subcategory"> </span></div> -->
  <div><b>Payment Mode:</b> <span id="payment_mode"> {{ $sales->payment_mode }} </span></div>
  @if($sales->payment_mode == "Cheque")
  <div style="font-size: 14px;margin-left: 20px;" id="cheque_details">Bank Name: <span id="bank_name"> {{ $sales['bank']['bank_name'] }} </span> <br> Cheque No: <b>"<span id="cheque_no"> {{ $sales->cheque_no }} </span>"</b><br> Cheque Date: <span id="cheque_date"> {{ $sales->cheque_date }} </span> <br> Cheque Amount: <span id="cheque_amount"> ₹ {{ $sales->cheque_amount }} </span></div>
  @endif
  @if($sales['getStudent']->student_name != "")
  <div><b>Student name:</b> <span id="payment_mode"> {{ $sales['getStudent']->student_name }} ({{ $sales['getSiblingClass']->class_name }} - {{$sales['getSections']->section_name}})</span></div>
  @endif

  @if($sales['getStaff']->staff_name != "")
  <div><b>Staff name:</b> <span id="payment_mode"> {{ $sales['getStaff']->staff_name }} ({{ get_staff_role_name($sales['role_id']) }}) </span></div>
  @endif

  <!-- @if($sales['getSiblingClass']->class_name != "")
  <div><b>Class name:</b> <span id="payment_mode"> {{ $sales['getSiblingClass']->class_name }} </span></div>
  @endif

  @if($sales['getSections']->section_name != "")
  <div><b>Section name:</b> <span id="payment_mode"> {{ $sales['getSections']->section_name }} </span></div>
  @endif -->


 
  <div><b>Calculation Amount:</b> </div>
  <div style="font-size: 14px;margin-left: 20px;">Total Amount: <span id="payable_amount"> ₹ {{ $sales->total_amount }} </span> <br> Tax: <span id="tax"> {{ $sales->tax }}% </span> <br> Discount: <span id="discount"> {{ $sales->discount }}% </span><br> Net Amount: <span id="net_amount"> ₹ {{ $sales->net_amount }} </span></div>
  
  <!-- <div><b>Tax:</b> <span id="tax"> </span></div>
  <div><b>Discount:</b> <span id="discount"> </span></div>
  <div><b>Net Amount:</b> <span id="net_amount"> </span></div> -->
</div>



<!-- 
$("#vendor").html(obj.vendor.vendor_name);
$("#payment_mode").html(obj.);
$("#cheque_details").hide();
if(obj.payment_mode == "Cheque") {
    $("#cheque_details").show();
    $("#bank_name").html(obj.bank_id);
    $("#cheque_no").html(obj.cheque_no);
    $("#cheque_date").html(obj.cheque_date);
    $("#cheque_amount").html('₹ ' + obj.cheque_amount);
}
$("#payable_amount").html('₹ ' + obj.total_amount);
$("#tax").html(obj.tax + '%');
$("#discount").html(obj.discount + '%');
$("#net_amount").html('₹ ' + obj.net_amount);
 -->




@endforeach