@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.system_invoice_no') !!} :</lable>
    <div class="form-group">
      {!! Form::text('system_invoice_no',old('system_invoice_no',isset($sale['system_invoice_no']) ? $sale['system_invoice_no']: ''), ['class' => 'form-control', 'placeholder'=>trans('language.system_invoice_no'), 'id' => 'system_invoice_no', 'readonly' => 'true']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.ref_invoice_no') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::text('ref_invoice_no',old('ref_invoice_no',isset($sale['ref_invoice_no']) ? $sale['ref_invoice_no']: ''),['class' => 'form-control', 'placeholder'=>trans('language.ref_invoice_no'), 'id' => 'ref_invoice_no']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.invoice_date') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::Date('invoice_date',old('date',isset($sale['date']) ? $sale['date']: ''),['class' => 'form-control', 'placeholder'=>trans('language.invoice_date'), 'id' => 'invoice_date']) !!}
    </div>
  </div>
  
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.sale_type') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group m-bottom-0">
        <label class=" field select" style="width: 100%">
            {!!Form::select('sale_type', $sale['arr_sale_type'], isset($sale['sale_type']) ? $sale['sale_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'sale_type', 'onChange' => 'saletype(this.value)' ]) !!}
            <i class="arrow double"></i>
        </label>
    </div>
  </div>

  @if($sale['sale_type'] == 2 && $sale['sale_type'] != "") 
      <div class="row clearfix" id="show_staff">
  @else 
      <div class="row clearfix" style="display: none;" id="show_staff">
  @endif

    <div class="col-lg-6 col-md-6">
      <lable class="from_one1">{!! trans('language.staff_role') !!} <span class="red-text">*</span> :</lable>
      <div class="form-group m-bottom-0">
          <label class=" field select" style="width: 100%">
              {!!Form::select('role_id', $sale['arr_staff_roles'], isset($sale['role_id']) ? $sale['role_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'role_id']) !!}
              <i class="arrow double"></i>
          </label>
      </div>
    </div>

    <div class="col-lg-6 col-md-6">
      <lable class="from_one1">{!! trans('language.staff') !!} <span class="red-text">*</span> :</lable>
      <div class="form-group m-bottom-0">
          <label class=" field select" style="width: 100%">
              {!!Form::select('staff_id', $sale['arr_staff'], isset($sale['staff_id']) ? $sale['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id']) !!}
              <i class="arrow double"></i>
          </label>
      </div>
    </div>
  </div>


  @if($sale['sale_type'] == 1 && $sale['sale_type'] != "") 
    <div class="row clearfix" id="show_student">    
@else 
    <div class="row clearfix" style="display: none;" id="show_student">
@endif
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.class') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $sale['arr_class'], isset($sale['class_id']) ? $sale['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)' ]) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.section') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('section_id', $sale['arr_section'], isset($sale['section_id']) ? $sale['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'getStudent(this.value)' ]) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.student') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_id', $sale['arr_student'], isset($sale['student_id']) ? $sale['student_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id']) !!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
  </div>
  
  <div class="col-lg-3">
    <div class="form-group m-bottom-0">
      <lable class="from_one1">{!! trans('language.payment_mode') !!} <span class="red-text">*</span> :</lable>
      <label class=" field select" style="width: 100%">
        {!!Form::select('payment_mode',$sale['payment_mode_data'],isset($sale['payment_mode']) ? $sale['payment_mode'] :'',['class' => 'form-control show-tick select_form1 select2','id'=>'payment_mode','onChange'=>'show_cheque_details(this.value)'])!!}
        <i class="arrow double"></i>
      </label>
     </div>
  </div>
</div>

@if($sale['payment_mode'] == 2)
<div class="row clearfix" style="margin-top:10px;" id="ask_cheque_details">
  <div class="col-lg-3">
    <div class="form-group m-bottom-0">
    <lable class="from_one1">{!! trans('language.bank_name') !!} <span class="red-text">*</span> :</lable>
    <label class=" field select" style="width: 100%">
      {!! Form::select('bank_id',$sale['bank'],isset($sale['bank_id']) ? $sale['bank_id'] :'',['class' => 'form-control show-tick select_form1 select2', 'id' => 'bank_name']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_no') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::number('cheque_no',old('cheque_no',isset($sale['cheque_no']) ? $sale['cheque_no'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_no'), 'id' => 'cheque_no']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_date') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::date('cheque_date',old('cheque_date',isset($sale['cheque_date']) ? $sale['cheque_date'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_date'), 'id' => 'cheque_date']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_amount') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::number('cheque_amount',old('cheque_amount',isset($sale['cheque_amount']) ? $sale['cheque_amount'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_amount'), 'id' => 'cheque_amount']) !!}
    </div>
    
  </div>
</div>
@else
<div class="row clearfix" style="margin-top:10px;display: none;" id="ask_cheque_details">
  <div class="col-lg-3">
    <div class="form-group m-bottom-0">
    <lable class="from_one1">{!! trans('language.bank_name') !!} <span class="red-text">*</span> :</lable>
    <label class=" field select" style="width: 100%">
      {!! Form::select('bank_id',$sale['bank'],isset($sale['bank_id']) ? $sale['bank_id'] :'',['class' => 'form-control show-tick select_form1 select2', 'id' => 'bank_name']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_no') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::number('cheque_no',old('cheque_no',isset($sale['cheque_no']) ? $sale['cheque_no'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_no'), 'id' => 'cheque_no']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_date') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::date('cheque_date',old('cheque_date',isset($sale['cheque_date']) ? $sale['cheque_date'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_date'), 'id' => 'cheque_date']) !!}
    </div>
  </div>
  <div class="col-lg-3">
    <lable class="from_one1">{!! trans('language.cheque_amount') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::number('cheque_amount',old('cheque_amount',isset($sale['cheque_amount']) ? $sale['cheque_amount'] :''),['class' => 'form-control', 'placeholder'=>trans('language.cheque_amount'), 'id' => 'cheque_amount']) !!}
    </div>
    
  </div>
</div>
@endif

<!--  DataTable for view Records  -->
<div class="table-responsive">
<table class="table table-bordered m-b-0 c_list" id="" style="width:100%; margin-top: 10px;">
{{ csrf_field() }}
<thead>
  <tr>
    <th style="width: 180px" class="text-center">{!! trans('language.invoice_category') !!}</th>
    <th style="width: 180px" class="text-center">{!! trans('language.invoice_subcategory') !!}</th>
    <th style="width: 180px" class="text-center">Item Name</th>
    <th  class="text-center">Unit</th>
    <th style="width: 120px" class="text-center">Rate</th>
    <th style="width: 120px" class="text-center">Quantity</th>
    <th  class="text-center">Amount</th>
    <th class="text-center">Action</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>
      {!!Form::select('category_id',$sale['category'],isset($sale['category_id']) ? $sale['category_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'category_id','onChange'=>'getSubCategory(this.value)'])!!}
    </td>
    <td>
      {!!Form::select('sub_category_id',$sale['arr_subcategory_data'],isset($sale['subcategory_id']) ? $sale['subcategory_id'] : '',['class' => 'form-control show-tick select_form1 select2','id'=>'sub_category_id','onChange'=>'getItems(this.value)'])!!}
    </td>
    <td> 
      {!!Form::select('item_id',$sale['items'],$sale['items'],['class' => 'form-control show-tick select_form1 select2','id'=>'item_id', 'onchange' => 'showItemDetails(this.value)' ])!!}
      <br>Available: <label id="available_quantity"> </label>
    </td>
    <td class="text-center"> <div id="unit_name" class="unit_name"> </div><input type="hidden" name="unit_id" value="" id="unit_id" /></td>
    <td>
      {!!Form::number('rate','',['class' => 'form-control', 'placeholder'=>trans('language.rate'), 'id' => 'rate', 'onkeyup' => 'itemAmount()', 'step' => 0.1, 'min' => 0])!!}
    </td>
    <input type="hidden" name="" class="option_value" value="">
    <input type="hidden" name="" class="option_value_category" value="">
    <input type="hidden" name="" class="option_value_sub_category" value="">
    <input type="hidden" name="get_available_quantity" id="get_available_quantity" value="">
    <td>
      {!!Form::number('quantity','',['class' => 'form-control', 'placeholder'=>trans('language.quantity'), 'id' => 'quantity', 'onkeyup' => 'itemAmount()', 'step' => 1, 'min' => 0])!!}
      <div style="color: red; display: none;" id="msg_avbl_qty"> Please enter Quantity less than available </div>
    </td>
    <td class="text-right">
      {!!Form::text('amount','', ['class' => 'form-control', 'placeholder' => trans('language.amount'), 'id' => 'amount', 'readonly' => 'true'])!!}
    </td>
    @php 
    $counter = 0;
    if(isset($sale_item)){
      $counter = COUNT($sale_item);
    }
    @endphp 
    <td style="text-align: center;"><button type="button" class="btn btn-raised btn-primary custom_btn" id="save_return" onclick="addItemBlock()">Add</button></td>
  </tr>
</tbody>
</table>


<div class="table-responsive">
{!! Form::hidden('counter',isset($sale_item) ? COUNT($sale_item) : 0,['class' => 'gui-input', 'id' => 'counter']) !!}
<table class="table table-bordered m-b-0 c_list" id="show_table11" style="width:100%">
{{ csrf_field() }}
<tbody>
  @if(!empty($sale_item))
    @foreach($sale_item as $key => $value)
    @php $sale_items_id = "value[".$key."][sale_items_id]";  @endphp
   
    <tr id="row-{{$key}}">
      
      <td style="width: 290px;"> {{$value['category']['category_name']}} <input type="hidden" name="items[{{$key}}][category_id]" value="{{$sale_item[$key]['category_id']}}" class="rate">
      </td>
      <td style="width: 290px;"> {{$value['subCategory']['category_name']}} <input type="hidden" name="items[{{$key}}][sub_category_id]" value="{{$sale_item[$key]['sub_category_id']}}" class="rate">
      </td>
      <td style="width: 290px;">{{$value['items']['item_name']}} <input type="hidden" name="items[{{$key}}][item_id]" value="{{$sale_item[$key]['item_id']}}" ><input type="hidden" name="items[{{$key}}][unit_id]" value="{{$sale_item[$key]['unit_id']}}" >
         
         <input type="hidden" name="items[{{$key}}][sale_items_id]" id="p_item{{$key}}" class="p_item_id" value="{{$value['sale_items_id']}}">
      </td>
      <td class="text-center"> {{$value['rate']}} <input type="hidden" name="items[{{$key}}][rate]" value="{{$sale_item[$key]['rate']}}" class="rate">
      </td>
      <td class="text-center">{{$value['quantity']}} <input type="hidden" name="items[{{$key}}][quantity]" value="{{$sale_item[$key]['quantity']}}" class="quantity">
      </td> 
      <td class="text-center">{{$value['amount']}}<input type="hidden" name="items[{{$key}}][amount]" value="{{$sale_item[$key]['amount']}}" id= "item{{$key}}" class="total_amount">
      </td>
      <td class="text-center">
        <button type="button" rel={{$key}} class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete" onclick="removeItemBlock('{{$key}}')"><i class="zmdi zmdi-delete"></i>
        </button>
      </td>
    </tr>
    @endforeach
@endif
</tbody>
</table>
</div>
</div>
<div class="col-lg-6 float-right padding-0" style="width:100%;">
   <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
    <tbody>
    <tr>
      <td>sale Entry</td>
      <td> </td>
      <td class="text-right"><input type="hidden" class="totalAmount" value="{{$sale['total_amount']}}" name="total_amount" /><label id="lbltotal">{{$sale['total_amount']}} </label></td>
    </tr>
    <tr>
      <td class="col-lg-3 col-md-3">Tax(%)</td>
      <td>
        <input id="txttax" class="select2_single form-control tax" type="text" name="tax" value="{{$sale['tax']}}" onkeyup="calculateTotalSum(), getTaxValue()" tax = 'tax'>
        <div class="help-block" id="tax-error"> </div>
      </td>
      <td class="text-right"><input type="hidden" class="taxAmount" value="{{$sale['add_tax_amount']}}" /><label id="lbltax">{{$sale['tax_amount']}} </label></td>
    </tr>
    <tr>
      <td class="col-lg-3 col-md-3">Discount(%)</td>
      <td>
        <input id="txtdiscount" type="text" class="select2_single form-control discount" name="discount" value="{{$sale['discount']}}" onkeyup="calculateTotalSum(), getDiscountValue()" discount = 'discount'>
        <div class=help-block id="dis-error"> </div>
      </td>
      <td class="text-right"><input type="hidden" class="discount" /><label id="lbldiscount">{{$sale['discount_amount']}} </label></td>
    </tr>
    <tr>
      <td class="col-lg-3 col-md-3">Gross Amount</td>
      <td></td>

      <td class="text-right"><input type="hidden" name="gross_amount" class="gross_amount" value="{{$sale['gross_amount']}}"><label id="lblgross">{{$sale['gross_amount']}}</label></td>
    </tr>
    <tr>
      <td class="col-lg-3 col-md-3">Round off (+)</td>
      <td class="text-center"><br><input class="select2_single form-control roundoff" type="text" id="round_off" name="roundoff" value="{{$sale['round_off']}}" onkeyup="calculateTotalSum()">
        </td>
      <td> </td>
    </tr>
    <tr>
      <td class="col-md-3 col-xs-3 col-sm-3">Net Amount</td>
      <td></td>
      <td class="text-right"><input type="hidden" name="net_amount" class="net_amount" value="{{$sale['net_amount']}}"><label id="lblnet">{{$sale['net_amount']}} </label></td>
    </tr>
   </tbody>
 </table>
  <div class="row float-right" style="margin: 20px 0px;">
  <div class="col-lg-2">
    <button type="submit" class="btn btn-raised btn-primary" title="Save" id="save">Save
    </button>
  </div>
</div>
</div>

<style type="text/css">
  #show_student{
    width: 75%;
    margin-left: 0px;
    margin-right: 0px;
  }
  #show_staff{
    width: 50%;
    margin-left: 0px;
    margin-right: 0px;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
  });  
  jQuery(document).ready(function () {

      jQuery.validator.addMethod("lettersonly", function(value, element) {
          return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
      }, "Please use only alphanumeric values");

      jQuery.validator.addMethod("numbersonly", function(value, element) {
          return this.optional(element) || /^[0-9]+$/i.test(value);
      }, "Please use only number values");

      jQuery.validator.addMethod("validamount", function(value, element) {
          return this.optional(element) || /^[0-9]+[.,]?[0-9]+$/i.test(value);
      }, "Please enter valid amount");

      $("#sale-form").validate({

          /* @validation states + elements 
            ------------------------------------------- */

          errorClass: "state-error",
          validClass: "state-success",
          errorElement: "em",

          /* @validation rules 
            ------------------------------------------ */

          rules: {
              ref_invoice_no: {
                  required: true,
                  normalizer: function(value) {
                      return $.trim(value);
                    }
              },
              invoice_date: {
                required: true,
                date:true,
                maxlength:10
              },
              invoice_vendor: {
                required: true
              },
              // category_id: {
              //   required: true
              // },
              // sub_category_id: {
              //   required: true
              // },
              payment_mode: {
                required: true
              },
              bank_id: {
                required: true
              },
              cheque_no: {
                required: true,
                numbersonly: true
              },
              cheque_date: {
                required: true,
                date:true,
                maxlength:10
              },
              cheque_amount: {
                required: true,
                validamount: true
              },
              tax: {
                required:true,
                min:0,
                max:100
              },
              discount: {
                required:true,
                min:0,
                max:100
              },
              sale_type: {
                  required: true
              },
              section_id: {
                  required: true
              },
              student_id: {
                  required: true
              },
              staff_id: {
                  required: true
              },
              role_id: {
                  required: true
              },
              class_id: {
                  required: true
              },
              roundoff: {
                required:true,
                min:0
              }
          },

          /* @validation highlighting + error placement  
            ---------------------------------------------------- */
          highlight: function (element, errorClass, validClass) {
              $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
              $(element).closest('.field').addClass(errorClass).removeClass(validClass);
          },
          unhighlight: function (element, errorClass, validClass) {
              $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
              $(element).closest('.field').removeClass(errorClass).addClass(validClass);
          },

          errorPlacement: function (error, element) {
              if (element.is(":radio") || element.is(":checkbox")) {
                  element.closest('.option-group').after(error);
              } else {
                  element.closest('.form-group').after(error);
                  // error.insertAfter(element.parent());
              }
          }
      });

  });

function addItemBlock() {
  var counter = parseInt($('#counter').val());
  $('#counter').val(counter+1);
  var arr_key = counter+1;
  var opval = $(".option_value").val();
  var opvalcat = $(".option_value_category").val();
  var opvalsbcat = $(".option_value_sub_category").val();
  var unitval = $("#unit_id").val();
  var catval = $("#category_id").val();
  var subcatval = $("#sub_category_id").val();
  var item = $("#item_id").val(); 
  var rate = $("#rate").val();
  var quantity = $("#quantity").val();
  var amount = rate*quantity;
  var item_amount = amount.toFixed(2);
  if(item != "" && unitval!= "" && rate!= "" && quantity!= "")  {
    $("#show_table11").append('<tr id="row-'+arr_key+'"><td style="width: 290px;">'+opvalcat+' <input type="hidden" name="items['+arr_key+'][category_id]" value="'+catval+'" ></td><td style="width: 290px;">'+opvalsbcat+' <input type="hidden" name="items['+arr_key+'][sub_category_id]" value="'+subcatval+'" ></td><td style="width: 290px;">'+opval+' <input type="hidden" name="items['+arr_key+'][item_id]" value="'+item+'" ><input type="hidden" name="items['+arr_key+'][unit_id]" value="'+unitval+'" ></td><td class="text-center"> '+rate+' <input type="hidden" name="items['+arr_key+'][rate]" value="'+rate+'" class="rate"></td><td class="text-center">'+quantity+' <input type="hidden" name="items['+arr_key+'][quantity]" value="'+quantity+'" class="quantity"></td> <td class="text-center amount">'+item_amount+' <input type="hidden" name="items['+arr_key+'][amount]" value="'+item_amount+'" id= "item'+arr_key+'" class="total_amount"></td><td class="text-center"><button type="button" class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete" onclick="removeItemBlock('+arr_key+')"><i class="zmdi zmdi-delete"></i></button></td></tr>');
  }
  $("#item_id").val("").trigger('change');
  $("#category_id").val("").trigger('change');
  $("#sub_category_id").val("").trigger('change');
  $("#unit_name").text(''); 
  $("#num").text('');
  $("#rate").val('');
  $("#quantity").val('');
  $("#amount").val('');
  $("#available_quantity").text('');  
  calculateTotalSum()
  }
function show_cheque_details(payment_mode) {
    if(payment_mode == 2)
    {
      $('#ask_cheque_details').show();
    }
    else
    {
      $('#ask_cheque_details').hide();
    }
}

function showItemDetails(item_id) {
  if(item_id != "") {
    var optionval = $("#item_id option:selected").text();
    $(".option_value").val(optionval);
    $(".mycustloading").show();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{url('admin-panel/inventory/get-unit-data')}}",
        type: 'GET',
        data: {
            'item_id': item_id
        },
        success: function (data) {
          data = JSON.parse(data);
            $("#unit_name").html(data.unit_name);
            $("#unit_id").val(data.unit_id);
            $("#available_quantity").html(data.available_quantity);
            $("#get_available_quantity").val(data.available_quantity);
            $(".mycustloading").hide();
        }
    });
  }
}

function getSubCategory(category_id)
{
  if(category_id != "") {
    var optionval = $("#category_id option:selected").text();
    $(".option_value_category").val(optionval);
      $(".mycustloading").show();
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('admin-panel/inventory/get-subcategory-data')}}",
          type: 'GET',
          data: {
              'category_id': category_id
          },
          success: function (data) {
              $("select[name='sub_category_id'").html(data.options);
              $(".mycustloading").hide();
          }
      });
  } else {
      $("select[name='sub_category_id'").html('<option value="">Select Sub-Category</option>');
      $(".mycustloading").hide();
  }
}

function getItems(sub_category_id)
{
  if(sub_category_id != "") {
    var optionval = $("#sub_category_id option:selected").text();
    $(".option_value_sub_category").val(optionval);
      $(".mycustloading").show();
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('admin-panel/inventory/get-items-data')}}",
          type: 'GET',
          data: {
              'sub_category_id': sub_category_id
          },
          success: function (data) {
              $("select[name='item_id'").html(data.options);
              $(".mycustloading").hide();
          }
      });
  } else {
      $("select[name='item_id'").html('<option value="">Select Items</option>');
      $(".mycustloading").hide();
  }
}

function calculateTotalSum()
{

  $(".mycustloading").show();
  var total_sum = 0;
  $('.total_amount').each(function(e){
    var value = parseFloat($(this).val()); 
     total_sum = parseFloat(total_sum) + value; 
  });
  $('.totalAmount').val(total_sum);
  $('#lbltotal').text(total_sum);
  // $('.gross_amount').val(total_sum);
  // $('#lblgross').text(total_sum);
  // $('.net_amount').val(total_sum);
  // $('#lblnet').text(total_sum);
  var tax = $(".tax").val();
  
  if(tax == ""){
    var tax = 0;
    $("#lbltax").text('0.00');
  }
  
  // if(tax > 0) {
  //     if(tax != "" && tax < 100) {
        var total_amount = $(".totalAmount").val();
        var tax_amount = (parseFloat(total_amount)*parseFloat(tax))/100;
        $('#lbltax').text(tax_amount.toFixed(2));
        var re_tax_amount = parseFloat(total_amount)+parseFloat(tax_amount);
        console.log('total_amount');
        console.log('tax_amount');
        console.log('re_tax_amount');
        $(".taxAmount").val(re_tax_amount.toFixed(2));
        $("#lblgross").text(re_tax_amount);
      $(".gross_amount").val(re_tax_amount);
      $("#lblnet").text(re_tax_amount);
      $(".net_amount").val(re_tax_amount);
  //     }
  // }
  var discount = $(".discount").val();
  if(discount == ""){
    var discount = 0;
    $("#lbldiscount").text('0.00');
  }
  // if(discount > 0) {
  //   if(discount != "") {
      var add_tax_amount = $(".taxAmount").val();
      var discount_amount = (parseFloat(add_tax_amount)*parseFloat(discount))/100;
      $("#lbldiscount").text(discount_amount.toFixed(2));
      var d_re_discount_amount = parseFloat(add_tax_amount)-parseFloat(discount_amount);
      var re_discount_amount = d_re_discount_amount.toFixed(2);
      $("#lblgross").text(re_discount_amount);
      $(".gross_amount").val(re_discount_amount);
      $("#lblnet").text(re_discount_amount);
      $(".net_amount").val(re_discount_amount);
  //   }
  // }
  var roundoff = $(".roundoff").val();
  if(roundoff == ""){
    var roundoff = 0;
    $("#round_off").text('0.00');
  }
  // if(roundoff > 0) {
  //   if(roundoff != "") {
      var gross_amount = $("#lblgross").text();
      //var roundoff = $(".roundoff").val();
      var net_amount = parseFloat(gross_amount) + parseFloat(roundoff);
      $("#lblnet").text(net_amount.toFixed(2));
      $(".net_amount").val(net_amount.toFixed(2));
  //   }
  // }
  $(".mycustloading").hide();
}
function removeItemBlock(id) {
  var p_item_id = $('#p_item'+ id).val();
  $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('admin-panel/inventory/delete-sale-item-data')}}",
          type: 'GET',
          data: {
              'p_item_id': p_item_id
          },
          success: function (data) {
              // $("select[name='item_id'").html(data.options);
              $("#row-"+id).remove();
              var counter = parseInt($('#counter').val());
              $('#counter').val(counter-1);
              calculateTotalSum()
              $(".mycustloading").hide(); 
          }
      });
  // alert($("#p_item"+ id).val());
  
}
function itemAmount() {
  var rate = $("#rate").val();
  var get_available_quantity = $("#get_available_quantity").val();
  var quantity = $("#quantity").val();

  if(parseInt(quantity) > parseInt(get_available_quantity)){
    $("#msg_avbl_qty").show();
    $("#save_return").attr("disabled", "disabled");
    $("#save").attr("disabled", "disabled");
    return false;
  } else {
    $("#msg_avbl_qty").hide();
    $("#save_return").removeAttr("disabled");
    $("#save").removeAttr("disabled");
  }

  var amount = rate*quantity;
  $('#amount').val(amount.toFixed(2));
}

function getTaxValue() {
  var get_taxvalue = $('#txttax').val();
  if(get_taxvalue < 0 || get_taxvalue > 100) {
      $errors = 'please enter correct tax!!';
      $('#tax-error').html($errors);
      $('#save').attr("disabled", "disabled");
      
    } else {
      $('#save').removeAttr("disabled");
      $('#tax-error').html('');
    }
}

function getDiscountValue() {
  var get_disvalue = $('#txtdiscount').val();
  if(get_disvalue < 0 || get_disvalue > 100) {
      $dis_errors = 'please enter correct discount!!';
      $('#dis-error').html($dis_errors);
      $('#save').attr("disabled", "disabled");
    } else {
      $('#save').removeAttr("disabled");
      $('#dis-error').html('');
    }
}



function saletype(saletype)
{
    if(saletype == 1){
        $("#show_student").show();
    } else {
        $("#show_student").hide();
    }

    if(saletype == 2){
        $("#show_staff").show();
    } else {
        $("#show_staff").hide();
    }    
}

function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }


    function getStudent(section_id)
    {
        var class_id = $("#class_id").val();
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/task-manager/get-student-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id': section_id
                },
                success: function (data) {
                    $("select[name='student_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='student_id'").html('<option value="">Select Student</option>');
            $(".mycustloading").hide();
        }
    }
</script>
   