@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Item Details</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item "><a href="{!! URL::to('admin-panel/inventory/sale/view-sale') !!}">{!! trans('language.view_sale') !!}</a></li>
          <li class="breadcrumb-item active">{!! trans('language.view_item_details') !!}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active">
            <div class="card">
              <div class="body form-gap">
                @if(session()->has('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session()->get('success') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                @endif  
                
                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                    <div class="row clearfix">
                      {!!Form::hidden('sale_id', $sale_id)!!}
                      <div class="col-lg-3">
                        <div class="form-group">
                          {!!Form::select('s_item_name',$sale['all_items'],'',['class' => 'form-control show-tick select_form1 select2','id'=>'s_item_name'])!!}
                        </div>
                      </div>
                    
                      <div class="col-lg-1">
                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                      </div>
                      <div class="col-lg-1">
                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                  <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;margin-bottom: 10px;">
                <div><b>System Invoice No:</b> {{$sale[0]['system_invoice_no']}}</div>
                <div><b>Ref. Invoice No:</b> {{$sale[0]['ref_invoice_no']}}</div>
                <div><b>Tax:</b> {{$sale[0]['tax']}}%</div>
                <div><b>Discount:</b> {{$sale[0]['discount']}}%</div>
                <div><b>Net Amount:</b> ₹ {{$sale[0]['net_amount']}}</div>

                @if($sale[0]['getStudent']->student_name != "")
                <div><b>Student name:</b> <span id="payment_mode"> {{ $sale[0]['getStudent']->student_name }} ({{ $sale[0]['getSiblingClass']['class_name'] }} - {{$sale[0]['getSections']['section_name']}}) </span></div>
                @endif

                @if($sale[0]['getStaff']->staff_name != "")
                <div><b>Staff name:</b> <span id="payment_mode"> {{ $sale[0]['getStaff']->staff_name }} ({{ get_staff_role_name($sale[0]['role_id']) }}) </span></div>
                @endif

                <!-- @if($sale[0]['getSiblingClass']->class_name != "")
                <div><b>Class name:</b> <span id="payment_mode"> {{ $sale[0]['getSiblingClass']->class_name }} </span></div>
                @endif

                @if($sale[0]['getSections']->section_name != "")
                <div><b>Section name:</b> <span id="payment_mode"> {{ $sale[0]['getSections']->section_name }} </span></div>
                @endif -->
              </div>

                  <!--  DataTable for view Records  -->
                  <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="sale-item-table" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>{{trans('language.invoice_s_no')}}</th>
                        <th>{{trans('language.s_item_name')}}</th>
                        <th>{{trans('language.unit')}}</th>
                        <th>{{trans('language.category_name')}}</th>
                        <th>{{trans('language.item_sub_category_name')}}</th>
                        <th>{{trans('language.rate')}}</th>
                        <th>{{trans('language.quantity')}}</th>
                        <th>{{trans('language.amount')}}</th>
                        <th>{{trans('language.sale_type')}}</th>

                        <!-- <th>Action</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
     $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }); 

    $(document).ready(function () {
        var table = $('#sale-item-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            ajax: {
                url: '{{url('admin-panel/inventory/sale/view-sale/item-details-data')}}',
                data: function (d) {
                    d.sale_id     = $('input[name=sale_id]').val();
                    d.s_item_name     = $('select[name=s_item_name]').val();
                    // d.s_unit_name     = $('select[name="s_unit_name"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'item_name', name: 'item_name'},
                {data: 'unit_name', name: 'unit_name'},
                {data: 'category_name', name: 'category_name'},
                {data: 'sub_category_name', name: 'sub_category_name'},
                {data: 'rate', name: 'rate'},
                {data: 'quantity', name: 'quantity'},
                {data: 'amount', name: 'amount'},
                {data: 'sale_type', name: 'sale_type'}
                // {data: 'action', name: 'action'}
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                // {
                //     "targets": 6, // your case first column
                //     "width": "10%"
                // },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });
</script>

@endsection