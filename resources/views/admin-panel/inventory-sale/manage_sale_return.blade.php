@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_sale_return') !!}</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-sale-return') !!}"> {!! trans('language.manage_sale_return') !!} </a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">

                @if(session()->has('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session()->get('success') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
              @if($errors->any())
                  <div class="alert alert-danger" role="alert">
                      {{$errors->first()}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
                <div class="row">
                  <!-- <div class="col-lg-3">
                    <lable class="from_one1">{!! trans('language.system_invoice_no') !!} :</lable>
                    <div class="form-group">
                      {!! Form::text('system_invoice_no',old('system_invoice_no',isset($sale['system_invoice_no']) ? $sale['system_invoice_no']: ''), ['class' => 'form-control', 'placeholder'=>trans('language.system_invoice_no'), 'id' => 'system_invoice_no', 'readonly' => 'true']) !!}
                    </div>
                  </div> -->
                  
                  <div class="col-lg-3">
                    <div class="form-group">
                      <lable class="from_one1">Invoice No: </lable>
                      <input type="text" name="invoice_no" value="" class="form-control" placeholder="Invoice No" id="invoice_no" onKeyup="checkSearch()">
                    </div>
                  </div>
                  <!-- <div class="col-lg-3">
                    <div class="form-group">
                      <lable class="from_one1">Date: </lable>
                      <input type="date" name="date" id="date" class="form-control" placeholder="Date">
                    </div>
                  </div>
                 
                  <div class="col-lg-3">
                    <div class="form-group m-bottom-0">
                      <lable class="from_one1">{!! trans('language.invoice_vendor') !!} :</lable>

                      <label class=" field select" style="width: 100%">
                        {!!Form::select('vendor_id',$sale['vendor'],'',['class' => 'form-control show-tick select_form1 select2','id'=>'vendor_id' ])!!}
                        <i class="arrow double"></i>
                      </label>
                     </div>
                  </div> -->

                  <div class="col-lg-1 padding-5">
                    <lable class="from_one1"></lable>
                      {!! Form::button('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'onclick'=>'showTable()', 'disabled']) !!}
                  </div>
                  <div class="col-lg-1 padding-5">
                    <lable class="from_one1"></lable>
                      {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                  </div>
                </div>
                  
                {!! Form::open(['files'=>TRUE,'id' => 'sale-return-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                
                  <div id = "show_sale_data"></div> 
                
                {!! Form::close() !!}  
                     
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->

<script type="text/javascript">

  $(document).ready(function() {
    //$('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    $('.select2').select2();

    $('#clearBtn').click(function(){
        location.reload();
    })
  });


  function showTable() {

    var invoice_no  = $("#invoice_no").val();
    var date        = $("#date").val();
    var vendor_id   = $("#vendor_id").val();

    $(".mycustloading").show();
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: "{{url('admin-panel/inventory/get-sale-return')}}",
      type: 'GET',
      data: {
          'invoice_no': invoice_no,
          'date'      : date,
          'vendor_id' : vendor_id
      },
      success: function (data) {
          $("#show_sale_data").html(data);
          $(".mycustloading").hide();   
      }
    });
  }

    function showAmount(id) {
      
      var quantity            = $("#quantity_"+id).val();
      var return_quantity     = $("#return_quantity_"+id).val();
      var available_quantity  = $("#available_quantity_"+id).val();
      
      if(parseInt(return_quantity) > parseInt(quantity)){
        $("#msg_inv_qty_"+id).show();
        $("#save_return").attr("disabled", "disabled");
        return false;
      } else {
        $("#msg_inv_qty_"+id).hide();
        $("#save_return").removeAttr("disabled");
      } 

      if(parseInt(return_quantity) > parseInt(available_quantity)){
        $("#msg_avbl_qty_"+id).show();
        $("#save_return").attr("disabled", "disabled");
        return false;
      } else {
        $("#msg_avbl_qty_"+id).hide();
        $("#save_return").removeAttr("disabled");
      }

      var total_amountGet    = $("#total_amount").val();
      var lessAmount         = $("#return_amount_"+id).val();
      var total_amount_get   = total_amountGet - lessAmount;
      var rate               = $("#rate_"+id).val();
      var amount             = parseInt(return_quantity) * parseInt(rate);
      var return_amount      = $("#return_amount_"+id).val(amount);
      var total_amount       = parseInt(total_amount_get)+parseInt(amount);

      $("#amount_"+id).html(amount);
      $("#total_amount").val(total_amount);
      $("#totalAmount").val('Total : '+ total_amount);
    
    }

     function checkSearch(){
        var invoice_no = $("#invoice_no").val();  
        if(invoice_no == ""){
            $(':input[name="Search"]').prop('disabled', true);
        } else {
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }

</script>
@endsection