@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.invoice_prefix') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      {!! Form::text('invoice_prefix',old('invoice_prefix',isset($configuration['invoice_prefix']) ? $configuration['invoice_prefix']: ''), ['class' => 'form-control', 'placeholder'=>trans('language.invoice_prefix'), 'id' => 'invoice_prefix']) !!}
    </div>
  </div>
  @if ($errors->has('invoice_prefix')) <p class="help-block">{{ $errors->first('invoice_prefix') }}</p> @endif
  <div class="col-lg-4 col-md-4">
    <lable class="from_one1">{!! trans('language.invoice_date') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group">
      <div class="checkbox" style="margin-top:11px;">
        @php $day = $configuration['day'] == 1 ? 'checked' : '';
             $month = $configuration['month'] == 1 ? 'checked' : '';
             $year = $configuration['year'] == 1 ? 'checked' : '';
        @endphp
        <input type="checkbox" id="checkbox5" name="day" value="1" {{$day}}> 
        <label for="checkbox5" style="margin-right: 28px;">D(Day)</label>

        <input type="checkbox" id="checkbox6" name="month" value="1" {{$month}}> 
        <label for="checkbox6" style="margin-right: 28px;">M(Month)</label>

        <input type="checkbox" id="checkbox7" name="year" value="1" {{$year}}>
        <label for="checkbox7" style="margin-right: 28px;">Y(Year)</label>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.invoice_type') !!} <span class="red-text">*</span> :</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select" style="width: 100%">
        {!!Form::select('invoice_type', $configuration['arr_invoice_type'],isset($configuration['invoice_type']) ? $configuration['invoice_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'invoice_type', 'disabled'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
</div>
<div class="row clearfix">
  <div class="col-lg-3 col-md-3">
    <br />
    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
    <a href="{!! url('admin-panel/inventory/manage-invoice-configuration') !!}" class="btn btn-raised" >Cancel</a>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_invoice_config").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                invoice_prefix: {
                    required: true
                    },
                invoice_type: {
                    required:true
                    }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>