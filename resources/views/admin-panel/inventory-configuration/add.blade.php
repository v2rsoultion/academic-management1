@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
   <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_invoice_config') !!}</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-invoice-configuration') !!}">{!! trans('language.manage_invoice_config') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        @if(Request::segment(4) != '')
          <div class="card gap-m-bottom">
            <div class="header">
              <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
            </div>
            <div class="body form-gap">
              {!! Form::open(['files'=>TRUE,'id' => 'manage_invoice_config' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                @include('admin-panel.inventory-configuration._form',['submit_button' => $submit_button])
              {!! Form::close() !!}
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <!-- <div class="headingcommon col-lg-12" style="margin-left: -15px;">Search By :-</div> -->
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
      {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
      <div class="row">
        <div class="col-lg-3 col-md-3">
          <!-- <lable class="from_one1">{!! trans('language.invoice_type') !!} <span class="red-text">*</span> :</lable> -->
          <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
              {!!Form::select('s_invoice_type', $configuration['arr_invoice_type'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_invoice_type'])!!}
              <i class="arrow double"></i>
            </label>
          </div>
        </div>
        
        <div class="col-lg-1">
          {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
        </div>
          <div class="col-lg-1">
            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary','name'=>'Clear', 'id' => "clearBtn"]) !!}
        </div>
      </div>
      {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="invoice-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.invoice_s_no') !!}</th>
              <th>{!! trans('language.invoice_prefix') !!}</th>
              <th>{!! trans('language.invoice_type') !!}</th>
              <th>{!! trans('language.invoice_format') !!}</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#invoice-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/inventory/manage-invoice-configuration-view')}}",
              data: function (f) {
                  f.s_invoice_type = $('#s_invoice_type').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'invoice_prefix', name: 'invoice_prefix'},
            {data: 'invoice_type', name: 'invoice_type'},
            {data: 'invoice_format', name: 'invoice_format'},
            {data: 'action', name: 'action'}
        ],
        columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "5%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
        ] 
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
</script>
@endsection