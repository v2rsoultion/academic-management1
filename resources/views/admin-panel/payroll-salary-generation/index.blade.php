@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <a href="{!! url('admin-panel/payroll/salary-generation') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-generation') !!}">{!! $page_title !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="staff-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.staff_name')}}</th>
                                            <th>{{trans('language.leave_date_range')}}</th>
                                            <th>{{trans('language.class_name')}}</th>
                                            <th>{{trans('language.class_wise_status')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/substitute-management/data')}}",
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'date_range', name: 'date_range'},
                {data: 'class_name', name: 'class_name'},
                {data: 'class_wise', name: 'class_wise_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "25%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




