@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row">
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.main_head') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('acc_sub_head_id', $open_balance['arr_heads'], isset($open_balance['acc_sub_head_id']) ? $open_balance['acc_sub_head_id']: '', ['class' => 'form-control show-tick select_form1 select2','id'=>'acc_sub_head_id', 'onChange'=>'getGroup(this.value)'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.group_name') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('acc_group_head_id',$open_balance['arr_groups'], isset($open_balance['acc_group_head_id']) ? $open_balance['acc_group_head_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'acc_group_head_id'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.balance_amount') !!}</lable>
      {!! Form::text('balance_amount',isset($open_balance->balance_amount) ? $open_balance->balance_amount : '',['class' => 'form-control','placeholder'=> trans('language.balance_amount'), 'id' => 'balance_amount']) !!}
    </div>
  </div>

  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.balance_date') !!}</lable>
      {!! Form::date('balance_date',isset($open_balance->balance_date) ? $open_balance->balance_date : '',['class' => 'form-control','placeholder'=> trans('language.balance_date'), 'id' => 'balance_date']) !!}
    </div>
  </div>
</div>



<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/account/manage-opening-balance') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("digitsonly", function(value, element) {
            return this.optional(element) || /^[1-9][0-9]*$/i.test(value);
        }, "Please use only integer values");

        $("#manage_acc_group").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                acc_sub_head_id: {
                    required: true
                },
                acc_group_head_id: {
                    required:true
                },
                balance_amount: {
                    required:true,
                    digits: true,
                    digitsonly: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                },
                balance_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });


    function getGroup(acc_sub_head_id)
    {
        if(acc_sub_head_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/account/acc-group-heads')}}",
                type: 'GET',
                data: {
                    'acc_sub_head_id': acc_sub_head_id
                },
                success: function (data) {
                    $("select[name='acc_group_head_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='acc_group_head_id'").html('<option value="">Select Group</option>');
            $(".mycustloading").hide();
        }
    }

</script>