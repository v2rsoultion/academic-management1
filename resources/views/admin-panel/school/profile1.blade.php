@extends('admin-panel.layout.header')

@section('content')

<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! $school['school_name']; !!}
                <small>Welcome to {!! $school['school_name']; !!}</small>
                </h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12"> 
                <a href="{!! URL::to('admin-panel/school/add-school/'.$login_info['encrypted_school_id']) !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-edit" style="margin-top: 0px !important;"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">{!! $school['school_name']; !!}</li>
                </ul>                
            </div>
        </div>
    </div>    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card profile-header">
                    <div class="body text-center">
                        @if(!empty($school['logo']))
                        <div class="profile-image"> <img src="{!! URL::to($school['logo']) !!}" alt=""> </div>
                        @else
                        <div class="profile-image"> <img src="{!! URL::to('public/assets/images/dummyy_profile.png') !!}" alt=""> </div>
                        @endif
                        <div>
                            <h4 class="m-b-0"><strong>{!! $school['school_name']; !!}</strong> </h4>
                            <span class="job_post">{!! $login_info['admin_role']; !!}</span>
                            <p>{!! $school['school_address']; !!}</p>
                        </div>
                        
                    </div>                    
                </div>                               
                <div class="card form-gap">
                    <div class="body">
                        <div class=" workingtime">
                            <h6>Session</h6>
                            @php
                            $updated_session    = get_current_session();
                            $updated_session_id = !empty($updated_session) ? $updated_session['session_id'] : null; 
                            
                            @endphp
                            <div class="row">
                                <label class="field select col-md-12">
                                    {!!Form::select('session_id',get_arr_session(),isset($updated_session_id) ? $updated_session_id : null, ['class' => 'form-control show-tick select_form1','id'=>'set_dashboard_session'])!!}
                                    <i class="arrow double"></i>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>  
                       
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card form-gap">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#basic">Basic Info</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#branch">Branch Info</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#address">Address Info</a></li>                        
                    </ul>
                    <div class="tab-content">

                        <!--  Basic Info Tab -->
                        <div class="tab-pane body active" id="basic">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_registration_no') !!}:</strong> {!! $school['school_registration_no'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_sno_numbers') !!}:</strong> {!! $school['school_sno_numbers'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_board_of_exams') !!}:</strong> {!! $school['school_board_of_exams'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_medium') !!}:</strong> {!! $school['school_medium'] !!} </p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_total_students') !!}:</strong> {!! $school['school_total_students']." Students" !!}:</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_total_staff') !!}:</strong> {!! $school['school_total_staff']." Staff" !!}:</p></li>
                            </ul>
                            <hr>
                            <p> {!! $school['school_description'] !!} </p>                                                        
                        </div>

                        <!-- Branch Info Tab -->
                        <div class="tab-pane body" id="branch">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_board_of_exams') !!}:</strong> {!! $school['boards'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_class_from') !!}:</strong> {!! $school['school_class_from']." Class" !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_class_to') !!}:</strong> {!! $school['school_class_to']." Class" !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_fee_range_from') !!}:</strong> {!! "Rs ".$school['school_fee_range_from'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_fee_range_to') !!}:</strong> {!! "Rs ".$school['school_fee_range_to'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_facilities') !!}:</strong> {!! $school['school_facilities'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_url') !!}:</strong> 
                                @if($school['school_url'] != "")
                                    <a href="{!! URL::to($school['school_url']) !!}" title="" target="_blank"> {!! trans('language.school_url') !!} </a>
                                @else 
                                    <a href="#" title="" target="_blank"> {!! trans('language.school_url') !!} </a>
                                @endif
                                </p></li>

                            </ul>
                        </div> 

                        <!-- Address Info Tab -->
                        <div class="tab-pane body" id="address">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_address') !!}:</strong> {!! $school['school_address'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.country') !!}:</strong> {!! $school['country_name'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.state') !!}:</strong> {!! $school['state_name'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.city') !!}:</strong> {!! $school['city_name'] !!}</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong>{!! trans('language.school_pincode') !!}:</strong> {!! $school['school_pincode'] !!}</p></li>

                            </ul>
                        </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card form-gap">
                            <div class="header">
                                <h2><strong>Recent</strong> Activity</h2>
                                <ul class="header-dropdown">
                                    <li class="remove">
                                        <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="body user_activity">
                                <div class="streamline b-accent">
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">Admin Birthday</h5>
                                            <small>Jan 21 <a href="javascript:void(0);" class="text-info">Sophia</a>.</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">Add New Contact</h5>
                                            <small>30min ago <a href="javascript:void(0);">Alexander</a>.</small>
                                            <small><strong>P:</strong> +264-625-2323</small>
                                            <small><strong>E:</strong> maryamamiri@gmail.com</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar6.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">General Surgery</h5>
                                            <small>Today <a href="javascript:void(0);">Grayson</a>.</small>
                                            <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar7.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">General Surgery</h5>
                                            <small>45min ago <a href="javascript:void(0);" class="text-info">Fidel Tonn</a>.</small>
                                            <small><strong>P:</strong> +264-625-2323</small>
                                            <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                                        </div>
                                    </div>                        
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>                              
            </div>
        </div>        
    </div>
</section>

<script>
    $(document).ready(function () {
        $("#set_dashboard_session").on('change', function (e)
        {
            var session_id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/session/set-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                success: function (response) {
                    location.reload();
                    alert(response.message);
                }
            });
        });
    });
</script>
@endsection

