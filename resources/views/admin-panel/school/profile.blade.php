@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nav_item7
    {
    min-height: 230px !important;
    }
    .imgclass:hover .active123 .tab_images123{
    left: 60px !important;
	}
	.tab_images123{
		left: 60px !important;
	}
    .imgclass:hover .tab_images123{
    left: 60px !important;
    }
    .nav_item3 {
    min-height: 255px !important;
    }
    .side-gap{
    width: 75% !important;
    }
    .tab1{
        padding-left: 5px !important;
    }
    .tab2{
        padding-right: 3px !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>View Profile</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 line">
                <a href="{!! URL::to('admin-panel/school/add-school/'.$login_info['encrypted_school_id']) !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-edit" style="margin-top: 0px !important;"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/configuration') !!}"> School</a></li>
                    <li class="breadcrumb-item active">{!! $school['school_name']; !!}</li>
                </ul>       
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card profile_card1">
                    <div class="body text-center pro_body_center1 form-gap ">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 text-left">
								@php $school_logo = 'public/images/default.png'; @endphp
								@if($school['logo'] != '')
									@php $school_logo = $school['logo']; @endphp
								@endif
                                <div class="profile-image"> <img src="{!! URL::to($school_logo) !!}" alt="" class="profile_image"> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std1">
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_name') !!}: </span> {!! $school['school_name']; !!}</label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_registration_no') !!}: </span> {!! $school['school_registration_no']; !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_sno_numbers') !!}: </span>{!! $school['school_sno_numbers']; !!}</label>
                                    </div>
                                    @php
                                    $updated_session    = get_current_session();
                                    $updated_session_id = !empty($updated_session) ? $updated_session['session_id'] : null; 
                                    
                                    @endphp
                                    <div class="profile_detail_3">
                                        <label> <span>Session: </span> 
                                        {!!Form::select('session_id',get_arr_session(),isset($updated_session_id) ? $updated_session_id : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'set_dashboard_session'])!!}
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std2">
									
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_email') !!}: </span> {!! $school['school_email']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_mobile_number') !!}: </span> {!! $school['school_mobile_number']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_telephone') !!}: </span> {!! $school['school_telephone']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.school_fax_number') !!}: </span> {!! $school['school_fax_number']; !!} </label>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="pull-left" style="width: 24%; margin-left: 2%">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card ">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#branch_info" onClick="toggleBlock('branch_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/personal-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Branch Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
								<li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#address_info" onClick="toggleBlock('address_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/address-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Address Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#images" onClick="toggleBlock('images');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Images </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                
                                
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right" style="width: 72%;">
                <div class="">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="branch_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Branch Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_medium') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_medium']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_board_of_exams') !!} :</b> <span class="profile_detail_span_6">{!! $school['boards']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">Class :</b> <span class="profile_detail_span_6">{!! $school['school_class_from'].' - '.$school['school_class_to'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">Fees Range :</b> <span class="profile_detail_span_6">Rs. {!! $school['school_fee_range_from'].' - Rs. '.$school['school_fee_range_to'] !!}</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_total_students') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_total_students']; !!}</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_total_staff') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_total_staff']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_facilities') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_facilities']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_url') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_url']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_lat_long') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_latitude']; !!} & {!! $school['school_longitude']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="address_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Address Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.school_address') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_address'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.city_name') !!} :</b> <span class="profile_detail_span_6">{!! $school['city_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.state_name') !!} :</b> <span class="profile_detail_span_6">{!! $school['state_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.country_name') !!} :</b> <span class="profile_detail_span_6">{!! $school['country_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.school_pincode') !!} :</b> <span class="profile_detail_span_6">{!! $school['school_pincode']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="images" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="">
                                                    <div class="card border_info_tabs_1 card_top_border1">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="card">
                                                                    <div class="body">
                                                                        <div class="nav_item3">
                                                                            <div class="nav_item1">
                                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> School Images </a>
                                                                            </div>
                                                                            <br />
                                                                            <div class="nav_item2">
                                                                                <div role="tabpanel" class="tab-pane in active" id="academic_current_info">
                                                                                    <div class="row">

                                                                                        @if($school['school_img1'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img1']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                        @if($school['school_img2'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img2']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                        @if($school['school_img3'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img3']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                        @if($school['school_img4'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img4']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                        @if($school['school_img5'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img5']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                        @if($school['school_img6'] != '')
                                                                                        <div class="col-md-2">
                                                                                        <img src="{!! URL::to($school['school_img6']) !!}" style="width: 100px; margin-left: 10%;" alt="{!! $school['school_name']; !!}">
                                                                                        </div>
                                                                                        @endif

                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->

<script>
    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }
    $(document).ready(function () {
        $('.select2').select2();
        $("#set_dashboard_session").on('change', function (e)
        {
            var session_id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/academic-year/set-academic-year')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                success: function (response) {
                    location.reload();
                    alert(response.message);
                }
            });
        });
    });
</script>
@endsection

