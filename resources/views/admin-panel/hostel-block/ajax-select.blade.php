<option value="">Select Block</option>
@if(!empty($block))
  @foreach($block as $key => $value)
    <option value="{{ $key }}" {{ ($key == $hostel_block_id) ? 'selected' : '' }}>{{ $value }}</option>
  @endforeach
@endif