@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_configuration') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                    {!! Form::open(['files'=>TRUE,'id' => 'block-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    @include('admin-panel.hostel-block._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2><strong>Search</strong> By  </h2>
                            </div>
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('h_id', $block['arr_hostels'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_id'])!!}
                                                    <i class="arrow double"></i>
                                                    
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.h_block_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="block-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('language.hostel_name')}}</th>
                                                <th>{{trans('language.h_block_name')}}</th>
                                                <th>{{trans('language.floors')}}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="floorModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Floors </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="green" id="floor_success"></p>
      <table class="table m-b-0 c_list" id="floor-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.h_floor_no') !!}</th>
                    <th>{!! trans('language.h_floor_rooms') !!}</th>
                    <th>{!! trans('language.h_floor_description') !!}</th>
                    <th>Action</th>
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
      
        var table = $('#block-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            ajax: {
                url: "{{url('admin-panel/hostel/configuration/data-hostel-block')}}",
                data: function (d) {
                    d.name = $('input[name=name]').val();
                    d.hostel_id = $('select[name=h_id]').val();
                }
            },
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'hostel_name', name: 'hostel_name'},
                {data: 'h_block_name', name: 'h_block_name'},
                {data: 'floors', name: 'floors'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
    $(document).on('click','.floors',function(e){
        var h_floor_id = $(this).attr('h_floor_id');
        console.log(h_floor_id);
        $('#floorModel').modal('show');
        var table1 = $('#floor-table').DataTable({
                //dom: 'Blfrtip',
                destroy: true,
                pageLength: 10,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                ajax: {
                    url: '{{url('admin-panel/hostel/configuration/floors')}}',
                    data: function (d) {
                        d.id = h_floor_id;
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'h_floor_no', name: 'h_floor_no'},
                    {data: 'h_floor_rooms', name: 'h_floor_rooms'},
                    {data: 'h_floor_description', name: 'h_floor_description'},
                    {data: 'action', name: 'action'},
                ],
                columnDefs: [
                    {
                        "targets": 3, // your case first column
                        "width": "30%"
                    },
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });
    })
    $(document).on('click','.floor-records',function(e){
        var h_floor_id = $(this).attr('floor-record');
        var h_block_id = $(this).attr('block-record');
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/delete-floor/')}}/"+h_floor_id,
                success: function (data) {
                    $("#floor_success").html(data);
                    $(".mycustloading").hide();
                    var table1 = $('#floor-table').DataTable({
                        //dom: 'Blfrtip',
                        destroy: true,
                        pageLength: 10,
                        processing: true,
                        serverSide: true,
                        bLengthChange: false,
                        ajax: {
                            url: '{{url('admin-panel/hostel/configuration/floors')}}',
                            data: function (d) {
                                d.id = h_block_id;
                            }
                        },
                        columns: [
                            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                            {data: 'h_floor_no', name: 'h_floor_no'},
                            {data: 'h_floor_rooms', name: 'h_floor_rooms'},
                            {data: 'h_floor_description', name: 'h_floor_description'},
                            {data: 'action', name: 'action'},
                        ],
                        columnDefs: [
                            {
                                "targets": 3, // your case first column
                                "width": "30%"
                            },
                            {
                                targets: [ 0, 1, 2 ],
                                className: 'mdl-data-table__cell--non-numeric'
                            }
                        ]
                    });
                }
            });
        }
    });
</script>
@endsection

