@if(isset($block['h_block_id']) && !empty($block['h_block_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('h_block_id',old('h_block_id',isset($block['h_block_id']) ? $block['h_block_id'] : ''),['class' => 'gui-input', 'id' => 'h_block_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.hostel_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('hostel_id', $block['arr_hostels'],isset($block['hostel_id']) ? $block['hostel_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_id'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_block_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_block_name', old('h_block_name',isset($block['h_block_name']) ? $block['h_block_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_block_name'), 'id' => 'h_block_name']) !!}
        </div>
    </div>
    
</div>


<!-- Floor Information -->
<div class="header">
    <h2><strong>Floor</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($block['floorData']) ? COUNT($block['floorData']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="gardetable-body">
    <p class="green" id="grade_success"></p>
    @if(isset($block['floorData']) && !empty($block['floorData']))
    @php  $key = 0; @endphp
    @foreach($block['floorData'] as $gradeKey => $floorData)
    @php  
        $h_floor_id = "floorData[".$key."][h_floor_id]"; 
    @endphp
    <div id="h_block{{$key}}">
        {!! Form::hidden($h_floor_id,$floorData['h_floor_id'],['class' => 'gui-input']) !!}
        

        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.h_floor_no') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::text('floorData['.$key.'][h_floor_no]',$floorData['h_floor_no'], ['class' => 'form-control','placeholder'=>trans('language.h_floor_no'), 'id' => 'h_floor_no'.$key.'', 'required']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.h_floor_rooms') !!} <span class="red-text">*</span> :</lable> :</lable>
                <div class="form-group">
                    {!! Form::number('floorData['.$key.'][h_floor_rooms]',$floorData['h_floor_rooms'], ['class' => 'form-control','placeholder'=>trans('language.h_floor_rooms'), 'id' => 'h_floor_rooms'.$key.'', 'min'=> '0','required']) !!}
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.h_floor_description') !!} :</lable>
                <div class="form-group">
                    {!! Form::textarea('floorData['.$key.'][h_floor_description]',$floorData['h_floor_description'], ['class' => 'form-control','placeholder'=>trans('language.h_floor_description'), 'id' => 'h_floor_description'.$key.'', 'rows' => '2']) !!}
                </div>
            </div>
            <div class="col-lg-1 col-md-1 text-right">
                <lable>&nbsp;</lable>
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$floorData['h_floor_id']}})" >Remove</button>
                @endif
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
        
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.h_floor_no') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('floorData[0][h_floor_no]', '', ['class' => 'form-control','placeholder'=>trans('language.h_floor_no'), 'id' => 'h_floor_no0', 'required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.h_floor_rooms') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('floorData[0][h_floor_rooms]', '', ['class' => 'form-control','placeholder'=>trans('language.h_floor_rooms'), 'id' => 'h_floor_rooms0', 'min'=> '0','required']) !!}
            </div>
        </div>

        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.h_floor_description') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('floorData[0][h_floor_description]', '', ['class' => 'form-control','placeholder'=>trans('language.h_floor_description'), 'id' => 'h_floor_description0', 'rows' => '2']) !!}
            </div>
        </div>
        <div class="col-lg-1 col-md-1 text-left"><lable>&nbsp;</lable><button class="btn btn-primary custom_btn" type="button" onclick="addBlock()" >Add More</button></div>
    </div>
    @endif
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/hostel') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#block-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                h_block_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                hostel_id: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    function addBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#gardetable-body").append('<div id="h_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"> <lable class="from_one1">{!! trans('language.h_floor_no') !!} <span class="red-text">*</span> :</lable><div class="form-group"> <input type="text" name="floorData['+arr_key+'][h_floor_no]" id="h_floor_no'+arr_key+'" class="form-control" placeholder="{{trans('language.h_floor_no')}}" required /></div> </div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.h_floor_rooms') !!} <span class="red-text">*</span> :</lable> :</lable><div class="form-group"><input type="number" name="floorData['+arr_key+'][h_floor_rooms]" id="h_floor_rooms'+arr_key+'" class="form-control" min="0" required placeholder="{{trans('language.h_floor_rooms')}}" /> </div> </div><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.h_floor_description') !!} :</lable><div class="form-group"> <textarea class="form-control" placeholder="{{trans('language.h_floor_description')}}" id="h_floor_description'+arr_key+'" rows="2" name="floorData['+arr_key+'][h_floor_description]" cols="50"></textarea></div></div><div class="col-lg-1 col-md-1 text-right"><lable>&nbsp;</lable><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div> </div></div>');
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }
    function remove_block(id){
        $("#h_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    

</script>