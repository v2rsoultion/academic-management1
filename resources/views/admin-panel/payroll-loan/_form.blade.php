@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.loan_employee') !!}</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select size">
            {!!Form::select('staff_id',$loan['staff'],isset($loan['staff_id']) ? $loan['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
            <i class="arrow double"></i>
        </label>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.loan_principal') !!}</lable>
        {!! Form::number('loan_principal',old('loan_principal',isset($loan['loan_principal']) ? $loan['loan_principal']: ''),['class' => 'form-control','placeholder' => trans('language.loan_principal'), 'id' => 'loan_principal', 'onkeyup' => 'calculateLoanAmount()']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.loan_from_date') !!}</lable>
            {!! Form::Date('loan_from_date',old('loan_from_date',isset($loan['loan_from_date']) ? $loan['loan_from_date']: ''),['class' => 'form-control','placeholder' => trans('language.loan_from_date'), 'id' => 'loan_from_date', 'onkeyup' => 'calculateLoanAmount()']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <lable class="from_one1">{!! trans('language.loan_to_date') !!}</lable>
            {!! Form::Date('loan_to_date',old('loan_to_date',isset($loan['loan_to_date']) ? $loan['loan_to_date']: ''),['class' => 'form-control','placeholder' => trans('language.loan_to_date'), 'id' => 'loan_to_date', 'onkeyup' => 'calculateLoanAmount()']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.loan_interest_rate') !!}</lable>
        {!! Form::number('loan_interest_rate',old('loan_interest_rate',isset($loan['loan_inrterest_rate']) ? $loan['loan_inrterest_rate']: ''),['class' => 'form-control','placeholder' => trans('language.loan_interest_rate'), 'id' => 'loan_interest_rate', 'min' => 0, 'step' => 0.1, 'onkeyup' => 'calculateLoanAmount()']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.loan_deduct_amount') !!}</lable>
        {!! Form::number('loan_deduct_amount',old('loan_deduct_amount',isset($loan['loan_deduct_amount']) ? $loan['loan_deduct_amount']: ''),['class' => 'form-control','placeholder' => trans('language.loan_deduct_amount'), 'id' => 'loan_deduct_amount', 'readonly']) !!}
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
        <lable class="from_one1">{!! trans('language.loan_total_amt') !!}</lable>
        {!! Form::number('loan_total_amount',old('loan_total_amount',isset($loan['loan_total_amount']) ? $loan['loan_total_amount']: ''),['class' => 'form-control','placeholder' => trans('language.loan_total_amt'), 'id' => 'loan_total_amount', 'readonly']) !!}
        </div>
    </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-loan') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }); 
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');

        $("#manage_loan").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_id: {
                    required: true
                },
                loan_principal: {
                    required: true
                },
                loan_from_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
                loan_to_date: {
                    required: true,
                    date: true,
                    maxlength:10,
                    greaterThan: "#loan_from_date"  
                },
                loan_interest_rate: {
                    required: true
                },
                // loan_deduct_amount: {
                //     required: true
                // },
                // loan_total_amount: {
                //     required: true
                // },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

function calculateLoanAmount() {
    var loan_principal = $('#loan_principal').val();
    var interest_rate = $('#loan_interest_rate').val();
    var loan_deduct_amt = (loan_principal * interest_rate)/100;
    var r_loan_deduct_amt = loan_deduct_amt.toFixed(2);
    $('#loan_deduct_amount').val(r_loan_deduct_amt);
    var loan_from_date = new Date($('#loan_from_date').val());
    var loan_to_date   = new Date($('#loan_to_date').val());
    var total_diff_month = diff_months(loan_from_date, loan_to_date);
    
    console.log(loan_principal);
    console.log(loan_from_date);
    console.log(loan_to_date);
    console.log(total_diff_month);
    console.log(r_loan_deduct_amt);
    var final_total_amount = parseFloat(loan_principal) + parseFloat(total_diff_month*r_loan_deduct_amt);
    console.log(final_total_amount);
    $('#loan_total_amount').val((final_total_amount).toFixed(2));
}

function diff_months(loan_to_date, loan_from_date) 
{

  var diff =(loan_to_date.getTime() - loan_from_date.getTime()) / 1000;
   diff /= (60 * 60 * 24 * 7 * 4);
  return Math.abs(Math.round(diff));
  
}
</script>