@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_route') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/transport/route/add-route') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_route') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('route_name', old('route_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.route_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="route-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.route_name')}}</th>
                                                <th>{{trans('language.route_description')}}</th>
                                                <th>{{trans('language.route_stops')}}</th>
                                                <th>{{trans('language.map_views')}}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="infoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Route Locations </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="alert alert-danger" role="alert" style="display: none;" id="show_delete_message">
            Route Location Delete Succcessfully.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="row clearfix" style="margin-top: 10px;">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-5 col-md-5">
                <div class="input-group ">
                    {!! Form::text('location_name', old('location_name', ''), ['class' => 'form-control route_location_name ','placeholder'=>trans('language.location_name'), 'id' => 'route_location_name']) !!}

                    {!! Form::hidden('route_location_id', old('route_location_id', ''), ['class' => 'form-control route_location_id ','placeholder'=>trans('language.route_location_id'), 'id' => 'route_location_id']) !!}
                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                </div>
            </div>
        </div>
        
        <div class="table-responsive" style="margin-left: 10px;">
            <table class="table m-b-0 c_list" id="route-location-table" style="width:97%">
            {{ csrf_field() }}
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{{trans('language.location_name')}}</th>
                        <th>{{trans('language.latitude_logitude')}}</th>
                        <th>{{trans('language.pick_drop')}}</th>
                        <th>{{trans('language.km_distance')}}</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="map_views" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Map View </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding-top: 0px !important;">
                <div id="map" style="width: 100%; height: 520px;"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.modal').on('hidden.bs.modal', function () {
        location.reload();
    })
    $('#clearBtn').click(function(){
        location.reload();
    })
    $('#clearBtn1').click(function(){
        location.reload();
    })
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH1iJIF-DtiFNs29Fogl3oSnX0YWYdhvE&libraries=places&language=en"></script>
<script type="text/javascript">

    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;

    function calculateAndDisplayRoute(start,end,wayptsnew) {
        
        var mapOptions = {
            center: new google.maps.LatLng(26.2703357, 72.9603313),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        directionsDisplay = new google.maps.DirectionsRenderer();
        
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
       
        directionsDisplay.setMap(map);
        
        var start = start;
        var end = end ;
        var request = {
          origin:start, 
          destination:end,
          waypoints: wayptsnew,
          travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var myRoute = response.routes[0];
            var txtDir = '';
            for (var i=0; i<myRoute.legs[0].steps.length; i++) {
              txtDir += myRoute.legs[0].steps[i].instructions+"<br />";
            }
            // document.getElementById('directions').innerHTML = txtDir;
          }
        });
    }

</script>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
        var table = $('#route-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/transport/route/data')}}',
                data: function (d) {
                    d.route_name = $('input[name="route_name"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'route_name', name: 'route_name'},
                {data: 'route_description', name: 'route_description'},
                {data: 'total_stops', name: 'total_stops'},
                {data: 'map_views', name: 'map_views'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "20%"
                },
                // {
                //     "targets": 5, // your case first column
                //     "width": "20%"
                // },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    
    $(document).on('click','.route',function(e){
        var route_location_id = $(this).attr('route_location_id');
        $('#infoModel').modal('show');
        $('#route_location_id').val(route_location_id);
        var table1 = $('#route-location-table').DataTable({
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: "{{url('admin-panel/transport/route/get-route-location')}}",
                data: function (d) {
                    d.route_location_id = route_location_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'location_name', name: 'location_name'},
                {data: 'latitude_logitude', name: 'latitude_logitude'},
                {data: 'pick_drop', name: 'pick_drop'},
                {data: 'location_km_distance', name: 'location_km_distance'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


    $(document).on("keyup",".route_location_name",function(){

        var route_location_id = $('#route_location_id').val();
        var route_location_name = $('#route_location_name').val();
        //$('#infoModel').modal('show');
        
        var table1 = $('#route-location-table').DataTable({
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: "{{url('admin-panel/transport/route/get-route-location')}}",
                data: function (d) {
                    d.route_location_id   = route_location_id;
                    d.route_location_name = route_location_name;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'location_name', name: 'location_name'},
                {data: 'latitude_logitude', name: 'latitude_logitude'},
                {data: 'pick_drop', name: 'pick_drop'},
                {data: 'location_km_distance', name: 'location_km_distance'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    });


    function delete_route_location(route_location_id){

        var n1="route_location_id_"+route_location_id;  

        if(confirm("Are you sure?")) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/transport/route/delete-route-location')}}",
                type: 'POST',
                data: {
                    'route_location_id': route_location_id
                },
                success: function (data) {
                    $("#route_location_id_"+route_location_id).parent().parent().remove();
                    $("#show_delete_message").show();
                }
            });
        }
    }

    $(document).on("click",".map_views",function(){
        $('#map_views').modal('show');
    });



</script>
@endsection




