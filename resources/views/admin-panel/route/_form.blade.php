{!! Form::hidden('route_id',old('route_id',isset($route['route_id']) ? $route['route_id'] : ''),['class' => 'gui-input', 'id' => 'route_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->

<div class="row clearfix"> 
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.route_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('route_name', old('route_name',isset($route['route_name']) ? $route['route_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.route_name'), 'id' => 'route_name']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1">{!! trans('language.route_description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('route_description', old('route_description',isset($route['route_description']) ? $route['route_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.route_description'), 'id' => 'route_description', 'rows'=> 3]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.route_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('route_location[]', $route['arr_route'],isset($route['route_location']) ? $route['route_location'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'route_location', 'multiple','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>


</div>

<!-- Create Routes -->
<div class="header">
    <h2><strong>Create</strong> Routes</h2>
</div>

{!! Form::hidden('hide',isset($route['route_locations']) ? COUNT($route['route_locations']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    @if(isset($route['route_locations']) && !empty($route['route_locations']))
    @php $key = 0; @endphp
    @foreach($route['route_locations'] as $routelocationKey => $route_locations)
    @php 
        $vehicle_route_location = "routes[".$key."][document_category_id]";  
        $route_location_id = "routes[".$key."][route_location_id]"; 
    @endphp
    <div id="document_block{{$routelocationKey}}">
        {!! Form::hidden($route_location_id,$route_locations['route_location_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.location_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!! Form::text('routes['.$key.'][location_name]', $route_locations['location_name'], ['class' => 'form-control','placeholder'=>trans('language.location_name'), 'id' => 'location_name' , required]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.location_sequence') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!! Form::text('routes['.$key.'][location_sequence]', $route_locations['location_sequence'], ['class' => 'form-control','placeholder'=>trans('language.location_sequence'), 'id' => 'location_sequence' , required]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>

        </div>

        <div class="row clearfix">
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.location_latitude') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!! Form::text('routes['.$key.'][location_latitude]', $route_locations['location_latitude'], ['class' => 'form-control','placeholder'=>trans('language.location_latitude'),'counter' => "'.$key.'", 'id' => 'location_latitude'.$key , required,readonly]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.location_longitude') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!! Form::text('routes['.$key.'][location_longitude]', $route_locations['location_logitude'], ['class' => 'form-control','placeholder'=>trans('language.location_longitude'),'counter' => "'.$key.'", 'id' => 'location_longitude'.$key , required,readonly]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>

            
            <div class="col-lg-1 col-md-1">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="getlatlong('{{$key}}')" >Get Lat Long</button></div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.eta_pickiup') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class="field select" style="width: 100%">
                        {!! Form::time('routes['.$key.'][eta_pickiup]', $route_locations['location_pickup_time'], ['class' => 'form-control times','placeholder'=>trans('language.eta_pickiup'), 'counter' => "'.$key.'" , 'id' => 'eta_pickiup'.$key , required]) !!}
                        <i class="arrow double"></i>
                    </label>
                    <p class="red" id="eta_pick_err{{$key}}"></p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.eta_drop') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!! Form::time('routes['.$key.'][eta_drop]', $route_locations['location_drop_time'], ['class' => 'form-control times','placeholder'=>trans('language.eta_drop'), 'id' => 'eta_drop'.$key , 'counter' => $key , required]) !!}
                        <i class="arrow double"></i>
                    </label>
                    <p class="red" id="eta_drop_err{{$key}}"></p>
                </div>
            </div>

            <div class="col-lg-1 col-md-1">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($routelocationKey == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="delete_block({{$routelocationKey}},{{$route_locations['route_location_id']}})" >Remove</button>
                @endif
            </div>
        </div>

    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.location_name') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('routes[0][location_name]', old('location_name',isset($route['location_name']) ? $route['location_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.location_name'), 'id' => 'location_name' , required]) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.location_sequence') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('routes[0][location_sequence]', old('location_sequence',isset($route['location_sequence']) ? $route['location_sequence']: ''), ['class' => 'form-control','placeholder'=>trans('language.location_sequence'), 'id' => 'location_sequence' , required]) !!}
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.location_latitude') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('routes[0][location_latitude]', old('location_latitude',isset($route['location_latitude']) ? $route['location_latitude']: ''), ['class' => 'form-control','placeholder'=>trans('language.location_latitude'),'counter' => "0", 'id' => 'location_latitude0', required , readonly]) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.location_longitude') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('routes[0][location_longitude]', old('location_longitude',isset($route['location_longitude']) ? $route['location_longitude']: ''), ['class' => 'form-control','placeholder'=>trans('language.location_longitude'), 'counter' => "0", 'id' => 'location_longitude0', required, readonly]) !!}
            </div>
        </div>
        <div class="col-lg-2 col-md-2">&nbsp;</div>
        <div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="getlatlong('0')" >Get Lat Long</button></div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.eta_pickiup') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::time('routes[0][eta_pickiup]', old('eta_pickiup',isset($route['eta_pickiup']) ? $route['eta_pickiup']: ''), ['class' => 'form-control times','placeholder'=>trans('language.eta_pickiup'), 'id' => 'eta_pickiup0' , 'counter' => "0" , required]) !!}
            </div>
            <p class="red" id="eta_pick_err0"></p>
        </div>
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.eta_drop') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::time('routes[0][eta_drop]', old('eta_drop',isset($route['eta_drop']) ? $route['eta_drop']: ''), ['class' => 'form-control times','placeholder'=>trans('language.eta_drop'), 'id' => 'eta_drop0' ,'counter' => "0", required]) !!}
            </div>
            <p class="red" id="eta_drop_err0"></p>
        </div>
        <div class="col-lg-2 col-md-2">&nbsp;</div>
        <div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button></div>
    </div>

    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save' ,'id'=>'save_form' , 'onclick' => 'saveform()' ]) !!}
        <a href="{!! url('admin-panel/transport') !!}" class="btn btn-raised" >Cancel</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: white;
    }
    div.pac-container {
        z-index: 99999999999 !important;
    }
    .input-controls {
      margin-top: 10px;
      border: 1px solid transparent;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      height: 32px;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    .searchInput {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 50%;
      height: 40px;
    }
    .searchInput:focus {
      border-color: #4d90fe;
    }

    #sectiontable-body .btn-primary {
        width: 115px !important;
    }
</style>

<script type="text/javascript">

    function saveform(){

        var hide = $("#hide").val();

        for (counter = 0; counter <= hide; counter++) {
            var start_time = $('#eta_pickiup'+counter).val(); //start time
            var end_time = $('#eta_drop'+counter).val();  //end time

            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();

            if(stt > endt && end_time != '') {
                $("#eta_drop_err"+counter).html('Drop time should be greater than pick up time');
                $('#eta_drop'+counter).val('');
                $("#save_form").attr("disabled", "disabled");
            } else {
                $("#eta_drop_err"+counter).html('');
            }
    
        }


        
    } 


    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

    // $(document).on('click','.gui-file',function(e){
    //     //var ext = $(this.value).val().split('.').pop().toLowerCase();
    //     // if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    //     //     alert('invalid extension!');
    //     // }
    //     alert();
    // });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#route-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                route_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                // staff_profile_img: {
                //     extension: 'jpg,png,jpeg,gif'
                // },
                route_description: {
                    required: true
                },
                route_location: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });


    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b);
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.location_name') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="text" name="routes['+arr_key+'][location_name]" class="form-control" placeholder="{{trans("language.location_name")}}" id="location_name'+arr_key+'" required/></div></div><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.location_sequence') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="text" name="routes['+arr_key+'][location_sequence]" class="form-control" placeholder="{{trans("language.location_sequence")}}" id="location_sequence'+arr_key+'" required/></div></div></div><div class="row clearfix"><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.location_latitude') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="text" name="routes['+arr_key+'][location_latitude]" class="form-control" placeholder="{{trans("language.location_latitude")}}" counter="'+arr_key+'" id="location_latitude'+arr_key+'" required readonly/></div></div><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.location_longitude') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="text" name="routes['+arr_key+'][location_longitude]" class="form-control" placeholder="{{trans("language.location_longitude")}}" counter="'+arr_key+'" id="location_longitude'+arr_key+'" required readonly/></div></div><div class="col-lg-2 col-md-2">&nbsp;</div><div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="getlatlong('+arr_key+')" >Get Lat Long</button></div></div><div class="row clearfix"><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.eta_pickiup') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="time" name="routes['+arr_key+'][eta_pickiup]" counter="'+arr_key+'" class="form-control times" placeholder="{{trans("language.eta_pickiup")}}" id="eta_pickiup'+arr_key+'" required/></div> <p class="red" id="eta_pick_err'+arr_key+'"></p> </div><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.eta_drop') !!} <span class="red-text">*</span> :</lable><div class="form-group "><input type="time" name="routes['+arr_key+'][eta_drop]" counter="'+arr_key+'" class="form-control times" placeholder="{{trans("language.eta_drop")}}" id="eta_drop'+arr_key+'" required/></div> <p class="red" id="eta_drop_err'+arr_key+'"></p> </div><div class="col-lg-2 col-md-2">&nbsp;</div><div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div></div>');


        $("#document_category_id"+arr_key).select2();
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function delete_block(id,route_location_id){

        if(confirm("Are you sure?")) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/transport/route/delete-route-location')}}",
                type: 'POST',
                data: {
                    'route_location_id': route_location_id
                },
                success: function (data) {
                    $("#document_block"+id).remove();
                }
            });
        }
    }

    

    $(document).on("click", ".times", function () {
        counter = $(this).attr('counter');
        var start_time = $('#eta_pickiup'+counter).val(); //start time
        var end_time = $('#eta_drop'+counter).val();  //end time
        
        //convert both time into timestamp
        var stt = new Date("November 30, 2018 " + start_time);
        stt = stt.getTime();
        var endt = new Date("November 30, 2018 " + end_time);
        endt = endt.getTime();
        if(stt > endt && end_time != '') {
            $("#eta_drop_err"+counter).html('Drop time should be greater than pick up time');
        //    $('#eta_drop'+counter).val('')
        } else {
            $("#eta_drop_err"+counter).html('');
            $("#save_form").removeAttr("disabled");
        }
        // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
        // $(this).closest(".times").bootstrapMaterialDatePicker('show');
        
    });
    $(document).on("focus", ".times", function () {
        counter = $(this).attr('counter');
        var start_time = $('#eta_pickiup'+counter).val(); //start time
        var end_time = $('#eta_drop'+counter).val();  //end time
        //convert both time into timestamp
        var stt = new Date("November 30, 2018 " + start_time);
        stt = stt.getTime();
        var endt = new Date("November 30, 2018 " + end_time);
        endt = endt.getTime();
        if(stt > endt && end_time != '') {
            $("#eta_drop_err"+counter).html('Drop time should be greater than pick up time');
        //    $('#eta_drop'+counter).val('')
        } else {
            $("#eta_drop_err"+counter).html('');
            $("#save_form").removeAttr("disabled");
        }
        // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
        // $(this).closest(".times").bootstrapMaterialDatePicker('show');
    });
    $(document).on("keyup", ".times", function () {
        console.log('keyup');
        counter = $(this).attr('counter');
        var start_time = $('#eta_pickiup'+counter).val(); //start time
        var end_time = $('#eta_drop'+counter).val();  //end time
        //convert both time into timestamp
        var stt = new Date("November 30, 2018 " + start_time);
        stt = stt.getTime();
        var endt = new Date("November 30, 2018 " + end_time);
        endt = endt.getTime();
         
        if(stt > endt && end_time != '') {
            $("#eta_drop_err"+counter).html('Drop time should be greater than pick up time');
        //    $('#eta_drop'+counter).val(c_date+':'+c_time);
        } else {
            $("#eta_drop_err"+counter).html('');
            $("#save_form").removeAttr("disabled");
        }

        // $('.times').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
        //       $(this).valid();  
        // });
        // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
        // $(this).closest(".times").bootstrapMaterialDatePicker('show');
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH1iJIF-DtiFNs29Fogl3oSnX0YWYdhvE&libraries=places&language=en"></script>


<script type="text/javascript">
 //Set up some of our variables.
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
        
//Function called to initialize / create the map.
//This is called when the page has loaded.

function getlatlong_old(counter) {
    $('#map_views').modal('show');
 
    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(26.4832015,73.2866121);
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom: 7, //The zoom value.
      counter : counter
    };
 
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {   
             
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true, //make it draggable
                counter : counter
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation(marker.counter);
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation(map.counter);
    });
}
        
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(counter){
    //Get location.

    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    $('#location_latitude'+counter).val(currentLocation.lat()); //start time
    $('#location_longitude'+counter).val(currentLocation.lng());  //end time
    $('#map_views').modal('hide');
}
        
        
//Load the map when the page has finished loading.
//google.maps.event.addDomListener(window, 'load', initMap);
</script>



<script>
/* script */
function getlatlong(counter) {
    $('#map_views').modal('show');

    $('.showsearchbox').html('<input id="searchInput'+counter+'" class="input-controls searchInput" type="text" placeholder="Enter a location">');

    var latlng = new google.maps.LatLng(26.23894689999999,73.02430939999999);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13,
      counter : counter
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput'+counter);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
    
        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng(),counter);
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);
       
    });
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {        
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),counter);
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });
}
function bindDataToForm(address,lat,lng,counter){

    //$("#route-form").valid();
    $('#location_latitude'+counter).val(lat); //start time
    $('#location_longitude'+counter).val(lng);  //end time
   // document.getElementById("route-form").valid();

    $('#location_latitude'+counter).parent().addClass("has-success");
    $('#location_latitude'+counter).parent().next().html("");


    $('#location_longitude'+counter).parent().addClass("has-success");
    $('#location_longitude'+counter).parent().next().html("");

    //$('#map_views').modal('hide');
}

</script>