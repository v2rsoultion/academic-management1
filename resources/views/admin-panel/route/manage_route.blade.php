@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.search_button4{
    margin-left: 20px !important;
}
/*.teacher_ids{
    color: red;
}*/

</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.manage_route') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_route') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2><strong>Map</strong> Route  </h2>
                            </div>
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'mapping-route-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}

                                    <div class="table m-b-0 c_list" id="map-data">
                                        {{ csrf_field() }}
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row ">
                                            {!! Form::submit('Apply', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/transport/map-route-data')}}",
            type: 'GET',
            success: function (data) {

                $('#map-data').html(data.data);
                $('.select2').select2();
            }
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

    function assignteacher(id){
        var teacher_ids = [];
        var class_id    = $('#class_id').val();
        var section_id    = $('#section_id').val();
        $('#teacher_ids_'+id+' :selected').each(function(i, selectedElement) {
         teacher_ids[i]   = $(selectedElement).val();
        });
        
            if(confirm("Are you sure?")){
                $('.mycustloading').css('display','block');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/teacher-subject-mapping/save')}}",
                    type: 'GET',
                    data: {
                        'class_id': class_id,
                        'section_id': section_id,
                        'subject_id': id,
                        'teacher_ids': teacher_ids
                    },
                    success: function (data) {
                        $('.mycustloading').css('display','none');
                        if(data.status = 1001){
                            $('#display_cust_msg').html(data.message);
                        }else if(data.status = 1002){
                            $('#display_cust_msg2').html(data.message);
                        }   //window.location.reload(true);     
                    }
                });
            }
    }

</script>
@endsection




