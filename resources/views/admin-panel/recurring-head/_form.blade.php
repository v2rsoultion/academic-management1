@if(isset($recurring['rc_head_id']) && !empty($recurring['rc_head_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('rc_head_id',old('rc_head_id',isset($recurring['rc_head_id']) ? $recurring['rc_head_id'] : ''),['class' => 'gui-input', 'id' => 'rc_head_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="alert alert-danger" role="alert" id="inst_error" style="display: none;">
</div>
<!-- Basic Info onetime -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.particular_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('rc_particular_name', old('rc_particular_name',isset($recurring['rc_particular_name']) ? $recurring['rc_particular_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.particular_name'), 'id' => 'rc_particular_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fee_classes') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('rc_classes[]', $recurring['arr_class'],isset($recurring['rc_classes']) ? $recurring['rc_classes'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'rc_classes', 'multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.recurring_fee') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('rc_amount', old('rc_amount',isset($recurring['rc_amount']) ? $recurring['rc_amount']: ''), ['class' => 'form-control','placeholder'=>trans('language.recurring_fee'), 'id' => 'rc_amount', 'min'=>'1', 'max'=>'9999999999', 'maxlength'=>'10']) !!}
        </div>
    </div>

</div>


<!-- Document Information -->
<div class="header">
    <h2><strong>Installment</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($recurring['inst_data']) ? COUNT($recurring['inst_data']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    <p class="green" id="document_success"></p>
    @if(isset($recurring['inst_data']) && !empty($recurring['inst_data']))
    @php  $key = 0; @endphp
    @foreach($recurring['inst_data'] as $instKey => $inst)
    @php  
        $rc_inst_id = "inst[".$key."][rc_inst_id]"; 
    @endphp
    <div id="inst_block{{$key}}">
        {!! Form::hidden($rc_inst_id,$inst['rc_inst_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.inst_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::text('inst['.$key.'][rc_inst_particular_name]', $inst['rc_inst_particular_name'], ['class' => 'form-control','placeholder'=>trans('language.inst_name'), 'id' => 'rc_inst_particular_name'.$key.'', 'required']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.fee_date') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::date('inst['.$key.'][rc_inst_date]',$inst['rc_inst_date'], ['class' => 'form-control date','placeholder'=>trans('language.fee_date'), 'id' => 'rc_inst_date'.$key.'', 'required']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.fee_amount') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('inst['.$key.'][rc_inst_amount]', $inst['rc_inst_amount'], ['class' => 'form-control','placeholder'=>trans('language.fee_amount'), 'id' => 'rc_inst_amount'.$key.'', 'required', 'min' => '1', 'max'=> '9999999999', 'maxlength'=>'10']) !!}
                </div>
            </div>
            
            <div class="col-lg-3 col-md-3 text-right">
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$inst['rc_inst_id']}})" >Remove</button>
                @endif
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.inst_name') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('inst[0][rc_inst_particular_name]', '', ['class' => 'form-control','placeholder'=>trans('language.inst_name'), 'id' => 'rc_inst_particular_name0', 'required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.fee_date') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::date('inst[0][rc_inst_date]', '', ['class' => 'form-control date','placeholder'=>trans('language.fee_date'), 'id' => 'rc_inst_date0', 'required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.fee_amount') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('inst[0][rc_inst_amount]', '0', ['class' => 'form-control','placeholder'=>trans('language.fee_amount'), 'id' => 'rc_inst_amount0', 'required', 'min' => '1', 'max'=> '9999999999', 'maxlngth'=>'10']) !!}
            </div>
        </div>
        
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button></div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        
        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate < inputDate1 || curDate == inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select future date");

        $("#recurring-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                rc_particular_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                rc_classes: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                rc_amount: {
                    required: true,
                    number: true
                },
                rc_inst_amount: {
                    number: true
                },
                rc_inst_date: {
                    date: true,
                    required: true,
                    maxDate: true,
                    maxlength:10
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        // $('.date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}).on('change', function(ev) {
        //     $(this).valid();  
        // });;

    });
    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body").append('<div id="inst_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.inst_name') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"><input type="text" name="inst['+arr_key+'][rc_inst_particular_name]" id="rc_inst_particular_name'+arr_key+'" class="form-control" placeholder="{{trans('language.inst_name')}}" required /></div></div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.fee_date') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"> <input type="date" name="inst['+arr_key+'][rc_inst_date]" id="rc_inst_date'+arr_key+'" class="form-control date" placeholder="{{trans('language.fee_date')}}" required /></div></div><div class="col-lg-3 col-md-3"> <lable class="from_one1">{!! trans('language.fee_amount') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"><input type="number" name="inst['+arr_key+'][rc_inst_amount]" id="rc_inst_amount'+arr_key+'" class="form-control" placeholder="{{trans('language.fee_amount')}}" min="1" max="9999999999" maxlength="10" required  value="0"/></div></div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div></div>');
        // $('.date').bootstrapMaterialDatePicker({ weekStart : 0,time: false});
    }
    function remove_block(id){
        $("#inst_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function remove_record(rc_inst_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recurring-head/delete-inst/')}}/"+rc_inst_id,
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }
    
    $(document).ready(function() {
        $("#recurring-form").submit(function() { 
            var totalFees = $("#rc_amount").val();  
            var totalInst = $("#hide").val();
            
            var calculateAmt = 0;
            for (i = 0; i < totalInst; i++) { 
                var instAmt = $("#rc_inst_amount"+i).val();
                calculateAmt = parseFloat(calculateAmt) + parseFloat(instAmt);
            }
            
            if(parseFloat(totalFees) < parseFloat(calculateAmt)){
                $('#inst_error').html("All installment fees should be equal to Total Fees.");
                $('#inst_error').show();
                return false;
            } else if(parseFloat(totalFees) > parseFloat(calculateAmt)){
                $('#inst_error').html("All installment fees should be equal to Total Fees.");
                $('#inst_error').show();
                return false;
            } else {
                $('#inst_error').html("");
                $('#inst_error').hide();
                return true;
            }
        })
    });

</script>

