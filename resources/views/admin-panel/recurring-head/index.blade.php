@extends('admin-panel.layout.header')
@section('content')

<style type="text/css">
  
</style>
<!--  Main content here -->
<section class="content contact">

  <div class="block-header">
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
              <h2>{!! trans('language.view_recurring_h') !!}</h2>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <a href="{!! url('admin-panel/recurring-head/add-recurring-head') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
              <li class="breadcrumb-item">{!! trans('language.view_recurring_h') !!}</li>
            </ul>
          </div>
      </div>
  </div>
  <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                  <div class="alert alert-success" role="alert">
                                      {{ session()->get('success') }}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                @if($errors->any())
                                  <div class="alert alert-danger" role="alert">
                                      {{$errors->first()}}
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                  <div class="row clearfix">
                                      <div class="col-lg-3 col-md-3">
                                          <label class=" field select" style="width: 100%">
                                              {!!Form::select('class_id', $recurring['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id'])!!}
                                              <i class="arrow double"></i>
                                          </label>
                                      </div>
                                      <div class="col-lg-3 col-md-3">
                                          <div class="input-group ">
                                              {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.particular_name'), 'id' => 'name']) !!}
                                              <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                          </div>
                                      </div>
                                      <div class="col-lg-1 col-md-1">
                                          {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                      </div>
                                      <div class="col-lg-1 col-md-1">
                                          {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                      </div>
                                      
                                  </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="recurring-head-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{!! trans('language.particular_name') !!}</th>
                                                <th>{!! trans('language.fee_classes') !!}</th>
                                                <th>{!! trans('language.recurring_fee') !!}(In Rs)</th>
                                                <th>{!! trans('language.no_of_inst') !!}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  
</section>
<div class="modal fade" id="installmentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Installments </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="green" id="inst_success"></p>
      <table class="table m-b-0 c_list" id="installment-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.inst_name') !!}</th>
                    <th>{!! trans('language.fee_date') !!}</th>
                    <th>{!! trans('language.fee_amount') !!}</th>
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ClassesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Classes </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table m-b-0 c_list" id="classes-table" width="100%">
            <thead>
                <tr>
                    <th>{{trans('language.s_no')}}</th>
                    <th>{!! trans('language.class_name') !!}</th>
                </tr>
            </thead>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recurring-head-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/recurring-head/data')}}',
                data: function (d) {
                    d.name = $('input[name=name]').val();
                    d.class_id = $('select[name="class_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'rc_particular_name', name: 'rc_particular_name'},
                {data: 'classes', name: 'classes'},
                {data: 'rc_amount', name: 'rc_amount'},
                {data: 'no_of_inst', name: 'no_of_inst'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "18%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $(document).on('click','.installments',function(e){
        var installment_id = $(this).attr('installment-id');
        $('#installmentModel').modal('show');
        var table1 = $('#installment-table').DataTable({
                //dom: 'Blfrtip',
                destroy: true,
                pageLength: 10,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ],
                ajax: {
                    url: '{{url('admin-panel/recurring-head/installment')}}',
                    data: function (d) {
                        d.id = installment_id;
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'rc_inst_particular_name', name: 'rc_inst_particular_name'},
                    {data: 'rc_inst_date', name: 'rc_inst_date'},
                    {data: 'rc_inst_amount', name: 'rc_inst_amount'},
                    // {data: 'action', name: 'action'},
                ],
                columnDefs: [
                   
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });
    })
    $(document).on('click','.inst-records',function(e){
        var installment_id = $(this).attr('inst-record');
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recurring-head/delete-inst/')}}/"+installment_id,
                success: function (data) {
                    $("#inst_success").html(data);
                    $(".mycustloading").hide();
                    var table1 = $('#installment-table').DataTable({
                        //dom: 'Blfrtip',
                        destroy: true,
                        pageLength: 10,
                        processing: true,
                        serverSide: true,
                        bLengthChange: false,
                        // buttons: [
                        //     'copy', 'csv', 'excel', 'pdf', 'print'
                        // ],
                        ajax: {
                            url: '{{url('admin-panel/recurring-head/installment')}}',
                            data: function (d) {
                                d.id = installment_id;
                            }
                        },
                        columns: [
                            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                            {data: 'rc_inst_particular_name', name: 'rc_inst_particular_name'},
                            {data: 'rc_inst_date', name: 'rc_inst_date'},
                            {data: 'rc_inst_amount', name: 'rc_inst_amount'},
                            {data: 'action', name: 'action'},
                        ],
                        columnDefs: [
                        
                            {
                                targets: [ 0, 1, 2 ],
                                className: 'mdl-data-table__cell--non-numeric'
                            }
                        ]
                    });
                }
            });
        }
    });
    
    $(document).on('click','.classes',function(e){
        var head_id = $(this).attr('head-id');
        $('#ClassesModel').modal('show');
        var table1 = $('#classes-table').DataTable({
            destroy: true,
            pageLength: 10,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            ajax: {
                url: '{{url('admin-panel/one-time/classes')}}',
                data: function (d) {
                    d.id = head_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_name', name: 'class_name'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    })
</script>
@endsection