<option value="">Select Lecture</option>
@if(!empty($lectures))
  @foreach($lectures as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif