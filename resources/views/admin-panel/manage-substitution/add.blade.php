@extends('admin-panel.layout.header')

@section('content')
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <a href="{!! url('admin-panel/substitute-management/view-allocations/'.$listData['id']) !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/substitute-management') !!}">{!! trans('language.menu_substitute_management') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/substitute-management/manage-substitution') !!}">{!! trans('language.manage_substitution') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small><br />
                        Teacher Name: {!! $leave_application['getStaff']->staff_name !!} <br />
                        Leave Dates: {!! date("d F Y", strtotime($leave_application->staff_leave_from_date)).' - '.date("d F Y", strtotime($leave_application->staff_leave_to_date)) !!} <br />
                        </small> </h2>
                    </div>
                    <div class="body form-gap">
                    {!! Form::open(['files'=>TRUE,'id' => 'substitution-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    @include('admin-panel.manage-substitution._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

