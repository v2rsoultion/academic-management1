<option value="">Select Hostel</option>
@if(!empty($hostels))
  @foreach($hostels as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif