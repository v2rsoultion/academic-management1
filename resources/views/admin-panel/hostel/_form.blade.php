@if(isset($hostel['hostel_id']) && !empty($hostel['hostel_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('hostel_id',old('hostel_id',isset($hostel['hostel_id']) ? $hostel['hostel_id'] : ''),['class' => 'gui-input', 'id' => 'hostel_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    @foreach ($errors->all() as $error)
        {{ $error }}</br >
    @endforeach
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.hostel_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('hostel_name', old('hostel_name',isset($hostel['hostel_name']) ? $hostel['hostel_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.hostel_name'), 'id' => 'hostel_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.hostel_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('hostel_type', $hostel['arr_hostel_type'],isset($hostel['hostel_type']) ? $hostel['hostel_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div> 
</div>
<div class="row clearfix">
    <div class="col-lg-12" style="margin-bottom: 10px !important;">
        <lable class="from_one1">{!! trans('language.hostel_fees_head') !!}:</lable>
    </div>
    <div class="row col-lg-12">
        @foreach($hostel['arr_fees_head'] as $key => $FeesHeadData)
          <div class="col-lg-2">
            <div class="checkbox" id="customid{{$key}}">
                 <!-- @php 
            $status = 0;
            @endphp -->
                @php $checked = in_array($key,$hostel['arr_h_fees_head']) ? 'checked' : '';
                @endphp
            @if($checked == 'checked')
            @php 
            $status = 1;
            @endphp

            {!!Form::hidden("fee_head[$key][ex_fees_head_id]", $key)!!}
            @endif
            <!-- {!!Form::checkbox("fee_head[$key][fees_head_id]", $key, in_array($key,$hostel['arr_h_fees_head']) ? true : false, ['class' => 'checkboxes headchecks', 'id' => 'checkbox'.$key, 'counter-id' => $key])!!} -->
            <input id="checkbox{{$key}}" class="checkboxes headchecks" counter-id="{{$key}}" type="checkbox" value="{{$key}}" name="fee_head[{{$key}}][fees_head_id]" {{$checked}}>
                <label  class="from_one1" style="margin-bottom: 0px !important;"  for="checkbox{{$key}}">{{$FeesHeadData}}</label>
            </div>
          </div>    
        @endforeach 
    </div>     
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/hostel') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#hostel-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                hostel_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                hostel_type: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>