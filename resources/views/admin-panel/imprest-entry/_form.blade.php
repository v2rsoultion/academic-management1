{!! Form::hidden('ac_entry_id',old('ac_entry_id',isset($imprest_entry['ac_entry_id']) ? $imprest_entry['ac_entry_id'] : ''),['class' => 'gui-input', 'id' => 'ac_entry_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
    <!-- <div class="form-group ">
    <input type="text" class="date-format form-control" name="datef" id="datef" required />
    </div> -->
        <lable class="from_one1">{!! trans('language.ac_entry_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('ac_entry_name', old('ac_entry_name',isset($imprest_entry['ac_entry_name']) ? $imprest_entry['ac_entry_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.ac_entry_name'), 'id' => 'ac_entry_name']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.ac_entry_amt') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('ac_entry_amt', old('ac_entry_amt',isset($imprest_entry['ac_entry_amt']) ? $imprest_entry['ac_entry_amt']: 1), ['class' => 'form-control','placeholder'=>trans('language.ac_entry_amt'), 'id' => 'ac_entry_amt', 'min'=> '1', 'step'=> 'any']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $imprest_entry['arr_class'],isset($imprest_entry['class_id']) ? $imprest_entry['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection("section_id",this.value)'])!!}
            </label>
        </div>
    </div> 
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
            {!!Form::select('section_id', $imprest_entry['arr_section'],isset($imprest_entry['section_id']) ? $imprest_entry['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#entry-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */

            rules: {
                ac_entry_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                class_id: {
                    required: true,
                },
                section_id: {
                    required: true,
                },
                ac_entry_amt: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });

    

</script>