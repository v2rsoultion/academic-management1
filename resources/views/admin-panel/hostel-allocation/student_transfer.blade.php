@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Student</strong> Transfer <small></small> </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                {{$errors->first()}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                            
                            <div class="alert alert-danger" role="alert" id="searchError" style="display: none;"></div>
                            <div class="clearfix"></div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('s_hostel_id', $allocate['arr_hostels'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_hostel_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $allocate['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $allocate['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: block">
                            <hr>
                           
                            <div class="table-responsive" >    
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.name')}}</th>
                                            <th>{{trans('language.student_father_name')}}</th>
                                            <th>{{trans('language.class_section')}}</th>
                                            <th>{{trans('language.hostel_block')}}</th>
                                            <th>{{trans('language.fees_head')}}</th>
                                            <th>{{trans('language.room_category')}}</th>
                                            <th>{{trans('language.floor_room_no')}}</th>
                                            <th>{{trans('language.leave_date')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<div class="modal fade" id="allocateModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Student Transfer </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 15px !important;padding-bottom: 25px !important;">
            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
            
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.hostel_name') !!} <span class="red-text">*</span> :</lable>
                    <label class=" field select" style="width: 100%">
                        {!!Form::select('hostel_id', $allocate['arr_hostels'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_id','onChange' => 'getBlock(this.value), getFeesHead(this.value)'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.h_block_name') !!} <span class="red-text">*</span> :</lable>
                    <label class=" field select" style="width: 100%">
                        {!!Form::select('h_block_id', $allocate['arr_block'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_block_id','onChange' => 'getFloor(this.value)'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.h_floor_no') !!} <span class="red-text">*</span> :</lable>
                    <div class="form-group m-bottom-0">
                        <label class=" field select" style="width: 100%">
                            {!!Form::select('h_floor_id', $allocate['arr_floor'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_floor_id'])!!}
                            <i class="arrow double"></i>
                            
                        </label>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.h_room_cate_name') !!} <span class="red-text">*</span> :</lable>
                    <div class="form-group ">
                        <label class=" field select" style="width: 100%">
                            {!!Form::select('h_room_cate_id', $allocate['arr_room_cate'],isset($allocate['h_room_cate_id']) ? $allocate['h_room_cate_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_room_cate_id','onChange' => 'getCateRoomInfo(this.value)'])!!}
                            <i class="arrow double"></i>
                            
                        </label>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">{!! trans('language.h_room_no') !!} <span class="red-text">*</span> :</lable>
                    <div class="form-group m-bottom-0">
                        <label class=" field select" style="width: 100%">
                            {!!Form::select('h_room_id', $allocate['arr_room'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_room_id','onChange' => 'getRoomInfo()'])!!}
                            <i class="arrow double"></i>
                            
                        </label>
                    </div>
                </div> 
            </div>
            <div class="row clearfix" id="show-fees-head" style="display: none;">
               
            </div>
            {!! Form::hidden('student_id', old('student_id',''), ['class' => 'form-control', 'id' => 'student_id']) !!}
            {!! Form::hidden('h_student_map_id', old('h_student_map_id',''), ['class' => 'form-control', 'id' => 'h_student_map_id']) !!}
            <div class="row clearfix">
                
                <div class="col-lg-2 col-md-2">
                    <lable class="from_one1">&nbsp;</lable> <br />
                    {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit', 'id'=>'list-submit']) !!}
                </div>
                <div class="col-lg-3 col-md-3">
                    <lable class="from_one1">&nbsp;</lable> <br />
                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                </div>

            </div>
            <br />
            <div id="room_exist_info" style="display: none">
                <div class="student_transfer_info">
                <b>Hostel-Block-Floor:</b> <span id="hostel_block_floor"></span> <br />
                <b>Room No(Room Category):</b> <span id="room_category"></span><br />
                <b>Room Capacity:</b> <span id="room_capacity_">0</span><br />
                <b>Fees Head:</b> <span id="fees_head"></span><br />
                </div>
            </div>
            <div id="room_info" style="display: none">
            <hr />
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3">
                        <b>Room Capacity: <div id="room_capacity">0</div></b>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <b>Room Available: <div id="room_availability">0</div></b>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive" >    
                            <table class="table m-b-0 c_list " id="student-list" style="width:100%;">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>{{trans('language.student_enroll_number')}}</th>
                                        <th>{{trans('language.name')}}</th>
                                        <th>{{trans('language.class_section')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ 
            $('#searchError').html('');
            $("#searchError").hide();
        });
        $('#leave_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false }).on('change', function(e, date){ $(this).valid(); });
        $('#join_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false,minDate : new Date()}).on('change', function(e, date) {  
            $('#leave_date').val(''); 
            $(this).valid();  
            $('#leave_date').bootstrapMaterialDatePicker('setMinDate', date);  
        });

        $('#allocateModel').on('hidden.bs.modal', function () {
            location.reload();
        })
    });
    $(document).ready(function () {
      
        var table = $('#student-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            ajax: {
                url: "{{url('admin-panel/hostel/allocation/get-student-list/')}}",
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                    d.section_id = $('select[name=section_id]').val();
                    d.hostel_id = $('select[name=s_hostel_id]').val();
                    d.status = 'exist';
                    d.leave_status = 0;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'class_section', name: 'class_section'},
                {data: 'hostel_block', name: 'hostel_block'},
                {data: 'h_fees_head', name: 'h_fees_head'},
                {data: 'room_category', name: 'room_category'},
                {data: 'floor_room_no', name: 'floor_room_no'},
                {data: 'leave_date', name: 'leave_date'},
                {data: 'student_transfer', name: 'student_transfer'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "1%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            // var class_id = $("#class_id").val();
            // var section_id = $("#section_id").val();
            // if(class_id == "" && section_id == ""){
            //     $("#list-block").hide();
            //     $('#searchError').html('Please select class');
            //     $("#searchError").show();
            // } else {
            //     $("#list-block").show();
            //     $('#searchError').html('');
            //     $("#searchError").hide();
            // }
            e.preventDefault();
        });
       
    });
    
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    $(document).on('click','.transfered',function(e){
        $("#error-block").hide();
        $("#success-block").hide();
        var student_id = $(this).attr('student-id');
        var h_student_map_id = $(this).attr('hostel-map');
        var h_room_id = $(this).attr('room-id');
        $('#student_id').val(student_id);
        $('#h_student_map_id').val(h_student_map_id);
        if(h_room_id != ''){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-room-availability')}}",
                type: 'GET',
                data: {
                    'h_room_id': h_room_id,
                    'h_student_map_id': h_student_map_id,
                },
                success: function (data) {
                    data = $.trim(data);
                    var res = JSON.parse(data);
                    $("#room_exist_info").show();
                    $("#hostel_block_floor").html(res.hostel_block_floor);
                    $("#room_category").html(res.room_category);
                    $("#room_capacity_").html(res.room_capacity);
                    $("#room_capacity").html(res.room_capacity);
                    $("#room_availability").html(res.room_available);
                    $("#fees_head").html(res.h_fees_heads);
                }
            });
        }
        $('#allocateModel').modal('show');
       // $('.select2').select2();
    })
    function getBlock(hostel_id)
    {
        if(hostel_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-block-data')}}",
                type: 'GET',
                data: {
                    'hostel_id': hostel_id
                },
                success: function (data) {
                    $("select[name='h_block_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_block_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function getFloor(h_block_id)
    {
        if(h_block_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-floor-data')}}",
                type: 'GET',
                data: {
                    'h_block_id': h_block_id
                },
                success: function (data) {
                    $("select[name='h_floor_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_floor_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getRoom(h_floor_id)
    {
        if(h_floor_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-room-data')}}",
                type: 'GET',
                data: {
                    'h_floor_id': h_floor_id
                },
                success: function (data) {
                    $("select[name='h_room_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_room_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getCateRoomInfo(h_room_cate_id)
    {
        var hostel_id  = $('#hostel_id').val();
        var h_block_id = $('#h_block_id').val();
        var h_floor_id = $('#h_floor_id').val();
        console.log(hostel_id);
        console.log(h_block_id);
        console.log(h_floor_id);
        if(h_room_cate_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-category-room-data')}}",
                type: 'GET',
                data: {
                    'hostel_id':hostel_id,
                    'h_block_id': h_block_id,
                    'h_floor_id': h_floor_id,
                    'h_room_cate_id': h_room_cate_id
                },
                success: function (data) {
                    $("select[name='h_room_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='h_room_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getRoomInfo(){
        var h_room_id = $('#h_room_id').val();
        var hostel_id = $('#hostel_id').val();
        if(h_room_id != '' ) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-room-availability')}}",
                type: 'GET',
                data: {
                    'h_room_id': h_room_id,
                },
                success: function (data) {
                    var res = JSON.parse(data);
                    $("#room_info").show();
                    $("#room_capacity").html(res.room_capacity);
                    $("#room_availability").html(res.room_available);
                    $(".mycustloading").hide();
                    var table1 = $('#student-list').DataTable({
                            //dom: 'Blfrtip',
                            destroy: true,
                            pageLength: 30,
                            processing: true,
                            serverSide: true,
                            bLengthChange: false,
                            searching: false,
                            paging: false,
                            info: false,
                            // buttons: [
                            //     'copy', 'csv', 'excel', 'pdf', 'print'
                            // ],
                            ajax: {
                                url: '{{url('admin-panel/hostel/allocation/get-student-list')}}',
                                data: function (d) {
                                    d.h_room_id = h_room_id;
                                    d.hostel_id = hostel_id;
                                    d.status = 'exist';
                                }
                            },
                            columns: [
                                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                                {data: 'student_enroll_number', name: 'student_enroll_number'},
                                {data: 'student_profile', name: 'student_profile'},
                                {data: 'class_section', name: 'class_section'},
                            ],
                            columnDefs: [
                            
                                {
                                    targets: [ 0, 1, 2 ],
                                    className: 'mdl-data-table__cell--non-numeric'
                                }
                            ]
                        });
                    if(res.room_available == 0){
                        $("#error-block").html('Sorry no space available in this room. !! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                        $("#error-block").show();
                        $('#list-submit').prop('disabled', true);
                    } else {
                        $("#error-block").html('');
                        $("#error-block").hide();
                        $('#list-submit').prop('disabled', false);
                        
                    }
                    
                }
            });
        } else {
            $(".mycustloading").hide();
            $("#room_info").hide();
        }
    }

    $('#list-form').on('submit', function(e) { 
        var hostel_id = $('#hostel_id').val();
        var h_block_id = $('#h_block_id').val();
        var h_floor_id = $('#h_floor_id').val();
        var h_room_id = $('#h_room_id').val();
        var join_date = $('#join_date').val();
        var leave_date = $('#leave_date').val();
        var h_student_map_id = $('#h_student_map_id').val();
        var student_id = $('#student_id').val();
        if(hostel_id != '' && h_block_id != '' && h_floor_id != '' && h_room_id != '' && join_date != '' && leave_date != '') {
            var formData = $(this).serialize();
            console.log();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{url('admin-panel/hostel/allocation/allocate')}}",
                data     : formData,
                cache    : false,

                success  : function(data) {
                    data = $.trim(data);
                    if(data == "success"){
                        $("#success-block").html('Allocation successfully complete !! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                        $("#success-block").show();
                        $("#error-block").hide();

                    } else {
                        $("#error-block").html("No record found !!");
                        $("#error-block").show();
                        $("#success-block").hide();
                    }
                }
            })
        } else {
            $("#error-block").html('Required field missing !! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
            $("#error-block").show();
        }
        e.preventDefault();
    });

    function getFeesHead(hostel_id)
    {
        if(hostel_id != "") {
            $(".mycustloading").show();
             $("#show-fees-head").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/get-hostel-fees-head-data')}}",
                type: 'GET',
                data: {
                    'hostel_id': hostel_id
                },
                success: function (data) {
                    console.log(data);
                    $("#show-fees-head").html(data);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#show-fees-head").html('');
            $(".mycustloading").hide();
        }
    }

</script>
@endsection

