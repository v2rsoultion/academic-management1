@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nav_item7
    {
    min-height: 230px !important;
    }
    .imgclass:hover .active123 .tab_images123{
    left: 60px !important;
	}
	.tab_images123{
		left: 60px !important;
	}
    .imgclass:hover .tab_images123{
    left: 60px !important;
    }
    .nav_item3 {
    min-height: 255px !important;
    }
    .side-gap{
    width: 75% !important;
    }
    .tab1{
        padding-left: 5px !important;
    }
    .tab2{
        padding-right: 3px !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Profile</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.parent_information') !!}</a></li>
                    <li class="breadcrumb-item">Profile</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card profile_card1">
                    <div class="body text-center pro_body_center1 form-gap ">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 text-left">
								@php $student_image = 'public/images/default.png'; @endphp
                                <div class="profile-image"> <img src="{!! URL::to($student_image) !!}" alt="" class="profile_image"> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std1">
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_father_name') !!}: </span> {!! $parent['student_father_name']; !!}</label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_father_mobile_number') !!}: </span> {!! $parent['student_father_mobile_number']; !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_father_email') !!}: </span> {!! $parent['student_father_email']; !!}</label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_father_occupation') !!}: </span> {!! $parent['student_father_occupation']; !!} </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std2">
									<div class="profile_detail_3">
                                        <label> <span>Login {!! trans('language.student_login_name') !!}: </span> {!! $parent['student_login_name']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>Login {!! trans('language.student_login_email') !!}: </span> {!! $parent['student_login_email']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>Login {!! trans('language.student_login_contact_no') !!}: </span> {!! $parent['student_login_contact_no']; !!} </label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="pull-left" style="width: 24%; margin-left: 2%">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card ">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#father_info" onClick="toggleBlock('father_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/personal-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Father Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
								<li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#mother_info" onClick="toggleBlock('mother_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/parent-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Mother Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#guardian_info" onClick="toggleBlock('guardian_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Guardian Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#child_info" onClick="toggleBlock('child_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Child Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right" style="width: 72%;">
                <div class="">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="father_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Father Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_father_name') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_father_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_father_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_email') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_father_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_occupation') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_father_occupation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_annual_salary') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_father_annual_salary']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="mother_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Father Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_mother_name') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_email') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_occupation') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_occupation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_annual_salary') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_annual_salary']; !!}</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_tongue') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_mother_tongue']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="guardian_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Guardian Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_guardian_name') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_guardian_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_email') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_guardian_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_guardian_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_relation') !!} :</b> <span class="profile_detail_span_6">{!! $parent['student_guardian_relation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="child_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    @foreach($parent['child_info'] as $child_info)
                                                                    <div class="nav_item7">
                                                                        
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> {!! $child_info['student_name'] !!} Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_name') !!} :</b> <span class="profile_detail_span_6">{!! $child_info['student_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_roll_no') !!} :</b> <span class="profile_detail_span_6">{!! $child_info['student_roll_no'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_enroll_number') !!} :</b> <span class="profile_detail_span_6">{!! $child_info['student_enroll_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_class_section') !!} :</b> <span class="profile_detail_span_6">{!! $child_info['student_class_section']; !!}</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.medium_type') !!} :</b> <span class="profile_detail_span_6">{!! $child_info['student_medium']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">Profile :</b> <span class="profile_detail_span_6"><a target="_blank" href="{!! url('admin-panel/student/student-profile/'.$child_info['encrypted_student_id']) !!}" >View Profile</a></span>
                                                                                    </div>
                                                                                </div>
																				
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    @endforeach
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       

                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
@endsection
<script>
    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }
</script>