@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nav_item7
    {
    min-height: 230px !important;
    }
    .imgclass:hover .active123 .tab_images123{
    left: 60px !important;
	}
	.tab_images123{
		left: 60px !important;
	}
    .imgclass:hover .tab_images123{
    left: 60px !important;
    }
    .nav_item3 {
    min-height: 255px !important;
    }
    .side-gap{
    width: 75% !important;
    }
    .tab1{
        padding-left: 5px !important;
    }
    .tab2{
        padding-right: 3px !important;
    }

    .fc-bg tr td:nth-child(5) {
    background: white !important;
    color: white !important;
}
.fc-bg tr td:nth-child(3) {
    background: white !important;
    color: white !important;
}
.fc-bg tr td:nth-child(1) {
    background: white !important;
    color: white !important;
}
.fc-row.fc-week.fc-widget-content.fc-rigid{
    height: 90px !important;
}
.fc-scroller.fc-day-grid-container{
    height: 540px !important;
}
.fc-unthemed td.fc-today {
    background: white;
}
.attendence_green{
    background-color: green !important; 
    color:white !important;
    width: 80% !important;
}
.attendence_red{
    background-color: red !important; 
    color:white !important;
    width: 80% !important;
}

</style>

<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Profile</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/view-profile') !!}">{!! trans('language.profile') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card profile_card1">
                    <div class="body text-center pro_body_center1 form-gap ">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-12 text-left">
								@php $student_image = 'public/images/default.png'; @endphp
								@if($student['student_image'] != '')
									@php $student_image = $student['student_image']; @endphp
								@endif
                                <div class="profile-image"> <img src="{!! URL::to($student_image) !!}" alt="" class="profile_image"> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std1">
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_roll_no') !!}: </span> {!! $student['student_roll_no']; !!}</label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_enroll_number') !!}: </span> {!! $student['student_enroll_number']; !!}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_name') !!}: </span> {!! $student['title_name']; !!} {!! $student['student_name']; !!}</label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.class_section') !!}: </span> {!! $student['class_section']; !!} </label>
                                    </div>
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_email') !!}: </span> {!! $student['student_email']; !!} </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="std2">
									<div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_reg_date') !!}: </span> {!! date("d M Y", strtotime($student['student_reg_date'])); !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_dob') !!}: </span> {!! date("d M Y", strtotime($student['student_dob'])); !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.student_gender') !!}: </span> {!! $student['student_gender']; !!} </label>
                                    </div>
                                    <div class="profile_detail_3">
                                        <label> <span>{!! trans('language.status') !!}: </span> {!! $student['status']; !!} </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="pull-left " style="width: 24%; margin-left: 2%;">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card right_scroll" style="height: 400px;overflow: scroll;overflow-x: hidden;">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#personal" onClick="toggleBlock('personal');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/personal-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Personal Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
								<li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#parent_info" onClick="toggleBlock('parent_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/parent-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Parent Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#academic_info" onClick="toggleBlock('academic_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Academic Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#address_info" onClick="toggleBlock('address_info');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/address-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Address Information </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#documents" onClick="toggleBlock('documents');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/documents.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Documents </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#attendance" onClick="toggleBlock('attendance');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/attendence.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Attendance </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#leave" onClick="toggleBlock('leave');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/leave-management.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Leave Application </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#fees" onClick="toggleBlock('fees');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/fees.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Fees </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#marks" onClick="toggleBlock('marks');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/marks.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Marks </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123" data-toggle="tab" href="#remark" onClick="toggleBlock('remark');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/remarks.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Remarks </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass" id="">
                                    <a class="nav-link active123" data-toggle="tab" href="#certificate"  onClick="toggleBlock('certificate');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/issue-certificate.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Issued Certificate </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right" style="width: 72%;">
                <div class="">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="personal" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Basic Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_enroll_number') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_enroll_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['title_name'] !!} {!! $student['student_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_type') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_type1'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_reg_date') !!} :</b> <span class="profile_detail_span_6">{!! date("d M Y", strtotime($student['student_reg_date'])); !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_dob') !!} :</b> <span class="profile_detail_span_6">{!! date("d M Y", strtotime($student['student_dob'])); !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_gender') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_gender']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_email') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_email']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Other Info </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_category') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_category']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.caste_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['caste_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.religion_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['religion_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.nationality_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['nationality_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<!-- <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_sibling_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_sibling_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_sibling_class_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_sibling_class_name']; !!}</span>
                                                                                    </div>
                                                                                </div> -->
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_adhar_card_number') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_adhar_card_number']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>

																	 <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Health Info </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_height') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_height']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_weight') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_weight']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_blood_group') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_blood_group']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_vision_left') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_vision_left']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_vision_right') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_vision_right']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.medical_issues_p') !!} :</b> <span class="profile_detail_span_6">{!! $student['medical_issues']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

						<div class="tab-pane body" id="parent_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Father Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_father_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_father_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_father_mobile_number'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_email') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_father_email'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_occupation') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_father_occupation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_father_annual_salary') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_father_annual_salary']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Mother Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_mobile_number']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_email') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_email']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_occupation') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_occupation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_tongue') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_tongue']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_mother_annual_salary') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_mother_annual_salary']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>

																	 <div class="nav_item3">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Guardian Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_guardian_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_email') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_guardian_email']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_mobile_number') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_guardian_mobile_number']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_guardian_relation') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_guardian_relation']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				
																				
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="address_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Address Information </a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                            <h6>Permanent Address :</h6>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_permanent_address') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_permanent_address'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_permanent_city') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_permanent_city_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_permanent_state') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_permanent_state_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_permanent_county') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_permanent_county_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_permanent_pincode') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_permanent_pincode']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div class="clearfix"></div>
                                                                                <hr />
                                                                                <h6>Temporary Address :</h6>
                                                                                <div class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_temporary_address') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_temporary_address'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_temporary_city') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_temporary_city_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_temporary_state') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_temporary_state_name'] !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_temporary_county') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_temporary_county_name']; !!}</span>
                                                                                    </div>
                                                                                </div>
																				<div  class="profile_detail_2">
                                                                                    <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                        <b class="profile_detail_bold_2">{!! trans('language.student_temporary_pincode') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_temporary_pincode']; !!}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                
                                                                                
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="academic_info" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="">
                                                    <div class="card border_info_tabs_1 card_top_border1">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="card">
                                                                    <div class="body">
                                                                        <div class="nav_item3">
                                                                            <div class="nav_item1">
                                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Academic Current Information </a>
                                                                            </div>
                                                                            <div class="nav_item2">
                                                                                <div role="tabpanel" class="tab-pane in active" id="academic_current_info">
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.admission_session_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['admission_session'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.admission_class_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['admission_class'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.admission_section_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['admission_section'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.current_session_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['current_session'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.current_class_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['current_class'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.current_section_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['current_section'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.student_unique_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_unique_id'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.group_id') !!} :</b> <span class="profile_detail_span_6">{!! $student['student_group_name'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="profile_detail_2">
                                                                                        <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.stream_name') !!} :</b> <span class="profile_detail_span_6">{!! $student['stream_name'] !!}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="nav_item3">
                                                                            <div class="nav_item1">
                                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Academic History Info </a>
                                                                            </div>
                                                                            <div class="nav_item2">
                                                                                <div role="tabpanel" class="tab-pane" id="academic_history_info">
                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="documents" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Documents</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                            <div class="table-responsive">
                                                                                <table class="table m-b-0 c_list" id="document-table" style="width:100%">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="width: 8%">{{trans('language.s_no')}}</th>
                                                                                            <th style="width: 20%">{{trans('language.student_document_category')}}</th>
                                                                                            <th>{{trans('language.student_document_details')}}</th>
                                                                                            <th style="width: 15%">{{trans('language.student_document_file')}}</th>
                                                                                            <th style="width: 15%">{{trans('language.submit_date')}}</th>
                                                                                            <th style="width: 8%">Status </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        @if($student['documents'] != '')
                                                                                            @php $documentKey = 0; @endphp
                                                                                            @foreach($student['documents'] as $documents )
                                                                                            @php $documentKey++; @endphp
                                                                                            <tr>
                                                                                                <td>{!! $documentKey !!}</td>
                                                                                                <td>{!! $documents['document_category_name'] !!}</td>
                                                                                                <td>{!! $documents['student_document_details'] !!}</td>
                                                                                                @php 
                                                                                                $student_document_file = '';
                                                                                                $status = 'Pending';
                                                                                                @endphp
                                                                                                @if($documents['student_document_file'] != '')
                                                                                                    @php
                                                                                                        $student_document_file = $documents['student_document_file'];
                                                                                                        $status = 'Uploaded';
                                                                                                    @endphp
                                                                                                @endif
                                                                                                <td>
                                                                                                    @if($student_document_file != '')
                                                                                                    <a target="_blank" href="{!! URL::to($student_document_file) !!}">Attachment</a>
                                                                                                    @else 
                                                                                                        ------
                                                                                                    @endif
                                                                                                </td>
                                                                                                <td>{!! $documents['document_submit_date'] !!}</td>
                                                                                                <td>{!! $status !!}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </tbody>
                                                                                </table>
                                                                                </div>
                                                                                
																				<div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="leave" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Leave Application</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                            <div class="table-responsive">
                                                                                <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="width: 8%">{{trans('language.s_no')}}</th>
                                                                                            <th style="width: 20%">{{trans('language.leave_app_reason')}}</th>
                                                                                            <th>{{trans('language.leave_date_range')}}</th>
                                                                                            <th style="width: 15%">{{trans('language.leave_app_attachment')}}</th>
                                                                                            <th style="width: 15%">{{trans('language.leave_days')}}</th>
                                                                                            <th style="width: 8%">Status </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        @if($student['leaves'] != '')
                                                                                            @php $leaveKey = 0; @endphp
                                                                                            @foreach($student['leaves'] as $leaves )
                                                                                            @php $leaveKey++; @endphp
                                                                                            <tr>
                                                                                                <td>{!! $leaveKey !!}</td>
                                                                                                <td>{!! $leaves['student_leave_reason'] !!}</td>
                                                                                                <td>{!! $leaves['student_leave_from_date'] !!} - {!! $leaves['student_leave_to_date'] !!}</td>

                                                                                                @php 
                                                                                                $student_leave_attachment = '';
                                                                                                $status = 'Pending';
                                                                                                @endphp
                                                                                                @if($leaves['student_leave_attachment'] != '')
                                                                                                    @php
                                                                                                        $student_leave_attachment = $leaves['student_leave_attachment'];
                                                                                                        $status = 'Uploaded';
                                                                                                    @endphp
                                                                                                @endif
                                                                                                <td>
                                                                                                    @if($student_leave_attachment != '')
                                                                                                    <a target="_blank" href="{!! URL::to($student_leave_attachment) !!}">Attachment</a>
                                                                                                    @else 
                                                                                                        ------
                                                                                                    @endif
                                                                                                </td>
                                                                                                <td>{!! $leaves['student_leave_days'] !!}</td>

                                                                                               
                                                                                                <td>{!! $leaves['leave_status'] !!}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                                
																				<div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="attendance" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content" style="background-color: #fff;">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="card border_info_tabs_1 card_top_border1">
                                                    <div class="body">
                                                        <div class="nav_item5">
                                                            <div class="nav_item1">
                                                                <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                                            </div>
                                                            <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                                                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                                                            <div id="student_attendence_calendar" class="m-t-15"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane body" id="fees" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Fees Summary</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                            <div class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.total_fees') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['total_fees'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.paid_amt') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['paid_fees'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.due_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['due_fees'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.consession_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['concession_amt'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.fine_amt') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['fine_fees'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.wallet_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['wallet_amt'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div  class="profile_detail_2">
                                                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                                                    <b class="profile_detail_bold_2">{!! trans('language.imprest_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $student_fees_info['fees_summary']['imprest_amt'] !!}</span>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="nav_item1">
                                                                        <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Fees Upcoming</a>
                                                                    </div>
                                                                    <div class="nav_item2">
                                                                        <div role="tabpanel" class="tab-pane" id="health_with_icon_title">

                                                                        @foreach($student_fees_info['upcoming_fees'] as $upcoming_fees)
                                                                        <h6> {{ $upcoming_fees['fees_name'] }} </h6>
                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.fee_date') !!} :</b> <span class="profile_detail_span_6">{!! date("d M Y", strtotime($upcoming_fees['effective_date'])) !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.head_type') !!} :</b> <span class="profile_detail_span_6">{!! $upcoming_fees['head_type'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.installment') !!} :</b> <span class="profile_detail_span_6">{!! $upcoming_fees['head_name'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.fee_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $upcoming_fees['head_amt'] !!} </span>
                                                                            </div>
                                                                        </div>


                                                                        <div class="clearfix"></div>
                                                                        <hr/>
                                                                        @endforeach   
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>

                                                                    
                                                                    <div class="nav_item1">
                                                                        <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Fees Paid</a>
                                                                    </div>
                                                                    <div class="nav_item2">
                                                                        <div role="tabpanel" class="tab-pane" id="health_with_icon_title">

                                                                        @foreach($student_fees_info['paid_fees'] as $paid_fees)
                                                                        <h6> {{ $paid_fees['fees_name'] }} </h6>
                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.receipt_date') !!} :</b> <span class="profile_detail_span_6">{!! date("d M Y", strtotime($paid_fees['receipt_date'])) !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.head_type') !!} :</b> <span class="profile_detail_span_6">{!! $paid_fees['head_type'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.installment') !!} :</b> <span class="profile_detail_span_6">{!! $paid_fees['head_name'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.fee_amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $paid_fees['total_amount'] !!} </span>
                                                                            </div>
                                                                        </div>
                                                                        @if($paid_fees['total_calculate_fine_amount'] != "")

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.fine_amt') !!} :</b> <span class="profile_detail_span_6">₹ {!! $paid_fees['total_calculate_fine_amount'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        @endif

                                                                        <div class="clearfix"></div>
                                                                        <hr/>
                                                                        @endforeach   
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>


                                                                    <div class="nav_item1">
                                                                        <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Fees Due</a>
                                                                    </div>
                                                                    <div class="nav_item2">
                                                                        <div role="tabpanel" class="tab-pane" id="health_with_icon_title">

                                                                        @foreach($student_fees_info['due_fees'] as $due_fees)
                                                                        <h6> {{ $due_fees['fees_name'] }} </h6>
                                                                        

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.receipt_date') !!} :</b> <span class="profile_detail_span_6">{!! date("d M Y", strtotime($due_fees['receipt_date'])) !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.head_type') !!} :</b> <span class="profile_detail_span_6">{!! $due_fees['head_type'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.installment') !!} :</b> <span class="profile_detail_span_6">{!! $due_fees['head_name'] !!} </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="profile_detail_2">
                                                                            <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">{!! trans('language.amount') !!} :</b> <span class="profile_detail_span_6">₹ {!! $due_fees['pay_later_amount'] !!} </span>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <div class="clearfix"></div>
                                                                        <hr/>
                                                                        @endforeach   
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="marks" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Marks Information</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="table-responsive">
                                                                                    <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th style="width: 8%;">{{trans('language.s_no')}}</th>
                                                                                                <th style="width: 15%;">{!! trans('language.terms_name') !!}</th>
                                                                                                <th style="width: 15%;">{!! trans('language.exam_name') !!}</th>
                                                                                                <th style="width: 20%;">{{trans('language.schedule_date_range')}}</th>
                                                                                                <th style="width: 20%;">{{trans('language.view_exam_schedules')}} </th>
                                                                                                <th style="width: 15%;">{{trans('language.my_marks')}}</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            @if($exam_marks != '')
                                                                                                @php $exam_marksKey = 0; @endphp
                                                                                                @foreach($exam_marks as $exam_marks )
                                                                                                @php $exam_marksKey++; @endphp
                                                                                                <tr>
                                                                                                    <td>{!! $exam_marksKey !!}</td>
                                                                                                    <td>{!! $exam_marks['get_exam']['get_term']['term_exam_name'] !!}</td>
                                                                                                    <td>{!! $exam_marks['get_exam']['exam_name'] !!}</td>
                                                                                                    <td>{!! date("d M Y", strtotime($exam_marks['start_date'])).' - '.date("d M Y", strtotime($exam_marks['end_date']))  !!}</td>

                                                                                                    <td>  
                                                                                                        <button type="button" class="btn btn-raised btn-primary subjects" exam-id="{!! $exam_marks['exam_id'] !!}"  exam-schedule-id="{!! $exam_marks['exam_schedule_id'] !!}"  class-id="{!! $exam_marks['class_id'] !!}" section-id="{!! $exam_marks['section_id'] !!}" > Schedule </button>
                                                                                                    </td>

                                                                                                    <td>
                                                                                                        <button type="button" class="btn btn-raised btn-primary marks" exam-id="{!! $exam_marks['exam_id'] !!}"  exam-schedule-id="{!! $exam_marks['exam_schedule_id'] !!}"  class-id="{!! $exam_marks['class_id'] !!}" section-id="{!! $exam_marks['section_id'] !!}" > Marks </button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>    
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="remark" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Remarks Information</a>
                                                                        </div>

                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                                <div class="table-responsive">
                                                                                    <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th style="width: 8%;">{{trans('language.s_no')}}</th>
                                                                                                <th style="width: 15%;">{{trans('language.remark_date')}}</th> 
                                                                                                <th style="width: 15%;">{{trans('language.subject')}}</th>
                                                                                                <th style="width: 15%;">{{trans('language.student_teacher')}}</th>
                                                                                                <th style="width: 15%;">{{trans('language.remark_description')}}</th> 
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            @if($student['remarks'] != '')
                                                                                                @php $remarkKey = 0; @endphp
                                                                                                @foreach($student['remarks'] as $remarks )
                                                                                                @php $remarkKey++; @endphp
                                                                                                <tr>
                                                                                                    <td>{!! $remarkKey !!}</td>
                                                                                                    <td>{!! $remarks['remark_date'] !!}</td>
                                                                                                    <td>{!! $remarks['subject_name'] !!}</td>
                                                                                                    <td>{!! $remarks['teacher_name'] !!}</td>

                                                                                                    <td><button type="button" class="btn btn-raised btn-primary remarks"  remark-id="{!! $remarks['remark_id'] !!}" > Remark </button></td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>    
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="certificate" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Certificate Information</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                                            
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="remarkModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Remark Info </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
            <div id="remark_block" style="width:100%;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subjectsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
           
            <table class="table m-b-0 c_list" id="subjects-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.exam_date') !!}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.schedule_time_range') !!}</th>
                        <th>{!! trans('language.room_no') !!}</th>
                    </tr>
                </thead>
            </table>
            
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="marksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Marks Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
           
            <table class="table m-b-0 c_list" id="marks-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.total_marks') !!}</th>
                        <th>{!! trans('language.obtain_marks') !!}</th>
                        <th>{!! trans('language.grades') !!}</th>
                    </tr>
                </thead>
            </table>
            
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  
  $(document).on('click','.remarks',function(e){
        var remark_id = $(this).attr('remark-id');
        if(remark_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/daily-remarks/remark-student-single-data')}}",
                type: 'GET',
                data: {
                    'remark_id': remark_id
                },
                success: function (data) {
                    $("#remark_block").html(data);
                    $(".mycustloading").hide();
                    $("#remarkModel").modal('show');
                  
                }
            });
        }
        
    })


    $(document).on('click','.subjects',function(e){
        var exam_id = $(this).attr('exam-id');
        var exam_schedule_id = $(this).attr('exam-schedule-id');
        var class_id = $(this).attr('class-id');
        var section_id = $(this).attr('section-id');
        $('#subjectsModel').modal('show');
        var table1 = $('#subjects-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-schedule-subjects-student')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'exam_date', name: 'exam_date'},
                {data: 'subject_name', name: 'subject_name'},
                {data: 'time_range', name: 'time_range'},
                {data: 'room_no', name: 'room_no'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    })



    $(document).on('click','.marks',function(e){
        var exam_id = $(this).attr('exam-id');
        var exam_schedule_id = $(this).attr('exam-schedule-id');
        var class_id = $(this).attr('class-id');
        var section_id = $(this).attr('section-id');

        $('#marksModel').modal('show');
        var table1 = $('#marks-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-marks-student')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'subject_name', name: 'subject_name'},
                {data: 'marks', name: 'marks'},
                {data: 'max_marks', name: 'max_marks'},
                {data: 'grade_name', name: 'grade_name'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    })


  $(document).ready(function() {

    $('#student_attendence_calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
    //    defaultDate: '2018-01-12',
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function() {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        eventLimit: true, // allow "more" link when too many events
        events: [
            @foreach($student['attendence'] as $arr_student_lists)
                {
                    title: "{{$arr_student_lists['student_attendance_type']}}",
                    start: "{{$arr_student_lists['student_attendance_date']}}",
                    className: "{{$arr_student_lists['attendence_class']}}"
                },
            @endforeach()    
        ]
    });


    // Change to month view
    $('#change-view-month').on('click',function(){
        $('#student_attendence_calendar').fullCalendar('changeView', 'month');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to week view
    $('#change-view-week').on('click',function(){
        $('#student_attendence_calendar').fullCalendar( 'changeView', 'agendaWeek');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to day view
    $('#change-view-day').on('click',function(){
        $('#student_attendence_calendar').fullCalendar( 'changeView','agendaDay');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to today view
    $('#change-view-today').on('click',function(){
        $('#student_attendence_calendar').fullCalendar('today');
    });

  });
</script>


<!-- Content end here  -->
@endsection
<script>
    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }
</script>


