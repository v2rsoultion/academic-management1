@extends('admin-panel.layout.header')
@section('content')

<style type="text/css">

.fc-bg tr td:nth-child(5) {
    background: white !important;
    color: white !important;
}
.fc-bg tr td:nth-child(3) {
    background: white !important;
    color: white !important;
}
.fc-bg tr td:nth-child(1) {
    background: white !important;
    color: white !important;
}
.fc-row.fc-week.fc-widget-content.fc-rigid{
    height: 90px !important;
}
.fc-scroller.fc-day-grid-container{
    height: 540px !important;
}
.fc-unthemed td.fc-today {
    background: white;
}
.attendence_green{
    background-color: green !important; 
    color:white !important;
    width: 80% !important;
}
.attendence_red{
    background-color: red !important; 
    color:white !important;
    width: 80% !important;
}
</style>

<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! trans('language.my_attendance') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.my_attendance') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="tab-pane body" id="attendance">
        <div class="">
            <div class="">
                <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                    <div class="tab-content" style="background-color: #fff;">
                        <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                    <div class="nav_item5">
                                        <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                        </div>
                                        <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                                        <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                                        <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                                        <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                                        <div id="student_attendence_calendar" class="m-t-15"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

<script type="text/javascript">
  
  $(document).ready(function() {

    $('#student_attendence_calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
    //    defaultDate: '2018-01-12',
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function() {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        eventLimit: true, // allow "more" link when too many events
        events: [
            @foreach($arr_student_list as $arr_student_lists)
                {
                    title: "{{$arr_student_lists['student_attendance_type']}}",
                    start: "{{$arr_student_lists['student_attendance_date']}}",
                    className: "{{$arr_student_lists['attendence_class']}}"
                },
            @endforeach()    
        ]
    });


    // Change to month view
    $('#change-view-month').on('click',function(){
        $('#student_attendence_calendar').fullCalendar('changeView', 'month');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to week view
    $('#change-view-week').on('click',function(){
        $('#student_attendence_calendar').fullCalendar( 'changeView', 'agendaWeek');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to day view
    $('#change-view-day').on('click',function(){
        $('#student_attendence_calendar').fullCalendar( 'changeView','agendaDay');

        // safari fix
        $('#content .main').fadeOut(0, function() {
            setTimeout( function() {
                $('#content .main').css({'display':'table'});
            }, 0);
        });

    });

    // Change to today view
    $('#change-view-today').on('click',function(){
        $('#student_attendence_calendar').fullCalendar('today');
    });

  });
</script>

@endsection




