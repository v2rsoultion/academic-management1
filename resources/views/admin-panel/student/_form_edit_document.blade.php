@if(isset($document['student_document_id']) && !empty($document['student_document_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
        width: 100%;
    }
</style>

{!! Form::hidden('student_document_id',old('student_document_id',isset($document['student_document_id']) ? $document['student_document_id'] : ''),['class' => 'gui-input', 'id' => 'document_id', 'readonly' => 'true']) !!}

<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>
<div id="sectiontable-body">
    <div id="document_block">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.student_document_category') !!} :</lable>
                <label class=" field select" style="width: 100%">
                    {!!Form::select('student_document_category', $document['arr_document_category'],$document['document_category_id'], ['class' => 'form-control show-tick select_form1','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.stud_leave_app_attachment') !!} :</lable>
                <div class="form-group">
                    <label for="file1" class="field file">
                        <input type="file" class="gui-file" name="documents" id="student_document_file" >
                        <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
                    </label>
                </div>
                @if ($errors->has('documents')) <p class="help-block">{{ $errors->first('documents') }}</p> @endif
            </div>

            <div class="col-lg-4 col-md-4 leavedocumentpadding">
                        @php
                        $config_document_upload_path   = \Config::get('custom.student_document_file');
                        $doc_path = $config_document_upload_path['display_path'].$document['student_document_file'];
                        @endphp                 
                        <a href="{{url($doc_path)}}" target="_blank">View Document</a>
            </div>

            <div class="col-lg-12 col-md-12">
                <lable class="from_one1">{!! trans('language.student_document_details') !!} :</lable>
                <div class="form-group">
                    <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
                    {!! Form::textarea('student_document_details',old('student_document_details',isset($document['student_document_details']) ? $document['student_document_details']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.student_document_details'),'rows' => 3, 'cols' => 50)) !!}
                </div>
                @if ($errors->has('student_document_details')) <p class="help-block">{{ $errors->first('student_document_details') }}</p> @endif
            </div>
        </div>
    </div>
    
    
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("extension", function(a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif|pdf", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only pdf,jpg,png format allowed."));

        $("#edit-document-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_document_category: {
                    required: true
                },
                documents: {
                    required: true,
                    extension: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },


            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#student_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#student_reg_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        
    });
</script>