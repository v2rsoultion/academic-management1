
{!! Form::hidden('student_id',old('student_id',isset($student['student_id']) ? $student['student_id'] : ''),['class' => 'gui-input', 'id' => 'student_id', 'readonly' => 'true']) !!}
<div class="alert alert-danger" role="alert" id="error-block" style="display: none">
   
</div>
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<!-- Guardian Information -->
<div class="header">
    <h2><strong>Parent Login</strong> Credentials</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_contact_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_contact_no', old('student_login_contact_no',isset($student['student_login_contact_no']) ? $student['student_login_contact_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_contact_no'), 'id' => 'student_login_contact_no', 'maxlength'=> '10', 'onkeyup' => "check_parent_existance()"]) !!}

            {!! Form::text('student_parent_id','',['id' => 'student_parent_id']) !!}
        </div>
        
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_name', old('student_login_name',isset($student['student_login_name']) ? $student['student_login_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_name'), 'id' => 'student_login_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_login_email', old('student_login_email',isset($student['student_login_email']) ? $student['student_login_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_email'), 'id' => 'student_login_email']) !!}
        </div>
    </div>

</div>

<!-- Father Information -->
<div class="header">
    <h2><strong>Father</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_name', old('student_father_name',isset($student['student_father_name']) ? $student['student_father_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_name'), 'id' => 'student_father_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_mobile_number', old('student_father_mobile_number',isset($student['student_father_mobile_number']) ? $student['student_father_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_mobile_number'), 'id' => 'student_father_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_email') !!}   :</lable>
        <div class="form-group">
            {!! Form::text('student_father_email', old('student_father_email',isset($student['student_father_email']) ? $student['student_father_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_email'), 'id' => 'student_father_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_annual_salary') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_father_annual_salary', $student['arr_annual_salary'],isset($student['student_father_annual_salary']) ? $student['student_father_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_father_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_father_occupation') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_father_occupation', old('student_father_occupation',isset($student['student_father_occupation']) ? $student['student_father_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_occupation'), 'id' => 'student_father_occupation', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- Mother Information -->
<div class="header">
    <h2><strong>Mother</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_name', old('student_mother_name',isset($student['student_mother_name']) ? $student['student_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_name'), 'id' => 'student_mother_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_mobile_number', old('student_mother_mobile_number',isset($student['student_mother_mobile_number']) ? $student['student_mother_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_mobile_number'), 'id' => 'student_mother_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_email', old('student_mother_email',isset($student['student_mother_email']) ? $student['student_mother_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_email'), 'id' => 'student_mother_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_annual_salary') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_mother_annual_salary', $student['arr_annual_salary'],isset($student['student_mother_annual_salary']) ? $student['student_mother_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_mother_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_tongue') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_tongue', old('student_mother_tongue',isset($student['student_mother_tongue']) ? $student['student_mother_tongue']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_tongue'), 'id' => 'student_mother_tongue']) !!}
        </div>
    </div>

    <div class="col-lg-9 col-md-9 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_mother_occupation') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_mother_occupation', old('student_mother_occupation',isset($student['student_mother_occupation']) ? $student['student_mother_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_occupation'), 'id' => 'student_mother_occupation', 'rows'=> 2]) !!}
        </div>
    </div>

</div>

<!-- Guardian Information -->
<div class="header">
    <h2><strong>Guardian</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_name', old('student_guardian_name',isset($student['student_guardian_name']) ? $student['student_guardian_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_name'), 'id' => 'student_guardian_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_mobile_number', old('student_guardian_mobile_number',isset($student['student_guardian_mobile_number']) ? $student['student_guardian_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_mobile_number'), 'id' => 'student_guardian_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_email', old('student_guardian_email',isset($student['student_guardian_email']) ? $student['student_guardian_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_email'), 'id' => 'student_guardian_email']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_relation') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_relation', old('student_guardian_relation',isset($student['student_guardian_relation']) ? $student['student_guardian_relation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_relation'), 'id' => 'student_guardian_relation']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/student') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>
<script type="text/javascript">
   
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });;
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#parent-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                student_login_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_father_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_father_mobile_number: {
                    required: true,
                    digits: true
                },
                student_login_contact_no: {
                    required: true,
                    digits: true
                },
                student_father_email: {
                    email: true,
                    emailvalid: true
                },
                student_father_occupation: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_mobile_number: {
                    // required: true,
                    digits: true
                },
                student_guardian_name: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_sibling_name: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_guardian_mobile_number: {
                    digits: true
                },
                student_guardian_email: {
                    email: true,
                    emailvalid: true
                },
                student_mother_email: {
                    email: true,
                    emailvalid: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
    });

    function check_parent_existance(){
        var mobile_no = $("#student_login_contact_no").val().toString();
        if(mobile_no.length == 10){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-parent-existance')}}",
                type: 'GET',
                data: {
                    'mobile_no': mobile_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == 1){
                            $("#student_login_name").val(data.student_login_name);
                            $("#student_parent_id").val(data.student_parent_id);
                            $("#student_login_email").val(data.student_login_email);
                            // $("#student_login_contact_no").val(data.student_login_contact_no);
                            $("#student_father_mobile_number").val(data.student_father_mobile_number);
                            $("#student_father_name").val(data.student_father_name);
                            $("#student_father_email").val(data.student_father_email);
                            $("#student_father_occupation").val(data.student_father_occupation);
                            $("#student_father_annual_salary").val(data.student_father_annual_salary);
                            $("#student_mother_name").val(data.student_mother_name);
                            $("#student_mother_mobile_number").val(data.student_mother_mobile_number);
                            $("#student_mother_email").val(data.student_mother_email);
                            $("#student_mother_occupation").val(data.student_mother_occupation);

                            $("#student_mother_tongue").val(data.student_mother_tongue);
                            $("#student_guardian_name").val(data.student_guardian_name);
                            $("#student_guardian_email").val(data.student_guardian_email);
                            $("#student_guardian_relation").val(data.student_guardian_relation);
                            $("#student_guardian_mobile_number").val(data.student_guardian_mobile_number);
                            $("#student_mother_annual_salary").val(data.student_mother_annual_salary);
                            $("select[name='student_father_annual_salary'").selectpicker('refresh');
                            $("select[name='student_mother_annual_salary'").selectpicker('refresh');
                        }
                    }  else {
                            $("#student_login_name").val('');
                            $("#student_login_email").val('');
                            $("#student_parent_id").val('');
                            // $("#student_login_contact_no").val('');
                            $("#student_father_mobile_number").val('');
                            $("#student_father_name").val('');
                            $("#student_father_email").val('');
                            $("#student_father_occupation").val('');
                            $("#student_father_annual_salary").val('');
                            $("#student_mother_name").val('');
                            $("#student_mother_mobile_number").val('');
                            $("#student_mother_email").val('');
                            $("#student_mother_occupation").val('');

                            $("#student_mother_tongue").val('');
                            $("#student_guardian_name").val('');
                            $("#student_guardian_email").val('');
                            $("#student_guardian_mobile_number").val('');
                            $("#student_mother_annual_salary").val('');
                            $("select[name='student_father_annual_salary'").selectpicker('refresh');
                            $("select[name='student_mother_annual_salary'").selectpicker('refresh'); 
                        }
                }
            });
        }
    }

</script>

