@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.import_students') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
               
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.import_students') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'import-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <lable class="from_one1">{!! trans('language.class') !!} <span class="red-text">*</span> :</lable>
                                            <div class="form-group  ">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('class_id', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <lable class="from_one1">{!! trans('language.section') !!} <span class="red-text">*</span> :</lable>
                                            <div class="form-group  ">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('section_id', $listData['arr_section'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <lable class="from_one1">{!! trans('language.import_file') !!} <span class="red-text">*</span> :</lable>
                                            <div class="form-group">
                                                <label for="file1" class="field file">
                                                    <input type="file" class="gui-file" name="import_file" id="import_file" >
                                                </label>
                                            </div>
                                           <p>Please upload only xls file format.</p>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Import', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'import']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                               
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" style="margin-top: 30px;">
            <div class="pull-left" style="width: 24%; margin-left: 2%">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card ">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#medium_type" onClick="toggleBlock('medium_type');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/academic/medium.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Medium Type </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#academic_session" onClick="toggleBlock('academic_session');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Academic Year.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Academic Session </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#class_section" onClick="toggleBlock('class_section');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/academic/class.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Class Section </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#student_type" onClick="toggleBlock('student_type');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Student Type </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#category" onClick="toggleBlock('category');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Category </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#caste" onClick="toggleBlock('caste');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Caste.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Caste </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#religion" onClick="toggleBlock('religion');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Religion.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Religion </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#title" onClick="toggleBlock('title');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Title.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Title </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#annual_salary" onClick="toggleBlock('annual_salary');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/profile-images/academic-information.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Annual Salary </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#nationality" onClick="toggleBlock('nationality');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Nationality.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Nationality </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#group" onClick="toggleBlock('group');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/images/Configuration/School%20Group_House.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> School Group / House </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="pull-right" style="width: 72%;">
                <div class="">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="medium_type" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Medium Type</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.medium_type')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_mediums'] as $mediumKey => $mediums)
                                                                                        <tr>
                                                                                            <td>{{$mediumKey}}</td>
                                                                                            <td>{{$mediums}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="academic_session" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Academic Session</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.session')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_session'] as $sessionKey => $session)
                                                                                        <tr>
                                                                                            <td>{{$sessionKey}}</td>
                                                                                            <td>{{$session}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="class_section" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Class Section</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.class')}}</th>
                                                                                        <th>{{trans('language.section_id')}}</th>
                                                                                        <th>{{trans('language.section')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_sections'] as $sectionKey => $section)
                                                                                        <tr>
                                                                                            <td>{{$section['class_id']}}</td>
                                                                                            <td>{{$section['class_name']}}</td>
                                                                                            <td>{{$section['section_id']}}</td>
                                                                                            <td>{{$section['section_name']}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="student_type" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Student Type</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.student_type')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_student_type'] as $studentTypeKey => $studentType)
                                                                                        <tr>
                                                                                            <td>{{$studentTypeKey}}</td>
                                                                                            <td>{{$studentType}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="category" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Category</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.category')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_category'] as $categoryKey => $category)
                                                                                    
                                                                                        <tr>
                                                                                            <td>{{$categoryKey}}</td>
                                                                                            <td>{{$category}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="caste" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Caste</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.catse')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_caste'] as $casteKey => $catse)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$casteKey}}</td>
                                                                                        <td>{{$catse}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="religion" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Religion</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.religion')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_religion'] as $religionKey => $religion)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$religionKey}}</td>
                                                                                        <td>{{$religion}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="title" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Title</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.title')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_title'] as $titleKey => $title)
                                                                                        <tr>
                                                                                            <td>{{$titleKey}}</td>
                                                                                            <td>{{$title}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="annual_salary" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Annual Salary</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.annual_salary')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_annual_salary'] as $annualSalaryKey => $annualSalary)
                                                                                        <tr>
                                                                                            <td>{{$annualSalaryKey}}</td>
                                                                                            <td>{{$annualSalary}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="nationality" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Nationality</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.natioanlity')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_natioanlity'] as $natioanlityKey => $natioanlity)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$natioanlityKey}}</td>
                                                                                        <td>{{$natioanlity}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="group" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> School Group / House</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.group')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_group'] as $groupKey => $group)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$groupKey}}</td>
                                                                                        <td>{{$group}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
     jQuery(document).ready(function () {
        $("#import-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                class_id: {
                    required: true
                },
                section_id: {
                    required: true
                },
                import_file: {
                    extension: 'xls',
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
        $("#import_file").change(function() {
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'xls':
                    $('#error-block').hide();
                    $('#error-block').html('');
                    $(':input[type="submit"]').prop('disabled', false);
                    break;
                default:
                    $(this).val('');
                    // error message here
                    $('#error-block').show();
                    $('#error-block').html('Please select only xlsx file for import');
                    $(':input[type="submit"]').prop('disabled', true);
                    break;
            }
        });
    });

    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }
</script>
@endsection




