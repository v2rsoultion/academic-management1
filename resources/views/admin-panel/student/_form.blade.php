

@if(isset($student['student_id']) && !empty($student['student_id']))
<?php  $readonly = 'readonly'; $disabled = 'disabled'; ?>
@else
<?php $readonly = ''; $disabled=''; ?>
@endif

@if(!isset($student['admission_class_id']) && empty($student['admission_class_id']))
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@else
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@endif

@if(!isset($student['current_class_id']) && empty($student['current_class_id']))
<?php $current_section_readonly = false; $current_section_disabled=''; ?>
@else
<?php $current_section_readonly = false; $current_section_disabled=''; ?>
@endif

{!! Form::hidden('student_id',old('student_id',isset($student['student_id']) ? $student['student_id'] : ''),['class' => 'gui-input', 'id' => 'student_id', 'readonly' => 'true']) !!}
<div class="alert alert-danger" role="alert" id="error-block" style="display: none">
   
</div>
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Academic Information -->
<div class="header">
    <h2><strong>Academic</strong> Information</h2>
</div>

<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $student['arr_medium'],isset($student['medium_type']) ? $student['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type','onChange' => 'getClass(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_session_id', $student['arr_session'],isset($student['admission_session_id']) ? $student['admission_session_id'] : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_class_id', $student['arr_class'],isset($student['admission_class_id']) ? $student['admission_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_class_id','onChange' => 'getSection(this.value,"1")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_section_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('admission_section_id', $student['admit_arr_section'],isset($student['admission_section_id']) ? $student['admission_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_section_id',$admit_section_disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

   
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_session_id', $student['arr_session'],isset($student['current_session_id']) ? $student['current_session_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_class_id', $student['arr_class'],isset($student['current_class_id']) ? $student['current_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_class_id','onChange' => 'getSection(this.value,"2")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_section_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_section_id', $student['arr_section'],isset($student['current_section_id']) ? $student['current_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_section_id',$current_section_disabled,'onChange'=> 'checkSectionCapacity(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <!-- <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.stream_name') !!}  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('stream_id', $student['arr_stream'],isset($student['stream_id']) ? $student['stream_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'stream_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('stream_id')) <p class="help-block">{{ $errors->first('stream_id') }}</p> @endif
    </div> -->
</div>

<div class="header">
    <h2><strong>Basic</strong> Information</h2>
</div>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_enroll_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group field select">
            {!! Form::text('student_enroll_number', old('student_enroll_number',isset($student['student_enroll_number']) ? $student['student_enroll_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_enroll_number'), 'id' => 'student_enroll_number']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_name', old('student_name',isset($student['student_name']) ? $student['student_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_name'), 'id' => 'student_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_type') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_type', $student['arr_student_type'],isset($student['student_type']) ? $student['student_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.student_image')}} </span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="student_image" id="file1" onChange="document.getElementById('student_image').value = this.value;">
                <input type="hidden" class="gui-input" id="student_image" placeholder="Upload Photo" readonly>
            </label>
            <span>Photo size will be 180X180</span>
            @if(isset($student['student_image']))<br /><a href="{{url($student['student_image'])}}" target="_blank" >View Image</a>@endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_reg_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('student_reg_date', old('student_reg_date',isset($student['student_reg_date']) ? $student['student_reg_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_reg_date'), 'id' => 'student_reg_date']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_dob') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('student_dob', old('student_dob',isset($student['student_dob']) ? $student['student_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_dob'), 'id' => 'student_dob']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_email', old('student_email',isset($student['student_email']) ? $student['student_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_email'), 'id' => 'student_email']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_gender') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_gender', $student['arr_gender'],isset($student['student_gender']) ? $student['student_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_gender'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<!-- Previous school Information -->
<div class="header">
    <h2><strong>Previous School</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_school') !!}  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_school', old('student_privious_school',isset($student['student_privious_school']) ? $student['student_privious_school']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_school'), 'id' => 'student_privious_school']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_class') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_class', old('student_privious_class',isset($student['student_privious_class']) ? $student['student_privious_class']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_class'), 'id' => 'student_privious_class']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_no') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_tc_no', old('student_privious_tc_no',isset($student['student_privious_tc_no']) ? $student['student_privious_tc_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_no'), 'id' => 'student_privious_tc_no']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_date') !!} :</lable>
        <div class="form-group">
            {!! Form::date('student_privious_tc_date', old('student_privious_tc_date',isset($student['student_privious_tc_date']) ? $student['student_privious_tc_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_date'), 'id' => 'student_privious_tc_date']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_privious_result') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_privious_result', old('student_privious_result',isset($student['student_privious_result']) ? $student['student_privious_result']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_result'), 'id' => 'student_privious_result', 'rows'=> 1]) !!}
        </div>
    </div>
</div>

<!-- Other Information -->
<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_category') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_category', $student['arr_category'],isset($student['student_category']) ? $student['student_category'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_category', 'data-live-search'=>'true'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.caste_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('caste_id', $student['arr_caste'],isset($student['caste_id']) ? $student['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.religion_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('religion_id', $student['arr_religion'],isset($student['religion_id']) ? $student['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.nationality_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('nationality_id', $student['arr_natioanlity'],isset($student['nationality_id']) ? $student['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<div class="row clearfix">
    <!-- <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_sibling_name', old('student_sibling_name',isset($student['student_sibling_name']) ? $student['student_sibling_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_sibling_name'), 'id' => 'student_sibling_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_class_id') !!} :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_sibling_class_id', $student['arr_class'],isset($student['student_sibling_class_id']) ? $student['student_sibling_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_sibling_class_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div> -->

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_adhar_card_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_adhar_card_number', old('student_adhar_card_number',isset($student['student_adhar_card_number']) ? $student['student_adhar_card_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_adhar_card_number'), 'id' => 'student_adhar_card_number', 'maxLength'=>'12']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.group_id') !!} :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('group_id', $student['arr_group'],isset($student['group_id']) ? $student['group_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'group_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.title_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('title_id', $student['arr_title'],isset($student['title_id']) ? $student['title_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'title_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>


<!-- Guardian Information -->
<div class="header">
    <h2><strong>Parent Login</strong> Credentials</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_contact_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_contact_no', old('student_login_contact_no',isset($student['student_login_contact_no']) ? $student['student_login_contact_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_contact_no'), 'id' => 'student_login_contact_no', 'maxlength'=> '10', 'onkeyup' => "check_parent_existance()", $readonly]) !!}
            {!! Form::hidden('update_contact_no_status','0', ['id' => 'update_contact_no_status']) !!}
        </div>
        
    </div>
    @if(isset($student['student_id']) && $student['student_id'] != '')
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">&nbsp; </lable>
        <div class="checkbox" id="customid">
            <input type="checkbox" id="update_check" class="check" >
            <label  class="from_one1" style="margin-bottom: 4px !important;"  for="update_check"> Update Login Contact No.</label>
        </div> 
    </div>
    @endif
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_name', old('student_login_name',isset($student['student_login_name']) ? $student['student_login_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_name'), 'id' => 'student_login_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_login_email', old('student_login_email',isset($student['student_login_email']) ? $student['student_login_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_email'), 'id' => 'student_login_email']) !!}
        </div>
    </div>

</div>

<!-- Father Information -->
<div class="header">
    <h2><strong>Father</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_name', old('student_father_name',isset($student['student_father_name']) ? $student['student_father_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_name'), 'id' => 'student_father_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_mobile_number', old('student_father_mobile_number',isset($student['student_father_mobile_number']) ? $student['student_father_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_mobile_number'), 'id' => 'student_father_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_email') !!}   :</lable>
        <div class="form-group">
            {!! Form::text('student_father_email', old('student_father_email',isset($student['student_father_email']) ? $student['student_father_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_email'), 'id' => 'student_father_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_annual_salary') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_father_annual_salary', $student['arr_annual_salary'],isset($student['student_father_annual_salary']) ? $student['student_father_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_father_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_father_occupation') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_father_occupation', old('student_father_occupation',isset($student['student_father_occupation']) ? $student['student_father_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_occupation'), 'id' => 'student_father_occupation', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- Mother Information -->
<div class="header">
    <h2><strong>Mother</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_name', old('student_mother_name',isset($student['student_mother_name']) ? $student['student_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_name'), 'id' => 'student_mother_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_mobile_number', old('student_mother_mobile_number',isset($student['student_mother_mobile_number']) ? $student['student_mother_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_mobile_number'), 'id' => 'student_mother_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_email', old('student_mother_email',isset($student['student_mother_email']) ? $student['student_mother_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_email'), 'id' => 'student_mother_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_annual_salary') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_mother_annual_salary', $student['arr_annual_salary'],isset($student['student_mother_annual_salary']) ? $student['student_mother_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_mother_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_tongue') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_tongue', old('student_mother_tongue',isset($student['student_mother_tongue']) ? $student['student_mother_tongue']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_tongue'), 'id' => 'student_mother_tongue']) !!}
        </div>
    </div>

    <div class="col-lg-9 col-md-9 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_mother_occupation') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_mother_occupation', old('student_mother_occupation',isset($student['student_mother_occupation']) ? $student['student_mother_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_occupation'), 'id' => 'student_mother_occupation', 'rows'=> 2]) !!}
        </div>
    </div>

</div>

<!-- Guardian Information -->
<div class="header">
    <h2><strong>Guardian</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_name', old('student_guardian_name',isset($student['student_guardian_name']) ? $student['student_guardian_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_name'), 'id' => 'student_guardian_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_mobile_number', old('student_guardian_mobile_number',isset($student['student_guardian_mobile_number']) ? $student['student_guardian_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_mobile_number'), 'id' => 'student_guardian_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_email', old('student_guardian_email',isset($student['student_guardian_email']) ? $student['student_guardian_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_email'), 'id' => 'student_guardian_email']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_relation') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_relation', old('student_guardian_relation',isset($student['student_guardian_relation']) ? $student['student_guardian_relation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_relation'), 'id' => 'student_guardian_relation']) !!}
        </div>
    </div>

</div>

<!-- Temporary Address Information -->
<div class="header">
    <h2><strong>Temporary Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_temporary_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_temporary_address', old('student_temporary_address',isset($student['student_temporary_address']) ? $student['student_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_address'), 'id' => 'student_temporary_address', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_county') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_county', $student['arr_country'], old('student_temporary_county',isset($student['student_temporary_county']) ? $student['student_temporary_county'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_country', 'onChange'=> 'showStates(this.value,"student_temporary_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_state') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_state', $student['arr_state_t'],isset($student['student_temporary_state']) ? $student['student_temporary_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_state' , 'onChange'=> 'showCity(this.value,"student_temporary_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_city') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_city', $student['arr_city_t'],isset($student['student_temporary_city']) ? $student['student_temporary_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_temporary_pincode', old('student_temporary_pincode',isset($student['student_temporary_pincode']) ? $student['student_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_pincode'), 'id' => 'student_temporary_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>

</div>

<!-- Permanent Address Information -->
<div class="header">
    <h2><strong>Permanent Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_permanent_address') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_permanent_address', old('student_permanent_address',isset($student['student_permanent_address']) ? $student['student_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_address'), 'id' => 'student_permanent_address', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_county') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_county', $student['arr_country'],isset($student['student_permanent_county']) ? $student['student_permanent_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_county' , 'onChange'=> 'showStates(this.value,"student_permanent_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_state') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_state', $student['arr_state_p'],isset($student['student_permanent_state']) ? $student['student_permanent_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_state', 'onChange'=> 'showCity(this.value,"student_permanent_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_city') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_city', $student['arr_city_p'],isset($student['student_permanent_city']) ? $student['student_permanent_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_pincode') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_permanent_pincode', old('student_permanent_pincode',isset($student['student_permanent_pincode']) ? $student['student_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_pincode'), 'id' => 'student_permanent_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>

</div>

<!-- Health Information -->
<div class="header">
    <h2><strong>Health</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_height') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_height', old('student_height',isset($student['student_height']) ? $student['student_height']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_height'), 'id' => 'student_height']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_weight') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_weight', old('student_weight',isset($student['student_weight']) ? $student['student_weight']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_weight'), 'id' => 'student_weight']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_blood_group') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_blood_group', old('student_blood_group',isset($student['student_blood_group']) ? $student['student_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_blood_group'), 'id' => 'student_blood_group']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_left') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_left', old('student_vision_left',isset($student['student_vision_left']) ? $student['student_vision_left']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_left'), 'id' => 'student_vision_left']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_right') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_right', old('student_vision_right',isset($student['student_vision_right']) ? $student['student_vision_right']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_right'), 'id' => 'student_vision_right']) !!}
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <lable class="from_one1">{!! trans('language.medical_issues') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group bs-example">
            {!! Form::text('medical_issues', old('medical_issues',isset($student['medical_issues']) ? $student['medical_issues']: ''), ['class' => 'form-control','placeholder'=>trans('language.medical_issues_p'), 'id' => 'medical_issues','data-role'=>'tagsinput']) !!}
        </div>
    </div>
</div>

<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($student['documents']) ? COUNT($student['documents']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    <p class="green" id="document_success"></p>
    @if(isset($student['documents']) && !empty($student['documents']))
    @php  $key = 0; @endphp
    @foreach($student['documents'] as $documentKey => $documents)
    @php 
        $student_document_category = "documents[".$key."][document_category_id]";  
        $student_document_id = "documents[".$key."][student_document_id]"; 
    @endphp
    <div id="document_block{{$documentKey}}">
        {!! Form::hidden($student_document_id,$documents['student_document_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.student_document_category') !!} :</lable>
                <div class="form-group  ">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select($student_document_category, $student['arr_document_category'],$documents['document_category_id'], ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            <span class="radioBtnpan">{{trans('language.student_document_file')}}</span>
                <label for="file1" class="field file">
                    
                    <input type="file" class="gui-file" name="documents[{{$key}}][student_document_file]" id="student_document_file{{$key}}" >
                    <input type="hidden" class="gui-input" id="student_document_file{{$key}}" placeholder="Upload Photo" readonly>
                    <br />
                    @if(isset($documents['student_document_file']) && !empty($documents['student_document_file']))<a href="{{url($documents['student_document_file'])}}" target="_blank" >View Document</a>@endif
                </label>
            
            </div>
            <div class="col-lg-3 col-md-3">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($documentKey == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$documentKey}}),remove_record({{$documents['student_document_id']}})" >Remove</button>
                @endif
            </div>
        </div>

        <!-- Document detail section -->
        <div class="row clearfix">
            <div class="col-sm-12 text_area_desc">
                <lable class="from_one1">{!! trans('language.student_document_details') !!} :</lable>
                <div class="form-group">
                    <textarea name="documents[{{$key}}][student_document_details]" id="" class="form-control" placeholder="{{trans('language.student_document_details')}}" rows="2">{!! $documents['student_document_details'] !!}</textarea>
                   
                </div>
                
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.student_document_category') !!} :</lable>
            <div class="form-group  ">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('documents[0][document_category_id]', $student['arr_document_category'],isset($student['document_category_id']) ? $student['document_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.student_document_file')}}</span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="documents[0][student_document_file]" id="student_document_file" onChange="document.getElementById('student_document_file').value = this.value;">
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
               
            </label>
           
        </div>
        <div class="col-lg-3 col-md-3">&nbsp;</div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button></div>
    </div>

    <!-- Document detail section -->
    <div class="row clearfix">
        <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">{!! trans('language.student_document_details') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('documents[0][student_document_details]', old('student_document_details',isset($student['student_document_details']) ? $student['student_document_details']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_document_details'), 'id' => 'student_document_details', 'rows'=> 2]) !!}
            </div>
        </div>
    </div>
    @endif
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/student') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>


<script type="text/javascript">
    $(document).on('click','.check',function(e){
        if ($('#update_check').is(':checked')) {
            console.log('checked');
            $('#updateContactNoModel').modal('show');
        } else {
            console.log('Uncheck');
        }
    });
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#student-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_enroll_number: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_email: {
                    // required: true,
                    email: true,
                    emailvalid: true
                },
                student_reg_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
                student_dob: {
                    required: true,
                    date: true,
                    minDate: true,
                    maxlength:10
                },
                student_privious_tc_date: {
                    required: true,
                    date:true,
                    maxlength:10
                },
                student_image: {
                    extension: 'jpg,png,jpeg,gif'
                },
                student_category: {
                    required: true
                },
                caste_id: {
                    required: true
                },
                religion_id: {
                    required: true
                },
                nationality_id: {
                    required: true
                },
                title_id: {
                    required: true
                },
                student_permanent_address: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_permanent_city: {
                    required: true
                },
                student_permanent_state: {
                    required: true
                },
                student_permanent_county: {
                    required: true
                },
                student_permanent_pincode: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                student_type: {
                    required: true
                },
                admission_session_id: {
                    required: true
                },
                admission_class_id: {
                    required: true
                },
                admission_section_id: {
                    required: true
                },
                current_session_id: {
                    required: true
                },
                current_class_id: {
                    required: true
                },
                current_section_id: {
                    required: true
                },
                student_blood_group: {
                    // required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                medical_issues: {
                    required: true
                },
                student_father_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_father_mobile_number: {
                    required: true,
                    digits: true
                },
                student_father_email: {
                    // required: true,
                    email: true,
                    emailvalid: true
                },
                student_father_occupation: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_mobile_number: {
                    // required: true,
                    digits: true
                },
                student_guardian_name: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                // student_sibling_name: {
                //     lettersonly: true,
                //     normalizer: function(value) {
                //         return $.trim(value);
                //     }
                // },
                student_guardian_mobile_number: {
                    digits: true
                },
                student_guardian_email: {
                    email: true,
                    emailvalid: true
                },
                student_mother_email: {
                    email: true,
                    emailvalid: true
                },
                student_temporary_pincode: {
                    digits: true
                },
                student_permanent_pincode: {
                    digits: true,
                    required: true
                },
                // student_privious_school: {
                //     required: true
                // },
                // student_privious_class: {
                //     required: true
                // },
                // student_privious_tc_no: {
                //     required: true
                // },
                student_privious_tc_date: {
                    date: true,
                   // minDate: true
                },
                // student_privious_result: {
                //     required: true
                // },
                student_login_name: {
                    required: true
                },
                student_login_email: {
                    // required: true,
                    email: true,
                    emailvalid: true
                },
                student_login_contact_no: {
                    digits: true,
                    required: true
                },
                student_adhar_card_number: {
                    digits: true
                },
                student_height: {
                    number: true
                },
                student_weight: {
                    number: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        // d = new Date();
        // d.setFullYear(d.getFullYear() - 1);
        // minD = new Date();
        // minD.setFullYear('1990');
        // $('#student_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false,maxDate : d, minDate :minD }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#student_reg_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,maxDate : new Date() }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#student_privious_tc_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,maxDate : new Date()}).on('change', function(ev) {
        //     $(this).valid();  
        // });

        $("#file1").change(function() {
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'gif': case 'jpg': case 'png': case 'jpeg' :
                    $('#error-block').hide();
                    $('#error-block').html('');
                    $(':input[type="submit"]').prop('disabled', false);
                    break;
                default:
                    $(this).val('');
                    // error message here
                    $('#error-block').show();
                    $('#error-block').html('Please select an image for student photo');
                    $(':input[type="submit"]').prop('disabled', true);
                    break;
            }
        });
    });

    function getSection(class_id,type)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    if(type == 1) {
                        $("select[name='admission_section_id'").html(data.options);
                        $(".mycustloading").hide();
                    } else if(type == 2){
                        $("select[name='current_section_id'").html(data.options);
                        $(".mycustloading").hide();
                    }
                }
            });
        } else {
            if(type == 1) {
                $("select[name='admission_section_id'").html('');
                $(".mycustloading").hide();
            } else if(type == 2){
                $("select[name='current_section_id'").html('');
                $(".mycustloading").hide();
            }
        }
    }

    function check_parent_existance(){
        var mobile_no = $("#student_login_contact_no").val().toString();
        if(mobile_no.length == 10){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-parent-existance')}}",
                type: 'GET',
                data: {
                    'mobile_no': mobile_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == 1){
                            $("#student_login_name").val(data.student_login_name);
                            $("#student_login_email").val(data.student_login_email);
                            // $("#student_login_contact_no").val(data.student_login_contact_no);
                            $("#student_father_mobile_number").val(data.student_father_mobile_number);
                            $("#student_father_name").val(data.student_father_name);
                            $("#student_father_email").val(data.student_father_email);
                            $("#student_father_occupation").val(data.student_father_occupation);
                            $("#student_father_annual_salary").val(data.student_father_annual_salary);
                            $("#student_mother_name").val(data.student_mother_name);
                            $("#student_mother_mobile_number").val(data.student_mother_mobile_number);
                            $("#student_mother_email").val(data.student_mother_email);
                            $("#student_mother_occupation").val(data.student_mother_occupation);

                            $("#student_mother_tongue").val(data.student_mother_tongue);
                            $("#student_guardian_name").val(data.student_guardian_name);
                            $("#student_guardian_email").val(data.student_guardian_email);
                            $("#student_guardian_relation").val(data.student_guardian_relation);
                            $("#student_guardian_mobile_number").val(data.student_guardian_mobile_number);
                            $("#student_mother_annual_salary").val(data.student_mother_annual_salary);
                            $("select[name='student_father_annual_salary'").selectpicker('refresh');
                            $("select[name='student_mother_annual_salary'").selectpicker('refresh');
                        }
                    }
                    // } else {
                    //     $("#student_login_name").val('');
                    //     $("#student_login_email").val('');
                    //     // $("#student_login_contact_no").val('');
                    //     $("#student_father_name").val('');
                    //     $("#student_father_email").val('');
                    //     $("#student_father_occupation").val('');
                    //     $("#student_father_annual_salary").val('');
                    //     $("#student_mother_name").val('');
                    //     $("#student_mother_mobile_number").val('');
                    //     $("#student_mother_email").val('');
                    //     $("#student_mother_occupation").val('');

                    //     $("#student_mother_tongue").val('');
                    //     $("#student_guardian_name").val('');
                    //     $("#student_guardian_email").val('');
                    //     $("#student_guardian_mobile_number").val('');
                    //     $("#student_mother_annual_salary").val('');
                    //     $("select[name='student_father_annual_salary'").selectpicker('refresh');
                    //     $("select[name='student_mother_annual_salary'").selectpicker('refresh'); 
                    // }
                }
            });
        }
    }
    function check_parent_existance1(){
        var mobile_no = $("#update_student_login_contact_no").val().toString();
        if(mobile_no.length == 10){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-parent-existance')}}",
                type: 'GET',
                data: {
                    'mobile_no': mobile_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == '1'){
                            $("#error_status").val('1');
                            $("#error-block1").html("Contact Number already exist!! Please try with another contact number!!");
                            $("#error-block1").css('display', 'block');
                        }
                    }
                    if(data.exist_status == "0"){
                        $("#error-block1").html("");
                        $("#error-block1").css('display', 'none');
                        $("#error_status").val('0');
                    }
                    
                }
            });
        }
    }

    function update_contact_no(){
        var update_student_login_contact_no = $("#update_student_login_contact_no").val();
        var error_status = $("#error_status").val();
        if(error_status == 1){
            $("#error-block1").html("Contact Number already exist!! Please try with another contact number!!");
            $("#error-block1").css('display', 'block');
        } else {
            $("#error-block1").html("");
            $("#error-block1").css('display', 'none');
            $("#student_login_contact_no").val(update_student_login_contact_no);
            $("#update_contact_no_status").val('1');
            $('#updateContactNoModel').modal('hide');

        }
    }
    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} :</lable><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]">@foreach ($student["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.student_document_file")}}</span><label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][student_document_file]" id="file'+arr_key+'" ><input type="hidden" class="gui-input" id="student_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.student_document_details") !!} :</lable><div class="form-group"><textarea name="documents['+arr_key+'][student_document_details]" class="form-control" placeholder="{{trans("language.student_document_details")}}" id="student_document_details'+arr_key+'" rows="2" ></textarea></div> </div></div></div>');
        $("select[name='documents["+arr_key+"][document_category_id]'").selectpicker('refresh');
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }
    function remove_record(student_document_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/delete-student-document/')}}/"+student_document_id+'/1',
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='admission_class_id'").html(data.options);
                   // $("select[name='student_sibling_class_id'").html(data.options);
                    $("select[name='current_class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='admission_class_id'").html('<option value="">Select Class</option>');
            $("select[name='current_class_id'").html('<option value="">Select Class</option>');
           // $("select[name='student_sibling_class_id'").html(data.options);
            $(".mycustloading").hide();
        }
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/stream/get-stream-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='stream_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='stream_id'").html('<option value="">Select Stream</option>');
            $(".mycustloading").hide();
        }
    }

    function checkSectionCapacity(section_id){
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-section-capacity')}}",
                type: 'GET',
                data: {
                    'section_id': section_id
                },
                success: function (data) {
                    if(data == 'False'){
                        $('#error-block').html("Sorry, We have already exceed section's capacity. Please add student into different section.");
                        $(':input[type="submit"]').prop('disabled', true);
                        $('#error-block').show();
                    } else {
                        $('#error-block').hide();
                        $('#error-block').html('');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                    $(".mycustloading").hide();
                }
            });
        } 
    }

    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>

