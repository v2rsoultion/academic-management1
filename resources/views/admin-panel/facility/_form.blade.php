@if(isset($facility['facility_id']) && !empty($facility['facility_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('facility_id',old('facility_id',isset($facility['facility_id']) ? $facility['facility_id'] : ''),['class' => 'gui-input', 'id' => 'facility_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.facility_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('facility_name', old('facility_name',isset($facility['facility_name']) ? $facility['facility_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.facility_name'), 'id' => 'facility_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
        <label class="from_one1">{!! trans('language.is_fees_applied') !!} :</label>
        <div class="form-group">
            <div class="radio">
                <?php $yes_paid = 'checked'; $not_paid = '';?>
                    @if(isset($facility['facility_fees_applied']) && $facility['facility_fees_applied'] == 1)
                       <?php $yes_paid = 'checked'; $not_paid =''; ?>
                    @endif
                       
                    @if(isset($facility['facility_fees_applied']) && $facility['facility_fees_applied'] == 0)
                        <?php $not_paid = 'checked';  ?>
                    @endif

                {!! Form::radio('is_fees_applied','1',$yes_paid,['class' => 'form-control','id'=>'is_fees_applied1','onClick'=>'display_fees()']) !!}
                <label for="is_fees_applied1">{!! trans('language.fees_applied_yes') !!}</label>
                {!! Form::radio('is_fees_applied','0',$not_paid,['class' => 'form-control','id'=>'is_fees_applied2','onClick'=>'hide_fees()']) !!}
                <label for="is_fees_applied2">{!! trans('language.fees_applied_no') !!}</label>
            </div>
        </div>
    </div>

    <!-- <div class="col-lg-4 col-md-4" id="facility_fees_id">
        <lable class="from_one1">{!! trans('language.facility_fees_amount') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('fees_amount', old('fees_amount',isset($facility['facility_fees']) ? $facility['facility_fees']: ''), ['class' => 'form-control','placeholder'=>trans('language.facility_fees_amount'), 'id' => 'facility_fees','min'=>'0', 'required']) !!}
        </div>
        @if ($errors->has('fees_amount')) <p class="help-block">{{ $errors->first('fees_amount') }}</p> @endif
    </div> -->

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#facility-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                facility_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                is_fees_applied: {
                    required: true
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });

    function display_fees() {
        // var x = document.getElementById("facility_fees_id");
        // if (x.style.display === "none") {
        //     x.style.display = "block";
        // } else {
        //     x.style.display = "none";
        // }
    }
    function hide_fees() {
        var x = document.getElementById("facility_fees_id");
        if (x.style.display === "none") {
            x.style.display = "none";

        } else {
            x.style.display = "none";
            $('#facility_fees').val();
        }
    }    

</script>