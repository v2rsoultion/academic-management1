
<!-- Jquery Core Js --> 
    
<!-- {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!} -->
@if(Request::segment(2) == "dashboard")
{!! Html::script('public/admin/assets/js/jquery-1.11.1.min.js') !!}
{!! Html::script('public/admin/assets/js/jquery.canvasjs.min.js') !!}
@endif

{!! Html::script('public/admin/assets/bundles/vendorscripts.bundle.js') !!}
{!! Html::script('public/admin/assets/bundles/morrisscripts.bundle.js') !!}
{!! Html::script('public/admin/assets/bundles/jvectormap.bundle.js') !!}
{!! Html::script('public/admin/assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') !!}
{!! Html::script('public/admin/assets/bundles/knob.bundle.js') !!}


{!! Html::script('public/admin/assets/plugins/momentjs/moment.js') !!}
{!! Html::script('public/admin/assets/plugins/autosize/autosize.js') !!}
{!! Html::script('public/admin/assets/plugins/fullcalendar/fullcalendar.js') !!}
{!! Html::script('public/admin/assets/bundles/fullcalendarscripts.bundle.js') !!}
{!! Html::script('public/admin/assets/js/pages/calendar/calendar.js') !!}
{!! Html::script('public/admin/assets/js/pages/widgets/infobox/infobox-1.js') !!}
{!! Html::script('public/admin/assets/js/pages/charts/jquery-knob.js') !!}
{!! Html::script('public/admin/assets/js/pages/cards/basic.js') !!}

{!! Html::script('public/admin/assets/bundles/datatablescripts.bundle.js') !!}
{!! Html::script('public/admin/assets/bundles/mainscripts.bundle.js') !!}
{!! Html::script('public/admin/assets/js/pages/tables/jquery-datatable.js') !!}

{!! Html::script('public/admin/assets/js/pages/index.js') !!}
{!! Html::script('public/admin/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}
{!! Html::script('public/admin/assets/js/pages/forms/basic-form-elements.js') !!}
{!! Html::script('public/admin/assets/plugins/jquery-validation/jquery.validate.js') !!}

<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
{!! Html::script('public/admin/assets/js/pages/forms/form-validation.js') !!}



{!! Html::script('public/admin/assets/datatables/dataTables.buttons.min.js') !!}
{!! Html::script('public/admin/assets/datatables/jszip.min.js') !!}
{!! Html::script('public/admin/assets/datatables/pdfmake.min.js') !!}
{!! Html::script('public/admin/assets/datatables/vfs_fonts.js') !!}
{!! Html::script('public/admin/assets/datatables/buttons.html5.min.js') !!}

{!! Html::script('public/admin/assets/datatables/buttons.flash.min.js') !!}
{!! Html::script('public/admin/assets/datatables/buttons.print.min.js') !!}
{!! Html::script('public/admin/assets/js/select2.full.js') !!}
<!-- {!! Html::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js') !!} -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js" type="text/javascript"></script>
@if(Request::segment(3) != "manage-leave-application")
<script>
    $(document).ready(function(){
    $('body').on('keyup', 'input', function(){
           $('#book_no_start').valid();  
           $('#serial_no_start').valid();  
    });
});
</script>
@endif
@if(Request::segment(3) != "add-student")
<!-- <script>
    $('.modal').on('hidden.bs.modal', function () {
        location.reload();
    })
    $('#clearBtn').click(function(){
        location.reload();
    })
    $('#clearBtn1').click(function(){
        location.reload();
    })
</script> -->
@endif

@if(Request::segment(3) == "add-student")
<script>
    $('.modal').on('hidden.bs.modal', function () {
        var mobile_no = $("#update_student_login_contact_no").val().toString();
        console.log(mobile_no);
        if(mobile_no.length != 10){
            $('#update_check').prop('checked', false);
        }
    });
</script>
@endif
</body>
</html>