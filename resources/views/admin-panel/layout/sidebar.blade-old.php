<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href=""><i class="zmdi zmdi-home"></i></a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="">{!! $login_info['admin_role']; !!}</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            @if ($login_info['admin_type'] == 0)
                                <div class="image"><a href="{{ url('/admin-panel/dashboard') }}"><img src="http://via.placeholder.com/80x80" alt="User"></a></div>
                            @elseif ($login_info['admin_type'] == 1)
                                <div class="image"><a href="{{ url('/admin-panel/school/view-school-detail/'.$login_info['encrypted_school_id']) }}"><img src="http://via.placeholder.com/80x80" alt="User"></a></div>
                            @endif
                            
                            <div class="detail">
                                <h4>{!! $login_info['admin_name']; !!} </h4>
                                <!-- <small>UI UX Designer</small> -->
                            </div>
                        </div>
                    </li>
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                    <li class="javascript:void(0); @if(Request::segment(2) == "session" || Request::segment(2) == "school" || Request::segment(2) == "holidays"  || Request::segment(2) == "shift" || Request::segment(2) == "facilities" || Request::segment(2) == "document-category" || Request::segment(2) == "designation" || Request::segment(2) == "title" || Request::segment(2) == "caste" || Request::segment(2) == "religion" || Request::segment(2) == "nationality" || Request::segment(2) == "schoolgroup" || Request::segment(2) == "room-no" || Request::segment(2) == "country" || Request::segment(2) == "state" || Request::segment(2) == "city" ) active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Configuration</strong></span>

                    </a>
                        <ul class="sub-menu">
                            <!-- For School Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "school") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>School Details</span> </a>
                                <ul class="ml-menu">
                                    @if ($login_info['encrypted_school_id'] == "")
                                    <li @if(Request::segment(3) == "add-school") class="active" @endif ><a href="{{ url('/admin-panel/school/add-school') }}">Add School </a></li>
                                    @elseif ($login_info['encrypted_school_id'] != "")
                                    <li @if(Request::segment(3) == "view-school-detail") class="active" @endif ><a href="{{ url('/admin-panel/school/view-school-detail/'.$login_info['encrypted_school_id']) }}">View School Detail</a></li>
                                    @endif
                                </ul> 
                            </li>
                            @endif

                            <!-- For Session Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "session") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Sessions</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-session") class="active" @endif><a href="{{ url('/admin-panel/session/add-session') }}">Add Session </a></li>
                                    <li @if(Request::segment(3) == "view-sessions") class="active" @endif><a href="{{ url('/admin-panel/session/view-sessions') }}">View Sessions </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Holidays Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "holidays") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Holidays</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-holiday") class="active" @endif><a href="{{ url('/admin-panel/holidays/add-holiday') }}">Add Holiday </a></li>
                                    <li @if(Request::segment(3) == "view-holidays") class="active" @endif><a href="{{ url('/admin-panel/holidays/view-holidays') }}">View Holidays </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Shifts Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "shift") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Shifts</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-shift") class="active" @endif><a href="{{ url('/admin-panel/shift/add-shift') }}">Add Shift </a></li>
                                    <li @if(Request::segment(3) == "view-shifts") class="active" @endif><a href="{{ url('/admin-panel/shift/view-shifts') }}">View Shifts </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Facilities Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "facilities") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Facilities</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-facility") class="active" @endif><a href="{{ url('/admin-panel/facilities/add-facility') }}">Add Facility </a></li>
                                    <li @if(Request::segment(3) == "view-facilities") class="active" @endif><a href="{{ url('/admin-panel/facilities/view-facilities') }}">View Facilities </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Document Categories Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "document-category") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Document Categories</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-document-category") class="active" @endif><a href="{{ url('/admin-panel/document-category/add-document-category') }}">Add Document Category </a></li>
                                    <li @if(Request::segment(3) == "view-document-categories") class="active" @endif><a href="{{ url('/admin-panel/document-category/view-document-categories') }}">View Document Categories </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Designation Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "designation") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Designations</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-designation") class="active" @endif><a href="{{ url('/admin-panel/designation/add-designation') }}">Add Designation </a></li>
                                    <li @if(Request::segment(3) == "view-designations") class="active" @endif><a href="{{ url('/admin-panel/designation/view-designations') }}">View Designations </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Title Add By Pratyush on 20 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "title") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Titles</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-title") class="active" @endif><a href="{{ url('/admin-panel/title/add-title') }}">Add Title </a></li>
                                    <li @if(Request::segment(3) == "view-titles") class="active" @endif><a href="{{ url('/admin-panel/title/view-titles') }}">View Titles </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Caste Add By Pratyush on 20 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "caste") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Caste</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-caste") class="active" @endif><a href="{{ url('/admin-panel/caste/add-caste') }}">Add Caste </a></li>
                                    <li @if(Request::segment(3) == "view-caste") class="active" @endif><a href="{{ url('/admin-panel/caste/view-caste') }}">View Caste </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Religion Add By Pratyush on 20 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "religion") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Religions</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-religion") class="active" @endif><a href="{{ url('/admin-panel/religion/add-religion') }}">Add Religion </a></li>
                                    <li @if(Request::segment(3) == "view-religions") class="active" @endif><a href="{{ url('/admin-panel/religion/view-religions') }}">View Religions </a></li>
                                </ul> 
                            </li>
                            @endif


                            <!-- For Nationality Add By Pratyush on 20 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "nationality") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Nationality</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-nationality") class="active" @endif><a href="{{ url('/admin-panel/nationality/add-nationality') }}">Add Nationality </a></li>
                                    <li @if(Request::segment(3) == "view-nationality") class="active" @endif><a href="{{ url('/admin-panel/nationality/view-nationality') }}">View nationality </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For School Groups Add By Pratyush on 20 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "schoolgroup") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>School Group/House</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-group") class="active" @endif><a href="{{ url('/admin-panel/schoolgroup/add-group') }}">Add Group </a></li>
                                    <li @if(Request::segment(3) == "view-groups") class="active" @endif><a href="{{ url('/admin-panel/schoolgroup/view-groups') }}">View Groups </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Room No Add By Pratyush on 28 July 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "room-no") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Room No</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-room-no") class="active" @endif><a href="{{ url('/admin-panel/room-no/add-room-no') }}">Add Room No </a></li>
                                    <li @if(Request::segment(3) == "view-room-no") class="active" @endif><a href="{{ url('/admin-panel/room-no/view-room-no') }}">View Room No </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Country Add By Khushbu on 10 Sept 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "country") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Country</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-country") class="active" @endif><a href="{{ url('/admin-panel/country/add-country') }}">Add Country </a></li>
                                    <li @if(Request::segment(3) == "view-country") class="active" @endif><a href="{{ url('/admin-panel/country/view-country') }}">View Country </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For State Add By Khushbu on 10 Sept 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "state") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>State</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-state") class="active" @endif><a href="{{ url('/admin-panel/state/add-state') }}">Add State </a></li>
                                    <li @if(Request::segment(3) == "view-state") class="active" @endif><a href="{{ url('/admin-panel/state/view-state') }}">View State </a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For City Add By Khushbu on 10 Sept 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "city") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>City</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-city") class="active" @endif><a href="{{ url('/admin-panel/city/add-city') }}">Add City </a></li>
                                    <li @if(Request::segment(3) == "view-city") class="active" @endif><a href="{{ url('/admin-panel/city/view-city') }}">View City </a></li>
                                </ul> 
                            </li>
                            @endif

                        </ul>
                    </li>
                    @endif
                    <!-- Academic Menu here -->
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)

                    <li class="javascript:void(0); @if(Request::segment(2) == "class" || Request::segment(2) == "section" || Request::segment(2) == "class-teacher"  || Request::segment(2) == "subject" || Request::segment(2) == "time-table" || Request::segment(2) == "virtual-class" || Request::segment(2) == "competitions" || Request::segment(2) == "certificates" || Request::segment(2) == "virtualclass" || Request::segment(2) == "competition" || Request::segment(2) == "class-teacher-allocation" || Request::segment(2) == "class-subject-mapping" || Request::segment(2) == "teacher-subject-mapping" || Request::segment(2) == "stream") active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Academic</strong></span>
                    </a>
                        <ul class="sub-menu">
                            <!-- For Class Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "class") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Class</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-class") class="active" @endif><a href="{{ url('/admin-panel/class/add-class') }}">Add Class </a></li>
                                    <li @if(Request::segment(3) == "view-classes") class="active" @endif><a href="{{ url('/admin-panel/class/view-classes') }}">View Classes </a></li>
                                </ul>
                            </li>
                            @endif

                            <!-- For Section Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "section") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Section</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-section") class="active" @endif><a href="{{ url('/admin-panel/section/add-section') }}">Add Section </a></li>
                                    <li @if(Request::segment(3) == "view-sections") class="active" @endif><a href="{{ url('/admin-panel/section/view-sections') }}">View Sections </a></li>
                                </ul>
                            </li>
                            @endif

                            <!-- For Stream added by Pratyush on 11 Aug 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "stream") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Streams</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-stream") class="active" @endif><a href="{{ url('/admin-panel/stream/add-stream') }}">Add Stream</a></li>
                                    <li @if(Request::segment(3) == "view-streams") class="active" @endif><a href="{{ url('/admin-panel/stream/view-streams') }}">View Streams</a></li>
                                </ul>
                            </li>
                            @endif

                             <!-- For Subject Menu -->
                             @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "subject") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Subject</span> </a>
                                <ul class="ml-menu sub-menu">
                                    <!-- For Type of Co-Scholastic Menu -->
                                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                                    <li class="@if(Request::segment(3) == "add-type-of-co-scholastic" || Request::segment(3) == "view-co-scholastic") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><span>Co-Scholastic</span> </a>
                                        <ul class="ml-menu sub-menu">
                                            <li @if(Request::segment(3) == "add-co-scholastic") class="active" @endif><a href="{{ url('/admin-panel/subject/add-co-scholastic') }}">Add Co-Scholastic </a></li>
                                            <li @if(Request::segment(3) == "view-co-scholastic") class="active" @endif><a href="{{ url('/admin-panel/subject/view-co-scholastic') }}">View Co-Scholastic </a></li>
                                        </ul>
                                    </li>
                                    @endif

                                    <!-- For Subject Menu -->
                                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                                    <li class="@if(Request::segment(3) == "add-subject" || Request::segment(3) == "view-subjects") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><span>Subject</span> </a>
                                        <ul class="ml-menu sub-menu">
                                            <li @if(Request::segment(3) == "add-subject") class="active" @endif><a href="{{ url('/admin-panel/subject/add-subject') }}">Add Subject </a></li>
                                            <li @if(Request::segment(3) == "view-subjects") class="active" @endif><a href="{{ url('/admin-panel/subject/view-subjects') }}">View Subjects</a></li>
                                        </ul>
                                    </li>
                                    @endif

                                </ul>
                            </li>
                            @endif

                            <!-- For Class Teacher Allocation added by Pratyush on 21 July 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "class-teacher-allocation") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Class-Teacher Allocate</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "allocate-class") class="active" @endif><a href="{{ url('/admin-panel/class-teacher-allocation/allocate-class') }}">Allocate Class</a></li>
                                    <li @if(Request::segment(3) == "view-allocations") class="active" @endif><a href="{{ url('/admin-panel/class-teacher-allocation/view-allocations') }}">View Allocations</a></li>
                                </ul>
                            </li>
                            @endif

                            <!-- For Subject Class Mapping added by Pratyush on 07 Aug 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "class-subject-mapping") active @endif"><a href="{{ url('/admin-panel/class-subject-mapping/view-subject-class-mapping') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Map Subject-Class</span></a>
                            </li>
                            @endif

                            <!-- For Subject Teacher Mapping added by Pratyush on 07 Aug 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <!-- <li class="@if(Request::segment(2) == "teacher-subject-mapping") active open @endif"><a href="{{ url('/admin-panel/teacher-subject-mapping/view-subject-teacher-mapping') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Map Subject-Teacher</span></a>
                            </li> -->
                            @endif

                            <!-- For Time Table added by Khushbu on 06 Sept 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "time-table") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Time Table</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-time-table") class="active" @endif><a href="{{ url('/admin-panel/time-table/add-time-table') }}">Add Time Table </a></li>
                                    <li @if(Request::segment(3) == "view-time-table") class="active" @endif><a href="{{ url('/admin-panel/time-table/view-time-table') }}">View Time Table </a></li>
                                </ul>
                            </li>
                            @endif 

                            <!-- For Virtual Class added by Pratyush on 21 July 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <!-- <li class="@if(Request::segment(2) == "virtual-class") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Virtual Class</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-virtual-class") class="active" @endif><a href="{{ url('/admin-panel/virtual-class/add-virtual-class') }}">Add Virtual Class </a></li>
                                    <li @if(Request::segment(3) == "view-virtual-classes") class="active" @endif><a href="{{ url('/admin-panel/virtual-class/view-virtual-classes') }}">View Virtual Classes </a></li>
                                </ul>
                            </li> -->
                            @endif 


                            <!-- For Competitions added by Pratyush on 21 July 2018-->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <!-- <li class="@if(Request::segment(2) == "competition") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Competitions</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-competition") class="active" @endif><a href="{{ url('/admin-panel/competition/add-competition') }}">Add Competition </a></li>
                                    <li @if(Request::segment(3) == "view-competitions") class="active" @endif><a href="{{ url('/admin-panel/competition/view-competitions') }}">View Competitions </a></li>
                                </ul>
                            </li> -->
                            @endif

                        </ul>
                    </li>
                    @endif 

                    <!-- Student Menu here -->
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 4)
                    <li class="javascript:void(0); @if(Request::segment(2) == "student" || Request::segment(2) == "student-leave-application" ) active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Student</strong></span>
                    </a>
                        <ul class="sub-menu ">
                            <!-- For Class Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            
                            <li @if(Request::segment(3) == "add-student") class="active" @endif><a href="{{ url('/admin-panel/student/add-student') }}">Add Student </a></li>
                            <li @if(Request::segment(3) == "view-list") class="active" @endif><a href="{{ url('/admin-panel/student/view-list') }}">View list </a></li>
                            <li @if(Request::segment(3) == "parent-information") class="active" @endif><a href="{{ url('/admin-panel/student/parent-information') }}">Parent Information </a></li>
                            @endif

                            <!-- For Student Leave Application Add By Pratyush on 10 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 4)
                            <li @if(Request::segment(2) == "student-leave-application") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Leave Application</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-leave-application") class="active" @endif><a href="{{ url('/admin-panel/student-leave-application/add-leave-application') }}">Add Leave Application </a></li>
                                    <li @if(Request::segment(3) == "view-leave-application") class="active" @endif><a href="{{ url('/admin-panel/student-leave-application/view-leave-application') }}">View Leave Application </a></li>
                                </ul> 
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    
                    
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                    <li class="javascript:void(0); @if(Request::segment(2) == "staff-role" || Request::segment(2) == "staff" ) active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Staff</strong></span>

                    </a>
                        <ul class="sub-menu">
                           <!-- For Staff Role Menu -->
                           <!-- @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "staff-role") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Staff Roles</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-staff-role") class="active" @endif><a href="{{ url('/admin-panel/staff-role/add-staff-role') }}">Add Staff Role </a></li>
                                    <li @if(Request::segment(3) == "view-staff-roles") class="active" @endif><a href="{{ url('/admin-panel/staff-role/view-staff-roles') }}">View Staff Roles </a></li>
                                </ul> 
                            </li>
                            @endif -->

                            <!-- For Staff Menu -->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "staff") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Staff</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-staff") class="active" @endif><a href="{{ url('/admin-panel/staff/add-staff') }}">Add Staff</a></li>
                                    <li @if(Request::segment(3) == "view-staff") class="active" @endif><a href="{{ url('/admin-panel/staff/view-staff') }}">View Staff</a></li>
                                </ul> 
                            </li>
                            @endif

                          
                        </ul>
                    </li>
                    @endif

                    
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 2)

                    <!-- Online Content Menu here -->
                    <li class="javascript:void(0); @if(Request::segment(2) == "notes" || Request::segment(2) == "question-paper" ) active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Online Content </strong></span>
                    </a>
                        <ul class="sub-menu">
                            <!-- For Class Menu -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li class="@if(Request::segment(2) == "notes") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Notes</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-notes") class="active" @endif><a href="{{ url('/admin-panel/notes/add-notes') }}">Add Notes </a></li>
                                    <li @if(Request::segment(3) == "view-notes") class="active" @endif><a href="{{ url('/admin-panel/notes/view-notes') }}">View Notes </a></li>
                                </ul>
                            </li>
                            @endif
                            <!-- For online content @Ashish 21 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 2)
                            <!-- <li class="@if(Request::segment(2) == "question-paper") active open @endif"><a href="javascript:void(0);" class="menu-toggle "><i class="zmdi zmdi-accounts-outline"></i><span>Question Paper</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-question-paper") class="active" @endif><a href="{{ url('/admin-panel/question-paper/add-question-paper') }}">Add Question Paper </a></li>
                                    <li @if(Request::segment(3) == "view-question-paper") class="active" @endif><a href="{{ url('/admin-panel/question-paper/view-question-paper') }}">View Question Paper </a></li>
                                </ul>
                            </li> -->
                            @endif

                        </ul>
                    </li>
                    @endif
                    
                    <!-- For 9.Examination and Report Cards @Pratyush on 11 Aug 2018  -->
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                    <li class="javascript:void(0); @if(Request::segment(2) == "examination") active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Examination</strong></span>

                    </a>
                        <ul class="sub-menu">
                            <!-- For Tern Menu @Pratyush 11 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "manage-term") class="active" @endif><a href="{{ url('/admin-panel/examination/manage-term') }}" ><i class="zmdi zmdi-accounts-outline"></i><span>Manage Term</span> </a>
                                <!-- <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-term") class="active" @endif><a href="{{ url('/admin-panel/term/create-term') }}">Create Term</a></li>
                                     <li @if(Request::segment(3) == "view-terms") class="active" @endif><a href="{{ url('/admin-panel/term/view-terms') }}">View Terms</a></li>
                                </ul>  -->
                            </li>
                            @endif

                            <!-- For Exam Menu @Pratyush 11 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "manage-exam") class="active" @endif><a href="{{ url('/admin-panel/examination/manage-exam') }}" ><i class="zmdi zmdi-accounts-outline"></i><span>Manage Exam</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-exam") class="active" @endif><a href="{{ url('/admin-panel/exam/create-exam') }}">Add Exam</a></li>
                                    <li @if(Request::segment(3) == "view-exams") class="active" @endif><a href="{{ url('/admin-panel/exam/view-exams') }}">View Exam</a></li>
                                </ul> 
                            </li>
                            @endif

                          
                        </ul>
                    </li>
                    @endif

                    @php /*
                    <!-- For Library @Pratyush on 13 Aug 2018  -->
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                    <li class="javascript:void(0); @if(Request::segment(2) == "book-category" || Request::segment(2) == "book-vendor" || Request::segment(2) == "book-cupboard" || Request::segment(2) == "cupboard-shelf" || Request::segment(2) == "book-allowance" || Request::segment(2) == "book" || Request::segment(2) == "member" || Request::segment(2) == "issue-book" || Request::segment(2) == "return-book" || Request::segment(2) == "library-fine") active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Library</strong></span>

                    </a>
                        <ul class="sub-menu">
                            <!-- For Book Category Menu @Pratyush 13 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "book-category") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Book Category</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-book-category") class="active" @endif><a href="{{ url('/admin-panel/book-category/add-book-category') }}">Add Book Category</a></li>
                                    <li @if(Request::segment(3) == "view-book-categories") class="active" @endif><a href="{{ url('/admin-panel/book-category/view-book-categories') }}">View Book Category</a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Book Allowance Menu @Pratyush 13 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "book-allowance") class="active open" @endif><a href="{{ url('/admin-panel/book-allowance/edit-book-category') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Book Allowance</span> </a>
                            </li>
                            @endif

                            <!-- For Book Vendor Menu @Pratyush 13 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "book-vendor") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Book Vendor</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-book-vendor") class="active" @endif><a href="{{ url('/admin-panel/book-vendor/add-book-vendor') }}">Add Book Vendor</a></li>
                                    <li @if(Request::segment(3) == "view-book-vendors") class="active" @endif><a href="{{ url('/admin-panel/book-vendor/view-book-vendors') }}">View Book Vendor</a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For CupBoard Menu @Pratyush 13 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "book-cupboard") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>CupBoard</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-book-cupboard") class="active" @endif><a href="{{ url('/admin-panel/book-cupboard/add-book-cupboard') }}">Add CupBoard</a></li>
                                    <li @if(Request::segment(3) == "view-book-cupboard") class="active" @endif><a href="{{ url('/admin-panel/book-cupboard/view-book-cupboard') }}">View CupBoard</a></li>
                                </ul> 
                            </li>
                            @endif


                            <!-- For CupBoard Shelf Menu @Pratyush 13 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "cupboard-shelf") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>CupBoard Shelf</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-cupboard-shelf") class="active" @endif><a href="{{ url('/admin-panel/cupboard-shelf/add-cupboard-shelf') }}">Add CupBoard Shelf</a></li>
                                    <li @if(Request::segment(3) == "view-cupboard-shelf") class="active" @endif><a href="{{ url('/admin-panel/cupboard-shelf/view-cupboard-shelf') }}">View CupBoard Shelf</a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Book Menu @Pratyush 14 Aug 2018-->
                           @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "book") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Book</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-book") class="active" @endif><a href="{{ url('/admin-panel/book/add-book') }}">Add Book</a></li>
                                    <li @if(Request::segment(3) == "view-books") class="active" @endif><a href="{{ url('/admin-panel/book/view-books') }}">View Books</a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Library member Add By Pratyush on 19 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "member") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Members</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "register-student") class="active" @endif><a href="{{ url('/admin-panel/member/register-student') }}">Register Student</a></li>
                                    <li @if(Request::segment(3) == "view-students") class="active" @endif><a href="{{ url('/admin-panel/member/view-students') }}">View Student</a></li>
                                    <li @if(Request::segment(3) == "register-staff") class="active" @endif><a href="{{ url('/admin-panel/member/register-staff') }}">Register Staff</a></li>
                                    <li @if(Request::segment(3) == "view-staff") class="active" @endif><a href="{{ url('/admin-panel/member/view-staff') }}">View Staff</a></li>
                                </ul> 
                            </li>
                            @endif

                            <!-- For Library Issue Book Add By Pratyush on 21 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "issue-book") class="active" @endif><a href="{{ url('/admin-panel/issue-book') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Issue Book</span> </a>
                            </li>
                            @endif

                            <!-- For Library Issue Return / Renew / Update Add By Pratyush on 22 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "return-book") class="active" @endif><a href="{{ url('/admin-panel/return-book') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Return/Renew Book</span> </a>
                            </li>
                            @endif

                            <!-- For Library Fine Add By Pratyush on 23 Aug 2018 -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                            <li @if(Request::segment(2) == "library-fine") class="active" @endif><a href="{{ url('/admin-panel/library-fine') }}"><i class="zmdi zmdi-accounts-outline"></i><span>Fine</span> </a>
                            </li>
                            @endif

                        </ul>
                    </li>
                    @endif
                    */ @endphp

                    <!-- Admission Menu here -->
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 4)
                    <li class="javascript:void(0); @if(Request::segment(2) == "brochure" || Request::segment(2) == "admission-form" ) active open @endif"><a class="menu-toggle menu-toggle waves-effect waves-block toggled"><i class="zmdi zmdi-home"></i><span><strong>Admission</strong></span>
                    </a>
                        <ul class="sub-menu ">
                            <!-- For Brochure module -->
                            @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 4)
                            <li @if(Request::segment(2) == "brochure") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Brochure</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-brochure") class="active" @endif><a href="{{ url('/admin-panel/brochure/add-brochure') }}">Add Brochure </a></li>
                                    <li @if(Request::segment(3) == "view-brochures") class="active" @endif><a href="{{ url('/admin-panel/brochure/view-brochures') }}">View Brochures </a></li>
                                </ul> 
                            </li>
                            @endif

                             <!-- For Admission form module -->
                             @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['admin_type'] == 4)
                            <li @if(Request::segment(3) == "add-admission-form" || Request::segment(3) == "view-admission-forms") class="active open" @endif><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-outline"></i><span>Admission Form</span> </a>
                                <ul class="ml-menu">
                                    <li @if(Request::segment(3) == "add-admission-form") class="active" @endif><a href="{{ url('/admin-panel/admission-form/add-admission-form') }}">Add Admission Form </a></li>
                                    <li @if(Request::segment(3) == "view-admission-forms") class="active" @endif><a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}">View Admission Form </a></li>

                                    
                                </ul> 
                            </li>
                            <li @if(Request::segment(3) == "manage-enquiries") class="active" @endif><a href="{{ url('/admin-panel/admission-form/manage-enquiries') }}">Manage Enquiries </a></li>

                            <li @if(Request::segment(3) == "manage-admissions") class="active" @endif><a href="{{ url('/admin-panel/admission-form/manage-admissions') }}">Manage Admissions </a></li>

                            <li @if(Request::segment(3) == "selected-students" || Request::segment(4) == "view-students") class="active" @endif><a href="{{ url('/admin-panel/admission-form/selected-students') }}">Selected Students </a></li>

                            @endif
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
       
    </div>    
</aside>