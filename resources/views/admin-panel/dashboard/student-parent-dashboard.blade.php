@extends('admin-panel.layout.header')

@section('content')
<div class="chat-launcher"></div>
<div class="chat-wrapper">
    <div class="card">
        <div class="header">
            <ul class="list-unstyled team-info margin-0">
                <li class="m-r-15"><h2>Pro. Team</h2></li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar4.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar6.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <a href="javascript:void(0);" title="Add Member"><i class="zmdi zmdi-plus-circle"></i></a>
                </li>
            </ul>                       
        </div>
        <div class="body form-gap">
            <div class="chat-widget">
            <ul class="chat-scroll-list clearfix">
                <li class="left float-left">
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info">
                        <a class="name" href="javascript:void(0);">Alexander</a>
                        <span class="datetime">6:12</span>                            
                        <span class="message">Hello, John </span>
                    </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:15</span> <span class="message">Hi, Alexander<br> How are you!</span> </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:16</span> <span class="message">There are many variations of passages of Lorem Ipsum available</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Elizabeth</a> <span class="datetime">6:25</span> <span class="message">Hi, Alexander,<br> John <br> What are you doing?</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar1.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Michael</a> <span class="datetime">6:28</span> <span class="message">I would love to join the team.</span> </div>
                </li>
                    <li class="right">
                    <div class="chat-info"><span class="datetime">7:02</span> <span class="message">Hello, <br>Michael</span> </div>
                </li>
            </ul>
            </div>
            <div class="input-group p-t-15">
                <input type="text" class="form-control" placeholder="Enter text here...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-mail-send"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Dashboard
                <small>Welcome to {!!trans('language.project_title') !!}</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="row clearfix">
                @if($login_info['admin_type'] == 3)
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Attendance</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$student_attend_count}}" data-speed="2500" data-fresh-interval="700">{{$student_attend_count}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Paid</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$student_fees_paid}}" data-speed="4000" data-fresh-interval="700">{{$student_fees_paid}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Due</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$total_due_amount}}" data-speed="4000" data-fresh-interval="700">{{$total_due_amount}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif   
                </div>
                <div class="row col-lg-12 padding-0" style="margin:0px;">
                    <div class="col-lg-6" style="padding-left:0px">
                        <div id="attendanceChart" style="height: 300px; width: 100%;"></div>
                    </div>  
                    <div class="col-lg-6" style="padding-right:0px">
                        <div id="classFeesPieChart" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>
            
                @if($login_info['admin_type'] == 3 || $login_info['admin_type'] == 4)
                <div class="col-lg-12 padding-0"  style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Notice List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap1">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="notice-table">
                                    <thead>
                                        <tr>
                                            <th>Notice Name</th>
                                            <th>Notice Text</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($class_notice_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['notice_name']}}</span></td>
                                            <td><div class="comment_notice more">{{$record['notice_text']}}</div></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                <div class="card student-list">
                    <div class="header">
                        <h2><strong>New</strong> Feeds List <small>Description text here...</small></h2>
                    </div>
                    <div class="body form-gap1">
                        <div class="table-responsive" >
                            <table class="table table-hover m-b-0" id="feeds-table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Joseph</td>
                                        <td>test</td>
                                    </tr>
                                    <tr>
                                        <td>Cameron</td>
                                        <td>test</td>
                                    </tr>
                                    <tr>
                                        <td>Alex</td>
                                        <td>test</td>
                                    </tr>
                                    <tr>
                                        <td>James</td>
                                        <td>test</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Online Content <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap1">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="online-content-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Subject Name</th>
                                            <th>Unit Name</th>
                                            <th>Unit Topic</th>
                                            <th>Note URL</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($online_content as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['online_note_name']}}</span></td>
                                            <td>{{$record['getSubject']['subject_name']}}</td>
                                            <td>{{$record['online_note_unit']}}</td>
                                            <td>{{$record['online_note_topic']}}</td>
                                            <td><a href="../public/uploads/online-notes/{{$record['online_note_url']}}">View File</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                
            </div>
            
        </div>        
    </div>
</section>
<script>
window.onload = function () {

var options = {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Attendance Graph",
        padding:20
	},
	axisX:{
        margin:10,
		valueFormatString: "DD MMM"
	},
	axisY: {
        margin:10,
		title: "Number of Student",
		suffix: "K",
		minimum: 30
	},
	toolTip:{
		shared:true
	},  
	legend:{
		cursor:"pointer",
		verticalAlign: "bottom",
		horizontalAlign: "left",
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		showInLegend: true,
		name: "Day Wise",
		markerType: "square",
		xValueFormatString: "DD MMM, YYYY",
		color: "#F08080",
		yValueFormatString: "#,##0K",
		dataPoints: [
			{ x: new Date(2017, 01, 1), y: 63 },
			{ x: new Date(2017, 01, 2), y: 69 },
			{ x: new Date(2017, 01, 3), y: 65 },
			{ x: new Date(2017, 01, 4), y: 70 },
			{ x: new Date(2017, 01, 5), y: 71 },
			{ x: new Date(2017, 01, 6), y: 65 },
			{ x: new Date(2017, 01, 7), y: 73 },
			{ x: new Date(2017, 01, 8), y: 96 },
			{ x: new Date(2017, 01, 9), y: 84 },
			{ x: new Date(2017, 01, 10), y: 85 },
			{ x: new Date(2017, 01, 11), y: 86 },
			{ x: new Date(2017, 01, 12), y: 94 }
		]
	}]
};
$("#attendanceChart").CanvasJSChart(options);

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

var options1 = {
	title: {
		text: "Subjects Wise Percantage Graph",
        padding:20
	},
	data: [{
			type: "pie",
			startAngle: 45,
			showInLegend: "true",
			legendText: "{label}",
			indexLabel: "{label} ({y})",
			yValueFormatString:"#,##0.#"%"",
			dataPoints: [
				{ label: "Subject-I", y: 36 },
				{ label: "Subject-II", y: 31 },
				{ label: "Subject-III", y: 7 },
				{ label: "Subject-IV", y: 7 },
				{ label: "Subject-V", y: 6 },
				{ label: "Subject-VI", y: 10 }
			]
	}]
};
$("#classFeesPieChart").CanvasJSChart(options1);
}

$(document).ready(function () {
    var table = $('#notice-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#feeds-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#online-content-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "15%"
            },
            {
                "targets": 2, // your case first column
                "width": "25%"
            },
            {
                "targets": 3, // your case first column
                "width": "30%"
            },
            {
                targets: [ 0, 1, 2, 3, 4],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
});

$(document).ready(function() {
	var showChar = 200;
	var ellipsestext = "...";
	var moretext = "Read more";
	var lesstext = "Read less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		} 
	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
</script>
@endsection

