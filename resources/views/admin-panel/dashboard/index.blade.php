@extends('admin-panel.layout.header')

@section('content')
<div class="chat-launcher"></div>
<div class="chat-wrapper">
    <div class="card">
        <div class="header">
            <ul class="list-unstyled team-info margin-0">
                <li class="m-r-15"><h2>Pro. Team</h2></li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar4.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar6.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <a href="javascript:void(0);" title="Add Member"><i class="zmdi zmdi-plus-circle"></i></a>
                </li>
            </ul>                       
        </div>
        <div class="body form-gap">
            <div class="chat-widget">
            <ul class="chat-scroll-list clearfix">
                <li class="left float-left">
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info">
                        <a class="name" href="javascript:void(0);">Alexander</a>
                        <span class="datetime">6:12</span>                            
                        <span class="message">Hello, John </span>
                    </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:15</span> <span class="message">Hi, Alexander<br> How are you!</span> </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:16</span> <span class="message">There are many variations of passages of Lorem Ipsum available</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Elizabeth</a> <span class="datetime">6:25</span> <span class="message">Hi, Alexander,<br> John <br> What are you doing?</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar1.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Michael</a> <span class="datetime">6:28</span> <span class="message">I would love to join the team.</span> </div>
                </li>
                    <li class="right">
                    <div class="chat-info"><span class="datetime">7:02</span> <span class="message">Hello, <br>Michael</span> </div>
                </li>
            </ul>
            </div>
            <div class="input-group p-t-15">
                <input type="text" class="form-control" placeholder="Enter text here...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-mail-send"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Dashboard
                <small>Welcome to {!!trans('language.project_title') !!}</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Present Student</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['student']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                                <div class="content">
                                    <div class="text">Present Staff</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff']}}" data-speed="4000" data-fresh-interval="700">{{$dashboard['staff']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Total Admission</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['total_admission']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_admission']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Parent Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['parent_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['parent_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Student Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['student_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Staff Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['staff_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance-wallet"></i> </div>
                                <div class="content">
                                    <div class="text">Expenses</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$dashboard['total_expense']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_expense']}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Collected</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$fees_collected}}" data-speed="2500" data-fresh-interval="700">{{$fees_collected}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="row col-lg-12 padding-0" style="margin:0px;">
                    <div class="col-lg-6" style="padding-left:0px">
                        <div id="feesLineChart" style="height: 300px; width: 100%;"></div>
                    </div>
                    <div class="col-lg-6" style="padding-right:0px">
                        <div id="classFeesPieChart" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>
    
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Admission List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="admission-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($admission_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Notice List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="notice-table">
                                    <thead>
                                        <tr>
                                            <th>Notice Name</th>
                                            <th>Notice Text</th>
                                            <th>For Class</th>
                                            <th>For Staff</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($notice_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['notice_name']}}</span></td>
                                            <td>
                                                <div class="comment more">{{$record['notice_text']}}</div>
                                            </td>
                                            @if($record->for_class == 0)
                                            <td>For All Class</td>
                                            @else
                                            <td>{{get_selected_classes($record->class_ids)}}</td>
                                            @endif
                                            @if($record->for_staff == 0)
                                            <td>For All Staff</td>
                                            @else
                                            <td>{{get_selected_staff($record->staff_ids)}}</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Enquiry List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="enquiry-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($enquiry_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                   </tbody>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Visitor List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="visitor-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Purpose</th>
                                            <th>Contact No</th>
                                            <th>Visit Date</th>
                                            <th>Timing</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($visitor_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['name']}}</span></td>
                                            <td>{{$record['purpose']}}</td>
                                            <td>{{$record['contact']}}</td>
                                            <td>{{$record['visit_date']}}</td>
                                            <td><span class="badge badge-primary">{{$record['check_in_time']. '-' .$record['check_out_time']}}</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>        
    </div>
</section>
<script>
window.onload = function () {

var options = {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Fees Graph",
        padding:20
	},
	axisX:{
		valueFormatString: "MMM"
	},
	axisY: {
		title: "Number of Student",
		suffix: "K",
		minimum: 30
	},
	toolTip:{
		shared:true
	},  
	legend:{
		cursor:"pointer",
		verticalAlign: "bottom",
		horizontalAlign: "left",
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		showInLegend: true,
		name: "Month Fees",
		markerType: "square",
		xValueFormatString: "MMM, YYYY",
		color: "#F08080",
		yValueFormatString: "#,##0K",
		dataPoints: [
			{ x: new Date(2017, 01, 1), y: 63 },
			{ x: new Date(2017, 02, 1), y: 69 },
			{ x: new Date(2017, 03, 1), y: 65 },
			{ x: new Date(2017, 04, 1), y: 70 },
			{ x: new Date(2017, 05, 1), y: 71 },
			{ x: new Date(2017, 06, 1), y: 65 },
			{ x: new Date(2017, 07, 1), y: 73 },
			{ x: new Date(2017, 08, 1), y: 96 },
			{ x: new Date(2017, 09, 1), y: 84 },
			{ x: new Date(2017, 10, 1), y: 85 },
			{ x: new Date(2017, 11, 1), y: 86 },
			{ x: new Date(2017, 12, 1), y: 94 }
		]
	}]
};
$("#feesLineChart").CanvasJSChart(options);

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

var options1 = {
	title: {
		text: "Class Wise Fees Graph",
        padding:20
	},
	data: [{
			type: "pie",
			startAngle: 45,
			showInLegend: "true",
			legendText: "{label}",
			indexLabel: "{label} ({y})",
			yValueFormatString:"#,##0.#"%"",
			dataPoints: [
				{ label: "Class-I", y: 36 },
				{ label: "Class-II", y: 31 },
				{ label: "Class-III", y: 7 },
				{ label: "Class-IV", y: 7 },
				{ label: "Class-V", y: 6 },
				{ label: "Class-VI", y: 10 },
				{ label: "Class-VII", y: 3 }
			]
	}]
};
$("#classFeesPieChart").CanvasJSChart(options1);
}

$(document).ready(function () {
    var table = $('#notice-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#visitor-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#enquiry-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#admission-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
});

$(document).ready(function() {
	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "Read more";
	var lesstext = "Read less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		} 
	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
</script>
@endsection

