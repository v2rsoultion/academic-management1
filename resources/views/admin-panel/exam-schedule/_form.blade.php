@if(isset($schedule['exam_schedule_id']) && !empty($schedule['exam_schedule_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('exam_schedule_id',old('exam_schedule_id',isset($schedule['exam_schedule_id']) ? $schedule['exam_schedule_id'] : ''),['class' => 'gui-input', 'id' => 'exam_schedule_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info Schedule -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.schedule_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('schedule_name', old('schedule_name',isset($schedule['schedule_name']) ? $schedule['schedule_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.schedule_name'), 'id' => 'schedule_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.exam_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('exam_id', $schedule['arr_exam'],isset($schedule['exam_id']) ? $schedule['exam_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
   
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination/schedules') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#schedule-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                schedule_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                exam_id: {
                    required: true
                }
            },
            /* @validation highlighting + error placement---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>