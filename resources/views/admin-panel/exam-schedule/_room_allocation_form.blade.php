
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

{!! Form::hidden('exam_schedule_id',old('exam_schedule_id',isset($scheduleMap['exam_schedule_id']) ? $scheduleMap['exam_schedule_id'] : ''),['class' => 'gui-input', 'id' => 'exam_schedule_id', 'readonly' => 'true']) !!}


{!! Form::hidden('exam_id',old('exam_id',isset($scheduleMap['exam_id']) ? $scheduleMap['exam_id'] : ''),['class' => 'gui-input', 'id' => 'exam_id', 'readonly' => 'true']) !!}


{!! Form::hidden('class_id',old('class_id',isset($scheduleMap['class_id']) ? $scheduleMap['class_id'] : ''),['class' => 'gui-input', 'id' => 'class_id', 'readonly' => 'true']) !!}

{!! Form::hidden('section_id',old('section_id',isset($scheduleMap['section_id']) ? $scheduleMap['section_id'] : ''),['class' => 'gui-input', 'id' => 'section_id', 'readonly' => 'true']) !!}


{!! Form::hidden('hide',isset($scheduleMap['room_map']) ? COUNT($scheduleMap['room_map']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="alert alert-success" role="alert" id = "detail_success" style="display: none">
</div>
    @if(isset($scheduleMap['room_map']) && !empty($scheduleMap['room_map']))
    @php  $key = 0; @endphp
    @foreach($scheduleMap['room_map'] as $scheduleMapKey => $scheduleMapVal)
    @php  
        $schedule_room_map_id = "room[".$key."][schedule_room_map_id]"; 
    @endphp
    <div id="detail_block{{$key}}">
        {!! Form::hidden($schedule_room_map_id,$scheduleMapVal['schedule_room_map_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.roll_no_from') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('room['.$key.'][roll_no_from]', $scheduleMapVal['roll_no_from'], ['class' => 'form-control','placeholder'=>trans('language.roll_no_from'), 'id' => 'roll_no_from'.$key.'', 'required', 'min'=>'1']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.roll_no_to') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('room['.$key.'][roll_no_to]',$scheduleMapVal['roll_no_to'], ['class' => 'form-control','placeholder'=>trans('language.roll_no_to'), 'id' => 'roll_no_to'.$key.'', 'required','min'=>'1']) !!}
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.room_no') !!} <span class="red-text">*</span>  :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select('room['.$key.'][room_no_id]', $scheduleMap['arr_room_no'], isset($scheduleMapVal['room_no_id']) ? $scheduleMapVal['room_no_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDetailBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$scheduleMapVal['schedule_room_map_id'],$key}})" >Remove</button>
                @endif
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.roll_no_from') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('room[0][roll_no_from]', '', ['class' => 'form-control','placeholder'=>trans('language.roll_no_from'), 'id' => 'roll_no_from0', 'required', 'min'=>'1']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.roll_no_to') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('room[0][roll_no_to]', '', ['class' => 'form-control ','placeholder'=>trans('language.roll_no_to'), 'id' => 'roll_no_to0', 'required', 'min'=> '1']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.room_no') !!} <span class="red-text">*</span>  :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('room[0][room_no_id]', $scheduleMap['arr_room_no'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'room_no_id0'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDetailBlock()" >Add More</button></div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination/schedules') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#room-allocation-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                roll_no_from: {
                    required: true,
                    digits: true
                },
                roll_no_to: {
                    required: true,
                    digits: true
                },
                room_no_id: {
                    required: true
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        

    });
    function addDetailBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b);
        
        $("#sectiontable-body").append('<div id="detail_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.roll_no_from') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"><input type="number" name="room['+arr_key+'][roll_no_from]" id="roll_no_from'+arr_key+'" class="form-control" placeholder="{{trans('language.roll_no_from')}}" required min="1" /></div></div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.roll_no_to') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"> <input type="number" name="room['+arr_key+'][roll_no_to]" id="roll_no_to'+arr_key+'" class="form-control" placeholder="{{trans('language.roll_no_to')}}" required min="1" /></div></div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.room_no') !!} <span class="red-text">*</span> :</lable><div class="form-group m-bottom-0"><label class=" field select" style="width: 100%"><select name="room['+arr_key+'][room_no_id]" id="room_no_id'+arr_key+'" required class="form-control show-tick select_form1 select2" > @foreach($scheduleMap['arr_room_no'] as $key => $value)<option value="{{$key}}">{{$value}}</option> @endforeach</select><i class="arrow double"></i> </label> </div> </div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div></div>');
        $('#room_no_id'+arr_key+'').select2().on('change', function(e, data){ $(this).valid(); });
    }
    function remove_block(id){
        $("#detail_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function remove_record(schedule_room_map_id,block_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/schedules/remove-room')}}/"+schedule_room_map_id,
                success: function (data) {
                    $("#detail_success").html(data+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>');
                    $(".mycustloading").hide();
                    $("#detail_block"+block_id).remove();
                }
            });
        }
    }
   
</script>

