@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/schedules') !!}">{!! trans('language.schedules') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    {!! Form::hidden('exam_schedule_id',$schedule->exam_schedule_id, ['id' => 'exam_schedule_id']) !!}
                                    {!! Form::hidden('exam_id',$schedule->exam_id, ['id' => 'exam_id']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $schedule['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>

                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $schedule['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                       
                                        
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="schedule-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.class_name')}}</th>
                                            <th>{{trans('language.section_name')}}</th>
                                            <th>{{trans('language.schedule_date_range')}}</th>
                                            <th>Subjects</th>
                                            <th>Room Allocation</th>
                                            <th>Publish</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>


<div class="modal fade" id="subjectsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
           
            <table class="table m-b-0 c_list" id="subjects-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.teachers') !!}</th>
                        <th>{!! trans('language.exam_date') !!}</th>
                        <th>{!! trans('language.schedule_time_range') !!}</th>
                    </tr>
                </thead>
            </table>
            
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var exam_id = $("#exam_id").val();
        var exam_schedule_id = $("#exam_schedule_id").val();
        var table = $('#schedule-table').DataTable({
            
            // dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'pdf','csvHtml5'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-schedule-data')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = $('select[name="class_id"]').val();
                    d.section_id = $('select[name="section_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_name', name: 'class_name'},
                {data: 'section_name', name: 'section_name'},
                {data: 'date_range', name: 'date_range'},
                {data: 'details', name: 'details'},
                {data: 'room_allocation', name: 'room_allocation'},
                {data: 'publish_notify', name: 'publish_notify'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })

        $(document).on('click','.publish',function(e){
            var exam_id = $("#exam_id").val();
            var exam_schedule_id = $(this).attr('exam-schedule-id');
            var class_id = $(this).attr('class-id');
            var section_id = $(this).attr('section-id');
            if(exam_id != "" && class_id != "" && section_id != "" && exam_schedule_id != "") {
                $(".mycustloading").show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/examination/schedule/publish-schedule')}}",
                    type: 'GET',
                    data: {
                        'class_id': class_id,
                        'section_id': section_id,
                        'exam_id': exam_id,
                        'exam_schedule_id': exam_schedule_id
                    },
                    success: function (data) {
                        table.draw();
                        $(".mycustloading").hide();
                    
                    }
                });
            } else {
                table.draw();
                $(".mycustloading").hide();
            }

        })
    });

    function getSection(class_id)
    {
        var exam_id = $("#exam_id").val();
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/get-map-sections')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'exam_id': exam_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    $(document).on('click','.subjects',function(e){
        var exam_id = $("#exam_id").val();
        var exam_schedule_id = $(this).attr('exam-schedule-id');
        var class_id = $(this).attr('class-id');
        var section_id = $(this).attr('section-id');
        $('#subjectsModel').modal('show');
        var table1 = $('#subjects-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-schedule-subjects')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'subject_name', name: 'subject_name'},
                {data: 'teachers', name: 'teachers'},
                {data: 'exam_date', name: 'exam_date'},
                {data: 'time_range', name: 'time_range'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    })
</script>
@endsection




