<div style="border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
  <div>
    <b>{!! trans('language.exam_total_mark') !!} : </b> 
    <span id="sys_invoice_no"> {{ $result_date_time['total_marks'] }} </span>
  </div>

  <div>
    <b>{!! trans('language.exam_obtain_mark') !!} : </b> 
    <span id="sys_invoice_no"> {{ $result_date_time['obtain_marks'] }} </span>
  </div>

  <div>
    <b>{!! trans('language.exam_percentage') !!} : </b> 
    <span id="sys_invoice_no"> {{ $result_date_time['percentage'] }} % </span>
  </div>

  <div>
    <b>{!! trans('language.class_rank') !!} : </b> 
    <span id="sys_invoice_no"> {{ $result_date_time['class_rank'] }} Rank</span>
  </div>

  <div>
    <b>{!! trans('language.section_rank') !!} : </b> 
    <span id="sys_invoice_no"> {{ $result_date_time['section_rank'] }} Rank</span>
  </div>

  <div>
    <b>{!! trans('language.pass_fail_status') !!} : </b> 
    <span id="sys_invoice_no"> @if($result_date_time->pass_fail == 0) Fail @else Pass @endif </span>
  </div>
  
</div>
