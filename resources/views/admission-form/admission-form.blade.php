<!DOCTYPE html>
<html>
<head>
	<title>Admission Form</title>
	
	{!! Html::style('public/web/css/style.css') !!}
	{!! Html::style('public/admin/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('public/admin/assets/css/main.css') !!}
    {!! Html::style('public/admin/assets/css/style1.css') !!}
    {!! Html::script('public/admin/assets/js/jquery.min.js') !!}
    {!! Html::script('public/admin/assets/js/bootstrap-tagsinput.min.js') !!}
    {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!}
    {!! Html::style('public/admin/assets/css/select2.min.css') !!}

</head>

<style type="text/css">
	.btn-primary {
	    background-color: #1f2f60 !important;
	}
</style>
<body>
<div class="header">
{!! Form::open(['files'=>TRUE,'id' => 'enquiry-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
<table style="width: 100%;">
    <tr>
        <td style="width: 4%;">
            <img src="{!! URL::to('public/uploads/school-logo/'.$school_data['school_logo']) !!}" alt="" width="120px">
        </td>
        <td style="width: 25%;" class="align">
            <h2 style="margin:0px;"> {{ $school_data['school_name'] }}</h2>	
            <p class="less" style="font-size: 12px;margin-top: 0px;margin-bottom: 2px;">
            <u style="font-size: 13px;">
                {{ $school_data['school_address'] }} 
                @if( $school_data['city_name'] != "" ) , {{ $school_data['city_name'] }} @endif
            </u><br> 
                @if( $school_data['school_telephone'] != "" ) Phone: {{ $school_data['school_telephone'] }} @endif
                @if( $school_data['school_fax_number'] != "" ) Fax: {{ $school_data['school_fax_number'] }} @endif
            &nbsp;&nbsp; 
            <br>E-mail : {{ $school_data['school_email'] }} </p>	
        </td>
        <td style="border:1px solid #000;width:4%;height: 130px;">
            <div class="avatar-upload">
                <div class="avatar-edit">
                    <input type='file' id="imageUpload" name="staff_profile_img" accept=".png, .jpg, .jpeg" />
                    <label for="imageUpload"> <i class="zmdi zmdi-edit" style="margin-left: 10px;"></i> </label>
                </div>
                <div class="avatar-preview">
                    <div id="imagePreview"></div>
                </div>
            </div>
        </td>
    </tr>
</table>
<hr>
@if(isset($admissionForm['form_type']) && $admissionForm['form_type'] == 1)
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

@if(!isset($admissionForm['admission_class_id']) && empty($admissionForm['admission_class_id']))
<?php $admit_section_readonly = false; $admit_section_disabled='disabled'; ?>
@else
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@endif

@if(!isset($admissionForm['current_class_id']) && empty($admissionForm['current_class_id']))
<?php $current_section_readonly = false; $current_section_disabled='disabled'; ?>
@else
<?php $current_section_readonly = false; $current_section_disabled=''; ?>
@endif

{!! Form::hidden('admission_form_id',old('admission_form_id',isset($admissionForm['admission_form_id']) ? $admissionForm['admission_form_id'] : ''),['class' => 'gui-input', 'id' => 'admission_form_id', 'readonly' => 'true']) !!}

{!! Form::hidden('admission_data_id',old('admission_data_id',isset($admissionForm['admission_data_id']) ? $admissionForm['admission_data_id'] : ''),['class' => 'gui-input', 'id' => 'admission_data_id', 'readonly' => 'true']) !!}


{!! Form::hidden('medium_type',old('medium_type',isset($admissionForm['medium_type']) ? $admissionForm['medium_type'] : ''),['class' => 'gui-input', 'id' => 'medium_type', 'readonly' => 'true']) !!}


{!! Form::hidden('form_type',old('form_type',isset($admissionForm['form_type']) ? $admissionForm['form_type'] : ''),['class' => 'gui-input', 'id' => 'form_type', 'readonly' => 'true']) !!}

{!! Form::hidden('class_id',old('form_type',isset($admissionForm['class_id']) ? $admissionForm['class_id'] : ''),['class' => 'gui-input', 'id' => 'class_id', 'readonly' => 'true']) !!}


{!! Form::hidden('session_id',old('form_type',isset($admissionForm['session_id']) ? $admissionForm['session_id'] : ''),['class' => 'gui-input', 'id' => 'session_id', 'readonly' => 'true']) !!}

<div style="margin-top: 20px;">
@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
</div>

<!-- Academic Information -->
<!-- <div class="header">
    <h2><strong>Academic</strong> Information</h2>
</div> -->

@if($admissionForm['form_type'] == 0)
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.enquiry_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group field select">
            {!! Form::text('enquiry_number', old('enquiry_number',isset($admissionForm['enquiry_number']) ? $admissionForm['enquiry_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.enquiry_number'), 'id' => 'enquiry_number']) !!}
        </div>
    </div>

     <div class="col-lg-1 col-md-1">
     <lable class="from_one1">&nbsp;</lable>
        {!! Form::button('Check', ['class' => 'btn btn-raised btn-round btn-primary ','onClick'=>'checkAvailability()','style'=>'margin-top: -2px']) !!}
    </div>
</div>
@endif
<div class="row clearfix">

    @if(in_array('admission_session_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_session_id', $admissionForm['arr_session'],isset($admissionForm['admission_session_id']) ? $admissionForm['admission_session_id'] : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('admission_class_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_class_id', $admissionForm['arr_class'],isset($admissionForm['current_class_id']) ? $admissionForm['current_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_class_id','onChange' => 'getSection(this.value,"1")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('admission_section_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_section_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('admission_section_id', $admissionForm['arr_section'],isset($admissionForm['admission_section_id']) ? $admissionForm['admission_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_section_id',$admit_section_disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
   
</div>



<div class="row clearfix">
    @if(in_array('current_session_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_session_id', $admissionForm['arr_session'],isset($admissionForm['current_session_id']) ? $admissionForm['current_session_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('current_class_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_class_id', $admissionForm['arr_class'],isset($admissionForm['current_class_id']) ? $admissionForm['current_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_class_id','onChange' => 'getSection(this.value,"2")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('current_section_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_section_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_section_id', $admissionForm['arr_section'],isset($admissionForm['current_section_id']) ? $admissionForm['current_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_section_id',$current_section_disabled,'onChange'=> 'checkSectionCapacity(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('stream_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.stream_name') !!}  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('stream_id', $admissionForm['arr_stream'],isset($admissionForm['stream_id']) ? $admissionForm['stream_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'stream_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

</div>

@if(in_array('student_name', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Basic Information:</h5>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    
    @if(in_array('student_enroll_number', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_enroll_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group field select">
            {!! Form::text('student_enroll_number', old('student_enroll_number',isset($admissionForm['student_enroll_number']) ? $admissionForm['student_enroll_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_enroll_number'), 'id' => 'student_enroll_number']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_name', old('student_name',isset($admissionForm['student_name']) ? $admissionForm['student_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_name'), 'id' => 'student_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_type', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_type') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_type', $admissionForm['arr_student_type'],isset($admissionForm['student_type']) ? $admissionForm['student_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_image', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.student_image')}} </span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="student_image" id="file1" onChange="document.getElementById('student_image').value = this.value;">
                <input type="hidden" class="gui-input" id="student_image" placeholder="Upload Photo" readonly>
            </label>
            <span>Photo size will be 180X180</span>
            @if(isset($admissionForm['student_image']))<br /><a href="{{url($admissionForm['student_image'])}}" target="_blank" >View Image</a>@endif
    </div>
    @endif
</div>

<div class="row clearfix">

    @if(in_array('student_reg_date', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_reg_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('student_reg_date', old('student_reg_date',isset($admissionForm['student_reg_date']) ? $admissionForm['student_reg_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_reg_date'), 'id' => 'student_reg_date']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_dob', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_dob') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('student_dob', old('student_dob',isset($admissionForm['student_dob']) ? $admissionForm['student_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_dob'), 'id' => 'student_dob']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_email', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_email', old('student_email',isset($admissionForm['student_email']) ? $admissionForm['student_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_email'), 'id' => 'student_email']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_gender', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_gender') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_gender', $admissionForm['arr_gender'],isset($admissionForm['student_gender']) ? $admissionForm['student_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_gender'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
</div>

<!-- Previous school Information -->
@if(in_array('student_privious_school', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Previous School Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_privious_school', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_school') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_school', old('student_privious_school',isset($admissionForm['student_privious_school']) ? $admissionForm['student_privious_school']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_school'), 'id' => 'student_privious_school']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_privious_class', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_class') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_class', old('student_privious_class',isset($admissionForm['student_privious_class']) ? $admissionForm['student_privious_class']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_class'), 'id' => 'student_privious_class']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_privious_tc_no', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_tc_no', old('student_privious_tc_no',isset($admissionForm['student_privious_tc_no']) ? $admissionForm['student_privious_tc_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_no'), 'id' => 'student_privious_tc_no']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_privious_tc_date', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::date('student_privious_tc_date', old('student_privious_tc_date',isset($admissionForm['student_privious_tc_date']) ? $admissionForm['student_privious_tc_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_date'), 'id' => 'student_privious_tc_date']) !!}
        </div>
    </div>
    @endif

</div>

<div class="row clearfix">

    @if(in_array('student_privious_result', $admissionForm['form_keys']))
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_privious_result') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_privious_result', old('student_privious_result',isset($admissionForm['student_privious_result']) ? $admissionForm['student_privious_result']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_result'), 'id' => 'student_privious_result', 'rows'=> 1]) !!}
        </div>
    </div>
    @endif
</div>

<!-- Other Information -->
@if(in_array('student_category', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Other Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_category', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_category') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_category', $admissionForm['arr_category'],isset($admissionForm['student_category']) ? $admissionForm['student_category'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_category', 'data-live-search'=>'true'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('caste_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.caste_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('caste_id', $admissionForm['arr_caste'],isset($admissionForm['caste_id']) ? $admissionForm['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('religion_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.religion_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('religion_id', $admissionForm['arr_religion'],isset($admissionForm['religion_id']) ? $admissionForm['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('nationality_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.nationality_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('nationality_id', $admissionForm['arr_natioanlity'],isset($admissionForm['nationality_id']) ? $admissionForm['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

</div>

<div class="row clearfix">

    @if(in_array('student_sibling_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_sibling_name', old('student_sibling_name',isset($admissionForm['student_sibling_name']) ? $admissionForm['student_sibling_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_sibling_name'), 'id' => 'student_sibling_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_sibling_class_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_class_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_sibling_class_id', $admissionForm['arr_class'],isset($admissionForm['student_sibling_class_id']) ? $admissionForm['student_sibling_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_sibling_class_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_adhar_card_number', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_adhar_card_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_adhar_card_number', old('student_adhar_card_number',isset($admissionForm['student_adhar_card_number']) ? $admissionForm['student_adhar_card_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_adhar_card_number'), 'id' => 'student_adhar_card_number']) !!}
        </div>
    </div>
    @endif

    @if(in_array('group_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.group_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('group_id', $admissionForm['arr_group'],isset($admissionForm['group_id']) ? $admissionForm['group_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'group_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('title_id', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.title_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('title_id', $admissionForm['arr_title'],isset($admissionForm['title_id']) ? $admissionForm['title_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'title_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

</div>


<!-- Guardian Information -->
@if(in_array('student_login_email', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Login Credentials:</h5>
</div>
@endif
<div class="row clearfix">

    @if(in_array('student_login_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_name', old('student_login_name',isset($admissionForm['student_login_name']) ? $admissionForm['student_login_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_name'), 'id' => 'student_login_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_login_email', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_email', old('student_login_email',isset($admissionForm['student_login_email']) ? $admissionForm['student_login_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_email'), 'id' => 'student_login_email', $disabled]) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_login_contact_no', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_contact_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_contact_no', old('student_login_contact_no',isset($admissionForm['student_login_contact_no']) ? $admissionForm['student_login_contact_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_contact_no'), 'id' => 'student_login_contact_no', 'maxlength'=> '10']) !!}
        </div>
    </div>
    @endif

</div>

<!-- Father Information -->
@if(in_array('student_father_name', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Father Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_father_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_name', old('student_father_name',isset($admissionForm['student_father_name']) ? $admissionForm['student_father_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_name'), 'id' => 'student_father_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_father_mobile_number', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_mobile_number', old('student_father_mobile_number',isset($admissionForm['student_father_mobile_number']) ? $admissionForm['student_father_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_mobile_number'), 'id' => 'student_father_mobile_number', 'maxlength'=> '10', 'onkeyup' => "check_parent_existance()"]) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_father_email', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_email', old('student_father_email',isset($admissionForm['student_father_email']) ? $admissionForm['student_father_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_email'), 'id' => 'student_father_email']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_father_annual_salary', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_annual_salary') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_father_annual_salary', $admissionForm['arr_annual_salary'],isset($admissionForm['student_father_annual_salary']) ? $admissionForm['student_father_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_father_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">

    @if(in_array('student_father_occupation', $admissionForm['form_keys']))
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_father_occupation') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_father_occupation', old('student_father_occupation',isset($admissionForm['student_father_occupation']) ? $admissionForm['student_father_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_occupation'), 'id' => 'student_father_occupation', 'rows'=> 2]) !!}
        </div>
    </div>
    @endif

</div>

<!-- Mother Information -->
@if(in_array('student_mother_name', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Mother Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_mother_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_name', old('student_mother_name',isset($admissionForm['student_mother_name']) ? $admissionForm['student_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_name'), 'id' => 'student_mother_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_mother_mobile_number', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_mobile_number', old('student_mother_mobile_number',isset($admissionForm['student_mother_mobile_number']) ? $admissionForm['student_mother_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_mobile_number'), 'id' => 'student_mother_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_mother_email', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_email', old('student_mother_email',isset($admissionForm['student_mother_email']) ? $admissionForm['student_mother_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_email'), 'id' => 'student_mother_email']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_mother_annual_salary', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_annual_salary') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_mother_annual_salary', $admissionForm['arr_annual_salary'],isset($admissionForm['student_mother_annual_salary']) ? $admissionForm['student_mother_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_mother_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_mother_tongue', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_tongue') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_tongue', old('student_mother_tongue',isset($admissionForm['student_mother_tongue']) ? $admissionForm['student_mother_tongue']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_tongue'), 'id' => 'student_mother_tongue']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_mother_occupation', $admissionForm['form_keys']))
    <div class="col-lg-9 col-md-9 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_mother_occupation') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_mother_occupation', old('student_mother_occupation',isset($admissionForm['student_mother_occupation']) ? $admissionForm['student_mother_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_occupation'), 'id' => 'student_mother_occupation', 'rows'=> 2]) !!}
        </div>
    </div>
    @endif

</div>

<!-- Guardian Information -->
@if(in_array('student_guardian_name', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Guardian Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_guardian_name', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_name', old('student_guardian_name',isset($admissionForm['student_guardian_name']) ? $admissionForm['student_guardian_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_name'), 'id' => 'student_guardian_name']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_guardian_mobile_number', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_mobile_number', old('student_guardian_mobile_number',isset($admissionForm['student_guardian_mobile_number']) ? $admissionForm['student_guardian_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_mobile_number'), 'id' => 'student_guardian_mobile_number', 'maxlength'=> '10']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_guardian_email', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_email', old('student_guardian_email',isset($admissionForm['student_guardian_email']) ? $admissionForm['student_guardian_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_email'), 'id' => 'student_guardian_email']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_guardian_relation', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_relation') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_relation', old('student_guardian_relation',isset($admissionForm['student_guardian_relation']) ? $admissionForm['student_guardian_relation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_relation'), 'id' => 'student_guardian_relation']) !!}
        </div>
    </div>
    @endif

</div>

<!-- Temporary Address Information -->
@if(in_array('student_temporary_address', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Temporary Address Information:</h5>
</div>
@endif

<!-- Address section -->
<div class="row clearfix">
    @if(in_array('student_temporary_address', $admissionForm['form_keys']))
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_temporary_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_temporary_address', old('student_temporary_address',isset($admissionForm['student_temporary_address']) ? $admissionForm['student_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_address'), 'id' => 'student_temporary_address', 'rows'=> 2]) !!}
        </div>
    </div>
    @endif

</div>

<!-- City section -->
<div class="row clearfix">

    @if(in_array('student_temporary_country', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_county') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_country', $admissionForm['arr_country'],isset($admissionForm['student_temporary_country']) ? $admissionForm['student_temporary_country'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_country', 'onChange'=> 'showStates(this.value,"student_temporary_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_temporary_state', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_state') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_state', $admissionForm['arr_state_t'],isset($admissionForm['student_temporary_state']) ? $admissionForm['student_temporary_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_state' , 'onChange'=> 'showCity(this.value,"student_temporary_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_temporary_city', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_city') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_city', $admissionForm['arr_city_t'],isset($admissionForm['student_temporary_city']) ? $admissionForm['student_temporary_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_temporary_pincode', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_temporary_pincode', old('student_temporary_pincode',isset($admissionForm['student_temporary_pincode']) ? $admissionForm['student_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_pincode'), 'id' => 'student_temporary_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>
    @endif

</div>

<!-- Permanent Address Information -->

@if(in_array('student_permanent_address', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Permanent Address Information:</h5>
</div>
@endif

<!-- Address section -->
<div class="row clearfix">

    @if(in_array('student_permanent_address', $admissionForm['form_keys']))
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_permanent_address') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_permanent_address', old('student_permanent_address',isset($admissionForm['student_permanent_address']) ? $admissionForm['student_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_address'), 'id' => 'student_permanent_address', 'rows'=> 2]) !!}
        </div>
    </div>
    @endif
</div>

<!-- City section -->
<div class="row clearfix">

    @if(in_array('student_permanent_county', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_county') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_county', $admissionForm['arr_country'],isset($admissionForm['student_permanent_county']) ? $admissionForm['student_permanent_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_county' , 'onChange'=> 'showStates(this.value,"student_permanent_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_permanent_state', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_state') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_state', $admissionForm['arr_state_p'],isset($admissionForm['student_permanent_state']) ? $admissionForm['student_permanent_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_state', 'onChange'=> 'showCity(this.value,"student_permanent_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_permanent_city', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_city') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_city', $admissionForm['arr_city_p'],isset($admissionForm['student_permanent_city']) ? $admissionForm['student_permanent_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif

    @if(in_array('student_permanent_pincode', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_pincode') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_permanent_pincode', old('student_permanent_pincode',isset($admissionForm['student_permanent_pincode']) ? $admissionForm['student_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_pincode'), 'id' => 'student_permanent_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>
    @endif
</div>

<!-- Health Information -->
@if(in_array('student_height', $admissionForm['form_keys']))
<div class="row col-lg-12">
    <h5 class="heading-style">Health Information:</h5>
</div>
@endif

<div class="row clearfix">

    @if(in_array('student_height', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_height') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_height', old('student_height',isset($admissionForm['student_height']) ? $admissionForm['student_height']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_height'), 'id' => 'student_height']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_weight', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_weight') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_weight', old('student_weight',isset($admissionForm['student_weight']) ? $admissionForm['student_weight']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_weight'), 'id' => 'student_weight']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_blood_group', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_blood_group') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_blood_group', old('student_blood_group',isset($admissionForm['student_blood_group']) ? $admissionForm['student_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_blood_group'), 'id' => 'student_blood_group']) !!}
        </div>
    </div>
    @endif

    @if(in_array('student_vision_left', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_left') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_left', old('student_vision_left',isset($admissionForm['student_vision_left']) ? $admissionForm['student_vision_left']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_left'), 'id' => 'student_vision_left']) !!}
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">

    @if(in_array('student_vision_right', $admissionForm['form_keys']))
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_right') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_right', old('student_vision_right',isset($admissionForm['student_vision_right']) ? $admissionForm['student_vision_right']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_right'), 'id' => 'student_vision_right']) !!}
        </div>
    </div>
    @endif

    @if(in_array('medical_issues', $admissionForm['form_keys']))
    <div class="col-lg-9 col-md-9">
        <lable class="from_one1">{!! trans('language.medical_issues') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group bs-example">
            {!! Form::text('medical_issues', old('medical_issues',isset($admissionForm['medical_issues']) ? $admissionForm['medical_issues']: ''), ['class' => 'form-control','placeholder'=>trans('language.medical_issues_p'), 'id' => 'medical_issues','data-role'=>'tagsinput']) !!}
        </div>
    </div>
    @endif

</div>


@if(isset($admissionForm['form_pay_mode']) && $admissionForm['form_type'] == 0)
<!-- Paymenet Mode -->
<!-- <div class="header">
    <h2><strong>Paymenet</strong> Mode</h2>
</div> -->

@php 
    $pay_mode = [];
    $paymodes = explode(',', $admissionForm['form_pay_mode']);
    if(isset($admissionForm['admission_data_pay_mode'])) {
        $pay_mode = explode(',', $admissionForm['admission_data_pay_mode']);
    }
    
@endphp
<br />
<br />
<div class="row clearfix">
    
     @foreach($paymodes as $paymode)
     <div class="col-lg-3 col-md-4 col-sm-4">
         <div class="checkbox">
             <input id="check{{$paymode}}" type="checkbox" @if(isset($pay_mode)) @if(in_array($paymode, $pay_mode)) checked @endif @endif name="paymodes[]" class="checkBoxClass" value="{{$paymode}}">
             <label for="check{{$paymode}}" class="checkbox_1">{{$paymode}}</label>
         </div>
     </div>
     @endforeach
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.mode') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_data_mode', $admissionForm['arr_mode'],isset($admissionForm['admission_data_mode']) ? $admissionForm['admission_data_mode'] : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_data_mode', 'required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fees_status') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_data_fees', $admissionForm['arr_fees'],isset($admissionForm['admission_data_fees']) ? $admissionForm['admission_data_fees'] : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_data_fees', 'required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>

@endif

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/admission') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>
{!! Form::close() !!}


{!! Html::script('public/admin/assets/plugins/jquery-validation/jquery.validate.js') !!}
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
{!! Html::script('public/admin/assets/js/pages/forms/form-validation.js') !!}
{!! Html::script('public/admin/assets/js/select2.full.js') !!}


</body>
</html>

<script type="text/javascript">

function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function(e) {
	            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
	            $('#imagePreview').hide();
	            $('#imagePreview').fadeIn(650);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#imageUpload").change(function() {
	    readURL(this);
    });
    
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#enquiry-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_enroll_number: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_email: {
                    required: true,
                    email: true,
                    emailvalid: true
                },
                student_image: {
                    extension: 'jpg,png,jpeg,gif'
                },
                student_blood_group: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                medical_issues: {
                    required: true
                },
                student_father_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_father_mobile_number: {
                    required: true,
                    digits: true
                },
                student_father_email: {
                    required: true,
                    email: true,
                    emailvalid: true
                },
                student_father_occupation: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_mother_mobile_number: {
                    required: true,
                    digits: true
                },
                student_guardian_name: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                student_sibling_name: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },student_guardian_mobile_number: {
                    digits: true
                },
                student_guardian_email: {
                    email: true,
                    emailvalid: true
                },
                student_mother_email: {
                    email: true,
                    emailvalid: true
                },
                student_temporary_pincode: {
                    digits: true
                },
                student_permanent_pincode: {
                    digits: true,
                    required: true
                },student_login_name: {
                    required: true
                },
                student_login_email: {
                    required: true,
                    email: true,
                    emailvalid: true
                },
                student_login_contact_no: {
                    digits: true,
                    required: true
                },
                student_adhar_card_number: {
                    digits: true
                },
                student_height: {
                    number: true
                },
                student_weight: {
                    number: true
                },
                student_dob: {
                    required: true,
                    date: true,
                    minDate: true,
                },
                student_reg_date: {
                    required: true,
                    date: true,
                },
                student_privious_tc_date: {
                    required: true,
                    date: true,
                    minDate: true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
        // d = new Date();
        // d.setFullYear(d.getFullYear() - 1);
        // minD = new Date();
        // minD.setFullYear('1990');
        // $('#student_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false,maxDate : d, minDate :minD }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#student_reg_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,maxDate : new Date() }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#student_privious_tc_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,maxDate : new Date()}).on('change', function(ev) {
        //     $(this).valid();  
        // });
    });

    function getSection(class_id,type)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    if(type == 1) {
                        $("select[name='admission_section_id'").html('');
                        $("select[name='admission_section_id'").html(data.options);
                        $("select[name='admission_section_id'").removeAttr("disabled");
                        $("select[name='admission_section_id'").selectpicker('refresh');
                        $(".mycustloading").hide();
                    } else if(type == 2){
                        $("select[name='current_section_id'").html('');
                        $("select[name='current_section_id'").html(data.options);
                        $("select[name='current_section_id'").removeAttr("disabled");
                        $("select[name='current_section_id'").selectpicker('refresh');
                        $(".mycustloading").hide();
                    }
                }
            });
        } else {
            if(type == 1) {
                $("select[name='admission_section_id'").html('');
                $("select[name='admission_section_id'").selectpicker('refresh');
                $(".mycustloading").hide();
            } else if(type == 2){
                $("select[name='current_section_id'").html('');
                $("select[name='current_section_id'").selectpicker('refresh');
                $(".mycustloading").hide();
            }
        }
    }

    function check_parent_existance(){
        var mobile_no = $("#student_father_mobile_number").val().toString();
        if(mobile_no.length == 10){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-parent-existance')}}",
                type: 'GET',
                data: {
                    'mobile_no': mobile_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == 1){
                            $("#student_login_name").val(data.student_login_name);
                            $("#student_login_email").val(data.student_login_email);
                            $("#student_login_contact_no").val(data.student_login_contact_no);
                            $("#student_father_name").val(data.student_father_name);
                            $("#student_father_email").val(data.student_father_email);
                            $("#student_father_occupation").val(data.student_father_occupation);
                            $("#student_father_annual_salary").val(data.student_father_annual_salary);
                            $("#student_mother_name").val(data.student_mother_name);
                            $("#student_mother_mobile_number").val(data.student_mother_mobile_number);
                            $("#student_mother_email").val(data.student_mother_email);
                            $("#student_mother_occupation").val(data.student_mother_occupation);

                            $("#student_mother_tongue").val(data.student_mother_tongue);
                            $("#student_guardian_name").val(data.student_guardian_name);
                            $("#student_guardian_email").val(data.student_guardian_email);
                            $("#student_guardian_mobile_number").val(data.student_guardian_mobile_number);
                            $("#student_mother_annual_salary").val(data.student_mother_annual_salary);
                            $("select[name='student_father_annual_salary'").selectpicker('refresh');
                            $("select[name='student_mother_annual_salary'").selectpicker('refresh');
                        }
                    }
                }
            });
        }
    }

    

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='admission_class_id'").html(data.options);
                    $("select[name='student_sibling_class_id'").html(data.options);
                    $("select[name='current_class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='admission_class_id'").html('<option value="">Select Class</option>');
            $("select[name='current_class_id'").html('<option value="">Select Class</option>');
            $("select[name='student_sibling_class_id'").html(data.options);
            $(".mycustloading").hide();
        }
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/stream/get-stream-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='stream_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='stream_id'").html('<option value="">Select Stream</option>');
            $(".mycustloading").hide();
        }
    }

    function checkSectionCapacity(section_id){
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-section-capacity')}}",
                type: 'GET',
                data: {
                    'section_id': section_id
                },
                success: function (data) {
                    if(data == 'False'){
                        $('#error-block').html("Sorry, We have already exceed section's capacity. Please add student into different section.");
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $('#error-block').html('');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                    $(".mycustloading").hide();
                }
            });
        } 
    }

    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

    function checkAvailability(){
        var enquiry_no = $("#enquiry_number").val().toString();
        if(enquiry_no != ''){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-enquiry-existance')}}",
                type: 'GET',
                data: {
                    'enquiry_no': enquiry_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == 1){
                            $("#student_enroll_number").val(data.student_enroll_number);
                            $("#student_roll_no").val(data.student_roll_no);
                            $("#student_name").val(data.student_name);
                            $("#student_reg_date").val(data.student_reg_date);
                            $("#student_dob").val(data.student_dob);
                            $("#student_email").val(data.student_email);
                            $("#student_gender").val(data.student_gender);
                            $("#student_type").val(data.student_type);
                            $("#student_category").val(data.student_category);
                            $("#caste_id").val(data.caste_id);
                            $("#religion_id").val(data.religion_id);

                            $("#nationality_id").val(data.nationality_id);
                            $("#student_sibling_name").val(data.student_sibling_name);
                            $("#student_sibling_class_id").val(data.student_sibling_class_id);
                            $("#student_adhar_card_number").val(data.student_adhar_card_number);
                            $("#student_privious_school").val(data.student_privious_school);
                            $("#student_privious_tc_no").val(data.student_privious_tc_no);
                            $("#student_privious_tc_date").val(data.student_privious_tc_date);
                            $("#student_privious_result").val(data.student_privious_result);
                            $("#student_temporary_address").val(data.student_temporary_address);
                            $("#student_temporary_city").val(data.student_temporary_city);
                            $("#student_temporary_state").val(data.student_temporary_state);
                            $("#student_temporary_county").val(data.student_temporary_county);
                            $("#student_temporary_pincode").val(data.student_temporary_pincode);
                            $("#student_permanent_address").val(data.student_permanent_address);
                            $("#student_permanent_city").val(data.student_permanent_city);
                            $("#student_permanent_state").val(data.student_permanent_state);
                            $("#student_permanent_county").val(data.student_permanent_county);
                            $("#student_permanent_pincode").val(data.student_permanent_pincode);
                            $("#group_id").val(data.group_id);
                            $("#title_id").val(data.title_id);
                            $("#stream_id").val(data.stream_id);
                            $("#student_height").val(data.student_height);
                            $("#student_weight").val(data.student_weight);
                            $("#student_blood_group").val(data.student_blood_group);
                            $("#student_vision_left").val(data.student_vision_left);
                            $("#student_vision_right").val(data.student_vision_right);
                            $("#medical_issues").val(data.medical_issues);
                            $("#student_login_name").val(data.student_login_name);
                            $("#student_login_email").val(data.student_login_email);
                            $("#student_login_contact_no").val(data.student_login_contact_no);
                            $("#student_father_name").val(data.student_father_name);
                            $("#student_father_mobile_number").val(data.student_father_mobile_number);
                            $("#student_father_email").val(data.student_father_email);
                            $("#student_father_occupation").val(data.student_father_occupation);
                            $("#student_father_annual_salary").val(data.student_father_annual_salary);
                            $("#student_mother_name").val(data.student_mother_name);
                            $("#student_mother_mobile_number").val(data.student_mother_mobile_number);
                            $("#student_mother_email").val(data.student_mother_email);
                            $("#student_mother_occupation").val(data.student_mother_occupation);
                            $("#student_mother_annual_salary").val(data.student_mother_annual_salary);
                            $("#student_mother_tongue").val(data.student_mother_tongue);
                            $("#student_guardian_name").val(data.student_guardian_name);
                            $("#student_guardian_email").val(data.student_guardian_email);
                            $("#student_guardian_mobile_number").val(data.student_guardian_mobile_number);
                            $("#student_guardian_relation").val(data.student_guardian_relation);
                        } 
                    } else {
                            $("#enquiry-error").html('Sorry no enquiry found !! <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>');
                            $("#enquiry-error").show();
                            $("#enquiry-form").trigger("reset");
                    }
                }
            });
        }
    }

</script>

